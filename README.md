# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

MAJOR version when you make incompatible API changes,

MINOR version when you add functionality in a backwards-compatible manner, and

PATCH version when you make backwards-compatible bug fixes.

## [Unreleased]

## [0.1.46] - 2019-9-19
- BUG: swap back price and shipping on purchase and sale grid view
- BUG: Promotional text under price was not showing different types on purchases and sale page
- BUG: points and prices on sale/purchase gridview are wrong
- BUG: Corrected shipping and sale price

## [0.1.45] - 2019-9-12
- fixed dispute detail bug
## [0.1.44] - 2019-9-11
- Added About Screen
- BUG: fixed seller policy title
- BUG: Add some height to promotions banner
- BUG: Update boolean fields for ListingDetails/PostDetails/Feed/Browse/and when viewing profiles listings & posts
- Update Dispute screen to include charge dispute information
- BUG: View Dispute Details is disabled in the sale page (list view). Also the pop-up box is partially out of view.
- BUG: Add comment dispute detail show something went wrong error
## [0.1.43] - 2019-8-24
- BUG: Should not be able to offer/purchase on a listing ON HOLD
- BUG: Seller should not be able to open a dispute on a sale!! STILL ISSUE!
- Updated push notification to use new icon
- BUG: Printing shipping label on Sale Details does not show popup before printing
- BUG: should say "view active offers" (plural) as the owner of the listing
- BUG: Top left button hard to press in this screen
- BUG: Top left button doesn't work on profile screen
- BUG: Feedback As Buyer shows incorrect username
- Now canReportDispute on listing details is false when seller logged in

## [0.1.42] - 2019-8-9
- Image compression removed
- BUG: Dispute greater than 7 days now shows cancel purchase screen
- BUG: Issue with seller setup and address resolved
- Updated Android API level to 29 as google specified

## [0.1.41] - 2019-7-30
- BUG: Login with Oauth didn't send device ID
- BUG: choosing petite on edit profile causes body type to not show up.

## [0.1.40] - 2019-7-17
- BUG: Update Editing Post to edit post product tags.
- Update some modal popup screens when a charge fails.
- BUG: When no address set, adding payment method had UI where there was existing address

## [0.1.39] - 2019-7-2
- Listing details change word "VIEW OFFERS" to "VIEW ACTIVE OFFER"
- Sales screen list view pop-up should have shadow border
- BUG: "1 fashionista liked this" toggle like instead of going to listing like
- BUG: getting post like is calling listing like api
- BUG: username not clickable in notifications
- BUG: Buyer declines offer but history of declined offer shows on seller side and wrong design
- BUG: Keep getting 'Something went wrong here' error message on notifications screen.
- BUG: where are we supposed to redirect after submitting an offer?
- BUG: Print shipping label does not show pop-up before purchase
- BUG: Google login still has issues
- BUG: Onboarding bloggers GET/POST with Text gives 500 error.
- BUG: Android login on small screens cutting off button on bottom
- BUG: Clicking on whitespace or anywhere other than links in filter page goes to listing details page
- BUG: Video Post now opens in app browser and play image open in app browser

## [0.1.38] - 2019-6-6
- BUG: clicking on user redirects to their profile with blank data
- BUG: Make an offer -> screen with card and address -> tap grey area multiple times -> random screen
- BUG: clicking outside the box when submitting an offer redirects to different listing
- BUG: clicking on bottom left of offer screen redirects to notifications settings page
- BUG: other user profile listing filter jump to another random page
- BUG: Clicking on whitespace or anywhere other than links in filter page goes to listing details page
- BUG: Reporting an issue as a buyer for same item shows different screens?
- BUG: cannot open a dispute as a buyer
- BUG: Buyer should not have to be asked to pick a reason to cancel sale
- BUG: Space out Sale and Purchase details screen a bit more
- make app icon round
- BUG: Toggling between My Sales screen styles produces issued
- For empty state, put this logo instead of current logo
- BUG: make sure offer bubbles are wider and same width
- ANDROID for modals use this logo


## [0.1.37] - 2019-5-27
- fixed post tags issue on small phones

## [0.1.36] - 2019-5-27
- BUG: user search page doesn't have infinite scroll
- BUG: Create Seller Account has MAJOR issue with adding a new payment
-   When setting up a seller account for the first time, under adding a payment, it looks like what's happening is the payment is being saved but not retrieved? or displayed back on the seller account setup screen
-   When adding a new payment and selecting an existing address, after saving, it looks like this saves the payment in the database but it create a NEW address, rather than linking the new payment to an existing address.
-   Same as point above, but with adding a new address instead of selecting from existing ones.
-   The address selection screen is missing a few important data for addresses under "Other addresses" section
- DESIGN ENHANCEMENTS: Small updates to the login & signup screens, and forgot password screens
- BUG: Unread Notifications not being marked as read & design updates
- BUG: create listing -> select size -> the top left right arrows flicker between each option
- BUG: Adding a listing and switching screens error
- BUG: Create listing -> Select Color -> grey and silver box border is shorter than box
- Create a Bynmix Seller Policy Screen
- BUG: the left and right arrows to flip through images is incorrect
- BUG: report an issue, issue drop down not showing
- BUG: Listing Brand selection has some incorrect design features
- BUG: Should not see a Report An issue icon if logged in as myself viewing a listing
- Create Listing Updated UI
- Added instagram icons
- BUG: onboarding users screen does not implement infinite scroll
- BUG: onboarding brand can't close board except clicking back button. When user click background, close keyboard
- BUG: Tags in edit/create listing or post should be updated from the new redesign screens.
- BUG: remove @ sign in front of usernames in onboarding blogger
- BUG: Wrong popup when trying to delete a default payment in Wallet
- BUG: other payment and addresses not appearing when selecting something other than default
- BUG: Listing offers screen has design flaw
- BUG: Comments of listings highlighted incorrectly
- BUG: Post Tags -> last modal button is cut off on small phones
- BUG: edit post now allow peopel to add post tags
- BUG: edit photo placeholder image not in app in post and listing
- Upgrade facebook sdk to 5.0.1
- All modal use new logo
- BUG: fix typo on login page saying "terms of service" instead of "terms of services"
- Updated all empty state logos to the new logo
- Welcome screen now uses new logo
- Seller Account screen now uses new logo
- BUG: Seller Account -> Add address -> Second address leave blank line if empty
- Fixed create listing back button issue where navigation would get stuck
- BUG: make offer -> offer page -> offer modal -> title has extra space
- BUG: adding invalid address should report error not 500 error
- BUG: Changed direct purchase modal to shorten the words used since button overflowed
- BUG: users onboarding who didn't get through onboarding still have device id saved
- BUG: top left button on user filter list was very hard to click
- BUG: post tags modal buttons are cut off on small phones
- BUG: should show print shipping label button under sale details screen
- BUG: spacing is off in top part that shows user suggestions
- BUG: promotion breakdown not showing in purchase details page
- BUG: Wrong popup when trying to delete a default payment in Wallet
- Reporting an issue on listing
- Redesign listing offer
- Redesign listing buy now
- Edit posts can now add post tags to new image
- Cannot delete default address anymore
- Cannot delete default card anymore
- BUG: Wrong popup when trying to delete a default payment in Wallet



## [0.1.35] - 2019-5-07
- BUG: My Address -> First address missing address information
- Added instagram link to profile
- Added instagram icon to profile
- Redesign Add Post/Edit Post
- Redesign Add Listing/Edit Listing
- Redesign Notification Settings
- Redesign Notifications/Offering
- Redesign Leave Feedback
- Redesign Reviews
- Redesign Post Dots
- Redesign Wallets screen
- Redesign Wallets popups
- Redesign to mark your comments pink
- Redesign notification unread to pink
- Clicking on Heart on Listing Detail goes to Listing like page
- BUG: Fixed facebook login issue on google play by adding hash
- Top left/right arrow on filters only show on overflow and new arrows
- Capitalize the filter types and added selected pink checkmark
- Redesign made listing colors border thicker
- BUG: fixed modal on add post tag where button is cut off on smaller phones
- BUG: Tap to view product tags" background bar should be black
- BUG: Edit listing no back button
- BUG: remove @ sign in front of usernames

## [0.1.34] - 2019-4-25
- Redesign listing filters  Profile user reviews - cards empty states
- BUG: Report an issue in "My Purchases" gives error
- BUG: in-app. Buyer cancels purchase gives error but cancels anyway.
- BUG: Google Login not working for new accounts
- BUG: remove abstract, geometric background
- Redesign Post Details
- BUG: fix Heart sizing on post and listing details
- Clicking on Heart of post detail goes to My liking page post section (listing detail one not done yet)
- Redesign: Profile -> My Addresses (and all popups)
- BUG: input text boxes not rounded in onboarding screen 4
- BUG: In Profile Screen, remove @ symbol before username
- BUG: Make sure that /Best-match loads 25 users at a time
- Redesign: Profile -> My Byn (and all popups)
- Redesign: Profile -> My Posts (and all popups)
- Redesign: Profile -> My Likes -> Listings
- Redesign: Profile -> My Likes -> Posts
- Redesign: Profile -> My Wallet (and all popups)
- Redesign: Seller Account Setup (Should be able to use similar concepts from My Wallet and My Addresses)
- BUG: any address card if no address two show white line. Dont show empty line if no address 2
- BUG: GET api/listings/{resource}/like/all and in follow following missing field userBrandsFormatted, userDescriptorsFormatted
- BUG: Align text and make text bigger in On boarding screen 4.
- Redesign Post Cards
- Redesign Listing Cards
- BUG: The select existing address screen from the Setup Seller Account is off
- BUG: Edit Address not working
- BUG: User Search -> Filter -> Style and Body Type -> API error something went wrong
- BUG: some background pages still have old image


## [0.1.33] - 2019-4-16
- User Search Redesign
- Other profile -> My Post -> Post card Redesign
- My profile -> My Post -> Post card Redesign
- BUG: Post Card overlapping image
- BUG: Forget password button not capitalize
- BUG: Reset password button not capitalize
- All Empty state new design
- BUG: about/terms/privacy not align at bottom of screen
- Alls user cards redesign on user search and followers
- All Post Cards are redesigned
- All Listing Cards are redesigned
- Bottom navigation is now redesigned
- User Carousel is now redesigned
- Listing/User filter partly redesigned
- All Onboarding pages redesigned
- Forgot Password redesign
- Welcome note redesigned
- Stats page redesigned
- Notification My Offer redesigned
- logout modal redesign
- following/follower redesign
- edit profile now 3 tabs
- BUG: Forgot password Back to login cut off on smaller phone
- BUG: Add Onboarding Tag line appear on 2 lines
- BUG: Listing filter Brand Search placeholder text too big and cut off
- BUG: add post tag can't save

## [0.1.32] - 2019-4-3
- added redesign elements to login, terms, privacy, createaccount pages
- BUG: Pass FinishedRegister = True after onboarding is complete
- BUG: Onboarding - selecting brands keyboard glitch
- BUG: Need to check that balance is above 0 to withdraw.
- Make first letter capitalized when focused in text box
- BUG: My likes > Posts. Clicking on a post does not redirect to actual post
- BUG: Editing a listing gives shipping error
- BUG: Clicking on a listing in Browse/Feed screens shows profile screen before going to Listing
- BUG: Should say "START SELLING" & make dropdowns larger text and smaller boxes
- BUG: Switching out of profile page then hitting Create button redirects back to profile screen
- BUG: Addresses not displaying when creating a new address in Seller Account Setup
- BUG: Switching out of profile page then hitting Create button redirects back to profile screen
- BUG: Registering with Facebook/Google first name and last name
- BUG: App crashes when I go to my offers and click to view an offer
- BUG: clicking on a listing brings me temporarily to the profile page then listong page.
- BUG: Create listing, Select shipping weight glitch
- BUG: Edit Sizing -> swipe to right on create/edit listing crash app
- Added agree with terms to register screen

## [0.1.31] - 2019-3-12
- View More button should redirect to User Search Screen
- Update Payout History screen to be paginated
- BUG: Cannot withdraw for first time with new bank account
- Cancel Sale under My Purchases screen should have different functionality.
- BUG: "Something went wrong" when user tries to change profile image with Camera
- BUG: Updating a listing shows popup "Something went wrong"
- BUG: Bank account nickname not saving
- BUG: User Short title does not save during Onboarding
- BUG: Seller Account setup - Create and Select Addresses Buggy
- Check for and display promotion banners on app load
- BUG: Changing the address of a Card gives an error.
- BUG: Update Listing lowest allowable offer shows error popup
- BUG: White Ribbon displaying points is off
- Update Dispute details screen when dispute has been canceled or closed
- Refactor code base and shrink homeactivity code

## [0.1.30] - 2019-3-1
- 3 new notification types
- Users should be given the option to take a snapshot wherever a photo is asked Background user profile photo User Profile photo, Creating a Listing, Creating a Post, Opening a dispute
- Report issue comments in dispute details
- Listing details shows "VIEW OFFERS" but should read "VIEW OFFER" because this is not the buyer's listing.
- When a seller makes a counter offer to a buyer's offer, they will receive a congrats! popup. The popup shows ** @null ** but should show the buyer's username.
- When a seller makes a counter offer to a buyer's offer, they will receive a congrats! popup. The popup shows ** @null ** but should show the buyer's username.
- Current UI listing offers page (that a seller sees when clicking "View Offers" onf listing details screen) is incorrect. It should look like this
- If a seller enters incorrect information in Step 1 of a new seller account setup, Stripe will spit back an error. The error mobile receives is a 400, and should use this popup
- Mobile Payout withdraw function
- Mobile Payout withdraw history
- Receive offers -> click listing -> Listing offers decimal typo
- BUG: clicking dispute detail image enlarges it
- BUG: browse page user icon didnt link to user filter

## [0.1.29] - 2019-2-17
- Removed unnecessary toast message throughout app
- Updated all offer pages with new flags
- Report an issue as Buyer on My purchase
- View Dispute detail as Seller on My Sales
- View Dispute detail as Seller on My Purchase
- BUG: Fix Sale cancel button not showing up
- Updated logic for buy it now and offer on listing. Cancel all offers when switch to buy it now
- Updated Purchases/Sale Detail
- Updated buttons on my purchase/my sales
- Increase button size on my purchase/my sales to prevent fat fingering buttons
- New Seller flow added before adding listing
- Fix issues regarding date of birth crashing api
- Comments on report issue(Pending)

## [0.1.28] - 2019-2-03
- HOTFIX: Updated shipping label
- HOTFIX: Updated new offer logic
- HOTFIX: Height Slider

## [0.1.27] - 2019-2-01
- BUG: Notification Type: Delivered. Missing top right icon
- View User Ratings Screens (As Buyer and As Seller)
- BUG: Updates to Purchase and Sale Stats Screens
- BUG: Sale Detail should say "Purchased By"
- BUG: Signing up with Facebook puts the user's entire full name in the user's firstName field
- Update Sales & Purchases logic to use bool fields.
- Pass isMobileRegistration = True when registering with Facebook or Google
- BUG: A disabled listing should not be viewable
- BUG: Under My Sales (List View), I should not be able to cancel orders
- BUG: Seller Counter Offer screen should say "Return Address"
- Updated new offer logic (Partial done, more QA)
- Updated shipping label (There are bugs, fix in hotfix)
- User filters (There are bugs, fix in hotfix)

## [0.1.26] - 2019-1-23
- terms and condition page
- privacy policy page
- user search basic page + searchable with 2 letters
- fixed dots fully
- BUG: edit listing error with shipping weight
- BUG: updating carousel of followers when you do pull refresh
- BUG: offer notification text change
- BUG: Onboarding blogger new logic
- BUG: backgound image upload crashing
- BUG: remove short title requirement
- BUG: fixed 3 buttons on offers overlapping
- toggle disable listing new logic
- BUG: my sale detail updated for commission
- BUG: Total Purchases in My Stats Screen should be a decimal
- payout page designs
- added rating to profile
- added view reviews to profile

## [0.1.25] - 2019-1-14
- finished onboarding blogger page
- finished onboarding profile page
- if user doesn't complete onboarding, we jump back to same spot they should be in
- cancel sale functionality
- relist item functionality
- fix design of posts and listing card
- migrated all hardcoded value to api (notification,listing filter, shipping weight/prices)
- changed post dots to tooltips and show them on feed/browse/my post (still has bug with too many tooltips)

## [0.1.24] - 2018-12-30
- changed new welcome page

## [0.1.23] - 2018-12-29
- BUG: fix typo in add tags
- Can't make image smaller as to calculate percentage location for dots, need image to be full width. Instead we made move button to top so theres more room to see image without scrolling
- BUG: remove text in onboarding add tags
- Add 5 dots
- made dots bigger
- Edit dots
- Edit dots show how many dots you have
- BUG: fix issue where if no image present edit post crash app
- Post Detail page
- Clicking on Post goes to Post detail page now
- Added hide tagged product now
- See post dots on other user profile and my posts
- Autocomplete listing for add dots
- BUG: fix adding add dot api
- BUG: fix edit dots saving correct values
- Select Brand screen is updated
- Select Tagged screen is updated
- removed unused imports

## [0.1.22] - 2018-12-06
- fixed listing detail duplication bug

## [0.1.21] - 2018-12-22
- add post dots
- For push notification icon color use the new purple
- BUG: When i leave a comment, it duplicates the listing detail and pastes that at the bottom of the listing details already there
- BUG: on onboarding bloggers search. After i type in two letters the auto search kicks in but the keyboard disappears. Can we keep the keyboard up so that the user can keep typing?
- Onboarding bloggers - needs a space between # and the word "followers".
- onboarding, the bottom popup
- My purchases - purchase stats. The Total purchases should be an int value. It shows the number of purchases made. currently looks like a dollar value.
- I didnt realize how small the tracking information is when you click on "View Shipping History". Its so tiny. Can we make it bigger
- in purchase/sales timeline view can you remove the left and right padding of each sale or purchase?
- Sales Stats - Total sales should be an int number because it shows the number of items a user ha sold so far.
- Offer/Counter offer, make the input box only integer no floats
- something i forgot to mention: When a user decides to click on the "What is this" link on adding post dots, they'll be shown a tutorial with an order like: "What are post tags?" -> "How do I remove Post tags?" -> "How do i link post tags?"
- In the "How do i remove Post tags?" screen, i wanted to see if an animation is possible
- purple hand icon that enlarges a bit when clicking down on the drop and dragging the dot out of the border and releasing the dot, and the hand icon shrinks a little back to its regular size.
- Update all api endpoint to https

## [0.1.20] - 2018-12-05
- Sale Gridview
- Sale Timeline
- Purchase Timeline completion
- Integrate push notification
- Default white icon for push notification
- Purchase Detail
- Sale Detail
- follow/unfollow on sale/purchase detail page
- fix all feed/browse error
- BUG:some pages missed the padding on top when scrolled
- Add empty state for notification, my offer, sales and purchase pages
- Sale stats
- Purchase stats
- preparing dots
- allow multi photo upload and cropping
- BUG: fixed upload image crashing issue

## [0.1.19] - 2018-10-31
- BUG: After onboarding is complete for a new user, I no longer see the popup now that tells the user about the middle button. See Feed Onboarding Screen.
- BUG: Adding a new payment for a user that just registered is giving an error. I added correct card info + an address. Only thing i didnt do is check of Set as Default Payment. I Hit save and first error says Oops something went wrong. I hit save again and now the error states Invalid Address. The address was actually invalid but not sure why the first message happened. This is an invalid address because it should be Apt c5. but idk why i got the first error message when i submitted.
- When making a bid, if i place a bid that is higher than the price listed i dont see the error until after the second screen. show a modal with the message on the first screen where im submiting the offer if price is higher than listing. Copy the error message you get when you submit a higher price as message
- the error message shown, can you please add padding on the left and right of the error modal? The error text spans all the way to the edges on either side.
- When i go to accept an offer i get a little message at the bottom saying that offer was successful but then i get a modal popup that says Something went wrong. Please try again later, and get redirected to My Purchases page.

- Remove the toast offer was successful
- BUG: Another weird thing relating to this, if i go to the offer history of the listing i just accepted an offer for, i see in the history that theres an Offer Accepted and an Offer Declined in the history timeline. Are you making calls to both endpoints? Check is it API issue or app issue
- Make My Address page full scroll with Add New Address button float on top.
- Make My Wallet page full scroll with Add New Payment button float on top.
- Make My Offer page full scroll with make an offer section float on top.
- If user cannot make an offer, the white timeline for offers should go to bottom of the page with no extra padding
- CounterOffer page full scroll with make an offer section float on top. Make sure padding at bottom so bar doesn't overlap anything.
- If user cannot make a counteroffer, the white timeline for offers should go to bottom of the page with no extra padding
- Test that a Buyer can accept a Seller Counter offer in My Notifications.
- Test that a Buyer can decline a Seller Counter offer in My Notifications
- BUG: Go to My Byn > then edit a listing that is On Hold. In the edit listing page, the Listing Status Should show as On Hold but it looks like its either not set or its set to the wrong option in that dropdown.
- it looks like there is no option to make a new offer if a previous offer has been declined. as a potential buyer i should be able to make a new offer.
- In `Received Notifications`, I can view the offer history of a listing that has been purchased. When I go to that listing details I can still edit it. I will need to make a screen to show what it should look like when viewing a listing that has been purchased already. I should not be able to edit it and it should show that it's been purchased.
- On a buy it now, I get the Congratulations Popup, immediately followed by a Something went wrong Popup. Since there's no offer history because it was a direct purchase. It does show as sold thoough.
- Post liked Notification  link out to the post with in app browser if link is present. Otherwise, user cannot click on it
- Test: Person A make a listing, Person B make offer, Person C make offer, Person A accepts Person B offer, Person B offer-history has accept, Person C has offer-history decline
- Test: Person A make a listing, Person B make offer, Person C make offer, Person A counter offer Person B -> Person C -> nothing change for C offer-history
- Test: Person A make a listing, Person B make offer, Person C make offer, Person A counter offer Person B -> Person B counter offer Person A -> Person A decline Person B -> Person C -> nothing change for C
- Bug: New user -> add new address -> redirect page has wrong icon on footer and address didnt show
- new screen for you to show what Listing Details page should look like when viewing a listing that has been purchased.
- My Purchases - Purchases Page.
- To leave feedback on a purchase
- Get tracking details
- Ignore Report an Issue please ignore for now.
- Ignore Timeline please ignore for now.
- White Ribbon = points in dispute. Blue Ribbon = points pending, waiting on feedback or delivery
- For My Purchases - Stats Screen
- Design changes Feed and Whats New sections under Zeplin
- Bug: in Selecting existing address screen when creating a new payment and clicking on Select existing address, the SELECT buttons under Other Addresses do not align with the other SELECT buttons above it.
- Bug: Accept offer
1.I logged in as FancyPie, made a bit on one of TeddyBear's listings
2.logged out and logged into TeddyBear
3.Go to notifications -> Received Notifications -> Clicked Accept offer
4.Got a popup that says ""Something went wrong. Please try again later""
5.Close out of the popup and go back to notifications.
6.I click on VIEW to see listing offer history. I see that its been purchased and that there is an Offer Declined and Offer Accepted
- Bug: When i go to edit a payment option, the Name textbox is cleared and i have to go in and add the name every time.
- can we set it so the keyboard popup shows capitalized letters first when a user has to input something in a text field? Right now its set to lowercase and i have to switch to uppercase everytime.
- Add firebase to project
- Offer flow DeclineReason that we should display for those users who had bids on a listing that was once available but no longer is.
- Bug: The seller return address is never set. I see thats it's never passed through the endpoint cause there's no field for it. I am going to add a field called AddressId for the seller offer endpoint /listings/{id}/actions/seller-offer/{offerId}.
- Timeline design done, waiting on API

## [0.1.18] - 2018-10-23
- Added Fabric crash analytics
- BUG: Forgot password pin boxes should be bigger and align to left
- BUG: Counteroffer, Accept Offer, Decline Offer, Make Offer work as expected now
- BUG: My wallet, Add Payment button should be fixed to bottom.
- BUG: If i am viewing the listing details of one of my own listings, the bar at the bottom should have a button that says VIEW OFFERS (instead of VIEW OFFER)
- When Seller view own listing and click VIEW OFFERS show page
- BUG: When editing a listing that is still active (has not been sold), the Listing Status dropdown does not reflect whats in the database,
Status should call api not hard code values
- BUG:  notifications/Follow/Like page - each notification should span the entire screen
- BUG: Settings notification page - the YES side should be pink while selecting NO should be gray. Right now it shows the opposite.
- BUG: The background color for the bottom portion of the "Profile" screen should be a lighter gray color
- BUG: Listing offer button sizes don't match zeplin. Recheck next buttons, change button
- BUG: When making an offer the NEXT button should be at the bottom of the screen as shown in Zeplin. Right now it moved up
- BUG: Whenever there is a feed, browse, i see a thin gap both at the top and bottom as i scroll down and up. There shouldn't be any gap.
- On My Addresses Screen:, remove delete button on "Preferred Shipping Address" and remove delete button on "Preferred Return Address"
- When user is editing "Preferred Shipping Address", user cannot see or set "Default shipping address"
- When user is editing "Preferred Return Address", user cannot see or set "Default return address"
- Under My Notifications > Offers, there will be two sub-sections: "My Offers" and "Received Offers". See screens:
"My Notifications - My Offers" and "My Notifications - Received Offers"
- Buy now flow
- If user doesn't have default credit card or default address, show modal when user goes to next screen after my offer or buy now
- BUG: cancel offer, should refresh the screen
- When im logged in under Profile > My Byn, there is no empty state. Should look like Zeplin Screen: My Byn Empty State (with a lighter background color and the background image)
- Under Profile > My Likes. for Listings it should look like My Likes - Listings - Empty State - Logged in User and for Posts it should look like My Likes - Posts - Empty State - Logged in User. Currently it is showing messages that another person would see when viewing another person's profile.


## [0.1.17] - 2018-10-09
- Updated gradle version
- Forget password flow
- Reset password screen
- update listing detail to go to make an offer screen
- buyer make an offer flow
- seller view an offer flow (need to do couter offer api manually for now)
- buyer can cancel offer flow
- Notifications now shows someone made offer to your listing but not linked to listing yet
- Added new library for entering pin

## [0.1.16] - 2018-09-25
- BUG: fix setting unread notification to read notification
- Updated Screen shown after calling api/listings/check

## [0.1.15] - 2018-09-18
- Edit Payment in My Wallet
- Delete Payment Flow linked with offers
- Delete Payment Flow without offers
- If User doesn't have payment option or shipping set it will ask them before listing
- Notification Settings Screen
- Alerts Page
- Update Unread Alerts
- Bug: Fix the keyboard popup when hitting the bottom back button after finishing typing in text in adding a new payment (under the section for adding a new address for the card)

## [0.1.14] - 2018-09-08
- Wallet page design
- Integrate Stripe with app
- BUG: When editing a listing, the Condition (Pre-loved or Brand new) does not get updated.
- Non-alphanumeric characters mess up add/updating listing.
- Non-alphanumeric characters mess up add/updating posts.
- Non-alphanumeric characters mess up add/updating comments.
- Bug: navigating a bunch of pages and pages overlap
- remove delete status from editing listing
- Add Payment option
- Select Existing address on payment
- Set default card on wallet (Still need to fix)
- Delete Card Modal

## [0.1.13] - 2018-08-29
- Allow All Subcategory in listing filter
- Bug: if invalid address, show correct error message
- Bug: no arrows on showing on listing filter
- Feed filter icon open filters, slide in right to left
- Bug: Are you sure modal when user exit add listing by clicking on footer icons
- on add listing -> size screen, push first tab from left to right by 15px so arrow doesnt overlap
- Bug: My Likes/My Byn/Feed/Browse > Select a Condition filter doesnt work -> I think they are flipped check again
- Fix navigation to not stack any more
- Bug: My likes -> unlike a listing -> click back -> scroll down -> pagination -> app crashes
- Bug: When i view a listing detail, the listing detail screen is scrolled down a little when i enter the page. It should be at the complete top. Nexus 6p phone size (doesn't happen on emulator, can't replicate)
- Change filter for listing likes
- Edit Listings
- research stripe for android
- Update the Category images under Create Listing > Select Category Type

## [0.1.12] - 2018-08-18
- BUG: Listing Filter saved across all listing views. Need to reset filter on MY byn, other profile, feeds listing
- Use multi image upload on posts and listings
- after leave listing listview page, filter return default
- listing detail buy now bar increase height, add 6dp on both sides
- Shipping rate page, priority box not exact same size. look on zeplin
- Border on selected shipping rate, increase border size to match zeplin
- Bug: My likes -> listing -> filter not working properly
- My likes change title to My Byn -> Listings, My Posts -> Posts
- More padding between the title categories on Sizes page 1.5X the padding
- On sizing page, make the arrows touch the borders top and bottom, no whitespace above
- on listing filter search part, reduce the white space tighter to the tags
- Listing Filter Size -> Increase arrow size
- Listing Filter Size -> if user click on the size category, go back
- Listing Filter Category -> Increase arrow size
- Listing Filter Category -> if suer click on the size Category, go back
- When i create a listing but never finish it and switch screens, there should be a popup saying something like "All your listing changes will be lost. Are you sure you want to go back?"
- If you click "All your listing changes will be lost. Are you sure you want to go back?" -> Yes -> destroy all information on screen go back
- Listing Address
- Link Address Box on my profile to Listing address
- Edit Address
- Delete Address Modal
- Edit Post
- My Posts -> Disable Post, toggle api, modal
- My Posts -> Delete Post, modal
- HACK: Temporary fix around my post to test edit post

## [0.1.11] - 2018-08-07
- Add listing Filter
- Bug: always showing priority shipping
- Swipe left and right for sizing tab titles on add listing page
- Bug: Selecting size -> Choose One Size -> N/a -> save -> edit listing size -> N/a not highlighted
- Category, make all boxes square and add more padding
- brand search box
- Bug: brand pagination, use 10000, to show whole list
- Show all boxes, when user type weight, highlight specific box
- Bug: My likes -> My Posts -> paginate
- Bug: remove profile add icon on My Likes
- My Likes -> My Posts
- Other profile -> listing should be newest to oldest
- Other profile -> posts should be newest to oldest
- increase image resolution
- MY Byn -> filter icon

## [0.1.10] - 2018-07-16
- Added Listing UI and logic
- Add My Likes page
- All like button on listing detail connected to My likes and listing likes
- My like box on my profile connect to my likes page
- Adding like to listing detail updates My Byn listing detail like ui
- Add Coment functionality allow reply to have username in comments

## [0.1.9] - 2018-07-16
- BUG: fixed tags to show in reverse order, add tags is first tag, show 10 tags
- Added Listing Details
- Implemented new Register and login flows
- Added My Byn listing view
- BUG: fixed navigation issue between pages
- Added dialog for when not logged in user clicks any button

## [0.1.8] - 2018-07-08
- Added Camera support for uploading images
- Removed all code to fix image url
- Removed code that always update background image in profile
- Add Post
- Modified HomeActivity to support new UI for POST and Listing UI
- Refactor CreateAccountFormActivity code to use databinding
- Changed background image for middle and bottom section for other profile
- Changed background for followers/following
- Updated icons on footer
- Added Create icons when you tap create on footer
- Added MY Post List View. Only Design
- Added Camera Support for Background
- Added Camera Support for Profile Image
- Added Edit Profile city Field
- Show City Field in Profile
- Edit Account point to edit profile
- Changed Word Sure to Choose when selecting media
- Add Post Image
- Can zoom and crop images now
- Post link out to URL
- Listing goes to Listing Detail Activity

## [0.1.7] - 2018-06-30
- Added image cropping screen so image is square when uploading
- Added Browse Activity
- Made horizontal carousel match the logic of which list is displayed below. Get best matches for all, get followering for Posts and Listing
- Refactor form code to use databind
- Implemented follower screens
- Implemented following screens
- Changed all dialogs to new design
- Upload profile image
- Upload profile background
- Edit profile information
- Refactor Feed Code to accommodate for Browse Code
- put in new dialogs for register, signin, oauth signin, onboarding brands, logout

## [0.1.6] - 2018-06-21
- Temporary fix for refresh token so it redirects to login page if it fails right now

## [0.1.5] - 2018-06-21
### Changed
- fixed Liking Post on Feeds page
- Posts under other profile title are cut off
- Social media buttons not showing up on other profile

## [0.1.4] - 2018-06-20
### Changed
- Logged in user profile from footer profile tab
- Other user profile design and implementation
- On username click or user photo click navigates to user profile
- Other user posts
- Other user byns
- Like click api call in feeds, my byns and my posts
- Handling of social links clicks to navigate outside app
- Handle my website click navigate to webview inside the app
- Show if logged in user is following other user or not
- Follow/ Unfollow other user and also manage count of followers.

## [0.1.3] - 2018-06-13
### Changed
- Fix all colors on onboarding pages
- Switch out logo
- Switched all progress bar to gradient
- Change design of Modals
- Change tags to use gradient

### Added
- Feed Design Page
- Footer Bar
- Post Card View
- Listing Card View
- Empty State on Feeds
- All card images are clickable
- All video cards open in youtube
- All Footer clickable
- Users to follow carousel scrollable and clickable

## [0.1.3] - 2018-05-29
### Changed
- Use Custom Dialog instead of normal dialog
- "keytool -exportcert -alias bynmix -keystore dev-key3.jks | openssl sha1 -binary | openssl base64"

## [0.1.2] - 2018-05-18
### Changed
- Fix onboarding pages to use new api changes
- Updated facebook and google icons for buttons
- register api is hooked into register page
- main page uses username instead of email to login
- refresh token logic is setup to ask for new access token
- login with google/facebook works with backend api

## [0.1.1] - 2018-05-18
### Changed
- Login API saves access token
- Onboarding pages now hit API to search and add
- Pojo models for responses and request

## [0.1.0] - 2018-05-04
### Added
- Icon for App
- SourceSans Fonts
- icons for forms
- 2 background video
- BaseActivity
- OauthActivity
- Constants
- CustomTextDialog
- ApiService wrapping retrofit
- ApiEndpoint interface to model api endpoints
- Register Pojo and responses
- Google login (Remember to add prod keys later)
- Facebook login (Remember to add prod keys later)
- MainActivity (Signin)
- CreateAccountActivity (3 button signin page)
- CreateAccountFormActivity (Register no external)
- WelcomeActivity (Onboarding page 1)
- WelcomeBrandActivity (Onboarding page 2 with brand)
- WelcomeBlogActivity (TODO)
- gradle handle environment variables
- Added Test Cases for UI
