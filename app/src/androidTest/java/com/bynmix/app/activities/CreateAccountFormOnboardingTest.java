package com.bynmix.app.activities;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.bynmix.app.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class CreateAccountFormOnboardingTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void createAccountFormOnboardingTest() {
        ViewInteraction textView = onView(
                allOf(withId(R.id.main_member), withText("Not a member? Create Account"), isDisplayed()));
        textView.perform(click());

        ViewInteraction button = onView(
                allOf(withId(R.id.create_account_email_button), withText("EMAIL"), isDisplayed()));
        button.perform(click());

        ViewInteraction editText = onView(
                allOf(withId(R.id.account_form_text_email),
                        withParent(withId(R.id.account_form_linear_layout_1)),
                        isDisplayed()));
        editText.perform(replaceText("g@gmail.com"), closeSoftKeyboard());

        ViewInteraction editText2 = onView(
                allOf(withId(R.id.account_form_text_username),
                        withParent(withId(R.id.account_form_linear_layout_2)),
                        isDisplayed()));
        editText2.perform(replaceText("abcd"), closeSoftKeyboard());

        ViewInteraction editText3 = onView(
                allOf(withId(R.id.account_form_text_first_name),
                        withParent(withId(R.id.account_form_linear_layout_3)),
                        isDisplayed()));
        editText3.perform(replaceText("asdf"), closeSoftKeyboard());

        ViewInteraction editText4 = onView(
                allOf(withId(R.id.account_form_text_last_name),
                        withParent(withId(R.id.account_form_linear_layout_4)),
                        isDisplayed()));
        editText4.perform(replaceText("rtyu"), closeSoftKeyboard());

        ViewInteraction editText5 = onView(
                allOf(withId(R.id.account_form_text_password),
                        withParent(withId(R.id.account_form_linear_layout_5)),
                        isDisplayed()));
        editText5.perform(replaceText("12345678"), closeSoftKeyboard());

        ViewInteraction button2 = onView(
                allOf(withId(R.id.create_account_email_button), withText("SUBMIT"), isDisplayed()));
        button2.perform(click());

        ViewInteraction imageView = onView(
                allOf(withId(R.id.welcome_tag_image),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                0),
                        isDisplayed()));
        imageView.check(matches(isDisplayed()));

        ViewInteraction textView2 = onView(
                allOf(withId(R.id.welcome_tag_text), withText("List New or Pre–Loved items for sale."),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        textView2.check(matches(withText("List New or Pre–Loved items for sale.")));

        ViewInteraction button3 = onView(
                allOf(withId(R.id.welcome_got_it_button),
                        childAtPosition(
                                allOf(withId(R.id.welcome_linear_layout_1),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.RelativeLayout.class),
                                                2)),
                                0),
                        isDisplayed()));
        button3.check(matches(isDisplayed()));

        ViewInteraction button4 = onView(
                allOf(withId(R.id.welcome_got_it_button), withText("COOL! GOT IT!"),
                        withParent(withId(R.id.welcome_linear_layout_1)),
                        isDisplayed()));
        button4.perform(click());

        ViewInteraction textView3 = onView(
                allOf(withId(R.id.dialog_body_text), withText("This helps us populate your feed with the brands you know and love!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        textView3.check(matches(withText("This helps us populate your feed with the brands you know and love!")));

        ViewInteraction clickBackground = onView(
                allOf(withId(R.id.dialog_close), withText("X"),
                        withParent(withId(R.id.dialog_text_layout)),
                        isDisplayed()));
        clickBackground.perform(click());

        ViewInteraction textView4 = onView(
                allOf(withId(R.id.welcome_brand_text), withText("Name your 3 top favorite brands. ( Why? )"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                0),
                        isDisplayed()));
        textView4.check(matches(withText("Name your 3 top favorite brands. ( Why? )")));

        ViewInteraction button5 = onView(
                allOf(withId(R.id.welcome_confirm_button),
                        childAtPosition(
                                allOf(withId(R.id.welcome_brand_linear_layout_1),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.RelativeLayout.class),
                                                2)),
                                0),
                        isDisplayed()));
        button5.check(matches(isDisplayed()));

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
