package com.bynmix.app.activities;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.bynmix.app.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void mainActivityTest() {
        ViewInteraction imageView = onView(
                allOf(withId(R.id.main_logo),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        imageView.check(matches(isDisplayed()));

        ViewInteraction textView = onView(
                allOf(withId(R.id.main_text_1), withText("Fashion made fun with bloggers in mind!"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                2),
                        isDisplayed()));
        textView.check(matches(withText("Fashion made fun with bloggers in mind!")));

        ViewInteraction textView2 = onView(
                allOf(withId(R.id.main_text_2), withText("Shop for fashion you love."),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                3),
                        isDisplayed()));
        textView2.check(matches(withText("Shop for fashion you love.")));

        ViewInteraction textView3 = onView(
                allOf(withId(R.id.main_text_3), withText("Discover & follow fashion bloggers and designers."),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                4),
                        isDisplayed()));
        textView3.check(matches(withText("Discover & follow fashion bloggers and designers.")));

        ViewInteraction button = onView(
                allOf(withId(R.id.main_signin_button),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                8),
                        isDisplayed()));
        button.check(matches(isDisplayed()));

        ViewInteraction textView4 = onView(
                allOf(withId(R.id.main_or), withText("Or login with"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                9),
                        isDisplayed()));
        textView4.check(matches(withText("Or login with")));

        ViewInteraction button2 = onView(
                allOf(withId(R.id.main_facebook),
                        childAtPosition(
                                allOf(withId(R.id.main_linear_layout_3),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.RelativeLayout.class),
                                                10)),
                                0),
                        isDisplayed()));
        button2.check(matches(isDisplayed()));

        ViewInteraction button3 = onView(
                allOf(withId(R.id.main_google),
                        childAtPosition(
                                allOf(withId(R.id.main_linear_layout_3),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.RelativeLayout.class),
                                                10)),
                                1),
                        isDisplayed()));
        button3.check(matches(isDisplayed()));

        ViewInteraction textView5 = onView(
                allOf(withId(R.id.main_member), withText("Not a member? Create Account"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                11),
                        isDisplayed()));
        textView5.check(matches(withText("Not a member? Create Account")));

        ViewInteraction textView6 = onView(
                allOf(withId(R.id.main_forgot), withText("Forgot?"),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                7),
                        isDisplayed()));
        textView6.check(matches(withText("Forgot?")));

        ViewInteraction editText3 = onView(
                allOf(withId(R.id.main_input_text_username),
                        withParent(withId(R.id.main_linear_layout_1)),
                        isDisplayed()));
        editText3.perform(replaceText("w@gmail.com"), closeSoftKeyboard());

        ViewInteraction editText4 = onView(
                allOf(withId(R.id.main_input_text_password),
                        withParent(withId(R.id.main_linear_layout_2)),
                        isDisplayed()));
        editText4.perform(replaceText("12345678"), closeSoftKeyboard());

        ViewInteraction button4 = onView(
                allOf(withId(R.id.main_signin_button), withText("SIGN IN"), isDisplayed()));
        button4.perform(click());

        ViewInteraction imageView2 = onView(
                allOf(withId(R.id.welcome_tag_image),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                0),
                        isDisplayed()));
        imageView2.check(matches(isDisplayed()));

        ViewInteraction textView7 = onView(
                allOf(withId(R.id.welcome_tag_text), withText("List New or Pre–Loved items for sale."),
                        childAtPosition(
                                childAtPosition(
                                        withId(android.R.id.content),
                                        0),
                                1),
                        isDisplayed()));
        textView7.check(matches(withText("List New or Pre–Loved items for sale.")));

        ViewInteraction button5 = onView(
                allOf(withId(R.id.welcome_got_it_button),
                        childAtPosition(
                                allOf(withId(R.id.welcome_linear_layout_1),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.widget.RelativeLayout.class),
                                                2)),
                                0),
                        isDisplayed()));
        button5.check(matches(isDisplayed()));

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
