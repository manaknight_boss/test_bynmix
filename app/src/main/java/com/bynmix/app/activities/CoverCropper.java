package com.bynmix.app.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.bynmix.app.R;
import com.bynmix.app.utils.BaseActivity;
import com.bynmix.app.utils.Constants;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;


public class CoverCropper extends BaseActivity {

    ImageView mCrossImageView;
    TextView coverText;
    private CropImageView cropImageView;
    private Bitmap croppedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cover_cropper);
        cropImageView = (CropImageView) findViewById(R.id.ImageView);
        coverText = (TextView) findViewById(R.id.cover_text);
        mCrossImageView = (ImageView) findViewById(R.id.close_activity_imageview);
        mCrossImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ImageView cover = (ImageView) findViewById(R.id.done);
        String path = getIntent().getStringExtra(getString(R.string.image));
        Bitmap bitmap = null;
        File f = new File(path);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        try {
            bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        cropImageView.setImageBitmap(bitmap);
        cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                croppedImage = cropImageView.getCroppedImage();
                sendCroppedResult(croppedImage);
            }
        });
    }

    void sendCroppedResult(Bitmap bitmap) {
        Intent intent = new Intent();
        String filePath = writeBitmapToLocalPath(bitmap, Constants.FULL_IMAGE, Constants.COMPRESS_100);
        intent.putExtra(Constants.COVER_FILE_EXTRA, filePath);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}
