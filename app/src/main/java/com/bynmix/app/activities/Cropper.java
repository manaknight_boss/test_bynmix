package com.bynmix.app.activities;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.utils.BaseActivity;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Cropper extends BaseActivity {

    private CropImageView cropImageView;
    private Bitmap croppedImage;
    ImageView mCrossImageView;
    TextView thumbnailText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cropper);
        thumbnailText = (TextView) findViewById(R.id.thumbnail_text);
        cropImageView = (CropImageView) findViewById(R.id.ImageView);
        mCrossImageView = (ImageView) findViewById(R.id.close_activity_imageview);

        mCrossImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ImageView cover = (ImageView) findViewById(R.id.done);
        String path = getIntent().getStringExtra(getString(R.string.image));
        Bitmap bitmap = null;
        File f = new File(path);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        try {
            bitmap = BitmapFactory.decodeStream(new FileInputStream(f), null, options);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        cropImageView.setImageBitmap(bitmap);

        cover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                croppedImage = cropImageView.getCroppedImage();
                sendCroppedResult(croppedImage);
            }
        });

    }

    void sendCroppedResult(Bitmap bitmap) {
        HomeActivity.croppedBitmap = bitmap;
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }
}
