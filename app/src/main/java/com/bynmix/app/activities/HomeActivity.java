package com.bynmix.app.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.adapter.PromotionAdapter;
import com.bynmix.app.databinding.ActivityHomeBinding;
import com.bynmix.app.fragments.AddAddressFragment;
import com.bynmix.app.fragments.AddBankDetailFragment;
import com.bynmix.app.fragments.AddDotsFragment;
import com.bynmix.app.fragments.AddListingDetailFragment;
import com.bynmix.app.fragments.AddNewAddressFragment;
import com.bynmix.app.fragments.AddPaymentFragment;
import com.bynmix.app.fragments.AddPostDetailFragment;
import com.bynmix.app.fragments.AddPostFragment;
import com.bynmix.app.fragments.AlertFragment;
import com.bynmix.app.fragments.BalanceHistoryFragment;
import com.bynmix.app.fragments.BestUserFragment;
import com.bynmix.app.fragments.BrandFragment;
import com.bynmix.app.fragments.BrowseFragment;
import com.bynmix.app.fragments.CancelSaleFragment;
import com.bynmix.app.fragments.CategoryFragment;
import com.bynmix.app.fragments.ColorFragment;
import com.bynmix.app.fragments.CreateListingFragment;
import com.bynmix.app.fragments.DisputeDetailFragment;
import com.bynmix.app.fragments.DotsDetailFragment;
import com.bynmix.app.fragments.EditListingFragment;
import com.bynmix.app.fragments.EditMyListingFragment;
import com.bynmix.app.fragments.EditMyPostFragment;
import com.bynmix.app.fragments.EditPostDotsFragment;
import com.bynmix.app.fragments.EditPostFragment;
import com.bynmix.app.fragments.EditProfileFragment;
import com.bynmix.app.fragments.FeedsFragment;
import com.bynmix.app.fragments.FilterFragment;
import com.bynmix.app.fragments.FilterUserFragment;
import com.bynmix.app.fragments.FollowFollowingFragment;
import com.bynmix.app.fragments.InAppBrowserFragment;
import com.bynmix.app.fragments.LikesFragment;
import com.bynmix.app.fragments.ListingDetailFragment;
import com.bynmix.app.fragments.ListingOfferFragment;
import com.bynmix.app.fragments.MakeAnOfferFragment;
import com.bynmix.app.fragments.MyAddressFragment;
import com.bynmix.app.fragments.MyBynProfileFragment;
import com.bynmix.app.fragments.MyLikesFragment;
import com.bynmix.app.fragments.MyPurchasesFragment;
import com.bynmix.app.fragments.MyWalletFragment;
import com.bynmix.app.fragments.NewSellerFragment;
import com.bynmix.app.fragments.NotificationSettingsFragment;
import com.bynmix.app.fragments.OfferCompletionFragment;
import com.bynmix.app.fragments.PostDetailFragment;
import com.bynmix.app.fragments.ProfileFragment;
import com.bynmix.app.fragments.PurchaseDetailFragment;
import com.bynmix.app.fragments.PurchasesFragment;
import com.bynmix.app.fragments.ReportAnIssueFragment;
import com.bynmix.app.fragments.ReportAnIssueListingFragment;
import com.bynmix.app.fragments.ReviewFragment;
import com.bynmix.app.fragments.SearchUserFragment;
import com.bynmix.app.fragments.SelectDifferentAddressFragment;
import com.bynmix.app.fragments.SelectDifferentPaymentFragment;
import com.bynmix.app.fragments.SelectExistingAddressFragment;
import com.bynmix.app.fragments.SelectExistingPaymentFragment;
import com.bynmix.app.fragments.SelectListingForDotsFragment;
import com.bynmix.app.fragments.SelectNewPaymentFragment;
import com.bynmix.app.fragments.SellerAccountSetupFragment;
import com.bynmix.app.fragments.SellerPolicyFragment;
import com.bynmix.app.fragments.ShippingFragment;
import com.bynmix.app.fragments.SizeMainFragment;
import com.bynmix.app.fragments.WithdrawFragment;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.AddTagsListener;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.interfaces.FeedItemClickListener;
import com.bynmix.app.interfaces.FragmentNavigation;
import com.bynmix.app.interfaces.LogoutInterface;
import com.bynmix.app.interfaces.PermissionRequestCallBacks;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.SelectExistingAddressListener;
import com.bynmix.app.interfaces.SelectNewPaymentListener;
import com.bynmix.app.models.Address;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.BankAccount;
import com.bynmix.app.models.Brand;
import com.bynmix.app.models.BrandResponse;
import com.bynmix.app.models.CardDetail;
import com.bynmix.app.models.Category;
import com.bynmix.app.models.CategoryType;
import com.bynmix.app.models.Colors;
import com.bynmix.app.models.Comments;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.models.Feeds;
import com.bynmix.app.models.FilterData;
import com.bynmix.app.models.HeightModel;
import com.bynmix.app.models.ImageData;
import com.bynmix.app.models.ListingCheck;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.models.ListingDisplayImage;
import com.bynmix.app.models.MyPurchasesStats;
import com.bynmix.app.models.MySalesStats;
import com.bynmix.app.models.PostImage;
import com.bynmix.app.models.Promotions;
import com.bynmix.app.models.PurchasesDetail;
import com.bynmix.app.models.SalesDetail;
import com.bynmix.app.models.SelectedFilter;
import com.bynmix.app.models.Size;
import com.bynmix.app.models.SizeType;
import com.bynmix.app.models.Stock;
import com.bynmix.app.models.Tracking;
import com.bynmix.app.models.UserFilterData;
import com.bynmix.app.models.UserResponse;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.sharedpreference.SharedPreferenceUtility;
import com.bynmix.app.utils.BaseActivity;
import com.bynmix.app.utils.CheckListingDialog;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.CreateAccountDialog;
import com.bynmix.app.utils.CreateAccountFormDialog;
import com.bynmix.app.utils.DialogAlert;
import com.bynmix.app.utils.LogoutDialog;
import com.bynmix.app.utils.MyGlideEngine;
import com.bynmix.app.utils.SavingListingDialog;
import com.bynmix.app.utils.SavingPostDialog;
import com.bynmix.app.utils.TrackingDialog;
import com.bynmix.app.utils.Utils;
import com.google.gson.Gson;
import com.ncapdevi.fragnav.FragNavController;
import com.ncapdevi.fragnav.FragNavTransactionOptions;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity implements FeedItemClickListener, ProfileListener, FragNavController.TransactionListener, FragmentNavigation, FragNavController.RootFragmentListener, LogoutInterface, CreatePostListener {
    public static Bitmap croppedBitmap;
    private final int INDEX_FEEDS = FragNavController.TAB1;
    private final int INDEX_BROWSE = FragNavController.TAB2;
    private final int INDEX_CREATE = FragNavController.TAB3;
    private final int INDEX_NOTIFICATION = FragNavController.TAB4;
    private final int INDEX_PROFILE = FragNavController.TAB5;
    private ActivityHomeBinding mBinding;
    private RefreshFeedsInFeedFragment refreshBroadcastReceiver = new RefreshFeedsInFeedFragment();
    private SharedPreferenceUtility mSharedPreferenceUtility;
    public List<FeedResponse> mFeedsList = new ArrayList<>();
    public List<UserResponse> mBestUser = new ArrayList<>();
    public List<UserResponse> mFollowingList = new ArrayList<>();
    private List<SelectedFilter> mSelectedFilterList = new ArrayList<>();
    private List<SelectedFilter> mSelectedUserFilterList = new ArrayList<>();
    private List<Integer> deletedListingImagesId = new ArrayList<>();
    private List<Integer> allCategoryList = new ArrayList<>();
    private ListingDetail listingDetail = new ListingDetail();
    public FilterData filterData = new FilterData();
    private UserFilterData userFilterData = new UserFilterData();
    private FragNavController mNavController;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener;
    private boolean isCreateOverlayOpened = false;
    public int filterFragmentType;
    private int imagesCropped = 0;
    private int imagesSelectedByUserCount;
    private List<String> mListingCroppedList = new ArrayList<>();
    private List<String> mListingImagesList = new ArrayList<>();
    private MySalesStats mySalesStats = new MySalesStats();
    private MyPurchasesStats myPurchasesStats = new MyPurchasesStats();
    private String mPostImage;
    private boolean isPicSelected = false;
    private HashMap<Integer,Integer> mFragmentClearCurrentStack =new HashMap<>();


    private boolean isImageUsed = false;
    private boolean isEditPost = false;
    private List<AnnotatedPoint> postDots, deletedDots;
    private HeightModel mSelectedHeightModel = new HeightModel();

    private void updateBottomBarUI(AppCompatImageView selectedView, AppCompatImageView unSelectedView, boolean isCreateFragment) {
        updateBottomBarUi(selectedView, unSelectedView);
        if (isCreateFragment) {
            isCreateOverlayOpened = true;
            mBinding.createFragmentLayout.setVisibility(View.VISIBLE);
            mBinding.createPost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isCreateOverlayOpened = false;
                    openAddPostFragment();
                    mBinding.createFragmentLayout.setVisibility(View.GONE);
                }
            });
            mBinding.createListing.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isCreateOverlayOpened = false;
                    checkListing();
                    mBinding.createFragmentLayout.setVisibility(View.GONE);
                }
            });
            mBinding.createFragmentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    setBottomBarForCreateFragment();
                }
            });
        } else {
            mBinding.createFragmentLayout.setVisibility(View.GONE);
        }
    }

    private void checkListing() {
        mProgressBarDialog.showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.checkListing("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<ListingCheck>>() {
            @Override
            public void onResponse(Call<ApiResponse<ListingCheck>> call, Response<ApiResponse<ListingCheck>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    ListingCheck status = response.body().getData();
                    if (status.isNewSeller()) {
                        openNewSellerFragment();
                    } else {
                        openCreateListingFragment();
                    }
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            checkListing();
                        }

                        @Override
                        public void errorAccessToken() {
                            mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(HomeActivity.this, response.errorBody());
                    mAlertDialog.showDialog(HomeActivity.this, message);
                }
                mProgressBarDialog.hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<ListingCheck>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    private void openNewSellerFragment() {
        hideBottomBar();
        NewSellerFragment newSellerFragment = NewSellerFragment.newInstance();
        pushFragment(newSellerFragment);
    }

    public void hideBottomBar() {
        mBinding.bottomBar.setVisibility(View.GONE);
        mBinding.bottomBarLayout.setVisibility(View.GONE);
    }

    public void showBottomBar() {
        mBinding.bottomBar.setVisibility(View.VISIBLE);
        mBinding.bottomBarLayout.setVisibility(View.VISIBLE);
    }

    public boolean isImageUsed() {
        return isImageUsed;
    }

    public void setImageUsed(boolean imageUsed) {
        isImageUsed = imageUsed;
    }

    @Override
    public void openCheckListingDialog(ListingCheck status) {
        CheckListingDialog checkListingDialog = new CheckListingDialog();
        checkListingDialog.showDialog(HomeActivity.this, this, status);
    }

    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        checkCaseNotification(intent);
    }

    private void checkCaseNotification(Intent intent) {
        String notificationSelection = intent.getStringExtra(Constants.NOTIFICATION_FRAGMENT_SELECTION);
        if (notificationSelection != null) {
            openNotificationFragment();
        }
    }

    @Override
    public void openListingOfferFragment(int listingDetailId) {
        ListingOfferFragment listingOfferFragment = ListingOfferFragment.newInstance();
        listingOfferFragment.setId(listingDetailId);
        pushFragment(listingOfferFragment);
    }

    private void updateBottomBarUi(AppCompatImageView selectedView, AppCompatImageView unSelectedView) {
        mBinding.navigationFeedSelected.setVisibility(View.INVISIBLE);
        mBinding.navigationBrowseSelected.setVisibility(View.INVISIBLE);
        mBinding.navigationCreateSelected.setVisibility(View.INVISIBLE);
        mBinding.navigationAlertSelected.setVisibility(View.INVISIBLE);
        mBinding.navigationProfileSelected.setVisibility(View.INVISIBLE);
        mBinding.navigationFeed.setVisibility(View.VISIBLE);
        mBinding.navigationBrowse.setVisibility(View.VISIBLE);
        mBinding.navigationCreate.setVisibility(View.VISIBLE);
        mBinding.navigationAlerts.setVisibility(View.VISIBLE);
        mBinding.navigationProfile.setVisibility(View.VISIBLE);
        selectedView.setVisibility(View.VISIBLE);
        unSelectedView.setVisibility(View.INVISIBLE);
    }

    public void openCreateListingFragment() {
        CreateListingFragment createListingFragment = CreateListingFragment.newInstance();
        listingDetail = new ListingDetail();
        pushFragment(createListingFragment);
    }

    private void setBottomBarForCreateFragment() {
        mBinding.createFragmentLayout.setVisibility(View.GONE);
        int i = mNavController.getCurrentStackIndex();
        if (i == 0) {
            selectTab(R.id.navigation_feeds_tab);
        } else if (i == 1) {
            selectTab(R.id.navigation_browse_tab);
        } else if (i == 3) {
            selectTab(R.id.navigation_alert_tab);
        } else if (i == 4) {
            selectTab(R.id.navigation_profile_tab);
        }
    }

    private void openAddPostFragment() {
        AddPostFragment addPostFragment = AddPostFragment.newInstance();
        pushFragment(addPostFragment);
    }

    @Override
    protected void onStop() {
        unregisterReceiver(refreshBroadcastReceiver);
        super.onStop();
    }


    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter(Constants.ACTION_BROADCAST_RECEIVER_REFRESH_FEEDS);
        registerReceiver(refreshBroadcastReceiver, intentFilter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        init(savedInstanceState);
        mBinding.bottomBar.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        disableShiftMode(mBinding.bottomBar);
        mSharedPreferenceUtility = new SharedPreferenceUtility(this);
        setProgressBarColor();
        if (conditionsForApiCall()) {
            getNotificationCount();
            checkCaseNotification(getIntent());
            checkPromotions();
        }
        mBinding.createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateBottomBarUI(mBinding.navigationCreateSelected, mBinding.navigationCreate, true);
            }
        });
    }

    private void checkPromotions() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getPromotions("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<List<Promotions>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Promotions>>> call, Response<ApiResponse<List<Promotions>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    List<Promotions> mPromotionList = response.body().getData();
                    if (mPromotionList != null && mPromotionList.size() > 0) {
                        mBinding.promotionRecyclerView.setVisibility(View.VISIBLE);
                        filterPromotionList(mPromotionList);
                    }
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            checkPromotions();
                        }

                        @Override
                        public void errorAccessToken() {
                            mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                        }
                    });
                } else {
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
                mProgressBarDialog.hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<List<Promotions>>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    private void filterPromotionList(List<Promotions> mPromotionList) {
        int lastPromotionId = mSharedPreferenceUtility.getPromotionId();
        int i = 0;
        while (true) {
            if (mPromotionList.size() > 0 && mPromotionList.get(i).getPromotionId() <= lastPromotionId) {
                mPromotionList.remove(i);
            } else {
                break;
            }
        }
        if (mPromotionList.size() > 0) {
            mSharedPreferenceUtility.setPromotionId(mPromotionList.get(mPromotionList.size() - 1).getPromotionId());
            setPromotionsAdapter(mPromotionList);
        }
    }

    private void setPromotionsAdapter(List<Promotions> promotions) {
        PromotionAdapter promotionAdapter = new PromotionAdapter(HomeActivity.this, promotions);
        mBinding.promotionRecyclerView.setLayoutManager(new LinearLayoutManager(HomeActivity.this));
        mBinding.promotionRecyclerView.setAdapter(promotionAdapter);
    }

    @Override
    public void getNotificationCount() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getNotificationCount("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<Integer>>() {
            @Override
            public void onResponse(Call<ApiResponse<Integer>> call, Response<ApiResponse<Integer>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    int count = response.body().getData();
                    if (count > 0) {
                        mBinding.notificationCountLayout.setVisibility(View.VISIBLE);
                        if (count > 99) {
                            mBinding.notificationCount.setText("99+");
                        } else {
                            mBinding.notificationCount.setText("" + count);
                        }
                    } else {
                        mBinding.notificationCountLayout.setVisibility(View.GONE);
                    }

                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getNotificationCount();
                        }

                        @Override
                        public void errorAccessToken() {
                            mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                        }
                    });
                } else {
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
                mProgressBarDialog.hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Integer>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    private void init(Bundle savedInstanceState) {
        mNavController = FragNavController.newBuilder(null, getSupportFragmentManager(), R.id.fragment_container)
                .transactionListener(this)
                .rootFragmentListener(this, 5)
                .build();
        mBinding.bottomBar.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener
                = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                if (fragment instanceof AddListingDetailFragment) {
                    AddListingDetailFragment addListingDetailFragment = (AddListingDetailFragment) fragment;
                    addListingDetailFragment.checkOnBackPress(id);
                } else if(fragment instanceof BrandFragment) {
                    BrandFragment brandFragment = (BrandFragment) fragment;
                    brandFragment.askForDataLoss(id);
                } else if(fragment instanceof SizeMainFragment) {
                    SizeMainFragment sizeMainFragment = (SizeMainFragment) fragment;
                    sizeMainFragment.askForDataLoss(id);
                } else if(fragment instanceof ColorFragment) {
                    ColorFragment colorFragment = (ColorFragment) fragment;
                    colorFragment.askForDataLoss(id);
                } else if(fragment instanceof CategoryFragment) {
                    CategoryFragment categoryFragment = (CategoryFragment) fragment;
                    categoryFragment.askForDataLoss(id);
                } else if(fragment instanceof ShippingFragment) {
                    ShippingFragment shippingFragment = (ShippingFragment) fragment;
                    shippingFragment.askForDataLoss(id);
                } else {
                    moveToTab(id);
                }
                return true;
            }
        });

        mBinding.bottomBar.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {

            @Override
            public void onNavigationItemReselected(@NonNull MenuItem item) {
                final int id = item.getItemId();
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                if (mNavController.getCurrentStackIndex() == FragNavController.TAB3) {

                } else {
                    if (fragment instanceof AddListingDetailFragment) {
                        if (id != R.id.navigation_create_tab) {
                            AddListingDetailFragment addListingDetailFragment = (AddListingDetailFragment) fragment;
                            addListingDetailFragment.checkOnBackPress(id);
                        }
                    } else if(fragment instanceof BrandFragment) {
                        if (id != R.id.navigation_create_tab) {
                            BrandFragment brandFragment = (BrandFragment) fragment;
                            brandFragment.askForDataLoss(id);
                        }
                    } else if(fragment instanceof SizeMainFragment) {
                        if (id != R.id.navigation_create_tab) {
                            SizeMainFragment sizeMainFragment = (SizeMainFragment) fragment;
                            sizeMainFragment.askForDataLoss(id);
                        }
                    } else if(fragment instanceof ColorFragment) {
                        if (id != R.id.navigation_create_tab) {
                            ColorFragment colorFragment = (ColorFragment) fragment;
                            colorFragment.askForDataLoss(id);
                        }
                    } else if(fragment instanceof CategoryFragment) {
                        if (id != R.id.navigation_create_tab) {
                            CategoryFragment categoryFragment = (CategoryFragment) fragment;
                            categoryFragment.askForDataLoss(id);
                        }
                    } else if(fragment instanceof ShippingFragment) {
                        if (id != R.id.navigation_create_tab) {
                            ShippingFragment shippingFragment = (ShippingFragment) fragment;
                            shippingFragment.askForDataLoss(id);
                        }
                    } else {
                        if (mNavController.getCurrentStackIndex() == FragNavController.TAB1) {
                            if (fragment instanceof FeedsFragment) {
                                FeedsFragment feedsFragment = (FeedsFragment) fragment;
                                if (feedsFragment.isVisible()) {
                                    feedsFragment.moveToTop();
                                }
                            }
                        }
                        if (mNavController.getCurrentStackIndex() == FragNavController.TAB2) {
                            if (fragment instanceof BrowseFragment) {
                                BrowseFragment browseFragment = (BrowseFragment) fragment;
                                if (browseFragment.isVisible()) {
                                    browseFragment.moveToTop();
                                }
                            }
                        }
                        mNavController.clearStack();
                    }
                }
            }
        });
    }

    public void moveToTab(int id) {
        selectTab(id, true);
    }

    @Override
    public void pushFragment(Fragment fragment) {
        if (mNavController != null) {
            mNavController.pushFragment(fragment);
        }
    }

    void selectTab(int tabId, boolean isDefault) {
        switch (tabId) {
            case R.id.navigation_feeds_tab:
                updateBottomBarUI(mBinding.navigationFeedSelected, mBinding.navigationFeed, false);
                mNavController.switchTab(INDEX_FEEDS, FragNavTransactionOptions.newBuilder().allowStateLoss(true).build());
                break;
            case R.id.navigation_browse_tab:
                updateBottomBarUI(mBinding.navigationBrowseSelected, mBinding.navigationBrowse, false);
                mNavController.switchTab(INDEX_BROWSE, FragNavTransactionOptions.newBuilder().allowStateLoss(true).build());
                break;
            case R.id.navigation_create_tab:
                break;
            case R.id.navigation_alert_tab:
                updateBottomBarUI(mBinding.navigationAlertSelected, mBinding.navigationAlerts, false);
                mNavController.switchTab(INDEX_NOTIFICATION, FragNavTransactionOptions.newBuilder().allowStateLoss(true).build());
                break;
            case R.id.navigation_profile_tab:
                updateBottomBarUI(mBinding.navigationProfileSelected, mBinding.navigationProfile, false);
                mNavController.switchTab(INDEX_PROFILE, FragNavTransactionOptions.newBuilder().allowStateLoss(true).build());
                break;
        }
        if(isDefault) {
            while (!mNavController.isRootFragment()) {
                mNavController.popFragment();
            }
        }
    }

    void selectTab(int tabId) {
        selectTab(tabId, false);
    }

    public void popFragment() {
        if (!mNavController.isRootFragment()) {
            mNavController.popFragment();
            setBottomBarForCreateFragment();
        } else {
            if (mNavController.getCurrentStackIndex() != FragNavController.TAB1) {
                mBinding.bottomBar.setSelectedItemId(R.id.navigation_feeds_tab);
            } else {
                super.onBackPressed();
            }
        }
    }

    @Override
    public void setAllCategoryList(List<Category> allCategoryList) {
        List<Integer> categoriesIds = new ArrayList<>();
        for (int i = 1; i < allCategoryList.size(); i++) {
            categoriesIds.add(allCategoryList.get(i).getCategoryId());
        }
        this.allCategoryList.addAll(categoriesIds);
    }

    @Override
    public void pushFragment(Fragment fragment, FragNavTransactionOptions transactionOptions) {
        if (mNavController != null) {
            mNavController.pushFragment(fragment, transactionOptions);
        }
    }

    @Override
    public void onTabTransaction(Fragment fragment, int index) {
        if (getSupportActionBar() != null && mNavController != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(!mNavController.isRootFragment());
        }
    }

    @Override
    public void onFragmentTransaction(Fragment fragment, FragNavController.TransactionType transactionType) {
        if (getSupportActionBar() != null && mNavController != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(!mNavController.isRootFragment());
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mNavController != null) {
            mNavController.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (isCreateOverlayOpened) {
            isCreateOverlayOpened = false;
            mBinding.createFragmentLayout.setVisibility(View.GONE);
            setBottomBarForCreateFragment();
        } else if (fragment instanceof AddListingDetailFragment) {
            AddListingDetailFragment addListingDetailFragment = (AddListingDetailFragment) fragment;
            addListingDetailFragment.checkOnBackPress(0);
        } else {
            popFragment();
        }
        showBottomBar();
    }

    @Override
    public Fragment getRootFragment(int index) {
        switch (index) {
            case INDEX_FEEDS:
                FeedsFragment feedsFragment = FeedsFragment.newInstance();
                return feedsFragment;
            case INDEX_BROWSE:
                final BrowseFragment browseFragment = BrowseFragment.newInstance();
                return browseFragment;
            case INDEX_CREATE:

            case INDEX_NOTIFICATION:
                AlertFragment alertFragment = AlertFragment.newInstance();
                return alertFragment;
            case INDEX_PROFILE:
                ProfileFragment profileFragment = ProfileFragment.newInstance();
                profileFragment.setUserId(mSharedPreferenceUtility.getMyUserId());
                return profileFragment;
        }
        throw new IllegalStateException("Need to send an index that we know");
    }

    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
//                item.setShiftingMode(false);
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("ERROR NO SUCH FIELD", "Unable to get shift mode field");
        } catch (IllegalAccessException e) {
            Log.e("ERROR ILLEGAL ALG", "Unable to change value of shift mode");
        }
    }

    private void setProgressBarColor() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Drawable wrapDrawable = DrawableCompat.wrap(mBinding.progressBar.getIndeterminateDrawable());
            DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(this, R.color.purple));
            mBinding.progressBar.setIndeterminateDrawable(DrawableCompat.unwrap(wrapDrawable));
        } else {
            mBinding.progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.purple), PorterDuff.Mode.SRC_IN);
        }
    }

    @Override
    public void onListingLike(final int id) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.putListingLikes("Bearer " + getAccessToken(), "" + id).enqueue(new Callback<ApiResponse<Feeds>>() {
            @Override
            public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                if (response.isSuccessful() && response.code() == 200) {

                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            onListingLike(id);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    mBinding.progressBar.setVisibility(View.GONE);
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void onPostsLike(final int id) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.putPostLikes("Bearer " + getAccessToken(), "" + id).enqueue(new Callback<ApiResponse<Feeds>>() {
            @Override
            public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                if (response.isSuccessful() && response.code() == 200) {

                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            onPostsLike(id);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    mBinding.progressBar.setVisibility(View.GONE);
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });

    }

    @Override
    public List<FeedResponse> getList() {
        return mFeedsList;
    }

    @Override
    public List<UserResponse> getBestUserList() {
        return mBestUser;
    }

    @Override
    public List<UserResponse> getFollowingList() {
        return mFollowingList;
    }

    @Override
    public void openProfile(int userId) {
        ProfileFragment profileFragment = ProfileFragment.newInstance();
        profileFragment.setUserId(userId);
        pushFragment(profileFragment);
    }

    @Override
    public void openFollowFollowingFragment(int type, int userId, String userName) {
        FollowFollowingFragment followFollowingFragment = FollowFollowingFragment.newInstance();
        followFollowingFragment.settype(type);
        followFollowingFragment.setUserId(userId);
        followFollowingFragment.setUserName(userName);
        pushFragment(followFollowingFragment);
    }

    @Override
    public void openBestUserFragment(int tabPosition) {
        BestUserFragment bestUserFragment = BestUserFragment.newInstance();
        bestUserFragment.setTabPosition(tabPosition);
        pushFragment(bestUserFragment);
    }

    public void openMyPostFragment() {
        EditPostFragment editPostFragment = EditPostFragment.newInstance();
        pushFragment(editPostFragment);
    }

    @Override
    public void openMyLikeFragment(String username, int userId, int type) {
        MyLikesFragment myLikesFragment = MyLikesFragment.newInstance();
        myLikesFragment.setMyUserId(mSharedPreferenceUtility.getMyUserId());
        myLikesFragment.setFragmentType(type);
        myLikesFragment.setUsername(username);
        pushFragment(myLikesFragment);
    }

    @Override
    public void openMyBynFragment() {
        MyBynProfileFragment myBynProfileFragment = MyBynProfileFragment.newInstance();
        pushFragment(myBynProfileFragment);
    }

    @Override
    public void openEditProfileFragment(UserResponse user) {
        EditProfileFragment editProfileFragment = EditProfileFragment.newInstance();
        editProfileFragment.setUser(user);
        pushFragment(editProfileFragment);
    }

    @Override
    public void openGalleryFor(final int selectImage, final int imageCount) {
        checkPermissionForMarshMellow(new PermissionRequestCallBacks() {
            @Override
            public void onPermissionsGranted() {
                Matisse.from(HomeActivity.this)
                        .choose(MimeType.ofImage())
                        .capture(true)
                        .captureStrategy(new CaptureStrategy(true, "com.bynmix.app.fileProvider"))
                        .maxSelectable(imageCount)
                        .gridExpectedSize(getResources().getDimensionPixelSize(R.dimen.grid_expected_size))
                        .restrictOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT)
                        .thumbnailScale(0.85f)
                        .imageEngine(new GlideEngine())
                        .showSingleMediaType(true)
                        .forResult(selectImage);
            }

            @Override
            public void onPermissionsDenied() {
                final List<String> notGrantedPermissions = getNotGrantedPermissions(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                        , Manifest.permission.READ_EXTERNAL_STORAGE);
            }
        }, Constants.STORAGE_PERMISSION_REQUIRED, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE);
    }


    @Override
    public void onFollowerUser(final int userId) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.putFollower("Bearer " + getAccessToken(), "" + userId).enqueue(new Callback<ApiResponse<UserResponse>>() {
            @Override
            public void onResponse(Call<ApiResponse<UserResponse>> call, Response<ApiResponse<UserResponse>> response) {
                if (response.isSuccessful() && response.code() == 200) {

                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            onFollowerUser(userId);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    mBinding.progressBar.setVisibility(View.GONE);
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<UserResponse>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void openListingDetailFragment(int id) {
        ListingDetailFragment listingDetailFragment = ListingDetailFragment.newInstance();
        listingDetailFragment.setId(id);
        pushFragment(listingDetailFragment);
    }

    public void openInAppBrowser(String url, String title) {
        InAppBrowserFragment inAppBrowserFragment = InAppBrowserFragment.newInstance();
        inAppBrowserFragment.setUrl(url, title);
        pushFragment(inAppBrowserFragment);
    }

    @Override
    public void getListingDetail(final int id) {
        mProgressBarDialog.showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.listingDetailById("Bearer " + getAccessToken(), "" + id).enqueue(new Callback<ApiResponse<ListingDetail>>() {
            @Override
            public void onResponse(Call<ApiResponse<ListingDetail>> call, Response<ApiResponse<ListingDetail>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    setListingDetailResponse(response);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getListingDetail(id);
                        }

                        @Override
                        public void errorAccessToken() {
                            mProgressBarDialog.hideProgress();
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(HomeActivity.this, response.errorBody());
                    mAlertDialog.showDialog(HomeActivity.this, message);

                }
                mProgressBarDialog.hideProgress();
                mBinding.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ApiResponse<ListingDetail>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mBinding.progressBar.setVisibility(View.GONE);
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void clearListingImagesList() {
        mListingCroppedList.clear();
    }

    @Override
    public void deletedListingImagesIdClear() {
        deletedListingImagesId.clear();
    }

    @Override
    public String getPostImage() {
        return mPostImage;
    }

    @Override
    public boolean isEditPost() {
        return isEditPost;
    }

    public void setEditPost(boolean isEdit) {
        isEditPost = isEdit;
    }

    @Override
    public List<AnnotatedPoint> getAnnotatedPoints() {
        return postDots;
    }

    @Override
    public void setAnnotatedPoints(List<AnnotatedPoint> points) {
        this.postDots = points;
    }

    @Override
    public List<AnnotatedPoint> getDeletedDots() {
        return deletedDots;
    }

    @Override
    public void setDeletedDots(List<AnnotatedPoint> points) {
        this.deletedDots = points;
    }

    @Override
    public void addDeletedDots(AnnotatedPoint point) {
        this.deletedDots.add(point);
    }

    @Override
    public void dotsDetailFragment(FeedResponse post) {
        DotsDetailFragment dotsDetailFragment = DotsDetailFragment.newInstance();
        dotsDetailFragment.setPost(post);
        pushFragment(dotsDetailFragment);
    }

    @Override
    public void openSelectListingForDotsFragment(int position, AddTagsListener dotsListener) {
        SelectListingForDotsFragment selectListingForDotsFragment = SelectListingForDotsFragment.newInstance();
        selectListingForDotsFragment.setPosition(position);
        selectListingForDotsFragment.setListener(dotsListener);
        pushFragment(selectListingForDotsFragment);
    }

    @Override
    public void setUserFilterData(List<SelectedFilter> mSelectedFilterList, HeightModel heightModel) {
        userFilterData = new UserFilterData();
        List<Integer> stylesIds = new ArrayList<>();
        List<Integer> bodyTypeIds = new ArrayList<>();
        List<Integer> brandsIds = new ArrayList<>();
        this.mSelectedHeightModel = heightModel;
        this.mSelectedUserFilterList = mSelectedFilterList;
        for (int i = 0; i < mSelectedFilterList.size(); i++) {
            if (mSelectedFilterList.get(i).getType() == Constants.USER_STYLE_FILTER) {
                if (mSelectedFilterList.get(i).getId() != 0) {
                    stylesIds.add(mSelectedFilterList.get(i).getId());
                    userFilterData.setStyleIds(stylesIds);
                }
            } else if (mSelectedFilterList.get(i).getType() == Constants.USER_BODY_TYPE_FILTER) {
                if (mSelectedFilterList.get(i).getId() != 0) {
                    bodyTypeIds.add(mSelectedFilterList.get(i).getId());
                    userFilterData.setBodyTypeIds(bodyTypeIds);
                }
            } else if (mSelectedFilterList.get(i).getType() == Constants.USER_BRAND_FILTER) {
                if (mSelectedFilterList.get(i).getId() != 0) {
                    brandsIds.add(mSelectedFilterList.get(i).getId());
                    userFilterData.setBrandIds(brandsIds);
                }
            }
        }
        userFilterData.setCustomMinHeightFeet("" + heightModel.getCustomMinHeightFeet());
        userFilterData.setCustomMinHeightInches("" + heightModel.getCustomMinHeightInches());
        userFilterData.setCustomMaxHeightFeet("" + heightModel.getCustomMaxHeightFeet());
        userFilterData.setCustomMaxHeightInches("" + heightModel.getCustomMaxHeightInches());
        if (heightModel.isHeightShowAll()) {
            userFilterData.setCustomMinHeightFeet(null);
            userFilterData.setCustomMinHeightInches(null);
            userFilterData.setCustomMaxHeightFeet(null);
            userFilterData.setCustomMaxHeightInches(null);
        } else if (heightModel.getCustomMinHeightFeet() == 3 && heightModel.getCustomMaxHeightFeet() == 8 && heightModel.getCustomMinHeightInches() == 0 && heightModel.getCustomMaxHeightInches() == 8) {
            userFilterData.setCustomMinHeightFeet(null);
            userFilterData.setCustomMinHeightInches(null);
            userFilterData.setCustomMaxHeightFeet(null);
            userFilterData.setCustomMaxHeightInches(null);
        }

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment != null && fragment instanceof SearchUserFragment) {
            SearchUserFragment SearchUserFragement = (SearchUserFragment) fragment;
            SearchUserFragement.clearListingList();
        }
    }

    @Override
    public HeightModel getHeightModel() {
        return mSelectedHeightModel;
    }

    @Override
    public void setHeightModel(HeightModel mSelectedHeightModel) {
        this.mSelectedHeightModel = mSelectedHeightModel;
    }

    @Override
    public void getListingDetailImages(final int id) {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.listingDetailImagesById("Bearer " + getAccessToken(), "" + id).enqueue(new Callback<ApiResponse<List<ListingDisplayImage>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<ListingDisplayImage>>> call, Response<ApiResponse<List<ListingDisplayImage>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    setListingDetailImagesResponse(response);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getListingDetailImages(id);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    mBinding.progressBar.setVisibility(View.GONE);
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<ListingDisplayImage>>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void openLikeFragment(int id, int type) {
        LikesFragment likesFragment = LikesFragment.newInstance(id, type);
        pushFragment(likesFragment);
    }

    @Override
    public void openFilterFragment(int type) {
        FilterFragment filterFragment = FilterFragment.newInstance();
        filterFragment.setFragmentType(type);
        FragNavTransactionOptions fragNavTransactionOptions = FragNavTransactionOptions.newBuilder()
                .transition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .customAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                .build();
        pushFragment(filterFragment, fragNavTransactionOptions);
    }

    @Override
    public int getFilterFragmentType() {
        return filterFragmentType;
    }

    @Override
    public void acceptOffer(final int listingId, final int offerId, final boolean isLoggedInUserOffer) {
        mProgressBarDialog.showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.acceptOffer("Bearer " + getAccessToken(), "" + listingId, "" + offerId).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    mProgressBarDialog.hideProgress();
                    onBackPressed();
                    if (isLoggedInUserOffer) {
                        openMyBynFragment();
                    } else {
                        openPurchasesFragment(Constants.MY_PURCHASES);
                    }
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {

                            acceptOffer(listingId, offerId, isLoggedInUserOffer);
                        }

                        @Override
                        public void errorAccessToken() {
                            mProgressBarDialog.hideProgress();
                        }
                    });
                } else {
                    if(isLoggedInUserOffer) {
                        DialogAlert.showCardNotAcceptedDialog(HomeActivity.this, "Looks like we could not process the transaction at this time due to the buyer’s faulty payment option.\n\nWe have notified the buyer to update their payment option and resubmit their offer.");
                    } else {
                        String message = Utils.ErrorMessage(HomeActivity.this, response.errorBody());
                        DialogAlert.showCardNotAcceptedDialog(HomeActivity.this, "Looks like we could not process your purchase at this time due to the following reason:\n\n" + message + "\n\nPlease use a different payment option and resubmit your offer to the seller.");
                    }

                    mProgressBarDialog.hideProgress();
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void declineOffer(final int listingId, final int offerId, final int type) {
        mProgressBarDialog.showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.declineOffer("Bearer " + getAccessToken(), "" + listingId, "" + offerId).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    mProgressBarDialog.hideProgress();
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    if (fragment != null && fragment instanceof AlertFragment) {
                        AlertFragment alertFragment = (AlertFragment) fragment;
                        alertFragment.refreshNotification(type);
                    } else if (fragment != null && fragment instanceof ListingOfferFragment) {
                        ListingOfferFragment listingOfferFragment = (ListingOfferFragment) fragment;
                        listingOfferFragment.refreshoffers();
                    }

                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {

                            declineOffer(listingId, offerId, type);
                        }

                        @Override
                        public void errorAccessToken() {
                            mProgressBarDialog.hideProgress();
                        }
                    });
                } else {
                    mProgressBarDialog.hideProgress();
                    try {
                        JSONObject errorObject = new JSONObject(response.errorBody().string().trim());
                        JSONArray error = errorObject.getJSONArray("errors");
                        JSONObject messageObject = error.getJSONObject(0);
                        String message = messageObject.getString("message");
                        mAlertDialog.showDialog(HomeActivity.this, message);
                    } catch (Exception ex) {
                        mAlertDialog.showDialog(HomeActivity.this, getString(R.string.error_server));
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    private void setListingDetailImagesResponse(Response<ApiResponse<List<ListingDisplayImage>>> response) {
        List<ListingDisplayImage> listingDisplayImages = response.body().getData();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof ListingDetailFragment) {
            ListingDetailFragment listingDetailFragment = (ListingDetailFragment) fragment;
            listingDetailFragment.setImages(listingDisplayImages);
        } else if (fragment instanceof EditMyListingFragment) {
            EditMyListingFragment editMyListingFragment = (EditMyListingFragment) fragment;
            editMyListingFragment.setImages(listingDisplayImages);
        } else if (fragment instanceof EditListingFragment) {
            EditListingFragment editMyListingFragment = (EditListingFragment) fragment;
            editMyListingFragment.setImages(listingDisplayImages);
        }
    }

    private void setListingDetailResponse(Response<ApiResponse<ListingDetail>> response) {
        mProgressBarDialog.hideProgress();
        ListingDetail listingDetail = response.body().getData();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof ListingDetailFragment) {
            ListingDetailFragment listingDetailFragment = (ListingDetailFragment) fragment;
            listingDetailFragment.setData(listingDetail);
        } else if (fragment instanceof EditMyListingFragment) {
            EditMyListingFragment editMyListingFragment = (EditMyListingFragment) fragment;
            this.listingDetail = listingDetail;
            editMyListingFragment.setData(listingDetail);
        } else if (fragment instanceof MakeAnOfferFragment) {
            MakeAnOfferFragment makeAnOfferFragment = (MakeAnOfferFragment) fragment;
            makeAnOfferFragment.setListingDetail(listingDetail);
        } else if (fragment instanceof AlertFragment) {
            AlertFragment alertFragment = (AlertFragment) fragment;
            alertFragment.setListingDetail(listingDetail);
        } else if (fragment instanceof EditListingFragment) {
            EditListingFragment editMyListingFragment = (EditListingFragment) fragment;
            this.listingDetail = listingDetail;
            updateListingIds();
            editMyListingFragment.setData(this.listingDetail);
        }
    }

    private void updateListingIds() {
        this.listingDetail.setShippingRateId(this.listingDetail.getShippingInfo().getShippingRateId());
        this.listingDetail.setSizeTypeId(this.listingDetail.getSizeType().getSizeTypeId());
        this.listingDetail.setSizeId(this.listingDetail.getSize().getSizeId());
        this.listingDetail.setCategoryId(this.listingDetail.getCategory().getCategoryId());
        this.listingDetail.setCategoryTypeId(this.listingDetail.getCategoryType().getCategoryTypeId());
        this.listingDetail.setBrandId(this.listingDetail.getBrand().getBrandId());
        this.listingDetail.setAvailabilityId(this.listingDetail.getAvailability().getAvailabilityId());
        this.listingDetail.setStatusId(this.listingDetail.getStatus().getStatus());
    }

    public void logout() {
        LogoutDialog mlogoutDialog = new LogoutDialog();
        mlogoutDialog.showDialog(this, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.FOR_PROFILE_PIC && resultCode == RESULT_OK && data != null) {
            List<String> images = Matisse.obtainPathResult(data);
            sendImageToCropper(images.get(0), Constants.COMPRESS_100, Constants.CROPPER_REQUEST_CODE);
        } else if (requestCode == Constants.CREATE_POST && resultCode == RESULT_OK && data != null) {
            createPost(requestCode, resultCode, Constants.CREATE_POST_CROPPER_REQUEST_CODE, data);
        } else if (requestCode == Constants.EDIT_POST && resultCode == RESULT_OK && data != null) {
            createPost(requestCode, resultCode, Constants.EDIT_POST_CROPPER_REQUEST_CODE, data);
        } else if (requestCode == Constants.CREATE_LISTING && resultCode == RESULT_OK && data != null) {
            mListingImagesList = Matisse.obtainPathResult(data);
            imagesSelectedByUserCount = mListingImagesList.size();
            imagesCropped = 0;
            mListingCroppedList.clear();
            sendImageToCropper(mListingImagesList.get(0), Constants.COMPRESS_100, Constants.CREATE_LISTING_CROPPER_REQUEST_CODE);
        } else if (requestCode == Constants.FOR_BACKGROUND_PIC && resultCode == RESULT_OK && data != null) {
            List<String> images = Matisse.obtainPathResult(data);
            sendImageToCropper(images.get(0), Constants.COMPRESS_100, Constants.CROPPER_COVER_REQUEST_CODE);
        } else if (requestCode == Constants.REPORT_AN_ISSUE_IMAGE_1 && resultCode == RESULT_OK && data != null) {
            List<String> images = Matisse.obtainPathResult(data);
            sendImageToCropper(images.get(0), Constants.COMPRESS_100, Constants.CROPPER_REPORT_AN_ISSUE_REQUEST_CODE_1);
        } else if (requestCode == Constants.REPORT_AN_ISSUE_IMAGE_2 && resultCode == RESULT_OK && data != null) {
            List<String> images = Matisse.obtainPathResult(data);
            sendImageToCropper(images.get(0), Constants.COMPRESS_100, Constants.CROPPER_REPORT_AN_ISSUE_REQUEST_CODE_2);
        } else if (requestCode == Constants.REPORT_AN_ISSUE_IMAGE_3 && resultCode == RESULT_OK && data != null) {
            List<String> images = Matisse.obtainPathResult(data);
            sendImageToCropper(images.get(0), Constants.COMPRESS_100, Constants.CROPPER_REPORT_AN_ISSUE_REQUEST_CODE_3);
        } else if (requestCode == Constants.REPORT_AN_ISSUE_IMAGE_4 && resultCode == RESULT_OK && data != null) {
            List<String> images = Matisse.obtainPathResult(data);
            sendImageToCropper(images.get(0), Constants.COMPRESS_100, Constants.CROPPER_REPORT_AN_ISSUE_REQUEST_CODE_4);
        } else if (requestCode == Constants.CROPPER_REQUEST_CODE && resultCode == RESULT_OK) {
            if (croppedBitmap != null) {
                ImageData imageData1 = thumbUrl(croppedBitmap);
                String thumbUrl = imageData1.getThumbUrl();
                try {
                    String imagePath = thumbUrl;
                    Bitmap bMapScaled = Bitmap.createScaledBitmap(croppedBitmap, 250, 250, true);
                    ImageData imageData = thumbUrl(bMapScaled);
                    String thumbUrl1 = imageData.getThumbUrl();
                    File f = new File(thumbUrl1);
                    ImageData imageData2 = compressImage(f.getAbsolutePath(), Constants.COMPRESS_100);
                    String compressedDisplayPic = imageData2.getUrl();
                    sendImageData(Constants.FOR_PROFILE_PIC, compressedDisplayPic);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == Constants.CREATE_POST_CROPPER_REQUEST_CODE && resultCode == RESULT_OK) {
            setCropperPhoto(requestCode);
        } else if (requestCode == Constants.EDIT_POST_CROPPER_REQUEST_CODE && resultCode == RESULT_OK) {
            setCropperPhoto(requestCode);
        } else if (requestCode == Constants.CROPPER_REPORT_AN_ISSUE_REQUEST_CODE_1 && resultCode == RESULT_OK) {
            setCropperPhoto(requestCode);
        } else if (requestCode == Constants.CROPPER_REPORT_AN_ISSUE_REQUEST_CODE_2 && resultCode == RESULT_OK) {
            setCropperPhoto(requestCode);
        } else if (requestCode == Constants.CROPPER_REPORT_AN_ISSUE_REQUEST_CODE_3 && resultCode == RESULT_OK) {
            setCropperPhoto(requestCode);
        } else if (requestCode == Constants.CROPPER_REPORT_AN_ISSUE_REQUEST_CODE_4 && resultCode == RESULT_OK) {
            setCropperPhoto(requestCode);
        } else if (requestCode == Constants.CREATE_LISTING_CROPPER_REQUEST_CODE && resultCode == RESULT_OK) {
            if (croppedBitmap != null) {
                try {
                    ImageData imageData = thumbUrl(croppedBitmap);
                    String thumbUrl1 = imageData.getThumbUrl();
                    File f = new File(thumbUrl1);
                    ImageData imageData2 = compressImage(f.getAbsolutePath(), Constants.COMPRESS_100);
                    String compressedDisplayPic = imageData2.getUrl();
                    mListingCroppedList.add(compressedDisplayPic);
                    checkIfAllPhotosCroppedAndUpload();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (requestCode == Constants.CROPPER_COVER_REQUEST_CODE && resultCode == RESULT_OK) {
            String filePath = data.getStringExtra(Constants.COVER_FILE_EXTRA);
            sendImageData(Constants.FOR_BACKGROUND_PIC, filePath);
        } else if (requestCode == Constants.CREATE_LISTING_CROPPER_REQUEST_CODE) {
            imagesSelectedByUserCount--;
            if (imagesSelectedByUserCount != 0)
                checkIfAllPhotosCroppedAndUpload();
        }

    }

    private void checkIfAllPhotosCroppedAndUpload() {
        imagesCropped++;
        if (mListingCroppedList.size() == imagesSelectedByUserCount) {
            setCreateListImage(mListingCroppedList);
        } else {
            sendImageToCropper(mListingImagesList.get(imagesCropped), Constants.COMPRESS_100, Constants.CREATE_LISTING_CROPPER_REQUEST_CODE);
        }
    }

    private void sendImageToCropper(String path, int ImageCompressed, int requestType) {
        try {
            File f = new File(path);
            ImageData imagedata = compressImage(f.getAbsolutePath(), ImageCompressed);
            if (imagedata != null) {
                String fullImagePath = imagedata.getUrl();
                Intent intent;
                if (requestType == Constants.CROPPER_COVER_REQUEST_CODE) {
                    intent = new Intent(HomeActivity.this, CoverCropper.class);
                } else {
                    intent = new Intent(HomeActivity.this, Cropper.class);
                }
                intent.putExtra("image", fullImagePath);
                startActivityForResult(intent, requestType);
            } else {
                Toast.makeText(this, getString(R.string.error_server), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setCropperPhoto(int requestCode) {
        if (croppedBitmap != null) {
            try {
                ImageData imageData = thumbUrl(croppedBitmap);
                String thumbUrl1 = imageData.getThumbUrl();
                File f = new File(thumbUrl1);
                ImageData imageData2 = compressImage(f.getAbsolutePath(), Constants.COMPRESS_100);
                String compressedDisplayPic = imageData2.getUrl();
                setCreatePostImage(compressedDisplayPic, requestCode);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void createPost(int requestCode, int resultCode, int cropperCode, Intent data) {
        List<String> images = Matisse.obtainPathResult(data);
        try {
            File f = new File(images.get(0));
            ImageData imagedata = compressImage(f.getAbsolutePath(), Constants.COMPRESS_100);
            if (imagedata != null) {
                String fullImagePath = imagedata.getUrl();
                Intent intent = new Intent(HomeActivity.this, Cropper.class);
                intent.putExtra("image", fullImagePath);
                startActivityForResult(intent, cropperCode);
            } else {
                Toast.makeText(this, getString(R.string.error_server), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setCreateListImage(List<String> compressedDisplayPic) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof CreateListingFragment) {
            CreateListingFragment createListingFragment = (CreateListingFragment) fragment;
            createListingFragment.setPhoto(compressedDisplayPic);
        } else if (fragment instanceof EditMyListingFragment) {
            EditMyListingFragment editMyListingFragment = (EditMyListingFragment) fragment;
            editMyListingFragment.updateList(compressedDisplayPic);
        } else if (fragment instanceof EditListingFragment) {
            EditListingFragment editMyListingFragment = (EditListingFragment) fragment;
            editMyListingFragment.updateList(compressedDisplayPic);
        }
    }

    private void setCreatePostImage(String compressedDisplayPic, int requestCode) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (requestCode == Constants.CREATE_POST_CROPPER_REQUEST_CODE) {
            if (fragment instanceof AddPostFragment) {
                mPostImage = compressedDisplayPic;
                isPicSelected = true;
                isEditPost = false;
            }
        } else if (requestCode == Constants.EDIT_POST_CROPPER_REQUEST_CODE) {
            if (fragment instanceof EditMyPostFragment) {
                mPostImage = compressedDisplayPic;
                isPicSelected = true;
                isEditPost = true;
            }
        } else {
            if (fragment instanceof ReportAnIssueFragment) {
                ReportAnIssueFragment reportAnIssueFragment = (ReportAnIssueFragment) fragment;
                reportAnIssueFragment.setPhoto(compressedDisplayPic, requestCode);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isPicSelected) {
            isPicSelected = false;
            pushFragment(AddDotsFragment.newInstance());
        }
    }

    private void sendImageData(final int type, final String selectedImagePath) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        MultipartBody.Part parts = prepareFilePart("image", selectedImagePath, Constants.IMAGE_MEDIA_TYPE, "jpg");
        final ProgressDialog loading = ProgressDialog.show(this, "Uploading...", "Please wait...", false, false);
        if (type == Constants.FOR_PROFILE_PIC) {
            apiInstance.putProfilePhoto("Bearer " + getAccessToken(), "" + mSharedPreferenceUtility.getMyUserId(), parts).enqueue(new Callback<ApiResponse<String>>() {
                @Override
                public void onResponse(Call<ApiResponse<String>> call, Response<ApiResponse<String>> response) {
                    if (response.isSuccessful() && response.code() == 201) {
                        setImages(type, response.body().getData());
                        loading.dismiss();
                    } else if (response.code() == 401) {
                        loading.dismiss();
                        getAccessTokenFromServer(new AccessTokenResponse() {
                            @Override
                            public void successAccessToken() {
                                sendImageData(type, selectedImagePath);
                            }

                            @Override
                            public void errorAccessToken() {
                                mBinding.progressBar.setVisibility(View.GONE);
                                loading.dismiss();
                            }
                        });
                    } else {
                        loading.dismiss();
                        mBinding.progressBar.setVisibility(View.GONE);
                        mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                    }
                }

                @Override
                public void onFailure(Call<ApiResponse<String>> call, Throwable t) {
                    loading.dismiss();
                    mBinding.progressBar.setVisibility(View.GONE);
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            });
        } else if (type == Constants.FOR_BACKGROUND_PIC) {
            apiInstance.putBackgroundPhoto("Bearer " + getAccessToken(), "" + mSharedPreferenceUtility.getMyUserId(), parts).enqueue(new Callback<ApiResponse<String>>() {
                @Override
                public void onResponse(Call<ApiResponse<String>> call, Response<ApiResponse<String>> response) {
                    if (response.isSuccessful() && response.code() == 201) {
                        setImages(type, response.body().getData());
                        loading.dismiss();

                    } else if (response.code() == 401) {
                        loading.dismiss();
                        getAccessTokenFromServer(new AccessTokenResponse() {
                            @Override
                            public void successAccessToken() {
                                sendImageData(type, selectedImagePath);
                            }

                            @Override
                            public void errorAccessToken() {
                                mBinding.progressBar.setVisibility(View.GONE);
                                loading.dismiss();
                            }
                        });
                    } else {
                        loading.dismiss();
                        mBinding.progressBar.setVisibility(View.GONE);
                        mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                    }
                }

                @Override
                public void onFailure(Call<ApiResponse<String>> call, Throwable t) {
                    loading.dismiss();
                    mBinding.progressBar.setVisibility(View.GONE);
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            });
        }
    }

    private void setImages(int type, String selectedImagePath) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment != null && fragment instanceof ProfileFragment) {
            ProfileFragment profileFragment = (ProfileFragment) fragment;
            if (type == Constants.FOR_PROFILE_PIC) {
                profileFragment.updateProfilePhoto(selectedImagePath);
            } else {
                profileFragment.updateBackgroundPhoto(selectedImagePath);
            }
        }
    }


    @NonNull
    public MultipartBody.Part prepareFilePart(String partName, String fileUri, String
            mediaType, String fileExtension) {
        File file = new File(fileUri);
        RequestBody requestFile = RequestBody.create(MediaType.parse(mediaType + "/" + fileExtension), file);
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    public String getPath(Uri uri) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            String path = cursor.getString(column_index);
            cursor.close();
            return path;
        }
        return uri.getPath();
    }

    @Override
    public void sendPostDetail(final FeedResponse post) {
        final SavingPostDialog loading = new SavingPostDialog(this, getString(R.string.saving_post));
        loading.showDialog();
        if (postDots != null && postDots.size() > 0) {
            post.setPostDots(postDots);
        }
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.sendPostData("Bearer " + getAccessToken(), post).enqueue(new Callback<ApiResponse<Integer>>() {
            @Override
            public void onResponse(Call<ApiResponse<Integer>> call, Response<ApiResponse<Integer>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    postDots = null;
                    int postId = response.body().getData();
                    sendMyPostImage(postId, loading, "", Constants.CREATE_POST);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            sendPostDetail(post);
                        }

                        @Override
                        public void errorAccessToken() {
                            loading.dismiss();
                        }
                    });
                } else {
                    loading.dismiss();
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Integer>> call, Throwable t) {
                loading.dismiss();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void editPostDetail(final FeedResponse post, final String photoUrl, final String oldPhotoUrl) {
        final SavingPostDialog loading = new SavingPostDialog(this, getString(R.string.saving_post));
        loading.showDialog();
        if (postDots != null && postDots.size() > 0) {
            if (deletedDots != null && deletedDots.size() > 0) {
                postDots.addAll(deletedDots);
            }
            post.setPostDots(postDots);
        }
        Log.d("msg",new Gson().toJson(post));
        ApiEndpointInterface apiInstance = ApiService.instance();
        if (oldPhotoUrl == null || !oldPhotoUrl.equalsIgnoreCase(photoUrl)) {
            apiInstance.deletePostDots("Bearer " + getAccessToken(), "" + post.getId()).enqueue(new Callback<ApiResponse<ResponseBody>>() {
                @Override
                public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                    if (response.isSuccessful() && response.code() == 200) {
                        editPost(post, photoUrl, oldPhotoUrl, loading);
                    } else if (response.code() == 401) {
                        getAccessTokenFromServer(new AccessTokenResponse() {
                            @Override
                            public void successAccessToken() {
                                editPostDetail(post, photoUrl, oldPhotoUrl);
                            }

                            @Override
                            public void errorAccessToken() {

                            }
                        });
                    } else {
                        mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                    }
                }

                @Override
                public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            });
        } else {
            editPost(post, photoUrl, oldPhotoUrl, loading);
        }

    }

    private void editPost(final FeedResponse post, final String photoUrl, final String oldPhotoUrl, final SavingPostDialog loading) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.editPostData("Bearer " + getAccessToken(), "" + post.getId(), post).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    postDots = null;
                    if (oldPhotoUrl != null && oldPhotoUrl.equalsIgnoreCase(photoUrl)) {
                        loading.dismiss();
                        onBackPressed();
                        popFragment();
                        openMyPostFragment();
                    } else {
                        getPostImageId(post.getId(), photoUrl, loading, post.isHasPostDots());
                    }
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            editPostDetail(post, photoUrl, oldPhotoUrl);
                        }

                        @Override
                        public void errorAccessToken() {
                            loading.dismiss();
                        }
                    });
                } else {
                    loading.dismiss();
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                loading.dismiss();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    private void getPostImageId(final int id, final String photoUrl, final SavingPostDialog loading, final boolean isHasDots) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getImageId("Bearer " + getAccessToken(), "" + id).enqueue(new Callback<ApiResponse<List<PostImage>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<PostImage>>> call, Response<ApiResponse<List<PostImage>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    List<PostImage> postImage = response.body().getData();
                    deletePostImage(id, postImage.get(0).getPostImageId(), photoUrl, loading, isHasDots);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getPostImageId(id, photoUrl, loading, isHasDots);
                        }

                        @Override
                        public void errorAccessToken() {
                            loading.dismiss();
                        }
                    });
                } else {
                    loading.dismiss();
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<PostImage>>> call, Throwable t) {
                loading.dismiss();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    private void deletePostImage(final int postId, final int postImageId, final String photoUrl, final SavingPostDialog loading, final boolean isHasDots) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        List<Integer> deletedPostImagesId = new ArrayList<>();
        deletedPostImagesId.add(postImageId);
        apiInstance.deletePostImage("Bearer " + getAccessToken(), "" + postId, deletedPostImagesId).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 200) {
//                    deletePostDots(postId);
                    sendMyPostImage(postId, loading, photoUrl, Constants.EDIT_POST);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            deletePostImage(postId, postImageId, photoUrl, loading, isHasDots);
                        }

                        @Override
                        public void errorAccessToken() {
                            loading.dismiss();
                        }
                    });
                } else {
                    loading.dismiss();
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                loading.dismiss();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }


    @Override
    public void openAddListingDetailFragment(List<ListingDisplayImage> photoList) {
        AddListingDetailFragment addListingDetailFragment = AddListingDetailFragment.newInstance();
        addListingDetailFragment.setPhotoList(photoList);
        pushFragment(addListingDetailFragment);
    }

    @Override
    public void setCategory(ListingDetail listingDetail) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof CategoryFragment) {
            CategoryFragment categoryFragment = (CategoryFragment) fragment;
            categoryFragment.setListingDetail(listingDetail);
        }
    }

    @Override
    public void showCategoryTick(CategoryType categoryType) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof CategoryFragment) {
            CategoryFragment categoryFragment = (CategoryFragment) fragment;
            categoryFragment.showTick(categoryType);
        }
    }

    @Override
    public void clearListingDetails() {
        clearListDetail();
    }

    @Override
    public void openEditMyPostFragment(FeedResponse post) {
        EditMyPostFragment editPostFragment = EditMyPostFragment.newInstance();
        setDeletedDots(new ArrayList<AnnotatedPoint>());
        editPostFragment.setPost(post);
        pushFragment(editPostFragment);
    }


    @Override
    public void openAddNewAddressFragment(Address address, int type, SelectExistingAddressListener mListener) {
        AddNewAddressFragment addNewAddressFragment = AddNewAddressFragment.newInstance();
        addNewAddressFragment.setAddress(address);
        addNewAddressFragment.setType(type);
        addNewAddressFragment.setListener(mListener);
        pushFragment(addNewAddressFragment);
    }

    @Override
    public void openMyAddressFragment() {
        MyAddressFragment myAddressFragment = MyAddressFragment.newInstance();
        pushFragment(myAddressFragment);
    }

    @Override
    public void openAddAddressFragment() {
        AddAddressFragment addAddressFragment = AddAddressFragment.newInstance();
        pushFragment(addAddressFragment);
    }

    @Override
    public void editAddress(final Address address) {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.editAddress("Bearer " + getAccessToken(), "" + address.getAddressId(), address).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                if (response.isSuccessful() && response.code() == 200) {
                    onBackPressed();
                    if (fragment != null && fragment instanceof MyAddressFragment) {
                        MyAddressFragment myAddressFragment = (MyAddressFragment) fragment;
                        myAddressFragment.updateUI();
                    }
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            editAddress(address);
                        }

                        @Override
                        public void errorAccessToken() {
                            mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(HomeActivity.this, response.errorBody());
                    mAlertDialog.showDialog(HomeActivity.this, message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void openWallet() {
        MyWalletFragment myWalletFragment = MyWalletFragment.newInstance();
        pushFragment(myWalletFragment);
    }

    @Override
    public void openEditMyListingFragment(int listingId) {
        EditListingFragment editMyListingFragment = EditListingFragment.newInstance();
        editMyListingFragment.setId(listingId);
        pushFragment(editMyListingFragment);
    }

    @Override
    public void openAddPaymentFragment(int cardId, DialogClickListener mListener, int type) {
        AddPaymentFragment addPaymentFragment = AddPaymentFragment.newInstance();
        addPaymentFragment.setCardId(cardId);
        addPaymentFragment.setListener(mListener);
        addPaymentFragment.setType(type);
        pushFragment(addPaymentFragment);
    }

    @Override
    public void getWalletDetail() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getWalletDetail("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<List<CardDetail>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<CardDetail>>> call, Response<ApiResponse<List<CardDetail>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mProgressBarDialog.hideProgress();
                    List<CardDetail> cardDetailList = response.body().getData();
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    if (fragment instanceof MyWalletFragment) {
                        MyWalletFragment myWalletFragment = (MyWalletFragment) fragment;
                        myWalletFragment.updateList(cardDetailList);
                    } else if (fragment instanceof SelectDifferentPaymentFragment) {
                        SelectDifferentPaymentFragment selectDifferentPaymentFragment = (SelectDifferentPaymentFragment) fragment;
                        selectDifferentPaymentFragment.updateList(cardDetailList);
                    } else if (fragment instanceof SellerAccountSetupFragment) {
                        SellerAccountSetupFragment sellerAccountSetupFragment = (SellerAccountSetupFragment) fragment;
                        sellerAccountSetupFragment.updatePaymentList(cardDetailList);
                    } else if (fragment instanceof SelectExistingPaymentFragment) {
                        SelectExistingPaymentFragment selectExistingPaymentFragment = (SelectExistingPaymentFragment) fragment;
                        selectExistingPaymentFragment.setList(cardDetailList);
                    }
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getWalletDetail();
                        }

                        @Override
                        public void errorAccessToken() {
                            mProgressBarDialog.hideProgress();
                        }
                    });
                } else {
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<CardDetail>>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void openSelectExistingAddressFragment(SelectExistingAddressListener mCallback, List<Address> mAddressList, boolean b) {
        SelectExistingAddressFragment selectExistingAddressFragment = SelectExistingAddressFragment.newInstance();
        selectExistingAddressFragment.setListener(mCallback);
        selectExistingAddressFragment.updateList(mAddressList);
        selectExistingAddressFragment.showAddAddressButton(b);
        pushFragment(selectExistingAddressFragment);
    }

    @Override
    public void openSelectNewPaymentFragment(List<CardDetail> mList, CardDetail cardDetail, int position) {
        SelectNewPaymentFragment selectNewPaymentFragment = SelectNewPaymentFragment.newInstance();
        selectNewPaymentFragment.setList(mList, cardDetail, position);
        pushFragment(selectNewPaymentFragment);
    }

    @Override
    public void openNotificationSettingsFragment() {
        NotificationSettingsFragment notificationSettingsFragment = NotificationSettingsFragment.newInstance();
        pushFragment(notificationSettingsFragment);
    }

    @Override
    public void openPurchasesFragment(int type) {
        PurchasesFragment purchasesFragment = PurchasesFragment.newInstance();
        purchasesFragment.setType(type);
        pushFragment(purchasesFragment);
    }

    @Override
    public void openPurchaseDetailFragment(int id, int type) {
        PurchaseDetailFragment purchaseDetailFragment = PurchaseDetailFragment.newInstance();
        purchaseDetailFragment.setId(id, type);
        pushFragment(purchaseDetailFragment);
    }

    @Override
    public MySalesStats mySaleState() {
        return mySalesStats;
    }

    @Override
    public MyPurchasesStats myPurchaseState() {
        return myPurchasesStats;
    }

    @Override
    public void openAddPostDetailFragment() {
        AddPostDetailFragment addPostDetailFragment = AddPostDetailFragment.newInstance();
        pushFragment(addPostDetailFragment);
    }

    @Override
    public void openCancelSaleFragment(Stock detail) {
        CancelSaleFragment cancelSaleFragment = CancelSaleFragment.newInstance();
        cancelSaleFragment.setStock(detail);
        pushFragment(cancelSaleFragment);
    }

    @Override
    public void openEditPostDotsFragment(FeedResponse post) {
        EditPostDotsFragment editPostDotsFragment = EditPostDotsFragment.newInstance();
        editPostDotsFragment.setPostDetail(post);
        pushFragment(editPostDotsFragment);
    }

    @Override
    public void openReviewFragment(int userId, String userName) {
        ReviewFragment reviewFragment = ReviewFragment.newInstance();
        reviewFragment.setMyUserId(userId);
        reviewFragment.setUsername(userName);
        pushFragment(reviewFragment);
    }

    @Override
    public void openFilterUserFragment() {
        FilterUserFragment filterUserFragment = FilterUserFragment.newInstance();
        FragNavTransactionOptions fragNavTransactionOptions = FragNavTransactionOptions.newBuilder()
                .transition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .customAnimations(R.anim.enter_from_right, R.anim.exit_to_left)
                .build();
        pushFragment(filterUserFragment, fragNavTransactionOptions);
    }

    @Override
    public UserFilterData getUserFilterData() {
        return userFilterData;
    }

    @Override
    public void openAddBankDetailFragment(int currentBalance) {
        AddBankDetailFragment addBankDetailFragment = AddBankDetailFragment.newInstance();
        addBankDetailFragment.setCurrentBalance(currentBalance);
        pushFragment(addBankDetailFragment);
    }

    @Override
    public void openReportAnIssueFragment(Stock detail) {
        ReportAnIssueFragment reportAnIssueFragment = ReportAnIssueFragment.newInstance();
        reportAnIssueFragment.setStock(detail);
        pushFragment(reportAnIssueFragment);
    }

    public void cancelPurchase(final int id) {
        mProgressBarDialog.showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.canelPurchase("Bearer " + getAccessToken(), "" + id).enqueue(new Callback<ApiResponse<Boolean>>() {
            @Override
            public void onResponse(Call<ApiResponse<Boolean>> call, Response<ApiResponse<Boolean>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    if (fragment instanceof PurchasesFragment) {
                        PurchasesFragment purchasesFragment = (PurchasesFragment) fragment;
                        purchasesFragment.getMyPurchases(0);
                    } else {
                        PurchaseDetailFragment.isReloadRequired = true;
                        MyPurchasesFragment.isReloadRequired = true;
                        popFragment();
                    }
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            cancelPurchase(id);
                        }

                        @Override
                        public void errorAccessToken() {
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(HomeActivity.this, response.errorBody());
                    mAlertDialog.showDialog(HomeActivity.this, message);
                }
                mProgressBarDialog.hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Boolean>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mAlertDialog.showDialog(HomeActivity.this, getString(R.string.error_server));
            }
        });
    }


    public void emailShippingLabel(final int saleId, final String listingTitle) {
        mProgressBarDialog.showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.emailShippingLabel("Bearer " + getAccessToken(), "" + saleId).enqueue(new Callback<ApiResponse<String>>() {
            @Override
            public void onResponse(Call<ApiResponse<String>> call, Response<ApiResponse<String>> response) {
                mProgressBarDialog.hideProgress();
                if (response.isSuccessful() && response.code() == 201) {
                    String title = Utils.getColorText(HomeActivity.this, listingTitle);
                    String message = ("All set! We’ve emailed you a shipping label for \"" + title + "\". Please ship at your earliest convenience to avoid negative feedback from the buyer.");
                    DialogAlert.showSuccessfulDialog(HomeActivity.this, message);
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    if (fragment instanceof PurchaseDetailFragment) {
                        PurchaseDetailFragment purchaseDetailFragment = (PurchaseDetailFragment) fragment;
                        purchaseDetailFragment.changeDetailStatus();
                    }
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            emailShippingLabel(saleId, listingTitle);
                        }

                        @Override
                        public void errorAccessToken() {
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(HomeActivity.this, response.errorBody());
                    mAlertDialog.showDialog(HomeActivity.this, message);
                }
                mProgressBarDialog.hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<String>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mAlertDialog.showDialog(HomeActivity.this, getString(R.string.error_server));
            }
        });
    }

    @Override
    public void checkReportAnIssue(final Stock detail) {
        mProgressBarDialog.showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        int id;
        if (detail instanceof PurchasesDetail) {
            id = ((PurchasesDetail) detail).getPurchaseId();
        } else {
            id = ((SalesDetail) detail).getSaleId();
        }
        apiInstance.checkReportAnIssue("Bearer " + getAccessToken(), "" + id).enqueue(new Callback<ApiResponse<ListingCheck>>() {
            @Override
            public void onResponse(Call<ApiResponse<ListingCheck>> call, Response<ApiResponse<ListingCheck>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mProgressBarDialog.hideProgress();
                    ListingCheck status = response.body().getData();
                    if (status.isError()) {
                        mAlertDialog.showDialog(HomeActivity.this, status.getErrorMessage());
                    } else {
                        openReportAnIssueFragment(detail);
                    }
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            checkReportAnIssue(detail);
                        }

                        @Override
                        public void errorAccessToken() {
                            mProgressBarDialog.hideProgress();
                        }
                    });
                } else {
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<ListingCheck>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void openDisputeDetailFragment(int disputeId, Stock detail) {
        DisputeDetailFragment disputeDetailFragment = DisputeDetailFragment.newInstance();
        disputeDetailFragment.setDisputeId(disputeId);
        disputeDetailFragment.setStock(detail);
        pushFragment(disputeDetailFragment);
    }

    @Override
    public void openWithDrawFragment(int currentBalance, List<BankAccount> payouts) {
        WithdrawFragment withdrawFragment = WithdrawFragment.newInstance();
        withdrawFragment.setCurrentBalance(currentBalance);
        withdrawFragment.setList(payouts);
        pushFragment(withdrawFragment);
    }

    public void openTransactionHistoryScreen() {
        BalanceHistoryFragment balanceHistoryFragment = BalanceHistoryFragment.newInstance();
        pushFragment(balanceHistoryFragment);
    }

    @Override
    public void updateSaleStats(MySalesStats myStats) {
        this.mySalesStats = myStats;
    }

    @Override
    public void updatePurchaseStats(MyPurchasesStats myStats) {
        this.myPurchasesStats = myStats;
    }

    @Override
    public void sendRating(final Stock detail, final int position) {
        mProgressBarDialog.showProgress();
        int id;
        if (detail instanceof PurchasesDetail) {
            PurchasesDetail purchasesDetail = (PurchasesDetail) detail;
            id = purchasesDetail.getPurchaseId();
            sendPurchaseRating(id, detail, position);
        } else {
            SalesDetail salesDetail = (SalesDetail) detail;
            id = salesDetail.getSaleId();
            sendSalesRating(id, detail, position);
        }
    }

    private void sendPurchaseRating(int id, final Stock detail, final int position) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.sendPurchaseRating("Bearer " + getAccessToken(), "" + id, detail).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                setRatingResponse(response, detail, position);
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    private void setRatingResponse(Response<ApiResponse<ResponseBody>> response, final Stock detail, final int position) {
        if (response.isSuccessful() && response.code() == 201) {
            mProgressBarDialog.hideProgress();
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            if (fragment instanceof PurchasesFragment) {
                PurchasesFragment purchasesFragment = (PurchasesFragment) fragment;
                purchasesFragment.updateFeedBack(detail, position);
            } else if (fragment instanceof PurchaseDetailFragment) {
                PurchaseDetailFragment purchasesFragment = (PurchaseDetailFragment) fragment;
                purchasesFragment.updateDetail(detail);
            }

        } else if (response.code() == 401) {
            getAccessTokenFromServer(new AccessTokenResponse() {
                @Override
                public void successAccessToken() {
                    sendRating(detail, position);
                }

                @Override
                public void errorAccessToken() {
                    mProgressBarDialog.hideProgress();
                }
            });
        } else {
            mProgressBarDialog.hideProgress();
            mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
        }
    }

    private void sendSalesRating(int id, final Stock detail, final int position) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.sendSaleRating("Bearer " + getAccessToken(), "" + id, detail).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                setRatingResponse(response, detail, position);
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void getTrackInformation(final Stock detail) {
        int id;
        if (detail instanceof PurchasesDetail) {
            PurchasesDetail purchasesDetail = (PurchasesDetail) detail;
            id = purchasesDetail.getPurchaseId();
        } else {
            SalesDetail salesDetail = (SalesDetail) detail;
            id = salesDetail.getSaleId();
        }
        mProgressBarDialog.showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.trackInformation("Bearer " + getAccessToken(), "" + id).enqueue(new Callback<ApiResponse<Tracking>>() {
            @Override
            public void onResponse(Call<ApiResponse<Tracking>> call, Response<ApiResponse<Tracking>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mProgressBarDialog.hideProgress();
                    Tracking tracking = response.body().getData();
                    TrackingDialog trackingDialog = new TrackingDialog();
                    trackingDialog.showDialog(HomeActivity.this, detail, tracking);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getTrackInformation(detail);
                        }

                        @Override
                        public void errorAccessToken() {
                            mProgressBarDialog.hideProgress();
                        }
                    });
                } else {
                    mProgressBarDialog.hideProgress();
                    TrackingDialog trackingDialog = new TrackingDialog();
                    trackingDialog.showDialog(HomeActivity.this, detail, null);
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Tracking>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void openCategoryFragment(int categoryId, int categoryTypeId) {
        CategoryFragment categoryFragment = CategoryFragment.newInstance();
        categoryFragment.setCategoryId(categoryId, categoryTypeId);
        pushFragment(categoryFragment);
    }

    @Override
    public void openColorFragment(List<Integer> colorIdsList) {
        ColorFragment colorFragment = ColorFragment.newInstance();
        colorFragment.setColorIdsList(colorIdsList);
        pushFragment(colorFragment);
    }

    @Override
    public void openSizeFragment(int sizeTypeId, int sizeId, String sizeHeader) {
        SizeMainFragment sizeMainFragment = SizeMainFragment.newInstance();
        sizeMainFragment.setSizeHeader(sizeTypeId, sizeId, sizeHeader);
        pushFragment(sizeMainFragment);
    }

    @Override
    public void openBrandFragment(int brandId) {
        BrandFragment brandFragment = BrandFragment.newInstance();
        brandFragment.setBrandId(brandId);
        pushFragment(brandFragment);
    }

    @Override
    public void getCategories() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getCategory("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<List<CategoryType>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<CategoryType>>> call, Response<ApiResponse<List<CategoryType>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    setCategoryResponse(response);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getCategories();
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    mBinding.progressBar.setVisibility(View.GONE);
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<CategoryType>>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));

            }
        });
    }

    private void setColorResponse(Response<ApiResponse<List<Colors>>> response) {
        mBinding.progressBar.setVisibility(View.GONE);
        List<Colors> colorList = response.body().getData();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof ColorFragment) {
            ColorFragment colorFragment = (ColorFragment) fragment;
            colorFragment.updateList(colorList);
        }
        if (fragment instanceof FilterFragment) {
            FilterFragment filterFragment = (FilterFragment) fragment;
            filterFragment.setColorList(colorList);
        }
    }

    private void setBrandResponse(Response<ApiResponse<BrandResponse>> response) {
        mBinding.progressBar.setVisibility(View.GONE);
        BrandResponse brandResponse = response.body().getData();
        List<Brand> brandList = brandResponse.getItems();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof BrandFragment) {
            BrandFragment brandFragment = (BrandFragment) fragment;
            brandFragment.updateList(brandList);
        } else if (fragment instanceof FilterFragment) {
            FilterFragment filterFragment = (FilterFragment) fragment;
            filterFragment.setBrandList(brandList);
        } else if (fragment instanceof FilterUserFragment) {
            FilterUserFragment filterUserFragment = (FilterUserFragment) fragment;
            filterUserFragment.setBrandList(brandList);
        }
    }

    @Override
    public void getColors() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getColors("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<List<Colors>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Colors>>> call, Response<ApiResponse<List<Colors>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    setColorResponse(response);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getColors();
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    mBinding.progressBar.setVisibility(View.GONE);
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<Colors>>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void getBrands() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getBrandsListing("Bearer " + getAccessToken(), 10000).enqueue(new Callback<ApiResponse<BrandResponse>>() {
            @Override
            public void onResponse(Call<ApiResponse<BrandResponse>> call, Response<ApiResponse<BrandResponse>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    setBrandResponse(response);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getBrands();
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    mBinding.progressBar.setVisibility(View.GONE);
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<BrandResponse>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void setCategoryInListingDetail(Category category, CategoryType categoryType) {
        this.listingDetail.setCategory(category);
        this.listingDetail.setCategoryType(categoryType);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment != null && fragment instanceof AddListingDetailFragment) {
            AddListingDetailFragment addListingDetailFragment = (AddListingDetailFragment) fragment;
            addListingDetailFragment.moveScrollDown();
        } else if (fragment != null && fragment instanceof EditMyListingFragment) {
            EditMyListingFragment editMyListingFragment = (EditMyListingFragment) fragment;
            editMyListingFragment.moveScrollDown();
        }

    }

    @Override
    public void setSizeInListingDetail(Size size, SizeType sizeType, String sizeHeader) {
        this.listingDetail.setSize(size);
        this.listingDetail.setSizeType(sizeType);
        this.listingDetail.setSizeHeader(sizeHeader);
    }

    @Override
    public void setColorInListingDetail(List<Colors> colorsList) {
        listingDetail.setColors(colorsList);
    }

    @Override
    public void setBrandInListingDetail(Brand brand) {
        this.listingDetail.setBrand(brand);
    }

    @Override
    public void sendListingDetail(final ListingDetail listingDetail, final List<ListingDisplayImage> photoUrlList) {
        final SavingListingDialog loading = new SavingListingDialog(this);
        loading.showDialog();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.sendListingData("Bearer " + getAccessToken(), listingDetail).enqueue(new Callback<ApiResponse<Integer>>() {
            @Override
            public void onResponse(Call<ApiResponse<Integer>> call, Response<ApiResponse<Integer>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    final int listingId = response.body().getData();
                    sendMyListingImage(listingId, photoUrlList, loading, 0, 0);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            sendListingDetail(listingDetail, photoUrlList);
                        }

                        @Override
                        public void errorAccessToken() {
                            loading.dismiss();
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(HomeActivity.this, response.errorBody());
                    mBinding.progressBar.setVisibility(View.GONE);
                    loading.dismiss();
                    mAlertDialog.showDialog(HomeActivity.this, message);
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Integer>> call, Throwable t) {
                loading.dismiss();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public ListingDetail listingDetail() {
        return listingDetail;
    }

    @Override
    public void clearListDetail() {
        listingDetail = null;
        listingDetail = new ListingDetail();
    }

    @Override
    public void openShippingFragment(ListingDetail listingDetail, List<ListingDisplayImage> photoUrlList) {
        ShippingFragment shippingFragment = ShippingFragment.newInstance();
        shippingFragment.setListingData(listingDetail, photoUrlList);
        pushFragment(shippingFragment);
    }

    @Override
    public void setSizesInMainSizesFragment(SizeType sizeType, Size size) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof SizeMainFragment) {
            SizeMainFragment sizeMainFragment = (SizeMainFragment) fragment;
            sizeMainFragment.updateSizes(sizeType, size);
        }
    }

    @Override
    public void updateListingImages(int position) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof CreateListingFragment) {
            CreateListingFragment createListingFragment = (CreateListingFragment) fragment;
            createListingFragment.removeItem(position);
        }
    }

    @Override
    public void setFilterData(List<SelectedFilter> mSelectedFilterList, int fragmentType) {
        filterData = new FilterData();
        filterFragmentType = fragmentType;
        List<Integer> conditionIds = new ArrayList<>();
        List<Integer> sizeIds = new ArrayList<>();
        List<Integer> colorIds = new ArrayList<>();
        List<Integer> categoriesIds = new ArrayList<>();
        List<Integer> brandsIds = new ArrayList<>();
        this.mSelectedFilterList = mSelectedFilterList;
        for (int i = 0; i < mSelectedFilterList.size(); i++) {
            if (mSelectedFilterList.get(i).getType() == Constants.CONDITION_FILTER) {
                if (mSelectedFilterList.get(i).getId() == 3) {
                    conditionIds.add(1);
                    conditionIds.add(2);
                } else {
                    conditionIds.add(mSelectedFilterList.get(i).getId());
                }
                filterData.setConditionIds(conditionIds);
            } else if (mSelectedFilterList.get(i).getType() == Constants.SIZE_FILTER) {
                sizeIds.add(mSelectedFilterList.get(i).getId());
                filterData.setSizeIds(sizeIds);
            } else if (mSelectedFilterList.get(i).getType() == Constants.COLOR_FILTER) {
                colorIds.add(mSelectedFilterList.get(i).getId());
                filterData.setColorIds(colorIds);
            } else if (mSelectedFilterList.get(i).getType() == Constants.CATEGORY_FILTER) {
                if (mSelectedFilterList.get(i).getId() == 0) {
                    categoriesIds.addAll(mSelectedFilterList.get(i).getIdsList());
                } else {
                    categoriesIds.add(mSelectedFilterList.get(i).getId());
                }
                filterData.setCategoryIds(categoriesIds);
            } else if (mSelectedFilterList.get(i).getType() == Constants.BRAND_FILTER) {
                brandsIds.add(mSelectedFilterList.get(i).getId());
                filterData.setBrandIds(brandsIds);
            } else if (mSelectedFilterList.get(i).getType() == Constants.PRICE_FILTER) {
                if (mSelectedFilterList.get(i).getId() == 1) {
                    filterData.setCustomMinPrice(0);
                    filterData.setCustomMaxPrice(20);
                } else if (mSelectedFilterList.get(i).getId() == 2) {
                    filterData.setCustomMinPrice(21);
                    filterData.setCustomMaxPrice(50);
                } else if (mSelectedFilterList.get(i).getId() == 3) {
                    filterData.setCustomMinPrice(51);
                    filterData.setCustomMaxPrice(100);
                } else if (mSelectedFilterList.get(i).getId() == 4) {
                    filterData.setCustomMinPrice(100);
                    filterData.setCustomMaxPrice(200);
                } else if (mSelectedFilterList.get(i).getId() == 5) {
                    filterData.setCustomMinPrice(200);
                    filterData.setCustomMaxPrice(500);
                }
            }
        }
        List<Fragment> allFragments = mNavController.getCurrentStack();
        for (Fragment fragment : allFragments) {
            if (fragment instanceof FeedsFragment) {
                FeedsFragment feedsFragment = (FeedsFragment) fragment;
                feedsFragment.clearListingList();
            }
            if (fragment instanceof BrowseFragment) {
                BrowseFragment browseFragment = (BrowseFragment) fragment;
                browseFragment.clearListingList();
            }
            if (fragment instanceof ProfileFragment) {
                ProfileFragment profileFragment = (ProfileFragment) fragment;
                profileFragment.clearBynList();
            }
            if (fragment instanceof MyLikesFragment) {
                MyLikesFragment myLikesFragment = (MyLikesFragment) fragment;
                myLikesFragment.clearBynList();
            }
            if (fragment instanceof MyBynProfileFragment) {
                MyBynProfileFragment myBynProfileFragment = (MyBynProfileFragment) fragment;
                myBynProfileFragment.clearBynList();
            }
        }
    }

    @Override
    public List<SelectedFilter> getSelectedFilterList() {
        return mSelectedFilterList;
    }

    @Override
    public List<SelectedFilter> getSelectedUserFilterList() {
        return mSelectedUserFilterList;
    }

    private void sendMyListingImage(final int listingId, final List<ListingDisplayImage> mPhotoList, final SavingListingDialog loading, final int currentUploadIndex, final int type) {
        List<String> photoUrlList = new ArrayList<>();
        for (int i = 0; i < mPhotoList.size(); i++) {
            if (mPhotoList.get(i).getType() == Constants.LOCAL_PHOTO) {
                photoUrlList.add(mPhotoList.get(i).getImageUrl());
            }
        }
        ApiEndpointInterface apiInstance = ApiService.instance();
        List<MultipartBody.Part> parts = new ArrayList<>();
        for (int i = 0; i < photoUrlList.size(); i++) {
            MultipartBody.Part part = prepareFilePart("image", photoUrlList.get(i), Constants.IMAGE_MEDIA_TYPE, "jpg");
            parts.add(part);
        }
        if (parts.size() == 0) {
            if (type == Constants.EDIT_LISTING) {
                loading.dismiss();
                deletedListingImagesId.clear();
                mListingCroppedList.clear();
                popFragment();
                return;
            }
        }

        apiInstance.sendListingImage("Bearer " + getAccessToken(), "" + listingId, parts).enqueue(new Callback<ApiResponse<List<String>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<String>>> call, Response<ApiResponse<List<String>>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    Log.d("msg", "" + response.body().getData().get(0));
                    loading.dismiss();
                    deletedListingImagesId.clear();
                    mListingCroppedList.clear();
                    onBackPressed();
                    if (type != Constants.EDIT_LISTING) {
                        popFragment();
                    }
//                    popFragment();
                    openMyBynFragment();
                    clearFilterList();
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            sendMyListingImage(listingId, mPhotoList, loading, 0, type);
                        }

                        @Override
                        public void errorAccessToken() {
                            loading.dismiss();
                        }
                    });
                } else {
                    loading.dismiss();
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<String>>> call, Throwable t) {
                loading.dismiss();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    private void setCategoryResponse(Response<ApiResponse<List<CategoryType>>> response) {
        mBinding.progressBar.setVisibility(View.GONE);
        List<CategoryType> categoryType = response.body().getData();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment != null && fragment instanceof CategoryFragment) {
            CategoryFragment categoryFragment = (CategoryFragment) fragment;
            categoryFragment.updateList(categoryType);
        }
        if (fragment != null && fragment instanceof FilterFragment) {
            FilterFragment filterFragment = (FilterFragment) fragment;
            filterFragment.setCategoryTypeList(categoryType);
        }
    }

    private void sendMyPostImage(final int postId, final SavingPostDialog loading, final String photoUrl, final int type) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        String image;
        if (type == Constants.EDIT_POST) {
            image = photoUrl;
        } else {
            image = mPostImage;
        }
        MultipartBody.Part parts = prepareFilePart("image", image, Constants.IMAGE_MEDIA_TYPE, "jpg");
        apiInstance.sendPostImage("Bearer " + getAccessToken(), "" + postId, parts).enqueue(new Callback<ApiResponse<List<UserResponse>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<UserResponse>>> call, Response<ApiResponse<List<UserResponse>>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    loading.dismiss();
                    mNavController.clearStack();
                    openMyPostFragment();
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            sendMyPostImage(postId, loading, photoUrl, type);
                        }

                        @Override
                        public void errorAccessToken() {
                            loading.dismiss();
                        }
                    });
                } else {
                    loading.dismiss();
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<UserResponse>>> call, Throwable t) {
                loading.dismiss();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void openCreateAccountDialog() {
        CreateAccountDialog createAccountDialog = new CreateAccountDialog();
        createAccountDialog.showDialog(this, this);
    }

    @Override
    public void openCreateAccountWithEmailDialog() {
        CreateAccountFormDialog createAccountFormDialog = new CreateAccountFormDialog();
        createAccountFormDialog.showDialog(this, this);
    }

    @Override
    public void addComment(final int id, final String comment) {
        mProgressBarDialog.showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.addComment("Bearer " + getAccessToken(), "" + id, comment).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    getComments(id);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            addComment(id, comment);
                        }

                        @Override
                        public void errorAccessToken() {
                            mProgressBarDialog.hideProgress();
                        }
                    });
                } else {
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));

            }
        });
    }

    private void getComments(final int id) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.listingDetailById("Bearer " + getAccessToken(), "" + id).enqueue(new Callback<ApiResponse<ListingDetail>>() {
            @Override
            public void onResponse(Call<ApiResponse<ListingDetail>> call, Response<ApiResponse<ListingDetail>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mProgressBarDialog.hideProgress();
                    ListingDetail listingDetail = response.body().getData();
                    List<Comments> comments = listingDetail.getComments();
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    if (fragment != null && fragment instanceof ListingDetailFragment) {
                        ListingDetailFragment listingDetailFragment = (ListingDetailFragment) fragment;
                        listingDetailFragment.setComments(comments);
                    }
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {

                            getComments(id);
                        }

                        @Override
                        public void errorAccessToken() {
                            mProgressBarDialog.hideProgress();
                        }
                    });
                } else {
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<ListingDetail>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void clearFilterList() {
        filterData = new FilterData();
        mSelectedFilterList.clear();
    }

    @Override
    public void openPostDetailFragmentWithId(int postId) {
        PostDetailFragment postDetailFragment = PostDetailFragment.newInstance();
        postDetailFragment.setPostId(postId);
        pushFragment(postDetailFragment);
    }

    @Override
    public void putEditListing(final int listingId, final ListingDetail listingDetail) {
        final SavingListingDialog loading = new SavingListingDialog(this);
        loading.showDialog();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.updateListingDetailById("Bearer " + getAccessToken(), "" + listingId, listingDetail).enqueue(new Callback<ApiResponse<ResponseBody>>() {

            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    HomeActivity.this.listingDetail = null;
                    loading.dismiss();
                    onBackPressed();
                    openMyBynFragment();
                    clearFilterList();

                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {

                            putEditListing(listingId, listingDetail);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                            loading.dismiss();
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(HomeActivity.this, response.errorBody());
                    mBinding.progressBar.setVisibility(View.GONE);
                    loading.dismiss();
                    mAlertDialog.showDialog(HomeActivity.this, message);
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                loading.dismiss();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    private void deleteListingImages(final int listingId, final List<ListingDisplayImage> photoList, final SavingListingDialog loading, int i) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.deleteListingImages("Bearer " + getAccessToken(), "" + listingId, deletedListingImagesId).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    deletedListingImagesId.clear();
                    if (photoList.size() > 0) {
                        sendMyListingImage(listingId, photoList, loading, 0, Constants.EDIT_LISTING);
                    } else {
                        HomeActivity.this.listingDetail = null;
                        loading.dismiss();
                        onBackPressed();
                        onBackPressed();
                        clearFilterList();
                        openMyBynFragment();
                    }
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            deleteListingImages(listingId, photoList, loading, 0);
                        }

                        @Override
                        public void errorAccessToken() {
                            loading.dismiss();
                        }
                    });
                } else {
                    loading.dismiss();
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                loading.dismiss();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void addDeletedImagesId(int listingImageId) {
        deletedListingImagesId.add(listingImageId);
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof EditListingFragment) {
            EditListingFragment editListingFragment = (EditListingFragment) fragment;
            editListingFragment.updateUI();
        }
    }

    @Override
    public void openMakeAnOfferFragment(int listingId, int offerId, int buyerId) {
        MakeAnOfferFragment makeAnOfferFragment = MakeAnOfferFragment.newInstance();
        makeAnOfferFragment.setListingId(listingId, offerId, buyerId);
        pushFragment(makeAnOfferFragment);
    }

    public void getPostTags(int id) {
        mProgressBarDialog.showProgress();
        getPostTags(-1, id, -1);
    }

    @Override
    public void openPostDetailFragment(FeedResponse postDetail) {
        PostDetailFragment postDetailFragment = PostDetailFragment.newInstance();
        postDetailFragment.setPostDetail(postDetail);
        pushFragment(postDetailFragment);
    }

    @Override
    public void openSearchUserFragment() {
        SearchUserFragment searchUserFragment = SearchUserFragment.newInstance();
        mSelectedHeightModel = new HeightModel();
        mSelectedUserFilterList = new ArrayList<>();
        pushFragment(searchUserFragment);
    }

    @Override
    public void getPostTags(final int position, final int id, final int feedType) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getPostTags("Bearer " + getAccessToken(), "" + id).enqueue(new Callback<ApiResponse<List<AnnotatedPoint>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<AnnotatedPoint>>> call, Response<ApiResponse<List<AnnotatedPoint>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    List<AnnotatedPoint> mList = response.body().getData();
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    if (position == -1) {
                        if (fragment instanceof DotsDetailFragment) {
                            mProgressBarDialog.hideProgress();
                            DotsDetailFragment dotsDetailFragment = (DotsDetailFragment) fragment;
                            dotsDetailFragment.setPostDots(mList);
                        }
                    } else {
                        if (fragment != null && fragment instanceof FeedsFragment) {
                            FeedsFragment feedFragment = (FeedsFragment) fragment;
                            feedFragment.setPostDotsTags(feedType, position, mList);
                        } else if (fragment != null && fragment instanceof BrowseFragment) {
                            BrowseFragment browseFragment = (BrowseFragment) fragment;
                            browseFragment.setPostDotsTags(feedType, position, mList);
                        } else if (fragment != null && fragment instanceof PostDetailFragment) {
                            PostDetailFragment postDetailFragment = (PostDetailFragment) fragment;
                            postDetailFragment.setPostDotsTags(mList);
                        } else if (fragment != null && fragment instanceof EditPostFragment) {
                            EditPostFragment editPostFragment = (EditPostFragment) fragment;
                            editPostFragment.setPostDotsTags(mList, position);
                        } else if (fragment != null && fragment instanceof ProfileFragment) {
                            ProfileFragment profileFragment = (ProfileFragment) fragment;
                            profileFragment.setPostDotsTags(mList, position);
                        } else if (fragment != null && fragment instanceof MyLikesFragment) {
                            MyLikesFragment myLikesFragment = (MyLikesFragment) fragment;
                            myLikesFragment.setPostDotsTags(mList, position);
                        }
                    }
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getPostTags(position, id, feedType);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                            mProgressBarDialog.hideProgress();
                        }
                    });
                } else {
                    mBinding.progressBar.setVisibility(View.GONE);
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<AnnotatedPoint>>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mBinding.progressBar.setVisibility(View.GONE);
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void openOfferCompletionFragment(ListingDetail listingDetail, int type) {
        OfferCompletionFragment offerCompletionFragment = OfferCompletionFragment.newInstance();
        offerCompletionFragment.setListingDetail(listingDetail);
        offerCompletionFragment.setType(type);
        pushFragment(offerCompletionFragment);
    }

    @Override
    public void getDefaultCard() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getDefaultCard("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<CardDetail>>() {
            @Override
            public void onResponse(Call<ApiResponse<CardDetail>> call, Response<ApiResponse<CardDetail>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    CardDetail cardDetail = response.body().getData();
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    if (fragment instanceof SellerAccountSetupFragment) {
                        getDefaultReturnAddress(cardDetail);
                    } else {
                        getDefaultAddress(cardDetail);
                    }
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getDefaultCard();
                        }

                        @Override
                        public void errorAccessToken() {
                            mProgressBarDialog.hideProgress();
                        }
                    });
                } else if (response.code() == 404) {
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    if (fragment instanceof OfferCompletionFragment) {
                        OfferCompletionFragment offerCompletionFragment = (OfferCompletionFragment) fragment;
                        offerCompletionFragment.error();
                    } else if (fragment instanceof SellerAccountSetupFragment) {
                        getDefaultReturnAddress(null);
                    }
                } else {
                    String message = Utils.ErrorMessage(HomeActivity.this, response.errorBody());
                    mAlertDialog.showDialog(HomeActivity.this, message);
                }
                mProgressBarDialog.hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<CardDetail>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });

    }

    @Override
    public void openSelectDifferentPaymentFragment(CardDetail selectedPayment, SelectNewPaymentListener mCallBack) {
        SelectDifferentPaymentFragment selectDifferentPaymentFragment = SelectDifferentPaymentFragment.newInstance();
        selectDifferentPaymentFragment.setDefaultPayment(selectedPayment);
        selectDifferentPaymentFragment.setListener(mCallBack);
        pushFragment(selectDifferentPaymentFragment);
    }

    @Override
    public void openSelectDifferentAddressFragment(Address selectedAddress, SelectExistingAddressListener mCallBack) {
        SelectDifferentAddressFragment selectDifferentAddressFragment = SelectDifferentAddressFragment.newInstance();
        selectDifferentAddressFragment.setListener(mCallBack);
        selectDifferentAddressFragment.setDefaultAddress(selectedAddress);
        pushFragment(selectDifferentAddressFragment);
    }

    @Override
    public void openNotificationFragment() {
        mBinding.bottomBar.setSelectedItemId(R.id.navigation_alert_tab);
    }

    @Override
    public void updateListingImages(int listingId, List<ListingDisplayImage> mPhotoList) {
        final SavingListingDialog loading = new SavingListingDialog(this);
        loading.showDialog();
        if (deletedListingImagesId.size() > 0) {
            deleteListingImages(listingId, mPhotoList, loading, 0);
        } else {
            if (mPhotoList.size() > 0) {
                sendMyListingImage(listingId, mPhotoList, loading, 0, Constants.EDIT_LISTING);
            } else {
                HomeActivity.this.listingDetail = null;
                loading.dismiss();
                onBackPressed();
                openMyBynFragment();
                clearFilterList();
            }

        }
    }

    @Override
    public void openSellerPolicyFragment() {
        SellerPolicyFragment sellerPolicyFragment = SellerPolicyFragment.newInstance();
        pushFragment(sellerPolicyFragment);
    }

    @Override
    public void openReportAnIssueListingFragment(ListingDetail listingDetail) {
        ReportAnIssueListingFragment reportAnIssueListingFragment = ReportAnIssueListingFragment.newInstance();
        reportAnIssueListingFragment.setListing(listingDetail);
        pushFragment(reportAnIssueListingFragment);
    }

    @Override
    public void clearAllFragment() {
        mNavController.clearStack();
    }


    @Override
    public void getDefaultReturnAddress(final CardDetail cardDetail) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getDefaultReturnAddress("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<Address>>() {
            @Override
            public void onResponse(Call<ApiResponse<Address>> call, Response<ApiResponse<Address>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Address address = response.body().getData();
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    if (fragment instanceof OfferCompletionFragment) {
                        OfferCompletionFragment offerCompletionFragment = (OfferCompletionFragment) fragment;
                        offerCompletionFragment.update(cardDetail, address);
                    } else if (fragment instanceof SellerAccountSetupFragment) {
                        SellerAccountSetupFragment sellerAccountSetupFragment = (SellerAccountSetupFragment) fragment;
                        sellerAccountSetupFragment.update(cardDetail, address);
                    }

                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getDefaultReturnAddress(cardDetail);
                        }

                        @Override
                        public void errorAccessToken() {
                            mProgressBarDialog.hideProgress();
                        }
                    });
                } else if (response.code() == 404) {
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    if (fragment != null && fragment instanceof OfferCompletionFragment) {
                        OfferCompletionFragment offerCompletionFragment = (OfferCompletionFragment) fragment;
                        offerCompletionFragment.error();
                    } else if (fragment instanceof SellerAccountSetupFragment) {
                        SellerAccountSetupFragment sellerAccountSetupFragment = (SellerAccountSetupFragment) fragment;
                        sellerAccountSetupFragment.update(cardDetail, null);
                    }
                } else {
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Address>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }

    public void getDefaultAddress(final CardDetail cardDetail) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getDefaultAddress("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<Address>>() {
            @Override
            public void onResponse(Call<ApiResponse<Address>> call, Response<ApiResponse<Address>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Address address = response.body().getData();
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    if (fragment != null && fragment instanceof OfferCompletionFragment) {
                        OfferCompletionFragment offerCompletionFragment = (OfferCompletionFragment) fragment;
                        offerCompletionFragment.update(cardDetail, address);
                    } else if (fragment != null && fragment instanceof SellerAccountSetupFragment) {
                        SellerAccountSetupFragment sellerAccountSetupFragment = (SellerAccountSetupFragment) fragment;
                        sellerAccountSetupFragment.update(cardDetail, address);
                    }
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getDefaultAddress(cardDetail);
                        }

                        @Override
                        public void errorAccessToken() {
                            mProgressBarDialog.hideProgress();
                        }
                    });
                } else if (response.code() == 404) {
                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    if (fragment != null && fragment instanceof OfferCompletionFragment) {
                        OfferCompletionFragment offerCompletionFragment = (OfferCompletionFragment) fragment;
                        offerCompletionFragment.error();
                    }
                } else {
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Address>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));

            }
        });
    }



    private class RefreshFeedsInFeedFragment extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            for (int i = 0; i < 5; i++) {
                List<Fragment> allFragments = mNavController.getStack(i);
                for (Fragment fragment : allFragments) {
                    if (fragment != null && fragment instanceof FeedsFragment) {
                        FeedsFragment feedsFragment = (FeedsFragment) fragment;
                        FeedResponse feeds = (FeedResponse) intent.getSerializableExtra(Constants.FEED);
                        feedsFragment.updateNewFeeds(feeds);
                    } else if (fragment != null && fragment instanceof BrowseFragment) {
                        BrowseFragment browseFragment = (BrowseFragment) fragment;
                        FeedResponse feeds = (FeedResponse) intent.getSerializableExtra(Constants.FEED);
                        browseFragment.updateNewFeeds(feeds);
                    } else if (fragment != null && fragment instanceof MyLikesFragment) {
                        MyLikesFragment likesFragment = (MyLikesFragment) fragment;
                        FeedResponse feeds = (FeedResponse) intent.getSerializableExtra(Constants.FEED);
                        likesFragment.updateItem(feeds);
                    }
                }
            }
        }
    }

    public void openSellerAccountSetupFragment() {
        SellerAccountSetupFragment sellerAccountSetupFragment = SellerAccountSetupFragment.newInstance();
        pushFragment(sellerAccountSetupFragment);
    }

    @Override
    public void openSelectExistingPaymentFragment(SelectNewPaymentListener mCallback, CardDetail cardDetail, boolean b) {
        SelectExistingPaymentFragment selectExistingPaymentFragment = SelectExistingPaymentFragment.newInstance();
        selectExistingPaymentFragment.setListener(mCallback);
        selectExistingPaymentFragment.updateSelectedCard(cardDetail);
        selectExistingPaymentFragment.showPaymentButton(b);
        pushFragment(selectExistingPaymentFragment);
    }

    @Override
    public void sendReportAnIssueImages(final int id, final List<String> mImageList) {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        List<MultipartBody.Part> parts = new ArrayList<>();
        for (int i = 0; i < mImageList.size(); i++) {
            MultipartBody.Part part = prepareFilePart("image", mImageList.get(i), Constants.IMAGE_MEDIA_TYPE, "jpg");
            parts.add(part);
        }
        apiInstance.reportAnIssueImages("Bearer " + getAccessToken(), "" + id, parts).enqueue(new Callback<ApiResponse<List<String>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<String>>> call, Response<ApiResponse<List<String>>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    Log.d("msg", "" + response.body().getData().get(0));
                    popFragment();
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            sendReportAnIssueImages(id, mImageList);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    mBinding.progressBar.setVisibility(View.GONE);
                    mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<String>>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                mAlertDialog.showDialog(HomeActivity.this, getResources().getString(R.string.error_server));
            }
        });
    }
}
