package com.bynmix.app.activities;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.bynmix.app.R;
import com.bynmix.app.databinding.ActivityInAppBinding;
import com.bynmix.app.utils.BaseActivity;
import com.bynmix.app.utils.Constants;


public class InAppActivity extends BaseActivity {
    ActivityInAppBinding mBinding;
    String postUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_in_app);
        postUrl = getIntent().getStringExtra(Constants.INTENT_EXTRA_URL);
        mBinding.webview.getSettings().setJavaScriptEnabled(true);
        mBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        openUrl();
    }

    private void openUrl() {
        mBinding.webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            public void onPageFinished(WebView view, String url) {
                mBinding.progressBar.setVisibility(View.GONE);

            }

            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(postUrl));
                if (i.resolveActivity(getPackageManager()) != null) {
                    startActivity(i);
                }
            }
        });

        mBinding.webview.loadUrl(postUrl);
    }

}
