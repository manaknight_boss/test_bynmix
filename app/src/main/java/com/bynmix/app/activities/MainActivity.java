package com.bynmix.app.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bynmix.app.R;
import com.bynmix.app.activities.createAccounts.AboutActivity;
import com.bynmix.app.activities.createAccounts.CreateAccountActivity;
import com.bynmix.app.activities.createAccounts.ForgotPasswordActivity;
import com.bynmix.app.activities.createAccounts.PrivacyPolicyActivity;
import com.bynmix.app.activities.createAccounts.TermsOfServicesActivity;
import com.bynmix.app.databinding.ActivityMainBinding;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;
import com.google.android.gms.auth.api.Auth;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MainActivity extends OAuthActivity {
    protected ApiEndpointInterface apiInstance;
    private ActivityMainBinding mBinding;
    private String username, password;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        generateHash();
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mBinding.termsOfServices.setPaintFlags(mBinding.termsOfServices.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mBinding.privacyPolicy.setPaintFlags(mBinding.privacyPolicy.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mBinding.about.setPaintFlags(mBinding.about.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mBinding.createAccount.setPaintFlags(mBinding.createAccount.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Utils.setCapitalizeTextWatcher(mBinding.mainInputTextUsername);
        init();
        initializeBackgroundVideo(config.PICK_CLOTHING_VIDEO_URI);
        initializeGoogleLogin();
        String accessToken = getAccessToken();

        int screenHeight = Utils.getScreenHeight(this);
        if(screenHeight < 2000) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(Utils.dpToPx(this,33), LinearLayout.LayoutParams.WRAP_CONTENT);
            mBinding.logo.setLayoutParams(params);
        }

        if (!TextUtils.isEmpty(accessToken)) {
            Intent intent = new Intent(MainActivity.this, HomeActivity.class);
            videoView.stopPlayback();
            startActivity(intent);
            finish();
        }
        mBinding.mainFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (conditionsForApiCall()) {
                    login_type = "Facebook";
                    initializeFacebookLogin(Constants.LOGIN);
                }
            }
        });

        mBinding.mainGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (conditionsForApiCall()) {
                    login_type = "Google";
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, RC_SIGN_IN);
                }
            }
        });

        mBinding.mainForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        mBinding.mainSigninButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = mBinding.mainInputTextUsername.getText().toString().trim();
                password = mBinding.mainInputTextPassword.getText().toString().trim();

                if (conditionsForApiCall()) {
                    if (!validate()) {
                        showToast(translateString(R.string.fail_signin));
                    } else {
                        sendSignInPost(username, password);
                    }
                }
            }
        });

        mBinding.mainMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CreateAccountActivity.class);
                videoView.stopPlayback();
                startActivity(intent);
            }
        });

        mBinding.termsOfServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TermsOfServicesActivity.class);
                videoView.stopPlayback();
                intent.putExtra("type", Constants.TERMS_OF_SERVICES);
                startActivity(intent);
            }
        });
        mBinding.privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), PrivacyPolicyActivity.class);
                videoView.stopPlayback();
                startActivity(intent);
            }
        });
        mBinding.about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AboutActivity.class);
                videoView.stopPlayback();
                startActivity(intent);
            }
        });
        apiInstance = ApiService.instance();
    }

    private void generateHash() {
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.bynmix.app", PackageManager.GET_SIGNATURES);

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("MY KEY HASH:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.start();
    }

    protected boolean validate() {
        boolean valid = true;

        if (username.isEmpty()) {
            mBinding.mainInputTextUsername.setError(translateString(R.string.error_username));
            valid = false;
        }

        if (password.isEmpty() || password.length() < 6) {
            mBinding.mainInputTextPassword.setError(translateString(R.string.error_password));
            valid = false;
        }
        return valid;
    }
}