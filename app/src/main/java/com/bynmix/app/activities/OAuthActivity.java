package com.bynmix.app.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import com.bynmix.app.R;
import com.bynmix.app.utils.BaseActivity;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.CustomTextDialogForLogin;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import org.json.JSONObject;

import java.util.Arrays;


public class OAuthActivity extends BaseActivity implements  GoogleApiClient.OnConnectionFailedListener {
    public int RC_SIGN_IN = 1;
    public String login_type = "none";
    protected GoogleApiClient mGoogleApiClient;
    private CallbackManager callbackManager;

    protected void initializeGoogleLogin() {
        callbackManager = CallbackManager.Factory.create();
        String serverClientId = getString(R.string.client_id);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(serverClientId)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (login_type.equalsIgnoreCase("Google")) {
            super.onActivityResult(requestCode, resultCode, data);
            callbackManager.onActivityResult(requestCode, resultCode, data);
            if (requestCode == RC_SIGN_IN) {
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                handleSignInResult(result);
            }
        }

        if (login_type.equalsIgnoreCase("Facebook")) {
            callbackManager.onActivityResult(requestCode, resultCode, data);
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    protected void handleSignInResult(GoogleSignInResult result) {
        if (result!=null && result.isSuccess()) {
            GoogleSignInAccount account = result.getSignInAccount();
            String email = account.getEmail();
            String token = account.getIdToken();
            String firstName = account.getGivenName();
            String lastName = account.getFamilyName();
            Log.d("SUCCESS Email", email);
            Log.d("Token", token);
            Log.d("First", firstName);
            Log.d("Last", lastName);
            if (conditionsForApiCall()) {
                signIn(email, firstName, lastName, token, login_type);
            }
        }
        else {
            CustomTextDialogForLogin alert = new CustomTextDialogForLogin();
            alert.showDialog(this, translateString(R.string.error_external_login));
        }
    }

    protected void initializeFacebookLogin(final int type) {
        LoginManager.getInstance().logOut();
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        final AccessToken accessToken = AccessToken.getCurrentAccessToken();
                        Log.d("SUCCESS", "FACEBOOK LOGIN WORKS " + accessToken.getToken());
                        GraphRequest.newGraphPathRequest(loginResult.getAccessToken(), "/me?fields=id,first_name,email,last_name", new GraphRequest.Callback() {
                            @Override
                            public void onCompleted(GraphResponse response) {
                                if (response.getError() != null) {
                                    showFacebookError(type);
                                } else {
                                    // get email and id of the user
                                    JSONObject me = response.getJSONObject();
                                    String email = me.optString("email");
                                    String firstName = me.optString("first_name");
                                    String lastName = me.optString("last_name");
                                    if (TextUtils.isEmpty(email)) {
                                        showFacebookError(type);
                                    } else {
                                        if (conditionsForApiCall()) {
                                            signIn(email, firstName, lastName, accessToken.getToken(), login_type);
                                        }

                                    }
                                }
                            }
                        }).executeAsync();
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException e) {
                        showFacebookError(type);
                    }
                });
    }

    private void showFacebookError(int type) {
        CustomTextDialogForLogin alert = new CustomTextDialogForLogin();
        if (type == Constants.LOGIN) {
            alert.showDialog(OAuthActivity.this, translateString(R.string.error_external_login));
        } else {
            alert.showDialog(OAuthActivity.this, translateString(R.string.error_external_login_facebook));
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        CustomTextDialogForLogin alert = new CustomTextDialogForLogin();
        alert.showDialog(this, translateString(R.string.error_external_login));
    }
}
