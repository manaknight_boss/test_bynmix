package com.bynmix.app.activities.createAccounts;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;

import com.bynmix.app.R;
import com.bynmix.app.activities.OAuthActivity;
import com.bynmix.app.databinding.ActivityCreateAccountBinding;
import com.bynmix.app.utils.Constants;
import com.google.android.gms.auth.api.Auth;

public class CreateAccountActivity extends OAuthActivity {
    private ActivityCreateAccountBinding mBinding;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_create_account);
        initializeBackgroundVideo(config.TRY_CLOTHING_VIDEO_URI);
        mBinding.loginHere.setPaintFlags(mBinding.loginHere.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        initializeGoogleLogin();

        mBinding.facebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (conditionsForApiCall()) {
                    login_type = "Facebook";
                    initializeFacebookLogin(Constants.CREATE_ACCOUNT);
                }
            }
        });

        mBinding.googlePlusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (conditionsForApiCall()) {
                    login_type = "Google";
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, RC_SIGN_IN);
                }
            }
        });

        mBinding.emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onCreateAccountClick(view);
            }
        });
        mBinding.mainMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void onCreateAccountClick(View v) {
        Intent intent = new Intent(getApplicationContext(), CreateAccountFormActivity.class);
        videoView.stopPlayback();
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.start();
    }
}