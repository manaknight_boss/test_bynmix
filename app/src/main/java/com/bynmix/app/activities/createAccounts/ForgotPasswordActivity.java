package com.bynmix.app.activities.createAccounts;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.activities.MainActivity;
import com.bynmix.app.databinding.ActivityForgotPasswordBinding;
import com.bynmix.app.models.UserResponse;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.BaseActivity;
import com.bynmix.app.utils.Utils;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends BaseActivity {
    protected ApiEndpointInterface apiInstance;
    private ActivityForgotPasswordBinding mBinding;
    private String email;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        initializeBackgroundVideo(config.PICK_CLOTHING_VIDEO_URI);
        apiInstance = ApiService.instance();
        mBinding.backToLogin.setPaintFlags(mBinding.backToLogin.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        mBinding.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email = mBinding.mainInputTextEmail.getText().toString().trim();

                if (conditionsForApiCall()) {
                    if (!validate()) {
                        showToast(translateString(R.string.fail_signin));
                    } else {
                        sendCode(email);
                    }
                }
            }
        });

        mBinding.backToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                videoView.stopPlayback();
                startActivity(intent);
            }
        });
        mBinding.haveCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), VerifyCodeActivity.class);
                videoView.stopPlayback();
                startActivity(intent);
            }
        });
    }

    private void sendCode(final String email) {
        mProgressBarDialog.showProgress();
        final UserResponse registerUser = new UserResponse();
        registerUser.setEmail(email);

        apiInstance.sendCode(registerUser).enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    mProgressBarDialog.hideProgress();
                    Toast.makeText(ForgotPasswordActivity.this, "Verification Code send to your register Email", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getApplicationContext(), VerifyCodeActivity.class);
                    intent.putExtra("email", email);
                    videoView.stopPlayback();
                    startActivity(intent);
                    finish();
                } else {
                    String message = Utils.ErrorMessage(ForgotPasswordActivity.this, response.errorBody());
                    mAlertDialog.showDialog(ForgotPasswordActivity.this, message);
                    mProgressBarDialog.hideProgress();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mAlertDialog.showDialog(ForgotPasswordActivity.this, getResources().getString(R.string.error_server));
                mProgressBarDialog.hideProgress();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.start();
    }

    protected boolean validate() {
        boolean valid = true;

        if (email.isEmpty()) {
            mBinding.mainInputTextEmail.setError(translateString(R.string.error_email));
            valid = false;
        }
        return valid;
    }
}