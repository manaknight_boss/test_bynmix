package com.bynmix.app.activities.createAccounts;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.databinding.ActivityResetPasswordBinding;
import com.bynmix.app.models.UserResponse;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.BaseActivity;
import com.bynmix.app.utils.ForgotPasswordDialog;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordActivity extends BaseActivity {
    protected ApiEndpointInterface apiInstance;
    private ActivityResetPasswordBinding mBinding;
    private String email, password, confirmPassword;
    private ForgotPasswordDialog mAlertDialog = new ForgotPasswordDialog();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_reset_password);
        initializeBackgroundVideo(config.PICK_CLOTHING_VIDEO_URI);
        apiInstance = ApiService.instance();
        email = getIntent().getStringExtra("email");


        mBinding.resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                password = mBinding.mainInputTextPassword.getText().toString().trim();
                confirmPassword = mBinding.mainInputTextConfirmPassword.getText().toString().trim();

                if (conditionsForApiCall()) {
                    if (!validate()) {
                        showToast(translateString(R.string.fail_signin));
                    } else {
                        mBinding.passwordNotMatched.setVisibility(View.GONE);
                        resetPassword(email, password);
                    }
                }
            }
        });
    }

    private void resetPassword(final String email, String password) {
        mProgressBarDialog.showProgress();
        final UserResponse registerUser = new UserResponse();
        registerUser.setEmail(email);
        registerUser.setPassword(password);

        apiInstance.resetPassword(registerUser).enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mProgressBarDialog.hideProgress();
                    Toast.makeText(ResetPasswordActivity.this, "Password reset", Toast.LENGTH_SHORT).show();
                    finish();
                    finish();
                } else {
                    mAlertDialog.showDialog(ResetPasswordActivity.this, getResources().getString(R.string.something_went_wrong), getResources().getString(R.string.not_reset_password));
                    mProgressBarDialog.hideProgress();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mAlertDialog.showDialog(ResetPasswordActivity.this, getResources().getString(R.string.error_server), "");
                mProgressBarDialog.hideProgress();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.start();
    }

    protected boolean validate() {
        boolean valid = true;
        if (password.isEmpty() || password.length() < 6) {
            mBinding.mainInputTextPassword.setError(translateString(R.string.error_password));
            valid = false;
        }
        if (confirmPassword.isEmpty() || !confirmPassword.equalsIgnoreCase(password)) {
            mBinding.passwordNotMatched.setVisibility(View.VISIBLE);
            valid = false;
        }
        return valid;
    }
}


