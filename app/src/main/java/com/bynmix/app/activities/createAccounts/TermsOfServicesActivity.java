package com.bynmix.app.activities.createAccounts;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.bynmix.app.R;
import com.bynmix.app.adapter.TermsOfServicesAdapter;
import com.bynmix.app.databinding.ActivityTermsOfServicesBinding;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Sections;
import com.bynmix.app.models.TermsOfServicesModel;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.BaseActivity;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TermsOfServicesActivity extends BaseActivity {
    private ActivityTermsOfServicesBinding mBinding;
    private TermsOfServicesAdapter mAdapter;
    private List<Sections> mList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_of_services);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_terms_of_services);
        mBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mBinding.header.setText("Terms of Service");
        setAdapter();
        callApi();
    }

    private void setAdapter() {
        mAdapter = new TermsOfServicesAdapter(this, mList);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    private void callApi() {
        callTermsOfServices();
    }

    private void callTermsOfServices() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        mProgressBarDialog.showProgress();
        apiInstance.getTermsOfServices().enqueue(new Callback<ApiResponse<TermsOfServicesModel>>() {
            @Override
            public void onResponse(Call<ApiResponse<TermsOfServicesModel>> call, Response<ApiResponse<TermsOfServicesModel>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    TermsOfServicesModel termsOfServicesModel = response.body().getData();
                    setUI(termsOfServicesModel);
                } else {
                    String message = Utils.ErrorMessage(TermsOfServicesActivity.this, response.errorBody());
                    mAlertDialog.showDialog(TermsOfServicesActivity.this, message);
                }
                mProgressBarDialog.hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<TermsOfServicesModel>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
            }
        });

    }

    private void setUI(TermsOfServicesModel termsOfServicesModel) {
        mList = termsOfServicesModel.getSections();
        mAdapter.setList(mList);
    }
}
