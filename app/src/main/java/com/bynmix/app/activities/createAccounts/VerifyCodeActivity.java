package com.bynmix.app.activities.createAccounts;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.databinding.ActivityVerifyCodeBinding;
import com.bynmix.app.models.UserResponse;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.BaseActivity;
import com.bynmix.app.utils.CustomTextDialog;
import com.bynmix.app.utils.ForgotPasswordDialog;
import com.bynmix.app.utils.Utils;
import com.goodiebag.pinview.Pinview;

import org.json.JSONArray;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyCodeActivity extends BaseActivity {
    protected ApiEndpointInterface apiInstance;
    private ActivityVerifyCodeBinding mBinding;
    private String email;
    private ForgotPasswordDialog mAlertDialog = new ForgotPasswordDialog();
    private CustomTextDialog mDialog = new CustomTextDialog();
    private String code;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_verify_code);
        initializeBackgroundVideo(config.PICK_CLOTHING_VIDEO_URI);
        apiInstance = ApiService.instance();
        email = getIntent().getStringExtra("email");
        if (!TextUtils.isEmpty(email)) {
            mBinding.resendEmail.setVisibility(View.VISIBLE);
            mBinding.mainInputTextEmail.setEnabled(false);
            mBinding.mainInputTextEmail.setText(email);
        } else {
            mBinding.mainInputTextEmail.setEnabled(true);
            mBinding.resendEmail.setVisibility(View.GONE);
        }
        mBinding.nextButton.setEnabled(false);

        int screenWidth = Utils.getScreenWidth(this);
        float pinMarginFromScreen = getResources().getDimension(R.dimen.pin_view_margin);
        int requiredPinViewWidth = screenWidth - (int) (pinMarginFromScreen * 2);
        int marginRequiredBetweenPin = Utils.dpToPx(this, 8);
        int pinTotalWidth = requiredPinViewWidth - (marginRequiredBetweenPin * 5);
        int sizeForOnePin = pinTotalWidth / 6;
        mBinding.txtPinEntry.setPinWidth(sizeForOnePin);
        mBinding.txtPinEntry.setPinHeight(sizeForOnePin);
        mBinding.txtPinEntry.setSplitWidth(marginRequiredBetweenPin);
        for (int i = 0; i < mBinding.txtPinEntry.getChildCount(); i++) {
            EditText pinEditText = (EditText) mBinding.txtPinEntry.getChildAt(i);
            pinEditText.setTextSize(Utils.dpToPx(this, 8));
            pinEditText.setTypeface(Typeface.createFromAsset(getAssets(), "font/raleway_semi_bold.ttf"));
        }

        if (mBinding.txtPinEntry != null) {


            mBinding.txtPinEntry.setPinViewEventListener(new Pinview.PinViewEventListener() {
                @Override
                public void onDataEntered(Pinview pinview, boolean fromUser) {
                    String str = pinview.getValue();
                    if (str.length() < 6) {
                        mBinding.nextButton.setEnabled(false);
                        mBinding.nextButton.setBackground(getResources().getDrawable(R.drawable.rectangle_disable_background));
                    } else {
                        mBinding.nextButton.setEnabled(true);
                        mBinding.nextButton.setBackground(getResources().getDrawable(R.drawable.purple_rounded_button));
                    }
                }
            });
        }

        mBinding.nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email = mBinding.mainInputTextEmail.getText().toString().trim();
                code = mBinding.txtPinEntry.getValue();
                if (conditionsForApiCall()) {
                    if (!validate()) {
                        showToast(translateString(R.string.fail_signin));
                    } else {
                        verifyCode(email, Integer.parseInt(code));
                    }
                }
            }
        });

        mBinding.resendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendEmail(email);
            }
        });
    }

    private void sendEmail(final String email) {
        mProgressBarDialog.showProgress();
        final UserResponse registerUser = new UserResponse();
        registerUser.setEmail(email);

        apiInstance.sendCode(registerUser).enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    mProgressBarDialog.hideProgress();
                    Toast.makeText(VerifyCodeActivity.this, "We have sent you a new verification code in your email. Please check it and re-enter it into the app", Toast.LENGTH_SHORT).show();
                } else if (response.code() == 404) {
                    try {
                        JSONObject errorObject = new JSONObject(response.errorBody().string().trim());
                        JSONArray error = errorObject.getJSONArray("errors");
                        JSONObject messageObject = error.getJSONObject(0);
                        String message = messageObject.getString("message");
                        mDialog.showDialog(VerifyCodeActivity.this, message);
                    } catch (Exception e) {
                        e.printStackTrace();
                        mDialog.showDialog(VerifyCodeActivity.this, getResources().getString(R.string.error_server));
                    }
                    mProgressBarDialog.hideProgress();
                } else {
                    mDialog.showDialog(VerifyCodeActivity.this, getResources().getString(R.string.error_server));
                    mProgressBarDialog.hideProgress();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mDialog.showDialog(VerifyCodeActivity.this, getResources().getString(R.string.error_server));
                mProgressBarDialog.hideProgress();

            }
        });
    }

    private void verifyCode(final String email, int code) {
        mProgressBarDialog.showProgress();
        final UserResponse registerUser = new UserResponse();
        registerUser.setEmail(email);
        registerUser.setCode(code);

        apiInstance.verifyCode(registerUser).enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mProgressBarDialog.hideProgress();
                    Intent intent = new Intent(VerifyCodeActivity.this, ResetPasswordActivity.class);
                    intent.putExtra("email", email);
                    startActivity(intent);
                    finish();
                } else {
                    mAlertDialog.showDialog(VerifyCodeActivity.this, getResources().getString(R.string.not_verify_email_and_code), getResources().getString(R.string.enter_email_and_code));
                    mProgressBarDialog.hideProgress();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mAlertDialog.showDialog(VerifyCodeActivity.this, getResources().getString(R.string.error_server), "");
                mProgressBarDialog.hideProgress();

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        videoView.start();
    }

    protected boolean validate() {
        boolean valid = true;

        if (email.isEmpty()) {
            mBinding.mainInputTextEmail.setError(translateString(R.string.error_email));
            valid = false;
        }
        if (code.isEmpty() || code.length() != 6) {
            Toast.makeText(this, "Enter 6 digit code", Toast.LENGTH_SHORT).show();
            valid = false;
        }
        return valid;
    }
}