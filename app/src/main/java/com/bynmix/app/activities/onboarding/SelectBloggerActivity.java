package com.bynmix.app.activities.onboarding;

import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.adapter.BloggerAdapter;
import com.bynmix.app.adapter.SelectedBlogAdapter;
import com.bynmix.app.databinding.ActivitySelectBlogBinding;
import com.bynmix.app.interfaces.BloggerListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Bloggers;
import com.bynmix.app.models.PostBloggerIds;
import com.bynmix.app.models.UserResponse;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.BaseActivity;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectBloggerActivity extends BaseActivity implements BloggerListener {

    private ActivitySelectBlogBinding mBinding;
    private List<UserResponse> mBloggerList = new ArrayList<>();
    private List<UserResponse> mSelectedBlogList = new ArrayList<>();
    private BloggerAdapter bloggerAdapter;
    private Bloggers mBlogger = new Bloggers();
    private SelectedBlogAdapter mAdapter;
    List<Integer> bloggerId = new ArrayList<>();
    private String searchBloggers;
    private boolean isAlreadyCalled;
    private int currentPageNumber;
    private int totalPages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_select_blog);
        init();
        getAllBloggers();
        setUIConfirmationButton();
        bloggerAdapter = new BloggerAdapter(this, mBloggerList, this);
        mBinding.recycleView.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recycleView.setAdapter(bloggerAdapter);
        mBinding.progressBar.getIndeterminateDrawable().setColorFilter(0xFFFF0000, android.graphics.PorterDuff.Mode.MULTIPLY);
        setProgressBarColors();
        mBinding.autoComplete.requestFocus();
        mBinding.autoComplete.setThreshold(1);
        mBinding.autoComplete.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable editable) {
                // TODO Auto-generated method stub

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newText = s.toString();
                Log.d(DEBUGTAG, String.valueOf(mBinding.autoComplete.isPerformingCompletion()));
                Log.d(DEBUGTAG, String.valueOf(mBinding.autoComplete.isPerformingCompletion()));
                if (newText.length() > 1) {
                    mBinding.progressBar.setVisibility(View.VISIBLE);
                    mBinding.searchIcon.setVisibility(View.GONE);
                    searchBloggers = newText;
                    getAllBloggers();
                } else {
                    searchBloggers = null;
                    getAllBloggers();
                }


            }

        });

        mAdapter = new SelectedBlogAdapter(mSelectedBlogList, this, this);
        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        mBinding.selectedBlogRecycleView.setLayoutManager(flowLayoutManager);
        mBinding.selectedBlogRecycleView.setAdapter(mAdapter);
        mBinding.selectedBlogRecycleView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.set(15, 15, 15, 15);
            }
        });
        mBinding.confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mSelectedBlogList.size() >= 1) {
                    if (conditionsForApiCall()) {
                        for (UserResponse user : mSelectedBlogList) {
                            bloggerId.add(user.getUserId());
                        }
                        if (bloggerId.size() >= 1) {
                            sendId(bloggerId);
                        }
                    }
                } else {
                    openWelcomeProfileActivity();
                }
            }

        });


        mBinding.recycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();
                boolean endHasBeenReached = lastVisible >= totalItemCount - 4;
                if (totalItemCount > 0 && endHasBeenReached) {
                    if (!isAlreadyCalled && currentPageNumber < totalPages && mBloggerList.size() > 0) {
                        isAlreadyCalled = true;
                        getAllBloggers();
                        setLoadMore();
                    }
                }

            }
        });
    }

    private void setLoadMore() {
        if (isAlreadyCalled) {
            bloggerAdapter.setProgress(true);
        } else {
            bloggerAdapter.setProgress(false);
        }
    }

    private void getAllBloggers() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        String accessToken = getTempToken();
        apiInstance.getBloggers("Bearer " + accessToken, searchBloggers, currentPageNumber + 1).enqueue(new Callback<ApiResponse<Bloggers>>() {
            @Override
            public void onResponse(Call<ApiResponse<Bloggers>> call, Response<ApiResponse<Bloggers>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    if (response.body().getData() != null) {
                        mBlogger = response.body().getData();
                        List<UserResponse> mList = mBlogger.getItems();

                        currentPageNumber = mBlogger.getPageNumber();
                        totalPages = mBlogger.getTotalPages();
                        if (currentPageNumber > 1) {
                            mBloggerList.addAll(mList);
                        } else {
                            mBloggerList = mList;
                        }
                        mBinding.progressBar.setVisibility(View.INVISIBLE);
                        mBinding.searchIcon.setVisibility(View.VISIBLE);
                        if (mBloggerList == null || mBloggerList.size() < 0) {
                            showToast("No bloggers found");
                        }
                    }
                    isAlreadyCalled = false;
                    setLoadMore();
                    filterAlreadySelected();
                    bloggerAdapter.setList(mBloggerList);
                    mProgressBarDialog.hideProgress();

                } else if (response.code() == 500) {
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(SelectBloggerActivity.this, getResources().getString(R.string.error_server));
                } else {
                    mAlertDialog.showDialog(SelectBloggerActivity.this, getResources().getString(R.string.error_server));
                    mProgressBarDialog.hideProgress();
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<Bloggers>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
            }
        });
    }


    private void setUIConfirmationButton() {
        if (mSelectedBlogList.size() >= 1) {
            mBinding.confirmButton.setText(R.string.done);
            mBinding.selectedBlogRecycleView.setVisibility(View.VISIBLE);
        } else {
            mBinding.confirmButton.setText(R.string.skip_this_step);
            mBinding.selectedBlogRecycleView.setVisibility(View.GONE);
        }

        if (mSelectedBlogList.size() == 3) {
            mBinding.horizontalProgressBar.setProgress(75);
        } else {
            mBinding.horizontalProgressBar.setProgress(50);
        }

    }

    private void setProgressBarColors() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {

            Drawable wrapDrawable1 = DrawableCompat.wrap(mBinding.progressBar.getIndeterminateDrawable());
            DrawableCompat.setTint(wrapDrawable1, ContextCompat.getColor(this, R.color.com_facebook_blue));
            mBinding.progressBar.setIndeterminateDrawable(DrawableCompat.unwrap(wrapDrawable1));

        } else {
            mBinding.progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.com_facebook_blue), PorterDuff.Mode.SRC_IN);
        }
    }


    @Override
    public void onClickBlogger(UserResponse user) {
        boolean isAlreadyAdded = false;
        int bloggerId = user.getUserId();
        if (mSelectedBlogList.size() < 1) {
            mSelectedBlogList.add(user);
            bloggerAdapter.setSelected(true);
            mAdapter.setList(mSelectedBlogList);
            user.setSelected(true);
            mBloggerList.remove(user);
            mBloggerList.add(0, user);
        } else if (mSelectedBlogList.size() < 3) {
            for (int i = 0; i < mSelectedBlogList.size(); i++) {
                if (mSelectedBlogList.get(i).getUserId() == (bloggerId)) {
                    isAlreadyAdded = true;
                    break;
                }
            }
            if (isAlreadyAdded) {
                Toast.makeText(SelectBloggerActivity.this, " has already been added", Toast.LENGTH_SHORT).show();
            } else {
                mSelectedBlogList.add(user);
                mAdapter.setList(mSelectedBlogList);
                mBloggerList.remove(user);
                user.setSelected(true);
                mBloggerList.add(0, user);
            }
        } else {
            Toast.makeText(this, "You can add a maximum of 3 bloggers", Toast.LENGTH_SHORT).show();
        }
        bloggerAdapter.setList(mBloggerList);
        setUIConfirmationButton();
    }

    void filterAlreadySelected() {
        for (int i = 0; i < mBloggerList.size(); i++) {
            for (int j = 0; j < mSelectedBlogList.size(); j++) {
                if (mBloggerList.get(i).getUserId() == mSelectedBlogList.get(j).getUserId()) {
                    UserResponse blogger = mBloggerList.get(i);
                    blogger.setSelected(true);
                }
            }
        }
    }

    @Override
    public void unSelectBlogger(UserResponse userResponse) {
        mSelectedBlogList.remove(userResponse);
        mAdapter.setList(mSelectedBlogList);
        for (int j = 0; j < mBloggerList.size(); j++) {
            if (userResponse.getUserId() == mBloggerList.get(j).getUserId()) {
                UserResponse user = mBloggerList.get(j);
                mBloggerList.remove(j);
                user.setSelected(false);
                mBloggerList.add(mBloggerList.size(), user);
                break;
            }
        }
        bloggerAdapter.setList(mBloggerList);
        setUIConfirmationButton();
    }

    void sendId(final List<Integer> bloggerIds) {
        mProgressBarDialog.showProgress();
        PostBloggerIds postBrandIds = new PostBloggerIds();
        postBrandIds.setBloggerIds(bloggerIds);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.postBloggers("Bearer " + getTempToken(), postBrandIds).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    openWelcomeProfileActivity();
                } else if (response.code() == 500) {
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(SelectBloggerActivity.this, getResources().getString(R.string.error_server));
                } else {
                    mProgressBarDialog.hideProgress();
                }


            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
            }
        });

    }
}