package com.bynmix.app.activities.onboarding;

import android.databinding.DataBindingUtil;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.adapter.TagsAdapter;
import com.bynmix.app.databinding.ActivitySelectTagsBinding;
import com.bynmix.app.interfaces.TagListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Descriptor;
import com.bynmix.app.models.PostTagIds;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.sharedpreference.SharedPreferenceUtility;
import com.bynmix.app.utils.BaseActivity;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectTagsActivity extends BaseActivity implements TagListener {
    public ArrayAdapter<String> aAdapter;
    public ArrayList<Integer> idList;
    private ActivitySelectTagsBinding mBinding;
    private TagsAdapter adapter;
    private boolean isConfirm = false;
    private String searchTags;
    private SharedPreferenceUtility mSharedPreference;
    private List<Descriptor> mTagList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_select_tags);
        init();
        getAllTags();
        setTags();
        mSharedPreference = new SharedPreferenceUtility(this);
        mBinding.progressBar.setVisibility(View.INVISIBLE);
        mBinding.welcomeTagsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(SelectTagsActivity.this);
            }
        });
        setProgressBarColors();
        mBinding.confirmButton.setEnabled(false);
        mBinding.autoComplete.requestFocus();
        mBinding.autoComplete.setThreshold(1);
        mBinding.autoComplete.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable editable) {
                // TODO Auto-generated method stub

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newText = s.toString();
                if (newText.length() > 1) {
                    mBinding.progressBar.setVisibility(View.VISIBLE);
                    searchTags = newText;
                    new getJson().execute(newText);
                }
            }
        });

        mBinding.welcomeTagsLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard(SelectTagsActivity.this);
                return false;
            }
        });

        mBinding.recycleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard(SelectTagsActivity.this);
                return false;
            }
        });

        mBinding.autoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int position,
                                    long id) {
                int selectedTagsCount = getSelectedTagsCount();
                if (selectedTagsCount < 3) {
                    Descriptor newTag = new Descriptor();
                    newTag.setDescriptorId(idList.get(position));
                    newTag.setDescriptorName(aAdapter.getItem(position));
                    mBinding.autoComplete.setText("");
                    if (isTagAlreadySelected(newTag)) {
                        Toast.makeText(SelectTagsActivity.this, "Already selected!*", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (isTagInList(newTag)) {
                        onClickTag(getSameTagFromList(newTag));
                        return;
                    }
                    newTag.setSelected(true);
                    mTagList.add(0, newTag);
                } else {
                    showToast(getString(R.string.maximum_tags_selected));
                }
                adapter.setList(mTagList);
                setTags();
            }
        });

        mBinding.confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isConfirm) {
                    if (conditionsForApiCall()) {
                        List<Integer> tagId = new ArrayList<>();
                        for (Descriptor tag : mTagList) {
                            if (tag.isSelected()) {
                                tagId.add(tag.getDescriptorId());
                            }
                        }
                        if (tagId.size() == 3) {
                            sendId(tagId);
                        }
                    }
                }
            }
        });

        adapter = new TagsAdapter(this, mTagList, this);
        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        mBinding.recycleView.setLayoutManager(flowLayoutManager);
        mBinding.recycleView.setAdapter(adapter);
        mBinding.recycleView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.set(15, 15, 15, 15);
            }
        });

    }

    private void setProgressBarColors() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Drawable wrapDrawable1 = DrawableCompat.wrap(mBinding.progressBar.getIndeterminateDrawable());
            DrawableCompat.setTint(wrapDrawable1, ContextCompat.getColor(this, R.color.com_facebook_blue));
            mBinding.progressBar.setIndeterminateDrawable(DrawableCompat.unwrap(wrapDrawable1));
        } else {
            mBinding.progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.com_facebook_blue), PorterDuff.Mode.SRC_IN);
        }
    }

    private void setTags() {
        if (getSelectedTagsCount() == 3) {
            isConfirm = true;
            mBinding.confirmButton.setEnabled(true);
            mBinding.confirmButton.setText(getResources().getString(R.string.done));
            mBinding.confirmButton.setBackground(getResources().getDrawable(R.drawable.purple_rounded_button));
            mBinding.horizontalProgressBar.setProgress(25);
        } else {
            isConfirm = false;
            mBinding.confirmButton.setEnabled(false);
            mBinding.confirmButton.setText(getResources().getString(R.string.done));
            mBinding.confirmButton.setBackground(getResources().getDrawable(R.drawable.disable_onboarding_button));
            mBinding.horizontalProgressBar.setProgress(20);
        }
    }

    @Override
    public void onClickTag(Descriptor tag) {
        if (getSelectedTagsCount() < 3 || tag.isSelected()) {
            tag.setSelected(!tag.isSelected());
            if (tag.isSelected()) {
                mTagList.remove(tag);
                mTagList.add(0, tag);
            } else {
                mTagList.remove(tag);
                if (mTagList.size() < 10) {
                    mTagList.add(tag);
                }
            }
            setTags();
            adapter.setList(mTagList);
        } else {
            showToast(getString(R.string.maximum_tags_selected));
        }
    }

    private Descriptor getSameTagFromList(Descriptor newTag) {
        for (Descriptor tag : mTagList) {
            if (newTag.getDescriptorId() == tag.getDescriptorId()) {
                return tag;
            }
        }
        return null;
    }

    private boolean isTagInList(Descriptor newTag) {
        for (Descriptor tag : mTagList) {
            if (tag.getDescriptorId() == newTag.getDescriptorId()) {
                return true;
            }
        }
        return false;
    }

    private boolean isTagAlreadySelected(Descriptor newTag) {
        for (Descriptor tag : mTagList) {
            if (tag.getDescriptorId() == newTag.getDescriptorId() && tag.isSelected()) {
                return true;
            }
        }
        return false;
    }

    private int getSelectedTagsCount() {
        int count = 0;
        for (Descriptor brand : mTagList) {
            if (brand.isSelected()) {
                count++;
            }
        }
        return count;
    }

    void sendId(List<Integer> tagIds) {
        mProgressBarDialog.showProgress();
        PostTagIds postTagIds = new PostTagIds();
        postTagIds.setTagIds(tagIds);
        ApiEndpointInterface apiInstance = ApiService.instance();
        String accessToken = getTempToken();
        apiInstance.postTags("Bearer " + accessToken, postTagIds).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    OpenBloggerActivity();
                } else if (response.code() == 500) {
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(SelectTagsActivity.this, getResources().getString(R.string.error_server));
                } else {
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(SelectTagsActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
            }
        });
    }

    private ArrayList<String> processTags(List<Descriptor> tags) {
        ArrayList<String> results = new ArrayList<>();

        for (Descriptor tag : tags) {
            results.add(tag.getDescriptorName());
        }
        return results;
    }

    private ArrayList<Integer> processTagsId(List<Descriptor> tags) {
        ArrayList<Integer> results = new ArrayList<>();

        for (Descriptor tag : tags) {
            results.add(tag.getDescriptorId());
        }

        return results;
    }

    private void getAllTags() {
        mProgressBarDialog.showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        String accessToken = getTempToken();
        apiInstance.getTags("Bearer " + accessToken, searchTags).enqueue(new Callback<ApiResponse<List<Descriptor>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Descriptor>>> call, Response<ApiResponse<List<Descriptor>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    if (response.body().getData() != null && response.body().getData().size() > 0) {
                        List<Descriptor> descriptors = response.body().getData();
                        final ArrayList<String> suggest = processTags(descriptors);
                        idList = processTagsId(descriptors);
                        if (searchTags == null) {
                            mTagList = descriptors;
                            adapter.setList(mTagList);
                        }
                        mBinding.progressBar.setVisibility(View.INVISIBLE);

                        runOnUiThread(new Runnable() {
                            public void run() {
                                aAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.autocomplete_dropdown, suggest);
                                mBinding.autoComplete.setAdapter(aAdapter);
                                aAdapter.notifyDataSetChanged();
                                if (suggest.size() == 0) {
                                    showToast("No brands found");
                                }

                            }
                        });
                        if (suggest != null && suggest.size() > 3) {
                            closeKeyboard();
                        }


                    }
                    mProgressBarDialog.hideProgress();

                } else if (response.code() == 500) {
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(SelectTagsActivity.this, getResources().getString(R.string.error_server));
                } else {
                    mAlertDialog.showDialog(SelectTagsActivity.this, getResources().getString(R.string.error_server));
                    mProgressBarDialog.hideProgress();
                }

            }

            @Override
            public void onFailure(Call<ApiResponse<List<Descriptor>>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
            }
        });
    }


    private class getJson extends AsyncTask<String, String, Boolean> {
        ArrayList<String> suggest;

        @Override
        protected Boolean doInBackground(String... key) {
            String newText = key[0];
            newText = newText.trim();
            try {
                ApiEndpointInterface apiInstance = ApiService.instance();
                Response<ApiResponse<List<Descriptor>>> response = apiInstance.getTags("Bearer " + getTempToken(), newText).execute();
                if (response.isSuccessful() && response.code() == 200) {
                    List<Descriptor> tags = response.body().getData();
                    suggest = processTags(tags);
                    idList = processTagsId(tags);
                    return true;
                } else {
                    mAlertDialog.showDialog(SelectTagsActivity.this, getResources().getString(R.string.error_server));
                }

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                suggest = new ArrayList<String>();
                idList = new ArrayList<Integer>();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean s) {
            super.onPostExecute(s);
            mBinding.progressBar.setVisibility(View.INVISIBLE);
            runOnUiThread(new Runnable() {
                public void run() {
                    aAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.autocomplete_dropdown, suggest);
                    mBinding.autoComplete.setAdapter(aAdapter);
                    aAdapter.notifyDataSetChanged();
                    if (suggest == null || suggest.size() == 0) {
                        showToast("No tags found");
                    }
                }
            });
//            if (suggest != null && suggest.size() > 3) {
//                closeKeyboard();
//            }
        }

    }
}
