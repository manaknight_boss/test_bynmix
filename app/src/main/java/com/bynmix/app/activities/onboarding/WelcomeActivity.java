package com.bynmix.app.activities.onboarding;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.view.View;
import com.bynmix.app.R;
import com.bynmix.app.databinding.ActivityWelcomeBinding;
import com.bynmix.app.utils.BaseActivity;

public class WelcomeActivity extends BaseActivity {
    private ActivityWelcomeBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_welcome);
        mBinding.welcomeGotItButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), WelcomeBrandActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
}
