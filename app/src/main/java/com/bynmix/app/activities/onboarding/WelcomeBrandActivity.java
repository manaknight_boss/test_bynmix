package com.bynmix.app.activities.onboarding;

import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.adapter.BrandAdapter;
import com.bynmix.app.databinding.ActivityWelcomeBrandBinding;
import com.bynmix.app.interfaces.BrandListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Brand;
import com.bynmix.app.models.PostBrandIds;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.sharedpreference.SharedPreferenceUtility;
import com.bynmix.app.utils.BaseActivity;
import com.bynmix.app.utils.CustomTextDialog;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WelcomeBrandActivity extends BaseActivity implements BrandListener {
    private ActivityWelcomeBrandBinding mBinding;
    public ArrayAdapter<String> aAdapter;
    public ArrayList<Integer> idList;
    List<Integer> brandid;
    private List<Brand> mBrandList = new ArrayList<>();
    private boolean isConfirm = false;
    private SharedPreferenceUtility mSharedPreference;
    private String searchBrands;
    private BrandAdapter brandAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_welcome_brand);
        init();
        getAllBrands();
        setBrands();
        mSharedPreference = new SharedPreferenceUtility(this);
        mBinding.progressBar.setVisibility(View.INVISIBLE);
        setProgressBarColors();
        mBinding.welcomeConfirmButton.setEnabled(false);
        mBinding.autoComplete.requestFocus();
        mBinding.autoComplete.setThreshold(1);
        mBinding.autoComplete.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable editable) {
                // TODO Auto-generated method stub

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newText = s.toString();
                Log.d(DEBUGTAG, String.valueOf(mBinding.autoComplete.isPerformingCompletion()));
                Log.d(DEBUGTAG, String.valueOf(mBinding.autoComplete.isPerformingCompletion()));
                if (newText.length() > 1) {
                    mBinding.progressBar.setVisibility(View.VISIBLE);
                    searchBrands = newText;
                    new getJson().execute(newText);
                }

            }

        });
        mBinding.autoComplete.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View arg1, int position, long id) {
                closeKeyboard();
                int selectedBrandsCount = getSelectedBrandsCount();
                if (selectedBrandsCount < 3) {
                    Brand newBrand = new Brand();
                    newBrand.setBrandId(idList.get(position));
                    newBrand.setBrandName(aAdapter.getItem(position));
                    mBinding.autoComplete.setText("");
                    if (isBrandAlreadySelected(newBrand)) {
                        Toast.makeText(WelcomeBrandActivity.this, "Already selected!*", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    if (isBrandInList(newBrand)) {
                        onClickBrand(getSameBrandFromList(newBrand));
                        return;
                    }
                    newBrand.setSelected(true);
                    mBrandList.add(0, newBrand);
                } else {
                    showToast(getString(R.string.maximum_brands_selected));
                }
                brandAdapter.setList(mBrandList);
                setBrands();
            }
        });

        mBinding.welcomeBrandLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard(WelcomeBrandActivity.this);
                return false;
            }
        });

        mBinding.recycleView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                hideKeyboard(WelcomeBrandActivity.this);
                return false;
            }
        });

//        mBinding.autoComplete.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (!hasFocus) {
//                    hideKeyboard(WelcomeBrandActivity.this);
//                }
//            }
//        });

        mBinding.welcomeConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isConfirm) {
                    if (conditionsForApiCall()) {
                        brandid = new ArrayList<>();
                        for (Brand brand : mBrandList) {
                            if (brand.isSelected()) {
                                brandid.add(brand.getBrandId());
                            }
                        }
                        if (brandid.size() == 3) {
                            sendId(brandid);
                        }
                    }
                }

            }
        });

        brandAdapter = new BrandAdapter(this, mBrandList, this);
        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        mBinding.recycleView.setLayoutManager(flowLayoutManager);
        mBinding.recycleView.setAdapter(brandAdapter);
        mBinding.welcomeBrandText.setPaintFlags(mBinding.welcomeBrandText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mBinding.recycleView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.set(15, 15, 15, 15);
            }
        });

    }

    private Brand getSameBrandFromList(Brand newBrand) {
        for (Brand brand : mBrandList) {
            if (brand.getBrandId() == newBrand.getBrandId()) {
                return brand;
            }
        }
        return null;
    }

    private boolean isBrandInList(Brand newBrand) {
        for (Brand brand : mBrandList) {
            if (brand.getBrandId() == newBrand.getBrandId()) {
                return true;
            }
        }
        return false;
    }

    private boolean isBrandAlreadySelected(Brand newBrand) {
        for (Brand brand : mBrandList) {
            if (brand.getBrandId() == newBrand.getBrandId() && brand.isSelected()) {
                return true;
            }
        }
        return false;
    }

    private int getSelectedBrandsCount() {
        int count = 0;
        for (Brand brand : mBrandList) {
            if (brand.isSelected()) {
                count++;
            }
        }
        return count;
    }

    private void setBrands() {
        if (getSelectedBrandsCount() == 3) {
            isConfirm = true;
            mBinding.welcomeConfirmButton.setEnabled(true);
            mBinding.welcomeConfirmButton.setText(getResources().getString(R.string.done));
            mBinding.welcomeConfirmButton.setBackground(getResources().getDrawable(R.drawable.purple_rounded_button));
            mBinding.horizontalProgressBar.setProgress(25);
        } else {
            isConfirm = false;
            mBinding.welcomeConfirmButton.setEnabled(false);
            mBinding.welcomeConfirmButton.setText(getResources().getString(R.string.done));
            mBinding.welcomeConfirmButton.setBackground(getResources().getDrawable(R.drawable.disable_onboarding_button));
            mBinding.horizontalProgressBar.setProgress(20);
        }
    }

    private void setProgressBarColors() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Drawable wrapDrawable = DrawableCompat.wrap(mBinding.progressBar.getIndeterminateDrawable());
            DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(this, R.color.com_facebook_blue));
            mBinding.progressBar.setIndeterminateDrawable(DrawableCompat.unwrap(wrapDrawable));

        } else {
            mBinding.progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(this, R.color.com_facebook_blue), PorterDuff.Mode.SRC_IN);
        }
    }

    public void onWhyClick(View v) {
        CustomTextDialog alert = new CustomTextDialog();
        alert.showDialog(this, translateString(R.string.welcome_brand_dialog_text));
    }

    @Override
    public void onClickBrand(Brand brand) {
        if (getSelectedBrandsCount() < 3 || brand.isSelected()) {
            brand.setSelected(!brand.isSelected());
            if (brand.isSelected()) {
                mBrandList.remove(brand);
                mBrandList.add(0, brand);
            } else {
                mBrandList.remove(brand);
                if (mBrandList.size() < 10) {
                    mBrandList.add(brand);
                }
            }
            setBrands();
            brandAdapter.setList(mBrandList);
        } else {
            showToast(getString(R.string.maximum_brands_selected));
        }
    }

    void sendId(final List<Integer> brandIds) {
        mProgressBarDialog.showProgress();
        PostBrandIds postBrandIds = new PostBrandIds();
        postBrandIds.setBrandIds(brandIds);
        ApiEndpointInterface apiInstance = ApiService.instance();
        String accessToken = getTempToken();
        apiInstance.postBrands("Bearer " + accessToken, postBrandIds).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    mProgressBarDialog.hideProgress();
                    OpenTagsActivity();
                } else if (response.code() == 500) {
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(WelcomeBrandActivity.this, getResources().getString(R.string.error_server));
                } else {
                    mAlertDialog.showDialog(WelcomeBrandActivity.this, getResources().getString(R.string.error_server));
                    mProgressBarDialog.hideProgress();
                }

            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
            }
        });
    }


    private class getJson extends AsyncTask<String, String, Boolean> {
        ArrayList<String> suggest;

        @Override
        protected Boolean doInBackground(String... key) {
            String newText = key[0];
            newText = newText.trim();
            try {
                ApiEndpointInterface apiInstance = ApiService.instance();
                Response<ApiResponse<List<Brand>>> response = apiInstance.getBrands("Bearer " + getTempToken(), newText).execute();
                if (response.isSuccessful() && response.code() == 200) {
                    List<Brand> brands = response.body().getData();
                    suggest = processBrands(brands);
                    idList = processBrandsId(brands);
                    return true;
                } else {
                    mAlertDialog.showDialog(WelcomeBrandActivity.this, getResources().getString(R.string.error_server));
                }

            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                suggest = new ArrayList<String>();
                idList = new ArrayList<Integer>();
            }

            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            if (success) {
                mBinding.progressBar.setVisibility(View.INVISIBLE);
                runOnUiThread(new Runnable() {
                    public void run() {
                        aAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.autocomplete_dropdown, suggest);
                        mBinding.autoComplete.setAdapter(aAdapter);
                        aAdapter.notifyDataSetChanged();
                        if (suggest == null || suggest.size() == 0) {
                            showToast("No brands found");
                        }
                    }
                });
//                if (suggest != null && suggest.size() > 3) {
//                    closeKeyboard();
//                }
            }
        }

        protected ArrayList<String> processBrands(List<Brand> brands) {
            ArrayList<String> results = new ArrayList<String>();

            for (Brand brand : brands) {
                results.add(brand.getBrandName());
            }
            return results;
        }

        protected ArrayList<Integer> processBrandsId(List<Brand> brands) {
            ArrayList<Integer> results = new ArrayList<Integer>();

            for (Brand brand : brands) {
                results.add(brand.getBrandId());
            }

            return results;
        }
    }

    private ArrayList<String> processBrands(List<Brand> brands) {
        ArrayList<String> results = new ArrayList<>();
        for (Brand brand : brands) {
            results.add(brand.getBrandName());
        }
        return results;
    }

    private ArrayList<Integer> processBrandsId(List<Brand> brands) {
        ArrayList<Integer> results = new ArrayList<Integer>();
        for (Brand brand : brands) {
            results.add(brand.getBrandId());
        }
        return results;
    }

    private void getAllBrands() {
        mProgressBarDialog.showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        String accessToken = getTempToken();
        apiInstance.getBrands("Bearer " + accessToken, searchBrands).enqueue(new Callback<ApiResponse<List<Brand>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Brand>>> call, Response<ApiResponse<List<Brand>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    if (response.body().getData() != null && response.body().getData().size() > 0) {
                        List<Brand> brands = response.body().getData();
                        final ArrayList<String> suggest = processBrands(brands);
                        idList = processBrandsId(brands);
                        if (searchBrands == null) {
                            mBrandList = brands;
                            brandAdapter.setList(mBrandList);
                        }
                        mBinding.progressBar.setVisibility(View.INVISIBLE);

                        runOnUiThread(new Runnable() {
                            public void run() {
                                aAdapter = new ArrayAdapter<>(getApplicationContext(), R.layout.autocomplete_dropdown, suggest);
                                mBinding.autoComplete.setAdapter(aAdapter);
                                aAdapter.notifyDataSetChanged();
                                if (suggest.size() == 0) {
                                    showToast("No brands found");
                                }

                            }
                        });
                        if (suggest != null && suggest.size() > 3) {
                            closeKeyboard();
                        }


                    }
                    mProgressBarDialog.hideProgress();
                } else if (response.code() == 500) {
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(WelcomeBrandActivity.this, getResources().getString(R.string.error_server));
                } else {
                    mAlertDialog.showDialog(WelcomeBrandActivity.this, getResources().getString(R.string.error_server));
                    mProgressBarDialog.hideProgress();
                }

            }

            @Override
            public void onFailure(Call<ApiResponse<List<Brand>>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
            }
        });
    }
}
