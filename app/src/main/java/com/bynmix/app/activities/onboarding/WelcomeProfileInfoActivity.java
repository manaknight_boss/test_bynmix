package com.bynmix.app.activities.onboarding;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.databinding.ActivityWelcomeProfileInfoBinding;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.BodyType;
import com.bynmix.app.models.UserResponse;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.sharedpreference.SharedPreferenceUtility;
import com.bynmix.app.utils.BaseActivity;
import com.bynmix.app.utils.CustomTextDialog;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WelcomeProfileInfoActivity extends BaseActivity {
    private ActivityWelcomeProfileInfoBinding mBinding;
    private List<String> mFeet = new ArrayList<>();
    private List<String> mInches = new ArrayList<>();
    private List<String> gender = new ArrayList<>();
    private List<String> bodyNameList = new ArrayList<>();
    private List<BodyType> bodyTypeList = new ArrayList<>();
    private SharedPreferenceUtility mSharedPreference;
    private boolean buttonStateChanged = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_welcome_profile_info);
        mBinding.whyText.setPaintFlags(mBinding.whyText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mSharedPreference = new SharedPreferenceUtility(this);
        chnageConfirmButtonState();
        addDataInLists();
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(this, R.layout.item_spinner, gender);
        mBinding.gender.setAdapter(genderAdapter);
        ArrayAdapter<String> inchesAdapter = new ArrayAdapter<String>(this, R.layout.item_spinner, mInches);
        mBinding.inches.setAdapter(inchesAdapter);
        ArrayAdapter<String> feetAdapter = new ArrayAdapter<String>(this, R.layout.item_spinner, mFeet);
        mBinding.feet.setAdapter(feetAdapter);
        getAdaptersStates();
        mBinding.title.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable editable) {
                // TODO Auto-generated method stub

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newText = s.toString();
                if (newText.length() > 1) {
                    buttonStateChanged = true;
                    chnageConfirmButtonState();
                }
            }

        });


        mBinding.confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (buttonStateChanged) {
                    sendData();
                } else {
                    sendOnBoardingTrue();
                }
            }
        });
        mBinding.whyText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomTextDialog alert = new CustomTextDialog();
                alert.showDialog(WelcomeProfileInfoActivity.this, translateString(R.string.welcome_profile_dialog_text));
            }
        });
    }

    private void getAdaptersStates() {

        mBinding.feet.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 0) {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.lightGrey));
                } else {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorPrimary));
                    buttonStateChanged = true;
                    chnageConfirmButtonState();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mBinding.inches.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 0) {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.lightGrey));
                } else {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorPrimary));
                    buttonStateChanged = true;
                    chnageConfirmButtonState();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        mBinding.body.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 0) {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.lightGrey));
                } else {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorPrimary));
                    buttonStateChanged = true;
                    chnageConfirmButtonState();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        mBinding.gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 0) {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.lightGrey));
                } else {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorPrimary));
                    buttonStateChanged = true;
                    chnageConfirmButtonState();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void chnageConfirmButtonState() {
        if (buttonStateChanged == true) {
            mBinding.confirmButton.setText(R.string.done);
        } else {
            mBinding.confirmButton.setText(R.string.skip_this_step);
        }
        mBinding.horizontalProgressBar.setProgress(95);
    }

    private void addDataInLists() {
        getBodyType();
        for (int i = 0; i <= 11; i++) {
            mInches.add("" + i);
        }
        for (int i = 3; i <= 8; i++) {
            mFeet.add("" + i);
        }
        mInches.add(0, "Inches");
        mFeet.add(0, "Feet");
        gender.add(0, "Select Gender");
        gender.add("Male");
        gender.add("Female");
    }

    private void getBodyType() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getBodyType("Bearer " + getTempToken()).enqueue(new Callback<ApiResponse<List<BodyType>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<BodyType>>> call, Response<ApiResponse<List<BodyType>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    bodyTypeList = response.body().getData();
                    for (int i = 0; i < bodyTypeList.size(); i++) {
                        bodyNameList.add(bodyTypeList.get(i).getBodyTypeName());
                    }
                    bodyNameList.add(0, "Select a body type");
                    setBodyTypeAdapter();
                } else if (response.code() == 500) {
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(WelcomeProfileInfoActivity.this, getResources().getString(R.string.error_server));
                } else {
                    mAlertDialog.showDialog(WelcomeProfileInfoActivity.this, getResources().getString(R.string.error_server));
                    mProgressBarDialog.hideProgress();
                }

            }

            @Override
            public void onFailure(Call<ApiResponse<List<BodyType>>> call, Throwable t) {
                mAlertDialog.showDialog(WelcomeProfileInfoActivity.this, getResources().getString(R.string.error_server));
                mProgressBarDialog.hideProgress();
            }
        });
    }

    private void setBodyTypeAdapter() {
        ArrayAdapter<String> feetAdapter = new ArrayAdapter<String>(this, R.layout.item_spinner, bodyNameList);
        mBinding.body.setAdapter(feetAdapter);
    }

    private void sendData() {
        String title, gender, feet, inches, bodyName;
        title = mBinding.title.getText().toString();
        gender = mBinding.gender.getSelectedItem().toString();
        feet = mBinding.feet.getSelectedItem().toString();
        inches = mBinding.inches.getSelectedItem().toString();
        bodyName = mBinding.body.getSelectedItem().toString();
        int genderType;
        int bodyTypeId = 0;
        if (gender.equalsIgnoreCase("male")) {
            genderType = 2;
        } else {
            genderType = 1;
        }

        if (inches.equalsIgnoreCase("inches")) {
            inches = "" + 0;
        }

        if (feet.equalsIgnoreCase("feet")) {
            feet = "" + 0;
        }
        if (gender.equalsIgnoreCase("Select Gender")) {
            genderType = 0;
        }

        for (int i = 0; i < bodyTypeList.size(); i++) {
            if (bodyName.equalsIgnoreCase(bodyTypeList.get(i).getBodyTypeName())) {
                bodyTypeId = bodyTypeList.get(i).getBodyTypeId();
                break;
            }
        }
        UserResponse user = new UserResponse();
        user.setShortTitle(title);
        user.setGender(genderType);
        user.setHeightFeet(Integer.parseInt(feet));
        user.setHeightInches(Integer.parseInt(inches));
        user.setBodyTypeId(bodyTypeId);
        user.setFinishedRegister(true);
        sendApi(user);
    }

    private void sendApi(UserResponse user) {
        mProgressBarDialog.showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        user.setOnboardingComplete(true);
        apiInstance.putOnEditUserProfile("Bearer " + getTempToken(), user).enqueue(new Callback<ApiResponse<UserResponse>>() {
            @Override
            public void onResponse(Call<ApiResponse<UserResponse>> call, Response<ApiResponse<UserResponse>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mProgressBarDialog.hideProgress();
                    preferences.edit().putString(config.ACCESS_TOKEN, getTempToken()).apply();
                    mProgressBarDialog.hideProgress();
                    mSharedPreference.setIsAppOpenFirstTime(true);
                    pushNotifications();
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(intent);
                    finish();
                } else if (response.code() == 500) {
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(WelcomeProfileInfoActivity.this, getResources().getString(R.string.error_server));
                } else {
                    mAlertDialog.showDialog(WelcomeProfileInfoActivity.this, getResources().getString(R.string.error_server));
                    mProgressBarDialog.hideProgress();
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<UserResponse>> call, Throwable t) {
                mAlertDialog.showDialog(WelcomeProfileInfoActivity.this, getResources().getString(R.string.error_server));
                mProgressBarDialog.hideProgress();
            }
        });
    }

    private void sendOnBoardingTrue() {
        String accessToken = getTempToken();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.putOnBoardingTrue("Bearer " + accessToken, true).enqueue(new Callback<ApiResponse<UserResponse>>() {
            @Override
            public void onResponse(Call<ApiResponse<UserResponse>> call, Response<ApiResponse<UserResponse>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    preferences.edit().putString(config.ACCESS_TOKEN, getTempToken()).apply();
                    mProgressBarDialog.hideProgress();
                    mSharedPreference.setIsAppOpenFirstTime(true);
                    pushNotifications();
                    Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                    startActivity(intent);
                    finish();
                } else if (response.code() == 500) {
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(WelcomeProfileInfoActivity.this, getResources().getString(R.string.error_server));
                } else {
                    mAlertDialog.showDialog(WelcomeProfileInfoActivity.this, getResources().getString(R.string.error_server));
                    mProgressBarDialog.hideProgress();
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<UserResponse>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
            }
        });
    }
}
