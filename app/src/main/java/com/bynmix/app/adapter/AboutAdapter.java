package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bynmix.app.databinding.ItemAboutBinding;
import com.bynmix.app.databinding.TermsOfServiceItemBinding;
import com.bynmix.app.databinding.TextViewBinding;
import com.bynmix.app.models.Sections;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;

import java.util.List;

public class AboutAdapter extends RecyclerView.Adapter<AboutAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<Sections> mList;

    public AboutAdapter(Context mContext, List<Sections> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public AboutAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemAboutBinding mBinding = ItemAboutBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new AboutAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(AboutAdapter.ViewHolderItem holder, int position) {
        Sections sections = mList.get(position);
        if(TextUtils.isEmpty(sections.getTitle())) {
            holder.itemView.title.setVisibility(View.GONE);
        } else {
            holder.itemView.title.setVisibility(View.VISIBLE);
            holder.itemView.title.setText(sections.getTitle());
        }

        holder.itemView.subDescription.setVisibility(View.GONE);
        if(TextUtils.isEmpty(sections.getDescription())) {
            holder.itemView.description.setVisibility(View.GONE);
            holder.itemView.points.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.description.setVisibility(View.VISIBLE);
            holder.itemView.points.setVisibility(View.GONE);
            holder.itemView.description.setText(sections.getDescription());
        }

        List<Sections> subSections = sections.getSubSection();
        if(subSections != null) {
            for (int i = 0; i < subSections.size(); i++) {
                TextViewBinding mBinding = TextViewBinding.inflate(LayoutInflater.from(mContext));
                String description = sections.getSubSection().get(i).getDescription();
                if (description.startsWith("-")) {
                    mBinding.pointText.setText("\t" + description);
                } else {
                    mBinding.pointText.setText(description);
                }
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                int marginSmall = Utils.dpToPx(mContext, 4);
                int marginLarge = Utils.dpToPx(mContext, 8);
                layoutParams.setMargins(0, marginSmall, marginSmall, marginSmall);
                mBinding.pointText.setLayoutParams(layoutParams);
                holder.itemView.points.addView(mBinding.pointText);
            }
        }

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<Sections> mList) {
        this.mList.clear();
        this.mList = mList;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ItemAboutBinding itemView;

        public ViewHolderItem(ItemAboutBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

