package com.bynmix.app.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.bynmix.app.R;
import com.bynmix.app.databinding.DotsDetailItemBinding;
import com.bynmix.app.interfaces.AddTagsListener;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class AddDotsAdapter extends RecyclerView.Adapter<AddDotsAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<String> mTypeList = new ArrayList<>();
    private List<AnnotatedPoint> mList;
    private AddTagsListener dotsListener;
    private CreatePostListener mCallback;
    private final int VALUE_TITLE = 0;
    private final int VALUE_URL = 1;

    private int linkErrorPosition = -1;

    public AddDotsAdapter(Context mContext, List<AnnotatedPoint> mList, AddTagsListener dotsListener, CreatePostListener mCallback) {
        this.mContext = mContext;
        this.dotsListener = dotsListener;
        this.mCallback = mCallback;
        addData();
        this.mList = mList;
    }

    private void addData() {
        mTypeList.add(mContext.getString(R.string.select_tag_type));
        mTypeList.add(mContext.getString(R.string.online_retailer));
        mTypeList.add(mContext.getString(R.string.my_listing));
    }

    @NonNull
    @Override
    public AddDotsAdapter.ViewHolderItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        DotsDetailItemBinding mBinding = DotsDetailItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new AddDotsAdapter.ViewHolderItem(mBinding);
    }

    public class CustomWatcher implements TextWatcher {
        private ViewHolderItem holder;
        private int type;

        private CustomWatcher(ViewHolderItem holder, int type) {
            this.holder = holder;
            this.type = type;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (type == VALUE_TITLE) {
                mList.get(getPosition()).setTitle(s.toString());
            } else {
                mList.get(getPosition()).setLink(s.toString());
                if (getPosition() != mList.size() - 1) {

                    if (!Utils.containsURL(s.toString())) {
                        holder.itemView.links.setError(mContext.getString(R.string.invalid_link));
                    } else {
                        holder.itemView.links.setError(null);
                    }
                }
            }
            dotsListener.changeSubmitButtonState(isAllDotsDetailFilled());
        }

        @Override
        public void afterTextChanged(Editable s) {
        }

        public int getPosition() {
            return holder.position;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final AddDotsAdapter.ViewHolderItem holder, final int position) {
        final AnnotatedPoint annotatedPoint = mList.get(position);
        holder.position = position;
        Drawable mDrawable = ContextCompat.getDrawable(mContext, R.drawable.circle_background);
        mDrawable.setColorFilter(new PorterDuffColorFilter(annotatedPoint.color, PorterDuff.Mode.SRC_IN));
        holder.itemView.dots.setBackground(mDrawable);
        ArrayAdapter<String> postTypeAdapter = new ArrayAdapter<String>(mContext, R.layout.spinner_item, mTypeList);
        holder.itemView.spinner.setAdapter(postTypeAdapter);

        holder.itemView.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int i, long id) {
                if (i == 0) {
                    ((TextView) selectedItemView).setTextColor(mContext.getResources().getColor(R.color.lightGrey));
                    ((TextView) selectedItemView).setTypeface(ResourcesCompat.getFont(mContext, R.font.raleway_regular));
                } else {
                    ((TextView) selectedItemView).setTextColor(mContext.getResources().getColor(R.color.category_dialog_color));
                    ((TextView) selectedItemView).setTypeface(ResourcesCompat.getFont(mContext, R.font.raleway_semi_bold));
                }

                if (i == Constants.DOT_TYPE_ONLINE_RETAILER) {
                    holder.itemView.listingLayout.setVisibility(View.GONE);
                    holder.itemView.titleLayout.setVisibility(View.VISIBLE);
                    mList.get(position).setPostDotTypeId(Constants.DOT_TYPE_ONLINE_RETAILER);
                } else if (i == Constants.DOT_TYPE_LISTING) {
                    holder.itemView.listingLayout.setVisibility(View.VISIBLE);
                    holder.itemView.titleLayout.setVisibility(View.GONE);
                    mList.get(position).setPostDotTypeId(Constants.DOT_TYPE_LISTING);
                } else {
                    holder.itemView.listingLayout.setVisibility(View.GONE);
                    holder.itemView.titleLayout.setVisibility(View.GONE);
                    mList.get(position).setPostDotTypeId(Constants.DOT_TYPE_DEFAULT);
                }
                dotsListener.changeSubmitButtonState(isAllDotsDetailFilled());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });
        if (!TextUtils.isEmpty(annotatedPoint.getTitle())) {
            holder.itemView.title.setText(annotatedPoint.getTitle());
        } else {
            holder.itemView.title.setText(null);
        }
        if (!TextUtils.isEmpty(annotatedPoint.getLink())) {
            holder.itemView.links.setText(annotatedPoint.getLink());
        } else {
            holder.itemView.links.setText(null);
        }
        holder.itemView.spinner.setSelection(annotatedPoint.getPostDotTypeId());

        holder.itemView.listingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectListingFromByn(position);
            }
        });

        if (!TextUtils.isEmpty(annotatedPoint.getListingTitle())) {
            holder.itemView.listingTitle.setText(annotatedPoint.getListingTitle());
        } else {
            holder.itemView.listingTitle.setText(mContext.getString(R.string.select_listing_from_byn));
        }
    }

    private void selectListingFromByn(int position) {
        mCallback.openSelectListingForDotsFragment(position, dotsListener);
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        DotsDetailItemBinding itemView;
        private int position;

        public ViewHolderItem(DotsDetailItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
            this.itemView.title.addTextChangedListener(new CustomWatcher(this, VALUE_TITLE));
            this.itemView.links.addTextChangedListener(new CustomWatcher(this, VALUE_URL));
        }
    }

    private boolean isAllDotsDetailFilled() {
        for (AnnotatedPoint dotDetail : mList) {
            if (dotDetail.getPostDotTypeId() == Constants.DOT_TYPE_DEFAULT) {
                return false;
            } else if (dotDetail.getPostDotTypeId() == Constants.DOT_TYPE_ONLINE_RETAILER) {
                if (TextUtils.isEmpty(dotDetail.getTitle())) {
                    return false;
                } else if (TextUtils.isEmpty(dotDetail.getLink())) {
                    return false;
                } else if (!Utils.containsURL(dotDetail.getLink())) {
                    return false;
                }
                if (!dotDetail.getLink().startsWith("http")) {
                    dotDetail.setLink("http://" + dotDetail.getLink());
                }
            } else {
                if (dotDetail.getListingId() == 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public List<AnnotatedPoint> getList() {
        return mList;
    }

}

