package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.databinding.AddDotsListingItemBinding;
import com.bynmix.app.databinding.ListingDotsHeaderBinding;
import com.bynmix.app.databinding.LoadMoreProgressBinding;
import com.bynmix.app.interfaces.AddTagsListener;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.FeedResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AddDotsListingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private List<FeedResponse> mList;
    private int TYPE_ITEM = 1;
    private int TYPE_PROGRESS = 0;
    private boolean isProgressRequired = false;
    private int extraCount = 0;
    private AddTagsListener listener;
    private CreatePostListener mCallback;
    private int finalPosition;

    public AddDotsListingAdapter(Context mContext, List<FeedResponse> mList, AddTagsListener listener, CreatePostListener mCallback, int position) {
        this.mContext = mContext;
        this.mList = mList;
        this.listener = listener;
        this.mCallback = mCallback;
        this.finalPosition = position;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_PROGRESS) {
            LoadMoreProgressBinding mBindingProgress = LoadMoreProgressBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new AddDotsListingAdapter.ViewHolderProgress(mBindingProgress);
        } else {
            AddDotsListingItemBinding mBindingItem = AddDotsListingItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new AddDotsListingAdapter.ViewHolderItem(mBindingItem);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof AddDotsListingAdapter.ViewHolderProgress) {
            position = position + 1;
            viewHolderProgress((AddDotsListingAdapter.ViewHolderProgress) holder);
        } else {
            viewHolderItem((AddDotsListingAdapter.ViewHolderItem) holder, position);
        }
    }


    private void viewHolderProgress(AddDotsListingAdapter.ViewHolderProgress holder) {
        if (isProgressRequired) {
            holder.itemView.loadMoreProgressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void viewHolderItem(final AddDotsListingAdapter.ViewHolderItem holder, final int position) {
        final FeedResponse listingDetail = mList.get(position);
        String photoUrl = listingDetail.getImageUrl();
        if (!TextUtils.isEmpty(photoUrl)) {
            Picasso.get().load(photoUrl).placeholder(R.mipmap.default_feed).into(holder.itemView.image);
        } else {
            holder.itemView.image.setImageResource(R.mipmap.default_feed);
        }
        holder.itemView.title.setText(listingDetail.getTitle());
        holder.itemView.mainItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(listingDetail, finalPosition);
                mCallback.onBackPressed();
            }
        });


    }


    @Override
    public int getItemCount() {
        return mList.size() + extraCount + 1;
    }


    public int getItemViewType(int position) {
        if (position == mList.size()) {
            return TYPE_PROGRESS;
        }
        return TYPE_ITEM;
    }

    public void setProgress(boolean progressStatus) {
        isProgressRequired = progressStatus;
        if (isProgressRequired) {
            extraCount = 1;
        } else {
            extraCount = 0;
        }
        notifyDataSetChanged();
    }

    public void setList(List<FeedResponse> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder {
        AddDotsListingItemBinding itemView;

        public ViewHolderItem(AddDotsListingItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    public class ViewHolderProgress extends RecyclerView.ViewHolder {
        LoadMoreProgressBinding itemView;

        public ViewHolderProgress(LoadMoreProgressBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

}
