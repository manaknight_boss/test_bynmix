package com.bynmix.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.databinding.ListingColorItemBinding;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.Colors;

import java.util.ArrayList;
import java.util.List;

public class AddListingColorAdapter extends RecyclerView.Adapter<AddListingColorAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<Colors> mColorsList;
    private CreatePostListener mCallback;
    private int type = 0;

    public AddListingColorAdapter(Context mContext, List<Colors> mColorsList, CreatePostListener mCallback, int type) {
        this.mContext = mContext;
        this.mColorsList = mColorsList;
        this.mCallback = mCallback;
        this.type = type;
    }

    @Override
    public AddListingColorAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        ListingColorItemBinding mBinding = ListingColorItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new AddListingColorAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(final AddListingColorAdapter.ViewHolderItem holder, int position) {

        final Colors colors = mColorsList.get(position);
        holder.itemView.colorName.setText(colors.getColorName());
        GradientDrawable drawable = (GradientDrawable) holder.itemView.color.getBackground();
        drawable.setColor(Color.parseColor("#" + colors.getHexCode()));
        if(type == 1) {
            holder.itemView.colorItem.setEnabled(true);
        } else {
            holder.itemView.colorItem.setEnabled(false);
        }
            holder.itemView.colorItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    List<Integer> colorsIds = new ArrayList<>();
                    for (int i = 0; i < mColorsList.size(); i++) {
                        colorsIds.add(mColorsList.get(i).getColorId());
                    }
                    mCallback.openColorFragment(colorsIds);
                }
            });
    }


    @Override
    public int getItemCount() {
        return mColorsList.size();
    }

    public void setList(List<Colors> colorsList) {
        this.mColorsList = colorsList;
        notifyDataSetChanged();

    }

    public class ViewHolderItem extends RecyclerView.ViewHolder {
        ListingColorItemBinding itemView;

        public ViewHolderItem(ListingColorItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

