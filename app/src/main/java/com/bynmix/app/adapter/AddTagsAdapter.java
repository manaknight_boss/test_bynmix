package com.bynmix.app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.bynmix.app.databinding.AddTagsFooterBinding;
import com.bynmix.app.databinding.AddTagsItemBinding;

import java.util.List;

public class AddTagsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private List<String> mTagsList;
    final private int TYPE_ITEM = 1;
    final private int TYPE_FOOTER = 0;

    public AddTagsAdapter(Context mContext, List<String> mTagsList) {
        this.mTagsList = mTagsList;
        this.mContext = mContext;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            AddTagsItemBinding mBindingItem = AddTagsItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new AddTagsAdapter.ViewHolderItem(mBindingItem);
        } else {
            AddTagsFooterBinding mBindingFooter = AddTagsFooterBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new AddTagsAdapter.ViewHolderFooter(mBindingFooter);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof AddTagsAdapter.ViewHolderFooter) {
            viewHolderFooter((AddTagsAdapter.ViewHolderFooter) holder);
        } else if (holder instanceof AddTagsAdapter.ViewHolderItem) {
            viewHolderItem((AddTagsAdapter.ViewHolderItem) holder, position - 1);
        }
    }


    private void viewHolderFooter(final AddTagsAdapter.ViewHolderFooter holder) {
        if (mTagsList.size() > 9) {
            holder.itemView.footerTagLayout.setVisibility(View.GONE);
        } else {
            holder.itemView.footerTagLayout.setVisibility(View.VISIBLE);
        }
        holder.itemView.footerTagLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTags(holder);
            }
        });

        holder.itemView.tag.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    holder.itemView.footerTagLayout.setVisibility(View.GONE);
                    holder.itemView.tag.setVisibility(View.VISIBLE);
                } else {
                    String text  = holder.itemView.tag.getText().toString().trim();
                    if(TextUtils.isEmpty(text)) {
                        holder.itemView.footerTagLayout.setVisibility(View.VISIBLE);
                        holder.itemView.tag.setVisibility(View.GONE);
                        holder.itemView.tag.clearFocus();
                    }
                }
            }
        });
    }

    private void addTags(final ViewHolderFooter holder) {
        holder.itemView.footerTagLayout.setVisibility(View.GONE);
        holder.itemView.tag.setVisibility(View.VISIBLE);
        ((AppCompatActivity) mContext).getWindow().getDecorView().clearFocus();
        InputMethodManager imm = (InputMethodManager)
                mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null) {
            imm.hideSoftInputFromWindow(holder.itemView.tag.getWindowToken(), 0);
        }
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
        holder.itemView.tag.requestFocus();
        holder.itemView.tag.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                String tag = holder.itemView.tag.getText().toString().trim();
                holder.itemView.footerTagLayout.setVisibility(View.VISIBLE);
                holder.itemView.tag.setVisibility(View.GONE);
                holder.itemView.tag.setText("");
                if (TextUtils.isEmpty(tag)) {
                    Toast.makeText(mContext, "Invalid Tag", Toast.LENGTH_SHORT).show();
                } else {
                    mTagsList.add(0,tag);
                    notifyDataSetChanged();
                }
                holder.itemView.footerTagLayout.requestFocus();
                InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                return false;
            }
        });

    }


    private void viewHolderItem(AddTagsAdapter.ViewHolderItem holder, final int position) {
        String tag = mTagsList.get(position);
        holder.itemView.tags.setText(tag + " x");
        holder.itemView.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTagsList.remove(position);
                notifyDataSetChanged();
            }
        });
        /*holder.itemView.crossIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTagsList.remove(position);
                notifyDataSetChanged();
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return mTagsList.size() + 1;
    }

    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_FOOTER;
        }

        return TYPE_ITEM;
    }

    public void setList(List<String> tagsList) {
        this.mTagsList = tagsList;
        notifyDataSetChanged();
    }


    class ViewHolderItem extends RecyclerView.ViewHolder {
        AddTagsItemBinding itemView;

        public ViewHolderItem(AddTagsItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderFooter extends RecyclerView.ViewHolder {
        AddTagsFooterBinding itemView;

        private ViewHolderFooter(AddTagsFooterBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}