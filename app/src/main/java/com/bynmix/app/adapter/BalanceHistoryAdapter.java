package com.bynmix.app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FollowFollowingItemBinding;
import com.bynmix.app.databinding.LoadMoreProgressBinding;
import com.bynmix.app.databinding.WithdrawHistoryItemBinding;
import com.bynmix.app.interfaces.FeedItemClickListener;
import com.bynmix.app.models.BalanceHistory;
import com.bynmix.app.models.UserResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BalanceHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private List<BalanceHistory> mList;
    private int TYPE_ITEM = 1;
    private int TYPE_PROGRESS = 0;
    private boolean isProgressRequired = false;
    private int extraCount = 0;

    public BalanceHistoryAdapter(Context mContext, List<BalanceHistory> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            WithdrawHistoryItemBinding mBindingItem = WithdrawHistoryItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new BalanceHistoryAdapter.ViewHolderItem(mBindingItem);
        } else if (viewType == TYPE_PROGRESS) {
            LoadMoreProgressBinding mBindingProgress = LoadMoreProgressBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new BalanceHistoryAdapter.ViewHolderProgress(mBindingProgress);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BalanceHistoryAdapter.ViewHolderProgress) {
            position = position + 1;
            viewHolderProgress((BalanceHistoryAdapter.ViewHolderProgress) holder);
        } else if (holder instanceof BalanceHistoryAdapter.ViewHolderItem) {
            viewHolderItem((BalanceHistoryAdapter.ViewHolderItem) holder, position);
        }
    }

    private void viewHolderProgress(BalanceHistoryAdapter.ViewHolderProgress holder) {
        if (isProgressRequired) {
            holder.itemView.loadMoreProgressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void viewHolderItem(final BalanceHistoryAdapter.ViewHolderItem holder, int position) {
        BalanceHistory history = mList.get(position);
        if (history.getStatusId() == BalanceHistory.IN_TRANSIT || history.getStatusId() == BalanceHistory.TYPE_PENDING) {
            holder.itemView.icon.setImageResource(R.mipmap.waiting);
        } else if (history.getStatusId() == BalanceHistory.TYPE_CANCELED || history.getStatusId() == BalanceHistory.TYPE_FAILED) {
            holder.itemView.icon.setImageResource(R.mipmap.fail);
        } else {
            holder.itemView.icon.setImageResource(R.mipmap.done);
        }
        holder.itemView.message.setText(Html.fromHtml(history.getFormattedDisplayText()), TextView.BufferType.SPANNABLE);
    }

    @Override
    public int getItemCount() {
        return mList.size() + extraCount;
    }


    public int getItemViewType(int position) {
        if (position == mList.size()) {
            return TYPE_PROGRESS;
        }
        return TYPE_ITEM;
    }

    public void setProgress(boolean progressStatus) {
        isProgressRequired = progressStatus;
        if (isProgressRequired) {
            extraCount = 1;
        } else {
            extraCount = 0;
        }
        notifyDataSetChanged();
    }

    public void setList(List<BalanceHistory> mUserList) {
        this.mList = mUserList;
        notifyDataSetChanged();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder {
        WithdrawHistoryItemBinding itemView;

        public ViewHolderItem(WithdrawHistoryItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderProgress extends RecyclerView.ViewHolder {
        LoadMoreProgressBinding itemView;

        public ViewHolderProgress(LoadMoreProgressBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

}
