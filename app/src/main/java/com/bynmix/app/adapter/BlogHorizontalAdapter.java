package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.databinding.BlogFooterItemBinding;
import com.bynmix.app.databinding.FeedBlogItemBinding;
import com.bynmix.app.interfaces.FeedItemClickListener;
import com.bynmix.app.models.UserResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BlogHorizontalAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private List<UserResponse> mUserList;
    private int TYPE_ITEM = 1;
    private int TYPE_FOOTER = 0;
    private int tabPosition;
    private FeedItemClickListener mCallback;

    public BlogHorizontalAdapter(Context mContext, List<UserResponse> mUserList, FeedItemClickListener mCallback) {
        this.mUserList = mUserList;
        this.mContext = mContext;
        this.mCallback = mCallback;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            FeedBlogItemBinding mBindingItem = FeedBlogItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderItem(mBindingItem);
        } else if (viewType == TYPE_FOOTER) {
            BlogFooterItemBinding mBindingFooter = BlogFooterItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderFooter(mBindingFooter);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderFooter) {
            position = position + 1;
            viewHolderFooter((ViewHolderFooter) holder);
        } else if (holder instanceof ViewHolderItem) {
            viewHolderItem((ViewHolderItem) holder, position);
        }
    }


    private void viewHolderFooter(final ViewHolderFooter holder) {
        if (mUserList.size() == 0) {
            holder.itemView.footerBlogLayout.setVisibility(View.GONE);
        } else {
            holder.itemView.footerBlogLayout.setVisibility(View.VISIBLE);
        }
        holder.itemView.footerBlogLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openSearchUserFragment();
            }
        });
    }


    private void viewHolderItem(ViewHolderItem holder, int position) {
        final UserResponse user = mUserList.get(position);
        String userName = user.getUsername();
        holder.itemView.userName.setText(userName);
        String photoUrl = user.getPhotoUrl();

        if (!TextUtils.isEmpty(photoUrl)) {
            Picasso.get().load(photoUrl).placeholder(R.mipmap.default_profile_image).into(holder.itemView.feedBlogPic);
        } else {
            holder.itemView.feedBlogPic.setImageResource(R.mipmap.default_profile_image);
        }

        holder.itemView.feedBlogItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openProfile(user.getUserId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return mUserList.size() + 1;
    }


    public int getItemViewType(int position) {
        if (position == mUserList.size()) {
            return TYPE_FOOTER;
        }

        return TYPE_ITEM;
    }

    public void setList(List<UserResponse> bestUser) {
        this.mUserList = bestUser;
        notifyDataSetChanged();
    }

    public void setTabPosition(int tabPosition) {
        this.tabPosition = tabPosition;
    }


    class ViewHolderItem extends RecyclerView.ViewHolder {
        FeedBlogItemBinding itemView;

        public ViewHolderItem(FeedBlogItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderFooter extends RecyclerView.ViewHolder {
        BlogFooterItemBinding itemView;

        private ViewHolderFooter(BlogFooterItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}