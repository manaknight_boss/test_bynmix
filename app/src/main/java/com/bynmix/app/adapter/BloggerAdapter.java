package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.databinding.BlogFooterItemBinding;
import com.bynmix.app.databinding.BlogItemBinding;
import com.bynmix.app.databinding.BloggerFooterBinding;
import com.bynmix.app.interfaces.BloggerListener;
import com.bynmix.app.models.UserResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class BloggerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private List<UserResponse> mUserList;
    private int TYPE_ITEM = 1;
    private int TYPE_FOOTER = 2;
    private int extraCount = 1;
    private BloggerListener bloggerListener;
    private boolean isSelected;
    private boolean isProgressRequired = false;

    public BloggerAdapter(Context mContext, List<UserResponse> mUserList, BloggerListener mCallback) {
        this.mContext = mContext;
        this.mUserList = mUserList;
        this.bloggerListener = mCallback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            BlogItemBinding mBindingItem = BlogItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new BloggerAdapter.ViewHolderItem(mBindingItem);
        } else if (viewType == TYPE_FOOTER) {
            BloggerFooterBinding mBindingFooter = BloggerFooterBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new BloggerAdapter.ViewHolderFooter(mBindingFooter);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BloggerAdapter.ViewHolderFooter) {
            position = position + 1;
            viewHolderFooter((BloggerAdapter.ViewHolderFooter) holder, position);
        } else if (holder instanceof BloggerAdapter.ViewHolderItem) {
            viewHolderItem((BloggerAdapter.ViewHolderItem) holder, position);
        }
    }

    private void viewHolderFooter(ViewHolderFooter holder, int position) {
        if (isProgressRequired) {
            holder.itemView.loadMoreProgressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void viewHolderItem(final BloggerAdapter.ViewHolderItem holder, int position) {
        final UserResponse user = mUserList.get(position);
        holder.itemView.userName.setText(user.getUsername());

        if (!TextUtils.isEmpty(user.getTitle())) {
            holder.itemView.blogerText.setVisibility(View.VISIBLE);
            holder.itemView.blogerText.setText(user.getTitle());
        } else {
            holder.itemView.blogerText.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(user.getUserDescriptorsFormatted())) {
            holder.itemView.stylesLayout.setVisibility(View.VISIBLE);
            holder.itemView.styles.setText(user.getUserDescriptorsFormatted());
        } else {
            holder.itemView.stylesLayout.setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(user.getUserBrandsFormatted())) {
            holder.itemView.brandLayout.setVisibility(View.VISIBLE);
            holder.itemView.brands.setText(user.getUserBrandsFormatted());
        } else {
            holder.itemView.brandLayout.setVisibility(View.GONE);
        }


        String photoUrl = user.getPhotoUrl();

        if (!TextUtils.isEmpty(photoUrl)) {
            Picasso.get().load(photoUrl).placeholder(R.mipmap.default_profile_image).into(holder.itemView.blogPic);
        } else {
            holder.itemView.blogPic.setImageResource(R.mipmap.default_profile_image);
        }
        updateListUI(holder, user);
        holder.itemView.selectBlogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bloggerListener.onClickBlogger(user);
            }
        });
    }

    private void updateListUI(ViewHolderItem holder, UserResponse user) {
        if (user.isSelected()) {
            holder.itemView.selectBlogButtonText.setText(mContext.getResources().getString(R.string.deselect));
            holder.itemView.selectBlogButtonText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 8);
            holder.itemView.selectBlogButton.setBackground(mContext.getResources().getDrawable(R.drawable.disable_onboarding_button));
            holder.itemView.selectBlogButton.setEnabled(false);
        } else {
            holder.itemView.selectBlogButtonText.setText(mContext.getResources().getString(R.string.select));
            holder.itemView.selectBlogButtonText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 9);
            holder.itemView.selectBlogButton.setBackground(mContext.getResources().getDrawable(R.drawable.purple_rounded_button));
            holder.itemView.selectBlogButton.setEnabled(true);
        }

    }

    @Override
    public int getItemCount() {
        return mUserList.size() + extraCount;
    }


    public int getItemViewType(int position) {
        if (position == mUserList.size()) {
            return TYPE_FOOTER;
        }

        return TYPE_ITEM;
    }

    public void setList(List<UserResponse> mUserList) {
        this.mUserList = mUserList;
        notifyDataSetChanged();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder {
        BlogItemBinding itemView;

        public ViewHolderItem(BlogItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public class ViewHolderFooter extends RecyclerView.ViewHolder {
        BloggerFooterBinding itemView;

        public ViewHolderFooter(BloggerFooterBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    public void setProgress(boolean progressStatus) {
        isProgressRequired = progressStatus;
        notifyDataSetChanged();
    }
}
