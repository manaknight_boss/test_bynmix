package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.databinding.BrandItemBinding;
import com.bynmix.app.interfaces.BrandListener;
import com.bynmix.app.models.Brand;

import java.util.List;

public class BrandAdapter extends RecyclerView.Adapter<BrandAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<Brand> mBrandList;
    private BrandListener brandListener;

    public BrandAdapter(Context mContext, List<Brand> mBrandList, BrandListener brandListener) {
        this.mContext = mContext;
        this.mBrandList = mBrandList;
        this.brandListener = brandListener;
    }

    @Override
    public BrandAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        BrandItemBinding mBinding = BrandItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new BrandAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(BrandAdapter.ViewHolderItem holder, int position) {
        final Brand brand = mBrandList.get(position);
        String brandName = brand.getBrandName();
        if (!TextUtils.isEmpty(brandName)) {
            holder.itemView.name.setText(brandName);
        }
        updateUI(holder, brand.isSelected());
        holder.itemView.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                brandListener.onClickBrand(brand);
            }
        });
    }

    private void updateUI(ViewHolderItem holder, boolean isSelected) {
        if (isSelected) {
            holder.itemView.item.setBackgroundColor(mContext.getResources().getColor(R.color.purple));
            holder.itemView.name.setTextColor(mContext.getResources().getColor(R.color.white));
            holder.itemView.heart.setImageResource(R.mipmap.heart_pink_on_boarding);
        } else {
            holder.itemView.item.setBackgroundColor(mContext.getResources().getColor(R.color.on_boarding_item_background));
            holder.itemView.name.setTextColor(mContext.getResources().getColor(R.color.purple));
            holder.itemView.heart.setImageResource(R.mipmap.heart_on_boarding);
        }
    }

    @Override
    public int getItemCount() {
        return mBrandList.size();
    }

    public void setList(List<Brand> defaultBrandList) {
        this.mBrandList = defaultBrandList;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {

        BrandItemBinding itemView;

        public ViewHolderItem(BrandItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
