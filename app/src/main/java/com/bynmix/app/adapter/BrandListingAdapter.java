package com.bynmix.app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.BrandFragmentItemBinding;
import com.bynmix.app.databinding.BrandItemBinding;
import com.bynmix.app.databinding.CategoryItemBinding;
import com.bynmix.app.databinding.ProfileIconsBinding;
import com.bynmix.app.interfaces.BrandListener;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.Brand;

import java.util.ArrayList;
import java.util.List;

public class BrandListingAdapter extends RecyclerView.Adapter<BrandListingAdapter.ViewHolderItem> implements SectionIndexer {

    private final Context mContext;
    private List<Brand> mBrandList;
    private final CreatePostListener mCallback;
    private ArrayList<Integer> mSectionPositions;

    public BrandListingAdapter(Context mContext, List<Brand> mBrandList, CreatePostListener mCallback) {
        this.mContext = mContext;
        this.mBrandList = mBrandList;
        this.mCallback = mCallback;

    }

    @Override
    public BrandListingAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        BrandFragmentItemBinding mBinding = BrandFragmentItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new BrandListingAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final BrandListingAdapter.ViewHolderItem holder, final int position) {
        final Brand brand = mBrandList.get(position);
        holder.itemView.brand.setText(brand.getBrandName());
        String brandName = brand.getBrandName();
        if (brand.isSelected()) {
            brandName = "<b>" + "<font color='" + mContext.getResources().getColor(R.color.purple) + "'>" + brandName + "</font>" + "</b> ";
            holder.itemView.brand.setText(Html.fromHtml(brandName), TextView.BufferType.SPANNABLE);
        } else {
            brandName = "<font color='" + mContext.getResources().getColor(R.color.colorPrimary) + "'>" + brandName + "</font>";
            holder.itemView.brand.setText(Html.fromHtml(brandName), TextView.BufferType.SPANNABLE);
        }
        holder.itemView.brandLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetBrandListSelection();
                brand.setSelected(true);
//                mCallback.onClickBrand(brand);
                mCallback.setBrandInListingDetail(brand);
                mCallback.onBackPressed();
//                notifyDataSetChanged();
            }
        });

    }

    private void resetBrandListSelection() {
        for (Brand brand : mBrandList) {
            brand.setSelected(false);
        }
    }

    @Override
    public int getItemCount() {
        return mBrandList.size();
    }

    public void setList(List<Brand> brandList) {
        this.mBrandList = brandList;
        notifyDataSetChanged();
    }

    @Override
    public int getSectionForPosition(int position) {
        return 0;
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>(26);
        mSectionPositions = new ArrayList<>(26);
        for (int i = 0, size = mBrandList.size(); i < size; i++) {
            String section = String.valueOf(mBrandList.get(i).getBrandName().charAt(0)).toUpperCase();
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int sectionIndex) {
        return mSectionPositions.get(sectionIndex);
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        BrandFragmentItemBinding itemView;

        public ViewHolderItem(BrandFragmentItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
