package com.bynmix.app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ahmadrosid.svgloader.SvgLoader;
import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.databinding.CategoryFragmentItemBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.CategoryType;
import com.bynmix.app.utils.Utils;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<CategoryType> mCategoryTypeList;
    private final ProfileListener mCallback;
    private final int cellSize;
    public CategoryAdapter(Context mContext, List<CategoryType> mCategoryTypeList, ProfileListener mCallback) {
        this.mContext = mContext;
        this.mCategoryTypeList = mCategoryTypeList;
        this.mCallback = mCallback;
        this.cellSize = Utils.getScreenWidth(mContext ) / 3;

    }

    @Override
    public CategoryAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        CategoryFragmentItemBinding mBinding = CategoryFragmentItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) mBinding.getRoot().getLayoutParams();
        layoutParams.height = cellSize;
        layoutParams.width = cellSize;
        layoutParams.setFullSpan(false);
        mBinding.getRoot().setLayoutParams(layoutParams);
        return new CategoryAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final CategoryAdapter.ViewHolderItem holder, final int position) {
        final CategoryType categoryType = mCategoryTypeList.get(position);
        String photoUrl = categoryType.getDisplayImage();

        if (!TextUtils.isEmpty(photoUrl)) {
            SvgLoader.pluck()
                    .with((HomeActivity)mContext)
                    .load(photoUrl, holder.itemView.pic);
        }

        holder.itemView.categoryName.setText(categoryType.getCategoryTypeName());
        if(categoryType.isSelected()) {
            holder.itemView.categoryLayout.setBackground(mContext.getResources().getDrawable(R.drawable.purple_out_line_rectangle_background));
        } else {
            holder.itemView.categoryLayout.setBackground(mContext.getResources().getDrawable(R.drawable.color_layout_background));
        }


        holder.itemView.categoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                resetCategoryListSelection();
                categoryType.setSelected(true);
                mCallback.setCategory(null);
                mCallback.showCategoryTick(categoryType);
                notifyDataSetChanged();
            }
        });
    }

    private void resetCategoryListSelection() {
        for(CategoryType type : mCategoryTypeList){
            type.setSelected(false);
        }
    }

    @Override
    public int getItemCount() {
        return mCategoryTypeList.size();
    }

    public void setList(List<CategoryType> categoryTypeList) {
        this.mCategoryTypeList = categoryTypeList;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        CategoryFragmentItemBinding itemView;

        public ViewHolderItem(CategoryFragmentItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
