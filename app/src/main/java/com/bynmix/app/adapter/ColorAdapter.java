package com.bynmix.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.databinding.ColorItemBinding;
import com.bynmix.app.databinding.ProfileIconsBinding;
import com.bynmix.app.interfaces.ColorListener;
import com.bynmix.app.models.Colors;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ColorAdapter extends RecyclerView.Adapter<ColorAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<Colors> mColorList;
    private List<Colors> selectedColor = new ArrayList<>();
    private final ColorListener mCallback;
    private final int cellSize;

    public ColorAdapter(Context mContext, List<Colors> mColorList, ColorListener mCallback) {
        this.mContext = mContext;
        this.mColorList = mColorList;
        this.mCallback = mCallback;
        this.cellSize = Utils.getScreenWidth(mContext ) / 3;

    }

    @Override
    public ColorAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        ColorItemBinding mBinding = ColorItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) mBinding.getRoot().getLayoutParams();
        layoutParams.height = cellSize;
        layoutParams.width = cellSize;
        layoutParams.setFullSpan(false);
        mBinding.getRoot().setLayoutParams(layoutParams);
        return new ColorAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ColorAdapter.ViewHolderItem holder, final int position) {
        final Colors colors = mColorList.get(position);
        holder.itemView.colorName.setText(colors.getColorName());
        GradientDrawable drawable = (GradientDrawable) holder.itemView.color.getBackground();
        drawable.setColor(Color.parseColor("#" + colors.getHexCode()));
        if (colors.isSelectedColor()) {
            holder.itemView.colorLayout.setBackground(mContext.getResources().getDrawable(R.drawable.purple_out_line_rectangle_background));
            selectedColor.add(colors);
        } else {
            holder.itemView.colorLayout.setBackground(mContext.getResources().getDrawable(R.drawable.color_layout_background));
        }
        holder.itemView.colorLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (colors.isSelectedColor()) {
                    colors.setSelectedColor(false);
                    selectedColor.remove(colors);
                    mCallback.addColor(selectedColor);
                    holder.itemView.colorLayout.setBackground(mContext.getResources().getDrawable(R.drawable.color_layout_background));
                } else {
                    if (selectedColor.size() < 2) {
                        colors.setSelectedColor(true);
                        selectedColor.add(colors);
                        mCallback.addColor(selectedColor);
                        holder.itemView.colorLayout.setBackground(mContext.getResources().getDrawable(R.drawable.purple_out_line_rectangle_background));
                    } else {
                        Toast.makeText(mContext, mContext.getString(R.string.max_color_error), Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return mColorList.size();
    }

    public void setList(List<Colors> colorsList) {
        this.mColorList = colorsList;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ColorItemBinding itemView;

        public ViewHolderItem(ColorItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
