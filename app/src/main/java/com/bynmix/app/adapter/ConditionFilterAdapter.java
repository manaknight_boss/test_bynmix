package com.bynmix.app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FilterBrandItemBinding;
import com.bynmix.app.interfaces.FilterListener;
import com.bynmix.app.models.Category;
import com.bynmix.app.models.Condition;
import com.bynmix.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class ConditionFilterAdapter extends RecyclerView.Adapter<ConditionFilterAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<Condition> conditionList;
    private final FilterListener mCallback;

    public ConditionFilterAdapter(Context mContext, List<Condition> conditionList, FilterListener mCallback) {
        this.mContext = mContext;
        this.conditionList = conditionList;
        this.mCallback = mCallback;
    }

    @Override
    public ConditionFilterAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        FilterBrandItemBinding mBinding = FilterBrandItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new ConditionFilterAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ConditionFilterAdapter.ViewHolderItem holder, final int position) {

        final Condition condition = conditionList.get(position);
        String filterName = condition.getConditionName();
        holder.itemView.item.setText(filterName);
        if (condition.isConditionSelected()) {
            filterName = "<b>" + "<font color='" + mContext.getResources().getColor(R.color.purple) + "'>" + filterName + "</font>" + "</b> ";
            holder.itemView.item.setText(Html.fromHtml(filterName), TextView.BufferType.SPANNABLE);
        } else {
            filterName = "<font color='" + mContext.getResources().getColor(R.color.colorPrimary) + "'>" + filterName + "</font>";
            holder.itemView.item.setText(Html.fromHtml(filterName), TextView.BufferType.SPANNABLE);
        }

        holder.itemView.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetListSelection();
                condition.setConditionSelected(true);
                mCallback.setSelectedView(condition.getConditionId(), condition.getConditionName(), Constants.CONDITION_FILTER, 0, new ArrayList<Category>(), new ArrayList<Integer>());
                notifyDataSetChanged();
            }
        });
    }

    private void resetListSelection() {
        for (Condition condition : conditionList) {
            condition.setConditionSelected(false);
        }
    }

    @Override
    public int getItemCount() {
        return conditionList.size();
    }

    public void setList(List<Condition> mConditionList) {
        this.conditionList = mConditionList;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        FilterBrandItemBinding itemView;

        public ViewHolderItem(FilterBrandItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

