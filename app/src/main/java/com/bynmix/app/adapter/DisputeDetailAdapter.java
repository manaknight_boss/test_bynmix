package com.bynmix.app.adapter;


import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.databinding.DisputeCommentItemBinding;
import com.bynmix.app.databinding.DisputeDetailHeaderBinding;
import com.bynmix.app.fragments.EnlargeDisputeImageDialogFragment;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.interfaces.DisputeDetailListener;
import com.bynmix.app.models.Comments;
import com.bynmix.app.models.DisputeDetails;
import com.bynmix.app.utils.DialogAlert;
import com.squareup.picasso.Picasso;

public class DisputeDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    final private int TYPE_ITEM = 1;
    final private int TYPE_HEADER = 0;
    private DisputeDetailListener mCallback;
    private DisputeDetails disputeDetail;

    public DisputeDetailAdapter(Context mContext, DisputeDetails disputeDetail, DisputeDetailListener mCallback) {
        this.disputeDetail = disputeDetail;
        this.mContext = mContext;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            DisputeCommentItemBinding disputeCommentItemBinding = DisputeCommentItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new DisputeDetailAdapter.ViewHolderItem(disputeCommentItemBinding);
        } else {
            DisputeDetailHeaderBinding disputeDetailHeaderBinding = DisputeDetailHeaderBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new DisputeDetailAdapter.ViewHolderHeader(disputeDetailHeaderBinding);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof DisputeDetailAdapter.ViewHolderHeader) {
            viewHolderHeader((DisputeDetailAdapter.ViewHolderHeader) holder);
        } else if (holder instanceof DisputeDetailAdapter.ViewHolderItem) {
            viewHolderItem((DisputeDetailAdapter.ViewHolderItem) holder, position - 1);
        }
    }


    private void viewHolderHeader(final DisputeDetailAdapter.ViewHolderHeader holder) {

        holder.itemView.status.setText(disputeDetail.getDisputeStatus());
        holder.itemView.date.setText("Submitted on " + disputeDetail.getCreatedDateFormatted());
        holder.itemView.reason.setText(disputeDetail.getDisputeType());
        if (disputeDetail.isCanCancelDispute()) {
            holder.itemView.cancelDisputeButton.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.cancelDisputeButton.setVisibility(View.GONE);
        }
        holder.itemView.cancelDisputeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogAlert.showDialog(mContext, disputeDetail.getListingTitle(), mContext.getResources().getString(R.string.are_you_sure_cancel_dispute), mContext.getResources().getString(R.string.you_can_not_open_other_dispute), R.string.yes_cancel_dispute, new DialogClickListener() {
                    @Override
                    public void onClick() {
                        mCallback.cancelDispute();
                    }
                });
            }
        });

        if (TextUtils.isEmpty(disputeDetail.getComments())) {
            holder.itemView.commentLayout.setVisibility(View.GONE);
        } else {
            holder.itemView.commentLayout.setVisibility(View.VISIBLE);
            holder.itemView.comment.setText(disputeDetail.getComments());
        }
        if (disputeDetail.getImages() != null && disputeDetail.getImages().size() > 0) {
            holder.itemView.imagesLayout.setVisibility(View.VISIBLE);
            addImages(holder);
        } else {
            holder.itemView.imagesLayout.setVisibility(View.GONE);
        }
        if(disputeDetail.getAdditionalComments() != null && disputeDetail.getAdditionalComments().size() > 0) {
            holder.itemView.additionalComments.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.additionalComments.setVisibility(View.GONE);
        }
    }

    private void addImages(ViewHolderHeader holder) {
        int count = disputeDetail.getImages().size();
        if (count == 4) {
            Picasso.get().load(disputeDetail.getImages().get(0)).placeholder(R.mipmap.default_feed).into(holder.itemView.addPhoto1);
            Picasso.get().load(disputeDetail.getImages().get(1)).placeholder(R.mipmap.default_feed).into(holder.itemView.addPhoto2);
            Picasso.get().load(disputeDetail.getImages().get(2)).placeholder(R.mipmap.default_feed).into(holder.itemView.addPhoto3);
            Picasso.get().load(disputeDetail.getImages().get(3)).placeholder(R.mipmap.default_feed).into(holder.itemView.addPhoto4);
        } else if (count == 3) {
            Picasso.get().load(disputeDetail.getImages().get(0)).placeholder(R.mipmap.default_feed).into(holder.itemView.addPhoto1);
            Picasso.get().load(disputeDetail.getImages().get(1)).placeholder(R.mipmap.default_feed).into(holder.itemView.addPhoto2);
            Picasso.get().load(disputeDetail.getImages().get(2)).placeholder(R.mipmap.default_feed).into(holder.itemView.addPhoto3);
            holder.itemView.addPhoto4Layout.setVisibility(View.INVISIBLE);
        } else if (count == 2) {
            Picasso.get().load(disputeDetail.getImages().get(0)).placeholder(R.mipmap.default_feed).into(holder.itemView.addPhoto1);
            Picasso.get().load(disputeDetail.getImages().get(1)).placeholder(R.mipmap.default_feed).into(holder.itemView.addPhoto2);
            holder.itemView.addPhoto3Layout.setVisibility(View.INVISIBLE);
            holder.itemView.addPhoto4Layout.setVisibility(View.INVISIBLE);
        } else if (count == 1) {
            Picasso.get().load(disputeDetail.getImages().get(0)).placeholder(R.mipmap.default_feed).into(holder.itemView.addPhoto1);
            holder.itemView.addPhoto2Layout.setVisibility(View.INVISIBLE);
            holder.itemView.addPhoto3Layout.setVisibility(View.INVISIBLE);
            holder.itemView.addPhoto4Layout.setVisibility(View.INVISIBLE);
        }

        if (count > 0) {
            holder.itemView.addPhoto1Layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    enlargeImage(disputeDetail.getImages().get(0));
                }
            });
        }

        if (count > 1) {
            holder.itemView.addPhoto2Layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    enlargeImage(disputeDetail.getImages().get(1));
                }
            });
        }

        if (count > 2) {
            holder.itemView.addPhoto3Layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    enlargeImage(disputeDetail.getImages().get(2));
                }
            });
        }

        if (count > 3) {
            holder.itemView.addPhoto4Layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    enlargeImage(disputeDetail.getImages().get(3));
                }
            });
        }

    }

    private void enlargeImage(String image) {
        FragmentTransaction ft = ((HomeActivity) mContext).getFragmentManager().beginTransaction();
        ft.addToBackStack(null);
        DialogFragment newFragment = EnlargeDisputeImageDialogFragment.newInstance(image);
        newFragment.show(ft, "dialog");

    }

    private void viewHolderItem(DisputeDetailAdapter.ViewHolderItem holder, final int position) {
        final Comments comment = disputeDetail.getAdditionalComments().get(position);
        if(comment.isChargeDisputeEvidenceComment()) {
            holder.itemView.chargeDisputeLayout.setVisibility(View.VISIBLE);
            holder.itemView.commentLayout.setVisibility(View.GONE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                holder.itemView.chargeDisputeComment.setText(Html.fromHtml(comment.getComment(), Html.FROM_HTML_MODE_COMPACT));
            } else {
                holder.itemView.chargeDisputeComment.setText(Html.fromHtml(comment.getComment()));
            }
        } else {
            holder.itemView.chargeDisputeLayout.setVisibility(View.GONE);
            holder.itemView.commentLayout.setVisibility(View.VISIBLE);
            holder.itemView.comment.setText(comment.getComment());
            if (comment.isBuyerComment()) {
                holder.itemView.buyerNameLayout.setVisibility(View.VISIBLE);
                holder.itemView.sellerNameLayout.setVisibility(View.GONE);
                holder.itemView.tringleLeft.setVisibility(View.VISIBLE);
                holder.itemView.tringleRight.setVisibility(View.GONE);
            } else {
                holder.itemView.buyerNameLayout.setVisibility(View.GONE);
                holder.itemView.sellerNameLayout.setVisibility(View.VISIBLE);
                holder.itemView.tringleLeft.setVisibility(View.GONE);
                holder.itemView.tringleRight.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        int size = disputeDetail.getAdditionalComments().size() + 1;
        return size;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }

        return TYPE_ITEM;
    }


    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        DisputeCommentItemBinding itemView;

        public ViewHolderItem(DisputeCommentItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderHeader extends RecyclerView.ViewHolder {
        DisputeDetailHeaderBinding itemView;

        private ViewHolderHeader(DisputeDetailHeaderBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}