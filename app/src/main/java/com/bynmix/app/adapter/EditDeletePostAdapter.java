package com.bynmix.app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.databinding.EditPostItemBinding;
import com.bynmix.app.databinding.LoadMoreProgressBinding;
import com.bynmix.app.interfaces.DotsSingleTabInterface;
import com.bynmix.app.interfaces.EditPostListener;
import com.bynmix.app.interfaces.EnableDisableDialog;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.DialogAlert;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class EditDeletePostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private List<FeedResponse> mPostList;
    private final int TYPE_ITEM = 1;
    private final int TYPE_PROGRESS = 0;
    private boolean isProgressRequired = false;
    private int extraCount = 0;
    private ProfileListener mCallback;
    private EditPostListener mListener;

    public EditDeletePostAdapter(Context mContext, List<FeedResponse> mPostList, ProfileListener mCallback, EditPostListener mListener) {
        this.mContext = mContext;
        this.mPostList = mPostList;
        this.mCallback = mCallback;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            EditPostItemBinding mBindingItem = EditPostItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new EditDeletePostAdapter.ViewHolderItem(mBindingItem);
        } else {
            LoadMoreProgressBinding mBindingProgress = LoadMoreProgressBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderProgress(mBindingProgress);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderProgress) {
            position = position + 1;
            viewHolderProgress((ViewHolderProgress) holder);
        } else if (holder instanceof ViewHolderItem) {
            viewHolderItem((ViewHolderItem) holder, position);
        }
    }

    private void viewHolderProgress(ViewHolderProgress holder) {
        if (isProgressRequired) {
            holder.itemView.loadMoreProgressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void viewHolderItem(final ViewHolderItem holder, final int position) {
        final FeedResponse post = mPostList.get(position);
        String postTitle = post.getTitle();
        holder.itemView.postTitle.setText(postTitle);
        holder.itemView.time.setText(post.getCreatedDateFormatted());

        holder.itemView.blogFullImageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (post.isActive()) {
                    mCallback.openPostDetailFragment(post);
                } else {
                    String postTitle = getColorText(post.getTitle().trim());
                    postTitle = mContext.getString(R.string.the_post_detail_for) + " \"" + postTitle.trim() + "\" " + mContext.getString(R.string.is_currently_disabled);
                    DialogAlert.showDialog(mContext, postTitle, Constants.ENABLE, Constants.TYPE_POST, true, new EnableDisableDialog() {
                        @Override
                        public void onClickListener() {
                            mListener.disablePost(post.getId(), position, Constants.ENABLE);
                        }

                        @Override
                        public void onDeleteListener() {
                            mListener.deletePost(post.getId(), position);
                        }
                    });
                }
            }
        });

        holder.itemView.deletePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String postTitle = getColorText(post.getTitle());
                postTitle = mContext.getString(R.string.are_you_sure_to_delete) + " \"" + postTitle.trim() + "\" ?";
                final boolean isActive = post.isActive();
                DialogAlert.showDialog(mContext, postTitle, Constants.DELETE, Constants.TYPE_POST, isActive, new EnableDisableDialog() {
                    @Override
                    public void onClickListener() {
                        if (isActive) {
                            mListener.disablePost(post.getId(), position, Constants.DISABLE);
                        } else {
                            mListener.disablePost(post.getId(), position, Constants.ENABLE);
                        }
                    }

                    @Override
                    public void onDeleteListener() {
                        mListener.deletePost(post.getId(), position);
                    }
                });
            }
        });

        holder.itemView.statusLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String postTitle = getColorText(post.getTitle());
                if (post.isActive()) {
                    postTitle = mContext.getString(R.string.are_you_sure_to_disable) + " \"" + postTitle.trim() + "\" ?";
                    DialogAlert.showDialog(mContext, postTitle, Constants.DISABLE, Constants.TYPE_POST, true, new EnableDisableDialog() {
                        @Override
                        public void onClickListener() {
                            mListener.disablePost(post.getId(), position, Constants.DISABLE);
                        }

                        @Override
                        public void onDeleteListener() {
                            mListener.deletePost(post.getId(), position);
                        }
                    });

                } else {
                    mListener.disablePost(post.getId(), position, Constants.ENABLE);
                }
            }
        });
        updateStatusUI(holder, post.isActive());
        holder.itemView.editPostLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openEditMyPostFragment(post);
            }
        });
        String photoUrl = post.getPhotoUrl();
        if (!TextUtils.isEmpty(photoUrl)) {
            if (post.getPostType() != null && post.getPostType().equalsIgnoreCase(Constants.VIDEO_POST)) {
                holder.itemView.videoPlayIcon.setVisibility(View.VISIBLE);
                Picasso.get().load(photoUrl).placeholder(R.mipmap.novideo).into(holder.itemView.blogFullImage);
                Picasso.get().load(photoUrl).placeholder(R.mipmap.novideo).into(holder.itemView.dotsFullImage);
            } else {
                holder.itemView.videoPlayIcon.setVisibility(View.GONE);
                Picasso.get().load(photoUrl).placeholder(R.mipmap.default_feed).into(holder.itemView.blogFullImage);
                Picasso.get().load(photoUrl).placeholder(R.mipmap.default_feed).into(holder.itemView.dotsFullImage);
            }

            if (post.getPostDots().size() == 0) {
                holder.itemView.dotsFullImageParent.setVisibility(View.INVISIBLE);
                holder.itemView.blogFullImage.setVisibility(View.VISIBLE);
                if (post.isHasPostDots()) {
                    holder.itemView.viewTagText.setText(mContext.getString(R.string.tap_to_view_tagged));
                    holder.itemView.viewTags.setVisibility(View.VISIBLE);
                } else {
                    holder.itemView.viewTags.setVisibility(View.GONE);
                }
            } else {
                holder.itemView.viewTags.setVisibility(View.VISIBLE);
                if (post.isVisibleDots()) {
                    holder.itemView.viewTagText.setText(mContext.getString(R.string.hide_tagged_products));
                    holder.itemView.dotsFullImageParent.setVisibility(View.VISIBLE);
                    holder.itemView.dotsFullImage.setViewDraw(true);
                    holder.itemView.dotsFullImage.setPointList(post.getPostDots());
                    holder.itemView.blogFullImage.setVisibility(View.INVISIBLE);
                } else {
                    holder.itemView.viewTagText.setText(mContext.getString(R.string.tap_to_view_tagged));
                    holder.itemView.dotsFullImage.setPointList(new ArrayList<AnnotatedPoint>());
                    holder.itemView.dotsFullImageParent.setVisibility(View.INVISIBLE);
                    holder.itemView.blogFullImage.setVisibility(View.VISIBLE);
                }
                holder.itemView.dotsFullImage.setDotsListener(new DotsSingleTabInterface() {
                    @Override
                    public void singleTab(AnnotatedPoint annotatedPoint) {
                        if (annotatedPoint.getPostDotTypeId() == Constants.DOT_TYPE_LISTING) {
                            mCallback.openListingDetailFragment(annotatedPoint.getListingId());
                        } else {
                            mCallback.openInAppBrowser(annotatedPoint.getLink(), annotatedPoint.getTitle());
                        }
                    }

                    @Override
                    public void singleTab() {
                        postClicked(post);
                    }
                });
            }
        } else {
            if (post.getPostType() != null && post.getPostType().equalsIgnoreCase(Constants.VIDEO_POST)) {
                holder.itemView.blogFullImage.setImageResource(R.mipmap.novideo);
            } else {
                holder.itemView.blogFullImage.setImageResource(R.mipmap.default_feed);
            }
            holder.itemView.dotsFullImageParent.setVisibility(View.INVISIBLE);
        }
        showIndividualProgress(holder, false);
        holder.itemView.viewTags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                post.setVisibleDots(!post.isVisibleDots());
                if (post.getPostDots().size() == 0) {
                    showIndividualProgress(holder, true);
                    mCallback.getPostTags(position, post.getId(), -2);
                } else {
                    hideProgress();
                }
            }
        });
    }

    private void showIndividualProgress(ViewHolderItem holder, boolean isIndividualProgressRequired) {
        if (isIndividualProgressRequired) {
            holder.itemView.progressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.progressBar.setVisibility(View.GONE);
        }
    }

    private void postClicked(FeedResponse post) {
        if (post.getLink() != null)
            mCallback.openPostDetailFragment(post);
    }

    private void updateStatusUI(ViewHolderItem holder, boolean active) {
        if (!active) {
            holder.itemView.status.setText(mContext.getResources().getString(R.string.enable));
            holder.itemView.statusLayout.setBackgroundColor(mContext.getResources().getColor(R.color.orange_transparent));
        } else {
            holder.itemView.status.setText(mContext.getResources().getString(R.string.disable));
            holder.itemView.statusLayout.setBackgroundColor(mContext.getResources().getColor(R.color.red_transparent));
        }
    }

    @Override
    public int getItemCount() {
        return mPostList.size() + extraCount;
    }


    public int getItemViewType(int position) {
        if (position == mPostList.size()) {
            return TYPE_PROGRESS;
        }
        return TYPE_ITEM;
    }

    public void setProgress(boolean progressStatus) {
        isProgressRequired = progressStatus;
        if (isProgressRequired) {
            extraCount = 1;
        } else {
            extraCount = 0;
        }
        notifyDataSetChanged();
    }

    public void setList(List<FeedResponse> mPostList) {
        this.mPostList = mPostList;
        notifyDataSetChanged();
    }

    public void hideProgress() {
        notifyDataSetChanged();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder {
        EditPostItemBinding itemView;

        public ViewHolderItem(EditPostItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderProgress extends RecyclerView.ViewHolder {
        LoadMoreProgressBinding itemView;

        private ViewHolderProgress(LoadMoreProgressBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private String getColorText(String string) {
        return "<b>" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + string + "</font>" + "</b> ";

    }

}



