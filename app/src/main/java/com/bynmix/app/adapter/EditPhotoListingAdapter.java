package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.databinding.SelectedPhotoListingHeaderBinding;
import com.bynmix.app.databinding.SelectedPhotoListingItemBinding;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.models.ListingDisplayImage;
import com.bynmix.app.utils.Constants;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

public class EditPhotoListingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private List<ListingDisplayImage> mPhotoList;
    final private int TYPE_ITEM = 1;
    final private int TYPE_FOOTER = 0;
    final private int MAX_IMAGE_LIMIT = 4;
    private CreatePostListener mCallback;
    private DialogClickListener mListener;

    public EditPhotoListingAdapter(Context mContext, List<ListingDisplayImage> mPhotoList, CreatePostListener mCallback, DialogClickListener mListener) {
        this.mPhotoList = mPhotoList;
        this.mContext = mContext;
        this.mCallback = mCallback;
        this.mListener = mListener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            SelectedPhotoListingItemBinding mBindingItem = SelectedPhotoListingItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderItem(mBindingItem);
        } else {
            SelectedPhotoListingHeaderBinding mBindingHeader = SelectedPhotoListingHeaderBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderHeader(mBindingHeader);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderHeader) {
            ViewHolderHeader((ViewHolderHeader) holder);
        } else if (holder instanceof ViewHolderItem) {
            viewHolderItem((ViewHolderItem) holder, position);
        }
    }

    private void ViewHolderHeader(final ViewHolderHeader holder) {
        if (mPhotoList.size() >= MAX_IMAGE_LIMIT) {
            holder.itemView.headerPhotoLayout.setVisibility(View.GONE);
        } else {
            holder.itemView.headerPhotoLayout.setVisibility(View.VISIBLE);
        }
        holder.itemView.addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int imageCount = MAX_IMAGE_LIMIT - mPhotoList.size();
                mCallback.openGalleryFor(Constants.CREATE_LISTING, imageCount);
            }
        });
    }

    private void viewHolderItem(ViewHolderItem holder, final int position) {
        final ListingDisplayImage photo = mPhotoList.get(position);
        if (photo.getType() == Constants.LOCAL_PHOTO) {
            File fLocal = new File(photo.getImageUrl());
            try {
                Picasso.get().load(fLocal).into(holder.itemView.listingPhoto);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            Picasso.get().load(photo.getImageUrl()).into(holder.itemView.listingPhoto);
        }
        holder.itemView.crossIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPhotoList.get(position).getListingImageId() != 0) {
                    mCallback.addDeletedImagesId(mPhotoList.get(position).getListingImageId());
                }
                mPhotoList.remove(position);
                mListener.onClick();
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mPhotoList.size() >= MAX_IMAGE_LIMIT) {
            return mPhotoList.size();
        }
        return mPhotoList.size() + 1;
    }

    public int getItemViewType(int position) {
        if (position == mPhotoList.size()) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    public void setList(List<ListingDisplayImage> photoList) {
        this.mPhotoList = photoList;
        notifyDataSetChanged();
    }


    private class ViewHolderItem extends RecyclerView.ViewHolder {
        SelectedPhotoListingItemBinding itemView;

        private ViewHolderItem(SelectedPhotoListingItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderHeader extends RecyclerView.ViewHolder {
        SelectedPhotoListingHeaderBinding itemView;

        private ViewHolderHeader(SelectedPhotoListingHeaderBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}