package com.bynmix.app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bynmix.app.databinding.SelectExistingAddressItemBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.SelectExistingAddressListener;
import com.bynmix.app.models.Address;

import java.util.List;

public class ExistingAddressAdapter extends RecyclerView.Adapter<ExistingAddressAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<Address> mList;
    private final ProfileListener mCallback;
    private SelectExistingAddressListener mListener;


    public ExistingAddressAdapter(Context mContext, List<Address> mList, ProfileListener mCallback, SelectExistingAddressListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mCallback = mCallback;
        this.mListener = mListener;
    }

    @Override
    public ExistingAddressAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        SelectExistingAddressItemBinding mBinding = SelectExistingAddressItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new ExistingAddressAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ExistingAddressAdapter.ViewHolderItem holder, final int position) {
        final Address address = mList.get(position);
        holder.itemView.fullName.setText(address.getFullName());
        if (TextUtils.isEmpty(address.getAddressOne())) {
            holder.itemView.addressOne.setVisibility(View.VISIBLE);
            holder.itemView.addressOne.setText(address.getAddressOne());
        } else {
            holder.itemView.addressOne.setVisibility(View.GONE);
        }
        if (TextUtils.isEmpty(address.getAddressTwo())) {
            holder.itemView.addressTwo.setVisibility(View.VISIBLE);
            holder.itemView.addressTwo.setText(address.getAddressTwo());
        } else {
            holder.itemView.addressTwo.setVisibility(View.GONE);
        }
        holder.itemView.zipcode.setText(address.getCity() + " " + address.getStateAbbreviation() + ", " + address.getZipCode());

        holder.itemView.selectAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             mListener.selectedAddress(address);
             mCallback.onBackPressed();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<Address> mAddressesList) {
        this.mList = mAddressesList;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        SelectExistingAddressItemBinding itemView;

        public ViewHolderItem(SelectExistingAddressItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}


