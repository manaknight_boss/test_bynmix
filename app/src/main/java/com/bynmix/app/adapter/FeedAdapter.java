package com.bynmix.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.databinding.LoadMoreProgressBinding;
import com.bynmix.app.databinding.PostItemBinding;
import com.bynmix.app.interfaces.DotsSingleTabInterface;
import com.bynmix.app.interfaces.FeedItemClickListener;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;


public class FeedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private List<FeedResponse> mFeedList;
    private int feedType;
    private int TYPE_ITEM = 1;
    private int TYPE_PROGRESS = 0;
    private boolean isProgressRequired = false;
    private int extraCount = 0;
    private FeedItemClickListener mCallback;

    public FeedAdapter(Context mContext, List<FeedResponse> mFeedList, int feedType, FeedItemClickListener mCallback) {
        this.mContext = mContext;
        this.mFeedList = mFeedList;
        this.feedType = feedType;
        this.mCallback = mCallback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            PostItemBinding mBindingItem = PostItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new FeedAdapter.ViewHolderItem(mBindingItem);
        } else if (viewType == TYPE_PROGRESS) {
            LoadMoreProgressBinding mBindingProgress = LoadMoreProgressBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new FeedAdapter.ViewHolderProgress(mBindingProgress);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderProgress) {
            position = position + 1;
            viewHolderProgress((ViewHolderProgress) holder);
        } else if (holder instanceof ViewHolderItem) {
            viewHolderItem((ViewHolderItem) holder, position);
        }
    }

    private void viewHolderProgress(ViewHolderProgress holder) {
        if (isProgressRequired) {
            holder.itemView.loadMoreProgressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void viewHolderItem(final ViewHolderItem holder, final int position) {
        final FeedResponse feed = mFeedList.get(position);
        String userPhoto = feed.getUserPhotoUrl();

        if (!TextUtils.isEmpty(userPhoto)) {
            Picasso.get().load(userPhoto).placeholder(R.mipmap.default_profile_image).into(holder.itemView.userPhoto);
        } else {
            holder.itemView.userPhoto.setImageResource(R.mipmap.default_profile_image);
        }

        holder.itemView.username.setText(feed.getUsername());
        holder.itemView.size.setText(feed.getSize());
        holder.itemView.price.setText("$" + (int) feed.getPrice());
        holder.itemView.time.setText(feed.getCreatedDateFormatted());

        updateLikeUI(holder, feed.isUserLiked());
        updateLikes(holder, feed);
        if (feed.isCanUserLikeItem()){
            holder.itemView.heart.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.heart.setVisibility(View.GONE);
        }

        holder.itemView.heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                feed.setUserLiked(!feed.isUserLiked());
                int likes;

                if (feed.isUserLiked()) {
                    likes = feed.getLikesCount() + 1;
                } else {
                    likes = feed.getLikesCount() - 1;
                }
                feed.setLikesCount(likes);
                updateLikes(holder, feed);
                updateLikeUI(holder, feed.isUserLiked());

                if (feed.getType() != null && feed.getType().equalsIgnoreCase(Constants.POSTS)) {
                    mCallback.onPostsLike(feed.getId());
                } else {
                    mCallback.onListingLike(feed.getId());
                }

                Intent intent = new Intent(Constants.ACTION_BROADCAST_RECEIVER_REFRESH_FEEDS);
                intent.putExtra(Constants.FEED, feed);
                mContext.sendBroadcast(intent);
            }


        });
        String feedTitle = feed.getTitle();

        if (feedType == Constants.AlL_FEEDS) {
            if (feed.getType() != null && feed.getType().equalsIgnoreCase(Constants.POSTS)) {
                holder.itemView.priceLayout.setVisibility(View.GONE);
                holder.itemView.bookmark.setVisibility(View.VISIBLE);
                holder.itemView.priceTag.setVisibility(View.GONE);
                holder.itemView.shareIcon.setVisibility(View.VISIBLE);
                holder.itemView.postTitle.setMaxLines(2);
            } else {
                holder.itemView.priceLayout.setVisibility(View.VISIBLE);
                holder.itemView.bookmark.setVisibility(View.GONE);
                holder.itemView.priceTag.setVisibility(View.VISIBLE);
                holder.itemView.shareIcon.setVisibility(View.VISIBLE);
                holder.itemView.postTitle.setMaxLines(1);
            }
        } else if (feedType == Constants.AlL_POSTS) {
            holder.itemView.priceLayout.setVisibility(View.GONE);
            holder.itemView.bookmark.setVisibility(View.VISIBLE);
            holder.itemView.shareIcon.setVisibility(View.VISIBLE);
            holder.itemView.priceTag.setVisibility(View.GONE);
            holder.itemView.postTitle.setMaxLines(2);
        }
        holder.itemView.postTitle.setText(feedTitle);

        holder.itemView.username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openProfile(feed.getUserId());
            }
        });


        holder.itemView.userPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openProfile(feed.getUserId());
            }
        });
        holder.itemView.shareIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "share clicked", Toast.LENGTH_SHORT).show();
            }
        });

        holder.itemView.likeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(feed.getType().equalsIgnoreCase(Constants.TYPE_LISTING)) {
                    mCallback.openLikeFragment(feed.getId(), Constants.FEED_LISTING);
                } else {
                    mCallback.openLikeFragment(feed.getId(), Constants.FEED_POST);
                }
            }
        });

        holder.itemView.bookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "bookmark clicked", Toast.LENGTH_SHORT).show();
            }
        });


        String photoUrl = feed.getPhotoUrl();
        if (!TextUtils.isEmpty(photoUrl)) {
            if (feed.getPostType() != null && feed.getPostType().equalsIgnoreCase(Constants.VIDEO_POST)) {
                holder.itemView.videoPlayIcon.setVisibility(View.VISIBLE);
                Picasso.get().load(photoUrl).placeholder(R.mipmap.novideo).into(holder.itemView.blogFullImage);
                Picasso.get().load(photoUrl).placeholder(R.mipmap.novideo).into(holder.itemView.dotsFullImage);
            } else {
                holder.itemView.videoPlayIcon.setVisibility(View.GONE);
                Picasso.get().load(photoUrl).placeholder(R.mipmap.default_feed).into(holder.itemView.blogFullImage);
                Picasso.get().load(photoUrl).placeholder(R.mipmap.default_feed).into(holder.itemView.dotsFullImage);
            }

            if (feed.getPostDots().size() == 0) {
                holder.itemView.dotsFullImageParent.setVisibility(View.INVISIBLE);
                holder.itemView.blogFullImage.setVisibility(View.VISIBLE);
                if (feed.isHasPostDots()) {
                    holder.itemView.viewTagText.setText(mContext.getString(R.string.tap_to_view_tagged));
                    holder.itemView.viewTags.setVisibility(View.VISIBLE);
                } else {
                    holder.itemView.viewTags.setVisibility(View.GONE);
                }
            } else {
                holder.itemView.viewTags.setVisibility(View.VISIBLE);
                if (feed.isVisibleDots()) {
                    holder.itemView.viewTagText.setText(mContext.getString(R.string.hide_tagged_products));
                    holder.itemView.dotsFullImageParent.setVisibility(View.VISIBLE);
                    holder.itemView.dotsFullImage.setViewDraw(true);
                    holder.itemView.dotsFullImage.setPointList(feed.getPostDots());
                    holder.itemView.blogFullImage.setVisibility(View.INVISIBLE);
                } else {
                    holder.itemView.viewTagText.setText(mContext.getString(R.string.tap_to_view_tagged));
                    holder.itemView.dotsFullImage.setPointList(new ArrayList<AnnotatedPoint>());
                    holder.itemView.dotsFullImageParent.setVisibility(View.INVISIBLE);
                    holder.itemView.blogFullImage.setVisibility(View.VISIBLE);
                }
                holder.itemView.dotsFullImage.setDotsListener(new DotsSingleTabInterface() {
                    @Override
                    public void singleTab(AnnotatedPoint annotatedPoint) {
                        if (annotatedPoint.getPostDotTypeId() == Constants.DOT_TYPE_LISTING) {
                            mCallback.openListingDetailFragment(annotatedPoint.getListingId());
                        } else {
                            mCallback.openInAppBrowser(annotatedPoint.getLink(), annotatedPoint.getTitle());
                        }
                    }

                    @Override
                    public void singleTab() {
                        postClicked(feed);
                    }
                });
            }
        } else {
            if (feed.getPostType() != null && feed.getPostType().equalsIgnoreCase(Constants.VIDEO_POST)) {
                holder.itemView.blogFullImage.setImageResource(R.mipmap.novideo);
            } else {
                holder.itemView.blogFullImage.setImageResource(R.mipmap.default_feed);
            }
            holder.itemView.dotsFullImageParent.setVisibility(View.INVISIBLE);
        }

        if (feed.getType().equalsIgnoreCase(Constants.TYPE_LISTING)) {
            holder.itemView.blogTitleLayout.setBackgroundColor(mContext.getResources().getColor(R.color.purple_heading));
            holder.itemView.blogFullImageLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.openListingDetailFragment(feed.getId());
                }
            });
        } else {
            holder.itemView.blogTitleLayout.setBackgroundColor(mContext.getResources().getColor(R.color.post_purple_heading));
            holder.itemView.blogFullImageLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    postClicked(feed);
                }
            });
        }
        showIndividualProgress(holder, false);
        holder.itemView.viewTags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                feed.setVisibleDots(!feed.isVisibleDots());
                if (feed.getPostDots().size() == 0) {
                    showIndividualProgress(holder, true);
                    mCallback.getPostTags(position, feed.getId(), feedType);
                } else {
                    hideProgress();
                }
            }
        });
    }

    private void postClicked(FeedResponse feed) {
        if (feed.getLink() != null) {
            mCallback.openPostDetailFragment(feed);
        }
    }

    private void showIndividualProgress(ViewHolderItem holder, boolean isIndividualProgressRequired) {
        if (isIndividualProgressRequired) {
            holder.itemView.progressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.progressBar.setVisibility(View.GONE);
        }
    }

    private void updateLikes(ViewHolderItem holder, FeedResponse feed) {
        int likeCount = feed.getLikesCount();
        String likes;
        if (feed.getType().equalsIgnoreCase(Constants.TYPE_LISTING)) {
            likes = Utils.getColorText("" + likeCount, mContext.getResources().getColor(R.color.lipstick));
        } else {
            likes = "" + likeCount;
        }
        if (likeCount == 0) {
            holder.itemView.likeLayout.setVisibility(View.GONE);
        } else if (likeCount == 1) {
            holder.itemView.likesCount.setText(Html.fromHtml(likes + " Like"), TextView.BufferType.SPANNABLE);
            holder.itemView.likeLayout.setVisibility(View.VISIBLE);
        } else if (likeCount > 1) {
            holder.itemView.likesCount.setText(Html.fromHtml(likes + " Likes"), TextView.BufferType.SPANNABLE);
            holder.itemView.likeLayout.setVisibility(View.VISIBLE);
        }
    }

    private void updateLikeUI(ViewHolderItem holder, boolean userLiked) {
        if (userLiked) {
            holder.itemView.heart.setImageResource(R.mipmap.heart);
        } else {
            holder.itemView.heart.setImageResource(R.mipmap.heart_un_selected);
        }
    }

    @Override
    public int getItemCount() {
        return mFeedList.size() + extraCount;
    }


    public int getItemViewType(int position) {
        if (position == mFeedList.size()) {
            return TYPE_PROGRESS;
        }
        return TYPE_ITEM;
    }

    public void setProgress(boolean progressStatus) {
        isProgressRequired = progressStatus;
        if (isProgressRequired) {
            extraCount = 1;
        } else {
            extraCount = 0;
        }
        notifyDataSetChanged();
    }

    public void setList(List<FeedResponse> mFeedList) {
        this.mFeedList = mFeedList;
        notifyDataSetChanged();
    }

    public void hideProgress() {
        notifyDataSetChanged();
    }

    public class ViewHolderItem extends RecyclerView.ViewHolder {
        PostItemBinding itemView;

        public ViewHolderItem(PostItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderProgress extends RecyclerView.ViewHolder {
        LoadMoreProgressBinding itemView;

        public ViewHolderProgress(LoadMoreProgressBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

}



