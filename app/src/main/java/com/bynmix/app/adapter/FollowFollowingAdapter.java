package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FollowFollowingItemBinding;
import com.bynmix.app.databinding.LoadMoreProgressBinding;
import com.bynmix.app.interfaces.FeedItemClickListener;
import com.bynmix.app.models.UserResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FollowFollowingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private List<UserResponse> mUserList;
    private int TYPE_ITEM = 1;
    private int TYPE_PROGRESS = 0;
    private boolean isProgressRequired = false;
    private int extraCount = 0;
    private FeedItemClickListener mCallback;

    public FollowFollowingAdapter(Context mContext, List<UserResponse> mUserList, FeedItemClickListener mCallback) {
        this.mContext = mContext;
        this.mUserList = mUserList;
        this.mCallback = mCallback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            FollowFollowingItemBinding mBindingItem = FollowFollowingItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new FollowFollowingAdapter.ViewHolderItem(mBindingItem);
        } else if (viewType == TYPE_PROGRESS) {
            LoadMoreProgressBinding mBindingProgress = LoadMoreProgressBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new FollowFollowingAdapter.ViewHolderProgress(mBindingProgress);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FollowFollowingAdapter.ViewHolderProgress) {
            position = position + 1;
            viewHolderProgress((FollowFollowingAdapter.ViewHolderProgress) holder);
        } else if (holder instanceof FollowFollowingAdapter.ViewHolderItem) {
            viewHolderItem((FollowFollowingAdapter.ViewHolderItem) holder, position);
        }
    }

    private void viewHolderProgress(FollowFollowingAdapter.ViewHolderProgress holder) {
        if (isProgressRequired) {
            holder.itemView.loadMoreProgressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void viewHolderItem(final FollowFollowingAdapter.ViewHolderItem holder, int position) {
        final UserResponse user = mUserList.get(position);
        String userPhoto = user.getPhotoUrl();
        holder.itemView.userName.setText(user.getUsername());

        if (!TextUtils.isEmpty(user.getTitle())) {
            holder.itemView.title.setText(user.getTitle());
        } else {
            holder.itemView.title.setText("N/A");
        }

        if (!TextUtils.isEmpty(user.getUserDescriptorsFormatted())) {
            holder.itemView.styles.setText(user.getUserDescriptorsFormatted());
        } else {
            holder.itemView.styles.setText("N/A");
        }


        if (!TextUtils.isEmpty(user.getUserBrandsFormatted())) {
            holder.itemView.brands.setText(user.getUserBrandsFormatted());
        } else {
            holder.itemView.brands.setText("N/A");
        }


        if (!TextUtils.isEmpty(userPhoto)) {
            Picasso.get().load(userPhoto).placeholder(R.mipmap.default_feed).into(holder.itemView.userPhoto);
        } else {
            holder.itemView.userPhoto.setImageResource(R.mipmap.default_feed);
        }

        if (user.isUserSameAsLoggedInUser()) {
            holder.itemView.followFollowingIcon.setVisibility(View.GONE);
        } else {
            holder.itemView.followFollowingIcon.setVisibility(View.VISIBLE);
        }

        updateFollowFollowingUI(holder, user.isLoggedInUserFollowing());
        holder.itemView.followFollowingIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user.setLoggedInUserFollowing(!user.isLoggedInUserFollowing());

                updateFollowFollowingUI(holder, user.isLoggedInUserFollowing());
                mCallback.onFollowerUser(user.getUserId());
            }
        });

        holder.itemView.userName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openProfile(user.getUserId());
            }
        });

        holder.itemView.userPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openProfile(user.getUserId());
            }
        });
    }

    private void updateFollowFollowingUI(ViewHolderItem holder, boolean loggedInUserFollowing) {
        if (loggedInUserFollowing) {
            holder.itemView.followFollowingIcon.setImageResource(R.mipmap.following_user);
        } else {
            holder.itemView.followFollowingIcon.setImageResource(R.mipmap.follow_user);
        }
    }


    @Override
    public int getItemCount() {
        return mUserList.size() + extraCount;
    }


    public int getItemViewType(int position) {
        if (position == mUserList.size()) {
            return TYPE_PROGRESS;
        }

        return TYPE_ITEM;
    }

    public void setProgress(boolean progressStatus) {
        isProgressRequired = progressStatus;

        if (isProgressRequired) {
            extraCount = 1;
        } else {
            extraCount = 0;
        }
        notifyDataSetChanged();
    }

    public void setList(List<UserResponse> mUserList) {
        this.mUserList = mUserList;
        notifyDataSetChanged();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder {
        FollowFollowingItemBinding itemView;

        public ViewHolderItem(FollowFollowingItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderProgress extends RecyclerView.ViewHolder {
        LoadMoreProgressBinding itemView;

        public ViewHolderProgress(LoadMoreProgressBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

}