package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.bynmix.app.R;
import com.bynmix.app.models.ListingDisplayImage;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ImageAdapter extends PagerAdapter {

    private Context mContext;
    private List<ListingDisplayImage> listingPhotos;


    public ImageAdapter(Context mContext, List<ListingDisplayImage> listingPhotos) {
        this.mContext = mContext;
        this.listingPhotos = listingPhotos;
    }

    @Override
    public int getCount() {
        return listingPhotos.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        View itemView = LayoutInflater.from(mContext).inflate(R.layout.listing_images_item, container, false);
        ImageView imageView = (ImageView) itemView.findViewById(R.id.imgPagerItem);
        final ProgressBar progressbar = (ProgressBar) itemView.findViewById(R.id.cover_progress_bar);
        if (listingPhotos != null) {
            ListingDisplayImage listingDisplayImage = listingPhotos.get(position);
            Picasso.get().load(listingDisplayImage.getImageUrl()).noPlaceholder().into(imageView, new Callback() {
                @Override
                public void onSuccess() {
                    progressbar.setVisibility(View.GONE);
                }

                @Override
                public void onError(Exception e) {
                    progressbar.setVisibility(View.GONE);
                }
            });
        }
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        container.removeView((RelativeLayout) object);


    }


}



