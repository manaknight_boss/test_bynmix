package com.bynmix.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.databinding.ListingItemBinding;
import com.bynmix.app.databinding.LoadMoreProgressBinding;
import com.bynmix.app.interfaces.FeedItemClickListener;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListingAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private List<FeedResponse> mFeedList;
    private int TYPE_ITEM = 1;
    private int TYPE_PROGRESS = 0;
    private boolean isProgressRequired = false;
    private int extraCount = 0;
    private FeedItemClickListener mCallback;

    public ListingAdapter(Context mContext, List<FeedResponse> mFeedList, FeedItemClickListener mCallback) {
        this.mContext = mContext;
        this.mFeedList = mFeedList;
        this.mCallback = mCallback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            ListingItemBinding mBindingItem = ListingItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ListingAdapter.ViewHolderItem(mBindingItem);

        } else if (viewType == TYPE_PROGRESS) {
            LoadMoreProgressBinding mBindingProgress = LoadMoreProgressBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ListingAdapter.ViewHolderProgress(mBindingProgress);

        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ListingAdapter.ViewHolderProgress) {
            position = position + 1;
            viewHolderProgress((ListingAdapter.ViewHolderProgress) holder);
        } else if (holder instanceof ListingAdapter.ViewHolderItem) {
            viewHolderItem((ListingAdapter.ViewHolderItem) holder, position);
        }
    }

    private void viewHolderProgress(ListingAdapter.ViewHolderProgress holder) {
        if (isProgressRequired) {
            holder.itemView.loadMoreProgressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void viewHolderItem(final ListingAdapter.ViewHolderItem holder, int position) {
        final FeedResponse feed = mFeedList.get(position);
        String userPhoto = feed.getUserPhotoUrl();
        holder.itemView.blogTitleLayout.setBackgroundColor(mContext.getResources().getColor(R.color.purple_heading));

        if (!TextUtils.isEmpty(userPhoto)) {
            Picasso.get().load(userPhoto).placeholder(R.mipmap.default_profile_image).into(holder.itemView.userPhoto);
        } else {
            holder.itemView.userPhoto.setImageResource(R.mipmap.default_profile_image);
        }

        holder.itemView.username.setText(feed.getUsername());
        holder.itemView.size.setText(feed.getSize());
        holder.itemView.price.setText("$" + (int) feed.getPrice());

        holder.itemView.time.setText(feed.getCreatedDateFormatted());
        if (feed.isCanUserLikeItem()){
            holder.itemView.heart.setVisibility(View.VISIBLE);
        } else {
        holder.itemView.heart.setVisibility(View.GONE);     
        }

        updateLikeUI(holder, feed.isUserLiked());
        updateLikes(holder, feed);
        holder.itemView.heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                feed.setUserLiked(!feed.isUserLiked());
                int likes;
                if (feed.isUserLiked()) {
                    likes = feed.getLikesCount() + 1;
                } else {
                    likes = feed.getLikesCount() - 1;
                }
                feed.setLikesCount(likes);
                updateLikes(holder, feed);
                updateLikeUI(holder, feed.isUserLiked());
                mCallback.onListingLike(feed.getId());

                Intent intent = new Intent(Constants.ACTION_BROADCAST_RECEIVER_REFRESH_FEEDS);
                intent.putExtra(Constants.FEED, feed);
                mContext.sendBroadcast(intent);
            }
        });

        String feedTitle = feed.getTitle();
        holder.itemView.postTitle.setText(feedTitle);
        holder.itemView.postTitle.setMaxLines(1);
        holder.itemView.username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openProfile(feed.getUserId());
            }
        });
        holder.itemView.userPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openProfile(feed.getUserId());
            }
        });
        holder.itemView.likeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openLikeFragment(feed.getId(), Constants.FEED_LISTING);
            }
        });

        if (feed.getPostType() != null && feed.getPostType().equalsIgnoreCase(Constants.VIDEO_POST)) {
            String photoUrl = feed.getPhotoUrl();
            if (!TextUtils.isEmpty(photoUrl)) {
                Picasso.get().load(photoUrl).placeholder(R.mipmap.novideo).into(holder.itemView.blogFullImage);
            } else {
                holder.itemView.blogFullImage.setImageResource(R.mipmap.novideo);
            }

            holder.itemView.videoPlayIcon.setVisibility(View.VISIBLE);

        } else {
            String photoUrl = feed.getPhotoUrl();
            if (!TextUtils.isEmpty(photoUrl)) {
                Picasso.get().load(photoUrl).placeholder(R.mipmap.default_feed).into(holder.itemView.blogFullImage);
            } else {
                holder.itemView.blogFullImage.setImageResource(R.mipmap.default_feed);
            }

            holder.itemView.videoPlayIcon.setVisibility(View.GONE);
        }
        holder.itemView.blogFullImageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openListingDetailFragment(feed.getId());
            }
        });
    }

    private void updateLikes(ViewHolderItem holder, FeedResponse feed) {
        int likeCount = feed.getLikesCount();
        if (likeCount == 0) {
            holder.itemView.likeLayout.setVisibility(View.GONE);
        } else if (likeCount == 1) {
            holder.itemView.likesCount.setText(feed.getLikesCount() + " Like");
            holder.itemView.likeLayout.setVisibility(View.VISIBLE);
        } else if (likeCount > 1) {
            holder.itemView.likesCount.setText(feed.getLikesCount() + " Likes");
            holder.itemView.likeLayout.setVisibility(View.VISIBLE);
        }
    }

    private void updateLikeUI(ViewHolderItem holder, boolean userLiked) {
        if (userLiked) {
            holder.itemView.heart.setImageResource(R.mipmap.heart);
        } else {
            holder.itemView.heart.setImageResource(R.mipmap.heart_un_selected);
        }
    }

    @Override
    public int getItemCount() {
        return mFeedList.size() + extraCount;
    }


    public int getItemViewType(int position) {
        if (position == mFeedList.size()) {
            return TYPE_PROGRESS;
        }
        return TYPE_ITEM;
    }

    public void setProgress(boolean progressStatus) {
        isProgressRequired = progressStatus;
        if (isProgressRequired) {
            extraCount = 1;
        } else {
            extraCount = 0;
        }
        notifyDataSetChanged();
    }

    public void setList(List<FeedResponse> mFeedList) {
        this.mFeedList = mFeedList;
        notifyDataSetChanged();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder {
        ListingItemBinding itemView;

        public ViewHolderItem(ListingItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    public class ViewHolderProgress extends RecyclerView.ViewHolder {
        LoadMoreProgressBinding itemView;

        public ViewHolderProgress(LoadMoreProgressBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

}



