package com.bynmix.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.databinding.CommentItemBinding;
import com.bynmix.app.databinding.ListingDetailHeaderBinding;
import com.bynmix.app.interfaces.CommentListener;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.Colors;
import com.bynmix.app.models.Comments;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.models.ListingDisplayImage;
import com.bynmix.app.models.Stock;
import com.bynmix.app.sharedpreference.SharedPreferenceUtility;
import com.bynmix.app.utils.CommentDialog;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.DialogAlert;
import com.squareup.picasso.Picasso;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;


import java.util.ArrayList;
import java.util.List;

public class ListingDetailAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    final private int TYPE_ITEM = 1;
    final private int TYPE_HEADER = 0;
    private CreatePostListener mCallback;
    private ListingDetail listingDetail;
    private ImageAdapter adapter;
    private ArrayList<ImageView> lines = new ArrayList<>();
    private SharedPreferenceUtility mSharedPreferenceUtility;


    public ListingDetailAdapter(Context mContext, ListingDetail listingDetail, CreatePostListener mCallback) {
        this.listingDetail = listingDetail;
        this.mContext = mContext;
        this.mCallback = mCallback;
        mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            CommentItemBinding mBindingItem = CommentItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderItem(mBindingItem);
        } else {
            ListingDetailHeaderBinding mBindingHeader = ListingDetailHeaderBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderHeader(mBindingHeader);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderHeader) {
            viewHolderHeader((ViewHolderHeader) holder);
        } else if (holder instanceof ViewHolderItem) {
            viewHolderItem((ViewHolderItem) holder, position - 1);
        }
    }


    private void viewHolderHeader(final ViewHolderHeader holder) {
        holder.itemView.viewPager.setLayoutParams(new LinearLayout.LayoutParams(getScreenWidth(), getScreenWidth()));
        setColors(listingDetail.getColors(), holder);
        String userPhoto = listingDetail.getUserPhotoUrl();
        if (!TextUtils.isEmpty(userPhoto)) {
            Picasso.get().load(userPhoto).placeholder(R.mipmap.default_profile_image).into(holder.itemView.userPhoto);
        } else {
            holder.itemView.userPhoto.setImageResource(R.mipmap.default_profile_image);
        }

        if (listingDetail.getUserId() == mSharedPreferenceUtility.getMyUserId()) {
            holder.itemView.error.setVisibility(View.GONE);
        } else {
            if (listingDetail.isCanUserReportAnIssue()) {
                holder.itemView.error.setVisibility(View.VISIBLE);
            } else {
                holder.itemView.error.setVisibility(View.GONE);
            }
        }

        holder.itemView.error.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openReportAnIssueListingFragment(listingDetail);
            }
        });

        holder.itemView.username.setText(listingDetail.getUsername());
        holder.itemView.size.setText(listingDetail.getSize().getSizeName());
        holder.itemView.brandName.setText(listingDetail.getBrand().getBrandName());
        holder.itemView.price.setText(String.format("$%.2f", listingDetail.getPrice()));
        holder.itemView.condition.setText(listingDetail.getCondition().getConditionName());
        holder.itemView.time.setText(listingDetail.getCreatedDateFormatted());
        holder.itemView.title.setText(listingDetail.getTitle());
        holder.itemView.description.setText(listingDetail.getDescription());
        holder.itemView.category.setText(listingDetail.getCategory().getCategoryName());
        holder.itemView.categoryType.setText(listingDetail.getCategoryType().getCategoryTypeName());
        holder.itemView.shipping.setText(String.format("$%.2f", listingDetail.getShippingInfo().getRate()) + " " + listingDetail.getShippingInfo().getShippingType().getType() + " " + listingDetail.getShippingInfo().getShippingType().getDescription() + " shipping");

        if (listingDetail.getLikesCount() > 0) {
            holder.itemView.likesLayout.setVisibility(View.VISIBLE);
            holder.itemView.likesText.setText(listingDetail.getLikesCount() + " " + mContext.getString(R.string.fashionistas));
        } else {
            holder.itemView.likesLayout.setVisibility(View.GONE);
        }

        holder.itemView.likesLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openLikeFragment(listingDetail.getId(), Constants.FEED_LISTING);
            }
        });

        holder.itemView.username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openProfile(listingDetail.getUserId());
            }
        });
        setImages(listingDetail.getListingDisplayImages(), holder);

        updateLikeUI(holder, listingDetail.isUserLiked());
        updateLikes(holder, listingDetail);
        if (!listingDetail.isCanUserLikeListing()){
            holder.itemView.heart.setVisibility(View.GONE);
            holder.itemView.likeIcon.setVisibility(View.GONE);
        } else{
            holder.itemView.heart.setVisibility(View.VISIBLE);
        }

        holder.itemView.heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onlikedClick(listingDetail, holder);
            }
        });
        holder.itemView.likeIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onlikedClick(listingDetail, holder);
            }
        });
        holder.itemView.addCommentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogAlert.showCommentDialog(mContext, Constants.TYPE_COMMENTS, new CommentListener() {
                    @Override
                    public void addComment(int id, String comment) {
                        mCallback.addComment(listingDetail.getId(), comment);
                    }
                });
            }
        });

    }

    private void updateLikes(ViewHolderHeader holder, ListingDetail listingDetail) {
        int likeCount = listingDetail.getLikesCount();
        if (likeCount > 0) {
            holder.itemView.likesLayout.setVisibility(View.VISIBLE);
            holder.itemView.likesText.setText(likeCount + " " + mContext.getString(R.string.fashionistas));
        } else {
            holder.itemView.likesLayout.setVisibility(View.GONE);
        }
    }

    private void updateLikeUI(ViewHolderHeader holder, boolean userLiked) {
        if (userLiked) {
            holder.itemView.heart.setImageResource(R.mipmap.heart);
            holder.itemView.likeIcon.setImageResource(R.mipmap.heart_white_outline);
        } else {
            holder.itemView.heart.setImageResource(R.mipmap.heart_un_selected);
            holder.itemView.likeIcon.setImageResource(R.mipmap.heart_with_white_bg);
        }
    }

    private void onlikedClick(ListingDetail listingDetail, ViewHolderHeader holder) {
        listingDetail.setUserLiked(!listingDetail.isUserLiked());
        int likes;
        if (listingDetail.isUserLiked()) {
            likes = listingDetail.getLikesCount() + 1;
        } else {
            likes = listingDetail.getLikesCount() - 1;
        }
        listingDetail.setLikesCount(likes);
        updateLikes(holder, listingDetail);
        updateLikeUI(holder, listingDetail.isUserLiked());
        mCallback.onListingLike(listingDetail.getId());

        FeedResponse feed = new FeedResponse();
        feed.setUserLiked(listingDetail.isUserLiked());
        feed.setId(listingDetail.getId());
        feed.setLikesCount(listingDetail.getLikesCount());
        feed.setType(Constants.TYPE_LISTING);
        Intent intent = new Intent(Constants.ACTION_BROADCAST_RECEIVER_REFRESH_FEEDS);
        intent.putExtra(Constants.FEED, feed);
        mContext.sendBroadcast(intent);

    }

    private int getScreenWidth() {
        WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.x;
    }

    private void setColors(final List<Colors> colors, ViewHolderHeader holder) {
        AddListingColorAdapter addListingColorAdapter = new AddListingColorAdapter(mContext, colors, mCallback, 0);
        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        holder.itemView.colorRecyclerView.setLayoutManager(flowLayoutManager);
        holder.itemView.colorRecyclerView.setAdapter(addListingColorAdapter);
        holder.itemView.colorRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);

                outRect.set(0, 0, 15, 0);

            }
        });
    }


    private void setImages(List<ListingDisplayImage> listingDetailImages, ViewHolderHeader holder) {
        adapter = new ImageAdapter(mContext, listingDetailImages);
        holder.itemView.viewPager.setAdapter(adapter);
        addLines(listingDetailImages.size(), holder);
        setLeftRightButtons(listingDetailImages.size(), holder, 0);
    }

    private void setLeftRightButtons(int size, final ViewHolderHeader holder, int position) {
        holder.itemView.nextButton.setVisibility(View.GONE);
        holder.itemView.previousButton.setVisibility(View.GONE);
        if (size > 1) {
            if (position == 0) {
                holder.itemView.nextButton.setVisibility(View.VISIBLE);
            } else if (position == size - 1) {
                holder.itemView.previousButton.setVisibility(View.VISIBLE);
            } else {
                holder.itemView.nextButton.setVisibility(View.VISIBLE);
                holder.itemView.previousButton.setVisibility(View.VISIBLE);
            }
        }
        holder.itemView.nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.itemView.viewPager.setCurrentItem(holder.itemView.viewPager.getCurrentItem() + 1);
            }
        });
        holder.itemView.previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                holder.itemView.viewPager.setCurrentItem(holder.itemView.viewPager.getCurrentItem() - 1);
            }
        });
    }

    private void addLines(final int NUM_PAGES, final ViewHolderHeader holder) {
        holder.itemView.views.removeAllViews();
        lines = new ArrayList<>();
        for (int i = 0; i < NUM_PAGES; i++) {
            ImageView line = new ImageView(mContext);
            if (i == 0) {
                line.setImageDrawable(mContext.getResources().getDrawable(R.drawable.selected_line));
            } else {
                line.setImageDrawable(mContext.getResources().getDrawable(R.drawable.un_selected_line));
            }
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    1.0f
            );


            if (i == 0 && NUM_PAGES - 1 == 0) {
                params.setMargins(0, 0, 0, 0);
            } else if (i == NUM_PAGES - 1) {
                params.setMargins(3, 0, 0, 0);
            } else if (i == 0) {
                params.setMargins(0, 0, 3, 0);
            } else {
                params.setMargins(3, 0, 3, 0);
            }


            holder.itemView.views.addView(line, params);

            lines.add(line);
        }

        holder.itemView.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                selectDot(position, NUM_PAGES);
                setLeftRightButtons(NUM_PAGES, holder, position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


    }

    private void selectDot(int idx, int NUM_PAGES) {
        Resources res = mContext.getResources();
        for (int i = 0; i < NUM_PAGES; i++) {
            int drawableId = (i == idx) ? (R.drawable.selected_line) : (R.drawable.un_selected_line);
            Drawable drawable = res.getDrawable(drawableId);
            lines.get(i).setImageDrawable(drawable);
        }
    }


    private void viewHolderItem(ViewHolderItem holder, final int position) {
        final Comments comment = listingDetail.getComments().get(position);
        setCommentText(comment, holder);
        holder.itemView.userName.setText(comment.getUsername());
        holder.itemView.comment.setText(Html.fromHtml(comment.getComment()), TextView.BufferType.SPANNABLE);
        String photoUrl = comment.getUserImage();
        holder.itemView.date.setText(comment.getCreatedDateFormatted());
        if (!TextUtils.isEmpty(photoUrl)) {
            Picasso.get().load(photoUrl).placeholder(R.mipmap.default_profile_image).into(holder.itemView.image);
        } else {
            holder.itemView.image.setImageResource(R.mipmap.default_profile_image);
        }
        if (comment.isBuyerComment()) {
            holder.itemView.backgroundLayout.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        } else {
            holder.itemView.backgroundLayout.setBackgroundColor(mContext.getResources().getColor(R.color.light_purple3));
        }

        holder.itemView.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openProfile(comment.getUserId());
            }
        });

        holder.itemView.reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommentDialog commentDialog = new CommentDialog();
                commentDialog.showDialog(mContext, mCallback, listingDetail.getId(), listingDetail.getUsername(), Constants.TYPE_REPLY);
            }
        });
    }

    private void setCommentText(Comments comment, ListingDetailAdapter.ViewHolderItem holder) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.itemView.comment.setText(Html.fromHtml("<b>" + "<font color='" + mContext.getResources().getColor(R.color.purple) + "'>" + comment.getUsername() + ":" + "</font>" + "</b> " + comment.getComment(), Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
        } else {
            holder.itemView.comment.setText(Html.fromHtml("<b>" + "<font color='" + mContext.getResources().getColor(R.color.purple) + "'>" + comment.getUsername() + ":" + "</font>" + "</b> " + comment.getComment()), TextView.BufferType.SPANNABLE);
        }
    }

    @Override
    public int getItemCount() {
        return listingDetail.getComments().size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }

        return TYPE_ITEM;
    }


    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    public void setListingDetail(ListingDetail listingDetail) {
        this.listingDetail = listingDetail;
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        CommentItemBinding itemView;

        public ViewHolderItem(CommentItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderHeader extends RecyclerView.ViewHolder {
        ListingDetailHeaderBinding itemView;

        private ViewHolderHeader(ListingDetailHeaderBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}