package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.LoadMoreProgressBinding;
import com.bynmix.app.databinding.NotificationOfferItemBinding;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.SubOfferHistory;
import com.bynmix.app.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListingOffersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private List<SubOfferHistory> mList;
    private final CreatePostListener mCallback;
    private int TYPE_ITEM = 1;
    private int TYPE_PROGRESS = 0;
    private boolean isProgressRequired = false;
    private int extraCount = 0;

    public ListingOffersAdapter(Context mContext, List<SubOfferHistory> mList, CreatePostListener mCallback) {
        this.mContext = mContext;
        this.mList = mList;
        this.mCallback = mCallback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            NotificationOfferItemBinding mBindingItem = NotificationOfferItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ListingOffersAdapter.ViewHolderItem(mBindingItem);
        } else if (viewType == TYPE_PROGRESS) {
            LoadMoreProgressBinding mBindingProgress = LoadMoreProgressBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ListingOffersAdapter.ViewHolderProgress(mBindingProgress);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ListingOffersAdapter.ViewHolderProgress) {
            position = position + 1;
            viewHolderProgress((ListingOffersAdapter.ViewHolderProgress) holder);
        } else if (holder instanceof ListingOffersAdapter.ViewHolderItem) {
            viewHolderItem((ListingOffersAdapter.ViewHolderItem) holder, position);
        }
    }

    private void viewHolderProgress(ListingOffersAdapter.ViewHolderProgress holder) {
        if (isProgressRequired) {
            holder.itemView.loadMoreProgressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void viewHolderItem(final ListingOffersAdapter.ViewHolderItem holder, int position) {
        final SubOfferHistory offerHistory = mList.get(position);
        holder.itemView.time.setText(offerHistory.getOfferDateFormatted());
        String username;
        if (offerHistory.isBuyerOffer()) {
            username = offerHistory.getBuyerUsername();
        } else {
            username = offerHistory.getSellerUsername();
        }
        if (offerHistory.isActiveOffer()) {
            if (offerHistory.isBuyerOffer()) {
                holder.itemView.viewLayout.setVisibility(View.GONE);
                holder.itemView.offersLayout.setVisibility(View.VISIBLE);
            } else {
                holder.itemView.viewLayout.setVisibility(View.VISIBLE);
                holder.itemView.offersLayout.setVisibility(View.GONE);
            }

        } else {
            holder.itemView.viewLayout.setVisibility(View.VISIBLE);
            holder.itemView.offersLayout.setVisibility(View.GONE);
        }

        String notificationType = getBoldText(mContext.getString(R.string.counteroffered) + " $" + Utils.formatValueToTwoDecimalPlaces(offerHistory.getOfferPrice())) + " for \"" + getUnderLineText(offerHistory.getListingTitle().trim()) + "\"!";
        String image = offerHistory.getListingImage();
        if (!TextUtils.isEmpty(image)) {
            Picasso.get().load(image).placeholder(R.mipmap.default_feed).into(holder.itemView.image);
        } else {
            holder.itemView.image.setImageResource(R.mipmap.default_feed);
        }

        holder.itemView.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openListingDetailFragment(offerHistory.getListingId());
            }
        });

        String notificationMainText = getColorText("@" + username + " ") + notificationType;
        holder.itemView.mainText.setText(Html.fromHtml(notificationMainText), TextView.BufferType.SPANNABLE);


        holder.itemView.counterOfferLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openMakeAnOfferFragment(offerHistory.getListingId(), offerHistory.getOfferId(), offerHistory.getBuyerId());
            }
        });

        holder.itemView.statusLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.acceptOffer(offerHistory.getListingId(), offerHistory.getOfferId(), true);
            }
        });


        holder.itemView.deleteLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.declineOffer(offerHistory.getListingId(), offerHistory.getOfferId(), 0);
            }
        });


        holder.itemView.viewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openMakeAnOfferFragment(offerHistory.getListingId(), offerHistory.getOfferId(), offerHistory.getBuyerId());
            }
        });


    }

    private String getUnderLineText(String string) {
        return "<u>" + string + "</u>";
    }


    private String getColorText(String string) {
        return "<b>" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + string + "</font>" + "</b> ";
    }

    private String getBoldText(String string) {
        return "<b>" + string + "</b> ";
    }

    @Override
    public int getItemCount() {
        return mList.size() + extraCount;
    }


    public int getItemViewType(int position) {
        if (position == mList.size()) {
            return TYPE_PROGRESS;
        }
        return TYPE_ITEM;
    }

    public void setProgress(boolean progressStatus) {
        isProgressRequired = progressStatus;
        if (isProgressRequired) {
            extraCount = 1;
        } else {
            extraCount = 0;
        }
        notifyDataSetChanged();
    }

    public void setList(List<SubOfferHistory> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder {
        NotificationOfferItemBinding itemView;

        public ViewHolderItem(NotificationOfferItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderProgress extends RecyclerView.ViewHolder {
        LoadMoreProgressBinding itemView;

        public ViewHolderProgress(LoadMoreProgressBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}