package com.bynmix.app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.databinding.AddressItemBinding;
import com.bynmix.app.databinding.MyAddressesHeaderBinding;
import com.bynmix.app.interfaces.AddressListener;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.Address;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.DialogAlert;
import com.bynmix.app.utils.SelectExistingAddressDialog;

import java.util.List;

public class MyAddressesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context mContext;
    final private int TYPE_ITEM = 1;
    final private int TYPE_HEADER = 0;
    private List<Address> mList;
    private ProfileListener mCallBack;
    private Address shippingAddress;
    private Address returnAddress;
    private AddressListener mListener;

    public MyAddressesAdapter(Context mContext, List<Address> mList, Address shippingAddress, Address returnAddress, ProfileListener profileListener, AddressListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.shippingAddress = shippingAddress;
        this.returnAddress = returnAddress;
        this.mCallBack = profileListener;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            AddressItemBinding itemViewItem = AddressItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderItem(itemViewItem);
        } else {
            MyAddressesHeaderBinding itemViewHeader = MyAddressesHeaderBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderHeader(itemViewHeader);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ViewHolderHeader) {
            viewHolderHeader((ViewHolderHeader) holder);
        } else if (holder instanceof ViewHolderItem) {
            position = position - 1;
            viewHolderItem((ViewHolderItem) holder, position);
        }
    }

    private void viewHolderHeader(final ViewHolderHeader holder) {
        if (shippingAddress != null) {
            holder.itemView.shippingAddress.mainLayout.setVisibility(View.VISIBLE);
            holder.itemView.shippingExistingAddress.mainLayout.setVisibility(View.GONE);
            holder.itemView.shippingAddress.deleteAddress.setVisibility(View.GONE);
            holder.itemView.shippingAddress.deleteAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogAlert.showDialog(mContext, shippingAddress, Constants.SHIPPING_ADDRESS, new DialogClickListener() {
                        @Override
                        public void onClick() {
                            mListener.deleteAddress(shippingAddress, 0);
                        }
                    });
                }
            });
            holder.itemView.shippingAddress.fullName.setText(shippingAddress.getFullName());
            if (!TextUtils.isEmpty(shippingAddress.getAddressOne())) {
                holder.itemView.shippingAddress.addressOne.setText(shippingAddress.getAddressOne());
            } else {
                holder.itemView.shippingAddress.addressOne.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(shippingAddress.getAddressTwo())) {
                holder.itemView.shippingAddress.addressTwo.setText(shippingAddress.getAddressTwo());
            } else {
                holder.itemView.shippingAddress.addressTwo.setVisibility(View.GONE);
            }
            holder.itemView.shippingAddress.zipcode.setText(shippingAddress.getCity() + " " + shippingAddress.getStateAbbreviation() + ", " + shippingAddress.getZipCode());

            holder.itemView.shippingAddress.editAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallBack.openAddNewAddressFragment(shippingAddress, Constants.EDIT_ADDRESS, null);
                }
            });
        } else {
            holder.itemView.shippingAddress.mainLayout.setVisibility(View.GONE);
            holder.itemView.shippingExistingAddress.mainLayout.setVisibility(View.VISIBLE);
            holder.itemView.shippingExistingAddress.addNewAddressLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Address address = new Address();
                    address.setShippingAddress(true);
                    address.setReturnAddress(false);
                    mCallBack.openAddNewAddressFragment(address, Constants.ADD_ADDRESS, null);
                }
            });
            holder.itemView.shippingExistingAddress.selectExistingAddressLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SelectExistingAddressDialog selectExistingAddressDialog = new SelectExistingAddressDialog();
                    selectExistingAddressDialog.showDialog(mContext, Constants.SHIPPING_ADDRESS, mList, mCallBack);
                }
            });
        }
        if (returnAddress != null) {
            holder.itemView.returnAddress.mainLayout.setVisibility(View.VISIBLE);
            holder.itemView.returnExistingAddress.mainLayout.setVisibility(View.GONE);
            holder.itemView.returnAddress.fullName.setText(returnAddress.getFullName());
            if (!TextUtils.isEmpty(returnAddress.getAddressOne())) {
                holder.itemView.returnAddress.addressOne.setText(returnAddress.getAddressOne());
            } else {
                holder.itemView.returnAddress.addressOne.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(returnAddress.getAddressTwo())) {
                holder.itemView.returnAddress.addressTwo.setText(returnAddress.getAddressTwo());
            } else {
                holder.itemView.returnAddress.addressTwo.setVisibility(View.GONE);
            }

            holder.itemView.returnAddress.zipcode.setText(returnAddress.getCity() + " " + returnAddress.getStateAbbreviation() + ", " + returnAddress.getZipCode());

            holder.itemView.returnAddress.editAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallBack.openAddNewAddressFragment(returnAddress, Constants.EDIT_ADDRESS, null);
                }
            });
            holder.itemView.returnAddress.deleteAddress.setVisibility(View.GONE);
            holder.itemView.returnAddress.deleteAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogAlert.showDialog(mContext, returnAddress, Constants.RETURN_ADDRESS, new DialogClickListener() {
                        @Override
                        public void onClick() {
                            mListener.deleteAddress(returnAddress, 0);
                        }
                    });
                }
            });
        } else {
            holder.itemView.returnAddress.mainLayout.setVisibility(View.GONE);
            holder.itemView.returnExistingAddress.mainLayout.setVisibility(View.VISIBLE);
            holder.itemView.returnExistingAddress.addNewAddressLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Address address = new Address();
                    address.setShippingAddress(true);
                    address.setReturnAddress(false);
                    mCallBack.openAddNewAddressFragment(address, Constants.ADD_ADDRESS, null);
                }
            });
            holder.itemView.returnExistingAddress.selectExistingAddressLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SelectExistingAddressDialog selectExistingAddressDialog = new SelectExistingAddressDialog();
                    selectExistingAddressDialog.showDialog(mContext, Constants.RETURN_ADDRESS, mList, mCallBack);
                }
            });
        }
        if (mList.size() == 0) {
            holder.itemView.otherAddressesText.setVisibility(View.GONE);
        } else {
            holder.itemView.otherAddressesText.setVisibility(View.VISIBLE);
        }
    }

    private void viewHolderItem(ViewHolderItem holder, final int position) {
        final Address address = mList.get(position);
        holder.itemView.fullName.setText(address.getFullName());
        if (!TextUtils.isEmpty(address.getAddressOne())) {
            holder.itemView.addressOne.setVisibility(View.VISIBLE);
            holder.itemView.addressOne.setText(address.getAddressOne());
        } else {
            holder.itemView.addressOne.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(address.getAddressTwo())) {
            holder.itemView.addressTwo.setVisibility(View.VISIBLE);
            holder.itemView.addressTwo.setText(address.getAddressTwo());
        } else {
            holder.itemView.addressTwo.setVisibility(View.GONE);
        }
        holder.itemView.zipcode.setText(address.getCity() + " " + address.getStateAbbreviation() + ", " + address.getZipCode());
        holder.itemView.editAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mCallBack.openAddNewAddressFragment(address, Constants.EDIT_ADDRESS, null);
            }
        });
        holder.itemView.deleteAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogAlert.showDialog(mContext, address, 2, new DialogClickListener() {
                    @Override
                    public void onClick() {
                        mListener.deleteAddress(address, position);
                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }

        return TYPE_ITEM;
    }


    private boolean isPositionHeader(int position) {
        return position == 0;
    }


    public void setList(List<Address> list, Address shippingAddress, Address returnAddress) {
        this.mList = list;
        this.shippingAddress = shippingAddress;
        this.returnAddress = returnAddress;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        AddressItemBinding itemView;

        public ViewHolderItem(AddressItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderHeader extends RecyclerView.ViewHolder {
        MyAddressesHeaderBinding itemView;

        private ViewHolderHeader(MyAddressesHeaderBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}