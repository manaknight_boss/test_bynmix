package com.bynmix.app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.databinding.LoadMoreProgressBinding;
import com.bynmix.app.databinding.MyBynBinding;
import com.bynmix.app.interfaces.EnableDisableDialog;
import com.bynmix.app.interfaces.MyBynListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.DialogAlert;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyBynProfileAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context mContext;
    private List<FeedResponse> mFeedList;
    private int TYPE_ITEM = 1;
    private int TYPE_PROGRESS = 0;
    private boolean isProgressRequired = false;
    private int extraCount = 0;
    private ProfileListener mCallback;
    private MyBynListener mListener;

    public MyBynProfileAdapter(Context mContext, List<FeedResponse> mFeedList, ProfileListener mCallback, MyBynListener mListener) {
        this.mContext = mContext;
        this.mFeedList = mFeedList;
        this.mCallback = mCallback;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            MyBynBinding mBindingItem = MyBynBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new MyBynProfileAdapter.ViewHolderItem(mBindingItem);
        } else {
            LoadMoreProgressBinding mBindingProgress = LoadMoreProgressBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new MyBynProfileAdapter.ViewHolderProgress(mBindingProgress);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderProgress) {
            viewHolderProgress((ViewHolderProgress) holder);
        } else if (holder instanceof ViewHolderItem) {
            viewHolderItem((ViewHolderItem) holder, position);
        }
    }

    private void viewHolderProgress(ViewHolderProgress holder) {
        if (isProgressRequired) {
            holder.itemView.loadMoreProgressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void viewHolderItem(final ViewHolderItem holder, final int position) {
        final FeedResponse feed = mFeedList.get(position);
        switch (feed.getStatus().toLowerCase()) {
            case Constants.ON_SOLD:
                holder.itemView.soldHoldLayout.setVisibility(View.VISIBLE);
                holder.itemView.soldHoldLayout.setBackgroundColor(mContext.getResources().getColor(R.color.red));
                holder.itemView.statusText.setText(mContext.getString(R.string.sold));
                holder.itemView.mainDeleteLayout.setVisibility(View.VISIBLE);
                holder.itemView.mainStatusLayout.setVisibility(View.GONE);
                break;
            case Constants.ON_HOLD:
                holder.itemView.soldHoldLayout.setVisibility(View.VISIBLE);
                holder.itemView.soldHoldLayout.setBackgroundColor(mContext.getResources().getColor(R.color.orange));
                holder.itemView.statusText.setText(mContext.getString(R.string.on_hold));
                holder.itemView.mainDeleteLayout.setVisibility(View.GONE);
                holder.itemView.mainStatusLayout.setVisibility(View.VISIBLE);
                break;
            case Constants.NOT_FOR_SALE:
                holder.itemView.soldHoldLayout.setVisibility(View.VISIBLE);
                holder.itemView.soldHoldLayout.setBackgroundColor(mContext.getResources().getColor(R.color.delete_post_black_color));
                holder.itemView.statusText.setText(mContext.getString(R.string.not_for_sale));
                holder.itemView.mainDeleteLayout.setVisibility(View.GONE);
                holder.itemView.mainStatusLayout.setVisibility(View.VISIBLE);
                break;
            default:
                holder.itemView.mainDeleteLayout.setVisibility(View.GONE);
                holder.itemView.mainStatusLayout.setVisibility(View.VISIBLE);
                holder.itemView.soldHoldLayout.setVisibility(View.GONE);
                break;
        }

        holder.itemView.time.setText(feed.getCreatedDateFormatted());
        String feedTitle = feed.getTitle();
        holder.itemView.postTitle.setText(feedTitle);
        holder.itemView.postTitle.setMaxLines(1);
        String photoUrl = feed.getPhotoUrl();
        if (!TextUtils.isEmpty(photoUrl)) {
            Picasso.get().load(photoUrl).placeholder(R.mipmap.default_feed).into(holder.itemView.blogFullImage);
        } else {
            holder.itemView.blogFullImage.setImageResource(R.mipmap.default_feed);
        }
        holder.itemView.videoPlayIcon.setVisibility(View.GONE);

        holder.itemView.blogFullImageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (feed.isActive()) {
                    mCallback.openListingDetailFragment(feed.getId());
                } else {
                    String listingTitle = getColorText(feed.getTitle());
                    listingTitle = mContext.getString(R.string.the_listing_detail_for) + " \"" + listingTitle.trim() + "\" " + mContext.getString(R.string.is_currently_disabled);
                    DialogAlert.showDialog(mContext, listingTitle, Constants.ENABLE, Constants.TYPE_LISTING, true, new EnableDisableDialog() {
                        @Override
                        public void onClickListener() {
                            mListener.disableListing(feed.getId(), position, Constants.ENABLE);
                        }

                        @Override
                        public void onDeleteListener() {
                            mListener.deleteListing(feed.getId(), position);
                        }
                    });
                }
            }
        });
        holder.itemView.editListingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (feed.isActive()) {
                    mCallback.openEditMyListingFragment(feed.getId());
                } else {
                    String listingTitle = getColorText(feed.getTitle());
                    listingTitle = mContext.getString(R.string.the_listing_detail_for) + " \"" + listingTitle.trim() + "\" " + mContext.getString(R.string.is_currently_disabled);
                    DialogAlert.showDialog(mContext, listingTitle, Constants.ENABLE, Constants.TYPE_LISTING, true, new EnableDisableDialog() {
                        @Override
                        public void onClickListener() {
                            mListener.disableListing(feed.getId(), position, Constants.ENABLE);
                        }

                        @Override
                        public void onDeleteListener() {
                            mListener.deleteListing(feed.getId(), position);
                        }
                    });
                }
            }
        });
        holder.itemView.deletePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String listingTitle = getColorText(feed.getTitle());
                listingTitle = mContext.getString(R.string.are_you_sure_to_delete) + " \"" + listingTitle.trim() + "\" ?";
                boolean isActive = feed.isActive();
                DialogAlert.showDialog(mContext, listingTitle, Constants.DELETE, Constants.TYPE_LISTING, isActive, new EnableDisableDialog() {
                    @Override
                    public void onClickListener() {
                        mListener.disableListing(feed.getId(), position, Constants.DISABLE);
                    }

                    @Override
                    public void onDeleteListener() {
                        mListener.deleteListing(feed.getId(), position);
                    }
                });
            }
        });
        holder.itemView.mainDeleteLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String listingTitle = getColorText(feed.getTitle());
                listingTitle = mContext.getString(R.string.are_you_sure_to_delete) + " \"" + listingTitle.trim() + "\" ?";
                boolean isActive = feed.isActive();
                DialogAlert.showDialog(mContext, listingTitle, Constants.DELETE, Constants.TYPE_LISTING, isActive, new EnableDisableDialog() {
                    @Override
                    public void onClickListener() {
                        mListener.disableListing(feed.getId(), position, Constants.DISABLE);
                    }

                    @Override
                    public void onDeleteListener() {
                        mListener.deleteListing(feed.getId(), position);
                    }
                });
            }
        });
        holder.itemView.statusLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String listingTitle = getColorText(feed.getTitle());
                if (feed.isActive()) {
                    listingTitle = mContext.getString(R.string.are_you_sure_to_disable) + " \"" + listingTitle.trim() + "\" ?";
                    DialogAlert.showDialog(mContext, listingTitle, Constants.DISABLE, Constants.TYPE_LISTING, true, new EnableDisableDialog() {
                        @Override
                        public void onClickListener() {
                            mListener.disableListing(feed.getId(), position, Constants.DISABLE);
                        }

                        @Override
                        public void onDeleteListener() {
                            mListener.deleteListing(feed.getId(), position);
                        }
                    });
                } else {
                    mListener.disableListing(feed.getId(), position, Constants.ENABLE);
                }
            }
        });
        updateStatusUI(holder, feed.isActive());
    }

    private String getColorText(String title) {
        return "<b>" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + title + "</font>" + "</b> ";
    }

    private void updateStatusUI(ViewHolderItem holder, boolean active) {
        if (!active) {
            holder.itemView.status.setText(mContext.getResources().getString(R.string.enable));
            holder.itemView.statusLayout.setBackgroundColor(mContext.getResources().getColor(R.color.orange_transparent));
        } else {
            holder.itemView.status.setText(mContext.getResources().getString(R.string.disable));
            holder.itemView.statusLayout.setBackgroundColor(mContext.getResources().getColor(R.color.red_transparent));
        }
    }

    @Override
    public int getItemCount() {
        return mFeedList.size() + extraCount;
    }


    public int getItemViewType(int position) {
        if (position == mFeedList.size()) {
            return TYPE_PROGRESS;
        }
        return TYPE_ITEM;
    }

    public void setProgress(boolean progressStatus) {
        isProgressRequired = progressStatus;
        if (isProgressRequired) {
            extraCount = 1;
        } else {
            extraCount = 0;
        }
        notifyDataSetChanged();
    }

    public void setList(List<FeedResponse> mFeedList) {
        this.mFeedList = mFeedList;
        notifyDataSetChanged();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder {
        MyBynBinding itemView;

        public ViewHolderItem(MyBynBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderProgress extends RecyclerView.ViewHolder {
        LoadMoreProgressBinding itemView;

        public ViewHolderProgress(LoadMoreProgressBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

}