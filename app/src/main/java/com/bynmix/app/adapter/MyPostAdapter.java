package com.bynmix.app.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.databinding.LoadMoreProgressBinding;
import com.bynmix.app.databinding.MyPostItemBinding;
import com.bynmix.app.interfaces.DotsSingleTabInterface;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MyPostAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private List<FeedResponse> mFeedList;
    private int TYPE_ITEM = 1;
    private int TYPE_PROGRESS = 0;
    private boolean isProgressRequired = false;
    private int extraCount = 0;
    private ProfileListener mCallback;

    public MyPostAdapter(Context mContext, List<FeedResponse> mFeedList, ProfileListener mCallback, int type) {
        this.mContext = mContext;
        this.mFeedList = mFeedList;
        this.mCallback = mCallback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            MyPostItemBinding mBindingItem = MyPostItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new MyPostAdapter.ViewHolderItem(mBindingItem);
        } else if (viewType == TYPE_PROGRESS) {
            LoadMoreProgressBinding mBindingProgress = LoadMoreProgressBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new MyPostAdapter.ViewHolderProgress(mBindingProgress);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyPostAdapter.ViewHolderProgress) {
            position = position + 1;
            viewHolderProgress((MyPostAdapter.ViewHolderProgress) holder);
        } else if (holder instanceof MyPostAdapter.ViewHolderItem) {
            viewHolderItem((MyPostAdapter.ViewHolderItem) holder, position);
        }
    }

    private void viewHolderProgress(MyPostAdapter.ViewHolderProgress holder) {
        if (isProgressRequired) {
            holder.itemView.loadMoreProgressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void viewHolderItem(final MyPostAdapter.ViewHolderItem holder, final int position) {
        final FeedResponse feed = mFeedList.get(position);
        updateLikeUI(holder, feed.isUserLiked());
        updateLikes(holder, feed);
        holder.itemView.heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (feed.isCanUserLikeItem()){
                    feed.setUserLiked(!feed.isUserLiked());
                    int likes;
                    if (feed.isUserLiked()) {
                        likes = feed.getLikesCount() + 1;
                    } else {
                        likes = feed.getLikesCount() - 1;
                    }
                    feed.setLikesCount(likes);
                    updateLikes(holder, feed);
                    updateLikeUI(holder, feed.isUserLiked());
                    mCallback.onPostsLike(feed.getId());

                    Intent intent = new Intent(Constants.ACTION_BROADCAST_RECEIVER_REFRESH_FEEDS);
                    intent.putExtra(Constants.FEED, feed);
                    mContext.sendBroadcast(intent);
                } else {
                    Toast.makeText(mContext, "You Can't Like this post", Toast.LENGTH_SHORT).show();
                }

            }
        });
        String feedTitle = feed.getTitle();
        holder.itemView.postTitle.setText(feedTitle);
        String description = feed.getDescription();
        if (description != null) {
            description = description.trim();
            if (TextUtils.isEmpty(description)) {
                holder.itemView.description.setVisibility(View.GONE);
            } else {
                holder.itemView.description.setVisibility(View.VISIBLE);
                holder.itemView.description.setMovementMethod(LinkMovementMethod.getInstance());
                if (description.length() < Constants.SHORT_DESCRIPTION_LENGTH) {
                    holder.itemView.description.setText(description);
                } else {
                    setDescription(holder, description, "Read More");
                }
            }
        } else {
            holder.itemView.description.setVisibility(View.GONE);
        }

        holder.itemView.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(mContext, "share clicked", Toast.LENGTH_SHORT).show();
            }
        });

        holder.itemView.likeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openLikeFragment(feed.getId(), Constants.FEED_POST);
            }
        });

        String photoUrl = feed.getPhotoUrl();
        if (!TextUtils.isEmpty(photoUrl)) {
            if (feed.getPostType() != null && feed.getPostType().equalsIgnoreCase(Constants.VIDEO_POST)) {
                holder.itemView.videoPlayIcon.setVisibility(View.VISIBLE);
                Picasso.get().load(photoUrl).placeholder(R.mipmap.novideo).into(holder.itemView.blogFullImage);
                Picasso.get().load(photoUrl).placeholder(R.mipmap.novideo).into(holder.itemView.dotsFullImage);
            } else {
                holder.itemView.videoPlayIcon.setVisibility(View.GONE);
                Picasso.get().load(photoUrl).placeholder(R.mipmap.default_feed).into(holder.itemView.blogFullImage);
                Picasso.get().load(photoUrl).placeholder(R.mipmap.default_feed).into(holder.itemView.dotsFullImage);
            }

            if (feed.getPostDots().size() == 0) {
                holder.itemView.dotsFullImageParent.setVisibility(View.INVISIBLE);
                holder.itemView.blogFullImage.setVisibility(View.VISIBLE);
                if (feed.isHasPostDots()) {
                    holder.itemView.viewTagText.setText(mContext.getString(R.string.tap_to_view_tagged));
                    holder.itemView.viewTags.setVisibility(View.VISIBLE);
                } else {
                    holder.itemView.viewTags.setVisibility(View.GONE);
                }
            } else {
                holder.itemView.viewTags.setVisibility(View.VISIBLE);
                if (feed.isVisibleDots()) {
                    holder.itemView.viewTagText.setText(mContext.getString(R.string.hide_tagged_products));
                    holder.itemView.dotsFullImageParent.setVisibility(View.VISIBLE);
                    holder.itemView.dotsFullImage.setViewDraw(true);
                    holder.itemView.dotsFullImage.setPointList(feed.getPostDots());
                    holder.itemView.blogFullImage.setVisibility(View.INVISIBLE);
                } else {
                    holder.itemView.viewTagText.setText(mContext.getString(R.string.tap_to_view_tagged));
                    holder.itemView.dotsFullImage.setPointList(new ArrayList<AnnotatedPoint>());
                    holder.itemView.dotsFullImageParent.setVisibility(View.INVISIBLE);
                    holder.itemView.blogFullImage.setVisibility(View.VISIBLE);
                }
                holder.itemView.dotsFullImage.setDotsListener(new DotsSingleTabInterface() {
                    @Override
                    public void singleTab(AnnotatedPoint annotatedPoint) {
                        if (annotatedPoint.getPostDotTypeId() == Constants.DOT_TYPE_LISTING) {
                            mCallback.openListingDetailFragment(annotatedPoint.getListingId());
                        } else {
                            mCallback.openInAppBrowser(annotatedPoint.getLink(), annotatedPoint.getTitle());
                        }
                    }

                    @Override
                    public void singleTab() {
                        postClicked(feed);
                    }
                });
            }
        } else {
            if (feed.getPostType() != null && feed.getPostType().equalsIgnoreCase(Constants.VIDEO_POST)) {
                holder.itemView.blogFullImage.setImageResource(R.mipmap.novideo);
            } else {
                holder.itemView.blogFullImage.setImageResource(R.mipmap.default_feed);
            }
            holder.itemView.dotsFullImageParent.setVisibility(View.INVISIBLE);
        }
        showIndividualProgress(holder, false);
        holder.itemView.viewTags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                feed.setVisibleDots(!feed.isVisibleDots());
                if (feed.getPostDots().size() == 0) {
                    showIndividualProgress(holder, true);
                    mCallback.getPostTags(position, feed.getId(), -2);
                } else {
                    hideProgress();
                }
            }
        });
        holder.itemView.blogFullImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postClicked(feed);
            }
        });
    }

    private void showIndividualProgress(ViewHolderItem holder, boolean isIndividualProgressRequired) {
        if (isIndividualProgressRequired) {
            holder.itemView.progressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.progressBar.setVisibility(View.GONE);
        }
    }

    private void postClicked(FeedResponse feed) {
        if (feed.getLink() != null)
            mCallback.openPostDetailFragment(feed);
    }

    private void updateLikes(MyPostAdapter.ViewHolderItem holder, FeedResponse feed) {
        int likeCount = feed.getLikesCount();
        if (likeCount == 0) {
            holder.itemView.likeLayout.setVisibility(View.GONE);
        } else if (likeCount == 1) {
            holder.itemView.likesCount.setText(feed.getLikesCount() + " Like");
            holder.itemView.likeLayout.setVisibility(View.VISIBLE);
        } else if (likeCount > 1) {
            holder.itemView.likesCount.setText(feed.getLikesCount() + " Likes");
            holder.itemView.likeLayout.setVisibility(View.VISIBLE);
        }
    }

    private void updateLikeUI(MyPostAdapter.ViewHolderItem holder, boolean userLiked) {
        if (userLiked) {
            holder.itemView.heart.setImageResource(R.mipmap.heart_white_outline);
        } else {
            holder.itemView.heart.setImageResource(R.mipmap.heart_with_white_bg);
        }
    }


    private void setDescription(final ViewHolderItem holder, final String description, final String s) {
        String descriptionSubString = description;
        if (s.equalsIgnoreCase("Read More")) {
            if (descriptionSubString.length() > Constants.SHORT_DESCRIPTION_LENGTH) {
                descriptionSubString = descriptionSubString.substring(0, Constants.SHORT_DESCRIPTION_LENGTH);
                descriptionSubString += "...";
            }
        }
        descriptionSubString.replace(" ", "&nbsp;");
        descriptionSubString.replace("\n", "<br>");
        descriptionSubString.replace("\r", "<br>");
        descriptionSubString.replace("\n\r", "<br>");

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            holder.itemView.description.setText(Html.fromHtml(descriptionSubString + " <b>" + s + "</b>", Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
        } else {
            holder.itemView.description.setText(Html.fromHtml(descriptionSubString + " <b>" + s + "</b>"), TextView.BufferType.SPANNABLE);
        }

        Spannable mySpannableDescriptionSubString = (Spannable) holder.itemView.description.getText();
        ClickableSpan myClickableDescriptionSubString = new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }

            @Override
            public void onClick(View widget) {
                if (s.equalsIgnoreCase("Read More")) {
                    setDescription(holder, description, "Read Less");
                } else {
                    setDescription(holder, description, "Read More");
                }
            }
        };
        try {
            String data = holder.itemView.description.getText().toString();
            mySpannableDescriptionSubString.setSpan(myClickableDescriptionSubString, data.length() - s.length(), data.length() - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mFeedList.size() + extraCount;
    }


    public int getItemViewType(int position) {
        if (position == mFeedList.size()) {
            return TYPE_PROGRESS;
        }
        return TYPE_ITEM;
    }

    public void setProgress(boolean progressStatus) {
        isProgressRequired = progressStatus;
        if (isProgressRequired) {
            extraCount = 1;
        } else {
            extraCount = 0;
        }
        notifyDataSetChanged();
    }

    public void setList(List<FeedResponse> mFeedList) {
        this.mFeedList = mFeedList;
        notifyDataSetChanged();
    }

    public void hideProgress() {
        notifyDataSetChanged();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder {
        MyPostItemBinding itemView;

        public ViewHolderItem(MyPostItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderProgress extends RecyclerView.ViewHolder {
        LoadMoreProgressBinding itemView;

        public ViewHolderProgress(LoadMoreProgressBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

}



