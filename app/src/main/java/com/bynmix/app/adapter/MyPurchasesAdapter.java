package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.databinding.LoadMoreProgressBinding;
import com.bynmix.app.databinding.MyPurchasesItemBinding;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.PurchaseListener;
import com.bynmix.app.models.PurchasesDetail;
import com.bynmix.app.models.SalesDetail;
import com.bynmix.app.models.Stock;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.DialogAlert;
import com.bynmix.app.utils.PrintLabelDialog;
import com.bynmix.app.utils.PurchaseFeedbackDialog;
import com.bynmix.app.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyPurchasesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context mContext;
    private List<Stock> mFeedList;
    private int TYPE_ITEM = 1;
    private int TYPE_PROGRESS = 0;
    private boolean isProgressRequired = false;
    private int extraCount = 0;
    private ProfileListener mCallback;
    private PurchaseListener purchaseListener;

    public MyPurchasesAdapter(Context mContext, List<Stock> mFeedList, ProfileListener mCallback, PurchaseListener purchaseListener) {
        this.mContext = mContext;
        this.mFeedList = mFeedList;
        this.mCallback = mCallback;
        this.purchaseListener = purchaseListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            MyPurchasesItemBinding mBindingItem = MyPurchasesItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new MyPurchasesAdapter.ViewHolderItem(mBindingItem);

        } else if (viewType == TYPE_PROGRESS) {
            LoadMoreProgressBinding mBindingProgress = LoadMoreProgressBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new MyPurchasesAdapter.ViewHolderProgress(mBindingProgress);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderProgress) {
            position = position + 1;
            viewHolderProgress((ViewHolderProgress) holder);
        } else if (holder instanceof ViewHolderItem) {
            viewHolderItem((ViewHolderItem) holder, position);
        }
    }

    private void viewHolderProgress(ViewHolderProgress holder) {
        if (isProgressRequired) {
            holder.itemView.loadMoreProgressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void viewHolderItem(final ViewHolderItem holder, final int position) {
        final Stock detail = mFeedList.get(position);
        String feedTitle = detail.getListingTitle();
        holder.itemView.listingTitle.setText(feedTitle);
        holder.itemView.listingTitle.setMaxLines(1);
        String photoUrl = detail.getListingImageUrl();
        if (!TextUtils.isEmpty(photoUrl)) {
            Picasso.get().load(photoUrl).placeholder(R.mipmap.default_feed).into(holder.itemView.listingFullImage);
        } else {
            holder.itemView.listingFullImage.setImageResource(R.mipmap.default_feed);
        }
        int statusId = 0;
        final int id;
        final int type;
        final String statusText, subStatusText;
        if (detail instanceof PurchasesDetail) {
            PurchasesDetail purchasesDetail = (PurchasesDetail) detail;
            statusId = purchasesDetail.getPurchaseStatusId();
            holder.itemView.price.setText("$" + (int) purchasesDetail.getPurchasePrice());
            holder.itemView.time.setText(purchasesDetail.getPurchaseDateFormatted());
            statusText = purchasesDetail.getPurchaseStatus();
            subStatusText = purchasesDetail.getPurchaseSubStatus();
            if (purchasesDetail.isCanCancelPurchase()) {
                holder.itemView.cancelLayout.setVisibility(View.VISIBLE);
                holder.itemView.mainFeedbackLayout.setVisibility(View.GONE);
            } else {
                holder.itemView.cancelLayout.setVisibility(View.GONE);
                holder.itemView.mainFeedbackLayout.setVisibility(View.VISIBLE);
            }
            holder.itemView.relistItem.setVisibility(View.GONE);
            id = purchasesDetail.getPurchaseId();
            type = Constants.MY_PURCHASES;
        } else {
            SalesDetail salesDetail = (SalesDetail) detail;
            statusId = salesDetail.getSaleStatusId();
            holder.itemView.price.setText("$" + (int) salesDetail.getSalePrice());
            holder.itemView.time.setText(salesDetail.getSaleDateFormatted());
            if (salesDetail.isCanCancelSale()) {
                holder.itemView.cancelLayout.setVisibility(View.VISIBLE);
                holder.itemView.mainFeedbackLayout.setVisibility(View.GONE);
            } else {
                holder.itemView.cancelLayout.setVisibility(View.GONE);
                holder.itemView.mainFeedbackLayout.setVisibility(View.VISIBLE);
            }
            statusText = salesDetail.getSaleStatus();
            subStatusText = salesDetail.getSaleSubStatus();
            holder.itemView.relistItem.setVisibility(View.VISIBLE);
            id = salesDetail.getSaleId();
            type = Constants.MY_SALES;
        }
        switch (statusId) {
            case 1:
                holder.itemView.icon.setImageResource(R.mipmap.in_transit);
                break;
            case 2:
                holder.itemView.icon.setImageResource(R.mipmap.in_transit);
                break;
            case 3:
                holder.itemView.icon.setImageResource(R.mipmap.in_transit);
                break;
            case 4:
                holder.itemView.icon.setImageResource(R.mipmap.label);
                break;
            case 5:
                holder.itemView.icon.setImageResource(R.mipmap.shipped);
                break;
            case 6:
                holder.itemView.icon.setImageResource(R.mipmap.in_transit);
                break;
            case 7:
                holder.itemView.icon.setImageResource(R.mipmap.delivered);
                break;
            case 8:
                holder.itemView.icon.setImageResource(R.mipmap.in_transit);
                break;
            case 9:
                holder.itemView.icon.setImageResource(R.mipmap.in_transit);
                break;
            case 10:
                holder.itemView.icon.setImageResource(R.mipmap.in_transit);
                break;
            case 11:
                holder.itemView.icon.setImageResource(R.mipmap.in_transit);
                break;
            case 12:
                holder.itemView.icon.setImageResource(R.mipmap.in_transit);
                break;
            case 14:
                holder.itemView.icon.setImageResource(R.mipmap.in_transit);
                break;
        }

        holder.itemView.purchasedLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detail.canPrintShippingLabel()) {
                    PrintLabelDialog.showDialog(mContext, detail, new DialogClickListener() {
                        @Override
                        public void onClick() {
                            if (mCallback.conditionsForApiCall()) {
                                SalesDetail salesDetail = (SalesDetail) detail;
                                ((HomeActivity)mContext).emailShippingLabel(salesDetail.getSaleId(), salesDetail.getListingTitle());
                            }
                        }
                    });
                } else {
                    mCallback.getTrackInformation(detail);
                }
            }
        });
        if (detail.isHasSubStatus()) {
            holder.itemView.purchaseStatusLayout.setVisibility(View.GONE);
            holder.itemView.subStatusLayout.setVisibility(View.VISIBLE);
            holder.itemView.purchaseSubStatus.setText(subStatusText);
            holder.itemView.purchaseStatusText.setText(statusText);
            holder.itemView.purchasedLayout.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        } else {
            holder.itemView.purchaseStatusLayout.setVisibility(View.VISIBLE);
            holder.itemView.subStatusLayout.setVisibility(View.GONE);
            holder.itemView.purchasedStatus.setText(statusText);
            holder.itemView.purchasedLayout.setBackgroundColor(mContext.getResources().getColor(R.color.disable_pink));
        }
        enableFeedback(holder, detail.isCanLeaveFeedback());

        if (detail.isHasOpenDispute()) {
            holder.itemView.reportText.setText(mContext.getString(R.string.view_dispute_details));
        } else {
            holder.itemView.reportText.setText(mContext.getString(R.string.report_an_issue));
        }
        enableDisableDispute(holder, detail.isCanOpenDispute() || detail.isHasOpenDispute());


        holder.itemView.reportLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detail.isHasOpenDispute()) {
                    mCallback.openDisputeDetailFragment(detail.getDisputeId(), detail);
                } else {
                    mCallback.checkReportAnIssue(detail);
                }
            }
        });

        holder.itemView.cancelLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detail instanceof PurchasesDetail) {
                    DialogAlert.showDialog(mContext, detail.getListingTitle(), mContext.getString(R.string.are_you_sure_cancel_purchase), R.string.yes_cancel_purchase, new DialogClickListener() {
                        @Override
                        public void onClick() {
                            purchaseListener.cancelPurchase(((PurchasesDetail) detail).getPurchaseId());
                        }
                    });
                } else {
                    mCallback.openCancelSaleFragment(detail);
                }
            }
        });

        holder.itemView.listingFullImageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mCallback.openPurchaseDetailFragment(id, type);
            }
        });

        if (detail.getRating() > 0) {
            holder.itemView.ratingLayout.setVisibility(View.VISIBLE);
            holder.itemView.feedbackLayout.setVisibility(View.GONE);
            drawStar(holder, detail);
        } else {
            holder.itemView.ratingLayout.setVisibility(View.GONE);
            holder.itemView.feedbackLayout.setVisibility(View.VISIBLE);
        }

        if (detail.getPointsStatusId() > 0) {
            holder.itemView.ribbonBackground.setVisibility(View.VISIBLE);
            if (detail.getPointsStatusId() == 1) {
                holder.itemView.points.setTextColor(mContext.getResources().getColor(R.color.white));
                holder.itemView.points.setText("" + detail.getPoints() + " Points");
                holder.itemView.ribbonBackground.setBackgroundResource(R.mipmap.ribbon_blue);
            } else if (detail.getPointsStatusId() == 2) {
                holder.itemView.ribbonBackground.setBackgroundResource(R.mipmap.ribbon_green);
                holder.itemView.points.setTextColor(mContext.getResources().getColor(R.color.white));
                holder.itemView.points.setText("" + detail.getPoints() + " Points");
            } else if (detail.getPointsStatusId() == 3) {
                holder.itemView.ribbonBackground.setBackgroundResource(R.mipmap.ribbon_white);
                holder.itemView.points.setTextColor(mContext.getResources().getColor(R.color.points_text_color));
                holder.itemView.points.setText("" + detail.getPoints() + " Points");
            } else if (detail.getPointsStatusId() == 4) {
                holder.itemView.ribbonBackground.setBackgroundResource(R.mipmap.ribbon_white);
                holder.itemView.points.setTextColor(mContext.getResources().getColor(R.color.points_text_color));
                holder.itemView.points.setText("" + detail.getPoints() + " Points");
            }
        } else {
            holder.itemView.ribbonBackground.setVisibility(View.GONE);
        }

        holder.itemView.feedbackLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PurchaseFeedbackDialog purchaseFeedbackDialog = new PurchaseFeedbackDialog();
                purchaseFeedbackDialog.showDialog(mContext, mCallback, detail, position);
            }
        });


        holder.itemView.relistItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogAlert.showDialog(mContext, mContext.getString(R.string.direct_purchase_text), mContext.getResources().getString(R.string.are_you_sure_to_relist), R.string.yes_relist, new DialogClickListener() {
                    @Override
                    public void onClick() {
                        if (mCallback.conditionsForApiCall()) {
                            purchaseListener.relistItem(detail.getListingId());
                        }
                    }
                });
            }
        });
    }

    private void enableDisableDispute(ViewHolderItem holder, boolean isEnabled) {
        if (isEnabled) {
            holder.itemView.reportLayout.setBackgroundColor(mContext.getResources().getColor(R.color.black));
            holder.itemView.reportLayout.setEnabled(true);
        } else {
            holder.itemView.reportLayout.setEnabled(false);
            holder.itemView.reportLayout.setBackgroundColor(mContext.getResources().getColor(R.color.disable_dialog));
        }
    }

    private void enableFeedback(ViewHolderItem holder, boolean isEnabled) {
        if (isEnabled) {
            holder.itemView.feedbackLayout.setBackgroundColor(mContext.getResources().getColor(R.color.purple));
            holder.itemView.feedbackLayout.setEnabled(true);
        } else {
            holder.itemView.feedbackLayout.setEnabled(false);
            holder.itemView.feedbackLayout.setBackgroundColor(mContext.getResources().getColor(R.color.disable_dialog));
        }
    }

    private void drawStar(ViewHolderItem holder, Stock detail) {
        holder.itemView.starLayout.removeAllViews();
        for (int i = 1; i <= 5; i++) {
            ImageView starImage = new ImageView(mContext);
            if (i <= detail.getRating()) {
                starImage.setImageResource(R.mipmap.star);
            } else {
                starImage.setImageResource(R.mipmap.star_no_color);
            }
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            int margin = Utils.dpToPx(mContext, 2);
            params.setMargins(margin, margin, margin, margin);
            holder.itemView.starLayout.addView(starImage);
        }
    }

    @Override
    public int getItemCount() {
        return mFeedList.size() + extraCount;
    }

    public int getItemViewType(int position) {
        if (position == mFeedList.size()) {
            return TYPE_PROGRESS;
        }
        return TYPE_ITEM;
    }

    public void setProgress(boolean progressStatus) {
        isProgressRequired = progressStatus;
        if (isProgressRequired) {
            extraCount = 1;
        } else {
            extraCount = 0;
        }
        notifyDataSetChanged();
    }

    public void setList(List<Stock> mFeedList) {
        this.mFeedList = mFeedList;
        notifyDataSetChanged();
    }

    public class ViewHolderItem extends RecyclerView.ViewHolder {
        MyPurchasesItemBinding itemView;

        public ViewHolderItem(MyPurchasesItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    public class ViewHolderProgress extends RecyclerView.ViewHolder {
        LoadMoreProgressBinding itemView;

        public ViewHolderProgress(LoadMoreProgressBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

}



