package com.bynmix.app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ahmadrosid.svgloader.SvgLoader;
import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.databinding.MyWalletHeaderBinding;
import com.bynmix.app.databinding.PaymentItemBinding;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.interfaces.MyWalletListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.CardDetail;
import com.bynmix.app.models.TotalSales;
import com.bynmix.app.utils.DialogAlert;

import java.util.List;

public class MyWalletAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    final private int TYPE_ITEM = 1;
    final private int TYPE_HEADER = 0;
    private List<CardDetail> mList;
    private ProfileListener mCallBack;
    private CardDetail cardDetail;
    private TotalSales currentBalance;
    private MyWalletListener myWalletListener;


    public MyWalletAdapter(Context mContext, List<CardDetail> mList, CardDetail mCardDetail, ProfileListener profileListener, MyWalletListener myWalletListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.cardDetail = mCardDetail;
        this.mCallBack = profileListener;
        this.myWalletListener = myWalletListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            PaymentItemBinding itemViewItem = PaymentItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderItem(itemViewItem);
        } else {
            MyWalletHeaderBinding itemViewHeader = MyWalletHeaderBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderHeader(itemViewHeader);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof ViewHolderHeader) {
            viewHolderHeader((ViewHolderHeader) holder);
        } else if (holder instanceof ViewHolderItem) {
            position = position - 1;
            viewHolderItem((ViewHolderItem) holder, position);
        }
    }


    private void viewHolderHeader(final ViewHolderHeader holder) {
        holder.itemView.balance.setText("" + currentBalance.getTotalAvailableForWithdrawal() + ".00");

        holder.itemView.defaultPayment.deletePayment.setVisibility(View.GONE);

        holder.itemView.defaultPayment.deletePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogAlert.showDialog(mContext, cardDetail, true, mList.size() > 0 ? 1 : 2, new DialogClickListener() {
                    @Override
                    public void onClick() {
                        mCallBack.openSelectNewPaymentFragment(mList, cardDetail, 0);
                    }
                });
            }
        });
        holder.itemView.defaultPayment.editPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.openAddPaymentFragment(cardDetail.getCardId(), null, 0);
            }
        });
        holder.itemView.addPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.openAddPaymentFragment(0, null, 0);
            }
        });

        if (mList.size() == 0) {
            holder.itemView.paymentLayout.setVisibility(View.GONE);
            holder.itemView.addDefaultPayment.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.paymentLayout.setVisibility(View.VISIBLE);
            holder.itemView.addDefaultPayment.setVisibility(View.GONE);
        }
        if (cardDetail != null) {
            holder.itemView.addDefaultPayment.setVisibility(View.GONE);
            holder.itemView.paymentLayout.setVisibility(View.VISIBLE);
            if (mList.size() == 0) {
                holder.itemView.savedPaymentText.setVisibility(View.GONE);
            } else {
                holder.itemView.savedPaymentText.setVisibility(View.VISIBLE);
            }
            String photoUrl = cardDetail.getBrandImageUrl();

            if (!TextUtils.isEmpty(photoUrl)) {
                SvgLoader.pluck()
                        .with((HomeActivity) mContext)
                        .load(photoUrl, holder.itemView.defaultPayment.cardImage);
            }
            String cardName = "<b>" + cardDetail.getBrand() + "</b> " + " ending in " + "<b>" + cardDetail.getLastFour() + "<b>";
            holder.itemView.defaultPayment.cardHeading.setText(Html.fromHtml(cardName), TextView.BufferType.SPANNABLE);
            holder.itemView.defaultPayment.name.setText(cardDetail.getNameOnCard());
            holder.itemView.defaultPayment.expiryDate.setText("Expires " + cardDetail.getExpirationMonth() + "/" + cardDetail.getExpirationYear());
        } else {
            holder.itemView.addDefaultPayment.setVisibility(View.VISIBLE);
            holder.itemView.paymentLayout.setVisibility(View.GONE);
        }
        if (currentBalance.getTotalAvailableForWithdrawal() > 0) {
            holder.itemView.withdraw.setBackground(mContext.getResources().getDrawable(R.drawable.purple_rounded_button));
            holder.itemView.withdraw.setEnabled(true);
        } else {
            holder.itemView.withdraw.setBackground(mContext.getResources().getDrawable(R.drawable.disable_onboarding_button));
            holder.itemView.withdraw.setEnabled(false);
        }
        holder.itemView.withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myWalletListener.getPayouts(currentBalance.getTotalAvailableForWithdrawal());
            }
        });

        holder.itemView.balanceHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.openTransactionHistoryScreen();
            }
        });

    }

    private void viewHolderItem(ViewHolderItem holder, final int position) {

        final CardDetail cardDetail = mList.get(position);
        String photoUrl = cardDetail.getBrandImageUrl();

        if (!TextUtils.isEmpty(photoUrl)) {
            SvgLoader.pluck()
                    .with((HomeActivity) mContext)
                    .load(photoUrl, holder.itemView.cardImage);
        }
        String cardName = "<b>" + cardDetail.getBrand() + "</b> " + " ending in " + "<b>" + cardDetail.getLastFour() + "<b>";
        holder.itemView.cardHeading.setText(Html.fromHtml(cardName), TextView.BufferType.SPANNABLE);
        holder.itemView.name.setText(cardDetail.getNameOnCard());
        holder.itemView.expiryDate.setText("Expires " + cardDetail.getExpirationMonth() + "/" + cardDetail.getExpirationYear());

        holder.itemView.deletePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myWalletListener.checkCard(mContext, cardDetail, mCallBack, position, mList);

                /*DialogAlert.showDialog(mContext, cardDetail, false, 1, new DialogClickListener() {
                    @Override
                    public void onClick() {
                        myWalletListener.deleteCardDetail(cardDetail, position);
                    }
                });*/
            }
        });

        holder.itemView.editPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.openAddPaymentFragment(cardDetail.getCardId(), null, 0);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }


    private boolean isPositionHeader(int position) {
        return position == 0;
    }


    public void setList(List<CardDetail> list, CardDetail cardDetail, TotalSales currentBalance) {
        this.mList = list;
        this.cardDetail = cardDetail;
        this.currentBalance = currentBalance;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        PaymentItemBinding itemView;

        public ViewHolderItem(PaymentItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderHeader extends RecyclerView.ViewHolder {
        MyWalletHeaderBinding itemView;

        private ViewHolderHeader(MyWalletHeaderBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}