package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ahmadrosid.svgloader.SvgLoader;
import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.databinding.LoadMoreProgressBinding;
import com.bynmix.app.databinding.NotificationOfferItemBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.Notification;
import com.bynmix.app.sharedpreference.SharedPreferenceUtility;
import com.bynmix.app.utils.Constants;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NotificationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context mContext;
    private List<Notification> mList;
    private final ProfileListener mCallback;
    private int TYPE_ITEM = 1;
    private int TYPE_PROGRESS = 0;
    private boolean isProgressRequired = false;
    private int extraCount = 0;
    private int type;

    public NotificationAdapter(Context mContext, List<Notification> mList, ProfileListener mCallback, int type) {
        this.mContext = mContext;
        this.mList = mList;
        this.mCallback = mCallback;
        this.type = type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            NotificationOfferItemBinding mBindingItem = NotificationOfferItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new NotificationAdapter.ViewHolderItem(mBindingItem);
        } else if (viewType == TYPE_PROGRESS) {
            LoadMoreProgressBinding mBindingProgress = LoadMoreProgressBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new NotificationAdapter.ViewHolderProgress(mBindingProgress);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof NotificationAdapter.ViewHolderProgress) {
            position = position + 1;
            viewHolderProgress((NotificationAdapter.ViewHolderProgress) holder);
        } else if (holder instanceof NotificationAdapter.ViewHolderItem) {
            viewHolderItem((NotificationAdapter.ViewHolderItem) holder, position);
        }
    }

    private void viewHolderProgress(NotificationAdapter.ViewHolderProgress holder) {
        if (isProgressRequired) {
            holder.itemView.loadMoreProgressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void viewHolderItem(final NotificationAdapter.ViewHolderItem holder, int position) {
        final Notification notification = mList.get(position);
        if (notification.isUnread() && !(type == Constants.RECEIVED_OFFERS || type == Constants.MY_OFFERS)) {
            holder.itemView.mainItem.setBackground(mContext.getResources().getDrawable(R.drawable.purple_rounded_button));
        } else {
            holder.itemView.mainItem.setBackground(mContext.getResources().getDrawable(R.drawable.white_round_corner_background));
        }
        holder.itemView.time.setText(notification.getNotificationDateFormatted());
        final int notificationTypeId = notification.getNotificationTypeId();
        if (type == Constants.RECEIVED_OFFERS || type == Constants.MY_OFFERS) {
            if (type == Constants.MY_OFFERS) {
                if (notification.isSellerOffer() && notification.isActiveOffer()) {
                    holder.itemView.viewLayout.setVisibility(View.GONE);
                    holder.itemView.offersLayout.setVisibility(View.VISIBLE);
                } else {
                    holder.itemView.viewLayout.setVisibility(View.VISIBLE);
                    holder.itemView.offersLayout.setVisibility(View.GONE);
                }
            } else {
                if (notification.isActiveOffer()) {
                    holder.itemView.viewLayout.setVisibility(View.GONE);
                    holder.itemView.offersLayout.setVisibility(View.VISIBLE);
                } else {
                    holder.itemView.viewLayout.setVisibility(View.VISIBLE);
                    holder.itemView.offersLayout.setVisibility(View.GONE);
                }
            }
        } else {
            holder.itemView.viewLayout.setVisibility(View.GONE);
            holder.itemView.offersLayout.setVisibility(View.GONE);
        }
        if (notificationTypeId == Constants.PROMOTION) {
            holder.itemView.icon.setVisibility(View.GONE);
            holder.itemView.image.setVisibility(View.GONE);
            holder.itemView.mainText.setTextColor(mContext.getResources().getColor(R.color.white));
            holder.itemView.time.setTextColor(mContext.getResources().getColor(R.color.white));
            holder.itemView.promotionImage.setVisibility(View.VISIBLE);
            setPromotion(notification.getPromotionTypeId(), holder);
        } else {
            holder.itemView.icon.setVisibility(View.VISIBLE);
            holder.itemView.image.setVisibility(View.VISIBLE);
            holder.itemView.promotionImage.setVisibility(View.GONE);
            holder.itemView.imageLayout.setBackgroundColor(mContext.getResources().getColor(R.color.white));
            holder.itemView.mainLayout.setBackgroundColor(mContext.getResources().getColor(R.color.white));
            holder.itemView.mainText.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
            holder.itemView.time.setTextColor(mContext.getResources().getColor(R.color.notification_time_color));
            String image;
            final String icon = notification.getIconUrl();
            if (!TextUtils.isEmpty(icon)) {
                SvgLoader.pluck()
                        .with((HomeActivity) mContext)
                        .load(icon, holder.itemView.icon);
            }
            if (notificationTypeId == Constants.POST_LIKED || notificationTypeId == Constants.COMMENT_RECEIVED || notificationTypeId == Constants.USER_FOLLOWING) {
                image = notification.getUserImage();
            } else {
                image = notification.getListingImage();
            }

            if (!TextUtils.isEmpty(image)) {
                Picasso.get().load(image).placeholder(R.mipmap.default_feed).into(holder.itemView.image);
            } else {
                holder.itemView.image.setImageResource(R.mipmap.default_feed);
            }
            holder.itemView.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (notificationTypeId == Constants.POST_LIKED) {
                        if (notification.getPostLink() != null) {
                            if (notification.isActive()) {
                                mCallback.openPostDetailFragmentWithId(notification.getPostId());
                            }
                        }
                    } else {
                        if (notificationTypeId == Constants.COMMENT_RECEIVED || notificationTypeId == Constants.USER_FOLLOWING) {
                            mCallback.openProfile(notification.getInitiatorUserId());
                        } else {
                            ((HomeActivity) mContext).mProgressBarDialog.showProgress();
                            mCallback.getListingDetail(notification.getListingId());
                        }
                    }
                }
            });
        }
        holder.itemView.counterOfferLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openMakeAnOfferFragment(notification.getListingId(), notification.getOfferId(), notification.getInitiatorUserId());
            }
        });

        holder.itemView.statusLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean isSeller = false;
                SharedPreferenceUtility sharedPreferenceUtility = new SharedPreferenceUtility(mContext);
                if (notification.getSellerId() == sharedPreferenceUtility.getMyUserId()) {
                    isSeller = true;
                } else {
                    isSeller = false;
                }
                mCallback.acceptOffer(notification.getListingId(), notification.getOfferId(), isSeller);
            }
        });

        holder.itemView.deleteLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.declineOffer(notification.getListingId(), notification.getOfferId(), type);
            }
        });

        holder.itemView.viewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openMakeAnOfferFragment(notification.getListingId(), notification.getOfferId(), notification.getInitiatorUserId());
            }
        });

        String notificationMainText = notification.getFormattedText();
        boolean hasUnderline = false;
        if (notificationMainText.contains("<u>")) {
            hasUnderline = true;
        }

        holder.itemView.mainText.setText(Html.fromHtml(notificationMainText), TextView.BufferType.SPANNABLE);
        holder.itemView.mainText.setMovementMethod(LinkMovementMethod.getInstance());
        Spannable mySpannableText = (Spannable) holder.itemView.mainText.getText();
        if (hasUnderline) {
            final String underlineText = notificationMainText.substring(notificationMainText.indexOf("<u>") + 3, notificationMainText.indexOf("</u>"));
            Spannable mySpannableUnderlineText = (Spannable) holder.itemView.mainText.getText();
            int index = mySpannableUnderlineText.toString().indexOf(underlineText);
            ClickableSpan myClickableSpanUnderlineText = new ClickableSpan() {
                @Override
                public void updateDrawState(TextPaint ds) {

                }

                @Override
                public void onClick(View widget) {
                    if (notificationTypeId == Constants.POST_LIKED) {
                        if (notification.getPostLink() != null) {
                            mCallback.openPostDetailFragmentWithId(notification.getPostId());
                        }
                    } else {
                        if (notificationTypeId == Constants.COMMENT_RECEIVED || notificationTypeId == Constants.USER_FOLLOWING) {
                            mCallback.openProfile(notification.getInitiatorUserId());
                        } else {
                            ((HomeActivity) mContext).mProgressBarDialog.showProgress();
                            mCallback.getListingDetail(notification.getListingId());
                        }
                    }
                }
            };

            try {
                mySpannableUnderlineText.setSpan(myClickableSpanUnderlineText, index, index + underlineText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        ClickableSpan myClickableSpanText = new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);    // this remove the underline
            }

            @Override
            public void onClick(View widget) {
                mCallback.openProfile(notification.getInitiatorUserId());
            }
        };
        int indexOfUserName = 0;
        int lengthOfUserName = mySpannableText.toString().indexOf(" ", indexOfUserName);
        try {
            mySpannableText.setSpan(myClickableSpanText, indexOfUserName, indexOfUserName + lengthOfUserName, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getColorText(String title) {
        return "<b>" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + title + "</font>" + "</b> ";
    }

    private void setPromotion(int notificationTypeId, ViewHolderItem holder) {
        int color = 0, imageColor = 0, icon = 0;
        if (notificationTypeId == Constants.FIVE_PERCENT_OFF || notificationTypeId == Constants.TEN_PERCENT_OFF) {
            color = mContext.getResources().getColor(R.color.notification_promotion_discount_color);
            imageColor = mContext.getResources().getColor(R.color.notification_promotion_discount_image_color);
            icon = R.mipmap.discount_icon;
        } else if (notificationTypeId == Constants.FREE_SHIPPING || notificationTypeId == Constants.THREE_DOLLAR_SHIPPING) {
            color = mContext.getResources().getColor(R.color.notification_promotion_shipping_color);
            imageColor = mContext.getResources().getColor(R.color.notification_promotion_shipping_image_color);
            icon = R.mipmap.free_shipping_icon;
        } else {
            color = mContext.getResources().getColor(R.color.notification_promotion_points_color);
            imageColor = mContext.getResources().getColor(R.color.notification_promotion_points_image_color);
            icon = R.mipmap.two_x_star;
        }
        holder.itemView.promotionImage.setBackgroundColor(imageColor);
        holder.itemView.promotionImage.setImageResource(icon);
        holder.itemView.mainLayout.setBackgroundColor(color);

    }

    @Override
    public int getItemCount() {
        return mList.size() + extraCount;
    }


    public int getItemViewType(int position) {
        if (position == mList.size()) {
            return TYPE_PROGRESS;
        }
        return TYPE_ITEM;
    }

    public void setProgress(boolean progressStatus) {
        isProgressRequired = progressStatus;
        if (isProgressRequired) {
            extraCount = 1;
        } else {
            extraCount = 0;
        }
        notifyDataSetChanged();
    }

    public void setList(List<Notification> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder {
        NotificationOfferItemBinding itemView;

        public ViewHolderItem(NotificationOfferItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderProgress extends RecyclerView.ViewHolder {
        LoadMoreProgressBinding itemView;

        public ViewHolderProgress(LoadMoreProgressBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}