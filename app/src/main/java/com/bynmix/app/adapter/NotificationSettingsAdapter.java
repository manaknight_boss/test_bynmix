package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.bynmix.app.R;
import com.bynmix.app.databinding.MyWalletFooterBinding;
import com.bynmix.app.databinding.NotificationSettingsItemBinding;
import com.bynmix.app.interfaces.NotificationSettingListener;
import com.bynmix.app.models.NotificationSettings;

import java.util.ArrayList;
import java.util.List;

import static com.bynmix.app.R.*;

public class NotificationSettingsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    final private int TYPE_ITEM = 1;
    final private int TYPE_FOOTER = 0;
    private List<NotificationSettings> mList;
    private NotificationSettingListener mCallBack;
    private List<NotificationSettings> settings = new ArrayList<>();

    public NotificationSettingsAdapter(Context mContext, List<NotificationSettings> mList, NotificationSettingListener mCallback) {
        this.mList = mList;
        this.mContext = mContext;
        this.mCallBack = mCallback;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {

            NotificationSettingsItemBinding mBinding = NotificationSettingsItemBinding.inflate
                    (LayoutInflater.from(mContext), parent, false);
            return new NotificationSettingsAdapter.ViewHolderItem(mBinding);
        } else {
            MyWalletFooterBinding mBindingHeader = MyWalletFooterBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new NotificationSettingsAdapter.ViewHolderHeader(mBindingHeader);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof NotificationSettingsAdapter.ViewHolderHeader) {
            ViewHolderHeader((NotificationSettingsAdapter.ViewHolderHeader) holder);
        } else if (holder instanceof NotificationSettingsAdapter.ViewHolderItem) {
            viewHolderItem((NotificationSettingsAdapter.ViewHolderItem) holder, position);
        }
    }


    private void ViewHolderHeader(final NotificationSettingsAdapter.ViewHolderHeader holder) {
        holder.itemView.text.setText(mContext.getString(string.update_text));
        holder.itemView.addNewPaymentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.updateNotificationSettings(settings);
            }
        });
    }


    private void viewHolderItem(final NotificationSettingsAdapter.ViewHolderItem holder, final int position) {
        final NotificationSettings notificationSettings = mList.get(position);
        holder.itemView.title.setText(notificationSettings.getSettingType().getTypeTitle());
        holder.itemView.description.setText(notificationSettings.getSettingType().getDescription());
        holder.itemView.switchButton.setChecked(!notificationSettings.isEnabled());
        holder.itemView.switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (!isChecked) {
                    notificationSettings.setEnabled(true);
                } else {
                    notificationSettings.setEnabled(false);
                }
                updateList(notificationSettings);
            }
        });

    }

    private void updateList(NotificationSettings notificationSettings) {
        if (settings.size() > 0) {
            for (int i = 0; i < settings.size(); i++) {
                if (notificationSettings.getSettingNotificationId() == settings.get(i).getSettingNotificationId()) {
                    settings.get(i).setEnabled(notificationSettings.isEnabled());
                } else {
                    settings.add(notificationSettings);
                }
            }
        } else {

            settings.add(notificationSettings);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size() + 1;

    }

    public int getItemViewType(int position) {
        if (position == mList.size()) {
            return TYPE_FOOTER;
        }

        return TYPE_ITEM;
    }

    public void setList(List<NotificationSettings> list) {
        this.mList = list;
        notifyDataSetChanged();
    }


    class ViewHolderItem extends RecyclerView.ViewHolder {
        NotificationSettingsItemBinding itemView;

        public ViewHolderItem(NotificationSettingsItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderHeader extends RecyclerView.ViewHolder {
        MyWalletFooterBinding itemView;

        private ViewHolderHeader(MyWalletFooterBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
