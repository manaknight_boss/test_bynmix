package com.bynmix.app.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.MakeOfferHeaderBinding;
import com.bynmix.app.databinding.OfferHistoryItemBinding;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.interfaces.OfferHistoryListener;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.models.SubOfferHistory;
import com.bynmix.app.utils.DialogAlert;
import com.bynmix.app.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class OfferHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context mContext;
    final private int TYPE_ITEM = 1;
    final private int TYPE_HEADER = 0;
    private List<SubOfferHistory> mList;
    private CreatePostListener mCallBack;
    private ListingDetail listingDetail;
    private boolean isBuyer;
    private boolean isOfferCancelable;
    private OfferHistoryListener offerHistoryListener;

    public OfferHistoryAdapter(Context mContext, List<SubOfferHistory> mList, CreatePostListener mCallBack, ListingDetail listingDetail, boolean isBuyer, boolean offerCancelable, OfferHistoryListener offerHistoryListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mCallBack = mCallBack;
        this.listingDetail = listingDetail;
        this.isBuyer = isBuyer;
        this.isOfferCancelable = offerCancelable;
        this.offerHistoryListener = offerHistoryListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            OfferHistoryItemBinding itemViewItem = OfferHistoryItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderItem(itemViewItem);
        } else {
            MakeOfferHeaderBinding itemViewHeader = MakeOfferHeaderBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderHeader(itemViewHeader);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderHeader) {
            viewHolderHeader((ViewHolderHeader) holder);
        } else if (holder instanceof ViewHolderItem) {
            position = position - 1;
            viewHolderItem((ViewHolderItem) holder, position);
        }
    }


    private void viewHolderHeader(final ViewHolderHeader holder) {
        String title = Utils.getColorText(listingDetail.getTitle().trim(), mContext.getResources().getColor(R.color.purple));
        holder.itemView.title.setText(Html.fromHtml("\"" + title + "\""), TextView.BufferType.SPANNABLE);

        if (mList.size() > 0) {
            holder.itemView.offerHistoryText.setVisibility(View.VISIBLE);
            holder.itemView.priceLayout.setVisibility(View.GONE);
            holder.itemView.currentPriceLayout.setVisibility(View.VISIBLE);
            holder.itemView.currentPrice.setText("$" + Utils.formatValueToTwoDecimalPlaces(listingDetail.getPrice()));
        } else {
            holder.itemView.currentPriceLayout.setVisibility(View.GONE);
            holder.itemView.offerHistoryText.setVisibility(View.GONE);
            holder.itemView.priceLayout.setVisibility(View.VISIBLE);
            holder.itemView.currentPriceText.setText("Current Price:");
            holder.itemView.price.setText("$" + Utils.formatValueToTwoDecimalPlaces(listingDetail.getPrice()));
        }

        String image = this.listingDetail.getPhotoUrl();
        if (!TextUtils.isEmpty(image)) {
            Picasso.get().load(image).into(holder.itemView.image);
        } else {
            holder.itemView.image.setImageResource(R.mipmap.default_feed);
        }
        if (isBuyer) {
            holder.itemView.headingText.setText("Make an offer for:");
        } else {
            holder.itemView.headingText.setText("Counter offer for :");
        }
    }

    private void viewHolderItem(ViewHolderItem holder, final int position) {
        final SubOfferHistory offerHistory = mList.get(position);
        int screenWidth = Utils.getScreenWidth(mContext);
        double offerWidth = screenWidth * .7;
        RelativeLayout.LayoutParams paramsLeft, paramsRight;
        paramsLeft = new RelativeLayout.LayoutParams((int)offerWidth, RelativeLayout.LayoutParams.WRAP_CONTENT);
        paramsRight = new RelativeLayout.LayoutParams((int)offerWidth, RelativeLayout.LayoutParams.WRAP_CONTENT);
        paramsRight.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        holder.itemView.mainLayoutLeft.setLayoutParams(paramsLeft);
        holder.itemView.mainLayoutRight.setLayoutParams(paramsRight);

        holder.itemView.rightCancelLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogAlert.showDialog(mContext, listingDetail.getTitle(), mContext.getResources().getString(R.string.are_you_sure_to_cancel_your_offer), R.string.cancel_my_offer, new DialogClickListener() {
                    @Override
                    public void onClick() {
                        if (mCallBack.conditionsForApiCall()) {
                            offerHistoryListener.cancelOffer(offerHistory);
                        }
                    }
                });
            }
        });

        holder.itemView.leftCancelLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogAlert.showDialog(mContext, listingDetail.getTitle(), mContext.getResources().getString(R.string.are_you_sure_to_cancel_your_offer), R.string.cancel_my_offer, new DialogClickListener() {
                    @Override
                    public void onClick() {
                        if (mCallBack.conditionsForApiCall()) {
                            offerHistoryListener.cancelOffer(offerHistory);
                        }
                    }
                });
            }
        });

        if(offerHistory.getOfferType() == SubOfferHistory.TYPE_OFFER) {
            if (offerHistory.isBuyerOffer()) {
                setContainer(holder, false, offerHistory, position == mList.size() - 1);
                setOfferUI(holder, false, offerHistory);
            } else {
                setContainer(holder, true, offerHistory, position == mList.size() - 1);
                setOfferUI(holder, true, offerHistory);
            }
        } else if(offerHistory.getOfferType() == SubOfferHistory.TYPE_DECLINED) {
            if (offerHistory.isDeclinedBySeller()) {
                setContainer(holder, true, offerHistory, position == mList.size() - 1);
                setDeclinedUI(holder, true, offerHistory);
            } else {
                setContainer(holder, false, offerHistory, position == mList.size() - 1);
                setDeclinedUI(holder, false, offerHistory);
            }
        } else {
            if (offerHistory.isAcceptedByBuyer()) {
                setContainer(holder, false, offerHistory, position == mList.size() - 1);
                setAcceptedUI(holder, false, offerHistory);
            } else {
                setContainer(holder, true, offerHistory, position == mList.size() - 1);
                setAcceptedUI(holder, true, offerHistory);
            }
        }
    }

    private void setOfferUI(ViewHolderItem holder, boolean isLeft, SubOfferHistory offerHistory) {
        String offerString;
        if(isLeft) {
            offerString = "counteroffered";
        } else {
            offerString = "offered";
        }
        String price = offerString + " " + String.format("$%.2f", offerHistory.getOfferPrice());
        SpannableStringBuilder str = new SpannableStringBuilder(price);
        str.setSpan(new StyleSpan(Typeface.BOLD), price.indexOf('$'), price.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        if(isLeft) {
            setDate(holder.itemView.dateLeft, offerHistory.getOfferDateFormatted());
            holder.itemView.leftOfferPrice.setText(str);
            holder.itemView.leftContainer.setBackgroundColor(mContext.getResources().getColor(R.color.offer_left));
        } else {
            setDate(holder.itemView.dateRight, offerHistory.getOfferDateFormatted());
            holder.itemView.rightOfferPrice.setText(str);
            holder.itemView.rightContainer.setBackgroundColor(mContext.getResources().getColor(R.color.offer_right));
        }
    }

    private void setDeclinedUI(ViewHolderItem holder, boolean isLeft, SubOfferHistory offerHistory) {
        String price = offerHistory.getOfferTypeName() + " " + String.format("$%.2f", offerHistory.getDeclinedPrice());
        SpannableStringBuilder str = new SpannableStringBuilder(price);
        str.setSpan(new StyleSpan(Typeface.BOLD), price.indexOf('$'), price.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        if(isLeft) {
            holder.itemView.leftOfferPrice.setText(str);
            setDate(holder.itemView.dateLeft, offerHistory.getDeclinedDateFormatted());
            holder.itemView.leftDeclinedLayout.setVisibility(View.VISIBLE);
            holder.itemView.rightDeclinedLayout.setVisibility(View.GONE);
            holder.itemView.leftDeclinedText.setText(offerHistory.getDeclinedReason());
            holder.itemView.leftContainer.setBackgroundColor(mContext.getResources().getColor(R.color.offer_decline));
        } else {
            holder.itemView.rightOfferPrice.setText(str);
            setDate(holder.itemView.dateRight, offerHistory.getDeclinedDateFormatted());
            holder.itemView.leftDeclinedLayout.setVisibility(View.GONE);
            holder.itemView.rightDeclinedLayout.setVisibility(View.VISIBLE);
            holder.itemView.rightDeclinedText.setText(offerHistory.getDeclinedReason());
            holder.itemView.rightContainer.setBackgroundColor(mContext.getResources().getColor(R.color.offer_decline));
        }
    }

    private void setAcceptedUI(ViewHolderItem holder, boolean isLeft, SubOfferHistory offerHistory) {
        String price = offerHistory.getOfferTypeName() + " " + String.format("$%.2f", offerHistory.getAcceptedPrice());
        SpannableStringBuilder str = new SpannableStringBuilder(price);
        str.setSpan(new StyleSpan(Typeface.BOLD), price.indexOf('$'), price.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        if(isLeft) {
            holder.itemView.leftOfferPrice.setText(str);
            setDate(holder.itemView.dateLeft, offerHistory.getAcceptedDateFormatted());
            holder.itemView.leftContainer.setBackgroundColor(mContext.getResources().getColor(R.color.offer_accept));
        } else {
            holder.itemView.rightOfferPrice.setText(str);
            setDate(holder.itemView.dateRight, offerHistory.getAcceptedDateFormatted());
            holder.itemView.leftContainer.setBackgroundColor(mContext.getResources().getColor(R.color.offer_accept));
        }
    }

    @Override
    public int getItemCount() {
        return mList.size() + 1;
    }

    private void setContainer(ViewHolderItem holder, boolean isLeft, SubOfferHistory offerHistory, boolean isCancelButtonRequired) {
        if(isLeft) {
            holder.itemView.mainLayoutRight.setVisibility(View.GONE);
            holder.itemView.mainLayoutLeft.setVisibility(View.VISIBLE);
            setUsername(holder.itemView.leftUserName, offerHistory.getSellerUsername());
            setUserImage(holder.itemView.imageLeft, offerHistory.getSellerUserImage());
            showCancelButton(holder.itemView.leftCancelLayout, isCancelButtonRequired);
        } else {
            holder.itemView.mainLayoutRight.setVisibility(View.VISIBLE);
            holder.itemView.mainLayoutLeft.setVisibility(View.GONE);
            setUsername(holder.itemView.rightUserName, offerHistory.getBuyerUsername());
            setUserImage(holder.itemView.imageRight, offerHistory.getBuyerUserImage());
            showCancelButton(holder.itemView.rightCancelLayout, isCancelButtonRequired);
        }
    }

    private void showCancelButton(RelativeLayout layout, boolean isCancelButtonRequired) {
        if (isCancelButtonRequired) {
            if (isOfferCancelable) {
                layout.setVisibility(View.VISIBLE);
            } else {
                layout.setVisibility(View.GONE);
            }
        } else {
            layout.setVisibility(View.GONE);
        }
    }


    private void setUserImage(ImageView userImage, String userPhoto) {
        if (!TextUtils.isEmpty(userPhoto)) {
            Picasso.get().load(userPhoto).placeholder(R.mipmap.default_feed).into(userImage);
        } else {
            userImage.setImageResource(R.mipmap.default_feed);
        }
    }

    private void setDate(AppCompatTextView dateTextView, String dateFormatted) {
        dateTextView.setText(dateFormatted);
    }

    private void setUsername(AppCompatTextView userNameTextView, String username) {
        userNameTextView.setText(username);
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }

        return TYPE_ITEM;
    }


    private boolean isPositionHeader(int position) {
        return position == 0;
    }


    public void setList(List<SubOfferHistory> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    public void setIsOfferCancelable(boolean isOfferCancelable) {
        this.isOfferCancelable = isOfferCancelable;
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        OfferHistoryItemBinding itemView;

        public ViewHolderItem(OfferHistoryItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderHeader extends RecyclerView.ViewHolder {
        MakeOfferHeaderBinding itemView;

        private ViewHolderHeader(MakeOfferHeaderBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
