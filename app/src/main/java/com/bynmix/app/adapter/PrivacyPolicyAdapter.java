package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bynmix.app.databinding.TermsOfServiceItemBinding;
import com.bynmix.app.databinding.TextViewBinding;
import com.bynmix.app.models.Sections;
import com.bynmix.app.utils.Utils;

import java.util.List;

public class PrivacyPolicyAdapter extends RecyclerView.Adapter<PrivacyPolicyAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<Sections> mList;

    public PrivacyPolicyAdapter(Context mContext, List<Sections> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public PrivacyPolicyAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        TermsOfServiceItemBinding mBinding = TermsOfServiceItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new PrivacyPolicyAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(PrivacyPolicyAdapter.ViewHolderItem holder, int position) {
        Sections sections = mList.get(position);
        holder.itemView.title.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(sections.getDescription())) {
            holder.itemView.description.setVisibility(View.VISIBLE);
            holder.itemView.description.setText(sections.getDescription());
        } else {
            holder.itemView.description.setVisibility(View.GONE);
        }
        if (sections.getPoints() == null) {
            holder.itemView.points.setVisibility(View.GONE);
        } else {
            holder.itemView.points.setVisibility(View.VISIBLE);
            for (int i = 0; i < sections.getPoints().size(); i++) {
                TextViewBinding mBinding = TextViewBinding.inflate(LayoutInflater.from(mContext));
                mBinding.pointText.setText("- " + sections.getPoints().get(i));
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                int marginSmall = Utils.dpToPx(mContext,4);
                int marginLarge = Utils.dpToPx(mContext,8);
                layoutParams.setMargins(marginSmall,marginSmall,marginSmall,marginLarge);
                mBinding.pointText.setLayoutParams(layoutParams);
                holder.itemView.points.addView(mBinding.pointText);
            }
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<Sections> mList) {
        this.mList.clear();
        this.mList = mList;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        TermsOfServiceItemBinding itemView;

        public ViewHolderItem(TermsOfServiceItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

