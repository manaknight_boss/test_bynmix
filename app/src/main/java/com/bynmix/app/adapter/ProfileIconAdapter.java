package com.bynmix.app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.databinding.ProfileIconsBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.UserResponse;
import com.bynmix.app.sharedpreference.SharedPreferenceUtility;
import com.bynmix.app.utils.Constants;

import java.util.List;

public class ProfileIconAdapter extends RecyclerView.Adapter<ProfileIconAdapter.ViewHolderItem> {
    private final Context mContext;
    private final List<Integer> profileIconList;
    private final ProfileListener mCallback;
    private UserResponse user;
    private SharedPreferenceUtility mSharedPreferenceUtility;

    public ProfileIconAdapter(Context mContext, List<Integer> profileIconList, ProfileListener mCallback) {
        this.mContext = mContext;
        this.profileIconList = profileIconList;
        this.mCallback = mCallback;
        mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);
    }

    @Override
    public ProfileIconAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        ProfileIconsBinding mBinding = ProfileIconsBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new ProfileIconAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ProfileIconAdapter.ViewHolderItem holder, final int position) {
        int pic = profileIconList.get(position);
        holder.itemView.profileIcons.setImageResource(pic);
        holder.itemView.profileIcons.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (position) {
                    case 0:
                        mCallback.openEditProfileFragment(user);
                        break;
                    case 1:
                        mCallback.openMyBynFragment();
                        break;
                    case 2:
                        mCallback.openMyPostFragment();
                        break;
                    case 3:
                        mCallback.openMyLikeFragment(user.getUsername(), mSharedPreferenceUtility.getMyUserId(),Constants.MY_BYN);
                        break;
                    case 4:
                        mCallback.openPurchasesFragment(Constants.MY_PURCHASES);
                        break;
                    case 5:
                        mCallback.openPurchasesFragment(Constants.MY_SALES);
                        break;
                    case 6:
                        mCallback.openWallet();

                        break;
                    case 7:
                        mCallback.openMyAddressFragment();
                        break;
                    case 8:
                        break;
                    case 9:
                        mCallback.openNotificationSettingsFragment();
                        break;
                    case 10:
                        break;
                    case 11:
                        break;
                    case 12:
                        mCallback.logout();
                        break;
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return profileIconList.size();
    }

    public void setUser(UserResponse user) {
        this.user = user;
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ProfileIconsBinding itemView;

        public ViewHolderItem(ProfileIconsBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
