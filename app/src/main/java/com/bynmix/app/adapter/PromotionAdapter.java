package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.PromotionItemBinding;
import com.bynmix.app.models.Promotions;

import java.util.List;

public class PromotionAdapter extends RecyclerView.Adapter<PromotionAdapter.ViewHolderItem> {

    private final Context mContext;
    private final List<Promotions> mList;

    public PromotionAdapter(Context mContext, List<Promotions> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public PromotionAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        PromotionItemBinding mBinding = PromotionItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new PromotionAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(PromotionAdapter.ViewHolderItem holder, final int position) {
        Promotions promotions = mList.get(position);
        if (promotions.getPromotionTypeId() > 0 && promotions.getPromotionTypeId() < 4) {
            holder.itemView.crossLayout.setBackgroundColor(mContext.getResources().getColor(R.color.notification_promotion_points_image_color));
            holder.itemView.mainLayout.setBackgroundColor(mContext.getResources().getColor(R.color.notification_promotion_points_color));
            holder.itemView.mainText.setText(Html.fromHtml(promotions.getBannerText()), TextView.BufferType.SPANNABLE);
            if (promotions.getPromotionTypeId() == 1) {
                holder.itemView.image.setImageResource(R.mipmap.two_x_star);
            } else if (promotions.getPromotionTypeId() == 2) {
                holder.itemView.image.setImageResource(R.mipmap.three_x_star);
            } else if (promotions.getPromotionTypeId() == 3) {
                holder.itemView.image.setImageResource(R.mipmap.four_x_star);
            }
        } else if (promotions.getPromotionTypeId() > 3 && promotions.getPromotionTypeId() < 6) {
            holder.itemView.crossLayout.setBackgroundColor(mContext.getResources().getColor(R.color.notification_promotion_shipping_image_color));
            holder.itemView.mainLayout.setBackgroundColor(mContext.getResources().getColor(R.color.notification_promotion_shipping_color));
            holder.itemView.image.setImageResource(R.mipmap.free_shipping_icon);
        } else if (promotions.getPromotionTypeId() > 5 && promotions.getPromotionTypeId() < 8) {
            holder.itemView.crossLayout.setBackgroundColor(mContext.getResources().getColor(R.color.notification_promotion_discount_image_color));
            holder.itemView.mainLayout.setBackgroundColor(mContext.getResources().getColor(R.color.notification_promotion_discount_color));
            holder.itemView.image.setImageResource(R.mipmap.discount_icon);
        }
        holder.itemView.crossLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mList.remove(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        PromotionItemBinding itemView;

        public ViewHolderItem(PromotionItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}


