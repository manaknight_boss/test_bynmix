package com.bynmix.app.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.bynmix.app.R;
import com.bynmix.app.databinding.LoadMoreProgressBinding;
import com.bynmix.app.databinding.PopUpItemBinding;
import com.bynmix.app.databinding.PurchaseHistoryHeaderBinding;
import com.bynmix.app.databinding.PurchaseHistoryItemBinding;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.PurchaseListener;
import com.bynmix.app.models.PurchasesDetail;
import com.bynmix.app.models.SalesDetail;
import com.bynmix.app.models.Stock;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.DialogAlert;
import com.bynmix.app.utils.PurchaseFeedbackDialog;
import com.bynmix.app.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PurchaseHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context mContext;
    final private int TYPE_HEADER = 0;
    final private int TYPE_ITEM = 1;
    final private int TYPE_PROGRESS = 2;
    private List<Stock> mList;
    private ProfileListener mCallBack;
    private int extraCount = 0;
    private boolean isProgressRequired = false;
    private int type;
    private PurchaseListener purchaseListener;

    public PurchaseHistoryAdapter(Context mContext, List<Stock> mList, ProfileListener mCallBack, int type, PurchaseListener purchaseListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mCallBack = mCallBack;
        this.type = type;
        this.purchaseListener = purchaseListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            PurchaseHistoryHeaderBinding itemViewHeader = PurchaseHistoryHeaderBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderHeader(itemViewHeader);
        } else if (viewType == TYPE_PROGRESS) {
            LoadMoreProgressBinding mBindingProgress = LoadMoreProgressBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderProgress(mBindingProgress);
        } else {
            PurchaseHistoryItemBinding itemViewItem = PurchaseHistoryItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ViewHolderItem(itemViewItem);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHolderHeader) {
            viewHolderHeader((ViewHolderHeader) holder);
        } else if (holder instanceof ViewHolderProgress) {
            viewHolderProgress((ViewHolderProgress) holder);
        } else {
            viewHolderItem((ViewHolderItem) holder, position - 1);
        }
    }

    private void viewHolderHeader(final ViewHolderHeader holder) {
        if (type == Constants.MY_SALES) {
            holder.itemView.purchasePointLayout.setVisibility(View.GONE);
            holder.itemView.salePointsLayout.setVisibility(View.VISIBLE);
            holder.itemView.salePointsEarned.setText("" + mCallBack.mySaleState().getTotalSalePoints());
            holder.itemView.salePendingPoints.setText("" + mCallBack.mySaleState().getTotalPendingSalePoints() + " Points Pending");
            holder.itemView.profitEarned.setText("$ " + Utils.formatValueToTwoDecimalPlaces(mCallBack.mySaleState().getTotalSales()));
            holder.itemView.pendingPrice.setText("$ " + mCallBack.mySaleState().getTotalPendingSales() + " Pending");
        } else {
            holder.itemView.purchasePointLayout.setVisibility(View.VISIBLE);
            holder.itemView.salePointsLayout.setVisibility(View.GONE);
            holder.itemView.pointsEarned.setText("" + mCallBack.myPurchaseState().getTotalPurchasePoints());
            holder.itemView.pendingPoints.setText("" + mCallBack.myPurchaseState().getTotalPendingPurchasePoints() + " Points Pending");
        }
    }

    private void viewHolderProgress(ViewHolderProgress holder) {
        if (isProgressRequired) {
            holder.itemView.loadMoreProgressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void viewHolderItem(final ViewHolderItem holder, final int position) {
        final Stock detail = mList.get(position);
        holder.itemView.promotionPrice.setVisibility(View.GONE);
        holder.itemView.promotionPoints.setVisibility(View.GONE);
        if (detail.getPromotions() != null) {
            int count = detail.getPromotions().size();
            if (count > 0) {
                List<String> promotion = detail.getPromotionsFormatted();
                for (int i = 0; i < count; i++) {
                    if(detail.getPromotionsFormatted().get(i).toLowerCase().contains(new String("Promotion Price").toLowerCase())) {
                        holder.itemView.promotionPrice.setVisibility(View.VISIBLE);
                        holder.itemView.promotionPrice.setText(promotion.get(i));
                    } else {
                        holder.itemView.promotionPoints.setVisibility(View.VISIBLE);
                        holder.itemView.promotionPoints.setText(promotion.get(i));
                    }
                }
            } else {
                holder.itemView.promotionPrice.setVisibility(View.GONE);
                holder.itemView.promotionPoints.setVisibility(View.GONE);
            }
        }

        if (detail.getPointsStatusId() > 0) {
            holder.itemView.pointsEarned.setVisibility(View.VISIBLE);
            if (detail.getPointsStatusId() == 1) {
                holder.itemView.pointsEarned.setTextColor(mContext.getResources().getColor(R.color.blue));
                holder.itemView.pointsEarned.setText("Points Earned: " + detail.getPoints() + " Points (Pending Feedback left)");
            } else if (detail.getPointsStatusId() == 2) {
                holder.itemView.pointsEarned.setTextColor(mContext.getResources().getColor(R.color.green));
                holder.itemView.pointsEarned.setText("Points Earned: " + detail.getPoints() + " Points");
            } else if (detail.getPointsStatusId() == 3) {
                holder.itemView.pointsEarned.setTextColor(mContext.getResources().getColor(R.color.black));
                holder.itemView.pointsEarned.setText("Points Retracted: " + detail.getPoints() + " Points");
            } else if (detail.getPointsStatusId() == 4) {
                holder.itemView.pointsEarned.setTextColor(mContext.getResources().getColor(R.color.black));
                holder.itemView.pointsEarned.setText("Points Pending: " + detail.getPoints() + " Points (Pending Dispute)");
            } else {
                holder.itemView.pointsEarned.setVisibility(View.GONE);
            }
        } else {
            holder.itemView.pointsEarned.setVisibility(View.GONE);
        }

        final int id;
        final String status;
        String newDate;
        if (detail instanceof PurchasesDetail) {
            PurchasesDetail purchasesDetail = (PurchasesDetail) detail;
            newDate = purchasesDetail.getPurchaseMonth() + " " + purchasesDetail.getPurchaseYear();
            status = purchasesDetail.getPurchaseStatus();
            id = purchasesDetail.getPurchaseId();
            holder.itemView.time.setText(purchasesDetail.getPurchaseDateFormatted());
            holder.itemView.originalPrice.setText("$" + (int) purchasesDetail.getPurchasePrice());
            holder.itemView.shippingPrice.setText("$" + (int) purchasesDetail.getShippingPrice());
            holder.itemView.totalPrice.setText("$" + (int) (purchasesDetail.getPurchaseCharged()));
        } else {
            SalesDetail salesDetail = (SalesDetail) detail;
            newDate = salesDetail.getSaleMonth() + " " + salesDetail.getSaleYear();
            status = salesDetail.getSaleStatus();
            id = salesDetail.getSaleId();
            holder.itemView.time.setText(salesDetail.getSaleDateFormatted());
            holder.itemView.originalPrice.setText("$" + (int) salesDetail.getSalePrice());
            holder.itemView.shippingPrice.setText("$" + (int) salesDetail.getShippingPrice());
            holder.itemView.totalPrice.setText("$" + (int) salesDetail.getSaleTotalDisplayPrice());
        }
        if (position == 0) {
            holder.itemView.date.setText(newDate);
            holder.itemView.dateLayout.setVisibility(View.VISIBLE);
        } else {
            Stock previousDetail = mList.get(position - 1);
            String previousDate;
            if (detail instanceof PurchasesDetail) {
                PurchasesDetail purchasesDetail = (PurchasesDetail) previousDetail;
                previousDate = purchasesDetail.getPurchaseMonth() + " " + purchasesDetail.getPurchaseYear();
            } else {
                SalesDetail salesDetail = (SalesDetail) previousDetail;
                previousDate = salesDetail.getSaleMonth() + " " + salesDetail.getSaleYear();
            }
            if (newDate.equalsIgnoreCase(previousDate)) {
                holder.itemView.dateLayout.setVisibility(View.GONE);
            } else {
                holder.itemView.date.setText(newDate);
                holder.itemView.dateLayout.setVisibility(View.VISIBLE);
            }
        }
        String feedTitle = detail.getListingTitle();
        if (!TextUtils.isEmpty(feedTitle)) {
            holder.itemView.listingTitle.setText(feedTitle);
        } else {
            holder.itemView.listingTitle.setText("");
        }
        String photoUrl = detail.getListingImageUrl();
        if (!TextUtils.isEmpty(photoUrl)) {
            Picasso.get().load(photoUrl).placeholder(R.mipmap.default_feed).into(holder.itemView.listingFullImage);
        } else {
            holder.itemView.listingFullImage.setImageResource(R.mipmap.default_feed);
        }
        holder.itemView.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.openPurchaseDetailFragment(id, type);
            }
        });

        holder.itemView.moreOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(holder, detail, status);
            }
        });
    }

    private void showPopup(ViewHolderItem holder, final Stock detail, String status) {
        PopUpItemBinding mBinding = PopUpItemBinding.inflate(LayoutInflater.from(mContext));
        String statusText, subStatusText;
        int statusId;
        if (detail instanceof PurchasesDetail) {
            PurchasesDetail purchasesDetail = (PurchasesDetail) detail;
            if (purchasesDetail.isCanCancelPurchase()) {
                mBinding.cancelLayout.setVisibility(View.VISIBLE);
                mBinding.mainFeedbackLayout.setVisibility(View.GONE);
            } else {
                mBinding.cancelLayout.setVisibility(View.GONE);
                mBinding.mainFeedbackLayout.setVisibility(View.VISIBLE);
            }
            mBinding.relistItem.setVisibility(View.GONE);
            statusText = purchasesDetail.getPurchaseStatus();
            subStatusText = purchasesDetail.getPurchaseSubStatus();
            statusId = purchasesDetail.getPurchaseStatusId();
        } else {
            SalesDetail salesDetail = (SalesDetail) detail;
            if (salesDetail.isCanCancelSale()) {
                mBinding.cancelLayout.setVisibility(View.VISIBLE);
                mBinding.mainFeedbackLayout.setVisibility(View.GONE);
            } else {
                mBinding.cancelLayout.setVisibility(View.GONE);
                mBinding.mainFeedbackLayout.setVisibility(View.VISIBLE);
            }
            subStatusText = salesDetail.getSaleSubStatus();
            statusText = salesDetail.getSaleStatus();
            statusId = salesDetail.getSaleStatusId();
        }

        if (detail.isHasSubStatus()) {
            mBinding.purchaseStatusLayout.setVisibility(View.GONE);
            mBinding.subStatusLayout.setVisibility(View.VISIBLE);
            mBinding.purchaseSubStatus.setText(subStatusText);
            mBinding.purchaseStatusText.setText(statusText);
        } else {
            mBinding.purchaseStatusLayout.setVisibility(View.VISIBLE);
            mBinding.subStatusLayout.setVisibility(View.GONE);
            mBinding.statusText.setText(statusText);
        }


        switch (statusId) {
            case 1:
                mBinding.icon.setImageResource(R.mipmap.in_transit);
                break;
            case 2:
                mBinding.icon.setImageResource(R.mipmap.in_transit);
                break;
            case 3:
                mBinding.icon.setImageResource(R.mipmap.in_transit);
                break;
            case 4:
                mBinding.icon.setImageResource(R.mipmap.label);
                break;
            case 5:
                mBinding.icon.setImageResource(R.mipmap.shipped);
                break;
            case 6:
                mBinding.icon.setImageResource(R.mipmap.in_transit);
                break;
            case 7:
                mBinding.icon.setImageResource(R.mipmap.delivered);
                break;
            case 8:
                mBinding.icon.setImageResource(R.mipmap.in_transit);
                break;
            case 9:
                mBinding.icon.setImageResource(R.mipmap.in_transit);
                break;
            case 10:
                mBinding.icon.setImageResource(R.mipmap.in_transit);
                break;
            case 11:
                mBinding.icon.setImageResource(R.mipmap.in_transit);
                break;
            case 12:
                mBinding.icon.setImageResource(R.mipmap.in_transit);
                break;
        }

        final PopupWindow popupWindow = new PopupWindow(
                mBinding.getRoot(),
                Utils.dpToPx(mContext, 185),
                ViewGroup.LayoutParams.WRAP_CONTENT);

//        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupWindow.setFocusable(true);
        popupWindow.setOutsideTouchable(true);
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                //TODO do sth here on dismiss
            }
        });
        mBinding.statusLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.getTrackInformation(detail);
                popupWindow.dismiss();
            }
        });

        mBinding.cancelLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detail instanceof PurchasesDetail) {
                    DialogAlert.showDialog(mContext, detail.getListingTitle(), mContext.getResources().getString(R.string.are_you_sure_cancel_purchase), R.string.yes_cancel_purchase, new DialogClickListener() {
                        @Override
                        public void onClick() {
                            purchaseListener.cancelPurchase(((PurchasesDetail) detail).getPurchaseId());
                        }
                    });
                } else {
                    mCallBack.openCancelSaleFragment(detail);
                }
                popupWindow.dismiss();
            }
        });

        enableDisableDispute(mBinding, detail.isCanOpenDispute());

        if (detail.isHasOpenDispute()) {
            mBinding.reportText.setText(mContext.getString(R.string.view_dispute_details));
            enableDisableDispute(mBinding, true);
        } else {
            mBinding.reportText.setText(mContext.getString(R.string.report_an_issue));
        }


        mBinding.reportAnIssueLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (detail.isHasOpenDispute()) {
                    mCallBack.openDisputeDetailFragment(detail.getDisputeId(), detail);
                } else {
                    mCallBack.checkReportAnIssue(detail);
                }
                popupWindow.dismiss();
            }
        });

        mBinding.leaveFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
                PurchaseFeedbackDialog purchaseFeedbackDialog = new PurchaseFeedbackDialog();
                purchaseFeedbackDialog.showDialog(mContext, mCallBack, detail, 0);
            }
        });

        mBinding.relistItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogAlert.showDialog(mContext, detail.getListingTitle(), mContext.getResources().getString(R.string.are_you_sure_to_relist), R.string.yes_relist, new DialogClickListener() {
                    @Override
                    public void onClick() {
                        if (mCallBack.conditionsForApiCall()) {
                            purchaseListener.relistItem(detail.getListingId());

                        }
                    }
                });
                popupWindow.dismiss();
            }
        });

        enableFeedBack(mBinding, detail.isCanLeaveFeedback());
        if (detail.getRating() > 0) {
            mBinding.ratingLayout.setVisibility(View.VISIBLE);
            mBinding.leaveFeedback.setVisibility(View.GONE);
            drawStar(detail, mBinding);
        } else {
            mBinding.ratingLayout.setVisibility(View.GONE);
            mBinding.leaveFeedback.setVisibility(View.VISIBLE);
        }
//        popupWindow.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.background_shadow));
        popupWindow.showAsDropDown(holder.itemView.moreOptions, 0, 0);
    }

    private void enableDisableDispute(PopUpItemBinding mBinding, boolean isEnabled) {
        if (isEnabled) {
            mBinding.reportAnIssueLayout.setBackgroundColor(mContext.getResources().getColor(R.color.black));
            mBinding.reportAnIssueLayout.setEnabled(true);
        } else {
            mBinding.reportAnIssueLayout.setEnabled(false);
            mBinding.reportAnIssueLayout.setBackgroundColor(mContext.getResources().getColor(R.color.disable_dialog));
        }
    }

    private void drawStar(Stock detail, PopUpItemBinding mBinding) {
        mBinding.starLayout.removeAllViews();
        for (int i = 1; i <= 5; i++) {
            ImageView starImage = new ImageView(mContext);
            if (i <= detail.getRating()) {
                starImage.setImageResource(R.mipmap.star);
            } else {
                starImage.setImageResource(R.mipmap.star_no_color);
            }
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            int margin = Utils.dpToPx(mContext, 2);
            params.setMargins(margin, margin, margin, margin);
            mBinding.starLayout.addView(starImage);
        }
    }

    private void enableFeedBack(PopUpItemBinding mBinding, boolean isEnabled) {
        if (isEnabled) {
            mBinding.leaveFeedback.setBackgroundColor(mContext.getResources().getColor(R.color.purple));
            mBinding.leaveFeedback.setEnabled(true);
        } else {
            mBinding.leaveFeedback.setEnabled(false);
            mBinding.leaveFeedback.setBackgroundColor(mContext.getResources().getColor(R.color.disable_dialog));
        }
    }

    private void setFeedTitleLength(ViewHolderItem holder, String feedTitle, int maxLength) {
        if (!TextUtils.isEmpty(feedTitle)) {
            if (feedTitle.length() > maxLength) {
                String feedTitleSubstring = feedTitle.substring(0, maxLength);
                feedTitleSubstring = feedTitleSubstring + "...";
                holder.itemView.listingTitle.setText(feedTitleSubstring);
            } else {
                holder.itemView.listingTitle.setText(feedTitle);
            }
        } else {
            holder.itemView.listingTitle.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return mList.size() + 1 + extraCount;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        } else if (isProgressRequired) {
            return TYPE_PROGRESS;
        }
        return TYPE_ITEM;
    }


    private boolean isPositionHeader(int position) {
        return position == 0;
    }


    public void setProgress(boolean progressStatus) {
        isProgressRequired = progressStatus;
        if (isProgressRequired) {
            extraCount = 1;
        } else {
            isProgressRequired = false;
            extraCount = 0;
        }
        notifyDataSetChanged();
    }


    public void setList(List<Stock> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        PurchaseHistoryItemBinding itemView;

        public ViewHolderItem(PurchaseHistoryItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderHeader extends RecyclerView.ViewHolder {
        PurchaseHistoryHeaderBinding itemView;

        private ViewHolderHeader(PurchaseHistoryHeaderBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    public class ViewHolderProgress extends RecyclerView.ViewHolder {
        LoadMoreProgressBinding itemView;

        public ViewHolderProgress(LoadMoreProgressBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}