package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bynmix.app.R;
import com.bynmix.app.databinding.LoadMoreProgressBinding;
import com.bynmix.app.databinding.MyPostItemBinding;
import com.bynmix.app.databinding.ReviewItemBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.Reviews;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ReviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private List<Reviews> mFeedList;
    private int TYPE_ITEM = 1;
    private int TYPE_PROGRESS = 0;
    private boolean isProgressRequired = false;
    private int extraCount = 0;
    private ProfileListener mCallback;
    private int type;

    public ReviewAdapter(Context mContext, List<Reviews> mFeedList, ProfileListener mCallback, int type) {
        this.mContext = mContext;
        this.mFeedList = mFeedList;
        this.mCallback = mCallback;
        this.type = type;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            ReviewItemBinding mBindingItem = ReviewItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ReviewAdapter.ViewHolderItem(mBindingItem);
        } else if (viewType == TYPE_PROGRESS) {
            LoadMoreProgressBinding mBindingProgress = LoadMoreProgressBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new ReviewAdapter.ViewHolderProgress(mBindingProgress);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ReviewAdapter.ViewHolderProgress) {
            position = position + 1;
            viewHolderProgress((ReviewAdapter.ViewHolderProgress) holder);
        } else if (holder instanceof ReviewAdapter.ViewHolderItem) {
            viewHolderItem((ReviewAdapter.ViewHolderItem) holder, position);
        }
    }

    private void viewHolderProgress(ReviewAdapter.ViewHolderProgress holder) {
        if (isProgressRequired) {
            holder.itemView.loadMoreProgressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void viewHolderItem(final ReviewAdapter.ViewHolderItem holder, final int position) {
        final Reviews reviews = mFeedList.get(position);
        String lisingTitle = reviews.getListingTitle();
        String userImage;
        if (type == Constants.BUYER) {
            userImage = reviews.getBuyerPhotoUrl();
            holder.itemView.username.setText(reviews.getSellerUsername());
        } else {
            userImage = reviews.getSellerPhotoUrl();
            holder.itemView.username.setText(reviews.getBuyerUsername());
        }
        holder.itemView.time.setText(reviews.getCreatedDateFormatted());
        holder.itemView.title.setText(lisingTitle);
        holder.itemView.feedback.setText("\"" + reviews.getFeedback() + "\"");
        String photoUrl = reviews.getListingPhotoUrl();
        if (!TextUtils.isEmpty(photoUrl)) {
            Picasso.get().load(photoUrl).placeholder(R.mipmap.default_feed).into(holder.itemView.image);
        } else {
            holder.itemView.image.setImageResource(R.mipmap.default_feed);
        }
        if (!TextUtils.isEmpty(userImage)) {
            Picasso.get().load(userImage).placeholder(R.mipmap.default_profile_image).into(holder.itemView.userPic);
        } else {
            holder.itemView.image.setImageResource(R.mipmap.default_profile_image);
        }
        drawStars(holder, reviews.getRating());
    }

    private void drawStars(ViewHolderItem holder, double rating) {
        holder.itemView.starLayout.removeAllViews();
        for (int i = 1; i <= 5; i++) {
            ImageView starImage = new ImageView(mContext);
            if (i <= rating) {
                starImage.setImageResource(R.mipmap.star_reviews);
            } else {
                starImage.setImageResource(R.mipmap.star_small);
            }
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            int margin = Utils.dpToPx(mContext, 2);
            params.setMargins(margin, 0, margin, 0);
            starImage.setLayoutParams(params);
            holder.itemView.starLayout.addView(starImage);
        }
    }


    @Override
    public int getItemCount() {
        return mFeedList.size() + extraCount;
    }


    public int getItemViewType(int position) {
        if (position == mFeedList.size()) {
            return TYPE_PROGRESS;
        }
        return TYPE_ITEM;
    }

    public void setProgress(boolean progressStatus) {
        isProgressRequired = progressStatus;
        if (isProgressRequired) {
            extraCount = 1;
        } else {
            extraCount = 0;
        }
        notifyDataSetChanged();
    }

    public void setList(List<Reviews> mFeedList) {
        this.mFeedList = mFeedList;
        notifyDataSetChanged();
    }

    public void hideProgress() {
        notifyDataSetChanged();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder {
        ReviewItemBinding itemView;

        public ViewHolderItem(ReviewItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderProgress extends RecyclerView.ViewHolder {
        LoadMoreProgressBinding itemView;

        public ViewHolderProgress(LoadMoreProgressBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

}



