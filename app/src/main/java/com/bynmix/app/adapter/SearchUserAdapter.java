package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.databinding.LoadMoreProgressBinding;
import com.bynmix.app.databinding.SearchUserItemBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.UserResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SearchUserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private List<UserResponse> mUserList;
    private int TYPE_ITEM = 1;
    private int TYPE_PROGRESS = 0;
    private boolean isProgressRequired = false;
    private int extraCount = 0;
    private ProfileListener mCallback;

    public SearchUserAdapter(Context mContext, List<UserResponse> mUserList, ProfileListener mCallback) {
        this.mContext = mContext;
        this.mUserList = mUserList;
        this.mCallback = mCallback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            SearchUserItemBinding mBindingItem = SearchUserItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new SearchUserAdapter.ViewHolderItem(mBindingItem);
        } else if (viewType == TYPE_PROGRESS) {
            LoadMoreProgressBinding mBindingProgress = LoadMoreProgressBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
            return new SearchUserAdapter.ViewHolderProgress(mBindingProgress);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SearchUserAdapter.ViewHolderProgress) {
            position = position + 1;
            viewHolderProgress((SearchUserAdapter.ViewHolderProgress) holder);
        } else if (holder instanceof SearchUserAdapter.ViewHolderItem) {
            viewHolderItem((SearchUserAdapter.ViewHolderItem) holder, position);
        }
    }

    private void viewHolderProgress(SearchUserAdapter.ViewHolderProgress holder) {
        if (isProgressRequired) {
            holder.itemView.loadMoreProgressBar.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.loadMoreProgressBar.setVisibility(View.GONE);
        }
    }

    private void viewHolderItem(final SearchUserAdapter.ViewHolderItem holder, int position) {
        final UserResponse user = mUserList.get(position);
        holder.itemView.userName.setText("@" + user.getUsername());

        if (!TextUtils.isEmpty(user.getTitle())) {
            holder.itemView.blogerText.setVisibility(View.VISIBLE);
            holder.itemView.blogerText.setText(user.getTitle());
        } else {
            holder.itemView.blogerText.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(user.getUserDescriptorsFormatted())) {
            holder.itemView.stylesLayout.setVisibility(View.VISIBLE);
            holder.itemView.styles.setText(user.getUserDescriptorsFormatted());
        } else {
            holder.itemView.stylesLayout.setVisibility(View.GONE);
        }


        if (!TextUtils.isEmpty(user.getUserBrandsFormatted())) {
            holder.itemView.brandLayout.setVisibility(View.VISIBLE);
            holder.itemView.brands.setText(user.getUserBrandsFormatted());
        } else {
            holder.itemView.brandLayout.setVisibility(View.GONE);
        }


        String photoUrl = user.getPhotoUrl();

        if (!TextUtils.isEmpty(photoUrl)) {
            Picasso.get().load(photoUrl).placeholder(R.mipmap.default_profile_image).into(holder.itemView.blogPic);
        } else {
            holder.itemView.blogPic.setImageResource(R.mipmap.default_profile_image);
        }
        holder.itemView.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openProfile(user.getUserId());
            }
        });


        if (user.isUserSameAsLoggedInUser()) {
            holder.itemView.followFollowingIcon.setVisibility(View.GONE);
        } else {
            holder.itemView.followFollowingIcon.setVisibility(View.VISIBLE);
        }

        updateFollowFollowingUI(holder, user.isLoggedInUserFollowing());
        holder.itemView.followFollowingIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user.setLoggedInUserFollowing(!user.isLoggedInUserFollowing());

                updateFollowFollowingUI(holder, user.isLoggedInUserFollowing());
                mCallback.onFollowerUser(user.getUserId());
            }
        });
    }

    private void updateFollowFollowingUI(ViewHolderItem holder, boolean loggedInUserFollowing) {
        if (loggedInUserFollowing) {
            holder.itemView.followFollowingIcon.setImageResource(R.mipmap.following_icon);
        } else {
            holder.itemView.followFollowingIcon.setImageResource(R.mipmap.follow_icon);
        }
    }

    @Override
    public int getItemCount() {
        return mUserList.size() + extraCount;
    }


    public int getItemViewType(int position) {
        if (position == mUserList.size()) {
            return TYPE_PROGRESS;
        }

        return TYPE_ITEM;
    }

    public void setProgress(boolean progressStatus) {
        isProgressRequired = progressStatus;

        if (isProgressRequired) {
            extraCount = 1;
        } else {
            extraCount = 0;
        }
        notifyDataSetChanged();
    }

    public void setList(List<UserResponse> mUserList) {
        this.mUserList = mUserList;
        notifyDataSetChanged();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder {
        SearchUserItemBinding itemView;

        public ViewHolderItem(SearchUserItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderProgress extends RecyclerView.ViewHolder {
        LoadMoreProgressBinding itemView;

        public ViewHolderProgress(LoadMoreProgressBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
