package com.bynmix.app.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.CategoryItemBinding;
import com.bynmix.app.interfaces.CategoryListener;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.Category;
import com.bynmix.app.models.CategoryType;
import com.bynmix.app.models.ListingDetail;

import java.util.List;

public class SelectCategoryAdapter extends RecyclerView.Adapter<SelectCategoryAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<Category> mCategoryList;
    private CreatePostListener mCallback;
    private ListingDetail listingDetail;
    private CategoryType categoryType = null;

    public SelectCategoryAdapter(Context mContext, CategoryType categoryType, List<Category> mCategoryList, ListingDetail listingDetail, CreatePostListener mCallback) {
        this.mContext = mContext;
        this.categoryType = categoryType;
        this.mCategoryList = mCategoryList;
        this.mCallback = mCallback;
        this.listingDetail = listingDetail;

    }

    @NonNull
    @Override
    public SelectCategoryAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        CategoryItemBinding mBinding = CategoryItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new SelectCategoryAdapter.ViewHolderItem(mBinding);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull final SelectCategoryAdapter.ViewHolderItem holder, final int position) {
        final Category category = mCategoryList.get(position);
        holder.itemView.category.setText(category.getCategoryName());
        String categoryName = category.getCategoryName();
        if (category.isCategorySelected()) {
            categoryName = "<b>" + "<font color='" + mContext.getResources().getColor(R.color.purple) + "'>" + categoryName + "</font>" + "</b> ";
            holder.itemView.category.setText(Html.fromHtml(categoryName), TextView.BufferType.SPANNABLE);
        } else {
            categoryName = "<font color='" + mContext.getResources().getColor(R.color.colorPrimary) + "'>" + categoryName + "</font>";
            holder.itemView.category.setText(Html.fromHtml(categoryName), TextView.BufferType.SPANNABLE);
        }
        holder.itemView.categoryParent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetCategoryDialogList();
                category.setCategorySelected(true);
//                listingDetail.setCategory(category);
                mCallback.setCategoryInListingDetail(category, categoryType);
                mCallback.onBackPressed();
//                mCallback.selectedCategory(listingDetail);
//                notifyDataSetChanged();
            }
        });
    }

    private void resetCategoryDialogList() {
        for (Category category : mCategoryList) {
            category.setCategorySelected(false);
        }
    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }

    public void setList(List<Category> categoryList) {

        this.mCategoryList = categoryList;
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        CategoryItemBinding itemView;

        public ViewHolderItem(CategoryItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
