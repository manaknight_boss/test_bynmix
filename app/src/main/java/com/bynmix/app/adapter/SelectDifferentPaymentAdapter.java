package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ahmadrosid.svgloader.SvgLoader;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.databinding.SelectDifferentPaymentItemBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.SelectNewPaymentListener;
import com.bynmix.app.models.CardDetail;
import com.bynmix.app.utils.Constants;

import java.util.List;

public class SelectDifferentPaymentAdapter extends RecyclerView.Adapter<SelectDifferentPaymentAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<CardDetail> mList;
    private ProfileListener mCallBack;
    private SelectNewPaymentListener mListener;
    private int type;

    public SelectDifferentPaymentAdapter(Context mContext, List<CardDetail> mList, ProfileListener profileListener, SelectNewPaymentListener mListener, int type) {
        this.mContext = mContext;
        this.mList = mList;
        this.mCallBack = profileListener;
        this.mListener = mListener;
        this.type = type;
    }

    @Override
    public SelectDifferentPaymentAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        SelectDifferentPaymentItemBinding mBinding = SelectDifferentPaymentItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new SelectDifferentPaymentAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(SelectDifferentPaymentAdapter.ViewHolderItem holder, final int position) {
        final CardDetail cardDetail = mList.get(position);
        String photoUrl = cardDetail.getBrandImageUrl();
        if (!TextUtils.isEmpty(photoUrl)) {
            SvgLoader.pluck()
                    .with((HomeActivity) mContext)
                    .load(photoUrl, holder.itemView.cardImage);
        }
        String cardName = "<b>" + cardDetail.getBrand() + "</b> " + " ending in " + "<b>" + cardDetail.getLastFour() + "<b>";
        holder.itemView.cardHeading.setText(Html.fromHtml(cardName), TextView.BufferType.SPANNABLE);
        holder.itemView.name.setText(cardDetail.getNameOnCard());
        holder.itemView.expiryDate.setText("Expires " + cardDetail.getExpirationMonth() + "/" + cardDetail.getExpirationYear());
        if (type == Constants.SELECT) {
            holder.itemView.buttonText.setText("Select");
        } else {
            holder.itemView.buttonText.setText("Change");
        }
        holder.itemView.changePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.selectedPayment(cardDetail);
                mCallBack.onBackPressed();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<CardDetail> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {

        SelectDifferentPaymentItemBinding itemView;

        public ViewHolderItem(SelectDifferentPaymentItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
