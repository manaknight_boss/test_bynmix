package com.bynmix.app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.databinding.ExistingAddressItemBinding;
import com.bynmix.app.models.Address;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.interfaces.SelectExistingAddressListener;

import java.util.List;

public class SelectExistingAddressAdapter extends RecyclerView.Adapter<SelectExistingAddressAdapter.ViewHolderItem> {

    private final Context mContext;
    private final List<Address> mList;
    private int type;
    private final SelectExistingAddressListener mCallback;


    public SelectExistingAddressAdapter(Context mContext, List<Address> mList, int type, SelectExistingAddressListener mCallback) {
        this.mContext = mContext;
        this.mList = mList;
        this.type = type;
        this.mCallback = mCallback;
    }

    @Override
    public SelectExistingAddressAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        ExistingAddressItemBinding mBinding = ExistingAddressItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new SelectExistingAddressAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final SelectExistingAddressAdapter.ViewHolderItem holder, final int position) {
        final Address address = mList.get(position);
        holder.itemView.fullName.setText(address.getFullName());
        if (TextUtils.isEmpty(address.getAddressOne())) {
            holder.itemView.addressOne.setVisibility(View.VISIBLE);
            holder.itemView.addressOne.setText(address.getAddressOne());
        } else {
            holder.itemView.addressOne.setVisibility(View.GONE);
        }
        if (TextUtils.isEmpty(address.getAddressTwo())) {
            holder.itemView.addressTwo.setVisibility(View.VISIBLE);
            holder.itemView.addressTwo.setText(address.getAddressTwo());
        } else {
            holder.itemView.addressTwo.setVisibility(View.GONE);
        }
        holder.itemView.zipcode.setText(address.getCity() + " " + address.getStateAbbreviation() + ", " + address.getZipCode());

        if (address.isSelectedCheckBox()) {
            holder.itemView.checkedImage.setImageResource(R.mipmap.checkbox_checked);
        } else {
            holder.itemView.checkedImage.setImageResource(R.mipmap.checkbox);
        }
        holder.itemView.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetListSelection();
                if (type == Constants.SHIPPING_ADDRESS) {
                    address.setShippingAddress(true);
                } else {
                    address.setReturnAddress(true);
                }
                mCallback.selectedAddress(address);
                address.setSelectedCheckBox(true);
                notifyDataSetChanged();
            }
        });
    }

    private void resetListSelection() {
        for (Address address : mList) {
            address.setSelectedCheckBox(false);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ExistingAddressItemBinding itemView;

        public ViewHolderItem(ExistingAddressItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

