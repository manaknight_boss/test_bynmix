package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ahmadrosid.svgloader.SvgLoader;
import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.databinding.SelectNewPaymentItemBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.SelectNewPaymentListener;
import com.bynmix.app.models.CardDetail;

import java.util.List;

public class SelectNewPaymentAdapter extends RecyclerView.Adapter<SelectNewPaymentAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<CardDetail> mList;
    private ProfileListener mCallBack;
    private SelectNewPaymentListener mListener;

    public SelectNewPaymentAdapter(Context mContext, List<CardDetail> mList, ProfileListener profileListener, SelectNewPaymentListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.mCallBack = profileListener;
        this.mListener = mListener;
    }

    @Override
    public SelectNewPaymentAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        SelectNewPaymentItemBinding mBinding = SelectNewPaymentItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new SelectNewPaymentAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(SelectNewPaymentAdapter.ViewHolderItem holder, final int position) {
        final CardDetail cardDetail = mList.get(position);
        String photoUrl = cardDetail.getBrandImageUrl();
        if (!TextUtils.isEmpty(photoUrl)) {
            SvgLoader.pluck()
                    .with((HomeActivity) mContext)
                    .load(photoUrl, holder.itemView.cardImage);
        }
        String cardName = "<b>" + cardDetail.getBrand() + "</b> " + " ending in " + "<b>" + cardDetail.getLastFour() + "<b>";
        holder.itemView.cardHeading.setText(Html.fromHtml(cardName), TextView.BufferType.SPANNABLE);
        holder.itemView.name.setText(cardDetail.getNameOnCard());
        holder.itemView.expiryDate.setText("Expires " + cardDetail.getExpirationMonth() + "/" + cardDetail.getExpirationYear());
        if (cardDetail.isSelected()) {
            holder.itemView.check.setImageResource(R.mipmap.checkbox_checked);
        } else {
            holder.itemView.check.setImageResource(R.mipmap.checkbox);
        }
        holder.itemView.mainItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetSelection();
                cardDetail.setSelected(true);
                mListener.selectedId(cardDetail.getCardId());
                notifyDataSetChanged();
            }
        });
    }

    private void resetSelection() {
        for (CardDetail cardDetail : mList) {
            cardDetail.setSelected(false);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<CardDetail> list) {
        this.mList = list;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {

        SelectNewPaymentItemBinding itemView;

        public ViewHolderItem(SelectNewPaymentItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
