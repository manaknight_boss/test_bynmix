package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.databinding.SelectPhotoHeaderBinding;
import com.bynmix.app.databinding.SelectPhotoItemBinding;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.utils.Constants;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

public class SelectPhotoAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context mContext;
    private List<String> mPhotoList;
    final private int TYPE_ITEM = 1;
    final private int TYPE_FOOTER = 0;
    private CreatePostListener mCallback;

    public SelectPhotoAdapter(Context mContext, List<String> mPhotoList, CreatePostListener mCallback) {
        this.mPhotoList = mPhotoList;
        this.mContext = mContext;
        this.mCallback = mCallback;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            SelectPhotoItemBinding mBindingItem = SelectPhotoItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new SelectPhotoAdapter.ViewHolderItem(mBindingItem);
        } else {
            SelectPhotoHeaderBinding mBindingHeader = SelectPhotoHeaderBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new SelectPhotoAdapter.ViewHolderHeader(mBindingHeader);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SelectPhotoAdapter.ViewHolderHeader) {
            ViewHolderHeader((SelectPhotoAdapter.ViewHolderHeader) holder);
        } else if (holder instanceof SelectPhotoAdapter.ViewHolderItem) {
            viewHolderItem((SelectPhotoAdapter.ViewHolderItem) holder, position);
        }
    }


    private void ViewHolderHeader(final SelectPhotoAdapter.ViewHolderHeader holder) {
        if (mPhotoList.size() == Constants.MAX_LISTING_IMAGES) {
            holder.itemView.headerPhotoLayout.setVisibility(View.GONE);
        } else {
            holder.itemView.headerPhotoLayout.setVisibility(View.VISIBLE);
        }
        holder.itemView.headerPhotoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int imageCount = Constants.MAX_LISTING_IMAGES - mPhotoList.size();
                mCallback.openGalleryFor(Constants.CREATE_LISTING, imageCount);
            }
        });
    }

    private void viewHolderItem(SelectPhotoAdapter.ViewHolderItem holder, final int position) {
        final String photo = mPhotoList.get(position);
        File fLocal = new File(photo);
        try {
            Picasso.get().load(fLocal).into(holder.itemView.listingPhoto);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        holder.itemView.crossIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.updateListingImages(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mPhotoList.size() + 1;

    }

    public int getItemViewType(int position) {
        if (position == mPhotoList.size()) {
            return TYPE_FOOTER;
        }
        return TYPE_ITEM;
    }

    public void setList(List<String> photoList) {
        this.mPhotoList = photoList;
        notifyDataSetChanged();
    }


    class ViewHolderItem extends RecyclerView.ViewHolder {
        SelectPhotoItemBinding itemView;

        public ViewHolderItem(SelectPhotoItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderHeader extends RecyclerView.ViewHolder {
        SelectPhotoHeaderBinding itemView;

        private ViewHolderHeader(SelectPhotoHeaderBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}