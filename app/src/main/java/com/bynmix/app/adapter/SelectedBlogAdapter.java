package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.activities.onboarding.SelectBloggerActivity;
import com.bynmix.app.databinding.BlogSelectedItemBinding;
import com.bynmix.app.databinding.BrandItemBinding;
import com.bynmix.app.interfaces.BloggerListener;
import com.bynmix.app.models.UserResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SelectedBlogAdapter extends RecyclerView.Adapter<SelectedBlogAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<UserResponse> mSelectedList;
    private BloggerListener mCallBack;

    public SelectedBlogAdapter(List<UserResponse> mSelectedList, SelectBloggerActivity mContext, BloggerListener mCallBack) {
        this.mContext = mContext;
        this.mSelectedList = mSelectedList;
        this.mCallBack = mCallBack;

    }

    @Override
    public ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        BrandItemBinding mBinding = BrandItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(final SelectedBlogAdapter.ViewHolderItem holder, int position) {
        final UserResponse user = mSelectedList.get(position);
        String username = user.getUsername();
        if (!TextUtils.isEmpty(username)) {
            holder.itemView.name.setText(username);
        }
        holder.itemView.item.setBackgroundColor(mContext.getResources().getColor(R.color.purple));
        holder.itemView.name.setTextColor(mContext.getResources().getColor(R.color.white));
        holder.itemView.heart.setImageResource(R.mipmap.heart_pink_on_boarding);

        holder.itemView.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.unSelectBlogger(user);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSelectedList.size();
    }

    public void setList(List<UserResponse> mBloggerList) {
        this.mSelectedList = mBloggerList;
        notifyDataSetChanged();
    }


    public class ViewHolderItem extends RecyclerView.ViewHolder {
        BrandItemBinding itemView;

        public ViewHolderItem(BrandItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

