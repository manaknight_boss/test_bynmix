package com.bynmix.app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.databinding.SelectedFilterItemBinding;
import com.bynmix.app.interfaces.BaseFilterListener;
import com.bynmix.app.interfaces.FilterListener;
import com.bynmix.app.interfaces.FilterUserListener;
import com.bynmix.app.models.SelectedFilter;

import java.util.List;

public class SelectedFilterAdapter extends RecyclerView.Adapter<SelectedFilterAdapter.ViewHolderItem> {

    private final Context mContext;
    private final List<SelectedFilter> selectedFilterList;
    private final BaseFilterListener mCallback;

    public SelectedFilterAdapter(Context mContext, List<SelectedFilter> selectedFilterList, BaseFilterListener mCallback) {
        this.mContext = mContext;
        this.selectedFilterList = selectedFilterList;
        this.mCallback = mCallback;
    }

    @Override
    public SelectedFilterAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        SelectedFilterItemBinding mBinding = SelectedFilterItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new SelectedFilterAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final SelectedFilterAdapter.ViewHolderItem holder, final int position) {
        final SelectedFilter selectedFilter = selectedFilterList.get(position);
        holder.itemView.item.setText(selectedFilter.getName());
        holder.itemView.crossIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mCallback instanceof FilterListener) {
                    FilterListener filterListener = (FilterListener) mCallback;
                    filterListener.removeCheck(selectedFilter.getType(), selectedFilter.getId(), selectedFilter.getSubType());
                }else {
                    FilterUserListener filterUserListener = (FilterUserListener) mCallback;
                    filterUserListener.removeCheck(selectedFilter.getType(), selectedFilter.getId(), selectedFilter.getSubType());
                }
                selectedFilterList.remove(position);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return selectedFilterList.size();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        SelectedFilterItemBinding itemView;

        public ViewHolderItem(SelectedFilterItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

