package com.bynmix.app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.databinding.SelectExistingAddressItemBinding;
import com.bynmix.app.databinding.SellerAddressHeaderBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.SelectExistingAddressListener;
import com.bynmix.app.models.Address;

import java.util.List;

public class SellerAddressAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context mContext;
    final private int TYPE_ITEM = 1;
    final private int TYPE_HEADER = 0;
    private List<Address> mList;
    private ProfileListener mCallBack;
    private Address shippingAddress;
    private Address returnAddress;
    private SelectExistingAddressListener mListener;

    public SellerAddressAdapter(Context mContext, List<Address> mList, Address shippingAddress, Address returnAddress, ProfileListener profileListener, SelectExistingAddressListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.shippingAddress = shippingAddress;
        this.returnAddress = returnAddress;
        this.mCallBack = profileListener;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            SelectExistingAddressItemBinding itemViewItem = SelectExistingAddressItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new SellerAddressAdapter.ViewHolderItem(itemViewItem);
        } else {
            SellerAddressHeaderBinding itemViewHeader = SellerAddressHeaderBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new SellerAddressAdapter.ViewHolderHeader(itemViewHeader);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof SellerAddressAdapter.ViewHolderHeader) {
            viewHolderHeader((SellerAddressAdapter.ViewHolderHeader) holder);
        } else if (holder instanceof SellerAddressAdapter.ViewHolderItem) {
            position = position - 1;
            viewHolderItem((SellerAddressAdapter.ViewHolderItem) holder, position);
        }
    }

    private void viewHolderHeader(final SellerAddressAdapter.ViewHolderHeader holder) {
        if (shippingAddress != null) {
            holder.itemView.shippingAddress.mainLayout.setVisibility(View.VISIBLE);
            holder.itemView.shippingAddress.fullName.setText(shippingAddress.getFullName());
            if (!TextUtils.isEmpty(shippingAddress.getAddressOne())) {
                holder.itemView.shippingAddress.addressOne.setText(shippingAddress.getAddressOne());
            } else {
                holder.itemView.shippingAddress.addressOne.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(shippingAddress.getAddressTwo())) {
                holder.itemView.shippingAddress.addressTwo.setText(shippingAddress.getAddressTwo());
            } else {
                holder.itemView.shippingAddress.addressTwo.setVisibility(View.GONE);
            }
            holder.itemView.shippingAddress.zipcode.setText(shippingAddress.getCity() + " " + shippingAddress.getStateAbbreviation() + ", " + shippingAddress.getZipCode());

        } else {
            holder.itemView.shippingAddressLayout.setVisibility(View.GONE);
        }
        if (returnAddress != null) {
            holder.itemView.returnAddress.mainLayout.setVisibility(View.VISIBLE);
            holder.itemView.returnAddress.fullName.setText(returnAddress.getFullName());
            if (!TextUtils.isEmpty(returnAddress.getAddressOne())) {
                holder.itemView.returnAddress.addressOne.setText(returnAddress.getAddressOne());
            } else {
                holder.itemView.returnAddress.addressOne.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(returnAddress.getAddressTwo())) {
                holder.itemView.returnAddress.addressTwo.setText(returnAddress.getAddressTwo());
            } else {
                holder.itemView.returnAddress.addressTwo.setVisibility(View.GONE);
            }
            holder.itemView.returnAddress.zipcode.setText(returnAddress.getCity() + " " + returnAddress.getStateAbbreviation() + ", " + returnAddress.getZipCode());
        } else {
            holder.itemView.returnAddressLayout.setVisibility(View.GONE);

        }
        if (shippingAddress == null && returnAddress == null) {
            holder.itemView.otherAddressesText.setText(mContext.getString(R.string.addresses));
        } else {
            holder.itemView.otherAddressesText.setText(mContext.getString(R.string.other_addresses));
        }
        if (mList.size() == 0) {
            holder.itemView.otherAddressesText.setVisibility(View.GONE);
        } else {
            holder.itemView.otherAddressesText.setVisibility(View.VISIBLE);
        }
        holder.itemView.shippingAddress.selectAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.selectedAddress(shippingAddress);
                mCallBack.onBackPressed();
            }
        });

        holder.itemView.returnAddress.selectAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.selectedAddress(returnAddress);
                mCallBack.onBackPressed();
            }
        });

    }

    private void viewHolderItem(SellerAddressAdapter.ViewHolderItem holder, final int position) {
        final Address address = mList.get(position);
        holder.itemView.fullName.setText(address.getFullName());
        if (!TextUtils.isEmpty(address.getAddressOne())) {
            holder.itemView.addressOne.setVisibility(View.VISIBLE);
            holder.itemView.addressOne.setText(address.getAddressOne());
        } else {
            holder.itemView.addressOne.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(address.getAddressTwo())) {
            holder.itemView.addressTwo.setVisibility(View.VISIBLE);
            holder.itemView.addressTwo.setText(address.getAddressTwo());
        } else {
            holder.itemView.addressTwo.setVisibility(View.GONE);
        }
        holder.itemView.zipcode.setText(address.getCity() + " " + address.getStateAbbreviation() + ", " + address.getZipCode());

        holder.itemView.selectAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.selectedAddress(address);
                mCallBack.onBackPressed();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }

        return TYPE_ITEM;
    }


    private boolean isPositionHeader(int position) {
        return position == 0;
    }


    public void setList(List<Address> list, Address shippingAddress, Address returnAddress) {
        this.mList = list;
        this.shippingAddress = shippingAddress;
        this.returnAddress = returnAddress;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        SelectExistingAddressItemBinding itemView;

        public ViewHolderItem(SelectExistingAddressItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderHeader extends RecyclerView.ViewHolder {
        SellerAddressHeaderBinding itemView;

        private ViewHolderHeader(SellerAddressHeaderBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}