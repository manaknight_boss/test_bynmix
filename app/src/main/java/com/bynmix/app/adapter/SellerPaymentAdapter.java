package com.bynmix.app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ahmadrosid.svgloader.SvgLoader;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.databinding.SelectExistingPaymentItemBinding;
import com.bynmix.app.databinding.SellerPaymentHeaderBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.SelectNewPaymentListener;
import com.bynmix.app.models.CardDetail;

import java.util.List;

public class SellerPaymentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final Context mContext;
    final private int TYPE_ITEM = 1;
    final private int TYPE_HEADER = 0;
    private List<CardDetail> mList;
    private ProfileListener mCallBack;
    private CardDetail selectedPayment;
    private SelectNewPaymentListener mListener;

    public SellerPaymentAdapter(Context mContext, List<CardDetail> mList, CardDetail selectedPayment, ProfileListener profileListener, SelectNewPaymentListener mListener) {
        this.mContext = mContext;
        this.mList = mList;
        this.selectedPayment = selectedPayment;
        this.mCallBack = profileListener;
        this.mListener = mListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            SelectExistingPaymentItemBinding itemViewItem = SelectExistingPaymentItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new SellerPaymentAdapter.ViewHolderItem(itemViewItem);
        } else {
            SellerPaymentHeaderBinding itemViewHeader = SellerPaymentHeaderBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new SellerPaymentAdapter.ViewHolderHeader(itemViewHeader);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (holder instanceof SellerPaymentAdapter.ViewHolderHeader) {
            viewHolderHeader((SellerPaymentAdapter.ViewHolderHeader) holder);
        } else if (holder instanceof SellerPaymentAdapter.ViewHolderItem) {
            position = position - 1;
            viewHolderItem((SellerPaymentAdapter.ViewHolderItem) holder, position);
        }
    }

    private void viewHolderHeader(final SellerPaymentAdapter.ViewHolderHeader holder) {
        if (selectedPayment != null) {
            holder.itemView.changePayment.mainLayout.setVisibility(View.VISIBLE);
            String photoUrl = selectedPayment.getBrandImageUrl();
            if (!TextUtils.isEmpty(photoUrl)) {
                SvgLoader.pluck()
                        .with((HomeActivity) mContext)
                        .load(photoUrl, holder.itemView.changePayment.cardImage);
            }
            String cardName = "<b>" + selectedPayment.getBrand() + "</b> " + " ending in " + "<b>" + selectedPayment.getLastFour() + "<b>";
            holder.itemView.changePayment.cardHeading.setText(Html.fromHtml(cardName), TextView.BufferType.SPANNABLE);
            holder.itemView.changePayment.name.setText(selectedPayment.getNameOnCard());
            holder.itemView.changePayment.expiryDate.setText("Expires " + selectedPayment.getExpirationMonth() + "/" + selectedPayment.getExpirationYear());
            holder.itemView.changePayment.selectPayment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mListener.selectedPayment(selectedPayment);
                    mCallBack.onBackPressed();
                }
            });

        } else {
            holder.itemView.changePayment.mainLayout.setVisibility(View.GONE);
        }

        if (mList.size() == 0) {
            holder.itemView.otherPaymentText.setVisibility(View.GONE);
        } else {
            holder.itemView.otherPaymentText.setVisibility(View.VISIBLE);
        }

    }

    private void viewHolderItem(SellerPaymentAdapter.ViewHolderItem holder, final int position) {
        final CardDetail cardDetail = mList.get(position);
        String photoUrl = cardDetail.getBrandImageUrl();
        if (!TextUtils.isEmpty(photoUrl)) {
            SvgLoader.pluck()
                    .with((HomeActivity) mContext)
                    .load(photoUrl, holder.itemView.cardImage);
        }
        String cardName = "<b>" + cardDetail.getBrand() + "</b> " + " ending in " + "<b>" + cardDetail.getLastFour() + "<b>";
        holder.itemView.cardHeading.setText(Html.fromHtml(cardName), TextView.BufferType.SPANNABLE);
        holder.itemView.name.setText(cardDetail.getNameOnCard());
        holder.itemView.expiryDate.setText("Expires " + cardDetail.getExpirationMonth() + "/" + cardDetail.getExpirationYear());
        holder.itemView.selectPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.selectedPayment(cardDetail);
                mCallBack.onBackPressed();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position)) {
            return TYPE_HEADER;
        }

        return TYPE_ITEM;
    }


    private boolean isPositionHeader(int position) {
        return position == 0;
    }


    public void setList(List<CardDetail> list, CardDetail selectedPayment) {
        this.mList = list;
        this.selectedPayment = selectedPayment;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        SelectExistingPaymentItemBinding itemView;

        public ViewHolderItem(SelectExistingPaymentItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class ViewHolderHeader extends RecyclerView.ViewHolder {
        SellerPaymentHeaderBinding itemView;

        private ViewHolderHeader(SellerPaymentHeaderBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}