package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.databinding.ItemSellerPolicyBinding;
import com.bynmix.app.models.Sections;

import java.util.List;

public class SellerPolicyAdapter extends RecyclerView.Adapter<SellerPolicyAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<Sections> mList;

    public SellerPolicyAdapter(Context mContext, List<Sections> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public SellerPolicyAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemSellerPolicyBinding mBinding = ItemSellerPolicyBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new SellerPolicyAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(SellerPolicyAdapter.ViewHolderItem holder, int position) {
        Sections sections = mList.get(position);
        holder.itemView.title.setText(sections.getTitle());
        if (!TextUtils.isEmpty(sections.getDescription())) {
            holder.itemView.description.setVisibility(View.VISIBLE);
            holder.itemView.description.setText(Html.fromHtml(sections.getDescription()), TextView.BufferType.SPANNABLE);
        } else {
            holder.itemView.description.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<Sections> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        ItemSellerPolicyBinding itemView;

        public ViewHolderItem(ItemSellerPolicyBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

