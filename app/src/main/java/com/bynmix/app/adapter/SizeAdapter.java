package com.bynmix.app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.bynmix.app.databinding.SizeFragmentItemBinding;
import com.bynmix.app.interfaces.SizeListener;
import com.bynmix.app.models.Size;
import com.bynmix.app.models.SizeType;

import java.util.ArrayList;
import java.util.List;

public class SizeAdapter extends RecyclerView.Adapter<SizeAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<SizeType> mSizeList;
    private final SizeListener mCallback;

    public SizeAdapter(Context mContext, List<SizeType> mSizeList, SizeListener mCallback) {
        this.mContext = mContext;
        this.mSizeList = mSizeList;
        this.mCallback = mCallback;

    }

    @Override
    public SizeAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        SizeFragmentItemBinding mBinding = SizeFragmentItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new SizeAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final SizeAdapter.ViewHolderItem holder, final int position) {
        SizeType sizeType = mSizeList.get(position);
        holder.itemView.sizeText.setText(sizeType.getSizeTypeName());
        List<List<Size>> sizeLists = new ArrayList<>();
        List<Size> sizes = sizeType.getSizes();
        List<Size> sizeData = null;
        int lastDisplayGroup = 0;
        for (Size size : sizes) {
            int currentDisplayGroup = Integer.parseInt(size.getDisplayGroup());
            if (currentDisplayGroup != lastDisplayGroup) {
                if (sizeData != null) {
                    sizeLists.add(sizeData);
                }
                sizeData = new ArrayList<>();
                lastDisplayGroup = currentDisplayGroup;
            }
            sizeData.add(size);
        }
        sizeLists.add(sizeData);

        SizeRecyclerAdapter adapter = new SizeRecyclerAdapter(mContext, sizeType, sizeLists, mCallback);
        holder.itemView.sizeRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        holder.itemView.sizeRecyclerView.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return mSizeList.size();
    }

    public void setList(List<SizeType> sizeTypeList) {
        this.mSizeList = sizeTypeList;
        notifyDataSetChanged();
    }

    public void resetSizeSelection() {
        for(SizeType sizeType:mSizeList){
            List<Size> sizes = sizeType.getSizes();
            for(Size size:sizes){
                size.setSelected(false);
            }
        }
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        SizeFragmentItemBinding itemView;

        public ViewHolderItem(SizeFragmentItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
