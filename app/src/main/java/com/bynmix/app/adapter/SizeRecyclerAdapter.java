package com.bynmix.app.adapter;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bynmix.app.databinding.SizeFragmentRecyclerBinding;
import com.bynmix.app.interfaces.SizeListener;
import com.bynmix.app.models.Size;
import com.bynmix.app.models.SizeType;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.List;

public class SizeRecyclerAdapter extends RecyclerView.Adapter<SizeRecyclerAdapter.ViewHolderItem> {

    private final Context mContext;
    private  List<List<Size>> mSizeList;
    private SizeType sizeType;
    private final SizeListener mCallback;

    public SizeRecyclerAdapter(Context mContext,SizeType sizeType, List<List<Size>> mSizeList, SizeListener mCallback) {
        this.mContext = mContext;
        this.sizeType = sizeType;
        this.mSizeList = mSizeList;
        this.mCallback = mCallback;

    }

    @Override
    public SizeRecyclerAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        SizeFragmentRecyclerBinding mBinding = SizeFragmentRecyclerBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new SizeRecyclerAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final SizeRecyclerAdapter.ViewHolderItem holder, final int position) {
        List<Size> sizes = mSizeList.get(position);
        SizeValueAdapter adapter = new SizeValueAdapter(mContext, sizeType, sizes, mCallback);
        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        holder.itemView.recyclerView.setLayoutManager(flowLayoutManager);
        holder.itemView.recyclerView.setAdapter(adapter);
        holder.itemView.recyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.set(0, 0, 15, 10);

            }
        });
    }

    @Override
    public int getItemCount() {
        return mSizeList.size();
    }


    class ViewHolderItem extends RecyclerView.ViewHolder {
        SizeFragmentRecyclerBinding itemView;

        public ViewHolderItem(SizeFragmentRecyclerBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
