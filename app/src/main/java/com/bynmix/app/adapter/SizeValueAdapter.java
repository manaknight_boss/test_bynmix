package com.bynmix.app.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.databinding.SizeFragmentRecyclerItemBinding;
import com.bynmix.app.interfaces.SizeListener;
import com.bynmix.app.models.Size;
import com.bynmix.app.models.SizeType;

import java.util.List;

public class SizeValueAdapter extends RecyclerView.Adapter<SizeValueAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<Size> mSizeList;
    private SizeType sizeType;
    private final SizeListener mCallback;

    public SizeValueAdapter(Context mContext, SizeType sizeType, List<Size> mSizeList, SizeListener mCallback) {
        this.mContext = mContext;
        this.sizeType = sizeType;
        this.mSizeList = mSizeList;
        this.mCallback = mCallback;

    }

    @Override
    public ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        SizeFragmentRecyclerItemBinding mBinding = SizeFragmentRecyclerItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderItem holder, final int position) {
        final Size size = mSizeList.get(position);
        holder.itemView.sizeText.setText(size.getSizeName());
        Typeface typeface;
        if (size.isSelected()) {
            holder.itemView.item.setBackground(mContext.getResources().getDrawable(R.drawable.purple_out_line_rectangle_background));
            typeface = ResourcesCompat.getFont(mContext, R.font.raleway_bold);
        } else {
            typeface = ResourcesCompat.getFont(mContext, R.font.raleway_regular);
            holder.itemView.item.setBackground(mContext.getResources().getDrawable(R.drawable.color_layout_background));
        }
        holder.itemView.sizeText.setTypeface(typeface);
        holder.itemView.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mCallback.resetSizeSelection();
                size.setSelected(true);
                mCallback.selectedSize(sizeType, size);
//                notifyDataSetChanged();
            }
        });

    }


    @Override
    public int getItemCount() {
        return mSizeList.size();
    }


    class ViewHolderItem extends RecyclerView.ViewHolder {
        SizeFragmentRecyclerItemBinding itemView;

        public ViewHolderItem(SizeFragmentRecyclerItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
