package com.bynmix.app.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bynmix.app.R;
import com.bynmix.app.databinding.BrandItemBinding;
import com.bynmix.app.interfaces.TagListener;
import com.bynmix.app.models.Descriptor;

import java.util.List;


public class TagsAdapter extends RecyclerView.Adapter<TagsAdapter.ViewHolderItem> {

    private Context mContext;
    private List<Descriptor> mTagsList;
    private TagListener tagListener;

    public TagsAdapter(Context mContext, List<Descriptor> mTagsList, TagListener tagListener) {
        this.mContext = mContext;
        this.mTagsList = mTagsList;
        this.tagListener = tagListener;
    }

    @NonNull
    @Override
    public TagsAdapter.ViewHolderItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        BrandItemBinding mBinding = BrandItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new TagsAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull TagsAdapter.ViewHolderItem holder, int position) {
        final Descriptor tag = mTagsList.get(position);
        String name = tag.getDescriptorName();
        if (!TextUtils.isEmpty(name)) {
            holder.itemView.name.setText(name);
        }
        updateUI(holder, tag.isSelected());
        holder.itemView.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tagListener.onClickTag(tag);
            }
        });
    }

    private void updateUI(ViewHolderItem holder, boolean isSelected) {
        if (isSelected) {
            holder.itemView.item.setBackgroundColor(mContext.getResources().getColor(R.color.purple));
            holder.itemView.name.setTextColor(mContext.getResources().getColor(R.color.white));
            holder.itemView.heart.setImageResource(R.mipmap.heart_pink_on_boarding);
        } else {
            holder.itemView.item.setBackgroundColor(mContext.getResources().getColor(R.color.on_boarding_item_background));
            holder.itemView.name.setTextColor(mContext.getResources().getColor(R.color.purple));
            holder.itemView.heart.setImageResource(R.mipmap.heart_on_boarding);
        }
    }

    @Override
    public int getItemCount() {
        return mTagsList.size();
    }

    public void setList(List<Descriptor> mTagList) {
        this.mTagsList = mTagList;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        BrandItemBinding itemView;

        public ViewHolderItem(BrandItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

