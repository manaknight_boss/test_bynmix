package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.bynmix.app.databinding.TermsOfServiceItemBinding;
import com.bynmix.app.databinding.TextViewBinding;
import com.bynmix.app.models.Sections;
import com.bynmix.app.utils.Utils;

import java.util.List;

public class TermsOfServicesAdapter extends RecyclerView.Adapter<TermsOfServicesAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<Sections> mList;

    public TermsOfServicesAdapter(Context mContext, List<Sections> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public TermsOfServicesAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        TermsOfServiceItemBinding mBinding = TermsOfServiceItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new TermsOfServicesAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(TermsOfServicesAdapter.ViewHolderItem holder, int position) {
        Sections sections = mList.get(position);
        holder.itemView.title.setText(sections.getOrder() + ". " + sections.getTitle());
        holder.itemView.subDescription.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(sections.getDescription())) {
            holder.itemView.description.setVisibility(View.VISIBLE);
            holder.itemView.description.setText(sections.getDescription());
        } else {
            holder.itemView.description.setVisibility(View.GONE);
        }
        if (sections.getSubSection() == null) {
            holder.itemView.points.setVisibility(View.GONE);
        } else {
            holder.itemView.points.setVisibility(View.VISIBLE);
            List<Sections> subSections = sections.getSubSection();
            if(subSections != null && subSections.size() > 0) {
                String description = subSections.get(0).getDescription();
                if(!TextUtils.isEmpty(description)) {
                    holder.itemView.description.setVisibility(View.VISIBLE);
                    holder.itemView.description.setText(description);
                } else {
                    holder.itemView.points.setVisibility(View.GONE);
                }
                if(subSections.size() > 1) {
                    holder.itemView.subDescription.setVisibility(View.VISIBLE);
                    holder.itemView.subDescription.setText(subSections.get(1).getDescription());
                } else {
                    holder.itemView.subDescription.setVisibility(View.GONE);
                }

            }
            for (int i = 0; i < subSections.size(); i++) {
                if (subSections.get(i).getPoints() != null && subSections.get(i).getPoints().size() > 0) {
                    for (int j = 0; j < subSections.get(i).getPoints().size(); j++) {
                        TextViewBinding mBinding = TextViewBinding.inflate(LayoutInflater.from(mContext));
                        mBinding.pointText.setText("- " + sections.getSubSection().get(i).getPoints().get(j));
                        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,ViewGroup.LayoutParams.WRAP_CONTENT);
                        int marginSmall = Utils.dpToPx(mContext,4);
                        int marginLarge = Utils.dpToPx(mContext,8);
                        layoutParams.setMargins(marginSmall,marginSmall,marginSmall,marginLarge);
                        mBinding.pointText.setLayoutParams(layoutParams);
                        holder.itemView.points.addView(mBinding.pointText);
                    }
                }


            }
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void setList(List<Sections> mList) {
        this.mList.clear();
        this.mList = mList;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        TermsOfServiceItemBinding itemView;

        public ViewHolderItem(TermsOfServiceItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

