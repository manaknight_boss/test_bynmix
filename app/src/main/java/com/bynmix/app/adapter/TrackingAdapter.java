package com.bynmix.app.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bynmix.app.databinding.TrackingInfoItemBinding;
import com.bynmix.app.models.ShippingHistory;

import java.util.List;

public class TrackingAdapter extends RecyclerView.Adapter<TrackingAdapter.ViewHolderItem> {

    private final Context mContext;
    private final List<ShippingHistory> mList;

    public TrackingAdapter(Context mContext, List<ShippingHistory> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @Override
    public TrackingAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        TrackingInfoItemBinding mBinding = TrackingInfoItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new TrackingAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(TrackingAdapter.ViewHolderItem holder, int position) {
        ShippingHistory shippingHistory = mList.get(position);
        holder.itemView.textView.setText(shippingHistory.getDisplayStatus());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        TrackingInfoItemBinding itemView;

        public ViewHolderItem(TrackingInfoItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

