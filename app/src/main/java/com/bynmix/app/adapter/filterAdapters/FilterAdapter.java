package com.bynmix.app.adapter.filterAdapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FilterMainItemBinding;
import com.bynmix.app.interfaces.BaseFilterListener;
import com.bynmix.app.interfaces.FilterListener;
import com.bynmix.app.interfaces.FilterUserListener;
import com.bynmix.app.models.FilterList;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;

import java.util.List;

public class FilterAdapter extends RecyclerView.Adapter<FilterAdapter.ViewHolderItem> {

    private final Context mContext;
    private final List<FilterList> filterList;
    private final BaseFilterListener mCallback;

    public FilterAdapter(Context mContext, List<FilterList> filterList, BaseFilterListener mCallback) {
        this.mContext = mContext;
        this.filterList = filterList;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public FilterAdapter.ViewHolderItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        FilterMainItemBinding mBinding = FilterMainItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new FilterAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final FilterAdapter.ViewHolderItem holder, final int position) {

        final FilterList filter = filterList.get(position);
        String filterName = filter.getFilterName();
        holder.itemView.mainItem.setText(filterName);
        Typeface typeface;
        if (filter.isSelected()) {
            filterName = Utils.getColorText(filterName, mContext.getResources().getColor(R.color.light_purple3));
            typeface = ResourcesCompat.getFont(mContext, R.font.raleway_extra_bold);
        } else {
            filterName = Utils.getColorText(filterName, mContext.getResources().getColor(R.color.white));
            typeface = ResourcesCompat.getFont(mContext, R.font.raleway_regular);
        }
        holder.itemView.mainItem.setText(Html.fromHtml(filterName), TextView.BufferType.SPANNABLE);
        holder.itemView.mainItem.setTypeface(typeface);
        if (filter.isItemSelected()) {
            holder.itemView.check.setVisibility(View.VISIBLE);
        } else {
            holder.itemView.check.setVisibility(View.GONE);
        }


        holder.itemView.filterMainLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                resetListSelection();

                if (mCallback instanceof FilterListener) {
                    FilterListener filterListener = (FilterListener) mCallback;
                    switch (position) {
                        case Constants.CONDITION_FILTER:
                            filter.setSelected(true);
                            filterListener.setConditionView();
                            break;
                        case Constants.SIZE_FILTER:
                            filter.setSelected(true);
                            filterListener.setSizeView();
                            break;
                        case Constants.COLOR_FILTER:
                            filter.setSelected(true);
                            filterListener.setColorView();
                            break;
                        case Constants.CATEGORY_FILTER:
                            filter.setSelected(true);
                            filterListener.setCategoryView();
                            break;
                        case Constants.PRICE_FILTER:
                            filter.setSelected(true);
                            filterListener.setPriceView();
                            break;
                        case Constants.BRAND_FILTER:
                            filter.setSelected(true);
                            filterListener.setBrandView();
                            break;
                    }
                } else {
                    FilterUserListener filterUserListener = (FilterUserListener) mCallback;

                    switch (position) {
                        case Constants.USER_STYLE_FILTER:
                            filter.setSelected(true);
                            filterUserListener.setStylesView();
                            break;
                        case Constants.USER_BODY_TYPE_FILTER:
                            filter.setSelected(true);
                            filterUserListener.setBodyTypeView();
                            break;
                        case Constants.USER_BRAND_FILTER:
                            filter.setSelected(true);
                            filterUserListener.setBrandView();
                            break;
                        case Constants.HEIGHT_FILTER:
                            filter.setSelected(true);
                            filterUserListener.setHeightView();
                            break;
                    }
                }
                notifyDataSetChanged();
            }
        });
    }

    private void resetListSelection() {
        for (FilterList filter : filterList) {
            filter.setSelected(false);
        }
    }

    @Override
    public int getItemCount() {
        return filterList.size();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        FilterMainItemBinding itemView;

        public ViewHolderItem(FilterMainItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}
