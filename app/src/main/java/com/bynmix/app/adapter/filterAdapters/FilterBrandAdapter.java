package com.bynmix.app.adapter.filterAdapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FilterBrandItemBinding;
import com.bynmix.app.databinding.FilterBrandItemHeaderBinding;
import com.bynmix.app.interfaces.BaseFilterListener;
import com.bynmix.app.interfaces.FilterListener;
import com.bynmix.app.interfaces.FilterUserListener;
import com.bynmix.app.models.Brand;
import com.bynmix.app.models.Category;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class FilterBrandAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private List<Brand> mBrandList;
    final private int TYPE_ITEM = 1;
    final private int TYPE_HEADER = 0;
    private BaseFilterListener mCallback;
    private List<Brand> brandList;

    public FilterBrandAdapter(Context mContext, List<Brand> mBrandList, BaseFilterListener mCallback) {
        this.mBrandList = mBrandList;
        this.brandList = mBrandList;
        this.mContext = mContext;
        this.mCallback = mCallback;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            FilterBrandItemBinding mBindingItem = FilterBrandItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new FilterBrandAdapter.ViewHolderItem(mBindingItem);
        } else {
            FilterBrandItemHeaderBinding mBindingHeader = FilterBrandItemHeaderBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new FilterBrandAdapter.viewHolderHeader(mBindingHeader);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof FilterBrandAdapter.viewHolderHeader) {
            viewHolderHeader((FilterBrandAdapter.viewHolderHeader) holder);
        } else if (holder instanceof FilterBrandAdapter.ViewHolderItem) {
            viewHolderItem((FilterBrandAdapter.ViewHolderItem) holder, position - 1);
        }
    }


    private void viewHolderHeader(final FilterBrandAdapter.viewHolderHeader holder) {
        holder.itemView.autoComplete.requestFocus();
        holder.itemView.autoComplete.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable editable) {
                // TODO Auto-generated method stub

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newText = s.toString();
                if (newText.length() > 0) {
                    List<Brand> searchedList = new ArrayList<>();
                    for (int i = 0; i < mBrandList.size(); i++) {
                        if ((Pattern.compile(Pattern.quote(newText), Pattern.CASE_INSENSITIVE).matcher(mBrandList.get(i).getBrandName()).find())) {
                            searchedList.add(mBrandList.get(i));
                        }
                    }
                    setList(searchedList);
                } else {
                    setList(brandList);
                }
            }

        });
    }

    private void viewHolderItem(FilterBrandAdapter.ViewHolderItem holder, final int position) {

        final Brand brand = mBrandList.get(position);
        String brandName = brand.getBrandName();
        Typeface typeface;
        if (brand.isSelected()) {
            brandName =  Utils.getColorText(brandName, mContext.getResources().getColor(R.color.purple));
            typeface = ResourcesCompat.getFont(mContext, R.font.raleway_semi_bold);
        } else {
            if (brand.getBrandId() == 0) {
                brandName =  Utils.getColorText(brandName, mContext.getResources().getColor(R.color.show_all_text));
                typeface = ResourcesCompat.getFont(mContext, R.font.raleway_semi_bold);
            } else {
                brandName =  Utils.getColorText(brandName, mContext.getResources().getColor(R.color.colorPrimary));
                typeface = ResourcesCompat.getFont(mContext, R.font.raleway_regular);
            }
        }
        holder.itemView.item.setText(Html.fromHtml(brandName), TextView.BufferType.SPANNABLE);
        holder.itemView.item.setTypeface(typeface);
        
        holder.itemView.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                brand.setSelected(true);
                if (mCallback instanceof FilterListener) {
                    FilterListener filterListener = (FilterListener) mCallback;
                    filterListener.setSelectedView(brand.getBrandId(), brand.getBrandName(), Constants.BRAND_FILTER, 0, new ArrayList<Category>(), new ArrayList<Integer>());
                } else {
                    FilterUserListener filterUserListener = (FilterUserListener) mCallback;
                    filterUserListener.setSelectedView(brand.getBrandId(), brand.getBrandName(), Constants.USER_BRAND_FILTER, 0, new ArrayList<Category>(), new ArrayList<Integer>());
                }
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mBrandList.size() + 1;

    }

    public void reset(int id, boolean isSet) {
        for (Brand brand : mBrandList) {
            if (brand.getBrandId() == id) {
                brand.setSelected(isSet);
                notifyDataSetChanged();
            }
        }
    }

    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        }

        return TYPE_ITEM;
    }

    public void setList(List<Brand> brandList) {
        this.mBrandList = brandList;
        notifyDataSetChanged();
    }

    public void setAdapterList(List<Brand> brandList) {
        this.brandList = brandList;
    }


    class ViewHolderItem extends RecyclerView.ViewHolder {
        FilterBrandItemBinding itemView;

        public ViewHolderItem(FilterBrandItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class viewHolderHeader extends RecyclerView.ViewHolder {
        FilterBrandItemHeaderBinding itemView;

        private viewHolderHeader(FilterBrandItemHeaderBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}