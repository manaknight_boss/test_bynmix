package com.bynmix.app.adapter.filterAdapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FilterBrandItemBinding;
import com.bynmix.app.interfaces.FilterListener;
import com.bynmix.app.models.Category;
import com.bynmix.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class FilterCategoryAdapter extends RecyclerView.Adapter<FilterCategoryAdapter.ViewHolderItem> {

    private final Context mContext;
    private final List<Category> categoryList;
    private final FilterListener mCallback;
    private int subTypeId;

    public FilterCategoryAdapter(Context mContext, List<Category> categoryList, int subTypeId, FilterListener mCallback) {
        this.mContext = mContext;
        this.categoryList = categoryList;
        this.mCallback = mCallback;
        this.subTypeId = subTypeId;
    }

    @Override
    public FilterCategoryAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        FilterBrandItemBinding mBinding = FilterBrandItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new FilterCategoryAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final FilterCategoryAdapter.ViewHolderItem holder, final int position) {

        final Category category = categoryList.get(position);
        String categoryText = category.getCategoryName();
        holder.itemView.item.setText(categoryText);
        if (category.getCategoryId() == 0) {
            categoryText = "<b>" + categoryText + "</b> ";
            holder.itemView.item.setText(Html.fromHtml(categoryText), TextView.BufferType.SPANNABLE);
        }
        if (category.isCategorySelected()) {
            categoryText = "<b>" + "<font color='" + mContext.getResources().getColor(R.color.purple) + "'>" + categoryText + "</font>" + "</b> ";
            holder.itemView.item.setText(Html.fromHtml(categoryText), TextView.BufferType.SPANNABLE);
        } else {
            categoryText = "<font color='" + mContext.getResources().getColor(R.color.colorPrimary) + "'>" + categoryText + "</font>";
            holder.itemView.item.setText(Html.fromHtml(categoryText), TextView.BufferType.SPANNABLE);
        }

        holder.itemView.mainLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                category.setCategorySelected(true);
                List<Integer> categoriesIds = new ArrayList<>();
                if (category.getCategoryId() == 0) {
                    for (int i = 1; i < categoryList.size(); i++) {
                        categoriesIds.add(categoryList.get(i).getCategoryId());
                    }
                }
                mCallback.setSelectedView(category.getCategoryId(), category.getCategoryName(), Constants.CATEGORY_FILTER, subTypeId, categoryList, categoriesIds);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public void reset(int id, int subType, boolean isSet) {
        for (Category category : categoryList) {
            if (subTypeId == subType) {
                if (category.getCategoryId() == id) {
                    category.setCategorySelected(isSet);
                    notifyDataSetChanged();
                }
            }
        }
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        FilterBrandItemBinding itemView;

        public ViewHolderItem(FilterBrandItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

