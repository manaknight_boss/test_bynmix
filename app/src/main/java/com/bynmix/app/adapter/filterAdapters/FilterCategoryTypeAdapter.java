package com.bynmix.app.adapter.filterAdapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FilterSizeTypeItemBinding;
import com.bynmix.app.interfaces.FilterListener;
import com.bynmix.app.models.CategoryType;

import java.util.List;

public class FilterCategoryTypeAdapter extends RecyclerView.Adapter<FilterCategoryTypeAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<CategoryType> categoryTypeList;
    private final FilterListener mCallback;

    public FilterCategoryTypeAdapter(Context mContext, List<CategoryType> categoryTypeList, FilterListener mCallback) {
        this.mContext = mContext;
        this.categoryTypeList = categoryTypeList;
        this.mCallback = mCallback;
    }

    @Override
    public FilterCategoryTypeAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        FilterSizeTypeItemBinding mBinding = FilterSizeTypeItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new FilterCategoryTypeAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final FilterCategoryTypeAdapter.ViewHolderItem holder, final int position) {

        final CategoryType categoryType = categoryTypeList.get(position);
        String category = categoryType.getCategoryTypeName();
        if (categoryType.isSelected()) {
            category = "<b>" + "<font color='" + mContext.getResources().getColor(R.color.purple) + "'>" + category + "</font>" + "</b> ";
            holder.itemView.item.setText(Html.fromHtml(category), TextView.BufferType.SPANNABLE);
        } else {
            category = "<font color='" + mContext.getResources().getColor(R.color.colorPrimary) + "'>" + category + "</font>";
            holder.itemView.item.setText(Html.fromHtml(category), TextView.BufferType.SPANNABLE);
        }
        holder.itemView.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetListSelection();
                categoryType.setSelected(true);
                mCallback.openCategoryView(categoryType);
                notifyDataSetChanged();
            }
        });

    }

    private void resetListSelection() {
        for (CategoryType categoryType : categoryTypeList) {
            categoryType.setSelected(false);
        }
    }

    @Override
    public int getItemCount() {
        return categoryTypeList.size();
    }

    public void setList(List<CategoryType> mCategoryTypeList) {
        this.categoryTypeList = mCategoryTypeList;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        FilterSizeTypeItemBinding itemView;

        public ViewHolderItem(FilterSizeTypeItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

