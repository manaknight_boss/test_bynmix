package com.bynmix.app.adapter.filterAdapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FilterColorItemBinding;
import com.bynmix.app.interfaces.FilterListener;
import com.bynmix.app.models.Category;
import com.bynmix.app.models.Colors;
import com.bynmix.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class FilterColorAdapter extends RecyclerView.Adapter<FilterColorAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<Colors> colorList;
    private final FilterListener mCallback;

    public FilterColorAdapter(Context mContext, List<Colors> colorList, FilterListener mCallback) {
        this.mContext = mContext;
        this.colorList = colorList;
        this.mCallback = mCallback;
    }

    @Override
    public FilterColorAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        FilterColorItemBinding mBinding = FilterColorItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new FilterColorAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final FilterColorAdapter.ViewHolderItem holder, final int position) {

        final Colors colors = colorList.get(position);
        String colorName = colors.getColorName();
        GradientDrawable drawable = (GradientDrawable) holder.itemView.color.getBackground();
        drawable.setColor(Color.parseColor("#" + colors.getHexCode()));
        if (colors.isSelectedColor()) {
            colorName = "<b>" + "<font color='" + mContext.getResources().getColor(R.color.purple) + "'>" + colorName + "</font>" + "</b> ";
            holder.itemView.colorName.setText(Html.fromHtml(colorName), TextView.BufferType.SPANNABLE);
        } else {
            colorName = "<font color='" + mContext.getResources().getColor(R.color.colorPrimary) + "'>" + colorName + "</font>";
            holder.itemView.colorName.setText(Html.fromHtml(colorName), TextView.BufferType.SPANNABLE);
        }
        holder.itemView.colorLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                colors.setSelectedColor(true);
                mCallback.setSelectedView(colors.getColorId(), colors.getColorName(), Constants.COLOR_FILTER, 0, new ArrayList<Category>(), new ArrayList<Integer>());
                notifyDataSetChanged();
            }
        });

    }

    @Override
    public int getItemCount() {
        return colorList.size();
    }

    public void setList(List<Colors> mColorList) {
        this.colorList = mColorList;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        FilterColorItemBinding itemView;

        public ViewHolderItem(FilterColorItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

