package com.bynmix.app.adapter.filterAdapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FilterBrandItemBinding;
import com.bynmix.app.interfaces.FilterListener;
import com.bynmix.app.models.Category;
import com.bynmix.app.models.Price;
import com.bynmix.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class FilterPriceAdapter extends RecyclerView.Adapter<FilterPriceAdapter.ViewHolderItem> {

    private final Context mContext;
    private final List<Price> priceList;
    private final FilterListener mCallback;

    public FilterPriceAdapter(Context mContext, List<Price> priceList, FilterListener mCallback) {
        this.mContext = mContext;
        this.priceList = priceList;
        this.mCallback = mCallback;
    }

    @Override
    public FilterPriceAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        FilterBrandItemBinding mBinding = FilterBrandItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new FilterPriceAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final FilterPriceAdapter.ViewHolderItem holder, final int position) {

        final Price price = priceList.get(position);
        String priceText = price.getPrice();
        holder.itemView.item.setText(priceText);
        if (price.isPriceSelected()) {
            priceText = "<b>" + "<font color='" + mContext.getResources().getColor(R.color.purple) + "'>" + priceText + "</font>" + "</b> ";
            holder.itemView.item.setText(Html.fromHtml(priceText), TextView.BufferType.SPANNABLE);
        } else {
            priceText = "<font color='" + mContext.getResources().getColor(R.color.colorPrimary) + "'>" + priceText + "</font>";
            holder.itemView.item.setText(Html.fromHtml(priceText), TextView.BufferType.SPANNABLE);
        }

        holder.itemView.mainLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                resetListSelection();
                price.setPriceSelected(true);
                mCallback.setSelectedView(price.getId(), price.getPrice(), Constants.PRICE_FILTER, 0, new ArrayList<Category>(), new ArrayList<Integer>());
                notifyDataSetChanged();
            }
        });
    }

    private void resetListSelection() {
        for (Price price : priceList) {
            price.setPriceSelected(false);
        }
    }

    @Override
    public int getItemCount() {
        return priceList.size();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        FilterBrandItemBinding itemView;

        public ViewHolderItem(FilterBrandItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

