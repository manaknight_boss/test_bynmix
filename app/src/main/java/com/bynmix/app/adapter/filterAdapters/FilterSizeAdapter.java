package com.bynmix.app.adapter.filterAdapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FilterSizeItemBinding;
import com.bynmix.app.interfaces.FilterListener;
import com.bynmix.app.models.Category;
import com.bynmix.app.models.Size;
import com.bynmix.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class FilterSizeAdapter extends RecyclerView.Adapter<FilterSizeAdapter.ViewHolderItem> {

    private final Context mContext;
    private final List<Size> mSizeList;
    private final FilterListener mCallback;
    private int subTypeId;

    public FilterSizeAdapter(Context mContext, List<Size> mSizeList, int subTypeId, FilterListener mCallback) {
        this.mContext = mContext;
        this.mSizeList = mSizeList;
        this.subTypeId = subTypeId;
        this.mCallback = mCallback;
    }

    @Override
    public FilterSizeAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        FilterSizeItemBinding mBinding = FilterSizeItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new FilterSizeAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final FilterSizeAdapter.ViewHolderItem holder, final int position) {

        final Size size = mSizeList.get(position);
        String sizeText = size.getSizeName();
        holder.itemView.mainItem.setText(sizeText);
        if (size.isSelected()) {
            sizeText = "<b>" + "<font color='" + mContext.getResources().getColor(R.color.purple) + "'>" + sizeText + "</font>" + "</b> ";
            holder.itemView.mainItem.setText(Html.fromHtml(sizeText), TextView.BufferType.SPANNABLE);
        } else {
            sizeText = "<font color='" + mContext.getResources().getColor(R.color.colorPrimary) + "'>" + sizeText + "</font>";
            holder.itemView.mainItem.setText(Html.fromHtml(sizeText), TextView.BufferType.SPANNABLE);
        }

        holder.itemView.filterMainLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                size.setSelected(true);
                mCallback.setSelectedView(size.getSizeId(), size.getSizeName(), Constants.SIZE_FILTER, subTypeId, new ArrayList<Category>(), new ArrayList<Integer>());
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mSizeList.size();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        FilterSizeItemBinding itemView;

        public ViewHolderItem(FilterSizeItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

