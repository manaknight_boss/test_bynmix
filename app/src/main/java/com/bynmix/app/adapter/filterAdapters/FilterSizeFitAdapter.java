package com.bynmix.app.adapter.filterAdapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bynmix.app.databinding.FilterSizeHeaderBinding;
import com.bynmix.app.interfaces.FilterListener;
import com.bynmix.app.models.FilterSizes;
import com.bynmix.app.models.Size;

import java.util.List;

public class FilterSizeFitAdapter extends RecyclerView.Adapter<FilterSizeFitAdapter.ViewHolderItem> {

    private final Context mContext;
    private final List<FilterSizes> mSizeList;
    private final FilterListener mCallback;
    private int subTypeId;

    public FilterSizeFitAdapter(Context mContext, List<FilterSizes> mSizeList, int subTypeId, FilterListener mCallback) {
        this.mContext = mContext;
        this.mSizeList = mSizeList;
        this.subTypeId = subTypeId;
        this.mCallback = mCallback;
    }

    @Override
    public FilterSizeFitAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        FilterSizeHeaderBinding mBinding = FilterSizeHeaderBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new FilterSizeFitAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final FilterSizeFitAdapter.ViewHolderItem holder, final int position) {

        final FilterSizes size = mSizeList.get(position);
        String sizeText = size.getFit();
        holder.itemView.sizeHeader.setText(sizeText);
        FilterSizeAdapter adapter = new FilterSizeAdapter(mContext, size.getSizes(), subTypeId, mCallback);
        holder.itemView.itemRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
        holder.itemView.itemRecyclerView.setAdapter(adapter);

    }

    @Override
    public int getItemCount() {
        return mSizeList.size();
    }

    public void reset(int id, boolean isSet) {
        for (FilterSizes filterSize : mSizeList) {
            for (Size size : filterSize.getSizes()) {
                if (size.getSizeId() == id) {
                    size.setSelected(isSet);
                }
            }
        }
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        FilterSizeHeaderBinding itemView;

        public ViewHolderItem(FilterSizeHeaderBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

