package com.bynmix.app.adapter.filterAdapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FilterSizeTypeItemBinding;
import com.bynmix.app.interfaces.FilterListener;
import com.bynmix.app.models.FilterSizeType;

import java.util.List;

public class FilterSizeTypeAdapter extends RecyclerView.Adapter<FilterSizeTypeAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<FilterSizeType> sizeTypeList;
    private final FilterListener mCallback;

    public FilterSizeTypeAdapter(Context mContext, List<FilterSizeType> sizeTypeList, FilterListener mCallback) {
        this.mContext = mContext;
        this.sizeTypeList = sizeTypeList;
        this.mCallback = mCallback;
    }

    @Override
    public FilterSizeTypeAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        FilterSizeTypeItemBinding mBinding = FilterSizeTypeItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new FilterSizeTypeAdapter.ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final FilterSizeTypeAdapter.ViewHolderItem holder, final int position) {

        final FilterSizeType sizeType = sizeTypeList.get(position);
        String category = sizeType.getSizeTypeName();
        if (sizeType.isSelected()) {
            category = "<b>" + "<font color='" + mContext.getResources().getColor(R.color.purple) + "'>" + category + "</font>" + "</b> ";
            holder.itemView.item.setText(Html.fromHtml(category), TextView.BufferType.SPANNABLE);
        } else {
            category = "<font color='" + mContext.getResources().getColor(R.color.colorPrimary) + "'>" + category + "</font>";
            holder.itemView.item.setText(Html.fromHtml(category), TextView.BufferType.SPANNABLE);
        }
        holder.itemView.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resetListSelection();
                sizeType.setSelected(true);
                mCallback.openSizeFitView(sizeType);
                notifyDataSetChanged();
            }
        });

    }

    private void resetListSelection() {
        for (FilterSizeType sizeType : sizeTypeList) {
            sizeType.setSelected(false);
        }
    }

    @Override
    public int getItemCount() {
        return sizeTypeList.size();
    }

    public void setList(List<FilterSizeType> filterSizeTypeList) {
        this.sizeTypeList = filterSizeTypeList;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        FilterSizeTypeItemBinding itemView;

        public ViewHolderItem(FilterSizeTypeItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

