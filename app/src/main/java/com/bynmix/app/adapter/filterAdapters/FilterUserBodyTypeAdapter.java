package com.bynmix.app.adapter.filterAdapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FilterBrandItemBinding;
import com.bynmix.app.interfaces.FilterUserListener;
import com.bynmix.app.models.BodyType;
import com.bynmix.app.models.Category;
import com.bynmix.app.models.Descriptor;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class FilterUserBodyTypeAdapter extends RecyclerView.Adapter<FilterUserBodyTypeAdapter.ViewHolderItem> {

    private final Context mContext;
    private List<BodyType> mList;
    private final FilterUserListener mCallback;

    public FilterUserBodyTypeAdapter(Context mContext, List<BodyType> mList, FilterUserListener mCallback) {
        this.mContext = mContext;
        this.mList = mList;
        this.mCallback = mCallback;
    }

    @Override
    public FilterUserBodyTypeAdapter.ViewHolderItem onCreateViewHolder(ViewGroup parent, int viewType) {
        FilterBrandItemBinding mBinding = FilterBrandItemBinding.inflate(LayoutInflater.from(mContext), parent, false);
        return new ViewHolderItem(mBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull final FilterUserBodyTypeAdapter.ViewHolderItem holder, final int position) {

        final BodyType bodyType = mList.get(position);
        String bodyTypeName = bodyType.getBodyTypeName();
        Typeface typeface;
        if (bodyType.isSelected()) {
            bodyTypeName = Utils.getColorText(bodyTypeName, mContext.getResources().getColor(R.color.purple));
            typeface = ResourcesCompat.getFont(mContext, R.font.raleway_semi_bold);
        } else {
            if (bodyType.getBodyTypeId() == 0) {
                bodyTypeName = Utils.getColorText(bodyTypeName, mContext.getResources().getColor(R.color.show_all_text));
                typeface = ResourcesCompat.getFont(mContext, R.font.raleway_semi_bold);
            } else {
                bodyTypeName = Utils.getColorText(bodyTypeName, mContext.getResources().getColor(R.color.colorPrimary));
                typeface = ResourcesCompat.getFont(mContext, R.font.raleway_regular);
            }
        }
        holder.itemView.item.setText(Html.fromHtml(bodyTypeName), TextView.BufferType.SPANNABLE);
        holder.itemView.item.setTypeface(typeface);

        holder.itemView.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bodyType.setSelected(true);
                mCallback.setSelectedView(bodyType.getBodyTypeId(), bodyType.getBodyTypeName(), Constants.USER_BODY_TYPE_FILTER, 0, new ArrayList<Category>(), new ArrayList<Integer>());
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public void reset(int id, boolean isSet) {
        for (BodyType bodyType : mList) {
            if (bodyType.getBodyTypeId() == id) {
                bodyType.setSelected(isSet);
                notifyDataSetChanged();
            }
        }
    }

    public void setList(List<BodyType> mmList) {
        this.mList = mmList;
        notifyDataSetChanged();
    }

    class ViewHolderItem extends RecyclerView.ViewHolder {
        FilterBrandItemBinding itemView;

        public ViewHolderItem(FilterBrandItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}

