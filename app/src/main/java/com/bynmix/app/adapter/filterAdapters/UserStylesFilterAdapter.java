package com.bynmix.app.adapter.filterAdapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FilterBrandItemBinding;
import com.bynmix.app.databinding.FilterBrandItemHeaderBinding;
import com.bynmix.app.models.Category;
import com.bynmix.app.models.Descriptor;
import com.bynmix.app.interfaces.FilterUserListener;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class UserStylesFilterAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context mContext;
    private List<Descriptor> mStylesList;
    final private int TYPE_ITEM = 1;
    final private int TYPE_HEADER = 0;
    private FilterUserListener mCallback;
    private List<Descriptor> stylesList;

    public UserStylesFilterAdapter(Context mContext, List<Descriptor> mStylesList, FilterUserListener mCallback) {
        this.mStylesList = mStylesList;
        this.stylesList = mStylesList;
        this.mContext = mContext;
        this.mCallback = mCallback;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            FilterBrandItemBinding mBindingItem = FilterBrandItemBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new UserStylesFilterAdapter.ViewHolderItem(mBindingItem);
        } else {
            FilterBrandItemHeaderBinding mBindingHeader = FilterBrandItemHeaderBinding.inflate
                    (LayoutInflater.from(parent.getContext()), parent, false);
            return new UserStylesFilterAdapter.viewHolderHeader(mBindingHeader);
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof UserStylesFilterAdapter.viewHolderHeader) {
            viewHolderHeader((UserStylesFilterAdapter.viewHolderHeader) holder);
        } else if (holder instanceof UserStylesFilterAdapter.ViewHolderItem) {
            viewHolderItem((UserStylesFilterAdapter.ViewHolderItem) holder, position - 1);
        }
    }


    private void viewHolderHeader(final UserStylesFilterAdapter.viewHolderHeader holder) {
        holder.itemView.autoComplete.requestFocus();
        holder.itemView.autoComplete.setHint(R.string.styles_search_hint);
        holder.itemView.autoComplete.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable editable) {
                // TODO Auto-generated method stub

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newText = s.toString();
                if (newText.length() > 0) {
                    List<Descriptor> searchedList = new ArrayList<>();
                    for (int i = 0; i < mStylesList.size(); i++) {
                        if ((Pattern.compile(Pattern.quote(newText), Pattern.CASE_INSENSITIVE).matcher(mStylesList.get(i).getDescriptorName()).find())) {
                            searchedList.add(mStylesList.get(i));
                        }
                    }
                    setList(searchedList);
                } else {
                    setList(stylesList);
                }
            }

        });
    }

    private void viewHolderItem(UserStylesFilterAdapter.ViewHolderItem holder, final int position) {

        final Descriptor style = mStylesList.get(position);
        String styleName = style.getDescriptorName();
        Typeface typeface;
        if (style.isSelected()) {
            styleName =  Utils.getColorText(styleName, mContext.getResources().getColor(R.color.purple));
            typeface = ResourcesCompat.getFont(mContext, R.font.raleway_semi_bold);
        } else {
            if (style.getDescriptorId() == 0) {
                styleName =  Utils.getColorText(styleName, mContext.getResources().getColor(R.color.show_all_text));
                typeface = ResourcesCompat.getFont(mContext, R.font.raleway_semi_bold);
            } else {
                styleName =  Utils.getColorText(styleName, mContext.getResources().getColor(R.color.colorPrimary));
                typeface = ResourcesCompat.getFont(mContext, R.font.raleway_regular);
            }
        }
        holder.itemView.item.setText(Html.fromHtml(styleName), TextView.BufferType.SPANNABLE);
        holder.itemView.item.setTypeface(typeface);

        holder.itemView.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                style.setSelected(true);
                mCallback.setSelectedView(style.getDescriptorId(), style.getDescriptorName(), Constants.USER_STYLE_FILTER, 0, new ArrayList<Category>(), new ArrayList<Integer>());
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return mStylesList.size() + 1;

    }

    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        }

        return TYPE_ITEM;
    }

    public void setList(List<Descriptor> stylesList) {
        this.mStylesList = stylesList;
        notifyDataSetChanged();
    }

    public void setAdapterList(List<Descriptor> stylesList) {
        this.stylesList = stylesList;

    }

    public void reset(int id, boolean isSet) {
        for (Descriptor style : mStylesList) {
            if (style.getDescriptorId() == id) {
                style.setSelected(isSet);
                notifyDataSetChanged();
            }
        }
    }


    class ViewHolderItem extends RecyclerView.ViewHolder {
        FilterBrandItemBinding itemView;

        public ViewHolderItem(FilterBrandItemBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }

    private class viewHolderHeader extends RecyclerView.ViewHolder {
        FilterBrandItemHeaderBinding itemView;

        private viewHolderHeader(FilterBrandItemHeaderBinding itemView) {
            super(itemView.getRoot());
            this.itemView = itemView;
        }
    }
}