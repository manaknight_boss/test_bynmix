package com.bynmix.app.customviews;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.interfaces.AnnotatIVInterface;
import com.bynmix.app.interfaces.DotsSingleTabInterface;
import com.bynmix.app.models.AnnotatedPoint;

import java.util.List;

public class AnnotateIV extends AppCompatImageView implements GestureDetector.OnGestureListener {

    private static final int RADIUS = 40;
    private static final int TAP_RADIUS = 65;
    private static final int MAX_POINTS = 5;
    private GestureDetector gestureDetector;
    private float MAX_X;
    private float MAX_Y;
    private float MIN_X;
    private float MIN_Y;
    private boolean isBoundaryInitialized;
    private List<AnnotatedPoint> pointList;
    private Paint pointPaint;
    private Paint paintTriangle;
    private boolean isMoving;
    private AnnotatedPoint movingPoint;
    private int id = 0;
    private AnnotatIVInterface mCallBack;
    private int color[] = getResources().getIntArray(R.array.bynmix_color);
    private boolean isEnableView = false;
    private DotsSingleTabInterface listener;
    private boolean isViewDraw = false;

    public AnnotateIV(Context context) {
        this(context, null);
        gestureDetector = new GestureDetector(context, this);
    }

    public AnnotateIV(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
        gestureDetector = new GestureDetector(context, this);
    }

    public AnnotateIV(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        pointPaint = new Paint();
        paintTriangle = new Paint();
        paintTriangle.setStyle(Paint.Style.FILL_AND_STROKE);
        pointPaint.setStrokeWidth(2);
        gestureDetector = new GestureDetector(context, this);
    }

    private void initBoundaries() {
        if (isBoundaryInitialized || this.getWidth() == 0 || this.getHeight() == 0)
            return;
        MAX_X = (float) (this.getWidth() - RADIUS - (RADIUS * .15));
        MAX_Y = (float) (this.getHeight() - RADIUS - (RADIUS * .15));
        MIN_X = (float) (RADIUS - (RADIUS * .15));
        MIN_Y = (float) (RADIUS - (RADIUS * .15));
        isBoundaryInitialized = true;
    }

    public void setPointList(List<AnnotatedPoint> pointList) {
        this.pointList = pointList;
        invalidate();
    }

    public void setListener(AnnotatIVInterface mCallBack) {
        this.mCallBack = mCallBack;
    }

    public void setDotsListener(DotsSingleTabInterface listener) {
        this.listener = listener;
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        initBoundaries();

        boolean shouldRemoveAllTextView = true;
        for (AnnotatedPoint point : pointList) {
            if (isViewDraw) {
                paintTriangle.setColor(point.color);
                float x = point.x * this.getWidth();
                float y = point.y * this.getHeight();
                drawToolTip(x, y, canvas, point, shouldRemoveAllTextView);
                shouldRemoveAllTextView = false;
            } else {
                pointPaint.setColor(point.color);
                canvas.drawCircle(point.x * this.getWidth(), point.y * this.getHeight(), RADIUS, pointPaint);
            }
        }
    }

    private AnnotatedPoint addPointFromEvent(MotionEvent event) {
        AnnotatedPoint annotatedPoint = new AnnotatedPoint();
        annotatedPoint.id = ++id;
        annotatedPoint.x = event.getX() / this.getWidth();
        annotatedPoint.y = event.getY() / this.getHeight();
        if (pointList.size() == 0) {
            annotatedPoint.color = color[0];
        } else {
            int i;
            for (i = 0; i < color.length; i++) {
                boolean found = false;
                for (AnnotatedPoint point : pointList) {
                    if (point.color == color[i]) {
                        found = true;
                        break;
                    }
                }
                if (!found) break;
            }
            annotatedPoint.color = color[i];
        }
        mCallBack.add(annotatedPoint);
        return annotatedPoint;
    }

    private void drawToolTip(float x, float y, Canvas canvas, final AnnotatedPoint point, boolean isRemoveAllTextViews) {
        float tooptipHeight = dpToPixel(7);
        float tooptipWidth = dpToPixel(6);
        float tooptipWidth1 = dpToPixel(1);
        float halfHeight = (tooptipHeight + tooptipWidth) / 2;
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setStroke(2, point.color);
        shape.setColor(point.color);
        ViewGroup parent = (ViewGroup) this.getParent();
        float textViewWidth = dpToPixel(51);
        float textViewHeight = dpToPixel(23);
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams((int) textViewWidth, (int) textViewHeight);
        shape.setCornerRadii(new float[]{4, 4, 4, 4, 4, 4, 4, 4});
        PointF point1_draw = new PointF(x, y);
        PointF point2_draw;
        PointF point3_draw;
        if (point.x > 0.01 && point.y < 0.15) {
            point2_draw = new PointF(x - halfHeight, y + tooptipHeight);
            point3_draw = new PointF(x + halfHeight, y + tooptipHeight);
            lp.setMargins((int) x - (int) (textViewWidth / 2), (int) y + (int) tooptipHeight, 0, 0);
        } else {
            point2_draw = new PointF(x - halfHeight, y - tooptipHeight);
            point3_draw = new PointF(x + halfHeight, y - tooptipHeight);
            lp.setMargins((int) x - (int) (textViewWidth / 2), (int) y - (int) (textViewHeight + (tooptipHeight - tooptipWidth1)), 0, 0);
        }
        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        path.moveTo(point1_draw.x, point1_draw.y);
        path.lineTo(point2_draw.x, point2_draw.y);
        path.lineTo(point3_draw.x, point3_draw.y);
        path.lineTo(point1_draw.x, point1_draw.y);
        path.close();
        canvas.drawPath(path, paintTriangle);


        TextView view = new TextView(getContext());
        view.setLayoutParams(lp);
        view.setGravity(Gravity.CENTER);
        view.setTextColor(getResources().getColor(R.color.white));
        view.setText("View");
        view.setTextSize(12);
        view.setBackground(shape);
        if (isRemoveAllTextViews) {
            for (int i = 0; i < parent.getChildCount(); i++) {
                View textView = parent.getChildAt(i);
                if (textView instanceof TextView) {
                    parent.removeView(textView);
                    i--;
                }
            }
        }
        parent.addView(view);
        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.singleTab(point);
            }
        });
    }

    private float dpToPixel(float dip) {
        Resources r = getResources();
        return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                dip,
                r.getDisplayMetrics());
    }

    private AnnotatedPoint updateMovingPoint(MotionEvent event) {
        movingPoint.x = event.getX() / this.getWidth();
        movingPoint.y = event.getY() / this.getHeight();
        mCallBack.update(movingPoint);
        return movingPoint;
    }

    public void setViewDraw(boolean isViewDraw) {
        this.isViewDraw = isViewDraw;
    }

    private void removeMovingPoint() {
        mCallBack.remove(movingPoint);
    }

    private AnnotatedPoint findAnnotatedPoint(float x, float y) {
        float leftTopMargin = TAP_RADIUS - RADIUS;
        for (AnnotatedPoint annotatedPoint : pointList) {
            float ax = (annotatedPoint.x * this.getWidth());
            float ay = (annotatedPoint.y * this.getHeight());
            if (x >= ax - leftTopMargin && x <= ax + TAP_RADIUS
                    && y >= ay - leftTopMargin && y <= ay + TAP_RADIUS) {
                return annotatedPoint;
            }
        }
        return null;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        gestureDetector.onTouchEvent(event);
        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN: {
                if (isEnableView) {
                    movingPoint = findAnnotatedPoint(x, y);
                    if (movingPoint != null) {
                        isMoving = true;
                    } else if (pointList.size() < MAX_POINTS) {
                        movingPoint = addPointFromEvent(event);
                        isMoving = true;
                    }
                    mCallBack.changeScrollStatus(false);
                }
                return true;
            }

            case MotionEvent.ACTION_MOVE: {
                if (isEnableView) {
                    if (isMoving) {
                        updateMovingPoint(event);
                    }
                }

                return true;
            }

            case MotionEvent.ACTION_UP: {
                if (isEnableView) {
                    if (isMoving)
                        if (x > MAX_X || x < MIN_X || y > MAX_Y || y < MIN_Y)
                            removeMovingPoint();

                    isMoving = false;
                    movingPoint = null;
                }
                if(mCallBack != null) {
                    mCallBack.changeScrollStatus(true);
                }
                return true;
            }
        }

        return super.onTouchEvent(event);
    }

    public void setMoveDots(boolean isMove) {
        isEnableView = isMove;
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        if (listener != null) {
            movingPoint = findAnnotatedPoint(motionEvent.getX(), motionEvent.getY());
            if (movingPoint != null) {
                listener.singleTab(movingPoint);
            } else {
                listener.singleTab();
            }
        }
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }
}
