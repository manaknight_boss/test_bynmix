package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.databinding.AddAddressFragmentBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.Address;
import com.bynmix.app.utils.Constants;


public class AddAddressFragment extends Fragment {

    private AddAddressFragmentBinding mBinding;
    private ProfileListener mCallback;
    private Context mContext;
    private Address address = new Address();

    public static AddAddressFragment newInstance() {
        return new AddAddressFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (ProfileListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = AddAddressFragmentBinding.inflate(inflater, container, false);
        mBinding.shippingAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                address.setShippingAddress(true);
                address.setReturnAddress(false);
                mCallback.openAddNewAddressFragment(address, Constants.ADD_ADDRESS,null);
            }
        });
        mBinding.returnAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                address.setReturnAddress(true);
                address.setShippingAddress(false);
                mCallback.openAddNewAddressFragment(address, Constants.ADD_ADDRESS, null);
            }
        });
        return mBinding.getRoot();
    }
}
