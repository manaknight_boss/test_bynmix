package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FragmentAddBankDetailBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Payout;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.DialogAlert;
import com.bynmix.app.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddBankDetailFragment extends BaseFragment {

    private FragmentAddBankDetailBinding mBinding;
    private ProfileListener mCallback;
    private Context mContext;
    private int currentBalance;

    public static AddBankDetailFragment newInstance() {
        return new AddBankDetailFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mCallback = (ProfileListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentAddBankDetailBinding.inflate(inflater, container, false);
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
        mBinding.balance.setText("" + currentBalance + ".00");
        mBinding.withdrawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addBankDetail();
            }
        });
        return mBinding.getRoot();
    }

    private void addBankDetail() {
        String bankAccountNumber, bankRoutingNumber, bankAccountNickName, fullName, withdrawAmount;
        bankAccountNumber = mBinding.bankAccountNumber.getText().toString().trim();
        bankRoutingNumber = mBinding.bankAccountRoutingNumber.getText().toString().trim();
        bankAccountNickName = mBinding.bankAccountNickName.getText().toString().trim();
        fullName = mBinding.fullName.getText().toString().trim();
        withdrawAmount = mBinding.withdrawAmount.getText().toString().trim();


        if (TextUtils.isEmpty(bankAccountNickName)) {
            mBinding.bankAccountNickName.setError("Enter your Bank Nick Name");
            return;
        }

        if (TextUtils.isEmpty(fullName)) {
            mBinding.fullName.setError("Enter your Bank Associated Full name");
            return;
        }

        if (TextUtils.isEmpty(bankAccountNumber)) {
            mBinding.bankAccountNumber.setError("Enter your Bank Account number");
            return;
        }

        if (TextUtils.isEmpty(bankRoutingNumber)) {
            mBinding.bankAccountRoutingNumber.setError("Enter your Routing number");
            return;
        }

        if (TextUtils.isEmpty(withdrawAmount)) {
            mBinding.withdrawAmount.setError("Enter Withdraw amount");
            return;
        }


        final double amount = Double.parseDouble(withdrawAmount);

        if (amount <= 0) {
            mBinding.withdrawAmount.setError("Invalid amount");
            return;
        }

        if (amount > currentBalance) {
            mBinding.withdrawAmount.setError(getString(R.string.error_insufficient_balance));
            return;
        }

        if (amount > Constants.MAX_WITHDRAW_LIMIT) {
            mBinding.withdrawAmount.setError(getString(R.string.error_max_withdraw_limit));
            return;
        }

        final Payout payout = new Payout();
        payout.setWithdrawAmount(amount);
        payout.setNameOnBankAccount(fullName);
        payout.setBankNickName(bankAccountNickName);
        payout.setRoutingNumber(bankRoutingNumber);
        payout.setAccountNumber(bankAccountNumber);
        String title = "$" + Utils.formatValueToTwoDecimalPlaces(amount);
        DialogAlert.showWithDrawDialog(mContext, title, new DialogClickListener() {
            @Override
            public void onClick() {
                callApi(payout);
            }
        });
    }

    private void callApi(final Payout payout) {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.withdrawAmount("Bearer " + getAccessToken(), payout).enqueue(new Callback<ApiResponse<Payout>>() {
            @Override
            public void onResponse(Call<ApiResponse<Payout>> call, Response<ApiResponse<Payout>> response) {
                mBinding.progressBar.setVisibility(View.GONE);
                if (response.isSuccessful() && response.code() == 200 || response.code() == 201) {
                    Payout payouts = response.body().getData();
                    String lastFour = Utils.getColorText(payouts.getLastFour(), mContext.getResources().getColor(R.color.withdraw_pink));
                    String formattedDate = Utils.getColorText(payouts.getFormattedDate(), mContext.getResources().getColor(R.color.withdraw_pink));
                    String withdrawAmount = Utils.getColorText("$" + Utils.formatValueToTwoDecimalPlaces(payouts.getAmount()), mContext.getResources().getColor(R.color.withdraw_pink));
                    String descriptionMiddle = "An amount of " + "<b><font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + withdrawAmount + "</font></b>" + " has been processed to bank account ending in " + "<b><font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + lastFour + "</font></b>" + " and is expected to appear in your account on " + "<b><font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + formattedDate + "</font></b>";
                    String message = mContext.getString(R.string.happy_pay_day) + descriptionMiddle + mContext.getString(R.string.withdraw_success_msg);
                    mCallback.onBackPressed();
                    mCallback.onBackPressed();
                    mCallback.onBackPressed();
                    DialogAlert.showSaleCancelSuccessfulDialog(mContext, mContext.getResources().getString(R.string.success), message, false);
                    mCallback.openTransactionHistoryScreen();

                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            callApi(payout);
                        }

                        @Override
                        public void errorAccessToken() {
                            showAlertDialog(getResources().getString(R.string.error_server));
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    DialogAlert.showCancelSaleErrorDialog(mContext, message, "Looks like we were not able to transfer your funds at this time due to the following reason:");
                }
                mBinding.progressBar.setVisibility(View.VISIBLE);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Payout>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    public void setCurrentBalance(int currentBalance) {
        this.currentBalance = Math.abs(currentBalance);
    }
}
