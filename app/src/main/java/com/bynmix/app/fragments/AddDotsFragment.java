package com.bynmix.app.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.databinding.AddDotsFragmentBinding;
import com.bynmix.app.interfaces.AnnotatIVInterface;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.utils.AddPostTagDialog;
import com.bynmix.app.utils.Constants;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AddDotsFragment extends Fragment {
    AddDotsFragmentBinding mBinding;
    private Context mContext;
    private CreatePostListener mCallback;
    private String photoUrl;
    private List<AnnotatedPoint> mPointList;
    private Vibrator vibe;
    private boolean isTagsSelected = false;
    private boolean isEditPost = false;

    public static AddDotsFragment newInstance() {
        return new AddDotsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPointList = new ArrayList<>();
        ((HomeActivity)mContext).setImageUsed(false);
        mCallback.setAnnotatedPoints(null);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.add_dots_fragment, container, false);
        vibe = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        mBinding.whatIsText.setPaintFlags(mBinding.whatIsText.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
        if(photoUrl == null) {
            photoUrl = mCallback.getPostImage();
        } else {
            isEditPost = true;
            addTags();
        }
        mBinding.whatIsText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddPostTagDialog addPostTagDialog = new AddPostTagDialog();
                addPostTagDialog.showDialog(mContext);
            }
        });

        inits();

        return mBinding.getRoot();
    }

    private void inits() {
        mBinding.postImage.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mBinding.postImage.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                if (photoUrl != null) {
                    if(photoUrl.startsWith("http")) {
                        try {
                            Picasso.get().load(photoUrl).into(mBinding.postImage);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    } else {
                        File fLocal = new File(photoUrl);
                        try {
                            Picasso.get().load(fLocal).into(mBinding.postImage);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
        });
        mBinding.postImage.setPointList(mPointList);
        mBinding.postImage.setListener(new AnnotatIVInterface() {
            @Override
            public void add(AnnotatedPoint annotatedPoint) {
                mPointList.add(annotatedPoint);
                mBinding.postImage.setPointList(mPointList);
            }

            @Override
            public void update(AnnotatedPoint annotatedPoint) {
                mBinding.postImage.setPointList(mPointList);
            }

            @Override
            public void remove(AnnotatedPoint annotatedPoint) {
                mPointList.remove(annotatedPoint);
                vibe.vibrate(100);
                mBinding.postImage.setPointList(mPointList);
            }

            @Override
            public void changeScrollStatus(boolean enable) {
                mBinding.scrollView.requestDisallowInterceptTouchEvent(!enable);
            }
        });
        mBinding.addTags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTags();
            }
        });
        mBinding.check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onTagsSubmit();
            }
        });
        mBinding.noContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onTagsSubmit();
            }
        });

        if (mPointList.size() > 0) {
            addTags();
        }

    }

    private void onTagsSubmit() {
        if (isTagsSelected) {
            if (mPointList.size() == 0) {
                Toast.makeText(mContext, "Please add tags first.", Toast.LENGTH_SHORT).show();
            } else {
                //TODO move list to next screen
                ((HomeActivity)mContext).setImageUsed(true);
                mCallback.setAnnotatedPoints(mPointList);
                if (mCallback.isEditPost() || isEditPost) {
                    mCallback.dotsDetailFragment(new FeedResponse());
                } else {
                    mCallback.dotsDetailFragment(null);
                }
            }
        } else {
            //Todo previous work

            if (mCallback.isEditPost()) {
                ((HomeActivity)mContext).setImageUsed(true);
                mCallback.popFragment();
            } else {
                mCallback.openAddPostDetailFragment();
            }
        }

    }

    private void addTags() {
        mBinding.addTagsTextPost.setText(getResources().getString(R.string.add_product));
        mBinding.tabAnywhereText.setVisibility(View.VISIBLE);
        mBinding.noContinue.setVisibility(View.VISIBLE);
        mBinding.check.setVisibility(View.VISIBLE);
        mBinding.addTags.setVisibility(View.GONE);
        mBinding.noContinueText.setText(getResources().getString(R.string.next));
        isTagsSelected = true;
        mBinding.postImage.setMoveDots(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (CreatePostListener) mContext;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}
