package com.bynmix.app.fragments;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ScrollView;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.adapter.AddListingColorAdapter;
import com.bynmix.app.adapter.AddTagsAdapter;
import com.bynmix.app.databinding.AddListingDetailFragmentBinding;
import com.bynmix.app.interfaces.ConfirmInterface;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.models.Brand;
import com.bynmix.app.models.Category;
import com.bynmix.app.models.Colors;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.models.ListingDisplayImage;
import com.bynmix.app.models.Size;
import com.bynmix.app.utils.ConfirmDialog;
import com.bynmix.app.utils.DialogAlert;
import com.bynmix.app.utils.ErrorDialog;
import com.bynmix.app.utils.Utils;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;
import java.util.List;

public class AddListingDetailFragment extends Fragment {

    private AddListingDetailFragmentBinding mBinding;
    private CreatePostListener mCallback;
    private Context mContext;
    private List<ListingDisplayImage> mPhotoList = new ArrayList<>();
    private ListingDetail listingDetail = new ListingDetail();
    private List<String> mTagsList = new ArrayList<>();
    ErrorDialog errorDialog = new ErrorDialog();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (CreatePostListener) context;
        mContext = context;
    }

    public static AddListingDetailFragment newInstance() {
        return new AddListingDetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = AddListingDetailFragmentBinding.inflate(inflater, container, false);
        listingDetail = mCallback.listingDetail();
        init();
        return mBinding.getRoot();
    }

    private void init() {
        Utils.setCapitalizeTextWatcher(mBinding.listingTitle);
        Utils.setCapitalizeTextWatcher(mBinding.shortDescription);
        setTagAdapter();
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.clearListDetail();
                mCallback.onBackPressed();
            }
        });
        mBinding.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openSellerPolicyFragment();
            }
        });
        mBinding.addCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openCategoryFragment(0, 0);
            }
        });

        mBinding.categoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openCategoryFragment(listingDetail.getCategory().getCategoryId(), listingDetail.getCategoryType().getCategoryTypeId());
            }
        });

        mBinding.addSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openSizeFragment(0, 0, null);
            }
        });

        mBinding.sizeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openSizeFragment(listingDetail.getSizeType().getSizeTypeId(), listingDetail.getSize().getSizeId(), listingDetail.getSizeHeader());
            }
        });

        mBinding.addColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openColorFragment(getColorsIdsList());
            }
        });

        mBinding.addBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openBrandFragment(0);
            }
        });

        mBinding.brandLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openBrandFragment(listingDetail.getBrand().getBrandId());
            }
        });

        mBinding.create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendData();
            }
        });

        mBinding.switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    listingDetail.setConditionId(1);
                } else {
                    listingDetail.setConditionId(2);
                }
            }
        });
        setLowestPriceUI(listingDetail.isBuyItNowOnly());
        mBinding.directSwitchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listingDetail.setBuyItNowOnly(false);
                setLowestPriceUI(false);
                if (mBinding.directSwitchButton.isChecked()) {
                    mBinding.directSwitchButton.setChecked(false);
                    DialogAlert.showDialog(mContext, mContext.getResources().getString(R.string.direct_purchase), mContext.getResources().getString(R.string.are_you_sure_want_to_change_direct_purchase), mContext.getResources().getString(R.string.convert_to_direct_purchase_detail), R.string.yes_converted_to_direct_purchase, new DialogClickListener() {
                        @Override
                        public void onClick() {
                            mBinding.directSwitchButton.setChecked(true);
                            listingDetail.setBuyItNowOnly(true);
                            setLowestPriceUI(true);
                        }
                    });
                }
            }
        });
        updateUI();
    }

    private void setLowestPriceUI(boolean isChecked) {
        if (isChecked) {
            mBinding.lowestAllowedPriceLayout.setVisibility(View.GONE);
        } else {
            mBinding.lowestAllowedPriceLayout.setVisibility(View.VISIBLE);
        }
    }


    private void sendData() {
        String listingTitle, shortDescription, originalPrice, listingPrice, lowestAllowedPrice;
        listingTitle = mBinding.listingTitle.getText().toString().trim();
        shortDescription = mBinding.shortDescription.getText().toString().trim();
        originalPrice = mBinding.originalPrice.getText().toString().trim();
        listingPrice = mBinding.listingPrice.getText().toString().trim();
        lowestAllowedPrice = mBinding.lowestAllowedPrice.getText().toString().trim();

        if (TextUtils.isEmpty(listingTitle)) {
            String error = getColorText(getString(R.string.listing_title_text));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (TextUtils.isEmpty(shortDescription)) {
            String error = getColorText(getString(R.string.short_description_text));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (mTagsList.size() == 0) {
            String error = getColorText(getString(R.string.listing_tag_text));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (TextUtils.isEmpty(originalPrice)) {
            String error = getColorText(getString(R.string.original_price));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (TextUtils.isEmpty(listingPrice)) {
            String error = getColorText(getString(R.string.listing_price));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (!TextUtils.isEmpty(lowestAllowedPrice)) {
            if (Integer.parseInt(lowestAllowedPrice) >= Double.parseDouble(listingPrice)) {
                ((HomeActivity) mContext).mAlertDialog.showDialog((HomeActivity) mContext, "Your lowest allowable offer must be less than the listing price.");
                return;
            }
        }


        if (listingDetail.getCategory() == null) {
            String error = getColorText(getString(R.string.Category));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (listingDetail.getSize() == null) {
            String error = getColorText(getString(R.string.size_text));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }


        if (listingDetail.getColors() == null) {
            String error = getColorText(getString(R.string.colors_text));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (listingDetail.getBrand() == null) {
            String error = getColorText(getString(R.string.brand_text));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }
        listingDetail.setOriginalPrice(Double.parseDouble(originalPrice));
        listingDetail.setPrice(Double.parseDouble(listingPrice));
        listingDetail.setTitle(listingTitle);
        listingDetail.setDescription(shortDescription);
        listingDetail.setTags(mTagsList);
        listingDetail.setAvailabilityId(1);
        listingDetail.setCategoryTypeId(listingDetail.getCategoryType().getCategoryTypeId());
        listingDetail.setCategoryId(listingDetail.getCategory().getCategoryId());
        listingDetail.setSizeTypeId(listingDetail.getSizeType().getSizeTypeId());
        listingDetail.setSizeId(listingDetail.getSize().getSizeId());
        listingDetail.setBrandId(listingDetail.getBrand().getBrandId());
        listingDetail.setColorIds(getColorsIdsList());
        if (!TextUtils.isEmpty(lowestAllowedPrice)) {
            listingDetail.setLowestAllowableOffer(Integer.parseInt(lowestAllowedPrice));
        }
        mCallback.openShippingFragment(listingDetail, mPhotoList);
    }

    private List<Integer> getColorsIdsList() {
        List<Integer> colorIdsList = new ArrayList<>();
        if (listingDetail.getColors() != null && listingDetail.getColors().size() > 0) {
            for (int i = 0; i < listingDetail.getColors().size(); i++) {
                colorIdsList.add(listingDetail.getColors().get(i).getColorId());
            }
        }
        return colorIdsList;
    }

    private String getColorText(String string) {
        return "<b>" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + string + "</font>" + "</b> ";
    }


    private void setTagAdapter() {
        AddTagsAdapter mAdapter = new AddTagsAdapter(mContext, mTagsList);
        mBinding.recycleView.setLayoutManager(new FlowLayoutManager());
        mBinding.recycleView.setAdapter(mAdapter);
        mBinding.recycleView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.set(15, 15, 15, 15);

            }
        });
    }

    public void setPhotoList(List<ListingDisplayImage> photoList) {
        this.mPhotoList = photoList;
    }

    private void updateUI() {
        if (listingDetail != null) {
            if (listingDetail.getCategory() != null) {
                mBinding.addCategory.setVisibility(View.GONE);
                mBinding.categoryLayout.setVisibility(View.VISIBLE);
                mBinding.category.setText(listingDetail.getCategory().getCategoryName());
                mBinding.categoryType.setText(listingDetail.getCategoryType().getCategoryTypeName());
            } else {
                mBinding.addCategory.setVisibility(View.VISIBLE);
                mBinding.categoryLayout.setVisibility(View.GONE);
            }

            if (listingDetail.getSize() != null) {
                mBinding.addSize.setVisibility(View.GONE);
                mBinding.sizeLayout.setVisibility(View.VISIBLE);
                mBinding.size.setText(listingDetail.getSize().getSizeName());
            } else {
                mBinding.addSize.setVisibility(View.VISIBLE);
                mBinding.sizeLayout.setVisibility(View.GONE);
            }

            if (listingDetail.getColors() != null && listingDetail.getColors().size() > 0) {
                mBinding.addColor.setVisibility(View.GONE);
                mBinding.colorRecyclerView.setVisibility(View.VISIBLE);
                setColors(listingDetail.getColors());
            } else {
                mBinding.addColor.setVisibility(View.VISIBLE);
                mBinding.colorRecyclerView.setVisibility(View.GONE);
            }

            if (listingDetail.getBrand() != null) {
                mBinding.addBrand.setVisibility(View.GONE);
                mBinding.brandLayout.setVisibility(View.VISIBLE);
                mBinding.brand.setText(listingDetail.getBrand().getBrandName());
            } else {
                mBinding.addBrand.setVisibility(View.VISIBLE);
                mBinding.brandLayout.setVisibility(View.GONE);
            }
        }

    }

    private void setColors(List<Colors> colors) {
        AddListingColorAdapter addListingColorAdapter = new AddListingColorAdapter(mContext, colors, mCallback, 1);
        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        mBinding.colorRecyclerView.setLayoutManager(flowLayoutManager);
        mBinding.colorRecyclerView.setAdapter(addListingColorAdapter);
        mBinding.colorRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.set(0, 0, 15, 0);
            }
        });
    }

    public void setColor(List<Colors> colorsList) {
        this.listingDetail.setColors(colorsList);
        updateUI();
    }

    public void setBrand(Brand brand) {
        this.listingDetail.setBrandId(brand.getBrandId());
        updateUI();
    }

    public void moveScrollDown() {
        mBinding.scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {
                mBinding.scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        }, 200);
    }

    public void checkOnBackPress(final int id) {
        Category category = listingDetail.getCategory();
        Size size = listingDetail.getSize();
        List<Colors> colors = listingDetail.getColors();
        Brand brand = listingDetail.getBrand();
        String listingTitle = mBinding.listingTitle.getText().toString().trim();
        String shortDescription = mBinding.shortDescription.getText().toString().trim();
        String originalPrice = mBinding.originalPrice.getText().toString().trim();
        String listingPrice = mBinding.listingPrice.getText().toString().trim();
        if (!TextUtils.isEmpty(listingTitle) || !TextUtils.isEmpty(shortDescription)
                || !TextUtils.isEmpty(originalPrice) ||
                !TextUtils.isEmpty(listingPrice) || category != null ||
                size != null || brand != null || colors != null || mTagsList.size() > 0) {
            ConfirmDialog confirmDialog = new ConfirmDialog();
            confirmDialog.showDialog(mContext, new ConfirmInterface() {
                @Override
                public void confirm() {
                    mCallback.clearListDetail();
                    if(id != 0) {
                        ((HomeActivity)mContext).moveToTab(id);
                    } else {
                        mCallback.popFragment();
                        mCallback.popFragment();
                    }
                }
            });

        } else {
            mCallback.clearListDetail();
            mCallback.popFragment();
        }
    }
}
