package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.adapter.SpinnerAdapter;
import com.bynmix.app.databinding.AddNewAddressBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.SelectExistingAddressListener;
import com.bynmix.app.models.Address;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.States;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNewAddressFragment extends BaseFragment {

    private AddNewAddressBinding mBinding;
    private Address address;
    private ProfileListener mCallback;
    private Context mContext;
    private SpinnerAdapter spinnerAdapter;
    private List<States> mStateList = new ArrayList<>();
    private States states = new States();
    private int type;
    private SelectExistingAddressListener mListener;

    public static AddNewAddressFragment newInstance() {
        return new AddNewAddressFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mCallback = (ProfileListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = AddNewAddressBinding.inflate(inflater, container, false);
        setData();
        getStatesList();
        if (type == Constants.EDIT_ADDRESS) {
            mBinding.toolbarHeading.setText(getString(R.string.edit_address));
            mBinding.saveButton.setText(getString(R.string.update_text));
            if (address.isShippingAddress()) {
                mBinding.defaultShippingLayout.setVisibility(View.GONE);
            } else {
                mBinding.defaultShippingLayout.setVisibility(View.VISIBLE);
            }
            if (address.isReturnAddress()) {
                mBinding.defaultReturnLayout.setVisibility(View.GONE);
            } else {
                mBinding.defaultReturnLayout.setVisibility(View.VISIBLE);
            }
        } else {
            mBinding.saveButton.setText(getString(R.string.add_address));
            mBinding.toolbarHeading.setText(getString(R.string.add_new_address));
            mBinding.defaultReturnLayout.setVisibility(View.VISIBLE);
            mBinding.defaultShippingLayout.setVisibility(View.VISIBLE);
        }

        spinnerAdapter = new SpinnerAdapter(mContext, R.layout.profile_spinner_item, mStateList);
        mBinding.state.setAdapter(spinnerAdapter);
        mBinding.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendAddressData();
            }
        });
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });


        mBinding.defaultShippingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                address.setShippingAddress(!address.isShippingAddress());
                updateUI();
            }
        });

        mBinding.defaultReturnLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                address.setReturnAddress(!address.isReturnAddress());
                updateUI();
            }
        });

        updateUI();

        return mBinding.getRoot();
    }

    private void updateUI() {
        if (address.isShippingAddress()) {
            mBinding.shippingCheckBox.setImageResource(R.mipmap.checkbox_checked);
        } else {
            mBinding.shippingCheckBox.setImageResource(R.mipmap.checkbox);
        }
        if (address.isReturnAddress()) {
            mBinding.returnCheckBox.setImageResource(R.mipmap.checkbox_checked);
        } else {
            mBinding.returnCheckBox.setImageResource(R.mipmap.checkbox);
        }
    }

    private void sendAddressData() {
        String fullName = mBinding.fullName.getText().toString().trim();
        String addressOne = mBinding.addressOne.getText().toString().trim();
        String addressTwo = mBinding.addressTwo.getText().toString().trim();
        int selectedId = mBinding.state.getSelectedItemPosition();
        String state = mStateList.get(selectedId).getName();
        String stateAbbreviation = mStateList.get(selectedId).getAbbreviation();
        String city = mBinding.city.getText().toString().trim();
        String zipcode = mBinding.zipcode.getText().toString().trim();

        if (TextUtils.isEmpty(fullName)) {
            mBinding.fullName.setError(getResources().getString(R.string.enter_full_name));
            return;
        }

        if (TextUtils.isEmpty(addressOne)) {
            mBinding.addressOne.setError(getResources().getString(R.string.enter_address_one));
            return;
        }

        if (TextUtils.isEmpty(city)) {
            mBinding.city.setError(getResources().getString(R.string.city_error));
            return;
        }

        if (state.equalsIgnoreCase("Select State")) {
            Toast.makeText(mContext, "Please select State.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (TextUtils.isEmpty(zipcode)) {
            mBinding.zipcode.setError(getResources().getString(R.string.enter_zipcode));
            return;
        }
        mBinding.save.setEnabled(false);
        address.setAddressOne(addressOne);
        address.setAddressTwo(addressTwo);
        address.setState(state);
        address.setStateAbbreviation(stateAbbreviation);
        address.setCity(city);
        address.setFullName(fullName);
        address.setZipCode(zipcode);
        if (type == Constants.EDIT_ADDRESS) {
            mCallback.editAddress(address);
        } else {
            addAddress(address);
        }
    }

    private void setData() {
        Utils.setCapitalizeTextWatcher(mBinding.addressOne);
        Utils.setCapitalizeTextWatcher(mBinding.addressTwo);
        Utils.setCapitalizeTextWatcher(mBinding.city);
        mBinding.fullName.setText(address.getFullName());
        mBinding.addressOne.setText(address.getAddressOne());
        mBinding.addressTwo.setText(address.getAddressTwo());
        mBinding.city.setText(address.getCity());
        mBinding.zipcode.setText(address.getZipCode());
    }

    public void setAddress(Address address) {

        this.address = address;
    }

    public void updateList(List<States> state) {
        this.mStateList = state;
        spinnerAdapter.setList(mStateList);

        if (address.getState() != null) {
            for (int i = 0; i < mStateList.size(); i++) {

                if (address.getStateAbbreviation().equals(mStateList.get(i).getAbbreviation())) {
                    mStateList.remove(i);
                    states.setName(address.getState());
                    states.setAbbreviation(address.getStateAbbreviation());
                    mStateList.add(0, states);
                    break;
                }
            }
        } else {
            states.setName("Select State");
            mStateList.add(0, states);
        }
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setSaveButtonEnable() {
        mBinding.progressBar.setVisibility(View.GONE);
        mBinding.save.setEnabled(true);
    }


    public void addAddress(final Address address) {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.addAddress("Bearer " + getAccessToken(), address).enqueue(new Callback<ApiResponse<Integer>>() {
            @Override
            public void onResponse(Call<ApiResponse<Integer>> call, Response<ApiResponse<Integer>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    int id = response.body().getData();
                    if (type == Constants.EXISTING_ADD_ADDRESS_FRAGMENT) {
                        address.setAddressId(id);
                        mListener.selectedAddress(address);
                    }
                    mCallback.onBackPressed();
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            addAddress(address);
                        }

                        @Override
                        public void errorAccessToken() {
                        }
                    });
                } else {
                    String message;
                    try {
                        message = Utils.ErrorMessage(mContext, response.errorBody());
                    } catch (Exception e) {
                        message = mContext.getString(R.string.error_server);
                    }
                    showAlertDialog(message);
                    setSaveButtonEnable();
                }
                hideProgress();
                mBinding.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ApiResponse<Integer>> call, Throwable t) {
                setSaveButtonEnable();
                hideProgress();
                mBinding.progressBar.setVisibility(View.GONE);
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    public void setListener(SelectExistingAddressListener mListener) {
        this.mListener = mListener;
    }
}
