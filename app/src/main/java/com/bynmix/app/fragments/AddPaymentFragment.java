package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import com.bynmix.app.BuildConfig;
import com.bynmix.app.R;
import com.bynmix.app.adapter.SpinnerAdapter;
import com.bynmix.app.databinding.AddPaymentFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.SelectExistingAddressListener;
import com.bynmix.app.models.Address;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.CardDetail;
import com.bynmix.app.models.MyCard;
import com.bynmix.app.models.States;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AddPaymentFragment extends BaseFragment implements SelectExistingAddressListener {
    private AddPaymentFragmentBinding mBinding;
    private Context mContext;
    private ProfileListener mCallback;
    private Address selectedAddress;
    private SpinnerAdapter spinnerAdapter;
    private List<States> mStateList = new ArrayList<>();
    private List<Address> mAddressList = new ArrayList<>();
    private States states = new States();
    private MyCard myCard = new MyCard();
    private CardDetail cardDetail;
    private int cardId;
    private int type;
    private DialogClickListener mListener;

    public static AddPaymentFragment newInstance() {

        return new AddPaymentFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (ProfileListener) context;
        mContext = context;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = AddPaymentFragmentBinding.inflate(inflater, container, false);
        if (cardId == 0) {
            mBinding.toolbarHeading.setText(R.string.add_a_new_payment_optiion);
            mBinding.buttonText.setText(R.string.add_payment);
        } else {
            mBinding.toolbarHeading.setText(R.string.edit_payment_optiion);
            mBinding.buttonText.setText(R.string.update_payment);
        }
        if (mStateList.size() == 0) {
            mBinding.mainLayout.setVisibility(View.GONE);
            getStatesList();
        } else {
            mBinding.mainLayout.setVisibility(View.VISIBLE);
        }
        if (mAddressList.size() == 0) {
            getMyAddress();
        } else {
            mBinding.selectExistingAddressLayout.setVisibility(View.VISIBLE);
        }
        updateUI();
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.hideKeyboard(mContext);
                mCallback.onBackPressed();
            }
        });
        mBinding.creditCardNumber.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable editable) {
                // TODO Auto-generated method stub

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String field = s.toString();
                int currCount = field.length();

                if (shouldIncrementOrDecrement(currCount, true)) {
                    appendOrStrip(field, true);
                } else if (shouldIncrementOrDecrement(currCount, false)) {
                    appendOrStrip(field, false);
                }
                prevCount = mBinding.creditCardNumber.getText().toString().length();

                Card card = new Card(field.replace(" ", ""), 0, 0, "");
                String brand = card.getBrand();
                setCardImage(brand);
            }
        });


        mBinding.selectExistingAddressLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openSelectExistingAddressFragment(AddPaymentFragment.this, mAddressList, false);
            }
        });
        mBinding.paymentCheckBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myCard.setDefault(!myCard.isDefault());
                updateUI();
            }
        });
        spinnerAdapter = new SpinnerAdapter(mContext, R.layout.profile_spinner_item, mStateList);
        mBinding.state.setAdapter(spinnerAdapter);
        mBinding.addPaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nameOnCard = mBinding.nameOnCard.getText().toString();
                String cardNumber = mBinding.creditCardNumber.getText().toString();
                String cardExpMonth = mBinding.month.getText().toString();
                String cardExpYear = mBinding.year.getText().toString();
                String cvc = mBinding.cvv.getText().toString().trim();
                String fullName = mBinding.fullName.getText().toString().trim();
                String addressOne = mBinding.addressOne.getText().toString().trim();
                String addressTwo = mBinding.addressTwo.getText().toString().trim();
                int selectedId = mBinding.state.getSelectedItemPosition();
                String state = mStateList.get(selectedId).getName();
                String stateAbbreviation = mStateList.get(selectedId).getAbbreviation();
                String city = mBinding.city.getText().toString().trim();
                String zipcode = mBinding.zipcode.getText().toString().trim();

                if (TextUtils.isEmpty(nameOnCard)) {
                    mBinding.nameOnCard.setError("Enter Name on card");
                    mBinding.nameOnCard.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(cardNumber)) {
                    mBinding.creditCardNumber.setError("Enter card number");
                    mBinding.creditCardNumber.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(cardExpMonth)) {
                    mBinding.month.setError("Enter Expiry month");
                    mBinding.month.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(cardExpYear)) {
                    mBinding.year.setError("Enter Expiry year");
                    mBinding.year.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(cvc)) {
                    mBinding.cvv.setError("Enter card cvc");
                    mBinding.cvv.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(fullName)) {
                    mBinding.fullName.setError(getResources().getString(R.string.enter_full_name));
                    mBinding.fullName.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(addressOne)) {
                    mBinding.addressOne.setError(getResources().getString(R.string.enter_address_one));
                    mBinding.addressOne.requestFocus();
                    return;
                }

                if (TextUtils.isEmpty(city)) {
                    mBinding.city.setError(getResources().getString(R.string.city_error));
                    mBinding.city.requestFocus();
                    return;
                }

                if (state.equalsIgnoreCase("Select a State")) {
                    Toast.makeText(mContext, "Please Select a State.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(zipcode)) {
                    mBinding.zipcode.setError(getResources().getString(R.string.enter_zipcode));
                    mBinding.zipcode.requestFocus();
                    return;
                }
                if (selectedAddress != null) {
                    if (selectedAddress.getFullName().equalsIgnoreCase(fullName) && selectedAddress.getAddressOne().equalsIgnoreCase(addressOne) && selectedAddress.getAddressTwo().equalsIgnoreCase(addressTwo) && selectedAddress.getStateAbbreviation().equalsIgnoreCase(stateAbbreviation) && selectedAddress.getCity().equalsIgnoreCase(city) && selectedAddress.getZipCode().equalsIgnoreCase(zipcode)) {
                        myCard.setSettingAddressId(selectedAddress.getAddressId());
                        myCard.setNewAddress(false);
                    } else {
                        myCard.setName(fullName);
                        myCard.setAddressLine1(addressOne);
                        myCard.setAddressLine2(addressTwo);
                        myCard.setAddressCountry("US");
                        myCard.setAddressState(state);
                        myCard.setAddressCity(city);
                        myCard.setAddressZipcode(zipcode);
                        myCard.setNewAddress(true);
                    }
                } else {
                    myCard.setName(fullName);
                    myCard.setAddressLine1(addressOne);
                    myCard.setAddressLine2(addressTwo);
                    myCard.setAddressCountry("US");
                    myCard.setAddressState(state);
                    myCard.setAddressCity(city);
                    myCard.setAddressZipcode(zipcode);
                    myCard.setNewAddress(true);
                }

                if (cardDetail != null) {
                    myCard.setNameOnCard(nameOnCard);
                    myCard.setExpirationMonth(Integer.parseInt(cardExpMonth));
                    myCard.setExpirationYear(Integer.parseInt(cardExpYear));
                    myCard.setSourceToken(cardDetail.getStripeCardId());
                    editPayment();
                } else {
                    Card card = new Card(cardNumber, Integer.parseInt(cardExpMonth), Integer.parseInt(cardExpYear), cvc);
                    card.setName(nameOnCard);
                    if (!card.validateCard()) {
                        Toast.makeText(mContext,
                                "Invalid Card",
                                Toast.LENGTH_LONG
                        ).show();
                    } else {
                        mBinding.progressBar.setVisibility(View.VISIBLE);
                        Stripe stripe = new Stripe(mContext, BuildConfig.STRIPE_PUBLISHABLE_KEY);
                        stripe.createToken(card, new TokenCallback() {
                            public void onSuccess(Token token) {
                                mBinding.progressBar.setVisibility(View.GONE);
                                myCard.setSourceToken(token.getId());
                                addPayment();
                            }

                            public void onError(Exception error) {
                                mBinding.progressBar.setVisibility(View.GONE);
                                Toast.makeText(mContext,
                                        error.getLocalizedMessage(),
                                        Toast.LENGTH_LONG
                                ).show();
                            }
                        });
                    }
                }
            }
        });


        mBinding.state.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @SuppressWarnings("deprecation")
                    @Override
                    public void onGlobalLayout() {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            mBinding.state.getViewTreeObserver()
                                    .removeOnGlobalLayoutListener(this);
                        } else {
                            mBinding.state.getViewTreeObserver()
                                    .removeGlobalOnLayoutListener(this);
                        }
                        if (selectedAddress != null) {
                            if (mStateList.size() > 0) {
                                setData();
                            }
                        }
                    }
                });


        return mBinding.getRoot();
    }

    private void editPayment() {
        showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.editPayment("Bearer " + getAccessToken(), "" + cardDetail.getCardId(), myCard).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    hideProgress();
                    mCallback.onBackPressed();
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            editPayment();
                        }

                        @Override
                        public void errorAccessToken() {
                            hideProgress();
                        }
                    });
                } else {
                    hideProgress();
                    try {
                        JSONObject errorObject = new JSONObject(response.errorBody().string().trim());
                        JSONArray error = errorObject.getJSONArray("errors");
                        JSONObject messageObject = error.getJSONObject(0);
                        String message = messageObject.getString("message");
                        showAlertDialog(message);
                    } catch (Exception ex) {
                        showAlertDialog(getString(R.string.error_server));
                    }
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    private void addPayment() {
        showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.addPayment("Bearer " + getAccessToken(), myCard).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    if (type == 1) {
                        mListener.onClick();
                    } else {
                        SellerAccountSetupFragment.isCallPaymentAPI = true;
                    }
                    mCallback.onBackPressed();
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            addPayment();
                        }

                        @Override
                        public void errorAccessToken() {
                            hideProgress();
                        }
                    });
                } else {
                    try {
                        JSONObject errorObject = new JSONObject(response.errorBody().string().trim());
                        JSONArray error = errorObject.getJSONArray("errors");
                        JSONObject messageObject = error.getJSONObject(0);
                        String message = messageObject.getString("message");
                        showAlertDialog(message);
                    } catch (Exception ex) {
                        showAlertDialog(getString(R.string.error_server));
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    private void setCardImage(String brand) {
        if (brand == null || brand.equalsIgnoreCase("Unknown")) {
            mBinding.creditCardImage.setVisibility(View.GONE);
        } else {
            mBinding.creditCardImage.setVisibility(View.VISIBLE);
            if (brand.equalsIgnoreCase(Constants.VISA)) {
                mBinding.creditCardImage.setImageResource(R.mipmap.card_visa);
            } else if (brand.equalsIgnoreCase(Constants.MASTERCARD)) {
                mBinding.creditCardImage.setImageResource(R.mipmap.card_master);
            } else if (brand.equalsIgnoreCase(Constants.AMERICAN_EXPRESS)) {
                mBinding.creditCardImage.setImageResource(R.mipmap.card_american_express);
            } else if (brand.equalsIgnoreCase(Constants.DINERS_CLUB)) {
                mBinding.creditCardImage.setImageResource(R.mipmap.card_diners_club);
            } else if (brand.equalsIgnoreCase(Constants.JCB)) {
                mBinding.creditCardImage.setImageResource(R.mipmap.card_jcb);
            } else if (brand.equalsIgnoreCase(Constants.DISCOVER)) {
                mBinding.creditCardImage.setImageResource(R.mipmap.card_discover);
            }
        }
    }


    private int prevCount = 0;

    private boolean isAtSpaceDelimiter(int currCount) {
        return currCount == 4 || currCount == 9 || currCount == 14;
    }

    private boolean shouldIncrementOrDecrement(int currCount, boolean shouldIncrement) {
        if (shouldIncrement) {
            return prevCount <= currCount && isAtSpaceDelimiter(currCount);
        } else {
            return prevCount > currCount && isAtSpaceDelimiter(currCount);
        }
    }

    private void appendOrStrip(String field, boolean shouldAppend) {
        StringBuilder sb = new StringBuilder(field);
        if (shouldAppend) {
            sb.append(" ");
        } else {
            sb.setLength(sb.length() - 1);
        }
        mBinding.creditCardNumber.setText(sb.toString());
        mBinding.creditCardNumber.setSelection(sb.length());
    }

    private void setMainData() {
        mBinding.nameOnCard.setText(cardDetail.getNameOnCard());
        mBinding.creditCardNumber.setText(getString(R.string.star) + " " + cardDetail.getLastFour());
        mBinding.month.setText("" + cardDetail.getExpirationMonth());
        mBinding.year.setText("" + cardDetail.getExpirationYear());
        mBinding.cvv.setText("XXX");
        mBinding.fullName.setText(cardDetail.getFullName());
        mBinding.addressOne.setText(cardDetail.getAddressLineOne());
        mBinding.addressTwo.setText(cardDetail.getAddressLineTwo());
        mBinding.city.setText(cardDetail.getCity());
        mBinding.zipcode.setText(cardDetail.getZipcode());
        setCardImage(cardDetail.getBrand());
        myCard.setDefault(cardDetail.isDefault());
        updateUI();
    }

    private void updateUI() {
        if (myCard.isDefault()) {
            mBinding.paymentCheckBox.setImageResource(R.mipmap.checkbox_checked);
        } else {
            mBinding.paymentCheckBox.setImageResource(R.mipmap.checkbox);
        }
    }

    @Override
    public void selectedAddress(Address address) {
        this.selectedAddress = address;
    }

    private void setData() {
        mBinding.fullName.setText(selectedAddress.getFullName());
        mBinding.addressOne.setText(selectedAddress.getAddressOne());
        mBinding.addressTwo.setText(selectedAddress.getAddressTwo());
        mBinding.city.setText(selectedAddress.getCity());
        mBinding.zipcode.setText(selectedAddress.getZipCode());
        if (selectedAddress.getState() != null) {
            if (mStateList.get(0).getName().equalsIgnoreCase("Select a State")) {
                mStateList.remove(0);
                spinnerAdapter.notifyDataSetChanged();
            }
            for (int i = 0; i < mStateList.size(); i++) {
                if (selectedAddress.getStateAbbreviation().equals(mStateList.get(i).getAbbreviation())) {
                    mBinding.state.setSelection(i);
                    break;
                }
            }
        }
    }

    private void disableCardFields() {
        mBinding.creditCardNumber.setEnabled(false);
        mBinding.cvv.setEnabled(false);
        mBinding.creditCardNumber.setBackgroundResource(R.color.very_light_grey_color);
        mBinding.cardNumberLayout.setBackgroundResource(R.color.very_light_grey_color);
        mBinding.cvv.setBackgroundResource(R.color.very_light_grey_color);
        mBinding.cvvLayout.setBackgroundResource(R.color.very_light_grey_color);
    }

    public void updateList(List<States> state) {
        this.mStateList = state;

        if (cardDetail != null) {
            mBinding.mainLayout.setVisibility(View.VISIBLE);
            disableCardFields();
            myCard.setDefault(cardDetail.isDefault());
            setAddressList();
        } else {
            if (cardId != 0) {
                getMyWalletCard(cardId);
                disableCardFields();
            } else {
                setAddressList();
                mBinding.mainLayout.setVisibility(View.VISIBLE);
                mBinding.creditCardNumber.setEnabled(true);
                mBinding.cvv.setEnabled(true);
                mBinding.creditCardNumber.setBackgroundResource(R.color.white);
                mBinding.cvv.setBackgroundResource(R.color.white);
                mBinding.cardNumberLayout.setBackgroundResource(R.color.white);
                mBinding.cvvLayout.setBackgroundResource(R.color.white);
            }

        }
    }

    private void setAddressList() {
        if (selectedAddress != null && selectedAddress.getStateAbbreviation() != null) {
            for (int i = 0; i < mStateList.size(); i++) {
                if (selectedAddress.getStateAbbreviation().equalsIgnoreCase(mStateList.get(i).getAbbreviation())) {
                    states.setName(mStateList.get(i).getName());
                    states.setAbbreviation(selectedAddress.getStateAbbreviation());
                    mStateList.remove(i);
                    mStateList.add(0, states);
                    break;
                }
            }
        } else {
            states.setName("Select a State");
            mStateList.add(0, states);
        }
        spinnerAdapter.setList(mStateList);
    }

    public void updateAddressList(List<Address> addresses) {
        this.mAddressList = addresses;
        String enterAddressText = "enter A New Address";
        if (mAddressList.size() > 0) {
            mBinding.selectExistingAddressLayout.setVisibility(View.VISIBLE);
            enterAddressText = "or " + enterAddressText;
        } else {
            mBinding.selectExistingAddressLayout.setVisibility(View.GONE);
        }
        mBinding.enterAddressText.setText(enterAddressText);
    }

    public void setCardDetail(CardDetail cardDetail) {
        mBinding.mainLayout.setVisibility(View.VISIBLE);
        this.cardDetail = cardDetail;
        setSelectedAddress();
        setAddressList();
        setMainData();
    }

    public void setSelectedAddress() {
        Address address = new Address();
        address.setZipCode(cardDetail.getZipcode());
        address.setCity(cardDetail.getCity());
        address.setAddressOne(cardDetail.getAddressLineOne());
        address.setAddressTwo(cardDetail.getAddressLineTwo());
        address.setAddressId(cardDetail.getAddressId());
        address.setFullName(cardDetail.getFullName());
        address.setStateAbbreviation(cardDetail.getState());
        address.setCountry(cardDetail.getCountry());
        this.selectedAddress = address;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }


    public void getMyWalletCard(final int cardId) {
        showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getMyWalletCard("Bearer " + getAccessToken(), "" + cardId).enqueue(new Callback<ApiResponse<CardDetail>>() {
            @Override
            public void onResponse(Call<ApiResponse<CardDetail>> call, Response<ApiResponse<CardDetail>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    hideProgress();
                    CardDetail cardDetail = response.body().getData();
                    setCardDetail(cardDetail);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getMyWalletCard(cardId);
                        }

                        @Override
                        public void errorAccessToken() {
                            hideProgress();
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<CardDetail>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    public void setListener(DialogClickListener mListener) {
        this.mListener = mListener;
    }

    public void setType(int type) {
        this.type = type;
    }
}