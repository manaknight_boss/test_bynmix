package com.bynmix.app.fragments;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.adapter.AddTagsAdapter;
import com.bynmix.app.databinding.FragmentAddPostDetailBinding;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.CustomTextDialog;
import com.bynmix.app.utils.PostBlogTypeError;
import com.bynmix.app.utils.Utils;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AddPostDetailFragment extends Fragment {
    private FragmentAddPostDetailBinding mBinding;
    private CreatePostListener mCallback;
    private Context mContext;
    private AddTagsAdapter mAdapter;
    private List<String> mTagsList = new ArrayList<>();
    private List<String> postTypeList = new ArrayList<>();
    private FeedResponse post;
    private CustomTextDialog mAddPostErrorDialog;
    private PostBlogTypeError mPostBlogTypeError;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (CreatePostListener) context;
        mContext = context;
    }

    /**
     * AddPostDetail Fragment
     *
     * @return AddPostDetail Fragment
     */
    public static AddPostDetailFragment newInstance() {
        return new AddPostDetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentAddPostDetailBinding.inflate(inflater, container, false);
        mAddPostErrorDialog = new CustomTextDialog();
        mPostBlogTypeError = new PostBlogTypeError();
        post = new FeedResponse();
        addPostType();
        init();
        return mBinding.getRoot();
    }

    private void addPostType() {
        postTypeList.add(mContext.getString(R.string.select_post_type));
        postTypeList.add(getString(R.string.blog_post));
        postTypeList.add(getString(R.string.video_post));
    }

    private void init() {
        Utils.setCapitalizeTextWatcher(mBinding.postTitle);
        Utils.setCapitalizeTextWatcher(mBinding.shortDescription);
        setAdapter();
        ArrayAdapter<String> postTypeAdapter = new ArrayAdapter<String>(mContext, R.layout.profile_spinner_item, postTypeList);
        mBinding.postType.setAdapter(postTypeAdapter);
        mBinding.postType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (view != null) {
                    if (position == 0) {
                        ((TextView) view).setTextColor(mContext.getResources().getColor(R.color.lightGrey));
                    } else {
                        ((TextView) view).setTextColor(mContext.getResources().getColor(R.color.category_dialog_color));
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mBinding.create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setData();
            }
        });

        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
    }

    private void setAdapter() {
        mAdapter = new AddTagsAdapter(mContext, mTagsList);
        mBinding.recycleView.setLayoutManager(new FlowLayoutManager());
        mBinding.recycleView.setAdapter(mAdapter);
        mBinding.recycleView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.set(15, 15, 15, 15);

            }
        });

    }

    private void setData() {
        String postTitle, postLink, shortDescription;
        postTitle = mBinding.postTitle.getText().toString().trim();
        int postType = mBinding.postType.getSelectedItemPosition();
        postLink = mBinding.postLink.getText().toString().trim();
        shortDescription = mBinding.shortDescription.getText().toString().trim();

        if (TextUtils.isEmpty(postTitle)) {
            String message = Utils.getColorText(getString(R.string.post_title_text), mContext.getResources().getColor(R.color.very_light_purple));
            message = "A " + message + " " + getString(R.string.required);
            mAddPostErrorDialog.showDialog(mContext, message);
            return;
        }

        if (postType == 0) {
            String message = Utils.getColorText(getString(R.string.post_type_text), mContext.getResources().getColor(R.color.very_light_purple));
            message = "A " + message + " " + getString(R.string.required);
            mAddPostErrorDialog.showDialog(mContext, message);
            return;
        }

        if (TextUtils.isEmpty(postLink)) {
            String message = Utils.getColorText(getString(R.string.post_link_text), mContext.getResources().getColor(R.color.very_light_purple));
            message = "A " + message + " " + getString(R.string.required);
            mAddPostErrorDialog.showDialog(mContext, message);
            return;
        }

        if (!Utils.containsURL(postLink)) {
            String message = Utils.getColorText(getString(R.string.post_link_text), mContext.getResources().getColor(R.color.very_light_purple));
            message = message + " " + getString(R.string.start_with_http);
            mAddPostErrorDialog.showDialog(mContext, message);
            return;
        }

        if (TextUtils.isEmpty(shortDescription)) {
            String message = Utils.getColorText(getString(R.string.short_description_text), mContext.getResources().getColor(R.color.very_light_purple));
            message = message + " " + getString(R.string.required);
            mAddPostErrorDialog.showDialog(mContext, message);
            return;
        }

        if (mTagsList.size() == 0) {
            String message = Utils.getColorText(getString(R.string.tag), mContext.getResources().getColor(R.color.very_light_purple));
            message = message + " " + getString(R.string.required);
            mAddPostErrorDialog.showDialog(mContext, message);
            return;
        }

        if (!postLink.startsWith("http")) {
            postLink = "http://" + postLink;
        }

        post.setTitle(postTitle);
        post.setShortDescription(shortDescription);
        post.setPostType("" + postType);
        post.setLink(postLink);
        post.setTags(mTagsList);

        sendData(post);

    }

    private void sendData(FeedResponse post) {

        boolean isYouTubeLink = extractYouTubeUrlId(post.getLink());
        int postType = Integer.parseInt(post.getPostType());
        if (isYouTubeLink && postType == Constants.WEB_LINK) {
            postLinkError(postType);

        } else if (!isYouTubeLink && postType == Constants.YOU_TUBE_LINK) {

            postLinkError(postType);
        } else {

            mCallback.sendPostDetail(post);
        }

    }

    private void postLinkError(int type) {
        final String postType, description, confirm, cancel;
        if (type == Constants.WEB_LINK) {
            postType = getString(R.string.blog_post);
            description = getString(R.string.non_video_error);
            confirm = getString(R.string.non_video_confirm);
            cancel = getString(R.string.non_video_cancel);

        } else {
            postType = getString(R.string.video_post);
            description = getString(R.string.video_error);
            confirm = getString(R.string.video_confirm);
            cancel = getString(R.string.video_cancel);
        }
        mPostBlogTypeError.showDialog(mContext, mCallback, postType, description, confirm, cancel, post, "", "");
    }

    private boolean extractYouTubeUrlId(String youtubeUrl) {
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|\\/e\\/|watch\\?v%3D|watch\\?feature=player_embedded&v=|%2Fvideos%2F|embed%\u200C\u200B2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(youtubeUrl);
        if (matcher.find()) {
            return true;
        }
        return false;
    }

}