package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.databinding.AddPostFragmentBinding;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.utils.Constants;

public class AddPostFragment extends Fragment {
    private AddPostFragmentBinding mBinding;
    private CreatePostListener mCallback;
    private Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (CreatePostListener) context;
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = AddPostFragmentBinding.inflate(inflater, container, false);
        init();
        return mBinding.getRoot();
    }

    private void init() {
        mBinding.addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openGalleryFor(Constants.CREATE_POST, 1);
            }
        });
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
        mCallback.setAnnotatedPoints(null);
    }

    public static AddPostFragment newInstance() {
        return new AddPostFragment();
    }

}
