package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.adapter.FeedViewPagerAdapter;
import com.bynmix.app.databinding.AlertFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.NotificationListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.models.Notification;
import com.bynmix.app.models.Settings;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.sharedpreference.SharedPreferenceUtility;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlertFragment extends BaseFragment implements NotificationListener {

    private AlertFragmentBinding mBinding;
    private ProfileListener mCallback;
    private Context mContext;
    private FeedViewPagerAdapter feedViewPagerAdapter;
    private AllNotificationFragment allNotificationFragment;
    private NotificationOffersFragment offersNotificationFragment;
    private Settings settings = new Settings();
    private SharedPreferenceUtility mSharedPreferenceUtility;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (ProfileListener) context;
        mContext = context;
    }

    public static AlertFragment newInstance() {
        return new AlertFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = AlertFragmentBinding.inflate(inflater, container, false);
        mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);
        setViewPager();
        if (mSharedPreferenceUtility.getOfferNotification()) {
            mBinding.viewpager.setCurrentItem(1);
        }
        return mBinding.getRoot();
    }

    private void setViewPager() {
        if (feedViewPagerAdapter == null) {
            feedViewPagerAdapter = new FeedViewPagerAdapter(getChildFragmentManager());
            allNotificationFragment = new AllNotificationFragment();
            offersNotificationFragment = new NotificationOffersFragment();
            feedViewPagerAdapter.addFragment(allNotificationFragment, getResources().getString(R.string.all));
            feedViewPagerAdapter.addFragment(offersNotificationFragment, getResources().getString(R.string.offers));
            allNotificationFragment.setListener(this);
            offersNotificationFragment.setListener(this);
        }
        mBinding.viewpager.setAdapter(feedViewPagerAdapter);
        mBinding.viewpager.setOffscreenPageLimit(2);
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);

        mBinding.viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    allNotificationFragment.callList();
                } else if (position == 1) {
                    if (mSharedPreferenceUtility.getOfferNotification()) {
                        return;
                    }
                    offersNotificationFragment.callList();
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void updateOffersNotificationList(List<Notification> mList, int currentPage, int totalPage, int type) {
        updateList(mList, 1);
        offersNotificationFragment.setList(mList, currentPage, totalPage, type);
    }

    public void updateAllNotificationList(List<Notification> mList, int currentPage, int totalPage) {
        Log.d("msg","notification");
        updateList(mList, 0);
        allNotificationFragment.setList(mList, currentPage, totalPage);
    }

    public void updateList(List<Notification> mList, int type) {
        List<Integer> unreadList = new ArrayList<>();
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).isUnread()) {
                unreadList.add(mList.get(i).getNotificationId());
            }
        }
        if (unreadList.size() > 0) {
            settings.setType(type);
            settings.setNotificationIds(unreadList);
            decreaseCount(settings);
        }
    }

    public void refreshNotification(int type) {
        offersNotificationFragment.refreshNotifications(type);
    }


    public void setListingDetail(ListingDetail listingDetail) {
        if (listingDetail.isActive() && !listingDetail.isDeleted()) {
            mCallback.openListingDetailFragment(listingDetail.getId());
        } else {
            ((HomeActivity) mContext).mAlertDialog.showDialog((HomeActivity) mContext, mContext.getResources().getString(R.string.listing_not_longer_avialable));
        }
    }

    @Override
    public void decreaseCount(final Settings settings) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.decreaseCount("Bearer " + getAccessToken(), settings).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mCallback.getNotificationCount();
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            decreaseCount(settings);
                        }

                        @Override
                        public void errorAccessToken() {
                            hideProgress();
                        }
                    });
                } else {
                    Log.d("msg","type : "+settings.getType()+","+settings.getNotificationIds().get(0));
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));

            }
        });
    }

    @Override
    public void getNotifications(final int currentpage) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getNotifications("Bearer " + getAccessToken(), currentpage + 1, "DESC").enqueue(new Callback<ApiResponse<Settings>>() {
            @Override
            public void onResponse(Call<ApiResponse<Settings>> call, Response<ApiResponse<Settings>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Settings settings = response.body().getData();
                    int currentPage = settings.getPageNumber();
                    int totalPage = settings.getTotalPages();
                    List<Notification> mList = settings.getItems();
                    updateAllNotificationList(mList, currentPage, totalPage);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getNotifications(currentpage);
                        }

                        @Override
                        public void errorAccessToken() {

                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Settings>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void getMyOffersNotifications(final int currentPageNumber) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getMyOffersNotifications("Bearer " + getAccessToken(), currentPageNumber + 1, "DESC").enqueue(new Callback<ApiResponse<Settings>>() {
            @Override
            public void onResponse(Call<ApiResponse<Settings>> call, Response<ApiResponse<Settings>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Settings settings = response.body().getData();
                    int currentPage = settings.getPageNumber();
                    int totalPage = settings.getTotalPages();
                    List<Notification> mList = settings.getItems();
                    updateOffersNotificationList(mList, currentPage, totalPage, Constants.MY_OFFERS);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getMyOffersNotifications(currentPageNumber);
                        }

                        @Override
                        public void errorAccessToken() {

                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Settings>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void getReceivedOffersNotifications(final int currentPageNumber) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getReceivedOffersNotifications("Bearer " + getAccessToken(), currentPageNumber + 1, "DESC").enqueue(new Callback<ApiResponse<Settings>>() {
            @Override
            public void onResponse(Call<ApiResponse<Settings>> call, Response<ApiResponse<Settings>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Settings settings = response.body().getData();
                    int currentPage = settings.getPageNumber();
                    int totalPage = settings.getTotalPages();
                    List<Notification> mList = settings.getItems();
                    updateOffersNotificationList(mList, currentPage, totalPage, Constants.RECEIVED_OFFERS);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getReceivedOffersNotifications(currentPageNumber);
                        }

                        @Override
                        public void errorAccessToken() {

                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Settings>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }
}
