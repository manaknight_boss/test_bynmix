package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.adapter.FeedAdapter;
import com.bynmix.app.databinding.PostFragmentBinding;
import com.bynmix.app.interfaces.FeedItemClickListener;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class AllFragment extends BaseFragment {

    private PostFragmentBinding mBinding;
    private FeedAdapter adapter;
    private Context mContext;
    private List<FeedResponse> mAllFeedList = new ArrayList<>();
    private FeedItemClickListener mCallback;
    private String searchPattern = "";
    private String type;
    private String oldSearchText;
    private boolean isAlreadyCalled;
    private int currentPageNumber;
    private int totalPages;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (FeedItemClickListener) context;
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = PostFragmentBinding.inflate(inflater, container, false);
        isAlreadyCalled = false;
        adapter = new FeedAdapter(mContext, mAllFeedList, Constants.AlL_FEEDS, mCallback);
        mBinding.recycleView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recycleView.setAdapter(adapter);
        if (mAllFeedList.size() == 0) {
            currentPageNumber = 0;
            fetchAllFeeds();
        }
        mBinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                {

                    if (mCallback.conditionsForApiCall()) {
                        currentPageNumber = 0;
                        bestUser(Constants.AlL_FEEDS);
                        fetchAllFeeds();
                    } else {
                        hideSwipeRefresh();
                    }
                }
            }
        });

        mBinding.recycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();
                boolean endHasBeenReached = lastVisible >= totalItemCount - 4;
                if (totalItemCount > 0 && endHasBeenReached) {
                    if (!isAlreadyCalled && currentPageNumber < totalPages && mAllFeedList.size() > 0) {
                        isAlreadyCalled = true;
                        fetchAllFeeds();
                    }
                }

            }
        });

        return mBinding.getRoot();
    }

    private void hideSwipeRefresh() {
        mBinding.swipeRefreshLayout.setRefreshing(false);
    }

    private void fetchAllFeeds() {
        setLoadMoreProgress();
        fetchFeeds(searchPattern, type, currentPageNumber);
        oldSearchText = searchPattern;
    }

    public void setFeedList(List<FeedResponse> allFeeds, int currentPageNumber, int totalPages) {
        this.currentPageNumber = currentPageNumber;
        this.totalPages = totalPages;
        if (currentPageNumber > 1) {
            this.mAllFeedList.addAll(allFeeds);
        } else {
            this.mAllFeedList = allFeeds;
        }
        isAlreadyCalled = false;
        setLoadMoreProgress();
        adapter.setList(mAllFeedList);
        mBinding.nothingToShowPopUp.setVisibility(View.GONE);
        hideSwipeRefresh();
    }

    public void setError(int error) {
        if (mAllFeedList.size() == 0) {
            if (error == 404) {
                mBinding.logo.setImageResource(R.mipmap.logo_black);
                mBinding.errorText.setText(Html.fromHtml(getString(R.string.feed_error)), TextView.BufferType.SPANNABLE);
            } else {
                mBinding.errorText.setText(Html.fromHtml(getString(R.string.sad_cloud_error)), TextView.BufferType.SPANNABLE);
                mBinding.logo.setImageResource(R.mipmap.cloud_sad);
            }
            mBinding.nothingToShowPopUp.setVisibility(View.VISIBLE);
        }
        mBinding.nothingToShowPopUp.setVisibility(View.VISIBLE);
        mCallback.getList().clear();
        currentPageNumber = 0;
        adapter.setList(mCallback.getList());
        hideSwipeRefresh();
        mBinding.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    public void updateFeeds(FeedResponse feeds) {
        if (mAllFeedList != null && mAllFeedList.size() > 0) {
            for (int i = 0; i < mAllFeedList.size(); i++) {
                FeedResponse tempFeed = mAllFeedList.get(i);
                if (tempFeed.getId() == feeds.getId() && tempFeed.getType().equalsIgnoreCase(feeds.getType())) {
                    tempFeed.setLikesCount(feeds.getLikesCount());
                    tempFeed.setUserLiked(feeds.isUserLiked());
                    mAllFeedList.set(i, tempFeed);
                    break;
                }
            }

            adapter.setList(mAllFeedList);
        }
    }

    public void callList() {
        mBinding.nothingToShowPopUp.setVisibility(View.GONE);
        if (mCallback != null && mCallback.getList().size() == 0 || !oldSearchText.equals(searchPattern)) {
            currentPageNumber = 0;
            fetchAllFeeds();
        }
    }

    public void setSearchPattern(String searchText) {
        this.searchPattern = searchText;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void clearList() {
        mAllFeedList.clear();
    }

    private void setLoadMoreProgress() {
        if (mAllFeedList.size() == 0) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
        } else {
            mBinding.progressBar.setVisibility(View.GONE);
        }
        if (isAlreadyCalled) {
            adapter.setProgress(true);
        } else {
            adapter.setProgress(false);
        }
    }

    public void moveToTop() {
        LinearLayoutManager layoutManager = (LinearLayoutManager) mBinding.recycleView.getLayoutManager();
        int lastVisible = layoutManager.findLastVisibleItemPosition();
        if (lastVisible > Constants.FEEDS_SCROLL_TO_TOP_SMOOTHLY_AFTER_POINT) {
            mBinding.recycleView.scrollToPosition(Constants.FEEDS_SCROLL_TO_TOP_SMOOTHLY_AFTER_POINT);
        }
        mBinding.recycleView.smoothScrollToPosition(0);
    }

    public void setPostDotsTags(int position, List<AnnotatedPoint> mList) {
        mAllFeedList.get(position).setPostDots(mList);
        adapter.setList(mAllFeedList);
        adapter.hideProgress();
        adapter.notifyDataSetChanged();

    }
}