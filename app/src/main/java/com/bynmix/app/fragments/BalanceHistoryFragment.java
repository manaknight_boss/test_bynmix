package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.BalanceHistoryAdapter;
import com.bynmix.app.databinding.FragmentBalanceHistoryBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.BalanceHistory;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BalanceHistoryFragment extends BaseFragment {
    private FragmentBalanceHistoryBinding mBinding;
    private ProfileListener mCallBack;
    private Context mContext;
    private List<BalanceHistory> mList = new ArrayList<>();
    private BalanceHistoryAdapter mAdapter;
    private int currentPageNumber;
    private int totalPages;
    private boolean isAlreadyCalled;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallBack = (ProfileListener) mContext;
    }

    public static BalanceHistoryFragment newInstance() {
        return new BalanceHistoryFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentBalanceHistoryBinding.inflate(inflater, container, false);
        isAlreadyCalled = false;
        setAdapter();
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.onBackPressed();
            }
        });
        if (mList.size() == 0) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
            currentPageNumber = 0;
            getHistory();
        }
        return mBinding.getRoot();
    }

    private void setAdapter() {
        mAdapter = new BalanceHistoryAdapter(mContext, mList);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(mAdapter);

        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();
                boolean endHasBeenReached = lastVisible >= totalItemCount - 4;
                if (totalItemCount > 0 && endHasBeenReached) {
                    if (!isAlreadyCalled && currentPageNumber < totalPages && mList.size() > 0) {
                        isAlreadyCalled = true;
                        getHistory();
                        setLoadMoreProgress();
                    }
                }
            }
        });
    }

    private void setLoadMoreProgress() {
        if (isAlreadyCalled) {
            mAdapter.setProgress(true);
        } else {
            mAdapter.setProgress(false);
        }
    }

    public void getHistory() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getBalanceHistory("Bearer " + getAccessToken(), "DESC", currentPageNumber + 1).enqueue(new Callback<ApiResponse<BalanceHistory>>() {
            @Override
            public void onResponse(Call<ApiResponse<BalanceHistory>> call, Response<ApiResponse<BalanceHistory>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    BalanceHistory balanceHistory = response.body().getData();
                    currentPageNumber = balanceHistory.getPageNumber();
                    totalPages = balanceHistory.getTotalPages();
                    List<BalanceHistory> list = balanceHistory.getItems();
                    if (currentPageNumber > 1) {
                        mList.addAll(list);
                    } else {
                        mList = list;
                    }
                    if (mList.size() == 0) {
                        mBinding.nothingToShowPopUp.setVisibility(View.VISIBLE);
                    } else {
                        mBinding.nothingToShowPopUp.setVisibility(View.GONE);
                        mBinding.recyclerView.setVisibility(View.VISIBLE);
                        mAdapter.setList(mList);
                    }
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getHistory();
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    mBinding.progressBar.setVisibility(View.GONE);
                    showAlertDialog(getResources().getString(R.string.error_server));
                }
                isAlreadyCalled = false;
                setLoadMoreProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<BalanceHistory>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

}
