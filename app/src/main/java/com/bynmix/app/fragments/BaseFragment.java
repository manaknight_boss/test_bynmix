package com.bynmix.app.fragments;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.models.Address;
import com.bynmix.app.models.ApiListResponse;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Bloggers;
import com.bynmix.app.models.BodyType;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.models.Feeds;
import com.bynmix.app.models.FilterData;
import com.bynmix.app.models.ShippingInfo;
import com.bynmix.app.models.States;
import com.bynmix.app.models.UserResponse;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseFragment extends Fragment {
    protected Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    void showProgress() {
        ((HomeActivity) mContext).mProgressBarDialog.showProgress();
    }

    void hideProgress() {
        ((HomeActivity) mContext).mProgressBarDialog.hideProgress();
    }


    String getAccessToken() {
        return ((HomeActivity) mContext).getAccessToken();
    }

    void showAlertDialog(String message) {
        ((HomeActivity) mContext).mAlertDialog.showDialog((HomeActivity) mContext, message);
    }

    public void getAccessTokenFromServer(AccessTokenResponse accessTokenResponse) {
        ((HomeActivity) mContext).getAccessTokenFromServer(accessTokenResponse);
    }


    public void fetchMyByn(final int userId, final int currentPageNumber) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        if (((HomeActivity) mContext).filterFragmentType != Constants.PROFILE_FILTER) {
            ((HomeActivity) mContext).filterData = new FilterData();
        }
        apiInstance.putMyBynFeeds("Bearer " + getAccessToken(), "" + userId, ((HomeActivity) mContext).filterData, "desc", currentPageNumber + 1).enqueue(new Callback<ApiResponse<Feeds>>() {
            @Override
            public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                setProfileResponse(response, userId, Constants.MY_BYN, currentPageNumber);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    private void setProfileResponse(Response<ApiResponse<Feeds>> response, final int userId, final int postType, final int currentPageNumber) {
        if (response.isSuccessful() && response.code() == 200) {
            setMyPostResponse(response, postType, currentPageNumber);
        } else if (response.code() == 401) {
            getAccessTokenFromServer(new AccessTokenResponse() {
                @Override
                public void successAccessToken() {
                    if (postType == Constants.MY_BYN) {
                        fetchMyByn(userId, currentPageNumber);
                    } else {
                        fetchMyPost(userId, currentPageNumber);
                    }
                }

                @Override
                public void errorAccessToken() {
                }
            });
        } else if (response.code() == 404) {
            setMyPostError(404, postType);

        } else {
            setMyPostError(400, postType);
        }
    }

    private void setMyPostResponse(Response<ApiResponse<Feeds>> response, int postType, int currentPageNumber) {
        Feeds feeds = response.body().getData();
        int currentPage = feeds.getPageNumber();
        int totalPage = feeds.getTotalPages();
        List<FeedResponse> myFeeds = feeds.getItems();
        Fragment fragment = ((HomeActivity) mContext).getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment != null && fragment instanceof ProfileFragment) {
            ProfileFragment profileFragment = (ProfileFragment) fragment;
            if (postType == Constants.MY_POST) {
                profileFragment.updateMyPostFeeds(myFeeds, currentPage, totalPage);
            } else {
                profileFragment.updateMyBynFeeds(myFeeds, currentPage, totalPage);
            }
        }

    }

    private void setMyPostError(int errorCode, int postType) {
        Fragment fragment = ((HomeActivity) mContext).getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment != null && fragment instanceof ProfileFragment) {
            ProfileFragment profileFragment = (ProfileFragment) fragment;
            if (errorCode == 404) {
                profileFragment.onItemFound(postType);
            } else {
                profileFragment.error(postType);
            }

        }
    }

    public void fetchMyPost(final int userId, final int currentPageNumber) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.putMyPostFeeds("Bearer " + getAccessToken(), "" + userId, ((HomeActivity) mContext).filterData, "desc", currentPageNumber + 1).enqueue(new Callback<ApiResponse<Feeds>>() {
            @Override
            public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                setProfileResponse(response, userId, Constants.MY_POST, currentPageNumber);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }


    public void fetchMyBynLikes(final int currentPageNumber) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        if (((HomeActivity) mContext).filterFragmentType != Constants.PROFILE_FILTER) {
            ((HomeActivity) mContext).filterData = new FilterData();
        }
        apiInstance.getMyBynLikes("Bearer " + getAccessToken(), ((HomeActivity) mContext).filterData, "likedDate", "DESC", currentPageNumber + 1).enqueue(new Callback<ApiResponse<Feeds>>() {
            @Override
            public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                setLikesResponse(response, Constants.MY_BYN, currentPageNumber, Constants.LIKE_FRAGMENT);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }


    public void fetchMyPostLike(final int currentPageNumber) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getMyPostLikes("Bearer " + getAccessToken(), "DESC", currentPageNumber + 1).enqueue(new Callback<ApiResponse<Feeds>>() {
            @Override
            public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                setLikesResponse(response, Constants.MY_POST, currentPageNumber, Constants.LIKE_FRAGMENT);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    private void setLikesResponse(Response<ApiResponse<Feeds>> response, final int postType, final int currentPageNumber, int likeFragment) {
        if (response.isSuccessful() && response.code() == 200) {
            Feeds feeds = response.body().getData();
            int currentPage = feeds.getPageNumber();
            int totalPage = feeds.getTotalPages();
            List<FeedResponse> myFeeds = feeds.getItems();
            Fragment fragment = ((HomeActivity) mContext).getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            if (fragment != null && fragment instanceof MyLikesFragment) {
                MyLikesFragment myLikesFragment = (MyLikesFragment) fragment;
                if (postType == Constants.MY_POST) {
                    myLikesFragment.updateMyPostFeeds(myFeeds, currentPage, totalPage);
                } else {
                    myLikesFragment.updateMyBynFeeds(myFeeds, currentPage, totalPage);
                }
            }
        } else if (response.code() == 401) {
            getAccessTokenFromServer(new AccessTokenResponse() {
                @Override
                public void successAccessToken() {
                    if (postType == Constants.MY_BYN) {
                        fetchMyBynLikes(currentPageNumber);
                    } else {
                        fetchMyPostLike(currentPageNumber);
                    }
                }

                @Override
                public void errorAccessToken() {
                }
            });
        } else if (response.code() == 404) {
            setMyLikesError(404, postType);

        } else {
            setMyLikesError(400, postType);
        }
    }

    private void setMyLikesError(int errorCode, int postType) {
        Fragment fragment = ((HomeActivity) mContext).getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment != null && fragment instanceof MyLikesFragment) {
            MyLikesFragment myLikesFragment = (MyLikesFragment) fragment;
            if (errorCode == 404) {
                myLikesFragment.onItemFound(postType);
            } else {
                myLikesFragment.error(postType);
            }
        }
    }

    public void getShippingInfo() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getShippingInfo("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<List<ShippingInfo>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<ShippingInfo>>> call, Response<ApiResponse<List<ShippingInfo>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    List<ShippingInfo> shippingInfoList = response.body().getData();
                    Fragment fragment = ((HomeActivity) mContext).getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    if (fragment != null && fragment instanceof ShippingFragment) {
                        ShippingFragment shippingFragment = (ShippingFragment) fragment;
                        shippingFragment.updateInfo(shippingInfoList);
                    } else if (fragment instanceof EditMyListingFragment) {
                        EditMyListingFragment editMyListingFragment = (EditMyListingFragment) fragment;
                        editMyListingFragment.updateInfo(shippingInfoList);
                    } else if (fragment instanceof EditListingFragment) {
                        EditListingFragment editMyListingFragment = (EditListingFragment) fragment;
                        editMyListingFragment.updateInfo(shippingInfoList);
                    }
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getShippingInfo();
                        }

                        @Override
                        public void errorAccessToken() {

                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<List<ShippingInfo>>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));

            }
        });
    }


    public void getBodyTypes() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getBodyType("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<List<BodyType>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<BodyType>>> call, Response<ApiResponse<List<BodyType>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    List<BodyType> bodyTypeList = response.body().getData();
                    Fragment fragment = ((HomeActivity) mContext).getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    if (fragment instanceof EditProfileFragment) {
                        EditProfileFragment editProfileFragment = (EditProfileFragment) fragment;
                        editProfileFragment.setBodyTypeList(bodyTypeList);
                    } else if (fragment instanceof FilterUserFragment) {
                        FilterUserFragment filterUserFragment = (FilterUserFragment) fragment;
                        filterUserFragment.setBodyTypeList(bodyTypeList);
                    }
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<List<BodyType>>> call, Throwable t) {
                showAlertDialog(getResources().getString(R.string.error_server));
                hideProgress();
            }
        });
    }


    public void getStatesList() {
        showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getState("Bearer " + getAccessToken()).enqueue(new Callback<ApiListResponse<List<States>>>() {
            @Override
            public void onResponse(Call<ApiListResponse<List<States>>> call, Response<ApiListResponse<List<States>>> response) {

                if (response.isSuccessful() && response.code() == 200) {
                    Fragment fragment = ((HomeActivity) mContext).getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    if (fragment instanceof EditProfileFragment) {
                        EditProfileFragment editProfileFragment = (EditProfileFragment) fragment;
                        editProfileFragment.updateList(response.body().getData());
                    } else if (fragment instanceof AddNewAddressFragment) {
                        AddNewAddressFragment addNewAddressFragment = (AddNewAddressFragment) fragment;
                        addNewAddressFragment.updateList(response.body().getData());
                    } else if (fragment instanceof AddPaymentFragment) {
                        AddPaymentFragment addPaymentFragment = (AddPaymentFragment) fragment;
                        addPaymentFragment.updateList(response.body().getData());
                    }
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiListResponse<List<States>>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    void setNewAddressInSellerAccountSetupFragment(Address address) {
        Fragment fragment = ((HomeActivity) mContext).getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof SelectExistingAddressFragment) {
            SelectExistingAddressFragment selectExistingAddressFragment = (SelectExistingAddressFragment) fragment;
            selectExistingAddressFragment.addedAddress(address);
        }
    }

    public void getMyAddress() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getMyAddress("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<List<Address>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Address>>> call, Response<ApiResponse<List<Address>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Fragment fragment = ((HomeActivity) mContext).getSupportFragmentManager().findFragmentById(R.id.fragment_container);
                    List<Address> addresses = response.body().getData();
                    if (fragment instanceof MyAddressFragment) {
                        MyAddressFragment myAddressFragment = (MyAddressFragment) fragment;
                        myAddressFragment.updateList(addresses);
                    } else if (fragment instanceof AddPaymentFragment) {
                        AddPaymentFragment addPaymentFragment = (AddPaymentFragment) fragment;
                        addPaymentFragment.updateAddressList(addresses);
                    } else if (fragment instanceof SelectDifferentAddressFragment) {
                        SelectDifferentAddressFragment selectDifferentAddressFragment = (SelectDifferentAddressFragment) fragment;
                        selectDifferentAddressFragment.updateAddressList(addresses);
                    } else if (fragment instanceof SellerAccountSetupFragment) {
                        SellerAccountSetupFragment sellerAccountSetupFragment = (SellerAccountSetupFragment) fragment;
                        sellerAccountSetupFragment.updateAddressList(addresses);
                    } else if (fragment instanceof SelectExistingAddressFragment) {
                        SelectExistingAddressFragment selectExistingAddressFragment = (SelectExistingAddressFragment) fragment;
                        selectExistingAddressFragment.updateAddressList(addresses);
                    }

                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getMyAddress();
                        }

                        @Override
                        public void errorAccessToken() {

                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<List<Address>>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));

            }
        });
    }

    public void fetchFeeds(final String searchText, final String type, final int currentPageNumber) {
        ((HomeActivity) mContext).filterData.setSearchText(searchText);
        ApiEndpointInterface apiInstance = ApiService.instance();
        if (type.equalsIgnoreCase(Constants.FEED_FRAGMENT)) {
            apiInstance.getAllFeeds("Bearer " + getAccessToken(), ((HomeActivity) mContext).filterData, currentPageNumber + 1).enqueue(new Callback<ApiResponse<Feeds>>() {
                @Override
                public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                    setFeedsResponse(response, searchText, Constants.AlL_FEEDS, type, currentPageNumber);
                    hideProgress();
                }

                @Override
                public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                    hideProgress();
                    setError(Constants.AlL_FEEDS);
                }
            });
        } else if (type.equalsIgnoreCase(Constants.BROWSE_FRAGMENT)) {
            apiInstance.getBrowseAllFeeds("Bearer " + getAccessToken(), ((HomeActivity) mContext).filterData, currentPageNumber + 1).enqueue(new Callback<ApiResponse<Feeds>>() {
                @Override
                public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                    setFeedsResponse(response, searchText, Constants.AlL_FEEDS, type, currentPageNumber);
                    hideProgress();
                }

                @Override
                public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                    hideProgress();
                    setError(Constants.AlL_FEEDS);
                }
            });
        }

    }

    private void setFeedsResponse(Response<ApiResponse<Feeds>> response, final String searchText, final int feedType, final String fragmentType, final int currentPageNumber) {
        if (response.isSuccessful() && response.code() == 200) {
            Feeds feeds = response.body().getData();
            int currentPage = feeds.getPageNumber();
            int totalPage = feeds.getTotalPages();
            List<FeedResponse> allFeeds = feeds.getItems();
            Fragment fragment = ((HomeActivity) mContext).getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            if (fragment != null && fragment instanceof FeedsFragment) {
                FeedsFragment feedFragment = (FeedsFragment) fragment;
                if (feedType == Constants.AlL_FEEDS) {
                    ((HomeActivity) mContext).mFeedsList = allFeeds;
                    feedFragment.updateFeeds(allFeeds, currentPage, totalPage);
                } else if (feedType == Constants.AlL_LISTING) {
                    feedFragment.updateListingFeeds(allFeeds, currentPage, totalPage);
                } else {
                    feedFragment.updatePostFeeds(allFeeds, currentPage, totalPage);
                }

            } else if (fragment != null && fragment instanceof BrowseFragment) {
                BrowseFragment browseFragment = (BrowseFragment) fragment;
                if (feedType == Constants.AlL_FEEDS) {
                    ((HomeActivity) mContext).mFeedsList = allFeeds;
                    browseFragment.updateFeeds(allFeeds, currentPage, totalPage);
                } else if (feedType == Constants.AlL_LISTING) {
                    browseFragment.updateListingFeeds(allFeeds, currentPage, totalPage);
                } else {
                    browseFragment.updatePostFeeds(allFeeds, currentPage, totalPage);
                }

            }
        } else if (response.code() == 401) {
            getAccessTokenFromServer(new AccessTokenResponse() {
                @Override
                public void successAccessToken() {
                    if (feedType == Constants.AlL_FEEDS) {
                        fetchFeeds(searchText, fragmentType, currentPageNumber);
                    } else if (feedType == Constants.AlL_LISTING) {
                        listingFeeds(searchText, fragmentType, currentPageNumber);
                    } else {
                        postFeeds(searchText, fragmentType, currentPageNumber);
                    }
                }

                @Override
                public void errorAccessToken() {
                    showAlertDialog(getResources().getString(R.string.error_server));
                }
            });
        } else if (response.code() == 404) {
            Fragment fragment = ((HomeActivity) mContext).getSupportFragmentManager().findFragmentById(R.id.fragment_container);
            if (fragment != null && fragment instanceof FeedsFragment) {
                FeedsFragment feedFragment = (FeedsFragment) fragment;
                feedFragment.onItemFound(feedType);
            } else if (fragment != null && fragment instanceof BrowseFragment) {
                BrowseFragment browseFragment = (BrowseFragment) fragment;
                browseFragment.onItemFound(feedType);
            }
        } else {
            setError(feedType);
        }
    }

    public void bestUser(final int tabPosition) {
        if (tabPosition == Constants.AlL_FEEDS) {
            ApiEndpointInterface apiInstance = ApiService.instance();
            apiInstance.getBestUser("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<Bloggers>>() {
                @Override
                public void onResponse(Call<ApiResponse<Bloggers>> call, Response<ApiResponse<Bloggers>> response) {
                    if (response.isSuccessful() && response.code() == 200) {
                        setBestUserResponse(response, tabPosition);
                    } else if (response.code() == 401) {
                        getAccessTokenFromServer(new AccessTokenResponse() {
                            @Override
                            public void successAccessToken() {
                                bestUser(tabPosition);
                            }

                            @Override
                            public void errorAccessToken() {
                                showAlertDialog(getResources().getString(R.string.error_server));
                            }
                        });
                    } else {
                        setBestUserResponseError(tabPosition);
                    }
                    hideProgress();
                }

                @Override
                public void onFailure(Call<ApiResponse<Bloggers>> call, Throwable t) {
                    hideProgress();
                    showAlertDialog(getResources().getString(R.string.error_server));
                }
            });
        } else {
            ApiEndpointInterface apiInstance = ApiService.instance();
            apiInstance.getBestUserForListingAndPost("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<Bloggers>>() {
                @Override
                public void onResponse(Call<ApiResponse<Bloggers>> call, Response<ApiResponse<Bloggers>> response) {
                    if (response.isSuccessful() && response.code() == 200) {
                        setBestUserResponse(response, tabPosition);
                    } else if (response.code() == 401) {
                        getAccessTokenFromServer(new AccessTokenResponse() {
                            @Override
                            public void successAccessToken() {
                                bestUser(tabPosition);
                            }

                            @Override
                            public void errorAccessToken() {
                                showAlertDialog(getResources().getString(R.string.error_server));
                            }
                        });
                    } else {
                        String message = Utils.ErrorMessage(mContext, response.errorBody());
                        showAlertDialog(message);
                    }
                    hideProgress();
                }

                @Override
                public void onFailure(Call<ApiResponse<Bloggers>> call, Throwable t) {
                    showAlertDialog(getResources().getString(R.string.error_server));
                }
            });
        }
    }

    private void setBestUserResponseError(int tabPosition) {
        Fragment fragment = ((HomeActivity) mContext).getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment != null && fragment instanceof FeedsFragment) {
            FeedsFragment feedFragment = (FeedsFragment) fragment;
            feedFragment.bestUserError(tabPosition);
        } else if (fragment != null && fragment instanceof BestUserFragment) {
            BestUserFragment bestUserFragment = (BestUserFragment) fragment;
            bestUserFragment.error();
        } else if (fragment != null && fragment instanceof BrowseFragment) {
            BrowseFragment browseFragment = (BrowseFragment) fragment;
            browseFragment.bestUserError(tabPosition);
        }
    }

    private void setBestUserResponse(Response<ApiResponse<Bloggers>> response, int tabPosition) {
        Bloggers bloggers = response.body().getData();
        List<UserResponse> bestUser = bloggers.getItems();
        if (tabPosition == Constants.AlL_FEEDS) {
            ((HomeActivity) mContext).mBestUser = bestUser;
        } else {
            ((HomeActivity) mContext).mFollowingList = bestUser;
        }
        Fragment fragment = ((HomeActivity) mContext).getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment instanceof FeedsFragment) {
            FeedsFragment feedFragment = (FeedsFragment) fragment;
            feedFragment.updateBestUser(bestUser, tabPosition);
        } else if (fragment instanceof BestUserFragment) {
            BestUserFragment bestUserFragment = (BestUserFragment) fragment;
            bestUserFragment.updateList(bestUser);
        } else if (fragment instanceof BrowseFragment) {
            BrowseFragment browseFragment = (BrowseFragment) fragment;
            browseFragment.updateBestUser(bestUser, tabPosition);
        }
    }

    public void listingFeeds(final String searchText, final String type, final int currentPageNumber) {
        if (((HomeActivity) mContext).filterFragmentType != Constants.FEED_FILTER) {
            ((HomeActivity) mContext).filterData = new FilterData();
        }
        ((HomeActivity) mContext).filterData.setSearchText(searchText);
        ApiEndpointInterface apiInstance = ApiService.instance();
        if (type.equalsIgnoreCase(Constants.FEED_FRAGMENT)) {
            apiInstance.putListingFeeds("Bearer " + getAccessToken(), ((HomeActivity) mContext).filterData, currentPageNumber + 1).enqueue(new Callback<ApiResponse<Feeds>>() {
                @Override
                public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                    setFeedsResponse(response, searchText, Constants.AlL_LISTING, type, currentPageNumber);
                    hideProgress();
                }

                @Override
                public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                    hideProgress();
                    showAlertDialog(getResources().getString(R.string.error_server));
                    setError(Constants.AlL_LISTING);
                }
            });
        } else if (type.equalsIgnoreCase(Constants.BROWSE_FRAGMENT)) {
            apiInstance.putBrowseListingFeeds("Bearer " + getAccessToken(), ((HomeActivity) mContext).filterData, currentPageNumber + 1).enqueue(new Callback<ApiResponse<Feeds>>() {
                @Override
                public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                    setFeedsResponse(response, searchText, Constants.AlL_LISTING, type, currentPageNumber);
                }

                @Override
                public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                    hideProgress();
                    showAlertDialog(getResources().getString(R.string.error_server));
                    setError(Constants.AlL_LISTING);
                }
            });
        }
    }

    private void setError(int feedType) {
        Fragment fragment = ((HomeActivity) mContext).getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        if (fragment != null && fragment instanceof FeedsFragment) {
            FeedsFragment feedFragment = (FeedsFragment) fragment;
            feedFragment.error(feedType);
        } else if (fragment != null && fragment instanceof BrowseFragment) {
            BrowseFragment browseFragment = (BrowseFragment) fragment;
            browseFragment.error(feedType);
        }
    }

    public void postFeeds(final String searchText, final String type, final int currentPageNumber) {
        ((HomeActivity) mContext).filterData.setSearchText(searchText);
        ApiEndpointInterface apiInstance = ApiService.instance();
        if (type.equalsIgnoreCase(Constants.FEED_FRAGMENT)) {
            apiInstance.putPostFeeds("Bearer " + getAccessToken(), ((HomeActivity) mContext).filterData, currentPageNumber + 1).enqueue(new Callback<ApiResponse<Feeds>>() {
                @Override
                public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                    setFeedsResponse(response, searchText, Constants.AlL_POSTS, type, currentPageNumber);
                    hideProgress();
                }

                @Override
                public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                    hideProgress();
                    setError(Constants.AlL_POSTS);
                }
            });
        } else if (type.equalsIgnoreCase(Constants.BROWSE_FRAGMENT)) {
            apiInstance.putBrowsePostFeeds("Bearer " + getAccessToken(), ((HomeActivity) mContext).filterData, currentPageNumber + 1).enqueue(new Callback<ApiResponse<Feeds>>() {
                @Override
                public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                    setFeedsResponse(response, searchText, Constants.AlL_POSTS, type, currentPageNumber);
                }

                @Override
                public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                    hideProgress();
                    setError(Constants.AlL_POSTS);
                }
            });
        }
    }
}
