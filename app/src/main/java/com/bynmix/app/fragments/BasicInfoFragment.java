package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FragmentBasicInfoBinding;
import com.bynmix.app.interfaces.EditProfileListener;
import com.bynmix.app.models.UserResponse;
public class BasicInfoFragment extends BaseFragment {

    private FragmentBasicInfoBinding mBinding;
    private UserResponse user;
    private EditProfileListener mCallback;
    private Context mContext;
    private String oldUsername;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentBasicInfoBinding.inflate(inflater, container, false);
        setData();
        mBinding.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendUserData();
            }
        });
        return mBinding.getRoot();

    }


    private void sendUserData() {
        String username = mBinding.username.getText().toString().trim();
        String firstName = mBinding.firstName.getText().toString().trim();
        String lastName = mBinding.lastName.getText().toString().trim();
        String title = mBinding.title.getText().toString().trim();
        String shortDescription = mBinding.shortDescription.getText().toString().trim();
        if (TextUtils.isEmpty(username)) {
            mBinding.username.setError(getResources().getString(R.string.enter_user_name));
            return;
        }

        if (oldUsername.equals(username)) {
            user.setUsername(null);
        } else {
            user.setUsername(username);
        }

        if (TextUtils.isEmpty(firstName)) {
            mBinding.firstName.setError(getResources().getString(R.string.enter_first_name));
            return;
        }

        if (TextUtils.isEmpty(lastName)) {
            mBinding.lastName.setError(getResources().getString(R.string.enter_last_name));
            return;
        }
        user.setShortTitle(title);
        user.setFirstname(firstName);
        user.setLastname(lastName);
        user.setShortBio(shortDescription);
        mCallback.onClick(user);
    }

    private void setData() {
        oldUsername = user.getUsername();
        mBinding.username.setText(user.getUsername());
        mBinding.firstName.setText(user.getFirstname());
        mBinding.lastName.setText(user.getLastname());
        mBinding.title.setText(user.getTitle());
        mBinding.shortDescription.setText(user.getShortBio());
    }

    public void setUser(UserResponse user) {
        this.user = user;
    }


    public void setListener(EditProfileListener mCallback, UserResponse user) {
        this.mCallback = mCallback;
        this.user = user;
    }
}
