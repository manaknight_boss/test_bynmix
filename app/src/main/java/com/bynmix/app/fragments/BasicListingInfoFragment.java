package com.bynmix.app.fragments;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.adapter.AddTagsAdapter;
import com.bynmix.app.adapter.ListingStatusAdapter;
import com.bynmix.app.databinding.FragmentBasicListingInfoBinding;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.models.Status;
import com.bynmix.app.utils.CustomTextDialog;
import com.bynmix.app.utils.DialogAlert;
import com.bynmix.app.utils.Utils;
import com.google.gson.Gson;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;
import java.util.List;

public class BasicListingInfoFragment extends BaseFragment {

    private FragmentBasicListingInfoBinding mBinding;
    private int listingId;
    private CreatePostListener mCallback;
    private Context mContext;
    private ListingDetail listingDetail;
    private AddTagsAdapter mAdapter;
    private List<String> mTagsList = new ArrayList<>();
    private CustomTextDialog errorDialog = new CustomTextDialog();
    private List<Status> mListingStatusList = new ArrayList<>();
    private ListingStatusAdapter mListingStatusAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (CreatePostListener) mContext;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentBasicListingInfoBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    private void init() {
        mTagsList.clear();
        for (int i = 0; i < listingDetail.getListingTags().size(); i++) {
            mTagsList.add(listingDetail.getListingTags().get(i).getTagName());
        }
        mBinding.directSwitchButton.setChecked(listingDetail.isBuyItNowOnly());
        setListingDetail();
        mBinding.directSwitchButton.setChecked(listingDetail.isBuyItNowOnly());
        setTagAdapter();
        mBinding.updateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listingDetail != null)
                    sendData();
            }
        });

        mBinding.directSwitchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listingDetail.setBuyItNowOnly(false);
                setLowestPriceUI(false);
                if (mBinding.directSwitchButton.isChecked()) {
                    mBinding.directSwitchButton.setChecked(false);
                    DialogAlert.showDialog(mContext, mContext.getResources().getString(R.string.direct_purchase), mContext.getResources().getString(R.string.are_you_sure_want_to_change_direct_purchase), mContext.getResources().getString(R.string.convert_to_direct_purchase_detail), R.string.yes_converted_to_direct_purchase, new DialogClickListener() {
                        @Override
                        public void onClick() {
                            mBinding.directSwitchButton.setChecked(true);
                            listingDetail.setBuyItNowOnly(true);
                            setLowestPriceUI(true);
                        }
                    });
                }
            }
        });

        mListingStatusAdapter = new ListingStatusAdapter(mContext, R.layout.spinner_item, mListingStatusList);
        mBinding.listingStatus.setAdapter(mListingStatusAdapter);
        updateUI();
    }


    private void sendData() {
        String listingTitle, shortDescription, originalPrice, listingPrice, lowestAllowedPrice;
        listingTitle = mBinding.listingTitle.getText().toString().trim();
        shortDescription = mBinding.shortDescription.getText().toString().trim();
        originalPrice = mBinding.originalPrice.getText().toString().trim();
        listingPrice = mBinding.listingPrice.getText().toString().trim();
        int selectedId = mBinding.listingStatus.getSelectedItemPosition();
        int status = mListingStatusList.get(selectedId).getStatus();
        lowestAllowedPrice = mBinding.lowestAllowedPrice.getText().toString().trim();

        if (TextUtils.isEmpty(listingTitle)) {
            String error = Utils.getColorText(getString(R.string.listing_title_text), mContext.getResources().getColor(R.color.very_light_purple));
            error = "A " + error + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (TextUtils.isEmpty(shortDescription)) {
            String error = Utils.getColorText(getString(R.string.short_description_text), mContext.getResources().getColor(R.color.very_light_purple));
            error = "A " + error + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (mTagsList.size() == 0) {
            String error = Utils.getColorText(getString(R.string.listing_tag_text), mContext.getResources().getColor(R.color.very_light_purple));
            error = "A " + error + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (TextUtils.isEmpty(originalPrice)) {
            String error = Utils.getColorText(getString(R.string.original_price), mContext.getResources().getColor(R.color.very_light_purple));
            error = "An " + error + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (TextUtils.isEmpty(listingPrice)) {
            String error = Utils.getColorText(getString(R.string.listing_price), mContext.getResources().getColor(R.color.very_light_purple));
            error = "A " + error + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (!TextUtils.isEmpty(lowestAllowedPrice)) {
            if (Integer.parseInt(lowestAllowedPrice) > Double.parseDouble(listingPrice)) {
                ((HomeActivity) mContext).mAlertDialog.showDialog((HomeActivity) mContext, "Your lowest allowable offer must be less than the listing price.");
                return;
            }
        }

        listingDetail.setOriginalPrice(Double.parseDouble(originalPrice));
        listingDetail.setPrice(Double.parseDouble(listingPrice));
        listingDetail.setTitle(listingTitle);
        listingDetail.setDescription(shortDescription);
        listingDetail.setStatusId(status);
        listingDetail.setTags(mTagsList);
        if (!TextUtils.isEmpty(lowestAllowedPrice)) {
            listingDetail.setLowestAllowableOffer(Integer.parseInt(lowestAllowedPrice));
        }
        mCallback.putEditListing(listingId, listingDetail);

    }

    private void setTagAdapter() {
        mAdapter = new AddTagsAdapter(mContext, mTagsList);
        mBinding.recycleView.setLayoutManager(new FlowLayoutManager());
        mBinding.recycleView.setAdapter(mAdapter);
        if (mBinding.recycleView.getItemDecorationCount() == 0) {
            mBinding.recycleView.addItemDecoration(new RecyclerView.ItemDecoration() {
                @Override
                public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                    super.getItemOffsets(outRect, view, parent, state);
                    outRect.set(15, 15, 15, 15);
                }
            });

        }
        mCallback.clearListingImagesList();
    }


    private void updateUI() {
        if (listingDetail != null) {
            setLowestPriceUI(listingDetail.isBuyItNowOnly());

        }

    }

    public void setData(int listingId, ListingDetail listingDetail, List<Status> mListingStatusList) {
        this.listingId = listingId;
        this.listingDetail = listingDetail;
        this.mListingStatusList = mListingStatusList;
        init();
    }

    private void setListingDetail() {
        mBinding.listingTitle.setText(listingDetail.getTitle());
        mBinding.shortDescription.setText(listingDetail.getDescription());
        mBinding.originalPrice.setText(String.format("%s", listingDetail.getOriginalPrice()));
        mBinding.listingPrice.setText(String.format("%s", listingDetail.getPrice()));
        mBinding.listingStatus.setSelection(listingDetail.getStatus().getStatus() - 1);
        mBinding.lowestAllowedPrice.setText(String.format("%s", listingDetail.getLowestAllowableOffer()));
        updateUI();
        for (int i = 1; i < mListingStatusList.size(); i++) {
            if (mListingStatusList.get(i).getStatus() == listingDetail.getStatus().getListingStatusId()) {
                mBinding.listingStatus.setSelection(i);
                break;
            }
        }
    }

    public void setId(int listingId) {
        this.listingId = listingId;
    }

    private void setLowestPriceUI(boolean isChecked) {
        if (isChecked) {
            mBinding.lowestAllowedPriceLayout.setVisibility(View.GONE);
        } else {
            mBinding.lowestAllowedPriceLayout.setVisibility(View.VISIBLE);
        }
    }
}
