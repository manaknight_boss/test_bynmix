package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.FollowFollowingAdapter;
import com.bynmix.app.databinding.FollowFollowingFragmentBinding;
import com.bynmix.app.interfaces.FeedItemClickListener;
import com.bynmix.app.models.UserResponse;
import com.bynmix.app.sharedpreference.SharedPreferenceUtility;

import java.util.ArrayList;
import java.util.List;

public class BestUserFragment extends BaseFragment {

    private FollowFollowingFragmentBinding mBinding;
    private Context mContext;
    private FeedItemClickListener mCallback;
    private FollowFollowingAdapter adapter;
    private SharedPreferenceUtility mSharedPreferenceUtility;
    private List<UserResponse> mList = new ArrayList<>();
    private int tabPosition;
    private String displayName;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (FeedItemClickListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FollowFollowingFragmentBinding.inflate(inflater, container, false);
        mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);
        if (displayName == null) {
            displayName = mSharedPreferenceUtility.getDisplayName();
        }
        if (tabPosition == 0) {

            mBinding.toolbarHeading.setText(R.string.follow_someone_interesting);
        } else {

            mBinding.toolbarHeading.setText("@" + displayName + " " + getString(R.string.following));
        }

        setRecyclerView();
        if (mList.size() == 0) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
           bestUser(tabPosition);
        }

        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        mBinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                {

                    if (mCallback.conditionsForApiCall()) {
                        bestUser(tabPosition);
                    } else {
                        hideSwipeRefresh();
                    }
                }
            }
        });

        return mBinding.getRoot();
    }

    private void hideSwipeRefresh() {
        mBinding.swipeRefreshLayout.setRefreshing(false);
    }


    private void setRecyclerView() {
        adapter = new FollowFollowingAdapter(mContext, mList, mCallback);
        mBinding.recycleView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recycleView.setAdapter(adapter);
    }

    /**
     * Best User Fragment
     *
     * @return BestUserFragment
     */
    public static BestUserFragment newInstance() {
        return new BestUserFragment();
    }

    public void setTabPosition(int tabPosition) {

        this.tabPosition = tabPosition;
    }

    public void updateList(List<UserResponse> bestUser) {
        mBinding.progressBar.setVisibility(View.GONE);
        this.mList = bestUser;
        adapter.setList(mList);
        hideSwipeRefresh();
    }

    public void error() {
    }
}