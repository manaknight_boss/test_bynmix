package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.adapter.BrandListingAdapter;
import com.bynmix.app.databinding.BrandFragmentBinding;
import com.bynmix.app.interfaces.BrandListener;
import com.bynmix.app.interfaces.ConfirmInterface;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.Brand;
import com.bynmix.app.utils.ConfirmDialog;
import com.bynmix.app.utils.ErrorDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class BrandFragment extends Fragment {

    private BrandFragmentBinding mBinding;
    private CreatePostListener mCallback;
    private Context mContext;
    private List<Brand> mList = new ArrayList<>();
    private BrandListingAdapter adapter;
    private Brand brand = null;
    private int brandId;
    private ErrorDialog errorDialog = new ErrorDialog();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (CreatePostListener) context;
        mContext = context;
    }

    public static BrandFragment newInstance() {
        return new BrandFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = BrandFragmentBinding.inflate(inflater, container, false);
        setRecyclerView();
        mBinding.toolbarHeading.setText(getString(R.string.brand_fragment_heading));
        getBrand();

        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        mBinding.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openSellerPolicyFragment();
            }
        });

        mBinding.autoComplete.requestFocus();
        mBinding.autoComplete.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable editable) {
                // TODO Auto-generated method stub

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newText = s.toString();
                if (newText.length() > 0) {
                    mBinding.progressBarSearch.setVisibility(View.VISIBLE);
                    List<Brand> searchedList = new ArrayList<>();
                    for (int i = 0; i < mList.size(); i++) {
                        if ((Pattern.compile(Pattern.quote(newText), Pattern.CASE_INSENSITIVE).matcher(mList.get(i).getBrandName()).find())) {
                            searchedList.add(mList.get(i));
                        }
                    }
                    adapter.setList(searchedList);
                } else {
                    adapter.setList(mList);
                }
                mBinding.progressBarSearch.setVisibility(View.GONE);
            }
        });
        return mBinding.getRoot();
    }

    private void getBrand() {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        mCallback.getBrands();
    }

    private void setRecyclerView() {
        adapter = new BrandListingAdapter(mContext, mList, mCallback);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(adapter);
        mBinding.recyclerView.setIndexBarTextColor(R.color.colorPrimary);
        mBinding.recyclerView.setIndexTextSize(15);
        mBinding.recyclerView.setIndexBarColor("#e8e8e8");
    }

    public void updateList(List<Brand> brandList) {
        mBinding.progressBar.setVisibility(View.GONE);
        this.mList = brandList;
        for (int i = 0; i < mList.size(); i++) {
            if (brandId == mList.get(i).getBrandId()) {
                mList.get(i).setSelected(true);
                brand = mList.get(i);
            }
        }
        adapter.setList(mList);
    }


    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public void askForDataLoss(final int id) {
        ConfirmDialog confirmDialog = new ConfirmDialog();
        confirmDialog.showDialog(mContext, new ConfirmInterface() {
            @Override
            public void confirm() {
                mCallback.clearListDetail();
                if(id != 0) {
                    ((HomeActivity)mContext).moveToTab(id);
                } else {
                    mCallback.popFragment();
                    mCallback.popFragment();
                    mCallback.popFragment();
                }
            }
        });
    }
}
