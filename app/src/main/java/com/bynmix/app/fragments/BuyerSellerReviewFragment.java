package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.adapter.ReviewAdapter;
import com.bynmix.app.databinding.FragmentBuyerSellerReviewsBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.ReviewListener;
import com.bynmix.app.models.Reviews;
import com.bynmix.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class BuyerSellerReviewFragment extends Fragment {

    private FragmentBuyerSellerReviewsBinding mBinding;
    private List<Reviews> mList = new ArrayList<>();
    private ReviewAdapter adapter;
    private Context mContext;
    private ProfileListener mCallback;
    private int userId;
    private String username;
    private boolean isAlreadyCalled;
    private int currentPageNumber;
    private int totalPages;
    private int type;
    private ReviewListener reviewListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (ProfileListener) context;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentBuyerSellerReviewsBinding.inflate(inflater, container, false);
        isAlreadyCalled = false;
        adapter = new ReviewAdapter(mContext, mList, mCallback, type);
        mBinding.recycleView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recycleView.setAdapter(adapter);
        mBinding.nothingToShowPopUp.setVisibility(View.GONE);

        mBinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                {

                    if (mCallback.conditionsForApiCall()) {
                        currentPageNumber = 0;
                        fetchReviews();
                    } else {
                        hideSwipeRefresh();
                    }
                }
            }
        });

        mBinding.recycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();
                boolean endHasBeenReached = lastVisible >= totalItemCount - 4;
                if (totalItemCount > 0 && endHasBeenReached) {
                    if (!isAlreadyCalled && currentPageNumber < totalPages && mList.size() > 0) {
                        isAlreadyCalled = true;
                        fetchReviews();
                    }
                }

            }
        });
        if (mList.size() == 0) {
            if (type == Constants.BUYER) {
                fetchReviews();
            }
        }

        return mBinding.getRoot();
    }


    private void fetchReviews() {
        setLoadMoreProgress();
        if (type == Constants.BUYER) {
            reviewListener.fetchBuyerReviews(userId, currentPageNumber);
        } else {
            reviewListener.fetchSellerReviews(userId, currentPageNumber);
        }
    }

    public void setList(List<Reviews> postFeeds, int currentPageNumber, int totalPages) {
        this.currentPageNumber = currentPageNumber;
        this.totalPages = totalPages;
        if (currentPageNumber > 1) {
            this.mList.addAll(postFeeds);
        } else {
            this.mList = postFeeds;
        }
        isAlreadyCalled = false;
        setLoadMoreProgress();
        adapter.setList(mList);
        mBinding.nothingToShowPopUp.setVisibility(View.GONE);
        hideSwipeRefresh();
    }

    private void hideSwipeRefresh() {
        mBinding.swipeRefreshLayout.setRefreshing(false);
    }

    public void setError(int error) {
        mBinding.progressBar.setVisibility(View.GONE);
        if (mList.size() == 0) {
            if (error == 404) {
                mBinding.logo.setImageResource(R.mipmap.logo_black);
                setEmptyListErrorMessage();
            } else {
                mBinding.errorText.setText(Html.fromHtml(getString(R.string.sad_cloud_error)), TextView.BufferType.SPANNABLE);
                mBinding.logo.setImageResource(R.mipmap.cloud_sad);
            }
            mBinding.nothingToShowPopUp.setVisibility(View.VISIBLE);
            mBinding.nothingToShowPopUp.setVisibility(View.VISIBLE);
        }
        mList.clear();
        currentPageNumber = 0;
        mBinding.progressBar.setVisibility(View.GONE);
        hideSwipeRefresh();
    }

    private void setEmptyListErrorMessage() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            mBinding.errorText.setText(Html.fromHtml("<b>" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + username + "</font>" + "</b> " + getResources().getString(R.string.no_review_yet), Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
        }
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setCallBack(ReviewListener reviewListener) {
        this.reviewListener = reviewListener;
    }

    public void callList() {
        if (mList.size() == 0) {
            mBinding.nothingToShowPopUp.setVisibility(View.GONE);
            currentPageNumber = 0;
            fetchReviews();
        }
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private void setLoadMoreProgress() {
        if (mList.size() == 0) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
        } else {
            mBinding.progressBar.setVisibility(View.GONE);
        }
        if (isAlreadyCalled) {
            adapter.setProgress(true);
        } else {
            adapter.setProgress(false);
        }
    }

    public void setType(int type) {
        this.type = type;
    }

}