package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FragmentCancelSaleBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.SaleCancelReason;
import com.bynmix.app.models.SalesDetail;
import com.bynmix.app.models.Stock;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.DialogAlert;
import com.bynmix.app.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CancelSaleFragment extends BaseFragment {

    private FragmentCancelSaleBinding mBinding;
    private Context mContext;
    private ProfileListener mCallback;
    private List<SaleCancelReason> mList = new ArrayList<>();
    private Stock detail;
    private List<String> saleCancelReasonsName = new ArrayList<>();


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (ProfileListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentCancelSaleBinding.inflate(inflater, container, false);
        if (mList.size() == 0) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
            mBinding.mainLayout.setVisibility(View.GONE);
            getCancelSaleReason();
        } else {
            setData();
        }
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
        mBinding.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelOrder();
            }
        });
        return mBinding.getRoot();
    }

    private void cancelOrder() {
        String reasonName, comment;
        int saleCancelReasonId = 0;
        reasonName = mBinding.spinner.getSelectedItem().toString();
        if (reasonName.equalsIgnoreCase("Select cancel reason…")) {
            Toast.makeText(mContext, "Please select a reason to cancel order", Toast.LENGTH_SHORT).show();
            return;
        }
        for (int i = 0; i < mList.size(); i++) {
            if (reasonName.equalsIgnoreCase(mList.get(i).getCancelReason())) {
                saleCancelReasonId = mList.get(i).getSaleCancelReasonId();
                break;
            }
        }
        comment = mBinding.comment.getText().toString();
        SaleCancelReason saleCancelReason = new SaleCancelReason();
        saleCancelReason.setSaleCancelReasonId(saleCancelReasonId);
        saleCancelReason.setComments(comment);
        SalesDetail salesDetail = (SalesDetail) detail;
        cancelOrder(salesDetail.getSaleId(), saleCancelReason, detail.getListingTitle());

    }

    private void setData() {
        mBinding.progressBar.setVisibility(View.GONE);
        mBinding.mainLayout.setVisibility(View.VISIBLE);
        ArrayAdapter<String> cancelReasonAdapter = new ArrayAdapter<String>(mContext, R.layout.spinner_item, saleCancelReasonsName);
        mBinding.spinner.setAdapter(cancelReasonAdapter);
        String title = Utils.getColorText(mContext, detail.getListingTitle());
        mBinding.title.setText(Html.fromHtml("\"" + title.trim() + "\" ?"));
        String image = this.detail.getListingImageUrl();
        if (!TextUtils.isEmpty(image)) {
            Picasso.get().load(image).into(mBinding.image);
        } else {
            mBinding.image.setImageResource(R.mipmap.default_feed);
        }
        mBinding.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 0) {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.lightGrey));
                } else {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorPrimary));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


    public static CancelSaleFragment newInstance() {
        return new CancelSaleFragment();
    }


    public void updateSaleCancelReason(List<SaleCancelReason> saleCancelReasons) {
        this.mList = saleCancelReasons;
        for (int i = 0; i < mList.size(); i++) {
            saleCancelReasonsName.add(mList.get(i).getCancelReason());
        }
        saleCancelReasonsName.add(0, "Select cancel reason…");
        setData();
    }

    public void setStock(Stock detail) {
        this.detail = detail;
    }

    public void getCancelSaleReason() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.cancelSaleReason("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<List<SaleCancelReason>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<SaleCancelReason>>> call, Response<ApiResponse<List<SaleCancelReason>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    List<SaleCancelReason> saleCancelReasons = response.body().getData();
                    updateSaleCancelReason(saleCancelReasons);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getCancelSaleReason();
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<List<SaleCancelReason>>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    public void cancelOrder(final int id, final SaleCancelReason saleCancelReason, final String listingTitle) {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.cancelOrder("Bearer " + getAccessToken(), "" + id, saleCancelReason).enqueue(new Callback<ApiResponse<Boolean>>() {
            @Override
            public void onResponse(Call<ApiResponse<Boolean>> call, Response<ApiResponse<Boolean>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    mCallback.onBackPressed();
                    String title = Utils.getColorText(mContext, listingTitle.trim());
                    String message = getString(R.string.cancel_sale_part1) + " " + title + getString(R.string.part2);
                    DialogAlert.showSaleCancelSuccessfulDialog(mContext, "Sale Canceled Succefully.", message, true);
                    mCallback.openPurchasesFragment(Constants.MY_SALES);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            cancelOrder(id, saleCancelReason, listingTitle);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    DialogAlert.showCancelSaleErrorDialog(mContext, message, getResources().getString(R.string.cancel_sale_error));
                }
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Boolean>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }
}
