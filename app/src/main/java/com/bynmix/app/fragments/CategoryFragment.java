package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.adapter.CategoryAdapter;
import com.bynmix.app.adapter.SelectCategoryAdapter;
import com.bynmix.app.databinding.CategoryFragmentBinding;
import com.bynmix.app.interfaces.CategoryListener;
import com.bynmix.app.interfaces.ConfirmInterface;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.Category;
import com.bynmix.app.models.CategoryType;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.utils.ConfirmDialog;
import com.bynmix.app.utils.ErrorDialog;

import java.util.ArrayList;
import java.util.List;

public class CategoryFragment extends Fragment implements CategoryListener {

    private CategoryFragmentBinding mBinding;
    private CreatePostListener mCallback;
    private Context mContext;
    private List<CategoryType> mList = new ArrayList<>();
    private CategoryAdapter adapter;
    private Category category = null;
    private CategoryType categoryType = null;
    private int categoryId;
    private int categoryTypeId;
    private ErrorDialog errorDialog = new ErrorDialog();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (CreatePostListener) context;
        mContext = context;
    }

    public static CategoryFragment newInstance() {
        return new CategoryFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = CategoryFragmentBinding.inflate(inflater, container, false);
        showSubCategory(false);
        setRecyclerView();
        mBinding.toolbarHeading.setText(getString(R.string.select_category));
        getCategories();
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
        mBinding.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openSellerPolicyFragment();
            }
        });

        return mBinding.getRoot();
    }

    private String getColorText(String string) {
        String error = "<b>" + "<font color='" + mContext.getResources().getColor(R.color.purple) + "'>" + string + "</font>" + "</b> ";
        return error;
    }

    private void getCategories() {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        if (mList.size() == 0) {
            mCallback.getCategories();
        } else {
            updateList(mList);
        }
    }

    private void setRecyclerView() {
        adapter = new CategoryAdapter(mContext, mList, mCallback);
        final StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        mBinding.recyclerView.setLayoutManager(layoutManager);
        mBinding.recyclerView.setAdapter(adapter);
    }

    public void updateList(List<CategoryType> categoryTypeList) {
        mBinding.progressBar.setVisibility(View.GONE);
        this.mList = categoryTypeList;
        for (int i = 0; i < mList.size(); i++) {
            if (categoryTypeId == mList.get(i).getCategoryTypeId()) {
                mList.get(i).setSelected(true);
                categoryType = mList.get(i);
                for (int j = 0; j < mList.get(i).getCategories().size(); j++) {
                    if (categoryId == mList.get(i).getCategories().get(j).getCategoryId()) {
                        mList.get(i).getCategories().get(j).setCategorySelected(true);
                        category = mList.get(i).getCategories().get(j);
                    }
                }
            }
        }
        adapter.setList(mList);
    }

    public void setListingDetail(ListingDetail listingDetail) {
        if (listingDetail != null) {
            this.category = listingDetail.getCategory();
            this.categoryType = listingDetail.getCategoryType();
        } else {
            this.category = null;
            this.categoryType = null;
        }
    }

    public void showTick(CategoryType categoryType) {
        this.categoryType = categoryType;
        showSubCategory(true);
    }

    private void showSubCategory(boolean show) {
        if (show) {
            final ListingDetail listingDetail = new ListingDetail();
            mBinding.subCategoryLayout.setVisibility(View.VISIBLE);
            mBinding.subCategory.categoryType.setText(categoryType.getCategoryTypeName());
            SelectCategoryAdapter adapter = new SelectCategoryAdapter(mContext, categoryType, categoryType.getCategories(), listingDetail, mCallback);
            mBinding.subCategory.recycleView.setLayoutManager(new LinearLayoutManager(mContext));
            mBinding.subCategory.recycleView.setAdapter(adapter);
            mBinding.subCategory.close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mBinding.subCategoryLayout.setVisibility(View.GONE);
                }
            });
            mBinding.noView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mBinding.subCategoryLayout.setVisibility(View.GONE);
                }
            });
            updateUI(listingDetail);
            mBinding.subCategory.select.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listingDetail.setCategoryType(categoryType);
                    mCallback.setCategory(listingDetail);
                    mBinding.subCategoryLayout.setVisibility(View.GONE);
                }
            });
        } else {
            mBinding.subCategoryLayout.setVisibility(View.GONE);
        }
    }

    private void updateUI(ListingDetail listingDetail) {
        if (listingDetail.getCategory() != null) {
            mBinding.subCategory.select.setVisibility(View.VISIBLE);
        } else {
            mBinding.subCategory.select.setVisibility(View.INVISIBLE);
        }
    }

    public void setCategoryId(int categoryId, int categoryTypeId) {
        this.categoryId = categoryId;
        this.categoryTypeId = categoryTypeId;
    }

    @Override
    public void selectedCategory(ListingDetail listingDetail) {
        updateUI(listingDetail);
    }

    public void askForDataLoss(final int id) {
        ConfirmDialog confirmDialog = new ConfirmDialog();
        confirmDialog.showDialog(mContext, new ConfirmInterface() {
            @Override
            public void confirm() {
                mCallback.clearListDetail();
                if(id != 0) {
                    ((HomeActivity)mContext).moveToTab(id);
                } else {
                    mCallback.popFragment();
                    mCallback.popFragment();
                    mCallback.popFragment();
                }
            }
        });
    }
}
