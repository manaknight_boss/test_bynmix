package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.adapter.ColorAdapter;
import com.bynmix.app.databinding.CategoryFragmentBinding;
import com.bynmix.app.interfaces.ColorListener;
import com.bynmix.app.interfaces.ConfirmInterface;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.Colors;
import com.bynmix.app.utils.ConfirmDialog;
import com.bynmix.app.utils.ErrorDialog;

import java.util.ArrayList;
import java.util.List;

public class ColorFragment extends Fragment implements ColorListener {

    private CategoryFragmentBinding mBinding;
    private CreatePostListener mCallback;
    private Context mContext;
    private List<Colors> mList = new ArrayList<>();
    private List<Colors> selectedColors = new ArrayList<>();
    private ColorAdapter adapter;
    private ErrorDialog errorDialog = new ErrorDialog();
    private List<Integer> colorsIdsList = new ArrayList<>();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (CreatePostListener) context;
        mContext = context;
    }

    public static ColorFragment newInstance() {
        return new ColorFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = CategoryFragmentBinding.inflate(inflater, container, false);
        mBinding.toolbarHeading.setText(getString(R.string.color_fragment_heading));
        if(mList.size() == 0) {
            getColors();
        }
        setRecyclerView();
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        mBinding.info.setImageResource(R.mipmap.check);

        mBinding.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedColors.size() > 0) {
                    mCallback.setColorInListingDetail(selectedColors);
                    mCallback.onBackPressed();
                } else {
                    errorDialog.showDialog(mContext, getString(R.string.color_error));
                }
            }
        });

        return mBinding.getRoot();
    }

    private void getColors() {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        mCallback.getColors();
    }


    private void setRecyclerView() {
        adapter = new ColorAdapter(mContext, mList, this);
        final StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(3, StaggeredGridLayoutManager.VERTICAL);
        mBinding.recyclerView.setLayoutManager(layoutManager);
        mBinding.recyclerView.setAdapter(adapter);
    }

    public void updateList(List<Colors> colorsList) {
        mBinding.progressBar.setVisibility(View.GONE);
        this.mList = colorsList;
        for (Colors color : mList) {
            if (colorsIdsList.contains(color.getColorId())) {
                color.setSelectedColor(true);
                selectedColors.add(color);
            }
        }
        adapter.setList(mList);
    }

    @Override
    public void addColor(List<Colors> colorsList) {
        this.selectedColors = colorsList;
    }

    public void setColorIdsList(List<Integer> colorIdsList) {
        this.colorsIdsList = colorIdsList;
    }

    public void askForDataLoss(final int id) {
        ConfirmDialog confirmDialog = new ConfirmDialog();
        confirmDialog.showDialog(mContext, new ConfirmInterface() {
            @Override
            public void confirm() {
                mCallback.clearListDetail();
                if(id != 0) {
                    ((HomeActivity)mContext).moveToTab(id);
                } else {
                    mCallback.popFragment();
                    mCallback.popFragment();
                    mCallback.popFragment();
                }
            }
        });
    }
}
