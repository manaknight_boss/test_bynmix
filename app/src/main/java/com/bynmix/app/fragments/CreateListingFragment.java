package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.EditPhotoListingAdapter;
import com.bynmix.app.adapter.SelectPhotoAdapter;
import com.bynmix.app.databinding.CreateListingFragmentBinding;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.models.ListingDisplayImage;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.SavingPostDialog;

import java.util.ArrayList;
import java.util.List;

public class CreateListingFragment extends Fragment implements DialogClickListener {

    private CreateListingFragmentBinding mBinding;
    private CreatePostListener mCallback;
    private Context mContext;
    private List<ListingDisplayImage> mPhotoList = new ArrayList<>();
    private EditPhotoListingAdapter mAdapter;
    private int type;
    private int listingId;

    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (CreatePostListener) context;
        mContext = context;
    }

    public static CreateListingFragment newInstance() {
        return new CreateListingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = CreateListingFragmentBinding.inflate(inflater, container, false);
        mCallback.clearListingImagesList();
        setAdapter();
        updateUI();
        mBinding.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openSellerPolicyFragment();
            }
        });
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
        mBinding.nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type == Constants.EDIT) {
                    mCallback.updateListingImages(listingId, mPhotoList);
                } else {
                    mCallback.openAddListingDetailFragment(mPhotoList);
                }
            }
        });

        return mBinding.getRoot();
    }

    public void updateUI() {
        mAdapter.setList(mPhotoList);
        if (type == Constants.EDIT) {
            mBinding.toolbar.setVisibility(View.GONE);
            mBinding.buttonText.setText(getString(R.string.update_text));
        } else {
            mBinding.toolbar.setVisibility(View.VISIBLE);
            mBinding.buttonText.setText(getString(R.string.next));
        }
        if (mPhotoList.size() > 0) {
            mBinding.nextButton.setVisibility(View.VISIBLE);
        } else {
            mBinding.nextButton.setVisibility(View.GONE);
        }
    }

    private void setAdapter() {
        mAdapter = new EditPhotoListingAdapter(mContext, mPhotoList, mCallback, this);
        mBinding.recyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    public void setPhoto(List<String> photoList) {
        for (int i = 0; i < photoList.size(); i++) {
            ListingDisplayImage listingDisplayImage = new ListingDisplayImage();
            listingDisplayImage.setImageUrl(photoList.get(i));
            listingDisplayImage.setType(Constants.LOCAL_PHOTO);
            mPhotoList.add(listingDisplayImage);
        }
        updateUI();
    }

    public void removeItem(int position) {
        mPhotoList.remove(position);
        updateUI();
    }

    public void setType(int type) {
        this.type = type;
    }

    public void updatePhotos(List<ListingDisplayImage> listingDisplayImages) {
        this.mPhotoList = listingDisplayImages;
        setAdapter();
        if (mPhotoList.size() > 0) {
            mBinding.nextButton.setVisibility(View.VISIBLE);
        } else {
            mBinding.nextButton.setVisibility(View.GONE);
        }
    }

    public void setData(int listingId) {
        this.listingId = listingId;
    }

    @Override
    public void onClick() {
        updateUI();
    }
}