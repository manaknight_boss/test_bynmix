package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.adapter.DisputeDetailAdapter;
import com.bynmix.app.databinding.FragmentDisputeDetailsBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.CommentListener;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.DisputeDetailListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Comments;
import com.bynmix.app.models.DisputeDetails;
import com.bynmix.app.models.PurchasesDetail;
import com.bynmix.app.models.Stock;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.DialogAlert;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DisputeDetailFragment extends Fragment implements DisputeDetailListener {

    private FragmentDisputeDetailsBinding mBinding;
    private Context mContext;
    private CreatePostListener mCallback;
    private int disputeId;
    private Stock stockDetail;
    private DisputeDetails detail;
    private DisputeDetailAdapter mAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (CreatePostListener) context;
    }

    public static DisputeDetailFragment newInstance() {
        return new DisputeDetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentDisputeDetailsBinding.inflate(inflater, container, false);
        if (detail == null) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
            getDisputeDetails();
        } else {
            setData();
        }
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
        mBinding.commentButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogAlert.showCommentDialog(mContext, Constants.TYPE_DISPUTES, new CommentListener() {
                    @Override
                    public void addComment(int id, String comment) {
                        addComments(comment);
                    }
                });
            }
        });

        mBinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                {

                    if (mCallback.conditionsForApiCall()) {
                        getDisputeDetails();
                    } else {
                        hideSwipeRefresh();
                    }
                }
            }
        });

        return mBinding.getRoot();
    }

    private void hideSwipeRefresh() {
        mBinding.swipeRefreshLayout.setRefreshing(false);
    }

    private void addComments(final String comment) {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        final Comments comments = new Comments();
        comments.setAdditionalComment(comment);
        apiInstance.addDisputeComment("Bearer " + ((HomeActivity) mContext).getAccessToken(), "" + disputeId, comments).enqueue(new Callback<ApiResponse<Integer>>() {
            @Override
            public void onResponse(Call<ApiResponse<Integer>> call, Response<ApiResponse<Integer>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    if (stockDetail instanceof PurchasesDetail) {
                        comments.setBuyerComment(true);
                    }
                    comments.setComment(comment);
                    detail.getAdditionalComments().add(comments);
                    setData();

                } else if (response.code() == 401) {
                    ((HomeActivity) mContext).getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getDisputeDetails();
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    ((HomeActivity) mContext).mAlertDialog.showDialog((HomeActivity) mContext, getResources().getString(R.string.error_server));
                }
                mBinding.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ApiResponse<Integer>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                ((HomeActivity) mContext).mAlertDialog.showDialog((HomeActivity) mContext, getResources().getString(R.string.error_server));
            }
        });
    }

    private void getDisputeDetails() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getDisputeDetails("Bearer " + ((HomeActivity) mContext).getAccessToken(), "" + disputeId).enqueue(new Callback<ApiResponse<DisputeDetails>>() {
            @Override
            public void onResponse(Call<ApiResponse<DisputeDetails>> call, Response<ApiResponse<DisputeDetails>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    detail = response.body().getData();
                    setData();
                } else if (response.code() == 401) {
                    ((HomeActivity) mContext).getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getDisputeDetails();
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {

                    ((HomeActivity) mContext).mAlertDialog.showDialog((HomeActivity) mContext, getResources().getString(R.string.error_server));
                }
                mBinding.progressBar.setVisibility(View.GONE);
                hideSwipeRefresh();
            }

            @Override
            public void onFailure(Call<ApiResponse<DisputeDetails>> call, Throwable t) {
                hideSwipeRefresh();
                mBinding.progressBar.setVisibility(View.GONE);
                ((HomeActivity) mContext).mAlertDialog.showDialog((HomeActivity) mContext, getResources().getString(R.string.error_server));
            }
        });
    }

    private void setData() {
        mBinding.progressBar.setVisibility(View.GONE);
        setAdapter();
        if (detail.isCanLeaveComment()) {
            mBinding.commentLayout.setVisibility(View.VISIBLE);
        } else {
            mBinding.commentLayout.setVisibility(View.GONE);
        }
    }

    private void setAdapter() {
        mAdapter = new DisputeDetailAdapter(mContext, detail, this);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    public void cancelDispute() {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.cancelDispute("Bearer " + ((HomeActivity) mContext).getAccessToken(), "" + disputeId).enqueue(new Callback<ApiResponse<DisputeDetails>>() {
            @Override
            public void onResponse(Call<ApiResponse<DisputeDetails>> call, Response<ApiResponse<DisputeDetails>> response) {
                mBinding.progressBar.setVisibility(View.GONE);
                if (response.isSuccessful() && response.code() == 201) {
                    DialogAlert.showSaleCancelSuccessfulDialog(mContext, mContext.getResources().getString(R.string.success), mContext.getResources().getString(R.string.dispute_successfully_canceled), false);
                    mCallback.popFragment();
                } else if (response.code() == 401) {
                    ((HomeActivity) mContext).getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            cancelDispute();
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    mBinding.progressBar.setVisibility(View.GONE);
                    ((HomeActivity) mContext).mAlertDialog.showDialog((HomeActivity) mContext, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<DisputeDetails>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                ((HomeActivity) mContext).mAlertDialog.showDialog((HomeActivity) mContext, getResources().getString(R.string.error_server));
            }
        });

    }

    public void error() {
        mBinding.progressBar.setVisibility(View.GONE);
    }

    public void setDisputeId(int disputeId) {
        this.disputeId = disputeId;
    }

    public void setStock(Stock stock) {
        this.stockDetail = stock;
    }
}
