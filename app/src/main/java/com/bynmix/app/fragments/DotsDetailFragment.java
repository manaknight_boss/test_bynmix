package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.AddDotsAdapter;
import com.bynmix.app.databinding.FragmentDotsDetailBinding;
import com.bynmix.app.interfaces.AddTagsListener;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;
import java.util.List;


public class DotsDetailFragment extends Fragment implements AddTagsListener {
    private FragmentDotsDetailBinding mBinding;
    private CreatePostListener mCallback;
    private Context mContext;
    private AddDotsAdapter mAdapter;
    private List<AnnotatedPoint> mDotsList = new ArrayList<>();
    private int postId = -1;
    private FeedResponse post = null;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (CreatePostListener) context;
        mContext = context;
    }

    public static DotsDetailFragment newInstance() {
        return new DotsDetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentDotsDetailBinding.inflate(inflater, container, false);
        if (postId != -1) {
            mBinding.toolbarHeading.setText(mContext.getString(R.string.edit_post_tag));
            if(postId != 0) {
                mBinding.editDots.setVisibility(View.VISIBLE);
            } else {
                mBinding.editDots.setVisibility(View.GONE);
            }
            mBinding.editDots.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mCallback.openEditPostDotsFragment(post);
                }
            });
        } else {
            mBinding.toolbarHeading.setText(mContext.getString(R.string.add_post_tags));
            mBinding.editDots.setVisibility(View.GONE);
        }
        if (mCallback.getAnnotatedPoints() != null) {
            mDotsList = mCallback.getAnnotatedPoints();
            setAdapter();
        }
        init();
        return mBinding.getRoot();
    }

    private void init() {
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
        mBinding.done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<AnnotatedPoint> points = mAdapter.getList();
                validatePointList(points);
                mCallback.setAnnotatedPoints(points);
                if (postId != -1) {
                    mCallback.onBackPressed();
                    if(postId == 0) {
                        mCallback.onBackPressed();
                    }
                } else {
                    mCallback.openAddPostDetailFragment();
                }

            }
        });
    }

    private void validatePointList(List<AnnotatedPoint> points) {
        for (AnnotatedPoint dotDetail : points) {
            if (dotDetail.getPostDotTypeId() == Constants.DOT_TYPE_ONLINE_RETAILER) {
                if (!dotDetail.getLink().startsWith("http")) {
                    dotDetail.setLink("http://" + dotDetail.getLink());
                }
            }
        }
    }

    private boolean isPointsListValid(List<AnnotatedPoint> points) {
        for (AnnotatedPoint dotDetail : points) {
            if (dotDetail.getPostDotTypeId() == Constants.DOT_TYPE_ONLINE_RETAILER) {
                if (!Utils.containsURL(dotDetail.getLink())) {
                    return false;
                }
            }
        }
        return true;
    }

    private void setAdapter() {
        mAdapter = new AddDotsAdapter(mContext, new ArrayList<>(mDotsList), this, mCallback);
        mBinding.recyclerView.setLayoutManager(new FlowLayoutManager());
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void changeSubmitButtonState(boolean enable) {
        mBinding.done.setBackground(enable ? mContext.getResources().getDrawable(R.drawable.purple_rounded_button) : mContext.getResources().getDrawable(R.drawable.disable_onboarding_button));
        mBinding.done.setEnabled(enable);
    }

    @Override
    public void onItemClick(FeedResponse feedResponse, int position) {
        mDotsList.get(position).setListingId(feedResponse.getId());
        mDotsList.get(position).setListingTitle(feedResponse.getTitle());
        mAdapter.notifyDataSetChanged();
    }

    public void setPostDots(List<AnnotatedPoint> mList) {
        mDotsList = mList;
        setAdapter();
    }

    public void setPost(FeedResponse post) {
        this.post = post;
        if(post != null) {
            this.postId = post.getId();
        } else {
            postId = -1;
        }
    }
}