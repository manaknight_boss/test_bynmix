package com.bynmix.app.fragments;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.bynmix.app.R;
import com.bynmix.app.adapter.AddListingColorAdapter;
import com.bynmix.app.databinding.FragmentEditListingDetailBinding;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.Brand;
import com.bynmix.app.models.Colors;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.utils.CustomTextDialog;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;
import java.util.List;

public class EditListingDetailFragment extends BaseFragment {

    private FragmentEditListingDetailBinding mBinding;
    private int listingId;
    private CreatePostListener mCallback;
    private Context mContext;
    private ListingDetail listingDetail;
    private CustomTextDialog errorDialog = new CustomTextDialog();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (CreatePostListener) mContext;
    }

    public static EditMyListingFragment newInstance() {
        return new EditMyListingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentEditListingDetailBinding.inflate(inflater, container, false);
        return mBinding.getRoot();
    }

    private void init() {
        mBinding.addCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openCategoryFragment(0, 0);
            }
        });

        mBinding.categoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openCategoryFragment(listingDetail.getCategory().getCategoryId(), listingDetail.getCategoryType().getCategoryTypeId());
            }
        });

        mBinding.addSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openSizeFragment(0, 0, null);
            }
        });

        mBinding.sizeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openSizeFragment(listingDetail.getSizeType().getSizeTypeId(), listingDetail.getSize().getSizeId(), listingDetail.getSizeHeader());
            }
        });

        mBinding.addColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mCallback.openColorFragment(getColorsIdsList());
            }
        });

        mBinding.addBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openBrandFragment(0);
            }
        });

        mBinding.brandLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openBrandFragment(listingDetail.getBrand().getBrandId());
            }
        });

        mBinding.updateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listingDetail != null)
                    sendData();
            }
        });
        updateUI();

        mBinding.switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    listingDetail.setConditionId(2);
                } else {
                    listingDetail.setConditionId(1);
                }
            }
        });
    }


    private void sendData() {
        if (listingDetail.getCategory() == null) {
            String error = getColorText(getString(R.string.Category));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (listingDetail.getSize() == null) {
            String error = getColorText(getString(R.string.size_text));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }


        if (listingDetail.getColors() == null) {
            String error = getColorText(getString(R.string.colors_text));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (listingDetail.getBrand() == null) {
            String error = getColorText(getString(R.string.brand_text));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }
        listingDetail.setCategoryTypeId(listingDetail.getCategoryType().getCategoryTypeId());
        listingDetail.setCategoryId(listingDetail.getCategory().getCategoryId());
        listingDetail.setSizeTypeId(listingDetail.getSizeType().getSizeTypeId());
        listingDetail.setSizeId(listingDetail.getSize().getSizeId());
        listingDetail.setBrandId(listingDetail.getBrand().getBrandId());
        listingDetail.setColorIds(getColorsIdsList());


        mCallback.putEditListing(listingId, listingDetail);

    }

    private List<Integer> getColorsIdsList() {
        List<Integer> colorIdsList = new ArrayList<>();
        if (listingDetail.getColors() != null && listingDetail.getColors().size() > 0) {
            for (int i = 0; i < listingDetail.getColors().size(); i++) {
                colorIdsList.add(listingDetail.getColors().get(i).getColorId());
            }
        }
        return colorIdsList;
    }

    private void updateUI() {
        if (listingDetail != null) {
            if (listingDetail.getCategory() != null) {
                mBinding.addCategory.setVisibility(View.GONE);
                mBinding.categoryLayout.setVisibility(View.VISIBLE);
                mBinding.category.setText(listingDetail.getCategory().getCategoryName());
                mBinding.categoryType.setText(listingDetail.getCategoryType().getCategoryTypeName());
            } else {
                mBinding.addCategory.setVisibility(View.VISIBLE);
                mBinding.categoryLayout.setVisibility(View.GONE);
            }

            if (listingDetail.getSize() != null) {
                mBinding.addSize.setVisibility(View.GONE);
                mBinding.sizeLayout.setVisibility(View.VISIBLE);
                mBinding.size.setText(listingDetail.getSize().getSizeName());
            } else {
                mBinding.addSize.setVisibility(View.VISIBLE);
                mBinding.sizeLayout.setVisibility(View.GONE);
            }

            if (listingDetail.getColors() != null && listingDetail.getColors().size() > 0) {
                mBinding.addColor.setVisibility(View.GONE);
                mBinding.colorRecyclerView.setVisibility(View.VISIBLE);
                setColors(listingDetail.getColors());
            } else {
                mBinding.addColor.setVisibility(View.VISIBLE);
                mBinding.colorRecyclerView.setVisibility(View.GONE);
            }

            if (listingDetail.getBrand() != null) {
                mBinding.addBrand.setVisibility(View.GONE);
                mBinding.brandLayout.setVisibility(View.VISIBLE);
                mBinding.brand.setText(listingDetail.getBrand().getBrandName());
            } else {
                mBinding.addBrand.setVisibility(View.VISIBLE);
                mBinding.brandLayout.setVisibility(View.GONE);
            }

            if (listingDetail.getCondition().getConditionId() == 1) {
                mBinding.switchButton.setChecked(false);
                listingDetail.setConditionId(1);
            } else {
                mBinding.switchButton.setChecked(true);
                listingDetail.setConditionId(2);
            }

        }

    }

    private void setColors(List<Colors> colors) {
        AddListingColorAdapter addListingColorAdapter = new AddListingColorAdapter(mContext, colors, mCallback, 1);
        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        mBinding.colorRecyclerView.setLayoutManager(flowLayoutManager);
        mBinding.colorRecyclerView.setAdapter(addListingColorAdapter);
        mBinding.colorRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.set(0, 0, 15, 0);
            }
        });
    }

    private String getColorText(String string) {
        return "<b>" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + string + "</font>" + "</b> ";
    }

    public void setColor(List<Colors> colorsList) {
        this.listingDetail.setColors(colorsList);
        updateUI();
    }

    public void setBrand(Brand brand) {
        this.listingDetail.setBrandId(brand.getBrandId());
        updateUI();
    }

    public void setId(int listingId) {
        this.listingId = listingId;
    }

    public void setData(int listingId, ListingDetail listingDetail) {
        this.listingDetail = listingDetail;
        this.listingId = listingId;
        init();
    }
}
