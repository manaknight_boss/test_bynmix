package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.FeedViewPagerAdapter;
import com.bynmix.app.databinding.AlertFragmentBinding;
import com.bynmix.app.databinding.EditProfileFragmentBinding;
import com.bynmix.app.databinding.FragmentEditListingDetailBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.models.ListingDisplayImage;
import com.bynmix.app.models.ShippingInfo;
import com.bynmix.app.models.Status;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditListingFragment extends BaseFragment {
    private EditProfileFragmentBinding mBinding;
    private int listingId;
    private CreatePostListener mCallback;
    private Context mContext;
    private ListingDetail listingDetail;
    private FeedViewPagerAdapter feedViewPagerAdapter;
    private CreateListingFragment photosFragment;
    private BasicListingInfoFragment basicInfoFragment;
    private EditListingDetailFragment detailFragment;
    private ShippingFragment shippingFragment;
    private List<Status> mListingStatusList = new ArrayList<>();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (CreatePostListener) context;
        mContext = context;
    }

    public static EditListingFragment newInstance() {
        return new EditListingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = EditProfileFragmentBinding.inflate(inflater, container, false);
        mBinding.toolbarHeading.setText(getString(R.string.edit_listing));
        mCallback.deletedListingImagesIdClear();
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
        if (listingDetail == null) {
            mCallback.getListingDetail(listingId);
        } else {
            listingDetail = mCallback.listingDetail();
            setViewPager();
        }
        if (mListingStatusList.size() == 0) {
            getListingStatus();
        }
        return mBinding.getRoot();
    }

    private void setViewPager() {
        if (feedViewPagerAdapter == null) {
            feedViewPagerAdapter = new FeedViewPagerAdapter(getChildFragmentManager());
            photosFragment = new CreateListingFragment();
            basicInfoFragment = new BasicListingInfoFragment();
            detailFragment = new EditListingDetailFragment();
            shippingFragment = new ShippingFragment();

            photosFragment.setType(Constants.EDIT);
            shippingFragment.setType(Constants.EDIT);

            feedViewPagerAdapter.addFragment(photosFragment, getResources().getString(R.string.photos));
            feedViewPagerAdapter.addFragment(basicInfoFragment, getResources().getString(R.string.basic));
            feedViewPagerAdapter.addFragment(detailFragment, getResources().getString(R.string.detail));
            feedViewPagerAdapter.addFragment(shippingFragment, getResources().getString(R.string.shipping_text));
            photosFragment.setData(listingId);
        }
        mBinding.viewpager.setAdapter(feedViewPagerAdapter);
        mBinding.viewpager.setOffscreenPageLimit(4);
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);

        mBinding.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {

                } else if (position == 1) {
                    basicInfoFragment.setData(listingId, listingDetail, mListingStatusList);
                } else if (position == 2) {
                    detailFragment.setData(listingId, listingDetail);
                } else if (position == 3) {
                    shippingFragment.setData(listingId, listingDetail);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }


    public void setId(int listingId) {
        this.listingId = listingId;
    }

    public void setImages(List<ListingDisplayImage> listingDisplayImages) {
        setViewPager();
        photosFragment.updatePhotos(listingDisplayImages);
    }

    public void updateList(List<String> compressedDisplayPic) {
        photosFragment.setPhoto(compressedDisplayPic);
    }

    public void getListingStatus() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getListingStatus("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<List<Status>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Status>>> call, Response<ApiResponse<List<Status>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mListingStatusList = response.body().getData();
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getListingStatus();
                        }

                        @Override
                        public void errorAccessToken() {
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<List<Status>>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    public void setData(ListingDetail listingDetail) {
        this.listingDetail = listingDetail;
        mCallback.getListingDetailImages(listingId);
    }

    public void updateInfo(List<ShippingInfo> shippingInfoList) {
        shippingFragment.updateInfo(shippingInfoList);
    }

    public void updateUI() {
        photosFragment.updateUI();
    }
}