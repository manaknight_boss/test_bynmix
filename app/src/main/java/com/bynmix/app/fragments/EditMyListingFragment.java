package com.bynmix.app.fragments;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.adapter.AddListingColorAdapter;
import com.bynmix.app.adapter.AddTagsAdapter;
import com.bynmix.app.adapter.EditPhotoListingAdapter;
import com.bynmix.app.adapter.ListingStatusAdapter;
import com.bynmix.app.databinding.EditMyListingFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.ConfirmInterface;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Brand;
import com.bynmix.app.models.Category;
import com.bynmix.app.models.Colors;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.models.ListingDisplayImage;
import com.bynmix.app.models.ShippingInfo;
import com.bynmix.app.models.Size;
import com.bynmix.app.models.Status;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.ConfirmDialog;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.DialogAlert;
import com.bynmix.app.utils.ErrorDialog;
import com.bynmix.app.utils.Utils;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditMyListingFragment extends BaseFragment {

    private EditMyListingFragmentBinding mBinding;
    private int listingId;
    private CreatePostListener mCallback;
    private Context mContext;
    private ListingDetail listingDetail;
    private List<ListingDisplayImage> mPhotoList = new ArrayList<>();
    private AddTagsAdapter mAdapter;
    private List<String> mTagsList = new ArrayList<>();
    private ErrorDialog errorDialog = new ErrorDialog();
    private EditPhotoListingAdapter selectPhotoAdapter;
    private List<Status> mListingStatusList = new ArrayList<>();
    private ListingStatusAdapter mListingStatusAdapter;
    private List<ShippingInfo> shippingInfoList = new ArrayList<>();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (CreatePostListener) mContext;
    }

    public static EditMyListingFragment newInstance() {
        return new EditMyListingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = EditMyListingFragmentBinding.inflate(inflater, container, false);
        if (mListingStatusList.size() == 0) {
            getListingStatus();
        }
        if (shippingInfoList.size() == 0) {
            getShippingInfo();
        }
        if (listingDetail == null) {
            mCallback.getListingDetail(listingId);
        } else {
            listingDetail = mCallback.listingDetail();
            mBinding.editListingMainLayout.setVisibility(View.VISIBLE);
            mBinding.directSwitchButton.setChecked(listingDetail.isBuyItNowOnly());
        }
        init();
        mCallback.deletedListingImagesIdClear();
        return mBinding.getRoot();
    }

    private void init() {
        setTagAdapter();
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.clearListDetail();
                mCallback.onBackPressed();
            }
        });
        mBinding.addCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openCategoryFragment(0, 0);
            }
        });

        mBinding.categoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openCategoryFragment(listingDetail.getCategory().getCategoryId(), listingDetail.getCategoryType().getCategoryTypeId());
            }
        });

        mBinding.addSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openSizeFragment(0, 0, null);
            }
        });

        mBinding.sizeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openSizeFragment(listingDetail.getSizeType().getSizeTypeId(), listingDetail.getSize().getSizeId(), listingDetail.getSizeHeader());
            }
        });

        mBinding.addColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mCallback.openColorFragment(getColorsIdsList());
            }
        });

        mBinding.addBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openBrandFragment(0);
            }
        });

        mBinding.brandLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openBrandFragment(listingDetail.getBrand().getBrandId());
            }
        });

        mBinding.updateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listingDetail != null)
                    sendData();
            }
        });

        mBinding.directSwitchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listingDetail.setBuyItNowOnly(false);
                setLowestPriceUI(false);
                if (mBinding.directSwitchButton.isChecked()) {
                    mBinding.directSwitchButton.setChecked(false);
                    DialogAlert.showDialog(mContext, mContext.getResources().getString(R.string.direct_purchase), mContext.getResources().getString(R.string.are_you_sure_want_to_change_direct_purchase), mContext.getResources().getString(R.string.convert_to_direct_purchase_detail), R.string.yes_converted_to_direct_purchase, new DialogClickListener() {
                        @Override
                        public void onClick() {
                            mBinding.directSwitchButton.setChecked(true);
                            listingDetail.setBuyItNowOnly(true);
                            setLowestPriceUI(true);
                        }
                    });
                }
            }
        });

        mListingStatusAdapter = new ListingStatusAdapter(mContext, R.layout.spinner_item, mListingStatusList);
        mBinding.listingStatus.setAdapter(mListingStatusAdapter);
        updateUI();

        mBinding.itemWeight.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable editable) {
                // TODO Auto-generated method stub

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newText = s.toString();
                if (newText.length() > 0) {
                    String weight = mBinding.itemWeight.getText().toString().trim();
                    try {
                        setWeight(Double.parseDouble(weight));
                    } catch (Exception ex) {
                        setWeight(0);
                    }
                } else {
                    updateShippingWeightUI(null, null);
                    listingDetail.setShippingInfo(null);
                }
            }
        });
        mBinding.switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    listingDetail.setConditionId(2);
                } else {
                    listingDetail.setConditionId(1);
                }
            }
        });
    }


    private void sendData() {
        String listingTitle, shortDescription, originalPrice, listingPrice, lowestAllowedPrice;
        listingTitle = mBinding.listingTitle.getText().toString().trim();
        shortDescription = mBinding.shortDescription.getText().toString().trim();
        originalPrice = mBinding.originalPrice.getText().toString().trim();
        listingPrice = mBinding.listingPrice.getText().toString().trim();
        int selectedId = mBinding.listingStatus.getSelectedItemPosition();
        int status = mListingStatusList.get(selectedId).getStatus();
        String weight = mBinding.itemWeight.getText().toString().trim();
        lowestAllowedPrice = mBinding.lowestAllowedPrice.getText().toString().trim();

        if (TextUtils.isEmpty(listingTitle)) {
            String error = getColorText(getString(R.string.listing_title_text));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (TextUtils.isEmpty(shortDescription)) {
            String error = getColorText(getString(R.string.short_description_text));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (mTagsList.size() == 0) {
            String error = getColorText(getString(R.string.listing_tag_text));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (TextUtils.isEmpty(originalPrice)) {
            String error = getColorText(getString(R.string.original_price));
            error = "An " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (TextUtils.isEmpty(listingPrice)) {
            String error = getColorText(getString(R.string.listing_price));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (!TextUtils.isEmpty(lowestAllowedPrice)) {
            if (Integer.parseInt(lowestAllowedPrice) > Double.parseDouble(listingPrice)) {
                ((HomeActivity) mContext).mAlertDialog.showDialog((HomeActivity) mContext, "Your lowest allowable offer must be less than the listing price.");
                return;
            }
        }

        if (TextUtils.isEmpty(weight)) {
            String error = getColorText(getString(R.string.item_weight));
            error = "An " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }


        if (listingDetail.getCategory() == null) {
            String error = getColorText(getString(R.string.Category));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (listingDetail.getSize() == null) {
            String error = getColorText(getString(R.string.size_text));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }


        if (listingDetail.getColors() == null) {
            String error = getColorText(getString(R.string.colors_text));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (listingDetail.getBrand() == null) {
            String error = getColorText(getString(R.string.brand_text));
            error = "A " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        if (mPhotoList.size() == 0) {
            String error = getColorText(getString(R.string.image));
            error = "An " + error + " " + getString(R.string.required);
            errorDialog.showDialog(mContext, error);
            return;
        }

        listingDetail.setOriginalPrice(Double.parseDouble(originalPrice));
        listingDetail.setPrice(Double.parseDouble(listingPrice));
        listingDetail.setTitle(listingTitle);
        listingDetail.setDescription(shortDescription);
        listingDetail.setStatusId(status);
        listingDetail.setTags(mTagsList);
        listingDetail.setAvailabilityId(1);
        listingDetail.setCategoryTypeId(listingDetail.getCategoryType().getCategoryTypeId());
        listingDetail.setCategoryId(listingDetail.getCategory().getCategoryId());
        listingDetail.setSizeTypeId(listingDetail.getSizeType().getSizeTypeId());
        listingDetail.setSizeId(listingDetail.getSize().getSizeId());
        listingDetail.setBrandId(listingDetail.getBrand().getBrandId());
        listingDetail.setColorIds(getColorsIdsList());
//        listingDetail.setShippingWeight(Double.parseDouble(weight));
        if (!TextUtils.isEmpty(lowestAllowedPrice)) {
            listingDetail.setLowestAllowableOffer(Integer.parseInt(lowestAllowedPrice));
        }
//        mCallback.putEditListing(listingId, listingDetail, mPhotoList);

    }

    private void setTagAdapter() {
        mAdapter = new AddTagsAdapter(mContext, mTagsList);
        mBinding.recycleView.setLayoutManager(new FlowLayoutManager());
        mBinding.recycleView.setAdapter(mAdapter);
        mBinding.recycleView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.set(15, 15, 15, 15);
            }
        });
        mCallback.clearListingImagesList();

    }

    private List<Integer> getColorsIdsList() {
        List<Integer> colorIdsList = new ArrayList<>();
        if (listingDetail.getColors() != null && listingDetail.getColors().size() > 0) {
            for (int i = 0; i < listingDetail.getColors().size(); i++) {
                colorIdsList.add(listingDetail.getColors().get(i).getColorId());
            }
        }
        return colorIdsList;
    }

    private void updateUI() {
        if (listingDetail != null) {
            if (listingDetail.getCategory() != null) {
                mBinding.addCategory.setVisibility(View.GONE);
                mBinding.categoryLayout.setVisibility(View.VISIBLE);
                mBinding.category.setText(listingDetail.getCategory().getCategoryName());
                mBinding.categoryType.setText(listingDetail.getCategoryType().getCategoryTypeName());
            } else {
                mBinding.addCategory.setVisibility(View.VISIBLE);
                mBinding.categoryLayout.setVisibility(View.GONE);
            }

            if (listingDetail.getSize() != null) {
                mBinding.addSize.setVisibility(View.GONE);
                mBinding.sizeLayout.setVisibility(View.VISIBLE);
                mBinding.size.setText(listingDetail.getSize().getSizeName());
            } else {
                mBinding.addSize.setVisibility(View.VISIBLE);
                mBinding.sizeLayout.setVisibility(View.GONE);
            }

            if (listingDetail.getColors() != null && listingDetail.getColors().size() > 0) {
                mBinding.addColor.setVisibility(View.GONE);
                mBinding.colorRecyclerView.setVisibility(View.VISIBLE);
                setColors(listingDetail.getColors());
            } else {
                mBinding.addColor.setVisibility(View.VISIBLE);
                mBinding.colorRecyclerView.setVisibility(View.GONE);
            }

            if (listingDetail.getBrand() != null) {
                mBinding.addBrand.setVisibility(View.GONE);
                mBinding.brandLayout.setVisibility(View.VISIBLE);
                mBinding.brand.setText(listingDetail.getBrand().getBrandName());
            } else {
                mBinding.addBrand.setVisibility(View.VISIBLE);
                mBinding.brandLayout.setVisibility(View.GONE);
            }

            if (listingDetail.getCondition().getConditionId() == 1) {
                mBinding.switchButton.setChecked(false);
                listingDetail.setConditionId(1);
            } else {
                mBinding.switchButton.setChecked(true);
                listingDetail.setConditionId(2);
            }
            setLowestPriceUI(listingDetail.isBuyItNowOnly());

        }

    }

    private void setColors(List<Colors> colors) {
        AddListingColorAdapter addListingColorAdapter = new AddListingColorAdapter(mContext, colors, mCallback, 1);
        FlowLayoutManager flowLayoutManager = new FlowLayoutManager();
        flowLayoutManager.setAutoMeasureEnabled(true);
        mBinding.colorRecyclerView.setLayoutManager(flowLayoutManager);
        mBinding.colorRecyclerView.setAdapter(addListingColorAdapter);
        mBinding.colorRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.set(0, 0, 15, 0);
            }
        });
    }

    private String getColorText(String string) {
        return "<b>" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + string + "</font>" + "</b> ";
    }

    public void setColor(List<Colors> colorsList) {
        this.listingDetail.setColors(colorsList);
        updateUI();
    }

    public void setBrand(Brand brand) {
        this.listingDetail.setBrandId(brand.getBrandId());
        updateUI();
    }

    public void moveScrollDown() {
        mBinding.scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {
                mBinding.scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        }, 200);
    }

    public void checkOnBackPress() {
        Category category = listingDetail.getCategory();
        Size size = listingDetail.getSize();
        List<Colors> colors = listingDetail.getColors();
        Brand brand = listingDetail.getBrand();
        String listingTitle = mBinding.listingTitle.getText().toString().trim();
        String shortDescription = mBinding.shortDescription.getText().toString().trim();
        String orginalPrice = mBinding.originalPrice.getText().toString().trim();
        String listingPrice = mBinding.listingPrice.getText().toString().trim();
        if (!TextUtils.isEmpty(listingTitle) || !TextUtils.isEmpty(shortDescription)
                || !TextUtils.isEmpty(orginalPrice) ||
                !TextUtils.isEmpty(listingPrice) || category != null ||
                size != null || brand != null || colors != null || mTagsList.size() > 0) {
            ConfirmDialog confirmDialog = new ConfirmDialog();
            confirmDialog.showDialog(mContext, new ConfirmInterface() {
                @Override
                public void confirm() {
                    mCallback.clearListDetail();
                    mCallback.popFragment();
                }
            });

        } else {
            mCallback.clearListDetail();
            mCallback.popFragment();
        }
    }


    public void setData(ListingDetail listingDetail) {
        this.listingDetail = listingDetail;
        mCallback.getListingDetailImages(listingId);
        for (int i = 0; i < listingDetail.getListingTags().size(); i++) {
            mTagsList.add(listingDetail.getListingTags().get(i).getTagName());
        }
        mAdapter.setList(mTagsList);
        mListingStatusAdapter.setList(mListingStatusList);
        mBinding.directSwitchButton.setChecked(listingDetail.isBuyItNowOnly());
        setListingDetail();
    }

    private void setListingDetail() {
        mBinding.listingTitle.setText(listingDetail.getTitle());
        mBinding.shortDescription.setText(listingDetail.getDescription());
        mBinding.originalPrice.setText(String.format("%s", listingDetail.getOriginalPrice()));
        mBinding.listingPrice.setText(String.format("%s", listingDetail.getPrice()));
        mBinding.itemWeight.setText("" + listingDetail.getShippingWeight());
        mBinding.listingStatus.setSelection(listingDetail.getStatus().getStatus() - 1);
        mBinding.lowestAllowedPrice.setText(String.format("%s", listingDetail.getLowestAllowableOffer()));
        updateUI();
        setWeight(listingDetail.getShippingWeight());
        for (int i = 1; i < mListingStatusList.size(); i++) {
            if (mListingStatusList.get(i).getStatus() == listingDetail.getStatus().getListingStatusId()) {
                mBinding.listingStatus.setSelection(i);
                break;
            }
        }
    }

    public void setId(int listingId) {
        this.listingId = listingId;
    }

    public void setImages(List<ListingDisplayImage> listingDisplayImages) {
        mBinding.editListingMainLayout.setVisibility(View.VISIBLE);
        listingDetail.setListingDisplayImages(listingDisplayImages);
        this.mPhotoList = listingDisplayImages;
        selectPhotoAdapter.setList(mPhotoList);
    }

    private void setLowestPriceUI(boolean isChecked) {
        if (isChecked) {
            mBinding.lowestAllowedPriceLayout.setVisibility(View.GONE);
        } else {
            mBinding.lowestAllowedPriceLayout.setVisibility(View.VISIBLE);
        }
    }

    public void updateInfo(List<ShippingInfo> shippingInfoList) {
        this.shippingInfoList = shippingInfoList;
    }

    private void setWeight(double weight) {
        if (shippingInfoList.size() > 0) {
            if (weight <= shippingInfoList.get(0).getShippingWeightMax()) {
                String priorityRate = getColorText(getString(R.string.priority_first_rate));
                priorityRate = getString(R.string.based_on_item_weight) + " " + priorityRate;
                listingDetail.setShippingWeight(weight);
                listingDetail.setShippingRateId(shippingInfoList.get(0).getShippingRateId());
                listingDetail.setShippingInfo(shippingInfoList.get(0));
                updateShippingWeightUI(mBinding.firstOption, priorityRate);
            } else if (weight <= shippingInfoList.get(1).getShippingWeightMax()) {
                String priorityRate = getColorText(getString(R.string.priority_second_rate));
                priorityRate = getString(R.string.based_on_item_weight) + " " + priorityRate;
                listingDetail.setShippingWeight(weight);
                listingDetail.setShippingInfo(shippingInfoList.get(1));
                listingDetail.setShippingRateId(shippingInfoList.get(1).getShippingRateId());
                updateShippingWeightUI(mBinding.secondOption, priorityRate);
            } else if (weight <= shippingInfoList.get(2).getShippingWeightMax()) {
                String priorityRate = getColorText(getString(R.string.priority_third_rate));
                priorityRate = getString(R.string.based_on_item_weight) + " " + priorityRate;
                listingDetail.setShippingWeight(weight);
                listingDetail.setShippingRateId(shippingInfoList.get(2).getShippingRateId());
                listingDetail.setShippingInfo(shippingInfoList.get(2));
                updateShippingWeightUI(mBinding.thirdOption, priorityRate);
            } else {
                String priorityRate = getColorText(getString(R.string.priority_fourth_rate));
                priorityRate = getString(R.string.based_on_item_weight) + " " + priorityRate;
                listingDetail.setShippingWeight(weight);
                listingDetail.setShippingRateId(shippingInfoList.get(3).getShippingRateId());
                listingDetail.setShippingInfo(shippingInfoList.get(3));
                updateShippingWeightUI(mBinding.fourthOption, priorityRate);
            }
        }
    }

    private void updateShippingWeightUI(RelativeLayout selectedView, String priorityRate) {
        mBinding.firstOption.setBackground(mContext.getResources().getDrawable(R.drawable.white_rectangle_background));
        mBinding.secondOption.setBackground(mContext.getResources().getDrawable(R.drawable.white_rectangle_background));
        mBinding.thirdOption.setBackground(mContext.getResources().getDrawable(R.drawable.white_rectangle_background));
        mBinding.fourthOption.setBackground(mContext.getResources().getDrawable(R.drawable.white_rectangle_background));
        if (selectedView != null) {
            selectedView.setBackground(mContext.getResources().getDrawable(R.drawable.purple_line_shipping));
            mBinding.priorityRate.setText(Html.fromHtml(priorityRate), TextView.BufferType.SPANNABLE);
            mBinding.priorityRate.setVisibility(View.VISIBLE);
        } else {
            mBinding.priorityRate.setVisibility(View.GONE);
        }
    }

    public void setStatusList(List<Status> statuses) {
        this.mListingStatusList = statuses;
        mListingStatusAdapter.setList(mListingStatusList);

    }

    public void updateList(List<String> photoList) {
        for (int i = 0; i < photoList.size(); i++) {
            ListingDisplayImage listingDisplayImage = new ListingDisplayImage();
            listingDisplayImage.setImageUrl(photoList.get(i));
            listingDisplayImage.setType(Constants.LOCAL_PHOTO);
            mPhotoList.add(listingDisplayImage);
            Log.d("msg", "" + photoList.get(i));
        }
        selectPhotoAdapter.setList(mPhotoList);
    }

    public void getListingStatus() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getListingStatus("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<List<Status>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Status>>> call, Response<ApiResponse<List<Status>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    List<Status> statuses = response.body().getData();
                    setStatusList(statuses);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getListingStatus();
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ApiResponse<List<Status>>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }
}
