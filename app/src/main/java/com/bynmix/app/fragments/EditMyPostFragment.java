package com.bynmix.app.fragments;

import android.content.Context;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.adapter.AddTagsAdapter;
import com.bynmix.app.databinding.EditMyPostFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.models.PostDetail;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.CustomTextDialog;
import com.bynmix.app.utils.DeleteImageAndDotsDialog;
import com.bynmix.app.utils.PostBlogTypeError;
import com.bynmix.app.utils.Utils;
import com.squareup.picasso.Picasso;
import com.xiaofeng.flowlayoutmanager.FlowLayoutManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditMyPostFragment extends BaseFragment implements DialogClickListener {

    private EditMyPostFragmentBinding mBinding;
    private CreatePostListener mCallback;
    private Context mContext;
    private String photoUrl;
    private String oldPhotoUrl;
    private AddTagsAdapter mAdapter;
    private List<String> mTagsList = new ArrayList<>();
    private List<String> postTypeList = new ArrayList<>();
    private FeedResponse post;
    private CustomTextDialog mAddPostErrorDialog;
    private PostBlogTypeError mPostBlogTypeError;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (CreatePostListener) context;
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCallback.setAnnotatedPoints(null);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mCallback.isEditPost()) {
            if(((HomeActivity)mContext).isImageUsed()) {
                this.photoUrl = mCallback.getPostImage();
            } else {
                this.photoUrl = null;
            }
            updateUI();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = EditMyPostFragmentBinding.inflate(inflater, container, false);

        mBinding.shortDescription.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                if (mBinding.shortDescription.hasFocus()) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK){
                        case MotionEvent.ACTION_SCROLL:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            return true;
                    }
                }
                return false;
            }
        });

        mAddPostErrorDialog = new CustomTextDialog();
        mPostBlogTypeError = new PostBlogTypeError();
        List<AnnotatedPoint> dots = mCallback.getAnnotatedPoints();
        if(dots != null && dots.size() > 0) {
            dots = filterPointsList(dots);
            mCallback.setAnnotatedPoints(dots);
            if(dots.size() > 0) {
                post.setHasPostDots(true);
                post.setPostDotsCount(dots.size());
            }
        }
        if(post.isHasPostDots()) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
            mBinding.postDetailFormLayout.setVisibility(View.GONE);
            if(dots == null || dots.size() == 0) {
                if (mCallback.conditionsForApiCall()) {
                    getMyEditPost(post.getId());
                }
            } else {
                setEditPost();
            }
        } else {
            setEditPost();
        }

        //getMyEditPost(post.getId());
        /*if (dots == null || dots.size() == 0) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
            mBinding.postDetailFormLayout.setVisibility(View.GONE);
            if (mCallback.conditionsForApiCall()) {
                getMyEditPost(post.getId());
            }
        } else {
            if (mCallback.isEditPost()) {
                post.setHasPostDots(true);
                post.setPostDotsCount(dots.size());
            }
            setEditPost();
        }*/
        addPostType();
        init();
        mBinding.crossIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (post.isHasPostDots()) {
                    DeleteImageAndDotsDialog.showDialog(mContext, EditMyPostFragment.this);
                } else {
                    photoUrl = null;
                    updateUI();
                }
            }
        });
        return mBinding.getRoot();
    }

    private void addPostType() {
        postTypeList.add(getString(R.string.blog_post));
        postTypeList.add(getString(R.string.video_post));
    }

    private List<AnnotatedPoint> filterPointsList(List<AnnotatedPoint> mPointList) {
        List<AnnotatedPoint> invalidPoints = new ArrayList<>();
        for(AnnotatedPoint point : mPointList) {
            if(point.getListingId() == 0 && TextUtils.isEmpty(point.getTitle())) {
                invalidPoints.add(point);
            }
        }
        mPointList.removeAll(invalidPoints);
        return mPointList;
    }

    private void init() {
        ArrayAdapter<String> postTypeAdapter = new ArrayAdapter<String>(mContext, R.layout.spinner_item, postTypeList);
        mBinding.postType.setAdapter(postTypeAdapter);
        mBinding.addPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openGalleryFor(Constants.EDIT_POST, 1);
            }
        });

        mBinding.create.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setData();
            }
        });
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
    }

    private void setEditPost() {
        this.photoUrl = post.getPhotoUrl();
        this.oldPhotoUrl = post.getPhotoUrl();
        mTagsList.clear();
        this.mTagsList.addAll(post.getPostTags());
        setAdapter();
        mBinding.progressBar.setVisibility(View.GONE);
        mBinding.postDetailFormLayout.setVisibility(View.VISIBLE);
        mBinding.postTitle.setText(post.getTitle());
        mBinding.postLink.setText(post.getLink());
        mBinding.shortDescription.setText(post.getDescription());
        if (post.getPostType().equalsIgnoreCase("VideoPost")) {
            mBinding.postType.setSelection(1);
        } else {
            mBinding.postType.setSelection(0);
        }
        if (photoUrl != null) {
            mBinding.addPhoto.setVisibility(View.GONE);
            mBinding.selectedPhotoLayout.setVisibility(View.VISIBLE);
            Picasso.get().load(photoUrl).into(mBinding.postPhoto);
        }
        setEditTagsButton();
    }

    private void setEditTagsButton() {
        if (post.isHasPostDots()) {
            mBinding.editTags.setVisibility(View.VISIBLE);
            mBinding.editTags.setText(getString(R.string.edit_post_tags) + " (" + post.getPostDotsCount() + ")");
            mBinding.editTags.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //mCallback.openEditPostDotsFragment(post);
                    mCallback.dotsDetailFragment(post);
                }
            });
        } else {
            if(photoUrl != null) {
                mBinding.editTags.setVisibility(View.VISIBLE);
                mBinding.editTags.setText(getString(R.string.add_product_tags));
                mBinding.editTags.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AddDotsFragment fragment = AddDotsFragment.newInstance();
                        fragment.setPhotoUrl(photoUrl);
                        ((HomeActivity) mCallback).pushFragment(fragment);
                    }
                });
            } else {
                mBinding.editTags.setVisibility(View.GONE);
            }
        }
    }

    private void setAdapter() {
        mAdapter = new AddTagsAdapter(mContext, mTagsList);
        mBinding.recycleView.setLayoutManager(new FlowLayoutManager());
        mBinding.recycleView.setAdapter(mAdapter);
        mBinding.recycleView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.set(15, 15, 15, 15);
            }
        });
    }

    private void setData() {
        String postTitle, postLink, shortDiscription;
        postTitle = mBinding.postTitle.getText().toString().trim();
        int postType = mBinding.postType.getSelectedItemPosition() + 1;
        postLink = mBinding.postLink.getText().toString().trim();
        shortDiscription = mBinding.shortDescription.getText().toString().trim();

        if (TextUtils.isEmpty(photoUrl)) {
            String message = Utils.getColorText(getString(R.string.post_photo), mContext.getResources().getColor(R.color.very_light_purple));
            message = "A " + message + " " + getString(R.string.required);
            mAddPostErrorDialog.showDialog(mContext, message);
            return;
        }

        if (TextUtils.isEmpty(postTitle)) {
            String message = Utils.getColorText(getString(R.string.post_title_text), mContext.getResources().getColor(R.color.very_light_purple));
            message = "A " + message + " " + getString(R.string.required);
            mAddPostErrorDialog.showDialog(mContext, message);
            return;
        }

        if (postType == 0) {
            String message = Utils.getColorText(getString(R.string.post_type_text), mContext.getResources().getColor(R.color.very_light_purple));
            message = "A " + message + " " + getString(R.string.required);
            mAddPostErrorDialog.showDialog(mContext, message);
            return;
        }

        if (TextUtils.isEmpty(postLink)) {
            String message = Utils.getColorText(getString(R.string.post_link_text), mContext.getResources().getColor(R.color.very_light_purple));
            message = "A " + message + " " + getString(R.string.required);
            mAddPostErrorDialog.showDialog(mContext, message);
            return;
        }

        if (!Utils.containsURL(postLink)) {
            String message = Utils.getColorText(getString(R.string.post_link_text), mContext.getResources().getColor(R.color.very_light_purple));
            message = message + " " + getString(R.string.start_with_http);
            mAddPostErrorDialog.showDialog(mContext, message);
            return;
        }

        if (TextUtils.isEmpty(shortDiscription)) {
            String message = Utils.getColorText(getString(R.string.short_description_text), mContext.getResources().getColor(R.color.very_light_purple));
            message = message + " " + getString(R.string.required);
            mAddPostErrorDialog.showDialog(mContext, message);
            return;
        }

        if (mTagsList.size() == 0) {
            String message = Utils.getColorText(getString(R.string.tag), mContext.getResources().getColor(R.color.very_light_purple));
            message = message + " " + getString(R.string.required);
            mAddPostErrorDialog.showDialog(mContext, message);
            return;
        }

        if (!postLink.startsWith("http")) {
            postLink = "http://" + postLink;
        }

        post.setTitle(postTitle);
        post.setShortDescription(shortDiscription);
        post.setPostType("" + postType);
        post.setLink(postLink);
        post.setTags(mTagsList);

        sendData(post);

    }

    private void sendData(FeedResponse post) {

        boolean isYouTubeLink = extractYouTubeUrlId(post.getLink());
        int postType = Integer.parseInt(post.getPostType());
        if (isYouTubeLink && postType == Constants.WEB_LINK) {
            postLinkError(postType);

        } else if (!isYouTubeLink && postType == 2) {

            postLinkError(postType);
        } else {
            if (mCallback.conditionsForApiCall()) {
                mCallback.editPostDetail(post, photoUrl, oldPhotoUrl);
            }
        }

    }

    private void postLinkError(int type) {
        final String postType, description, confirm, cancel;
        if (type == Constants.WEB_LINK) {
            postType = getString(R.string.blog_post);
            description = getString(R.string.non_video_error);
            confirm = getString(R.string.non_video_confirm);
            cancel = getString(R.string.non_video_cancel);
        } else {
            postType = getString(R.string.video_post);
            description = getString(R.string.video_error);
            confirm = getString(R.string.video_confirm);
            cancel = getString(R.string.video_cancel);
        }
        mPostBlogTypeError.showDialog(mContext, mCallback, postType, description, confirm, cancel, post, photoUrl, oldPhotoUrl);
    }

    private boolean extractYouTubeUrlId(String youtubeUrl) {
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|\\/e\\/|watch\\?v%3D|watch\\?feature=player_embedded&v=|%2Fvideos%2F|embed%\u200C\u200B2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";

        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(youtubeUrl);
        if (matcher.find()) {
            return true;
        }
        return false;
    }

    private void updateUI() {
        if (photoUrl != null) {
            mBinding.addPhoto.setVisibility(View.GONE);
            mBinding.selectedPhotoLayout.setVisibility(View.VISIBLE);
            File fLocal = new File(photoUrl);
            try {
                Picasso.get().load(fLocal).into(mBinding.postPhoto);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            mBinding.editTags.setVisibility(View.VISIBLE);
            mBinding.editTags.setText(getString(R.string.add_product_tags));
            mBinding.editTags.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AddDotsFragment fragment = AddDotsFragment.newInstance();
                    fragment.setPhotoUrl(photoUrl);
                    ((HomeActivity) mCallback).pushFragment(fragment);
                }
            });
        } else {
            mBinding.addPhoto.setVisibility(View.VISIBLE);
            mBinding.selectedPhotoLayout.setVisibility(View.GONE);
            mBinding.editTags.setVisibility(View.GONE);
        }
        setEditTagsButton();
    }

    public static EditMyPostFragment newInstance() {
        return new EditMyPostFragment();
    }


    @Override
    public void onClick() {
        photoUrl = null;
        mBinding.editTags.setVisibility(View.GONE);
        post.setHasPostDots(false);
        post.setPostDotsCount(0);
        post.setPhotoUrl(null);
        updateUI();
    }

    public void setPost(FeedResponse post) {
        this.post = (FeedResponse)post.clone();
    }

    public void setPostDotCount(int postDotsCount) {
        post.setPostDotsCount(postDotsCount);
        setEditPost();
    }

    public void getMyEditPost(final int postId) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getMyEditPost("Bearer " + getAccessToken(), "" + postId).enqueue(new Callback<ApiResponse<PostDetail>>() {
            @Override
            public void onResponse(Call<ApiResponse<PostDetail>> call, Response<ApiResponse<PostDetail>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    PostDetail data = response.body().getData();
                    if (data.getPostDots().size() > 0) {
                        List<AnnotatedPoint> postDots = data.getPostDots();
                        mCallback.setAnnotatedPoints(postDots);
                    }
                    setPostDotCount(data.getPostDotsCount());
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getMyEditPost(postId);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<PostDetail>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }
}

