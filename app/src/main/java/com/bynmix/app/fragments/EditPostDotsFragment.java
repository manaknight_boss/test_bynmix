package com.bynmix.app.fragments;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FragmentEditPostDotsBinding;
import com.bynmix.app.interfaces.AnnotatIVInterface;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.models.FeedResponse;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class EditPostDotsFragment extends Fragment {
    FragmentEditPostDotsBinding mBinding;
    private Context mContext;
    private CreatePostListener mCallback;
    private List<AnnotatedPoint> mPointList;
    private Vibrator vibe;
    private boolean isTagsSelected = false;
    private FeedResponse post;


    public static EditPostDotsFragment newInstance() {
        return new EditPostDotsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_post_dots, container, false);
        vibe = (Vibrator) mContext.getSystemService(Context.VIBRATOR_SERVICE);
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        inits();

        return mBinding.getRoot();
    }

    private void inits() {
        mBinding.postImage.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mBinding.postImage.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                String photoUrl = post.getPhotoUrl();
                String postImage = mCallback.getPostImage();
                if (!TextUtils.isEmpty(postImage)) {
                    File fLocal = new File(mCallback.getPostImage());
                    try {
                        Picasso.get().load(fLocal).into(mBinding.postImage);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                } else {
                    try {
                        Picasso.get().load(photoUrl).into(mBinding.postImage);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        });
        mPointList = mCallback.getAnnotatedPoints();
        mBinding.postImage.setPointList(mPointList);
        mBinding.postImage.setListener(new AnnotatIVInterface() {
            @Override
            public void add(AnnotatedPoint annotatedPoint) {
                mPointList.add(annotatedPoint);
                mBinding.postImage.setPointList(mPointList);
            }

            @Override
            public void update(AnnotatedPoint annotatedPoint) {
                mBinding.postImage.setPointList(mPointList);
            }

            @Override
            public void remove(AnnotatedPoint annotatedPoint) {
                mPointList.remove(annotatedPoint);
                vibe.vibrate(100);
                if (annotatedPoint.getPostDotId() != 0) {
                    annotatedPoint.setDeleted(true);
                    mCallback.addDeletedDots(annotatedPoint);
                }
                mBinding.postImage.setPointList(mPointList);
            }

            @Override
            public void changeScrollStatus(boolean enable) {
                //todo
            }
        });
        mBinding.addTabs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTags();
            }
        });
        mBinding.check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isTagsSelected) {
                    if (mPointList.size() == 0) {
                        Toast.makeText(mContext, "Please add tags first.", Toast.LENGTH_SHORT).show();
                    } else {
                        //TODO move list to next screen
                        post.setPostDots(mPointList);
                        mCallback.setAnnotatedPoints(mPointList);
                        //mCallback.dotsDetailFragment(post.getId());
                        mCallback.popFragment();
                    }
                }
            }
        });
        if (mPointList.size() > 0) {
            addTags();
        }

    }

    private void addTags() {
        mBinding.addTagsTextPost.setText("Edit product tags of your post");
        mBinding.check.setVisibility(View.VISIBLE);
        mBinding.addTabs.setVisibility(View.GONE);
        isTagsSelected = true;
        mBinding.postImage.setMoveDots(true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (CreatePostListener) mContext;
    }

    public void setPostDetail(FeedResponse post) {
        this.post = post;
    }

}
