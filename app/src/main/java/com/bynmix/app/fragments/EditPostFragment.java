package com.bynmix.app.fragments;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.adapter.EditDeletePostAdapter;
import com.bynmix.app.databinding.EditPostFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.EditPostListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.models.Feeds;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.SavingPostDialog;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditPostFragment extends BaseFragment implements EditPostListener {
    private EditPostFragmentBinding mBinding;
    private List<FeedResponse> mPostFeedList = new ArrayList<>();
    private EditDeletePostAdapter adapter;
    private Context mContext;
    private ProfileListener mCallback;
    private boolean isAlreadyCalled;
    private int currentPageNumber;
    private int totalPages;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (ProfileListener) context;
    }

    public static EditPostFragment newInstance() {
        return new EditPostFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = EditPostFragmentBinding.inflate(inflater, container, false);
        adapter = new EditDeletePostAdapter(mContext, mPostFeedList, mCallback, this);
        mBinding.filter.setVisibility(View.GONE);
        if (mPostFeedList.size() == 0) {
            getMyPosts();
        }
        ((HomeActivity)mContext).setEditPost(false);
        mBinding.noItemLayout.setVisibility(View.GONE);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(adapter);
        setProgressBarColor();
        mBinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mCallback.conditionsForApiCall()) {
                    currentPageNumber = 0;
                    getMyPosts();
                } else {
                    hideSwipeRefresh();
                }
            }
        });

        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();
                boolean endHasBeenReached = lastVisible >= totalItemCount - 4;
                if (totalItemCount > 0 && endHasBeenReached) {
                    if (!isAlreadyCalled && currentPageNumber < totalPages && mPostFeedList.size() > 0) {
                        isAlreadyCalled = true;
                        getMyPosts();
                    }
                }

            }
        });

        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        return mBinding.getRoot();
    }

    private void setProgressBarColor() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Drawable wrapDrawable = DrawableCompat.wrap(mBinding.progressBar.getIndeterminateDrawable());
            DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(mContext, R.color.purple));
            mBinding.progressBar.setIndeterminateDrawable(DrawableCompat.unwrap(wrapDrawable));
        } else {
            mBinding.progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(mContext, R.color.purple), PorterDuff.Mode.SRC_IN);
        }
    }

    private void getMyPosts() {
        setLoadMoreProgress();
        getMyPost(currentPageNumber);
    }

    private void hideSwipeRefresh() {
        mBinding.swipeRefreshLayout.setRefreshing(false);
    }


    public void updateMyPostFeeds(List<FeedResponse> postFeeds, int currentPageNumber, int totalPages) {
        this.currentPageNumber = currentPageNumber;
        this.totalPages = totalPages;
        if (currentPageNumber > 1) {
            this.mPostFeedList.addAll(postFeeds);
        } else {
            this.mPostFeedList = postFeeds;
        }
        isAlreadyCalled = false;
        setLoadMoreProgress();
        adapter.setList(mPostFeedList);
        hideSwipeRefresh();

    }

    public void noItemFound() {
        mBinding.progressBar.setVisibility(View.GONE);
        hideSwipeRefresh();
        mBinding.noItemLayout.setVisibility(View.VISIBLE);
        mBinding.errorText.setText(getString(R.string.my_post_error));
    }

    public void error() {
        mBinding.progressBar.setVisibility(View.GONE);
        hideSwipeRefresh();
        mBinding.noItemLayout.setVisibility(View.VISIBLE);
        mBinding.errorText.setText(getString(R.string.something_went_wrong));
    }

    private void setLoadMoreProgress() {
        if (mPostFeedList.size() == 0) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
        } else {
            mBinding.progressBar.setVisibility(View.GONE);
        }
        if (isAlreadyCalled) {
            adapter.setProgress(true);
        } else {
            adapter.setProgress(false);
        }
    }

    public void deletePost(int position) {
        mPostFeedList.remove(position);
        adapter.notifyDataSetChanged();
    }

    public void updatePost(int position, int type) {
        if (type == Constants.DISABLE) {
            mPostFeedList.get(position).setActive(false);
        } else {
            mPostFeedList.get(position).setActive(true);
        }
        adapter.notifyDataSetChanged();
    }

    public void setPostDotsTags(List<AnnotatedPoint> mList, int position) {
        mPostFeedList.get(position).setPostDots(mList);
        adapter.setList(mPostFeedList);
        adapter.hideProgress();
        adapter.notifyDataSetChanged();

    }

    public void getMyPost(final int currentPageNumber) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getMyPostFeeds("Bearer " + getAccessToken(), "desc", currentPageNumber + 1).enqueue(new Callback<ApiResponse<Feeds>>() {
            @Override
            public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    Feeds feeds = response.body().getData();
                    int currentPage = feeds.getPageNumber();
                    int totalPages = feeds.getTotalPages();
                    List<FeedResponse> myPosts = feeds.getItems();
                    updateMyPostFeeds(myPosts, currentPage, totalPages);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {

                            getMyPost(currentPageNumber);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else if (response.code() == 404) {
                    noItemFound();

                } else {
                    error();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    public void disablePost(final int id, final int position, final int type) {
        String title;
        if (type == Constants.DISABLE) {
            title = getString(R.string.disable_post);
        } else {
            title = getString(R.string.enable_post);
        }
        final SavingPostDialog loading = new SavingPostDialog(mContext, title);
        loading.showDialog();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.enableDisablePost("Bearer " + getAccessToken(), "" + id).enqueue(new Callback<ApiResponse<Feeds>>() {
            @Override
            public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    loading.dismiss();
                    updatePost(position, type);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            disablePost(id, position, type);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                            loading.dismiss();
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    public void deletePost(final int id, final int position) {
        final SavingPostDialog loading = new SavingPostDialog(mContext, getString(R.string.deleting_post));
        loading.showDialog();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.deletePost("Bearer " + getAccessToken(), "" + id).enqueue(new Callback<ApiResponse<Feeds>>() {
            @Override
            public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    loading.dismiss();
                   deletePost(position);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            deletePost(id, position);
                        }

                        @Override
                        public void errorAccessToken() {
                            loading.dismiss();
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                }  else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }
}
