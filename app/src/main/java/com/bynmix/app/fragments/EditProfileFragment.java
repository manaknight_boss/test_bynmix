package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.FeedViewPagerAdapter;
import com.bynmix.app.databinding.EditProfileFragmentBinding;
import com.bynmix.app.interfaces.EditProfileListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.BodyType;
import com.bynmix.app.models.States;
import com.bynmix.app.models.UserResponse;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Utils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileFragment extends BaseFragment implements EditProfileListener {

    private EditProfileFragmentBinding mBinding;
    private ProfileListener mCallback;
    private UserResponse user;
    private Context mContext;
    private FeedViewPagerAdapter feedViewPagerAdapter;
    private BasicInfoFragment basicInfoFragment;
    private FragmentAboutMe fragmentAboutMe;
    private SocialLinksFragment socialLinksFragment;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (ProfileListener) context;
        mContext = context;
    }

    public static EditProfileFragment newInstance() {
        return new EditProfileFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = EditProfileFragmentBinding.inflate(inflater, container, false);
        setViewPager();
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
        return mBinding.getRoot();
    }

    private void setViewPager() {
        if (feedViewPagerAdapter == null) {
            feedViewPagerAdapter = new FeedViewPagerAdapter(getChildFragmentManager());
            basicInfoFragment = new BasicInfoFragment();
            fragmentAboutMe = new FragmentAboutMe();
            socialLinksFragment = new SocialLinksFragment();
            feedViewPagerAdapter.addFragment(basicInfoFragment, getResources().getString(R.string.basic_info));
            feedViewPagerAdapter.addFragment(fragmentAboutMe, getResources().getString(R.string.about_me));
            feedViewPagerAdapter.addFragment(socialLinksFragment, getResources().getString(R.string.social));
            basicInfoFragment.setListener(this, user);
            fragmentAboutMe.setListener(this, user);
            socialLinksFragment.setListener(this, user);
        }
        mBinding.viewpager.setAdapter(feedViewPagerAdapter);
        mBinding.viewpager.setOffscreenPageLimit(2);
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);

    }

    public void setUser(UserResponse user) {
        this.user = user;
    }

    @Override
    public void onClick(UserResponse user) {
        this.user = user;
        editUserProfile();
    }

    public void editUserProfile() {
        showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.putOnEditUserProfile("Bearer " + getAccessToken(), user).enqueue(new Callback<ApiResponse<UserResponse>>() {
            @Override
            public void onResponse(Call<ApiResponse<UserResponse>> call, Response<ApiResponse<UserResponse>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    hideProgress();
                    mCallback.onBackPressed();
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<UserResponse>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    public void setBodyTypeList(List<BodyType> list) {
        if (fragmentAboutMe != null) {
            fragmentAboutMe.setBodyTypeList(list);
        }
    }

    public void updateList(List<States> data) {
        if (fragmentAboutMe != null) {
            fragmentAboutMe.updateList(data);
        }
    }
}
