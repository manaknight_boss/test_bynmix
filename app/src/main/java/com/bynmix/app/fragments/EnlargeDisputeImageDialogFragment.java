package com.bynmix.app.fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;

import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bynmix.app.R;
import com.squareup.picasso.Picasso;


public class EnlargeDisputeImageDialogFragment extends DialogFragment implements TextView.OnEditorActionListener {
    private Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View dialogView = LayoutInflater.from(mContext).inflate(R.layout.dialog_enlarge_dispute_image, null);
        ImageView imageView = dialogView.findViewById(R.id.image);

        if (getArguments() != null) {
            String imageUrl = getArguments().getString("image");
            Picasso.get().load(imageUrl).into(imageView);

        }
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(dialogView);
        return builder.create();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        return false;
    }

    public static DialogFragment newInstance(String imageUrl) {
        EnlargeDisputeImageDialogFragment frag = new EnlargeDisputeImageDialogFragment();
        Bundle args = new Bundle();
        args.putString("image", imageUrl);
        frag.setArguments(args);
        return frag;

    }

}
