package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.BlogHorizontalAdapter;
import com.bynmix.app.adapter.FeedViewPagerAdapter;
import com.bynmix.app.databinding.FeedsFragmentBinding;
import com.bynmix.app.interfaces.FeedItemClickListener;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.models.UserResponse;
import com.bynmix.app.sharedpreference.SharedPreferenceUtility;
import com.bynmix.app.utils.Constants;

import java.util.List;

public class FeedsFragment extends BaseFragment {
    private FeedsFragmentBinding mBinding;
    private FeedViewPagerAdapter feedViewPagerAdapter;
    private Context mContext;
    private FeedItemClickListener mCallback;
    private ListingFragment listingFragment;
    private AllFragment allFragment;
    private PostFragment postFragment;
    private BlogHorizontalAdapter blogHorizontalAdapter;
    private SharedPreferenceUtility mSharedPreferenceUtility;
    private int currentPage = 0;
    private int tabPosition;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (FeedItemClickListener) context;
    }

    /**
     * Feeds Fragment
     *
     * @return FeedsFragment
     */
    public static FeedsFragment newInstance() {
        return new FeedsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FeedsFragmentBinding.inflate(inflater, container, false);
        setViewPager();
        setType();
        setHorizontalRecyclerView();
        bestUsers(tabPosition);
        mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);

        mBinding.dialogClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.dialogLayout.setVisibility(View.GONE);
            }
        });


        mBinding.filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openFilterFragment(Constants.FEED_FILTER);
            }
        });

        mBinding.addPeople.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openSearchUserFragment();
            }
        });

        mBinding.autoComplete.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable editable) {
                // TODO Auto-generated method stub

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newText = s.toString();
                if (newText.length() >= 1) {
                    mBinding.crossIcon.setVisibility(View.VISIBLE);
                    mBinding.searchIcon.setVisibility(View.GONE);
                } else {
                    mBinding.crossIcon.setVisibility(View.GONE);
                    mBinding.searchIcon.setVisibility(View.VISIBLE);
                }

            }

        });

        mBinding.crossIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.autoComplete.setText("");
                onSearch();
            }
        });

        mBinding.autoComplete.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            allFragment.clearList();
                            postFragment.clearList();
                            listingFragment.clearList();
                            onSearch();
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });

        return mBinding.getRoot();
    }

    private void setType() {
        allFragment.setType(Constants.FEED_FRAGMENT);
        listingFragment.setType(Constants.FEED_FRAGMENT);
        postFragment.setType(Constants.FEED_FRAGMENT);
    }

    private void onSearch() {
        String searchText = mBinding.autoComplete.getText().toString().trim();
        int tabPosition = mBinding.tabs.getSelectedTabPosition();
        if (searchText.isEmpty()) {
            searchText = "";
        }
        allFragment.setSearchPattern(searchText);
        listingFragment.setSearchPattern(searchText);
        postFragment.setSearchPattern(searchText);
        if (tabPosition == 0) {
            fetchFeeds(searchText, Constants.FEED_FRAGMENT, currentPage);
        } else if (tabPosition == 1) {
            listingFeeds(searchText, Constants.FEED_FRAGMENT, currentPage);
        } else {
            postFeeds(searchText, Constants.FEED_FRAGMENT, currentPage);
        }
    }

    private void bestUsers(int tabPosition) {
        if (tabPosition == Constants.AlL_FEEDS) {
            if (mCallback.getBestUserList().size() == 0) {
                bestUser(tabPosition);
            } else {
                updateBestUser(mCallback.getBestUserList(), tabPosition);
            }
            mBinding.addMoreText.setText(R.string.best_user_all_feed);
        } else {
            if (mCallback.getFollowingList().size() == 0) {
                bestUser(tabPosition);
            } else {
                updateBestUser(mCallback.getFollowingList(), tabPosition);
            }
            mBinding.addMoreText.setText(R.string.best_user_listing_and_post);
        }

    }

    private void setHorizontalRecyclerView() {
        blogHorizontalAdapter = new BlogHorizontalAdapter(mContext, mCallback.getBestUserList(), mCallback);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mBinding.horizontalRecyclerView.setAdapter(blogHorizontalAdapter);
        mBinding.horizontalRecyclerView.setLayoutManager(layoutManager);
    }

    private void setViewPager() {
        if (feedViewPagerAdapter == null) {
            feedViewPagerAdapter = new FeedViewPagerAdapter(getChildFragmentManager());
            allFragment = new AllFragment();
            listingFragment = new ListingFragment();
            postFragment = new PostFragment();
            feedViewPagerAdapter.addFragment(allFragment, "All");
            feedViewPagerAdapter.addFragment(listingFragment, "Listings");
            feedViewPagerAdapter.addFragment(postFragment, "Posts");
        }

        mBinding.viewpager.setAdapter(feedViewPagerAdapter);
        mBinding.viewpager.setOffscreenPageLimit(3);
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);


        mBinding.viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    allFragment.callList();
                    mBinding.filter.setVisibility(View.GONE);
                } else if (position == 1) {
                    listingFragment.callList();
                    mBinding.filter.setVisibility(View.VISIBLE);
                } else {
                    postFragment.callList();
                    mBinding.filter.setVisibility(View.GONE);
                }
                bestUsers(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void updateBestUser(List<UserResponse> bestUser, int tabPosition) {
        blogHorizontalAdapter.setList(bestUser);
        blogHorizontalAdapter.setTabPosition(tabPosition);
        setBestUserView(tabPosition);
    }

    private void setBestUserView(int position) {
        this.tabPosition = position;
        mBinding.addMoreText.setVisibility(View.GONE);
        if (mCallback.getBestUserList().size() == 0 && tabPosition == 0) {
            mBinding.horizontalRecyclerView.setVisibility(View.GONE);
            mBinding.errorText.setText(getString(R.string.you_have_followed_everyone));
            mBinding.errorText.setVisibility(View.VISIBLE);
        } else if (mCallback.getFollowingList().size() == 0 && tabPosition == 1) {
            mBinding.horizontalRecyclerView.setVisibility(View.GONE);
            mBinding.errorText.setText(getString(R.string.you_have__not_followed_anyone));
            mBinding.errorText.setVisibility(View.VISIBLE);
        } else if (mCallback.getFollowingList().size() == 0 && tabPosition == 2) {
            mBinding.errorText.setText(getString(R.string.you_have__not_followed_anyone));
            mBinding.horizontalRecyclerView.setVisibility(View.GONE);
            mBinding.errorText.setVisibility(View.VISIBLE);
        } else {
            mBinding.horizontalRecyclerView.setVisibility(View.VISIBLE);
            mBinding.errorText.setVisibility(View.GONE);
            mBinding.addMoreText.setVisibility(View.VISIBLE);
        }
    }

    public void updateFeeds(List<FeedResponse> feeds, int currentPageNumber, int totalPage) {
        allFragment.setFeedList(feeds, currentPageNumber, totalPage);
        showFirstTimeDialog();
    }

    private void showFirstTimeDialog() {
        if (mSharedPreferenceUtility.getAppOpenFirstTime()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    mBinding.dialogLayout.setVisibility(View.VISIBLE);
                    mSharedPreferenceUtility.setIsAppOpenFirstTime(false);
                }
            }, 1000);
        } else {
            mBinding.dialogLayout.setVisibility(View.GONE);
        }
    }

    public void updateListingFeeds(List<FeedResponse> listingFeeds, int currentPage, int totalPage) {
        listingFragment.setList(listingFeeds, currentPage, totalPage);
    }

    public void updatePostFeeds(List<FeedResponse> postFeeds, int currentPage, int totalPage) {
        postFragment.setList(postFeeds, currentPage, totalPage);
    }

    public void onItemFound(int feedType) {
        showFirstTimeDialog();
        if (feedType == Constants.AlL_FEEDS) {
            allFragment.setError(404);
        } else if (feedType == Constants.AlL_LISTING) {
            listingFragment.setError(404);
        } else {
            postFragment.setError(404);
        }
    }

    public void error(int feedType) {
        showFirstTimeDialog();
        if (feedType == Constants.AlL_FEEDS) {
            allFragment.setError(401);

        } else if (feedType == Constants.AlL_LISTING) {
            listingFragment.setError(401);
        } else {
            postFragment.setError(401);
        }
    }

    public void updateNewFeeds(FeedResponse feeds) {
        allFragment.updateFeeds(feeds);
        postFragment.updateFeeds(feeds);
        listingFragment.updateFeeds(feeds);
    }

    public void bestUserError(int tabPosition) {
        setBestUserView(tabPosition);
    }

    public void moveToTop() {
        if (mBinding.tabs.getSelectedTabPosition() == 0) {
            allFragment.moveToTop();
        } else if (mBinding.tabs.getSelectedTabPosition() == 1) {
            listingFragment.moveToTop();
        } else {
            postFragment.moveToTop();
        }
    }

    public void clearListingList() {
        listingFragment.clearList();
    }

    public void setPostDotsTags(int feedType, int position, List<AnnotatedPoint> mList) {
        if (feedType == Constants.AlL_FEEDS) {
            allFragment.setPostDotsTags(position, mList);
        } else {
            postFragment.setPostDotsTags(position, mList);
        }
    }
}
