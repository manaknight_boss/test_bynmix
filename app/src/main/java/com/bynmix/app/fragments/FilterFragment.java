package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.adapter.ConditionFilterAdapter;
import com.bynmix.app.adapter.filterAdapters.FilterAdapter;
import com.bynmix.app.adapter.filterAdapters.FilterBrandAdapter;
import com.bynmix.app.adapter.filterAdapters.FilterCategoryAdapter;
import com.bynmix.app.adapter.filterAdapters.FilterCategoryTypeAdapter;
import com.bynmix.app.adapter.filterAdapters.FilterColorAdapter;
import com.bynmix.app.adapter.filterAdapters.FilterPriceAdapter;
import com.bynmix.app.adapter.filterAdapters.FilterSizeFitAdapter;
import com.bynmix.app.adapter.filterAdapters.FilterSizeTypeAdapter;
import com.bynmix.app.adapter.SelectedFilterAdapter;
import com.bynmix.app.databinding.FilterFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.FilterListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Brand;
import com.bynmix.app.models.Category;
import com.bynmix.app.models.CategoryType;
import com.bynmix.app.models.Colors;
import com.bynmix.app.models.Condition;
import com.bynmix.app.models.FilterList;
import com.bynmix.app.models.FilterSizeType;
import com.bynmix.app.models.FilterSizes;
import com.bynmix.app.models.Price;
import com.bynmix.app.models.SelectedFilter;
import com.bynmix.app.models.Size;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterFragment extends BaseFragment implements FilterListener {

    private FilterFragmentBinding mBinding;
    private Context mContext;
    private CreatePostListener mCallback;
    private FilterAdapter adapter;
    private List<FilterList> mList = new ArrayList<>();
    private List<FilterSizeType> mFilterSizeTypeList = new ArrayList<>();
    private List<CategoryType> mCategoryTypeList = new ArrayList<>();
    private List<Brand> mBrandList = new ArrayList<>();
    private List<Condition> mConditionList = new ArrayList<>();
    private List<Colors> mColorList = new ArrayList<>();
    private List<SelectedFilter> mSelectedFilterList = new ArrayList<>();
    private List<Price> mPriceList = new ArrayList<>();
    private int type = 0;
    private int fragmentType = 0;
    private FilterColorAdapter colorAdapter;
    private FilterCategoryTypeAdapter categoryTypeAdapter;
    private FilterBrandAdapter brandAdapter;
    private FilterSizeTypeAdapter filterSizeTypeAdapter;
    private SelectedFilterAdapter selectedFilterAdapter;
    private ConditionFilterAdapter conditionAdapter;
    private FilterPriceAdapter priceAdapter;
    private FilterSizeFitAdapter filterSizeFitAdapter;
    private FilterCategoryAdapter filterCategoryAdapter;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (CreatePostListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FilterFragmentBinding.inflate(inflater, container, false);
        setRecyclerView();
        if (mCallback.getFilterFragmentType() != fragmentType) {
            mSelectedFilterList.clear();
        } else {
            if (mCallback.getSelectedFilterList().size() != 0) {
                mSelectedFilterList = mCallback.getSelectedFilterList();
            }
        }
        mBinding.heading.setText(mContext.getString(R.string.filter_listings));
        setSelectedViewAdapter();
        setAdapters();
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
        mBinding.backIconItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type == Constants.CATEGORY_FILTER) {
                    setCategoryView();
                } else if (type == Constants.SIZE_FILTER) {
                    setSizeView();
                }
                mBinding.itemHeaderLayout.setVisibility(View.GONE);
            }
        });

        mBinding.nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.topRecyclerView.smoothScrollBy((int) mBinding.topRecyclerView.getScrollX() + 100, (int) mBinding.topRecyclerView.getScrollY());
            }
        });
        mBinding.previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.topRecyclerView.smoothScrollBy((int) mBinding.topRecyclerView.getScrollX() - 100, 0);
            }
        });

        mBinding.nextButton.setVisibility(View.GONE);
        mBinding.previousButton.setVisibility(View.GONE);

        mBinding.itemHeading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type == Constants.CATEGORY_FILTER) {
                    setCategoryView();
                } else if (type == Constants.SIZE_FILTER) {
                    setSizeView();
                }
                mBinding.itemHeaderLayout.setVisibility(View.GONE);
            }
        });


        mBinding.clearAllLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelectedFilterList.clear();
                clearAllList();
                mCallback.setFilterData(mSelectedFilterList, fragmentType);
                mCallback.onBackPressed();

            }
        });
        mBinding.applyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.setFilterData(mSelectedFilterList, fragmentType);
                mCallback.onBackPressed();
            }
        });
        updateUI();

        mBinding.topRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = (LinearLayoutManager)recyclerView.getLayoutManager();
                int firstCompleteVisiblePosition = layoutManager.findFirstCompletelyVisibleItemPosition();
                int lastCompleteVisiblePosition = layoutManager.findLastCompletelyVisibleItemPosition();
                if(lastCompleteVisiblePosition < mSelectedFilterList.size()-1) {
                    mBinding.nextButton.setVisibility(View.VISIBLE);
                } else {
                    mBinding.nextButton.setVisibility(View.GONE);
                }

                if(firstCompleteVisiblePosition > 0) {
                    mBinding.previousButton.setVisibility(View.VISIBLE);
                } else {
                    mBinding.previousButton.setVisibility(View.GONE);
                }

            }
        });



        return mBinding.getRoot();
    }

    private void clearAllList() {
        mConditionList.clear();
        mColorList.clear();
        mFilterSizeTypeList.clear();
        mCategoryTypeList.clear();
        mBrandList.clear();
        mPriceList.clear();
        mList.clear();
    }

    private void setAdapters() {

        switch (type) {
            case 0: {
                setConditionView();
                break;
            }
            case 1: {
                setSizeView();
                break;
            }
            case 2: {
                setColorView();
                break;
            }
            case 3: {
                setCategoryView();
                break;
            }
            case 4: {
                setPriceView();
                break;
            }
            case 5: {
                setBrandView();
                break;
            }
        }
    }

    private void showSelected(int type) {
        for (int i = 0; i < mSelectedFilterList.size(); i++) {
            if (type == mSelectedFilterList.get(i).getType())
                updateSelectedFilterItemUI(mSelectedFilterList.get(i).getType(), mSelectedFilterList.get(i).getId(), mSelectedFilterList.get(i).getSubType(), true);
        }
    }

    private void updateUI() {
        for (int i = 0; i < mSelectedFilterList.size(); i++) {
            if (mSelectedFilterList.get(i).getType() == Constants.CONDITION_FILTER) {
                mList.get(Constants.CONDITION_FILTER).setItemSelected(true);
            }
            if (mSelectedFilterList.get(i).getType() == Constants.SIZE_FILTER) {
                mList.get(Constants.SIZE_FILTER).setItemSelected(true);
            }
            if (mSelectedFilterList.get(i).getType() == Constants.COLOR_FILTER) {
                mList.get(Constants.COLOR_FILTER).setItemSelected(true);
            }
            if (mSelectedFilterList.get(i).getType() == Constants.CATEGORY_FILTER) {
                mList.get(Constants.CATEGORY_FILTER).setItemSelected(true);
            }
            if (mSelectedFilterList.get(i).getType() == Constants.PRICE_FILTER) {
                mList.get(Constants.PRICE_FILTER).setItemSelected(true);
            }
            if (mSelectedFilterList.get(i).getType() == Constants.BRAND_FILTER) {
                mList.get(Constants.BRAND_FILTER).setItemSelected(true);
            }
        }
        adapter.notifyDataSetChanged();
    }

    private void addPriceData() {
        mPriceList.add(new Price(1, getString(R.string.price_under_20), false));
        mPriceList.add(new Price(2, getString(R.string.price_21_to_50), false));
        mPriceList.add(new Price(3, getString(R.string.price_51_to_100), false));
        mPriceList.add(new Price(4, getString(R.string.price_100_to_200), false));
        mPriceList.add(new Price(5, getString(R.string.price_up_201), false));
        showSelected(Constants.PRICE_FILTER);
    }

    private void addConditionTypes() {
        mConditionList.add(new Condition(1, getString(R.string.pre_loved), false));
        mConditionList.add(new Condition(2, getString(R.string.brand_new), false));
        mConditionList.add(new Condition(3, getString(R.string.show_all), false));
        showSelected(Constants.CONDITION_FILTER);
    }

    private void addTypes() {
        mList.add(new FilterList(getString(R.string.condition_text), true));
        mList.add(new FilterList(getString(R.string.size_text), false));
        mList.add(new FilterList(getString(R.string.colors_text), false));
        mList.add(new FilterList(getString(R.string.Category), false));
        mList.add(new FilterList(getString(R.string.filter_price), false));
        mList.add(new FilterList(getString(R.string.brand_text), false));
    }

    private void setRecyclerView() {
        if (mList.size() == 0) {
            addTypes();
        }
        mBinding.itemHeaderLayout.setVisibility(View.GONE);
        adapter = new FilterAdapter(mContext, mList, this);
        mBinding.typeRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.typeRecyclerView.setAdapter(adapter);
    }


    public static FilterFragment newInstance() {
        return new FilterFragment();
    }

    @Override
    public void setConditionView() {
        this.type = Constants.CONDITION_FILTER;
        if (mConditionList.size() == 0) {
            getConditionTypes();
        }
        mBinding.itemHeaderLayout.setVisibility(View.GONE);
        conditionAdapter = new ConditionFilterAdapter(mContext, mConditionList, this);
        mBinding.itemRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.itemRecyclerView.setAdapter(conditionAdapter);
    }

    @Override
    public void setSizeView() {
        this.type = Constants.SIZE_FILTER;
        if (mFilterSizeTypeList.size() == 0) {
            getFilterSizes();
        }
        mBinding.itemHeaderLayout.setVisibility(View.GONE);
        filterSizeTypeAdapter = new FilterSizeTypeAdapter(mContext, mFilterSizeTypeList, this);
        mBinding.itemRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.itemRecyclerView.setAdapter(filterSizeTypeAdapter);
    }

    @Override
    public void setColorView() {
        if (mColorList.size() == 0) {
            mCallback.getColors();
        }
        mBinding.itemHeaderLayout.setVisibility(View.GONE);
        mBinding.itemHeaderLayout.setVisibility(View.GONE);
        colorAdapter = new FilterColorAdapter(mContext, mColorList, this);
        mBinding.itemRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.itemRecyclerView.setAdapter(colorAdapter);
    }

    @Override
    public void setCategoryView() {
        type = Constants.CATEGORY_FILTER;
        if (mCategoryTypeList.size() == 0) {
            mCallback.getCategories();
        }
        mBinding.itemHeaderLayout.setVisibility(View.GONE);
        categoryTypeAdapter = new FilterCategoryTypeAdapter(mContext, mCategoryTypeList, this);
        mBinding.itemRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.itemRecyclerView.setAdapter(categoryTypeAdapter);
    }

    @Override
    public void setPriceView() {
        this.type = Constants.PRICE_FILTER;
        if (mPriceList.size() == 0) {
            addPriceData();
        }
        mBinding.itemHeaderLayout.setVisibility(View.GONE);
        priceAdapter = new FilterPriceAdapter(mContext, mPriceList, this);
        mBinding.itemRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.itemRecyclerView.setAdapter(priceAdapter);
    }

    @Override
    public void setBrandView() {
        this.type = Constants.BRAND_FILTER;
        if (mBrandList.size() == 0) {
            mCallback.getBrands();
        }
        mBinding.itemHeaderLayout.setVisibility(View.GONE);
        brandAdapter = new FilterBrandAdapter(mContext, mBrandList, this);
        mBinding.itemRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.itemRecyclerView.setAdapter(brandAdapter);
    }

    @Override
    public void setSelectedView(int id, String name, int filterType, int subType, List<Category> categories, List<Integer> categoryIds) {
        setCheck(filterType);
        SelectedFilter selectedFilter = new SelectedFilter(id, name, filterType, subType, categoryIds);
        List<Integer> ids = new ArrayList<>();
        boolean isAlreadyAdded = false;
        for (int i = 0; i < mSelectedFilterList.size(); i++) {
            if (filterType == mSelectedFilterList.get(i).getType()) {
                if (mSelectedFilterList.get(i).getSubType() == subType) {
                    if (id == mSelectedFilterList.get(i).getId()) {
                        isAlreadyAdded = true;
                        Toast.makeText(mContext, "Already Selected", Toast.LENGTH_SHORT).show();
                        break;
                    }
                }
                if (filterType == Constants.CONDITION_FILTER) {
                    mSelectedFilterList.remove(i);
                }
                if (filterType == Constants.PRICE_FILTER) {
                    mSelectedFilterList.remove(i);
                }
                if (filterType == Constants.CATEGORY_FILTER) {
                    if (mSelectedFilterList.get(i).getSubType() == subType)
                        if (selectedFilter.getId() == 0 && mSelectedFilterList.get(i).getId() != 0) {
                            ids.add(mSelectedFilterList.get(i).getId());
                        } else {
                            if (mSelectedFilterList.get(i).getId() == 0) {
                                filterCategoryAdapter.reset(mSelectedFilterList.get(i).getId(), subType, false);
                                mSelectedFilterList.remove(i);
                            }
                        }
                }
                if (filterType == Constants.SIZE_FILTER) {

                    if (subType != mSelectedFilterList.get(i).getSubType()) {
                        for (int j = 0; j < mFilterSizeTypeList.size(); j++) {
                            for (FilterSizes filterSize : mFilterSizeTypeList.get(j).getSizes()) {
                                for (Size size : filterSize.getSizes()) {
                                    if (size.getSizeId() != id) {
                                        size.setSelected(false);
                                    }
                                }
                            }
                        }
                        while (true) {
                            if (i < mSelectedFilterList.size() && mSelectedFilterList.get(i).getType() == Constants.SIZE_FILTER && mSelectedFilterList.get(i).getSubType() != subType) {
                                mSelectedFilterList.remove(i);
                            } else {
                                break;
                            }
                        }
                    }
                }

            }
        }

        if (ids.size() > 0) {
            int count = 0;
            int idCounter = 0;
            while (true) {
                if (mSelectedFilterList.get(count).getId() == ids.get(idCounter)) {
                    mSelectedFilterList.remove(count);
                    filterCategoryAdapter.reset(ids.get(idCounter), subType, false);
                    idCounter++;
                    if (idCounter == ids.size()) {
                        break;
                    }
                } else {
                    count++;
                }
            }
        }
        if (!isAlreadyAdded) {
            mSelectedFilterList.add(selectedFilter);
        }
        setSelectedViewAdapter();
    }


    private void setSelectedViewAdapter() {
        selectedFilterAdapter = new SelectedFilterAdapter(mContext, mSelectedFilterList, this);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mBinding.topRecyclerView.setLayoutManager(layoutManager);
        mBinding.topRecyclerView.setAdapter(selectedFilterAdapter);
    }


    private void setCheck(int filterType) {
        mList.get(filterType).setItemSelected(true);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void openCategoryView(CategoryType categoryType) {
        mBinding.itemHeaderLayout.setVisibility(View.VISIBLE);
        mBinding.itemHeading.setText(categoryType.getCategoryTypeName());
        boolean isAllOptionAlreadyAdded = false;
        for (Category addedCategory : categoryType.getCategories()) {
            if (addedCategory.getCategoryId() == 0) {
                isAllOptionAlreadyAdded = true;
                break;
            }
        }
        if (!isAllOptionAlreadyAdded) {
            Category category = new Category();
            category.setCategoryName("All " + categoryType.getCategoryTypeName());
            category.setCategorySelected(false);
            category.setCategoryId(0);
            categoryType.getCategories().add(0, category);
        }
        filterCategoryAdapter = new FilterCategoryAdapter(mContext, categoryType.getCategories(), categoryType.getCategoryTypeId(), this);
        mBinding.itemRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.itemRecyclerView.setAdapter(filterCategoryAdapter);
        showSelected(Constants.CATEGORY_FILTER);

    }

    @Override
    public void openSizeFitView(FilterSizeType filterSizeType) {
        mBinding.itemHeaderLayout.setVisibility(View.VISIBLE);
        mBinding.itemHeading.setText(filterSizeType.getSizeTypeName());
        filterSizeFitAdapter = new FilterSizeFitAdapter(mContext, filterSizeType.getSizes(), filterSizeType.getSizeTypeId(), this);
        mBinding.itemRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.itemRecyclerView.setAdapter(filterSizeFitAdapter);
        showSelected(Constants.SIZE_FILTER);
    }

    @Override
    public void removeCheck(int filterType, int id, int subTypeId) {
        int itemCount = 0;
        for (int i = 0; i < mSelectedFilterList.size(); i++) {

            if (mSelectedFilterList.get(i).getType() == filterType) {
                itemCount++;
            }
        }
        if (itemCount == 1) {
            mList.get(filterType).setItemSelected(false);
            adapter.notifyDataSetChanged();
        }
        updateSelectedFilterItemUI(filterType, id, subTypeId, false);
    }

    @Override
    public void setAllCategoryList(List<Category> allCategoryList) {
        mCallback.setAllCategoryList(allCategoryList);
    }

    private void updateSelectedFilterItemUI(int filterType, int id, int subTypeId, boolean isSet) {

        for (int i = 0; i < mSelectedFilterList.size(); i++) {
            switch (filterType) {
                case 0:
                    for (Condition condition : mConditionList) {
                        if (condition.getConditionId() == id)
                            condition.setConditionSelected(isSet);
                        if (conditionAdapter != null) {
                            conditionAdapter.notifyDataSetChanged();
                        }
                    }
                    break;
                case 1:
                    for (FilterSizeType sizeType : mFilterSizeTypeList) {
                        if (sizeType.getSizeTypeId() == subTypeId) {
                            sizeType.setSelected(isSet);
                            if (filterSizeTypeAdapter != null) {
                                filterSizeTypeAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                    if (filterSizeFitAdapter != null) {
                        filterSizeFitAdapter.reset(id, isSet);
                    }
                    break;
                case 2:
                    for (Colors colors : mColorList) {
                        if (colors.getColorId() == id)
                            colors.setSelectedColor(isSet);
                        if (colorAdapter != null) {
                            colorAdapter.notifyDataSetChanged();
                        }
                    }
                    break;
                case 3:
                    for (CategoryType categoryType : mCategoryTypeList) {
                        if (categoryType.getCategoryTypeId() == subTypeId) {
                            categoryType.setSelected(isSet);
                            if (categoryTypeAdapter != null) {
                                categoryTypeAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                    if (filterCategoryAdapter != null) {
                        filterCategoryAdapter.reset(id, subTypeId, isSet);
                    }
                    break;
                case 4:
                    for (Price price : mPriceList) {
                        if (price.getId() == id)
                            price.setPriceSelected(isSet);
                        if (priceAdapter != null) {
                            priceAdapter.notifyDataSetChanged();
                        }
                    }
                    break;
                case 5:
                    for (Brand brand : mBrandList) {
                        if (brand.getBrandId() == id)
                            brand.setSelected(isSet);
                        if (brandAdapter != null) {
                            brandAdapter.notifyDataSetChanged();
                        }
                    }
                    break;
            }
        }
    }

    public void setColorList(List<Colors> colorList) {
        this.mColorList = colorList;
        colorAdapter.setList(mColorList);
        showSelected(Constants.COLOR_FILTER);
    }

    public void setCategoryTypeList(List<CategoryType> categoryTypeList) {
        this.mCategoryTypeList = categoryTypeList;
        categoryTypeAdapter.setList(mCategoryTypeList);
        showSelected(Constants.CATEGORY_FILTER);
    }

    public void setBrandList(List<Brand> brandList) {
        this.mBrandList = brandList;
        brandAdapter.setList(brandList);
        brandAdapter.setAdapterList(brandList);
        showSelected(Constants.BRAND_FILTER);
    }

    public void setSizeList(List<FilterSizeType> filterSizeTypeList) {
        this.mFilterSizeTypeList = filterSizeTypeList;
        filterSizeTypeAdapter.setList(filterSizeTypeList);
        showSelected(Constants.SIZE_FILTER);
    }

    public void setFragmentType(int fragmentType) {
        this.fragmentType = fragmentType;
    }


    public void updateConditionTypes(List<Condition> mList) {
        this.mConditionList = mList;
        conditionAdapter.setList(mConditionList);
        showSelected(Constants.CATEGORY_FILTER);
    }

    public void getConditionTypes() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getConditionTypes("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<List<Condition>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Condition>>> call, Response<ApiResponse<List<Condition>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    List<Condition> mList = response.body().getData();
                    updateConditionTypes(mList);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getConditionTypes();
                        }

                        @Override
                        public void errorAccessToken() {

                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<List<Condition>>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    public void getFilterSizes() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getFilterSize("Bearer " + getAccessToken(), true).enqueue(new Callback<ApiResponse<List<FilterSizeType>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<FilterSizeType>>> call, Response<ApiResponse<List<FilterSizeType>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    List<FilterSizeType> filterSizeType = response.body().getData();
                    setSizeList(filterSizeType);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getFilterSizes();
                        }

                        @Override
                        public void errorAccessToken() {
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<List<FilterSizeType>>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }
}