package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.adapter.filterAdapters.FilterAdapter;
import com.bynmix.app.adapter.filterAdapters.FilterBrandAdapter;
import com.bynmix.app.adapter.SelectedFilterAdapter;
import com.bynmix.app.adapter.filterAdapters.FilterUserBodyTypeAdapter;
import com.bynmix.app.adapter.filterAdapters.UserStylesFilterAdapter;
import com.bynmix.app.databinding.FilterFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.BodyType;
import com.bynmix.app.models.Brand;
import com.bynmix.app.models.Category;
import com.bynmix.app.models.Descriptor;
import com.bynmix.app.models.FilterList;
import com.bynmix.app.interfaces.FilterUserListener;
import com.bynmix.app.models.HeightModel;
import com.bynmix.app.models.SelectedFilter;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;
import com.jaygoo.widget.OnRangeChangedListener;
import com.jaygoo.widget.RangeSeekBar;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterUserFragment extends BaseFragment implements FilterUserListener {

    private FilterFragmentBinding mBinding;
    private Context mContext;
    private CreatePostListener mCallback;
    private FilterAdapter adapter;
    private List<FilterList> mList = new ArrayList<>();
    private List<Descriptor> mStylesList = new ArrayList<>();
    private List<Brand> mBrandList = new ArrayList<>();
    private List<BodyType> mBodyTypeList = new ArrayList<>();
    private List<SelectedFilter> mSelectedUserFilterList = new ArrayList<>();
    private int type = 0;
    private FilterBrandAdapter brandAdapter;
    private SelectedFilterAdapter selectedFilterAdapter;
    private UserStylesFilterAdapter filterStylesAdapter;
    private FilterUserBodyTypeAdapter filterUserBodyTypeAdapter;
    private int minHeight = 3;
    private double divFactor = 1.66;
    private HeightModel mSelectedHeightModel;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (CreatePostListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FilterFragmentBinding.inflate(inflater, container, false);
        setRecyclerView();
        if (mCallback.getSelectedUserFilterList().size() != 0) {
            mSelectedUserFilterList = mCallback.getSelectedUserFilterList();
        }
        if (mSelectedHeightModel == null) {
            mSelectedHeightModel = mCallback.getHeightModel();
        }
        mBinding.heading.setText(mContext.getString(R.string.filter_user));
        setSelectedViewAdapter();
        setAdapters();
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        mBinding.nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.topRecyclerView.smoothScrollBy((int) mBinding.topRecyclerView.getScrollX() + 100, (int) mBinding.topRecyclerView.getScrollY());
            }
        });
        mBinding.previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.topRecyclerView.smoothScrollBy((int) mBinding.topRecyclerView.getScrollX() - 100, 0);
            }
        });

        mBinding.clearAllLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelectedUserFilterList.clear();
                clearAllList();
                mCallback.setUserFilterData(mSelectedUserFilterList, mSelectedHeightModel);
                mSelectedHeightModel = new HeightModel();
                mCallback.setHeightModel(mSelectedHeightModel);
                mCallback.onBackPressed();

            }
        });
        mBinding.applyLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.setUserFilterData(mSelectedUserFilterList, mSelectedHeightModel);
                mCallback.onBackPressed();
            }
        });
        updateUI();

        mBinding.nextButton.setVisibility(View.GONE);
        mBinding.previousButton.setVisibility(View.GONE);

        mBinding.topRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager layoutManager = (LinearLayoutManager)recyclerView.getLayoutManager();
                int firstCompleteVisiblePosition = layoutManager.findFirstCompletelyVisibleItemPosition();
                int lastCompleteVisiblePosition = layoutManager.findLastCompletelyVisibleItemPosition();
                if(lastCompleteVisiblePosition < mSelectedUserFilterList.size()-1) {
                    mBinding.nextButton.setVisibility(View.VISIBLE);
                } else {
                    mBinding.nextButton.setVisibility(View.GONE);
                }

                if(firstCompleteVisiblePosition > 0) {
                    mBinding.previousButton.setVisibility(View.VISIBLE);
                } else {
                    mBinding.previousButton.setVisibility(View.GONE);
                }

            }
        });


        setHeightValue();
        mBinding.checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelectedHeightModel.setHeightShowAll(!mSelectedHeightModel.isHeightShowAll());
                setHeightUI();
                addHeightInSelectedFilterList(1, getString(R.string.show_all), Constants.HEIGHT_FILTER, 0, new ArrayList<Integer>(), mSelectedHeightModel.isHeightShowAll());
            }
        });
        enableRangebarListener();
        return mBinding.getRoot();
    }

    private void setHeightUI() {
        if (mSelectedHeightModel.isHeightShowAll()) {
            mBinding.rangeBar.setEnabled(false);
            mBinding.rangeBar.setProgressColor(mContext.getResources().getColor(R.color.lightGrey));
            mBinding.rangeBar.setProgressDefaultColor(mContext.getResources().getColor(R.color.lightGrey));
            mBinding.checkbox.setImageResource(R.mipmap.checkbox_checked);
            mBinding.rangeBar.getRightSeekBar().setThumbInactivatedDrawableId(R.drawable.rectangle_disable_progress);
            mBinding.rangeBar.getLeftSeekBar().setThumbInactivatedDrawableId(R.drawable.rectangle_disable_progress);
            mSelectedHeightModel.setCustomMaxHeightFeet(8);
            mSelectedHeightModel.setCustomMinHeightFeet(3);
            mSelectedHeightModel.setCustomMaxHeightInches(0);
            mSelectedHeightModel.setCustomMinHeightInches(0);
            mBinding.selected.setTextColor(mContext.getResources().getColor(R.color.disable_grey));
            mBinding.heightText.setTextColor(mContext.getResources().getColor(R.color.disable_grey));
        } else {
            mBinding.rangeBar.setEnabled(true);
            mBinding.rangeBar.setProgressColor(mContext.getResources().getColor(R.color.purple));
            mBinding.rangeBar.setProgressDefaultColor(mContext.getResources().getColor(R.color.greyTextColor));
            mBinding.rangeBar.getLeftSeekBar().setThumbInactivatedDrawableId(R.drawable.rectangle_progress);
            mBinding.rangeBar.getRightSeekBar().setThumbInactivatedDrawableId(R.drawable.rectangle_progress);
            mBinding.checkbox.setImageResource(R.mipmap.checkbox);
            mBinding.selected.setTextColor(mContext.getResources().getColor(R.color.add_comment_heading));
            mBinding.heightText.setTextColor(mContext.getResources().getColor(R.color.add_comment_heading));
        }
        setHeightValue();
    }

    private void addHeightInSelectedFilterList(int id, String name, int type, int subType, ArrayList<Integer> list, boolean isSelected) {
        for (int i = 0; i < mSelectedUserFilterList.size(); i++) {
            if (mSelectedUserFilterList.get(i).getType() == Constants.HEIGHT_FILTER) {
                mSelectedUserFilterList.remove(i);
                break;
            }
        }
        if (isSelected) {
            SelectedFilter selectedFilter = new SelectedFilter(id, name, type, subType, list);
            mSelectedUserFilterList.add(selectedFilter);
        }

        selectedFilterAdapter.notifyDataSetChanged();
        updateUI();

    }

    private void setHeightValue() {
        mBinding.rangeBar.setOnRangeChangedListener(null);
        mBinding.rangeBar.setValue(mSelectedHeightModel.getMinPinValue(), mSelectedHeightModel.getMaxPinValue());
        mBinding.heightText.setText(mSelectedHeightModel.getCustomMinHeightFeet() + " ft " + mSelectedHeightModel.getCustomMinHeightInches() + " in " + " - " + mSelectedHeightModel.getCustomMaxHeightFeet() + " ft " + mSelectedHeightModel.getCustomMaxHeightInches() + " in ");
        enableRangebarListener();
    }

    private void enableRangebarListener() {
        mBinding.rangeBar.setOnRangeChangedListener(new OnRangeChangedListener() {
            @Override
            public void onRangeChanged(RangeSeekBar view, float leftValue, float rightValue, boolean isFromUser) {
                int selectedMinValue = (int) (leftValue / divFactor);
                mSelectedHeightModel.setCustomMinHeightInches(selectedMinValue % 12);
                int selectedMinFeetValue = selectedMinValue / 12;
                selectedMinFeetValue = minHeight + selectedMinFeetValue;
                mSelectedHeightModel.setCustomMinHeightFeet(selectedMinFeetValue);
                mSelectedHeightModel.setMinPinValue(leftValue);

                int selectedMaxValue = (int) (rightValue / divFactor);
                mSelectedHeightModel.setCustomMaxHeightInches(selectedMaxValue % 12);
                int selectedMaxFeetValue = selectedMaxValue / 12;
                selectedMaxFeetValue = minHeight + selectedMaxFeetValue;
                mSelectedHeightModel.setCustomMaxHeightFeet(selectedMaxFeetValue);
                mSelectedHeightModel.setMaxPinValue(rightValue);
                addHeightInSelectedFilterList(1, mBinding.heightText.getText().toString(), Constants.HEIGHT_FILTER, 0, new ArrayList<Integer>(), true);
                setHeightValue();
            }

            @Override
            public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {

            }

            @Override
            public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {

            }
        });
    }

    private void clearAllList() {
        mBodyTypeList.clear();
        mStylesList.clear();
        mBrandList.clear();
        mList.clear();
    }

    private void setAdapters() {

        switch (type) {
            case 0: {
                setStylesView();
                break;
            }
            case 1: {
                setBodyTypeView();
                break;
            }
            case 2: {
                setBrandView();
                break;
            }
            case 3: {
                setHeightView();
                break;
            }
        }
    }

    private void showSelected(int type) {
        for (int i = 0; i < mSelectedUserFilterList.size(); i++) {
            if (type == mSelectedUserFilterList.get(i).getType())
                updateSelectedFilterItemUI(mSelectedUserFilterList.get(i).getType(), mSelectedUserFilterList.get(i).getId(), mSelectedUserFilterList.get(i).getSubType(), true);
        }
    }

    private void updateUI() {
        for (int i = 0; i < mSelectedUserFilterList.size(); i++) {
            if (mSelectedUserFilterList.get(i).getType() == Constants.USER_STYLE_FILTER) {
                mList.get(Constants.USER_STYLE_FILTER).setItemSelected(true);
            }
            if (mSelectedUserFilterList.get(i).getType() == Constants.USER_BODY_TYPE_FILTER) {
                mList.get(Constants.USER_BODY_TYPE_FILTER).setItemSelected(true);
            }
            if (mSelectedUserFilterList.get(i).getType() == Constants.USER_BRAND_FILTER) {
                mList.get(Constants.USER_BRAND_FILTER).setItemSelected(true);
            }
            if (mSelectedUserFilterList.get(i).getType() == Constants.HEIGHT_FILTER) {
                mList.get(Constants.HEIGHT_FILTER).setItemSelected(true);
            }
        }
        adapter.notifyDataSetChanged();
    }


    private void addTypes() {
        mList.add(new FilterList(getString(R.string.styles_text), true));
        mList.add(new FilterList(getString(R.string.body_type), false));
        mList.add(new FilterList(getString(R.string.brand_text), false));
        mList.add(new FilterList(getString(R.string.height_text), false));
    }

    private void setRecyclerView() {
        if (mList.size() == 0) {
            addTypes();
        }
        mBinding.itemHeaderLayout.setVisibility(View.GONE);
        adapter = new FilterAdapter(mContext, mList, this);
        mBinding.typeRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.typeRecyclerView.setAdapter(adapter);
    }


    public static FilterUserFragment newInstance() {
        return new FilterUserFragment();
    }


    public void setStylesView() {
        this.type = Constants.USER_STYLE_FILTER;
        if (mStylesList.size() == 0) {
            getUserStyles();
        }
        setView(true);
        filterStylesAdapter = new UserStylesFilterAdapter(mContext, mStylesList, this);
        mBinding.itemRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.itemRecyclerView.setAdapter(filterStylesAdapter);
    }

    private void setView(boolean showRecycler) {
        mBinding.itemHeaderLayout.setVisibility(View.GONE);
        if (showRecycler) {
            mBinding.itemRecyclerView.setVisibility(View.VISIBLE);
            mBinding.heightView.setVisibility(View.GONE);
        } else {
            mBinding.itemRecyclerView.setVisibility(View.GONE);
            mBinding.heightView.setVisibility(View.VISIBLE);
        }
    }

    public void setBodyTypeView() {
        this.type = Constants.USER_BODY_TYPE_FILTER;
        if (mBodyTypeList.size() == 0) {
            getBodyTypes();
        }
        setView(true);
        filterUserBodyTypeAdapter = new FilterUserBodyTypeAdapter(mContext, mBodyTypeList, this);
        mBinding.itemRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.itemRecyclerView.setAdapter(filterUserBodyTypeAdapter);
    }

    @Override
    public void setBrandView() {
        this.type = Constants.BRAND_FILTER;
        if (mBrandList.size() == 0) {
            mCallback.getBrands();
        }
        setView(true);
        brandAdapter = new FilterBrandAdapter(mContext, mBrandList, this);
        mBinding.itemRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.itemRecyclerView.setAdapter(brandAdapter);
    }

    public void setHeightView() {
        setView(false);
        setHeightUI();
    }

    @Override
    public void setSelectedView(int id, String name, int filterType, int subType, List<Category> categories, List<Integer> idsList) {
        setCheck(filterType);
        SelectedFilter selectedFilter = new SelectedFilter(id, name, filterType, subType, idsList);
        List<Integer> ids = new ArrayList<>();
        boolean isAlreadyAdded = false;
        for (int i = 0; i < mSelectedUserFilterList.size(); i++) {
            if (filterType == mSelectedUserFilterList.get(i).getType()) {
                if (id == mSelectedUserFilterList.get(i).getId()) {
                    isAlreadyAdded = true;
                    Toast.makeText(mContext, "Already Selected", Toast.LENGTH_SHORT).show();
                    break;
                } else {
                    if (id == 0) {
                        ids.add(mSelectedUserFilterList.get(i).getId());
                    } else {
                        if (mSelectedUserFilterList.get(i).getId() == 0) {
                            resetAdapters(mSelectedUserFilterList.get(i).getId(), filterType);
                            mSelectedUserFilterList.remove(i);
                        }
                    }
                }
            }
        }

        if (ids.size() > 0) {
            int count = 0;
            int idCounter = 0;
            while (true) {
                if (mSelectedUserFilterList.get(count).getId() == ids.get(idCounter)) {
                    mSelectedUserFilterList.remove(count);
                    resetAdapters(ids.get(idCounter), filterType);
                    idCounter++;
                    if (idCounter == ids.size()) {
                        break;
                    }
                } else {
                    count++;
                }
            }
        }

        if (!isAlreadyAdded) {
            mSelectedUserFilterList.add(selectedFilter);
        }
        setSelectedViewAdapter();
    }

    private void resetAdapters(int id, int filterType) {
        if (filterType == Constants.USER_STYLE_FILTER) {
            filterStylesAdapter.reset(id, false);
        } else if (filterType == Constants.USER_BODY_TYPE_FILTER) {
            filterUserBodyTypeAdapter.reset(id, false);
        } else if (filterType == Constants.USER_BRAND_FILTER) {
            brandAdapter.reset(id, false);
        }
    }

    private void setSelectedViewAdapter() {
        selectedFilterAdapter = new SelectedFilterAdapter(mContext, mSelectedUserFilterList, this);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mBinding.topRecyclerView.setAdapter(selectedFilterAdapter);
        mBinding.topRecyclerView.setLayoutManager(layoutManager);
    }

    private void setCheck(int filterType) {
        mList.get(filterType).setItemSelected(true);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void removeCheck(int filterType, int id, int subTypeId) {
        int itemCount = 0;
        for (int i = 0; i < mSelectedUserFilterList.size(); i++) {

            if (mSelectedUserFilterList.get(i).getType() == filterType) {
                if (filterType == Constants.HEIGHT_FILTER) {
                    mSelectedHeightModel.setHeightShowAll(false);
                    setHeightUI();
                    mBinding.rangeBar.setValue(0, 100);
                }
                itemCount++;

            }
        }
        if (itemCount == 1) {
            mList.get(filterType).setItemSelected(false);
            adapter.notifyDataSetChanged();
        }
        updateSelectedFilterItemUI(filterType, id, subTypeId, false);
    }

    private void updateSelectedFilterItemUI(int filterType, int id, int subTypeId, boolean isSet) {

        for (int i = 0; i < mSelectedUserFilterList.size(); i++) {
            switch (filterType) {
                case Constants.USER_STYLE_FILTER:
                    for (Descriptor descriptor : mStylesList) {
                        if (descriptor.getDescriptorId() == id)
                            descriptor.setSelected(isSet);
                        if (filterStylesAdapter != null) {
                            filterStylesAdapter.notifyDataSetChanged();
                        }
                    }
                    break;
                case 1:
                    for (BodyType bodyType : mBodyTypeList) {
                        if (bodyType.getBodyTypeId() == id) {
                            bodyType.setSelected(isSet);
                            if (filterUserBodyTypeAdapter != null) {
                                filterUserBodyTypeAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                    break;
                case 2:
                    for (Brand brand : mBrandList) {
                        if (brand.getBrandId() == id)
                            brand.setSelected(isSet);
                        if (brandAdapter != null) {
                            brandAdapter.notifyDataSetChanged();
                        }
                    }
                    break;
            }
        }
    }

    public void setBrandList(List<Brand> brandList) {
        this.mBrandList = brandList;
        Brand brand = new Brand();
        brand.setBrandId(0);
        brand.setBrandName(getString(R.string.show_all_text));
        mBrandList.add(0, brand);
        brandAdapter.setList(brandList);
        brandAdapter.setAdapterList(brandList);
        showSelected(Constants.USER_BRAND_FILTER);
    }

    public void setStylesList(List<Descriptor> mList) {
        this.mStylesList = mList;
        Descriptor style = new Descriptor();
        style.setDescriptorId(0);
        style.setDescriptorName(getString(R.string.show_all_text));
        mStylesList.add(0, style);
        filterStylesAdapter.setList(mStylesList);
        filterStylesAdapter.setAdapterList(mStylesList);
        showSelected(Constants.USER_STYLE_FILTER);
    }

    public void setBodyTypeList(List<BodyType> bodyTypeList) {
        this.mBodyTypeList = bodyTypeList;
        BodyType bodyType = new BodyType();
        bodyType.setBodyTypeId(0);
        bodyType.setBodyTypeName(getString(R.string.show_all_text));
        mBodyTypeList.add(0, bodyType);
        filterUserBodyTypeAdapter.setList(mBodyTypeList);
        showSelected(Constants.USER_BODY_TYPE_FILTER);
    }


    public void getUserStyles() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getDescriptors("Bearer " + getAccessToken(), null).enqueue(new Callback<ApiResponse<List<Descriptor>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<Descriptor>>> call, Response<ApiResponse<List<Descriptor>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    List<Descriptor> mList = response.body().getData();
                    setStylesList(mList);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getUserStyles();
                        }

                        @Override
                        public void errorAccessToken() {
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<List<Descriptor>>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }
}