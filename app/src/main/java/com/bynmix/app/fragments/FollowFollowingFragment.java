package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.FollowFollowingAdapter;
import com.bynmix.app.databinding.FollowFollowingFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Bloggers;
import com.bynmix.app.models.UserResponse;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FollowFollowingFragment extends BaseFragment {

    private FollowFollowingFragmentBinding mBinding;
    private Context mContext;
    private ProfileListener mCallback;
    private FollowFollowingAdapter adapter;
    private List<UserResponse> mList = new ArrayList<>();
    private int type;
    private int userId;
    private String userName;
    private int currentPageNumber;
    private int totalPages;
    private boolean isAlreadyCalled;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (ProfileListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FollowFollowingFragmentBinding.inflate(inflater, container, false);
        isAlreadyCalled = false;
        setRecyclerView();
        if (type == Constants.FOLLOWER) {
            mBinding.toolbarHeading.setText(userName + " " + getString(R.string.followers));
        } else {
            mBinding.toolbarHeading.setText(userName + " " + getString(R.string.following));
        }
        if (mList.size() == 0) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
            currentPageNumber = 0;
            fetchFollowFollowing();
        }

        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        mBinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                {

                    if (mCallback.conditionsForApiCall()) {
                        currentPageNumber = 0;
                        fetchFollowFollowing();
                    } else {
                        hideSwipeRefresh();
                    }
                }
            }
        });

        return mBinding.getRoot();
    }

    private void fetchFollowFollowing() {
        getFollowFollowing(userId, type, currentPageNumber);
    }

    private void hideSwipeRefresh() {
        mBinding.swipeRefreshLayout.setRefreshing(false);
    }


    private void setRecyclerView() {
        adapter = new FollowFollowingAdapter(mContext, mList, mCallback);
        mBinding.recycleView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recycleView.setAdapter(adapter);

        mBinding.recycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();
                boolean endHasBeenReached = lastVisible >= totalItemCount - 4;
                if (totalItemCount > 0 && endHasBeenReached) {
                    if (!isAlreadyCalled && currentPageNumber < totalPages && mList.size() > 0) {
                        isAlreadyCalled = true;
                        fetchFollowFollowing();
                        setLoadMoreProgress();
                    }
                }
            }
        });
    }

    /**
     * FollowFollowing Fragment
     *
     * @return FollowFollowingFragment
     */
    public static FollowFollowingFragment newInstance() {
        return new FollowFollowingFragment();
    }

    public void settype(int type) {

        this.type = type;
    }

    public void updateList(List<UserResponse> bestUser, int currentPageNumber, int totalPages) {
        mBinding.progressBar.setVisibility(View.GONE);
        this.currentPageNumber = currentPageNumber;
        this.totalPages = totalPages;
        if (currentPageNumber > 1) {
            this.mList.addAll(bestUser);
        } else {
            this.mList = bestUser;
        }
        isAlreadyCalled = false;
        setLoadMoreProgress();
        adapter.setList(mList);
        hideSwipeRefresh();
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    private void setLoadMoreProgress() {
        if (isAlreadyCalled) {
            adapter.setProgress(true);
        } else {
            adapter.setProgress(false);
        }
    }

    public void getFollowFollowing(final int userId, final int type, final int currentPageNumber) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        if (type == Constants.FOLLOWER) {
            apiInstance.getFollower("Bearer " + getAccessToken(), "" + userId, currentPageNumber + 1).enqueue(new Callback<ApiResponse<Bloggers>>() {
                @Override
                public void onResponse(Call<ApiResponse<Bloggers>> call, Response<ApiResponse<Bloggers>> response) {
                    setFollowFollowingResponse(response, userId, type, currentPageNumber);
                    hideProgress();
                }

                @Override
                public void onFailure(Call<ApiResponse<Bloggers>> call, Throwable t) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    hideProgress();
                    showAlertDialog(getResources().getString(R.string.error_server));
                }
            });
        } else {
            apiInstance.getFollowing("Bearer " + getAccessToken(), "" + userId, currentPageNumber + 1).enqueue(new Callback<ApiResponse<Bloggers>>() {
                @Override
                public void onResponse(Call<ApiResponse<Bloggers>> call, Response<ApiResponse<Bloggers>> response) {
                    setFollowFollowingResponse(response, userId, type, currentPageNumber);
                    hideProgress();
                }

                @Override
                public void onFailure(Call<ApiResponse<Bloggers>> call, Throwable t) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    hideProgress();
                    showAlertDialog(getResources().getString(R.string.error_server));
                }
            });
        }
    }

    private void setFollowFollowingResponse(Response<ApiResponse<Bloggers>> response, final int userId, final int type, final int currentPageNumber) {
        if (response.isSuccessful() && response.code() == 200) {
            Bloggers bloggers = response.body().getData();
            int currentPage = bloggers.getPageNumber();
            int totalPages = bloggers.getTotalPages();
            List<UserResponse> bestUser = bloggers.getItems();
            updateList(bestUser, currentPage, totalPages);
        } else if (response.code() == 401) {
            getAccessTokenFromServer(new AccessTokenResponse() {
                @Override
                public void successAccessToken() {
                    getFollowFollowing(userId, type, currentPageNumber);
                }

                @Override
                public void errorAccessToken() {
                    mBinding.progressBar.setVisibility(View.GONE);
                }
            });
        } else {
            String message = Utils.ErrorMessage(mContext, response.errorBody());
            mBinding.progressBar.setVisibility(View.GONE);
            showAlertDialog(message);
        }
    }
}