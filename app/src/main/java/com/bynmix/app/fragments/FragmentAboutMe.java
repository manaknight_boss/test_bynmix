package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.adapter.SpinnerAdapter;
import com.bynmix.app.databinding.FragmentAboutMeBinding;
import com.bynmix.app.interfaces.EditProfileListener;
import com.bynmix.app.models.BodyType;
import com.bynmix.app.models.States;
import com.bynmix.app.models.UserResponse;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class FragmentAboutMe extends BaseFragment {

    private FragmentAboutMeBinding mBinding;
    private UserResponse user;
    private EditProfileListener mCallback;
    private Context mContext;
    private SpinnerAdapter spinnerAdapter;
    private List<States> mStateList = new ArrayList<>();
    private States states = new States();
    private List<String> mFeet = new ArrayList<>();
    private List<String> mInches = new ArrayList<>();
    private List<String> gender = new ArrayList<>();
    private List<String> bodyNameList = new ArrayList<>();
    private List<BodyType> bodyTypeList = new ArrayList<>();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentAboutMeBinding.inflate(inflater, container, false);
        addDataInLists();
        setData();
        ArrayAdapter<String> genderAdapter = new ArrayAdapter<String>(mContext, R.layout.profile_spinner_item, gender);
        mBinding.gender.setAdapter(genderAdapter);
        ArrayAdapter<String> inchesAdapter = new ArrayAdapter<String>(mContext, R.layout.profile_spinner_item, mInches);
        mBinding.inches.setAdapter(inchesAdapter);
        ArrayAdapter<String> feetAdapter = new ArrayAdapter<String>(mContext, R.layout.profile_spinner_item, mFeet);
        mBinding.feet.setAdapter(feetAdapter);
        getAdaptersStates();
        spinnerAdapter = new SpinnerAdapter(mContext, R.layout.profile_spinner_item, mStateList);
        mBinding.state.setAdapter(spinnerAdapter);
        Utils.setCapitalizeTextWatcher(mBinding.city);
        mBinding.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendUserData();
            }
        });

        return mBinding.getRoot();

    }

    private void getAdaptersStates() {
        mBinding.feet.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 0) {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.lightGrey));
                } else {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorPrimary));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mBinding.inches.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 0) {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.lightGrey));
                } else {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorPrimary));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        mBinding.bodyType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 0) {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.lightGrey));
                } else {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorPrimary));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        mBinding.gender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (position == 0) {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.lightGrey));
                } else {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorPrimary));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void addDataInLists() {
        getStatesList();
        getBodyTypes();
        for (int i = 0; i <= 11; i++) {
            mInches.add("" + i);
        }
        for (int i = 3; i <= 8; i++) {
            mFeet.add("" + i);
        }
        mInches.add(0, "Select Inches");
        mFeet.add(0, "Select Feet");
        gender.add(0, "Select Gender");
        gender.add(1, "Female");
        gender.add(2, "Male");
        ;
    }


    private void sendUserData() {
        int selectedId = mBinding.state.getSelectedItemPosition();
        String state = mStateList.get(selectedId).getName();
        String stateAbbreviation = mStateList.get(selectedId).getAbbreviation();
        String city = mBinding.city.getText().toString().trim();
        String gender = mBinding.gender.getSelectedItem().toString();
        String feet = mBinding.feet.getSelectedItem().toString();
        String inches = mBinding.inches.getSelectedItem().toString();
        String bodyName = mBinding.bodyType.getSelectedItem().toString();
        int genderType;
        int bodyTypeId = 0;
        if (gender.equalsIgnoreCase("male")) {
            genderType = 2;
        } else {
            genderType = 1;
        }

        if (inches.equalsIgnoreCase("Select inches")) {
            inches = "" + 0;
        }

        if (feet.equalsIgnoreCase("Select feet")) {
            feet = "" + 0;
        }
        if (gender.equalsIgnoreCase("Select Gender")) {
            genderType = 0;
        }

        if(!bodyName.equalsIgnoreCase("Select a body type")) {
            for (int i = 0; i < bodyTypeList.size(); i++) {
                if (bodyName.equalsIgnoreCase(bodyTypeList.get(i).getBodyTypeName())) {
                    bodyTypeId = bodyTypeList.get(i).getBodyTypeId();
                    break;
                }
            }
        }


        if (state.equalsIgnoreCase("Select State")) {
            state = null;
        }

        user.setState(state);
        user.setStateAbbreviated(stateAbbreviation);
        user.setCity(city);
        user.setGender(genderType);
        user.setHeightFeet(Integer.parseInt(feet));
        user.setHeightInches(Integer.parseInt(inches));
        user.setBodyTypeId(bodyTypeId);
        mCallback.onClick(user);
    }

    private void setData() {
        mBinding.city.setText(user.getCity());
    }

    public void setUser(UserResponse user) {
        this.user = user;
    }

    public void updateList(List<States> state) {
        this.mStateList = state;
        spinnerAdapter.setList(mStateList);

        if (user.getState() != null) {
            for (int i = 0; i < mStateList.size(); i++) {

                if (user.getStateAbbreviated().equals(mStateList.get(i).getAbbreviation())) {
                    mStateList.remove(i);
                    states.setName(user.getState());
                    states.setAbbreviation(user.getStateAbbreviated());
                    mStateList.add(0, states);
                    break;
                }
            }

        } else {
            states.setName("Select State");
            mStateList.add(0, states);
        }

    }

    public void setBodyTypeList(List<BodyType> list) {
        this.bodyTypeList = list;
        if (bodyTypeList != null) {
            for (int i = 0; i < bodyTypeList.size(); i++) {
                bodyNameList.add(bodyTypeList.get(i).getBodyTypeName());
            }
        }
        bodyNameList.add(0, "Select a body type");
        ArrayAdapter<String> bodyAdapter = new ArrayAdapter<String>(mContext, R.layout.profile_spinner_item, bodyNameList);
        mBinding.bodyType.setAdapter(bodyAdapter);
        setSelectedSpinnerItem();
    }

    private void setSelectedSpinnerItem() {
        int bodyTypePosition = 0, feetPosition = 0, inchesPosition = 0;
        if (user.getBodyTypeId() != 0) {
            for (int i = 0; i < bodyTypeList.size(); i++) {
                if (user.getBodyTypeId() == bodyTypeList.get(i).getBodyTypeId()) {
                    bodyTypePosition = i + 1;
                    break;
                }
            }
        }

        for (int i = 1; i < mFeet.size(); i++) {
            if (user.getHeightFeet() == Integer.parseInt(mFeet.get(i))) {
                feetPosition = i;
                break;
            }
        }


        for (int i = 1; i < mInches.size(); i++) {
            if (user.getHeightInches() == Integer.parseInt(mInches.get(i))) {
                inchesPosition = i;
                break;
            }
        }

        mBinding.bodyType.setSelection(bodyTypePosition);
        mBinding.feet.setSelection(feetPosition);
        mBinding.inches.setSelection(inchesPosition);
        mBinding.gender.setSelection(user.getGender());
    }

    public void setListener(EditProfileListener mCallback, UserResponse user) {
        this.mCallback = mCallback;
        this.user = user;
    }
}
