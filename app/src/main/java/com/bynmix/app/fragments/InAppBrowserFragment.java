package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.bynmix.app.databinding.FragmentInAppBrowserBinding;
import com.bynmix.app.interfaces.OnBackPressedListener;


public class InAppBrowserFragment extends Fragment {
    private FragmentInAppBrowserBinding mBinding;
    private Context mContext;
    private OnBackPressedListener mCallback;
    private String url;
    private String title;

    public static InAppBrowserFragment newInstance() {
        return new InAppBrowserFragment();
    }

    public void setUrl(String url, String title) {
        this.url = url;
        this.title = title;
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentInAppBrowserBinding.inflate(inflater,container,false);
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onBackPressed();
            }
        });
        mBinding.webView.getSettings().setJavaScriptEnabled(true);
        mBinding.toolbarHeading.setText(title);
        mBinding.webView.setWebViewClient(new WebViewClient(){
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
            public void onPageFinished(WebView view, String url) {
                mBinding.progressBar.setVisibility(View.GONE);
            }
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(mContext, "" + description, Toast.LENGTH_SHORT).show();
            }
        });
        mBinding.webView.loadUrl(url);
        return mBinding.getRoot();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (OnBackPressedListener) context;
    }

}
