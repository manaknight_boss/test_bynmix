package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.FollowFollowingAdapter;
import com.bynmix.app.databinding.FollowFollowingFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Bloggers;
import com.bynmix.app.models.UserResponse;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LikesFragment extends BaseFragment {

    private FollowFollowingFragmentBinding mBinding;
    private Context mContext;
    private ProfileListener mCallback;
    private FollowFollowingAdapter adapter;
    private List<UserResponse> mList = new ArrayList<>();
    private int id;
    private int type;
    private int currentPageNumber;
    private int totalPages;
    private boolean isAlreadyCalled;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (ProfileListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FollowFollowingFragmentBinding.inflate(inflater, container, false);
        if(getArguments() != null) {
            id = getArguments().getInt(Constants.ID, 0);
            type = getArguments().getInt(Constants.TYPE, 0);
        }
        isAlreadyCalled = false;
        setRecyclerView();
        mBinding.toolbarHeading.setText(getString(R.string.likes));
        if (mList.size() == 0) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
            currentPageNumber = 0;
            fetchLikes();
        }

        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        mBinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                {

                    if (mCallback.conditionsForApiCall()) {
                        currentPageNumber = 0;
                        fetchLikes();
                    } else {
                        hideSwipeRefresh();
                    }
                }
            }
        });

        return mBinding.getRoot();
    }

    private void fetchLikes() {
     getLikes();
    }

    private void hideSwipeRefresh() {
        mBinding.swipeRefreshLayout.setRefreshing(false);
    }


    private void setRecyclerView() {
        adapter = new FollowFollowingAdapter(mContext, mList, mCallback);
        mBinding.recycleView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recycleView.setAdapter(adapter);

        mBinding.recycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();
                boolean endHasBeenReached = lastVisible >= totalItemCount - 4;
                if (totalItemCount > 0 && endHasBeenReached) {
                    if (!isAlreadyCalled && currentPageNumber < totalPages && mList.size() > 0) {
                        isAlreadyCalled = true;
                        fetchLikes();
                        setLoadMoreProgress();
                    }
                }

            }
        });
    }

    public static LikesFragment newInstance(int id, int type) {
        LikesFragment fragment = new LikesFragment();

        Bundle args = new Bundle();
        args.putInt(Constants.TYPE, type);
        args.putInt(Constants.ID, id);
        fragment.setArguments(args);

        return fragment;
    }


    public void updateList(List<UserResponse> bestUser, int currentPageNumber, int totalPages) {
        mBinding.progressBar.setVisibility(View.GONE);
        this.currentPageNumber = currentPageNumber;
        this.totalPages = totalPages;
        if (currentPageNumber > 1) {
            this.mList.addAll(bestUser);
        } else {
            this.mList = bestUser;
        }
        isAlreadyCalled = false;
        setLoadMoreProgress();
        adapter.setList(mList);
        hideSwipeRefresh();
    }

    private void setLoadMoreProgress() {
        if (isAlreadyCalled) {
            adapter.setProgress(true);
        } else {
            adapter.setProgress(false);
        }
    }

    public void getLikes() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        if(type == Constants.FEED_LISTING) {
            apiInstance.getListingLikes("Bearer " + getAccessToken(), "" + id).enqueue(new Callback<ApiResponse<Bloggers>>() {
                @Override
                public void onResponse(Call<ApiResponse<Bloggers>> call, Response<ApiResponse<Bloggers>> response) {
                    handleResponse(response);
                }

                @Override
                public void onFailure(Call<ApiResponse<Bloggers>> call, Throwable t) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    showAlertDialog(getResources().getString(R.string.error_server));
                }
            });
        } else {
            apiInstance.getPostLikes("Bearer " + getAccessToken(), "" + id).enqueue(new Callback<ApiResponse<Bloggers>>() {
                @Override
                public void onResponse(Call<ApiResponse<Bloggers>> call, Response<ApiResponse<Bloggers>> response) {
                    handleResponse(response);
                }

                @Override
                public void onFailure(Call<ApiResponse<Bloggers>> call, Throwable t) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    showAlertDialog(getResources().getString(R.string.error_server));
                }
            });
        }
    }

    private void handleResponse(Response<ApiResponse<Bloggers>> response) {
        if (response.isSuccessful() && response.code() == 200) {
            Bloggers bloggers = response.body().getData();
            int currentPage = bloggers.getPageNumber();
            int totalPages = bloggers.getTotalPages();
            List<UserResponse> user = bloggers.getItems();
            updateList(user, currentPage, totalPages);
        } else if (response.code() == 401) {
            getAccessTokenFromServer(new AccessTokenResponse() {
                @Override
                public void successAccessToken() {
                    getLikes();
                }

                @Override
                public void errorAccessToken() {
                    mBinding.progressBar.setVisibility(View.GONE);
                }
            });
        } else {
            mBinding.progressBar.setVisibility(View.GONE);
            String message = Utils.ErrorMessage(mContext, response.errorBody());
            showAlertDialog(message);
        }
    }

}