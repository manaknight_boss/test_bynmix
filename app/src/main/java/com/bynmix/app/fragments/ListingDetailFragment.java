package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.ListingDetailAdapter;
import com.bynmix.app.databinding.ListingDetailFragmentBinding;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.Comments;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.models.ListingDisplayImage;
import com.bynmix.app.sharedpreference.SharedPreferenceUtility;
import com.bynmix.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class ListingDetailFragment extends Fragment {

    private ListingDetailFragmentBinding mBinding;
    private CreatePostListener mCallback;
    private Context mContext;
    private SharedPreferenceUtility mSharedPreferences;
    private int id;
    private ListingDetail listingDetail;
    private ListingDetailAdapter listingDetailAdapter;
    private List<ListingDisplayImage> mImagesList = new ArrayList<>();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (CreatePostListener) context;
        mContext = context;
    }

    public static ListingDetailFragment newInstance() {
        return new ListingDetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = ListingDetailFragmentBinding.inflate(inflater, container, false);
        mSharedPreferences = new SharedPreferenceUtility(mContext);
        init();
        if (listingDetail == null && mImagesList.size() == 0) {
            callListingDetail();
        } else {
            setImages(mImagesList);
        }
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        return mBinding.getRoot();
    }

    private void callListingDetail() {
        mBinding.listingDetailMainLayout.setVisibility(View.GONE);
        mCallback.getListingDetail(id);
    }

    private void init() {
        mBinding.buyNowLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listingDetail.getUserId() == mSharedPreferences.getMyUserId()) {
                    mCallback.openEditMyListingFragment(listingDetail.getId());
                } else {
                    mCallback.openOfferCompletionFragment(listingDetail, Constants.BUY);
                }
            }
        });

        mBinding.heartIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openMyLikeFragment(listingDetail.getUsername(), listingDetail.getId(), Constants.MY_BYN);
            }
        });

        mBinding.makeAnOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listingDetail.getUserId() == mSharedPreferences.getMyUserId()) {
                    mCallback.openListingOfferFragment(listingDetail.getId());
                } else {
                    mCallback.openMakeAnOfferFragment(listingDetail.getId(), 0, 0);
                }
            }
        });
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setData(ListingDetail details) {
        this.listingDetail = details;
        mCallback.getListingDetailImages(id);
    }

    public void setImages(List<ListingDisplayImage> listingDetailImages) {
        this.mImagesList = listingDetailImages;
        mBinding.listingDetailMainLayout.setVisibility(View.VISIBLE);
        listingDetail.setListingDisplayImages(mImagesList);
        mBinding.priceBottom.setText(String.format("$%.2f", listingDetail.getPrice()));

        if (listingDetail.getUserId() == mSharedPreferences.getMyUserId()) {
            String listingStatus = listingDetail.getStatus().getStatusName().toLowerCase();
            switch (listingStatus) {
                case Constants.ON_SOLD:
                    mBinding.priceBottom.setVisibility(View.VISIBLE);
                    mBinding.buyNow.setText("SOLD");
                    mBinding.makeAnOffer.setVisibility(View.GONE);
                    mBinding.buyNowLayout.setEnabled(false);
                    mBinding.buyNowLayout.setBackgroundColor(mContext.getResources().getColor(R.color.sold_color));
                    break;
                case Constants.ON_HOLD:
                    mBinding.priceBottom.setVisibility(View.VISIBLE);
                    mBinding.buyNow.setText("ON HOLD");
                    mBinding.makeAnOffer.setVisibility(View.GONE);
                    mBinding.buyNowLayout.setEnabled(false);
                    mBinding.buyNowLayout.setBackgroundColor(mContext.getResources().getColor(R.color.orange));
                    break;
                case Constants.NOT_FOR_SALE:
                    mBinding.priceBottom.setVisibility(View.VISIBLE);
                    mBinding.buyNow.setText("NO SALE");
                    mBinding.makeAnOffer.setVisibility(View.GONE);
                    mBinding.buyNowLayout.setEnabled(false);
                    mBinding.buyNowLayout.setBackgroundColor(mContext.getResources().getColor(R.color.delete_post_black_color));
                    break;
                default:
                    mBinding.priceBottom.setVisibility(View.GONE);
                    mBinding.buyNow.setText("Edit");
                    if (listingDetail.isBuyItNowOnly()) {
                        mBinding.makeAnOffer.setVisibility(View.GONE);
                    } else {
                        mBinding.makeAnOffer.setVisibility(View.VISIBLE);
                        mBinding.makeAnOffer.setText(getString(R.string.view_offers));
                    }
                    mBinding.buyNowLayout.setEnabled(true);
                    mBinding.buyNowLayout.setBackgroundColor(mContext.getResources().getColor(R.color.purple));
            }

        } else {
            String listingStatus = listingDetail.getStatus().getStatusName().toLowerCase();
            switch (listingStatus) {
                case Constants.ON_SOLD:
                    mBinding.priceBottom.setVisibility(View.VISIBLE);
                    mBinding.buyNow.setText("SOLD");
                    mBinding.makeAnOffer.setVisibility(View.GONE);
                    mBinding.buyNowLayout.setEnabled(false);
                    mBinding.buyNowLayout.setBackgroundColor(mContext.getResources().getColor(R.color.sold_color));
                    break;
                case Constants.ON_HOLD:
                    mBinding.priceBottom.setVisibility(View.VISIBLE);
                    mBinding.buyNow.setText("ON HOLD");
                    mBinding.makeAnOffer.setVisibility(View.GONE);
                    mBinding.buyNowLayout.setEnabled(false);
                    mBinding.buyNowLayout.setBackgroundColor(mContext.getResources().getColor(R.color.orange));
                    break;
                case Constants.NOT_FOR_SALE:
                    mBinding.priceBottom.setVisibility(View.VISIBLE);
                    mBinding.buyNow.setText("NO SALE");
                    mBinding.makeAnOffer.setVisibility(View.GONE);
                    mBinding.buyNowLayout.setEnabled(false);
                    mBinding.buyNowLayout.setBackgroundColor(mContext.getResources().getColor(R.color.delete_post_black_color));
                    break;
                default:
                    if (listingDetail.isBuyItNowOnly()) {
                        mBinding.makeAnOffer.setVisibility(View.GONE);
                    } else {
                        if (listingDetail.isDoesLoggedInUserHaveActiveBid()) {
                            mBinding.makeAnOffer.setText(getString(R.string.view_offer));
                        } else {
                            mBinding.makeAnOffer.setText(getString(R.string.make_an_offer));
                        }
                        mBinding.makeAnOffer.setVisibility(View.VISIBLE);
                    }
                    mBinding.buyNow.setText("Buy now");
                    mBinding.priceBottom.setVisibility(View.VISIBLE);
                    mBinding.buyNowLayout.setEnabled(true);
                    mBinding.buyNowLayout.setBackgroundColor(mContext.getResources().getColor(R.color.purple));
            }

        }

        listingDetailAdapter = new ListingDetailAdapter(mContext, listingDetail, mCallback);
        mBinding.recycleView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recycleView.setAdapter(listingDetailAdapter);
    }

    public void setComments(List<Comments> comments) {
        this.listingDetail.setComments(comments);
        listingDetailAdapter.notifyDataSetChanged();
    }
}