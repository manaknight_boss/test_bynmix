package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.ListingOffersAdapter;
import com.bynmix.app.databinding.ListingOffersFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.OfferDetail;
import com.bynmix.app.models.SubOfferHistory;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListingOfferFragment extends BaseFragment {

    private ListingOffersFragmentBinding mBinding;
    private Context mContext;
    private CreatePostListener mCallback;
    private List<SubOfferHistory> mList = new ArrayList<>();
    private ListingOffersAdapter mAdapter;
    private boolean isAlreadyCalled;
    private int currentPageNumber;
    private int totalPages;
    private int listingId;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (CreatePostListener) context;
    }

    public static ListingOfferFragment newInstance() {
        return new ListingOfferFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = ListingOffersFragmentBinding.inflate(inflater, container, false);
        if (mList.size() == 0) {

            currentPageNumber = 0;
            fetchBuyerBids();

        }
        setAdapter();
        mBinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                {
                    if (mCallback.conditionsForApiCall()) {
                        currentPageNumber = 0;
                        fetchBuyerBids();
                    } else {
                        hideSwipeRefresh();
                    }
                }
            }
        });

        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();
                boolean endHasBeenReached = lastVisible >= totalItemCount - 4;
                if (totalItemCount > 0 && endHasBeenReached) {
                    if (!isAlreadyCalled && currentPageNumber < totalPages && mList.size() > 0) {
                        isAlreadyCalled = true;
                        fetchBuyerBids();
                    }
                }

            }
        });


        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        return mBinding.getRoot();
    }

    private void setLoadMoreProgress() {
        if (mList.size() == 0) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
        } else {
            mBinding.progressBar.setVisibility(View.GONE);
        }
    }

    private void hideSwipeRefresh() {
        mBinding.swipeRefreshLayout.setRefreshing(false);
    }

    private void fetchBuyerBids() {
        setLoadMoreProgress();
        getBuyerBids();
    }

    private void getBuyerBids() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getBuyerBids("Bearer " + getAccessToken(), "" + listingId, currentPageNumber + 1).enqueue(new Callback<ApiResponse<OfferDetail>>() {
            @Override
            public void onResponse(Call<ApiResponse<OfferDetail>> call, Response<ApiResponse<OfferDetail>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    OfferDetail offerDetail = response.body().getData();
                    int currentPage = offerDetail.getPageNumber();
                    int totalPages = offerDetail.getTotalPages();
                    List<SubOfferHistory> mList = offerDetail.getItems();
                    setList(mList, currentPage, totalPages);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getBuyerBids();
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    mBinding.progressBar.setVisibility(View.GONE);
                    showAlertDialog(getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<OfferDetail>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });

    }

    private void setAdapter() {
        mAdapter = new ListingOffersAdapter(mContext, mList, mCallback);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    public void callList() {
        currentPageNumber = 0;
        fetchBuyerBids();
    }

    public void setList(List<SubOfferHistory> list, int currentPage, int totalPages) {
        this.currentPageNumber = currentPage;
        this.totalPages = totalPages;
        if (currentPageNumber > 1) {
            this.mList.addAll(list);
        } else {
            this.mList = list;
        }
        if (mList.size() > 0) {
            mBinding.recyclerView.setVisibility(View.VISIBLE);
            mBinding.noItemLayout.setVisibility(View.GONE);
        } else {
            mBinding.recyclerView.setVisibility(View.GONE);
            mBinding.noItemLayout.setVisibility(View.VISIBLE);
            mBinding.errorText.setText(getString(R.string.my_offers_error));
        }
        isAlreadyCalled = false;
        mBinding.progressBar.setVisibility(View.GONE);
        mAdapter.setList(mList);
        hideSwipeRefresh();
    }

    public void setId(int listingId) {
        this.listingId = listingId;
    }

    public void refreshoffers() {
        currentPageNumber = 0;
        callList();
    }
}
