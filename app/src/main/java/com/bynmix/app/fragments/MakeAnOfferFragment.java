package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.adapter.OfferHistoryAdapter;
import com.bynmix.app.databinding.MakeOfferFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.CommentListener;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.MakeAnOfferListener;
import com.bynmix.app.interfaces.OfferHistoryListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.models.OfferHistory;
import com.bynmix.app.models.SubOfferHistory;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.sharedpreference.SharedPreferenceUtility;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.DialogAlert;
import com.bynmix.app.utils.ErrorDialog;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MakeAnOfferFragment extends BaseFragment implements OfferHistoryListener {

    private MakeOfferFragmentBinding mBinding;
    private Context mContext;
    private CreatePostListener mCallback;
    private List<SubOfferHistory> mList = new ArrayList<>();
    private ListingDetail listingDetail;
    private OfferHistoryAdapter adapter;
    private SharedPreferenceUtility mSharedPreferenceUtility;
    private int listingId;
    private int offerId;
    private int buyerId;
    private boolean isBuyer;
    private OfferHistory offerHistory = new OfferHistory();


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (CreatePostListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = MakeOfferFragmentBinding.inflate(inflater, container, false);
        mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);
        if (listingDetail == null) {
            mCallback.getListingDetail(listingId);
        } else {
            setData();
        }
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        mBinding.makeAnOfferLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogAlert.makeAnOfferDialog(mContext, listingDetail, isBuyer, new MakeAnOfferListener() {
                    @Override
                    public void submitOffer(String offer) {
                        if (offerHistory.isListingBuyItNowOnly()) {
                            mCallback.openOfferCompletionFragment(listingDetail, Constants.BUY);
                        } else {
                            double offerPrice = Double.parseDouble(offer);
                            listingDetail.setOfferPrice(offerPrice);
                            listingDetail.setOfferId(offerId);
                            listingDetail.setBuyerName(offerHistory.getBuyerUsername());
                            mCallback.openOfferCompletionFragment(listingDetail, Constants.OFFER);
                        }
                    }
                });
            }
        });

        return mBinding.getRoot();
    }

    private void setData() {
        if (this.listingDetail.getUserId() == mSharedPreferenceUtility.getMyUserId()) {
            mBinding.makeAnOffer.setText(getString(R.string.counter_offer));
            getOfferHistorySeller(listingDetail.getId(), buyerId);
            isBuyer = false;
        } else {
            mBinding.makeAnOffer.setText(getString(R.string.make_new_offer));
            getOfferHistory(listingDetail.getId());
            isBuyer = true;
        }
        setRecyclerView();
    }

    private void setRecyclerView() {
        adapter = new OfferHistoryAdapter(mContext, mList, mCallback, listingDetail, isBuyer, offerHistory.isOfferCancelable(), this);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(adapter);
    }

    public static MakeAnOfferFragment newInstance() {
        return new MakeAnOfferFragment();
    }

    public void updateList(List<SubOfferHistory> list) {
        this.mList = list;
        mBinding.recyclerView.setVisibility(View.VISIBLE);
        if (offerHistory.isOfferAllowed()) {
            if (offerHistory.isListingBuyItNowOnly()) {
                mBinding.makeAnOffer.setText("Buy");
            } else {
                if (isBuyer) {
                    mBinding.makeAnOffer.setText(getString(R.string.make_new_offer));
                } else {
                    mBinding.makeAnOffer.setText(getString(R.string.counter_offer));
                }
            }
            mBinding.makeAnOfferLayout.setVisibility(View.VISIBLE);
        } else {
            mBinding.makeAnOfferLayout.setVisibility(View.GONE);
        }
        adapter.setList(mList);
        mBinding.recyclerView.scrollToPosition(mList.size() - 1);
    }

    public void setListingDetail(ListingDetail listingDetail) {
        this.listingDetail = listingDetail;
        setData();
    }

    public void setListingId(int listingId, int offerId, int buyerId) {
        this.listingId = listingId;
        this.offerId = offerId;
        this.buyerId = buyerId;
    }

    public void updateOfferHistory(OfferHistory offerHistory) {
        this.offerHistory = offerHistory;
        adapter.setIsOfferCancelable(offerHistory.isOfferCancelable());
        updateList(offerHistory.getOfferHistories());
    }

    @Override
    public void cancelOffer(final SubOfferHistory offerHistory) {
        showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        final int listingId = offerHistory.getListingId();
        final int offerId = offerHistory.getOfferId();
        apiInstance.cancelOffer("Bearer " + getAccessToken(), "" + listingId, "" + offerId).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    getOfferHistory(listingId);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            cancelOffer(offerHistory);
                        }

                        @Override
                        public void errorAccessToken() {
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }

                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                showAlertDialog(getResources().getString(R.string.error_server));
                hideProgress();
            }
        });
    }


    public void getOfferHistorySeller(final int id, final int buyerId) {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.offerHistorySeller("Bearer " + getAccessToken(), "" + id, "" + buyerId, "DESC").enqueue(new Callback<ApiResponse<OfferHistory>>() {
            @Override
            public void onResponse(Call<ApiResponse<OfferHistory>> call, Response<ApiResponse<OfferHistory>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    OfferHistory offerHistory = response.body().getData();
                    if (offerHistory == null) {
                        offerHistory = new OfferHistory();
                    }
                    updateOfferHistory(offerHistory);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getOfferHistorySeller(id, buyerId);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<OfferHistory>> call, Throwable t) {
                hideProgress();
                mBinding.progressBar.setVisibility(View.GONE);
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    public void getOfferHistory(final int id) {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.offerHistory("Bearer " + getAccessToken(), "" + id, "DESC").enqueue(new Callback<ApiResponse<OfferHistory>>() {
            @Override
            public void onResponse(Call<ApiResponse<OfferHistory>> call, Response<ApiResponse<OfferHistory>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    OfferHistory offerHistory = response.body().getData();
                    if (offerHistory == null) {
                        offerHistory = new OfferHistory();
                    }
                    updateOfferHistory(offerHistory);

                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getOfferHistory(id);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<OfferHistory>> call, Throwable t) {
                hideProgress();
                mBinding.progressBar.setVisibility(View.GONE);
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });

    }
}
