package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.MyAddressesAdapter;
import com.bynmix.app.databinding.MyAddressesFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.AddressListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.Address;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Feeds;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.SavingPostDialog;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyAddressFragment extends BaseFragment implements AddressListener {

    private MyAddressesFragmentBinding mBinding;
    private Context mContext;
    private ProfileListener mCallBack;
    private List<Address> mAddressesList = new ArrayList<>();
    private MyAddressesAdapter mAdapter;
    private Address shippingAddress;
    private Address returnAddress;

    public static MyAddressFragment newInstance() {
        return new MyAddressFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mCallBack = (ProfileListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = MyAddressesFragmentBinding.inflate(inflater, container, false);
        setAdapter();
        mBinding.addNewAddressLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Address address = new Address();
                address.setShippingAddress(true);
                address.setReturnAddress(false);
                mCallBack.openAddNewAddressFragment(address, Constants.ADD_ADDRESS, null);
            }
        });
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.onBackPressed();
            }
        });
        mBinding.addAddressFragment.shippingAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Address address = new Address();
                address.setShippingAddress(true);
                address.setReturnAddress(false);
                mCallBack.openAddNewAddressFragment(address, Constants.ADD_ADDRESS,null);
            }
        });
        mBinding.addAddressFragment.returnAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Address address = new Address();
                address.setReturnAddress(true);
                address.setShippingAddress(false);
                mCallBack.openAddNewAddressFragment(address, Constants.ADD_ADDRESS, null);
            }
        });
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        mBinding.progressBar.setVisibility(View.VISIBLE);
        getMyAddress();
        shippingAddress = null;
        returnAddress = null;
    }

    private void setAdapter() {
        mAdapter = new MyAddressesAdapter(mContext, mAddressesList, shippingAddress, returnAddress, mCallBack, this);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    public void updateList(final List<Address> addresses) {
        this.mAddressesList = addresses;
        mBinding.progressBar.setVisibility(View.GONE);
        if (mAddressesList.size() > 0) {
            mBinding.addNewAddressLayout.setVisibility(View.VISIBLE);
            mBinding.recyclerView.setVisibility(View.VISIBLE);

            List<Address> defaultAddresses = new ArrayList<>();
            for (int i = 0; i < mAddressesList.size(); i++) {

                boolean isDefaultAddress = false;
                if (mAddressesList.get(i).isShippingAddress()) {
                    shippingAddress = mAddressesList.get(i);
                    isDefaultAddress = true;
                }
                if (mAddressesList.get(i).isReturnAddress()) {
                    returnAddress = mAddressesList.get(i);
                    isDefaultAddress = true;
                }

                if(isDefaultAddress) {
                    defaultAddresses.add(mAddressesList.get(i));
                }
            }
            mAddressesList.removeAll(defaultAddresses);
            mAdapter.setList(mAddressesList, shippingAddress, returnAddress);
        } else {
            mBinding.addAddressFragmentParent.setVisibility(View.VISIBLE);
            mBinding.recyclerView.setVisibility(View.GONE);
            mBinding.addNewAddressLayout.setVisibility(View.GONE);

        }
    }

    public void deleteAddress(int position) {
        mAddressesList.remove(position);
        mAdapter.notifyDataSetChanged();
    }

    public void updateUI() {
        getMyAddress();
    }

    @Override
    public void deleteAddress(final Address address, final int position) {
        final SavingPostDialog loading = new SavingPostDialog(mContext, getString(R.string.deleting_address));
        loading.showDialog();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.deleteAddress("Bearer " + getAccessToken(), "" + address.getAddressId()).enqueue(new Callback<ApiResponse<Feeds>>() {
            @Override
            public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    loading.dismiss();
                    deleteAddress(position);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            deleteAddress(address, position);
                        }

                        @Override
                        public void errorAccessToken() {
                            loading.dismiss();
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    loading.dismiss();
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                loading.dismiss();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }
}
