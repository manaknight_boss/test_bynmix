package com.bynmix.app.fragments;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.adapter.FeedAdapter;
import com.bynmix.app.adapter.ListingAdapter;
import com.bynmix.app.adapter.MyBynAdapter;
import com.bynmix.app.adapter.MyPostAdapter;
import com.bynmix.app.databinding.MyPostFragmentBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.models.Feeds;
import com.bynmix.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class MyBynFragment extends BaseFragment {
    private MyPostFragmentBinding mBinding;
    private List<FeedResponse> mBynFeedList = new ArrayList<>();
    private MyBynAdapter adapter;
    private ListingAdapter listingAdapter;
    private Context mContext;
    private ProfileListener mCallback;
    private int userId;
    private String username;
    private int type;
    private boolean isAlreadyCalled;
    private int currentPageNumber;
    private int totalPages;
    private boolean isCallApi;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (ProfileListener) mContext;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = MyPostFragmentBinding.inflate(inflater, container, false);
        isAlreadyCalled = false;
        if (type == Constants.LIKE_FRAGMENT) {
            listingAdapter = new ListingAdapter(mContext, mBynFeedList, mCallback);
            mBinding.recycleView.setLayoutManager(new GridLayoutManager(mContext, 2));
            mBinding.recycleView.setAdapter(listingAdapter);
        } else {
            adapter = new MyBynAdapter(mContext, mBynFeedList, mCallback, type);
            mBinding.recycleView.setLayoutManager(new GridLayoutManager(mContext, 2));
            mBinding.recycleView.setAdapter(adapter);
        }
        setProgressBarColor();
        mBinding.nothingToShowPopUp.setVisibility(View.GONE);
        mBinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                {
                    if (mCallback.conditionsForApiCall()) {
                        currentPageNumber = 0;
                        fetchMyByn();
                    } else {
                        hideSwipeRefresh();
                    }
                }
            }
        });
        if (isCallApi) {
            if (mBynFeedList.size() == 0) {
                fetchMyByn();
            }
        }

        mBinding.recycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();
                boolean endHasBeenReached = lastVisible >= totalItemCount - 4;
                if (totalItemCount > 0 && endHasBeenReached) {
                    if (!isAlreadyCalled && currentPageNumber < totalPages && mBynFeedList.size() > 0) {
                        isAlreadyCalled = true;
                        fetchMyByn();
                    }
                }

            }
        });

        return mBinding.getRoot();
    }

    private void fetchMyByn() {
        setLoadMoreProgress();
        if (type == Constants.LIKE_FRAGMENT) {
            fetchMyBynLikes(currentPageNumber);
        } else {
            fetchMyByn(userId, currentPageNumber);
        }
    }

    public void setList(List<FeedResponse> postFeeds, int currentPageNumber, int totalPages) {
        this.currentPageNumber = currentPageNumber;
        this.totalPages = totalPages;
        if (currentPageNumber > 1) {
            this.mBynFeedList.addAll(postFeeds);
        } else {
            this.mBynFeedList = postFeeds;
        }
        isAlreadyCalled = false;
        setLoadMoreProgress();
        if (type == Constants.LIKE_FRAGMENT) {
            listingAdapter.setList(mBynFeedList);
        } else {
            adapter.setList(mBynFeedList);
        }
        mBinding.nothingToShowPopUp.setVisibility(View.GONE);
        hideSwipeRefresh();
    }

    private void hideSwipeRefresh() {
        mBinding.swipeRefreshLayout.setRefreshing(false);
    }

    public void setError(int error) {
        currentPageNumber = 0;
        mBynFeedList.clear();
        if (mBynFeedList.size() == 0) {
            if (error == 404) {
                mBinding.logo.setImageResource(R.mipmap.logo_black);
                if (type == Constants.LIKE_FRAGMENT) {
                    mBinding.errorText.setText(Html.fromHtml(getString(R.string.no_likes_listing_error)), TextView.BufferType.SPANNABLE);
                } else {
                    setEmptyListErrorMessage();
                }
            } else {
                mBinding.errorText.setText(Html.fromHtml(getString(R.string.sad_cloud_error)), TextView.BufferType.SPANNABLE);
                mBinding.logo.setImageResource(R.mipmap.cloud_sad);
            }
            mBinding.nothingToShowPopUp.setVisibility(View.VISIBLE);
        }
        mBinding.progressBar.setVisibility(View.GONE);
        hideSwipeRefresh();
    }

    private void setEmptyListErrorMessage() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            mBinding.errorText.setText(Html.fromHtml("<b>" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + username + "</font>" + "</b> " + getResources().getString(R.string.not_listing_yet) + " " + "<b>" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + username + "</font>" + "</b> " + getResources().getString(R.string.get_latest_update), Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
        } else {
            mBinding.errorText.setText(Html.fromHtml("<b>" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + username + "</font>" + "</b> " + getResources().getString(R.string.not_listing_yet) + " " + "<b>" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + username + "</font>" + "</b> " + getResources().getString(R.string.get_latest_update)), TextView.BufferType.SPANNABLE);
        }
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setCallBack(ProfileListener mCallback) {
        this.mCallback = mCallback;
    }

    private void setProgressBarColor() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Drawable wrapDrawable = DrawableCompat.wrap(mBinding.progressBar.getIndeterminateDrawable());
            DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(mContext, R.color.purple));
            mBinding.progressBar.setIndeterminateDrawable(DrawableCompat.unwrap(wrapDrawable));
        } else {
            mBinding.progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(mContext, R.color.purple), PorterDuff.Mode.SRC_IN);
        }
    }


    public void callList() {
        if (mBynFeedList.size() == 0) {
            currentPageNumber = 0;
            mBinding.nothingToShowPopUp.setVisibility(View.GONE);
            fetchMyByn();
        }

    }

    public void setUsername(String username) {
        this.username = username;
    }

    private void setLoadMoreProgress() {
        if (mBynFeedList.size() == 0) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
        } else {
            mBinding.progressBar.setVisibility(View.GONE);
        }
        if (type == Constants.LIKE_FRAGMENT) {
            if (isAlreadyCalled) {
                listingAdapter.setProgress(true);
            } else {
                listingAdapter.setProgress(false);
            }
        } else {
            if (isAlreadyCalled) {
                adapter.setProgress(true);
            } else {
                adapter.setProgress(false);
            }
        }
    }

    public void setType(int type) {
        this.type = type;
    }

    public void clearList() {
        mBynFeedList.clear();
        currentPageNumber = 0;
    }

    public void updateItem(FeedResponse feeds) {
        if (type != Constants.LIKE_FRAGMENT) {
            if (mBynFeedList != null && mBynFeedList.size() > 0) {
                for (int i = 0; i < mBynFeedList.size(); i++) {
                    FeedResponse tempFeed = mBynFeedList.get(i);
                    if (tempFeed.getId() == feeds.getId() && tempFeed.getType().equalsIgnoreCase(feeds.getType())) {
                        tempFeed.setLikesCount(feeds.getLikesCount());
                        tempFeed.setUserLiked(feeds.isUserLiked());
                        mBynFeedList.set(i, tempFeed);
                        break;
                    }
                }
                adapter.setList(mBynFeedList);
            } else {
                setError(404);
            }
        }
    }

    public void isCallApi(boolean callApi) {
        this.isCallApi = callApi;
    }
}
