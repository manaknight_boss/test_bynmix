package com.bynmix.app.fragments;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.adapter.MyBynProfileAdapter;
import com.bynmix.app.databinding.EditPostFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.MyBynListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.models.Feeds;
import com.bynmix.app.models.FilterData;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.SavingPostDialog;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyBynProfileFragment extends BaseFragment implements MyBynListener {

    private EditPostFragmentBinding mBinding;
    private List<FeedResponse> mBynFeedList = new ArrayList<>();
    private MyBynProfileAdapter adapter;
    private Context mContext;
    private ProfileListener mCallback;
    private boolean isAlreadyCalled;
    private int currentPageNumber;
    private int totalPages;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (ProfileListener) context;
    }

    public static MyBynProfileFragment newInstance() {
        return new MyBynProfileFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = EditPostFragmentBinding.inflate(inflater, container, false);
        adapter = new MyBynProfileAdapter(mContext, mBynFeedList, mCallback, this);
        mBinding.toolbarHeading.setText(getString(R.string.my_byn));
        mBinding.noItemLayout.setVisibility(View.GONE);
        if (mBynFeedList.size() == 0) {
            getMyByn();
        }
        mBinding.recyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
        mBinding.recyclerView.setAdapter(adapter);
        setProgressBarColor();
        mBinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                {

                    if (mCallback.conditionsForApiCall()) {
                        currentPageNumber = 0;
                        getMyByn();
                    } else {
                        hideSwipeRefresh();
                    }
                }
            }
        });

        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();
                boolean endHasBeenReached = lastVisible >= totalItemCount - 4;
                if (totalItemCount > 0 && endHasBeenReached) {
                    if (!isAlreadyCalled && currentPageNumber < totalPages && mBynFeedList.size() > 0) {
                        isAlreadyCalled = true;
                        getMyByn();
                    }
                }

            }
        });

        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        mCallback.clearListingDetails();

        mBinding.filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openFilterFragment(Constants.PROFILE_FILTER);
            }
        });

        return mBinding.getRoot();
    }

    private void setProgressBarColor() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Drawable wrapDrawable = DrawableCompat.wrap(mBinding.progressBar.getIndeterminateDrawable());
            DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(mContext, R.color.purple));
            mBinding.progressBar.setIndeterminateDrawable(DrawableCompat.unwrap(wrapDrawable));
        } else {
            mBinding.progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(mContext, R.color.purple), PorterDuff.Mode.SRC_IN);
        }
    }

    private void getMyByn() {
        setLoadMoreProgress();
        getMyByn(currentPageNumber);

    }

    private void hideSwipeRefresh() {
        mBinding.swipeRefreshLayout.setRefreshing(false);
    }


    public void updateMyBynFeeds(List<FeedResponse> myByn, int currentPageNumber, int totalPages) {
        this.currentPageNumber = currentPageNumber;
        this.totalPages = totalPages;
        if (currentPageNumber > 1) {
            this.mBynFeedList.addAll(myByn);
        } else {
            this.mBynFeedList = myByn;
        }
        isAlreadyCalled = false;
        setLoadMoreProgress();
        adapter.setList(mBynFeedList);
        hideSwipeRefresh();

    }

    public void onItemFound() {
        mBinding.progressBar.setVisibility(View.GONE);
        hideSwipeRefresh();
        mBinding.noItemLayout.setVisibility(View.VISIBLE);
        mBinding.errorText.setText(getString(R.string.my_byn_error));
    }

    public void error() {
        mBinding.progressBar.setVisibility(View.GONE);
        hideSwipeRefresh();
        mBinding.noItemLayout.setVisibility(View.VISIBLE);
        mBinding.errorText.setText(getString(R.string.something_went_wrong));
    }

    private void setLoadMoreProgress() {
        if (mBynFeedList.size() == 0) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
        } else {
            mBinding.progressBar.setVisibility(View.GONE);
        }
        if (isAlreadyCalled) {
            adapter.setProgress(true);
        } else {
            adapter.setProgress(false);
        }
    }

    public void clearBynList() {
        mBynFeedList.clear();
        currentPageNumber = 0;
    }

    public void deleteListing(int position) {
        mBynFeedList.remove(position);
        adapter.notifyDataSetChanged();
    }

    public void updateListing(int position, int type) {
        if (type == Constants.DISABLE) {
            mBynFeedList.get(position).setActive(false);
        } else {
            mBynFeedList.get(position).setActive(true);
        }
        adapter.setList(mBynFeedList);
    }

    @Override
    public void disableListing(final int id, final int position, final int type) {
        String title;
        if (type == Constants.DISABLE) {
            title = getString(R.string.disable_listing);
        } else {
            title = getString(R.string.enable_listing);
        }
        final SavingPostDialog loading = new SavingPostDialog(mContext, title);
        loading.showDialog();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.enableDisableListing("Bearer " + getAccessToken(), "" + id).enqueue(new Callback<ApiResponse<Feeds>>() {
            @Override
            public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    loading.dismiss();
                    updateListing(position, type);

                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            disableListing(id, position, type);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                            loading.dismiss();
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void deleteListing(final int id, final int position) {
        final SavingPostDialog loading = new SavingPostDialog(mContext, getString(R.string.deleting_listing));
        loading.showDialog();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.deleteListing("Bearer " + getAccessToken(), "" + id).enqueue(new Callback<ApiResponse<Feeds>>() {
            @Override
            public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    loading.dismiss();
                    deleteListing(position);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            deleteListing(id, position);
                        }

                        @Override
                        public void errorAccessToken() {
                            loading.dismiss();
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    public void getMyByn(final int currentPageNumber) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        if (((HomeActivity) mContext).filterFragmentType != Constants.PROFILE_FILTER) {
            ((HomeActivity) mContext).filterData = new FilterData();
        }
        apiInstance.getMyBynFeeds("Bearer " + getAccessToken(), ((HomeActivity) mContext).filterData, "desc", currentPageNumber + 1).enqueue(new Callback<ApiResponse<Feeds>>() {
            @Override
            public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    Feeds feeds = response.body().getData();
                    int currentPage = feeds.getPageNumber();
                    int totalPages = feeds.getTotalPages();
                    List<FeedResponse> myByn = feeds.getItems();
                    updateMyBynFeeds(myByn, currentPage, totalPages);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getMyByn(currentPageNumber);
                        }

                        @Override
                        public void errorAccessToken() {
                        }
                    });
                } else if (response.code() == 404) {
                    onItemFound();

                } else {
                    error();
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }
}
