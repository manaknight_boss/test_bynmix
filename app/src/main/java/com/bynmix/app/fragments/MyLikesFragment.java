package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.FeedViewPagerAdapter;
import com.bynmix.app.databinding.MyLikesFragmentBinding;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.models.Feeds;
import com.bynmix.app.utils.Constants;

import java.util.List;

public class MyLikesFragment extends BaseFragment {

    private MyLikesFragmentBinding mBinding;
    private CreatePostListener mCallback;
    private Context mContext;
    private FeedViewPagerAdapter feedViewPagerAdapter;
    private MyBynFragment myBynFragment;
    private MyPostFragment myPostFragment;
    private int userId;
    private String username;
    private int fragmentType;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (CreatePostListener) context;
        mContext = context;
    }

    public static MyLikesFragment newInstance() {
        return new MyLikesFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = MyLikesFragmentBinding.inflate(inflater, container, false);
        setViewPager();
        setType();
        setUserId();
        if(fragmentType == Constants.MY_POST){
            mBinding.filter.setVisibility(View.GONE);
            myPostFragment.isCallApi(true);
        }else {
            myBynFragment.isCallApi(true);
            mBinding.filter.setVisibility(View.VISIBLE);
        }
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        mBinding.filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openFilterFragment(Constants.PROFILE_FILTER);
            }
        });

        return mBinding.getRoot();
    }

    private void setUserId() {
        myBynFragment.setUserId(userId);
        myBynFragment.setUsername(username);
        myPostFragment.setUsername(username);
        myPostFragment.setUserId(userId);
    }

    private void setType() {
        myBynFragment.setType(Constants.LIKE_FRAGMENT);
        myPostFragment.setType(Constants.LIKE_FRAGMENT);
    }

    private void setViewPager() {
        if (feedViewPagerAdapter == null) {
            feedViewPagerAdapter = new FeedViewPagerAdapter(getChildFragmentManager());
            myBynFragment = new MyBynFragment();
            myPostFragment = new MyPostFragment();
            feedViewPagerAdapter.addFragment(myBynFragment, getResources().getString(R.string.listing));
            feedViewPagerAdapter.addFragment(myPostFragment, getResources().getString(R.string.posts));
        }
        mBinding.viewpager.setAdapter(feedViewPagerAdapter);
        mBinding.viewpager.setOffscreenPageLimit(2);
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);
        mBinding.viewpager.setCurrentItem(fragmentType);

        mBinding.viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    myBynFragment.callList();
                    mBinding.filter.setVisibility(View.VISIBLE);
                } else {
                    myPostFragment.callList();
                    mBinding.filter.setVisibility(View.GONE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void setMyUserId(int myUserId) {
        this.userId = myUserId;
    }

    public void updateMyPostFeeds(List<FeedResponse> myPostFeeds, int currentPageNumber, int totalPage) {
        myPostFragment.setList(myPostFeeds, currentPageNumber, totalPage);
    }

    public void updateMyBynFeeds(List<FeedResponse> myBynFeeds, int currentPageNumber, int totalPage) {
        myBynFragment.setList(myBynFeeds, currentPageNumber, totalPage);
    }

    public void onItemFound(int postType) {
        if (postType == Constants.MY_POST) {
            myPostFragment.setError(404);
        } else {
            myBynFragment.setError(404);
        }
    }

    public void error(int postType) {
        if (postType == Constants.MY_POST) {
            myPostFragment.setError(400);
        } else {
            myBynFragment.setError(400);
        }
    }

    public void clearBynList() {
        myBynFragment.clearList();
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void updateItem(FeedResponse feeds) {
        myBynFragment.updateItem(feeds);
        myPostFragment.updateItem(feeds);
    }

    public void setPostDotsTags(List<AnnotatedPoint> mList, int position) {
        myPostFragment.setPostDotsTags(mList, position);
    }

    public void setFragmentType(int fragmentType) {
        this.fragmentType = fragmentType;
    }
}
