package com.bynmix.app.fragments;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.adapter.FeedAdapter;
import com.bynmix.app.adapter.MyPostAdapter;
import com.bynmix.app.databinding.MyPostFragmentBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;


public class MyPostFragment extends BaseFragment {

    private MyPostFragmentBinding mBinding;
    private List<FeedResponse> mPostFeedList = new ArrayList<>();
    private MyPostAdapter adapter;
    private FeedAdapter feedAdapter;
    private Context mContext;
    private ProfileListener mCallback;
    private int userId;
    private String username;
    private boolean isAlreadyCalled;
    private int currentPageNumber;
    private int totalPages;
    private int type;
    private boolean isCallApi;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (ProfileListener) context;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = MyPostFragmentBinding.inflate(inflater, container, false);
        isAlreadyCalled = false;
        if (type == Constants.LIKE_FRAGMENT) {
            feedAdapter = new FeedAdapter(mContext, mPostFeedList, Constants.AlL_POSTS, mCallback);
            mBinding.recycleView.setLayoutManager(new LinearLayoutManager(mContext));
            mBinding.recycleView.setAdapter(feedAdapter);
        } else {
            adapter = new MyPostAdapter(mContext, mPostFeedList, mCallback, type);
            mBinding.recycleView.setLayoutManager(new LinearLayoutManager(mContext));
            mBinding.recycleView.setAdapter(adapter);
        }
        setProgressBarColor();
        mBinding.nothingToShowPopUp.setVisibility(View.GONE);

        mBinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                {

                    if (mCallback.conditionsForApiCall()) {
                        currentPageNumber = 0;
                        fetchPostFeeds();
                    } else {
                        hideSwipeRefresh();
                    }
                }
            }
        });

        mBinding.recycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();
                boolean endHasBeenReached = lastVisible >= totalItemCount - 4;
                if (totalItemCount > 0 && endHasBeenReached) {
                    if (!isAlreadyCalled && currentPageNumber < totalPages && mPostFeedList.size() > 0) {
                        isAlreadyCalled = true;
                        fetchPostFeeds();
                    }
                }

            }
        });
        if (isCallApi) {
            isCallApi = false;
            callList();
        }

        return mBinding.getRoot();
    }

    private void setProgressBarColor() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            Drawable wrapDrawable = DrawableCompat.wrap(mBinding.progressBar.getIndeterminateDrawable());
            DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(mContext, R.color.purple));
            mBinding.progressBar.setIndeterminateDrawable(DrawableCompat.unwrap(wrapDrawable));
        } else {
            mBinding.progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(mContext, R.color.purple), PorterDuff.Mode.SRC_IN);
        }
    }

    private void fetchPostFeeds() {
        setLoadMoreProgress();
        if (type == Constants.LIKE_FRAGMENT) {
            fetchMyPostLike(currentPageNumber);
        } else {
            fetchMyPost(userId, currentPageNumber);
        }
    }

    public void setList(List<FeedResponse> postFeeds, int currentPageNumber, int totalPages) {
        this.currentPageNumber = currentPageNumber;
        this.totalPages = totalPages;
        if (currentPageNumber > 1) {
            this.mPostFeedList.addAll(postFeeds);
        } else {
            this.mPostFeedList = postFeeds;
        }
        isAlreadyCalled = false;
        setLoadMoreProgress();
        if (type == Constants.LIKE_FRAGMENT) {
            feedAdapter.setList(mPostFeedList);
        } else {
            adapter.setList(mPostFeedList);
        }
        mBinding.nothingToShowPopUp.setVisibility(View.GONE);
        hideSwipeRefresh();
    }

    private void hideSwipeRefresh() {
        mBinding.swipeRefreshLayout.setRefreshing(false);
    }

    public void setError(int error) {
        if (mPostFeedList.size() == 0) {
            if (error == 404) {
                mBinding.logo.setImageResource(R.mipmap.logo_black);
                if (type == Constants.LIKE_FRAGMENT) {
                    mBinding.errorText.setText(Html.fromHtml(getString(R.string.no_likes_post_error)), TextView.BufferType.SPANNABLE);
                } else {
                    setEmptyListErrorMessage();
                }
            } else {
                mBinding.errorText.setText(Html.fromHtml(getString(R.string.sad_cloud_error)), TextView.BufferType.SPANNABLE);
                mBinding.logo.setImageResource(R.mipmap.cloud_sad);
            }
            mBinding.nothingToShowPopUp.setVisibility(View.VISIBLE);
        }
        mPostFeedList.clear();
        currentPageNumber = 0;
        mBinding.progressBar.setVisibility(View.GONE);
        hideSwipeRefresh();
    }

    private void setEmptyListErrorMessage() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            mBinding.errorText.setText(Html.fromHtml("<b>" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + username + "</font>" + "</b> " + getResources().getString(R.string.not_post_yet) + " " + "<b>" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + username + "</font>" + "</b> " + getResources().getString(R.string.get_latest_update), Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
        } else {
            mBinding.errorText.setText(Html.fromHtml("<b>" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + username + "</font>" + "</b> " + getResources().getString(R.string.not_post_yet) + " " + "<b>" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + username + "</font>" + "</b> " + getResources().getString(R.string.get_latest_update)), TextView.BufferType.SPANNABLE);
        }
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setCallBack(ProfileListener mCallback) {
        this.mCallback = mCallback;
    }

    public void callList() {
        if (mPostFeedList.size() == 0) {
            mBinding.nothingToShowPopUp.setVisibility(View.GONE);
            fetchPostFeeds();
        }
    }

    public void setUsername(String username) {
        this.username = username;
    }

    private void setLoadMoreProgress() {
        if (mPostFeedList.size() == 0) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
        } else {
            mBinding.progressBar.setVisibility(View.GONE);
        }
        if (type == Constants.LIKE_FRAGMENT) {
            if (isAlreadyCalled) {
                feedAdapter.setProgress(true);
            } else {
                feedAdapter.setProgress(false);
            }
        } else {
            if (isAlreadyCalled) {
                adapter.setProgress(true);
            } else {
                adapter.setProgress(false);
            }
        }
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setPostDotsTags(List<AnnotatedPoint> mList, int position) {
        mPostFeedList.get(position).setPostDots(mList);
        if (type == Constants.LIKE_FRAGMENT) {
            feedAdapter.setList(mPostFeedList);
            feedAdapter.hideProgress();
        } else {
            adapter.setList(mPostFeedList);
            adapter.hideProgress();
        }
    }

    public void updateItem(FeedResponse feeds) {
        if (type != Constants.LIKE_FRAGMENT) {
            feedAdapter.setList(mPostFeedList);
            if (mPostFeedList != null && mPostFeedList.size() > 0) {
                for (int i = 0; i < mPostFeedList.size(); i++) {
                    FeedResponse tempFeed = mPostFeedList.get(i);
                    if (tempFeed.getId() == feeds.getId() && tempFeed.getType().equalsIgnoreCase(feeds.getType())) {
                        tempFeed.setLikesCount(feeds.getLikesCount());
                        tempFeed.setUserLiked(feeds.isUserLiked());
                        mPostFeedList.set(i, tempFeed);
                        break;
                    }
                }
                adapter.setList(mPostFeedList);
            } else {
                setError(404);
            }
        }
    }

    public void isCallApi(boolean callApi) {
        this.isCallApi = callApi;
    }
}