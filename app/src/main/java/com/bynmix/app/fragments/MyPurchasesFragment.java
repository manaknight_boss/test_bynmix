package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.MyPurchasesAdapter;
import com.bynmix.app.adapter.PurchaseHistoryAdapter;
import com.bynmix.app.databinding.MyPurchaseFragmentBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.PurchaseListener;
import com.bynmix.app.models.Stock;
import com.bynmix.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;


public class MyPurchasesFragment extends Fragment {

    private MyPurchaseFragmentBinding mBinding;
    private List<Stock> mList = new ArrayList<>();
    private MyPurchasesAdapter mAdapter;
    private PurchaseHistoryAdapter purchaseHistoryAdapter;
    private Context mContext;
    private ProfileListener mCallback;
    private boolean isAlreadyCalled;
    private int currentPageNumber;
    private int totalPages;
    private int type;
    private int sortType = 0;
    private RecyclerView recycleView;
    private PurchaseListener purchaseListener;
    public static boolean isReloadRequired = false;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (ProfileListener) context;
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = MyPurchaseFragmentBinding.inflate(inflater, container, false);

        if(isReloadRequired) {
            mList.clear();
            isReloadRequired = false;
        }
        setAdapter();
        if (mList.size() == 0) {
            isReloadRequired = false;
            currentPageNumber = 0;
            showProgress();
            getMyPurchases();
        }


        mBinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                {
                    if (mCallback.conditionsForApiCall()) {
                        currentPageNumber = 0;
                        getMyPurchases();
                    } else {
                        hideSwipeRefresh();
                    }
                }
            }
        });

        recycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();
                boolean endHasBeenReached = lastVisible >= totalItemCount - 4;
                if (totalItemCount > 0 && endHasBeenReached) {
                    if (!isAlreadyCalled && currentPageNumber < totalPages && mList.size() > 0) {
                        isAlreadyCalled = true;
                        getMyPurchases();
                    }
                }

            }
        });
        return mBinding.getRoot();
    }

    private void hideSwipeRefresh() {
        mBinding.swipeRefreshLayout.setRefreshing(false);
    }

    private void getMyPurchases() {
        setLoadMoreProgress();
        if (type == Constants.MY_SALES) {
            purchaseListener.getMySales(currentPageNumber);
        } else {
            purchaseListener.getMyPurchases(currentPageNumber);
        }
    }

    private void setLoadMoreProgress() {
        if (isAlreadyCalled) {
            if (sortType == 0) {
                mAdapter.setProgress(true);
            } else {
                purchaseHistoryAdapter.setProgress(true);
            }
        } else {
            if (sortType == 0) {
                mAdapter.setProgress(false);
            } else {
                purchaseHistoryAdapter.setProgress(false);
            }
        }
    }

    private void setAdapter() {
        if (sortType == 0) {
            recycleView = mBinding.recycleView;
            mBinding.historyRecycleView.setVisibility(View.GONE);
            mBinding.recycleView.setVisibility(View.VISIBLE);
            mAdapter = new MyPurchasesAdapter(mContext, mList, mCallback, purchaseListener);
            recycleView.setLayoutManager(new GridLayoutManager(mContext, 2));
            recycleView.setAdapter(mAdapter);
        } else {
            recycleView = mBinding.historyRecycleView;
            mBinding.historyRecycleView.setVisibility(View.VISIBLE);
            mBinding.recycleView.setVisibility(View.GONE);
            purchaseHistoryAdapter = new PurchaseHistoryAdapter(mContext, mList, mCallback, type, purchaseListener);
            recycleView.setLayoutManager(new LinearLayoutManager(mContext));
            recycleView.setAdapter(purchaseHistoryAdapter);
        }
    }

    public void callList() {
        if (mList.size() == 0) {
            currentPageNumber = 0;
            showProgress();
            getMyPurchases();
        }
    }

    public void setList(List<Stock> list, int currentPage, int totalPages) {
        this.currentPageNumber = currentPage;
        this.totalPages = totalPages;
        if (currentPageNumber > 1) {
            this.mList.addAll(mList);
        } else {
            this.mList = list;
        }
        if (mList.size() == 0) {
            setError();
        } else {
            mBinding.nothingToShowPopUp.setVisibility(View.GONE);
            if (sortType == 0) {
                mAdapter.setList(mList);
            } else {
                purchaseHistoryAdapter.setList(mList);
            }

        }
        isAlreadyCalled = false;
        mBinding.progressBar.setVisibility(View.GONE);
        hideSwipeRefresh();
    }

    public void updateFeedBack(Stock detail, int position) {
        mList.set(position, detail);
        mAdapter.notifyDataSetChanged();
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setError() {
        if (type == Constants.MY_SALES) {
            mBinding.errorText.setText("You have not made any sales yet.");
        } else {
            mBinding.errorText.setText("You have not made any purchases yet.");
        }
        mBinding.nothingToShowPopUp.setVisibility(View.VISIBLE);
        mList.clear();
        currentPageNumber = 0;
    }

    public void setSortType(int sortType) {
        this.sortType = sortType;
        setAdapter();
    }

    public void setListener(PurchaseListener purchaseListener) {
        this.purchaseListener = purchaseListener;
    }

    public void hideProgress() {
        mBinding.progressBar.setVisibility(View.GONE);
    }

    public void showProgress() {
        mBinding.progressBar.setVisibility(View.VISIBLE);
    }

}
