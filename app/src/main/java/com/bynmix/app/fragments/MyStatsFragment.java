package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.databinding.MyStatsFragmentBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.PurchaseListener;
import com.bynmix.app.models.MyPurchasesStats;
import com.bynmix.app.models.MySalesStats;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;

public class MyStatsFragment extends Fragment {

    private MyStatsFragmentBinding mBinding;
    private ProfileListener mCallback;
    private Context mContext;
    private MyPurchasesStats myPurchasesStats;
    private MySalesStats mySalesStats;
    private int type;
    private PurchaseListener purchaseListener;

    public static MyStatsFragment newInstance() {
        return new MyStatsFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (ProfileListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = MyStatsFragmentBinding.inflate(inflater, container, false);
        callList();
        return mBinding.getRoot();
    }

    public void callList() {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        mBinding.mainLayout.setVisibility(View.GONE);
        if (type == Constants.MY_SALES) {
            purchaseListener.getMySalesStats();
        } else {
            purchaseListener.getMyPurchasesStats();
        }
    }

    public void updateMySalesStats(MySalesStats myStats) {
        this.mySalesStats = myStats;
        setData();
    }

    private void setData() {
        mBinding.progressBar.setVisibility(View.GONE);
        mBinding.mainLayout.setVisibility(View.VISIBLE);
        String feedbackText, totalPurchasesText, spentThisMonthText;
        String feedback, totalPurchases, spentThisMonth, pointsEarned, pointsPending;
        if (type == Constants.MY_PURCHASES) {
            feedbackText = "Purchases need feedback";
            totalPurchasesText = "Total Purchases";
            spentThisMonthText = "Spent this month";
            feedback = "" + myPurchasesStats.getPurchasesNeedingFeedback();
            totalPurchases = "" + myPurchasesStats.getTotalPurchaseItems();
            spentThisMonth = Utils.formatValueToTwoDecimalPlaces(myPurchasesStats.getTotalSpentThisMonth());
            pointsEarned = "" + myPurchasesStats.getTotalPurchasePoints();
            pointsPending = "" + myPurchasesStats.getTotalPendingPurchasePoints();
            setValues(feedbackText, totalPurchasesText, spentThisMonthText, feedback, totalPurchases, spentThisMonth, pointsEarned, pointsPending);
        } else {
            feedbackText = "Seller feedback";
            totalPurchasesText = "Total Sales";
            spentThisMonthText = "Total Earned";
            feedback = "" + mySalesStats.getTotalSellerFeedbackScore() + "%";
            totalPurchases = "" + mySalesStats.getTotalSaleItems() + mySalesStats.getTotalPendingSaleItems();
            spentThisMonth = Utils.formatValueToTwoDecimalPlaces(mySalesStats.getTotalSales());
            pointsEarned = "" + mySalesStats.getTotalSalePoints();
            pointsPending = "" + mySalesStats.getTotalPendingSalePoints();
            setValues(feedbackText, totalPurchasesText, spentThisMonthText, feedback, totalPurchases, spentThisMonth, pointsEarned, pointsPending);
        }
    }

    private void setValues(String feedbackText, String totalPurchasesText, String spentThisMonthText, String feedback, String totalPurchases, String spentThisMonth, String pointsEarned, String pointsPending) {
        mBinding.purchasesNeedFeedback.setText(feedback);
        mBinding.totalPurchases.setText(totalPurchases);
        mBinding.spentThisMonth.setText("$" + spentThisMonth);
        mBinding.pointsEarned.setText("" + pointsEarned);
        mBinding.pendingPoints.setText("" + pointsPending);
        mBinding.feedbackText.setText(feedbackText);
        mBinding.totalPurchasesText.setText(totalPurchasesText);
        mBinding.totalEarnedText.setText(spentThisMonthText);
    }

    public void setType(int type) {
        this.type = type;
    }

    public void updateMyPurchasesStats(MyPurchasesStats myStats) {
        this.myPurchasesStats = myStats;
        setData();
    }

    public void setListener(PurchaseListener purchaseListener) {
        this.purchaseListener = purchaseListener;
    }

    public void hideProgress() {
        mBinding.progressBar.setVisibility(View.GONE);
    }

}

