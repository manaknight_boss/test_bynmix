package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.MyWalletAdapter;
import com.bynmix.app.databinding.MyWalletFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.interfaces.MyWalletListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.BankAccount;
import com.bynmix.app.models.CardDetail;
import com.bynmix.app.models.TotalSales;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.DialogAlert;
import com.bynmix.app.utils.SavingPostDialog;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyWalletFragment extends BaseFragment implements MyWalletListener {
    private MyWalletFragmentBinding mBinding;
    private ProfileListener mCallBack;
    private Context mContext;
    private List<CardDetail> mCardDetailList = new ArrayList<>();
    private CardDetail cardDetail = new CardDetail();
    private MyWalletAdapter mAdapter;
    private TotalSales currentBalance;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallBack = (ProfileListener) mContext;
    }

    public static MyWalletFragment newInstance() {
        return new MyWalletFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = MyWalletFragmentBinding.inflate(inflater, container, false);
        setAdapter();
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.onBackPressed();
            }
        });

        mBinding.addNewPaymentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.openAddPaymentFragment(0, null, 0);
            }
        });

        if (currentBalance == null) {
            getPayment();
        } else {
            mCallBack.getWalletDetail();
        }


        return mBinding.getRoot();
    }

    private void setAdapter() {
        mAdapter = new MyWalletAdapter(mContext, mCardDetailList, cardDetail, mCallBack, this);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    public void updateList(List<CardDetail> cardDetailList) {
        this.mCardDetailList = cardDetailList;
        mBinding.recyclerView.setVisibility(View.VISIBLE);
        if (mCardDetailList.size() > 0) {
            mBinding.addNewPaymentLayout.setVisibility(View.VISIBLE);
            for (CardDetail cardDetail : mCardDetailList) {
                if (cardDetail.isDefault()) {
                    this.cardDetail = cardDetail;
                    mCardDetailList.remove(cardDetail);
                    break;
                } else {
                    this.cardDetail = null;
                }
            }
        } else {
            mBinding.addNewPaymentLayout.setVisibility(View.GONE);
            this.cardDetail = null;
        }
        mAdapter.setList(mCardDetailList, cardDetail, currentBalance);
        mBinding.progressBar.setVisibility(View.GONE);
    }

    public void deleteCard(int position) {
        mCardDetailList.remove(position);
        mAdapter.notifyDataSetChanged();
    }

    public void updateBalance(TotalSales currentBalance) {
        this.currentBalance = currentBalance;
        mCallBack.getWalletDetail();
    }

    public void setAsDefault() {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        mBinding.recyclerView.setVisibility(View.GONE);
        mBinding.addNewPaymentLayout.setVisibility(View.GONE);
        mCallBack.getWalletDetail();
    }

    public void getPayment() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getTotalBalance("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<TotalSales>>() {
            @Override
            public void onResponse(Call<ApiResponse<TotalSales>> call, Response<ApiResponse<TotalSales>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    TotalSales currentBalance = response.body().getData();
                    updateBalance(currentBalance);

                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getPayment();
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    mBinding.progressBar.setVisibility(View.GONE);
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<TotalSales>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));

            }
        });
    }

    @Override
    public void checkCard(final Context mContext, final CardDetail cardDetail, final ProfileListener mCallBack, final int position, final List<CardDetail> mList) {
        showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.checkCard("Bearer " + getAccessToken(), "" + cardDetail.getCardId()).enqueue(new Callback<ApiResponse<Boolean>>() {
            @Override
            public void onResponse(Call<ApiResponse<Boolean>> call, Response<ApiResponse<Boolean>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    hideProgress();
                    final Boolean status = response.body().getData();
                    int type;
                    if (cardDetail.isDefault()) {
                        type = 1;
                    } else {
                        type = 0;
                    }

                    DialogAlert.showDialog(mContext, cardDetail, status, type, new DialogClickListener() {
                        @Override
                        public void onClick() {

                            if (!status) {
                                deleteCardDetail(cardDetail, position);
                            } else {
                                mCallBack.openSelectNewPaymentFragment(mList, cardDetail, position);
                            }
                        }
                    });
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            checkCard(mContext, cardDetail, mCallBack, position, mList);
                        }

                        @Override
                        public void errorAccessToken() {
                            hideProgress();
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Boolean>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    public void deleteCardDetail(final CardDetail cardDetail, final int position) {
        final SavingPostDialog loading = new SavingPostDialog(mContext, getString(R.string.deleting_wallet));
        loading.showDialog();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.deleteCardDetail("Bearer " + getAccessToken(), "" + cardDetail.getCardId()).enqueue(new Callback<ApiResponse<CardDetail>>() {
            @Override
            public void onResponse(final Call<ApiResponse<CardDetail>> call, Response<ApiResponse<CardDetail>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    loading.dismiss();
                    deleteCard(position);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            deleteCardDetail(cardDetail, position);
                        }

                        @Override
                        public void errorAccessToken() {
                            loading.dismiss();
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
                loading.dismiss();
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<CardDetail>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                loading.dismiss();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    @Override
    public void setAsDefaultCard(final int cardId) {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.SetAsDefaultCard("Bearer " + getAccessToken(), "" + cardId).enqueue(new Callback<ApiResponse<TotalSales>>() {
            @Override
            public void onResponse(Call<ApiResponse<TotalSales>> call, Response<ApiResponse<TotalSales>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    setAsDefault();
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            setAsDefaultCard(cardId);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<TotalSales>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                showAlertDialog(getResources().getString(R.string.error_server));
                hideProgress();
            }
        });
    }

    public void getPayouts(final int currentBalance) {
        showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getPayout("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<List<BankAccount>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<BankAccount>>> call, Response<ApiResponse<List<BankAccount>>> response) {
                hideProgress();
                if (response.isSuccessful() && response.code() == 200) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    List<BankAccount> payouts = response.body().getData();
                    if (payouts.size() == 0) {
                        mCallBack.openAddBankDetailFragment(currentBalance);
                    } else {
                        mCallBack.openWithDrawFragment(currentBalance, payouts);
                    }
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getPayouts(currentBalance);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<List<BankAccount>>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));

            }
        });
    }
}
