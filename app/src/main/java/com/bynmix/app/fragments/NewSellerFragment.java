package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FragmentNewSellerBinding;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.sharedpreference.SharedPreferenceUtility;
import com.bynmix.app.utils.Utils;

public class NewSellerFragment extends Fragment {

    private FragmentNewSellerBinding mBinding;
    private Context mContext;
    private CreatePostListener mCallback;
    private String displayName;
    private SharedPreferenceUtility mSharedPreferenceUtility;

    public static NewSellerFragment newInstance() {
        return new NewSellerFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (CreatePostListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentNewSellerBinding.inflate(inflater, container, false);
        mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);
        if (displayName == null) {
            displayName = mSharedPreferenceUtility.getDisplayName();
        }
        String name = Utils.getColorText(displayName, mContext.getResources().getColor(R.color.light_purple3));
        mBinding.name.setText(Html.fromHtml("Hi " + name + "!"), TextView.BufferType.SPANNABLE);

        mBinding.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
                mCallback.openSellerAccountSetupFragment();
            }
        });
        return mBinding.getRoot();
    }

}
