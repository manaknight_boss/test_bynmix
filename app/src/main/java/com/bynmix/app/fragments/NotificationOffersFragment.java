package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.FeedViewPagerAdapter;
import com.bynmix.app.databinding.NotificationOffersFragmentBinding;
import com.bynmix.app.interfaces.NotificationListener;
import com.bynmix.app.models.Notification;
import com.bynmix.app.models.Settings;
import com.bynmix.app.sharedpreference.SharedPreferenceUtility;
import com.bynmix.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class NotificationOffersFragment extends Fragment {

    private NotificationOffersFragmentBinding mBinding;
    private Context mContext;
    private FeedViewPagerAdapter feedViewPagerAdapter;
    private MyOffersNotificationFragment myOffersNotificationFragment;
    private ReceivedOfferFragment receivedOfferFragment;
    private Settings settings = new Settings();
    private NotificationListener notificationListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    public static NotificationOffersFragment newInstance() {
        return new NotificationOffersFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = NotificationOffersFragmentBinding.inflate(inflater, container, false);
        setViewPager();
        SharedPreferenceUtility mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);
        if (mSharedPreferenceUtility.getOfferNotification()) {
            mBinding.viewpager.setCurrentItem(0);
        }
        mBinding.receivedOfferButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.viewpager.setCurrentItem(1);
            }
        });

        mBinding.myOfferButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.viewpager.setCurrentItem(0);
            }
        });

        return mBinding.getRoot();
    }

    private void setViewPager() {
        if (feedViewPagerAdapter == null) {
            feedViewPagerAdapter = new FeedViewPagerAdapter(getChildFragmentManager());
            myOffersNotificationFragment = new MyOffersNotificationFragment();
            receivedOfferFragment = new ReceivedOfferFragment();
            feedViewPagerAdapter.addFragment(myOffersNotificationFragment, getResources().getString(R.string.my_offers));
            feedViewPagerAdapter.addFragment(receivedOfferFragment, getResources().getString(R.string.received_offers));
            myOffersNotificationFragment.setListener(notificationListener);
            receivedOfferFragment.setListener(notificationListener);
        }
        mBinding.viewpager.setAdapter(feedViewPagerAdapter);
        mBinding.viewpager.setOffscreenPageLimit(2);
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);

        mBinding.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    myOffersNotificationFragment.callList();
                } else {
                    receivedOfferFragment.callList();
                }
                updateButtonUI(position);

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void updateButtonUI(int position) {
        if (position == 0) {
            mBinding.myOfferButton.setBackground(mContext.getResources().getDrawable(R.drawable.selected_offer_button));
            mBinding.receivedOfferButton.setBackground(mContext.getResources().getDrawable(R.drawable.un_selected_offer_button));
            mBinding.receivedOfferText.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
            mBinding.myOfferText.setTextColor(mContext.getResources().getColor(R.color.white));
        } else {
            mBinding.myOfferButton.setBackground(mContext.getResources().getDrawable(R.drawable.un_selected_offer_button));
            mBinding.receivedOfferButton.setBackground(mContext.getResources().getDrawable(R.drawable.selected_offer_button));
            mBinding.myOfferText.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
            mBinding.receivedOfferText.setTextColor(mContext.getResources().getColor(R.color.white));
        }
    }

    public void updateList(List<Notification> mList, int currentPage, int totalPage) {
        List<Integer> unreadList = new ArrayList<>();
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).isUnread()) {
                unreadList.add(mList.get(i).getNotificationId());
            }
        }
        if (unreadList.size() > 0) {
            settings.setNotificationIds(unreadList);
            notificationListener.decreaseCount(settings);
        }

        myOffersNotificationFragment.setList(mList, currentPage, totalPage);

    }

    public void setList(List<Notification> mList, int currentPage, int totalPage, int type) {
        if (type == Constants.MY_OFFERS) {
            myOffersNotificationFragment.setList(mList, currentPage, totalPage);
        } else {
            receivedOfferFragment.setList(mList, currentPage, totalPage);
        }
    }

    public void refreshNotifications(int type) {
        if (type == Constants.MY_OFFERS) {
            myOffersNotificationFragment.callList();
        } else {
            receivedOfferFragment.callList();
        }
    }

    public void callList() {
        myOffersNotificationFragment.callList();
    }

    public void setListener(NotificationListener notificationListener) {
        this.notificationListener = notificationListener;
    }
}
