package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.NotificationSettingsAdapter;
import com.bynmix.app.databinding.NotificationSettingsFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.NotificationSettingListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.NotificationSettings;
import com.bynmix.app.models.Settings;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationSettingsFragment extends BaseFragment implements NotificationSettingListener {

    private NotificationSettingsFragmentBinding mBinding;
    private Context mContext;
    private ProfileListener mCallback;
    private List<NotificationSettings> mList = new ArrayList<>();
    private NotificationSettingsAdapter mAdapter;

    public static NotificationSettingsFragment newInstance() {

        return new NotificationSettingsFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (ProfileListener) context;
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = NotificationSettingsFragmentBinding.inflate(inflater, container, false);
        getNotificationSettings();
        setAdapter();
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
        return mBinding.getRoot();
    }

    private void setAdapter() {
        mAdapter = new NotificationSettingsAdapter(mContext, mList, this);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    public void updateList(List<NotificationSettings> mList) {
        this.mList = mList;
        mAdapter.setList(mList);
        mBinding.progressBar.setVisibility(View.GONE);
        mBinding.mainLayout.setVisibility(View.VISIBLE);

    }

    @Override
    public void updateNotificationSettings(final List<NotificationSettings> notificationSettings) {
        showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        Settings settings = new Settings();
        settings.setSettings(notificationSettings);
        apiInstance.updateNotificationSettings("Bearer " + getAccessToken(), settings).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    hideProgress();
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            updateNotificationSettings(notificationSettings);
                        }

                        @Override
                        public void errorAccessToken() {
                            hideProgress();
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    public void getNotificationSettings() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getNotificationSettings("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<List<NotificationSettings>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<NotificationSettings>>> call, Response<ApiResponse<List<NotificationSettings>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    hideProgress();
                    List<NotificationSettings> mList = response.body().getData();
                    updateList(mList);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getNotificationSettings();
                        }

                        @Override
                        public void errorAccessToken() {
                            hideProgress();
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<List<NotificationSettings>>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }
}
