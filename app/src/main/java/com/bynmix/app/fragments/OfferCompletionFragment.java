package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ahmadrosid.svgloader.SvgLoader;
import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.databinding.OfferCompletionFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.SelectExistingAddressListener;
import com.bynmix.app.interfaces.SelectNewPaymentListener;
import com.bynmix.app.models.Address;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.CardDetail;
import com.bynmix.app.models.ListingCheck;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.models.SubOfferHistory;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.sharedpreference.SharedPreferenceUtility;
import com.bynmix.app.utils.CongoDialog;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.DialogAlert;
import com.bynmix.app.utils.OfferSubmittedDialog;
import com.bynmix.app.utils.Utils;
import com.squareup.picasso.Picasso;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OfferCompletionFragment extends BaseFragment implements SelectNewPaymentListener, SelectExistingAddressListener {

    private OfferCompletionFragmentBinding mBinding;
    private Context mContext;
    private CreatePostListener mCallback;
    private ListingDetail listingDetail;
    private Address selectedAddress;
    private CardDetail selectedPayment;
    private SharedPreferenceUtility mSharedPreferenceUtility;
    private ListingCheck status = new ListingCheck();
    private int type;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (CreatePostListener) context;
    }

    public static OfferCompletionFragment newInstance() {
        return new OfferCompletionFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = OfferCompletionFragmentBinding.inflate(inflater, container, false);
        mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);
        setData();
        if (listingDetail.getUserId() == mSharedPreferenceUtility.getMyUserId()) {
            if (selectedAddress == null) {
                mCallback.getDefaultReturnAddress(null);
            } else {
                setDefaultValues();
            }
        } else {
            if (selectedPayment == null) {
                mCallback.getDefaultCard();
            } else {
                setDefaultValues();
            }
        }

        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        mBinding.changeAddress.editAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openSelectExistingAddressFragment(OfferCompletionFragment.this, null, false);
            }
        });

        mBinding.changePayment.editPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openSelectExistingPaymentFragment(OfferCompletionFragment.this, selectedPayment, false);
            }
        });

        mBinding.submitButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SubOfferHistory offerHistory = new SubOfferHistory();
                offerHistory.setOfferPrice(listingDetail.getOfferPrice());
                offerHistory.setAddressId(selectedAddress.getAddressId());
                offerHistory.setListingId(listingDetail.getId());
                offerHistory.setListingTitle(listingDetail.getTitle());
                offerHistory.setOfferId(listingDetail.getOfferId());
                offerHistory.setSellerUsername(listingDetail.getUsername());
                offerHistory.setBuyerUsername(listingDetail.getBuyerName());
                if (type == Constants.BUY) {
                    offerHistory.setCardId(selectedPayment.getCardId());
                    quickBuy(offerHistory);
                } else {
                    if (listingDetail.getUserId() == mSharedPreferenceUtility.getMyUserId()) {
                        counterOffer(offerHistory);
                    } else {
                        offerHistory.setCardId(selectedPayment.getCardId());
                        submitOffer(offerHistory);
                    }

                }
            }
        });
        return mBinding.getRoot();
    }

    private void quickBuy(final SubOfferHistory offerHistory) {
        showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.quickBuy("Bearer " + getAccessToken(), "" + offerHistory.getListingId(), offerHistory).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    CongoDialog congoDialog = new CongoDialog();
                    String message = "You successfully purchased " + "<b>" + "<font color='" + getResources().getColor(R.color.light_purple2) + "'>" + offerHistory.getListingTitle() + "</font>" + "</b> ";
                    congoDialog.showDialog(mContext, "Congratulations!", message);
                    mCallback.clearAllFragment();
                    mCallback.openPurchasesFragment(Constants.MY_PURCHASES);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            quickBuy(offerHistory);
                        }

                        @Override
                        public void errorAccessToken() {
                            hideProgress();
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext,response.errorBody());
                    DialogAlert.showCardNotAcceptedDialog(mContext, message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });

    }

    private void setData() {
        mBinding.title.setText(Html.fromHtml("\"" + "<b>" + "<font color='" + mContext.getResources().getColor(R.color.purple) + "'>" + listingDetail.getTitle() + "</font>" + "</b>" + "\""), TextView.BufferType.SPANNABLE);
        double price, totalPrice;
        if (type == Constants.BUY) {
            price = listingDetail.getPrice();
            mBinding.submit.setText("purchase");
            mBinding.headingText.setText("Buy it now");
            mBinding.toolbarHeading.setText("Buy Now");
            mBinding.yourOfferText.setText("Price: ");
        } else {
            if (mSharedPreferenceUtility.getMyUserId() == listingDetail.getUserId()) {
                mBinding.headingText.setText("Counter offer for:");
                mBinding.toolbarHeading.setText("Counter offer");
            } else {
                mBinding.headingText.setText("Make an offer for:");
                mBinding.toolbarHeading.setText("Make an offer");
            }
            price = listingDetail.getOfferPrice();
            mBinding.submit.setText("next");
            mBinding.yourOfferText.setText("Your offer: ");
        }


        double shippingPrice = listingDetail.getShippingInfo().getRate();
        if (listingDetail.getUserId() == mSharedPreferenceUtility.getMyUserId()) {
            totalPrice = (int) (price - shippingPrice);
            mBinding.shippingPriceText.setText("Minus Fees: ");
            mBinding.totalChargesText.setText("Total Profits (if accepted): ");
        } else {
            totalPrice = (int) (price + shippingPrice);
            mBinding.shippingPriceText.setText("Plus Shipping: ");
            if (type == Constants.BUY) {
                mBinding.totalChargesText.setText("Total Charge : ");
            } else {
                mBinding.totalChargesText.setText("Total Charge (if accepted): ");
            }

        }
        mBinding.price.setText(String.format("$%.2f", price));
        mBinding.shippingPrice.setText(String.format("$%.2f", shippingPrice));
        mBinding.totalPrice.setText(String.format("$%.2f", totalPrice));
        String image = listingDetail.getPhotoUrl();
        if (!TextUtils.isEmpty(image)) {
            Picasso.get().load(image).into(mBinding.image);
        } else {
            mBinding.image.setImageResource(R.mipmap.default_feed);
        }
    }

    public void update(CardDetail cardDetail, Address address) {
        this.selectedPayment = cardDetail;
        this.selectedAddress = address;
        setDefaultValues();
    }

    private void setDefaultValues() {
        mBinding.makeAnOfferParentLayout.setVisibility(View.VISIBLE);
        mBinding.submitButtonLayout.setVisibility(View.VISIBLE);
        mBinding.progressBar.setVisibility(View.GONE);
        if (selectedPayment != null) {
            mBinding.changePaymentLayout.setVisibility(View.VISIBLE);
            mBinding.changePayment.deletePayment.setVisibility(View.GONE);
            String photoUrl = selectedPayment.getBrandImageUrl();
            if (!TextUtils.isEmpty(photoUrl)) {
                SvgLoader.pluck()
                        .with((HomeActivity) mContext)
                        .load(photoUrl, mBinding.changePayment.cardImage);
            }
            String cardName = "<b>" + selectedPayment.getBrand() + "</b> " + " ending in " + "<b>" + selectedPayment.getLastFour() + "<b>";
            mBinding.changePayment.cardHeading.setText(Html.fromHtml(cardName), TextView.BufferType.SPANNABLE);
            mBinding.changePayment.name.setText(selectedPayment.getNameOnCard());
            mBinding.changePayment.expiryDate.setText("Expires " + selectedPayment.getExpirationMonth() + "/" + selectedPayment.getExpirationYear());
        } else {
            mBinding.changePaymentLayout.setVisibility(View.GONE);
            mBinding.paymentTextLayout.setVisibility(View.GONE);
        }

        if (listingDetail.getUserId() == mSharedPreferenceUtility.getMyUserId()) {
            mBinding.shippingAddress.setText("return address");
        } else {
            mBinding.shippingAddress.setText(R.string.shipping_text);
            if (selectedPayment == null && selectedAddress == null) {
                status.setAddressMissing(true);
                status.setPaymentMissing(true);
                status.setError(true);
                status.setErrorMessage("You cannot place a bid on a listing until you have added at least one payment option and a shipping address.");
                mCallback.openCheckListingDialog(status);
            } else if (selectedAddress == null) {
                status.setAddressMissing(true);
                status.setError(true);
                status.setErrorMessage("You cannot place a bid on a listing until you have added at least one shipping address.");
                mCallback.openCheckListingDialog(status);
            } else if (selectedPayment == null) {
                status.setPaymentMissing(true);
                status.setError(true);
                status.setErrorMessage("You cannot place a bid on a listing until you have added at least one payment option");
                mCallback.openCheckListingDialog(status);
            }


        }
        if (selectedAddress != null) {
            mBinding.addressLayout.setVisibility(View.VISIBLE);
            mBinding.changeAddress.deleteAddress.setVisibility(View.GONE);
            mBinding.changeAddress.fullName.setText(selectedAddress.getFullName());
            if (!TextUtils.isEmpty(selectedAddress.getAddressOne())) {
                mBinding.changeAddress.addressOne.setVisibility(View.VISIBLE);
                mBinding.changeAddress.addressOne.setText(selectedAddress.getAddressOne());
            } else {
                mBinding.changeAddress.addressOne.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(selectedAddress.getAddressTwo())) {
                mBinding.changeAddress.addressTwo.setVisibility(View.VISIBLE);
                mBinding.changeAddress.addressTwo.setText(selectedAddress.getAddressTwo());
            } else {
                mBinding.changeAddress.addressTwo.setVisibility(View.GONE);
            }
            mBinding.changeAddress.zipcode.setText(selectedAddress.getCity() + " " + selectedAddress.getStateAbbreviation() + ", " + selectedAddress.getZipCode());
        } else {
            mBinding.addressLayout.setVisibility(View.GONE);
        }
    }

    public void setListingDetail(ListingDetail listingDetail) {
        this.listingDetail = listingDetail;
    }

    @Override
    public void selectedAddress(Address address) {
        this.selectedAddress = address;
    }

    @Override
    public void selectedId(int id) {

    }

    @Override
    public void selectedPayment(CardDetail cardDetail) {
        this.selectedPayment = cardDetail;
    }

    public void error() {
        mBinding.progressBar.setVisibility(View.GONE);
        setDefaultValues();
    }

    public void setType(int type) {
        this.type = type;
    }


    public void submitOffer(final SubOfferHistory offerHistory) {
        showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        int listingId = offerHistory.getListingId();
        final OfferSubmittedDialog offerSubmittedDialog = new OfferSubmittedDialog();
        apiInstance.offerSubmit("Bearer " + getAccessToken(), "" + listingId, offerHistory).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    String username = Utils.getColorText(mContext, offerHistory.getSellerUsername());
                    String message = "Offer successfully sent to " + username + ". Hold tight to see what " + username + " thinks.";
                    offerSubmittedDialog.showDialog(mContext, 1, message);
                    mSharedPreferenceUtility.setIsOfferNotification(true);
                    mCallback.clearAllFragment();
                    mCallback.openNotificationFragment();
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            submitOffer(offerHistory);
                        }

                        @Override
                        public void errorAccessToken() {
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    offerSubmittedDialog.showDialog(mContext, 0, message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));

            }
        });
    }

    public void counterOffer(final SubOfferHistory offerHistory) {
        showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        int listingId = offerHistory.getListingId();
        final OfferSubmittedDialog offerSubmittedDialog = new OfferSubmittedDialog();
        apiInstance.counterOffer("Bearer " + getAccessToken(), "" + listingId, "" + offerHistory.getOfferId(), offerHistory).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    String username = Utils.getColorText(mContext, offerHistory.getBuyerUsername());
                    String message = "Offer successfully sent to " + username + ". Hold tight to see what " + username + " thinks.";
                    offerSubmittedDialog.showDialog(mContext, 1, message);
                    mCallback.clearAllFragment();
                    mCallback.openNotificationFragment();
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            counterOffer(offerHistory);
                        }

                        @Override
                        public void errorAccessToken() {

                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    offerSubmittedDialog.showDialog(mContext, 0, message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));

            }
        });
    }
}
