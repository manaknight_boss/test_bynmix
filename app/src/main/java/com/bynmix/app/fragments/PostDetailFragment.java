package com.bynmix.app.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.activities.InAppActivity;
import com.bynmix.app.databinding.FragmentPostDetailBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.DotsSingleTabInterface;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.sharedpreference.SharedPreferenceUtility;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostDetailFragment extends BaseFragment {

    private FragmentPostDetailBinding mBinding;
    private CreatePostListener mCallback;
    private Context mContext;
    private FeedResponse postDetail;
    private int postId;
    private SharedPreferenceUtility mSharedPreferenceUtility;

    public static PostDetailFragment newInstance() {
        return new PostDetailFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (CreatePostListener) context;
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentPostDetailBinding.inflate(inflater, container, false);
        mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);
        if (postDetail == null) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
            mBinding.mainLayout.setVisibility(View.GONE);
            getPostDetail(postId);
        } else {
            init();
        }

        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        return mBinding.getRoot();
    }

    private void init() {
        mBinding.progressBar.setVisibility(View.GONE);
        mBinding.mainLayout.setVisibility(View.VISIBLE);
        if (mSharedPreferenceUtility.getMyUserId() == postDetail.getUserId()) {
            mBinding.heart.setVisibility(View.GONE);
            mBinding.heartLarge.setVisibility(View.GONE);
        } else {
            mBinding.heart.setVisibility(View.VISIBLE);
            mBinding.heartLarge.setVisibility(View.VISIBLE);
        }
        updateLikeUI(postDetail.isUserLiked());
        mBinding.heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                likeClick();
            }
        });
        mBinding.heartLarge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                likeClick();
            }
        });
        mBinding.continueReadingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (postDetail.getLink() != null) {
                    Intent in = new Intent(mContext, InAppActivity.class);
                    in.putExtra(Constants.INTENT_EXTRA_URL, postDetail.getLink());
                    mContext.startActivity(in);
                }
//                    mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(postDetail.getLink())));
            }
        });
        String userPhoto = postDetail.getUserPhotoUrl();

        if (!TextUtils.isEmpty(userPhoto)) {
            Picasso.get().load(userPhoto).placeholder(R.mipmap.default_profile_image).into(mBinding.userPhoto);
            Picasso.get().load(userPhoto).placeholder(R.mipmap.default_profile_image).into(mBinding.postedByUserPhoto);
        } else {
            mBinding.userPhoto.setImageResource(R.mipmap.default_profile_image);
            mBinding.postedByUserPhoto.setImageResource(R.mipmap.default_profile_image);
        }
        mBinding.username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openProfile(postDetail.getUserId());
            }
        });
        mBinding.postedByUsername.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openProfile(postDetail.getUserId());
            }
        });

        mBinding.postedByUserPhotoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openProfile(postDetail.getUserId());
            }
        });
        mBinding.userPhotoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openProfile(postDetail.getUserId());
            }
        });

        mBinding.whiteHeart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openMyLikeFragment(postDetail.getUsername(),postDetail.getUserId(), Constants.MY_POST);
            }
        });

        mBinding.username.setText(postDetail.getUsername());
        mBinding.time.setText(postDetail.getCreatedDateFormatted());
        mBinding.title.setText(postDetail.getTitle());
        mBinding.postedByTime.setText(postDetail.getCreatedDateFormatted());
        mBinding.postedByUsername.setText(postDetail.getUsername());
        mBinding.shortDescription.setText(postDetail.getDescription() + "...");
        String photoUrl = postDetail.getPhotoUrl();
        if (!TextUtils.isEmpty(photoUrl)) {
            if (postDetail.getPostType() != null && postDetail.getPostType().equalsIgnoreCase(Constants.VIDEO_POST)) {
                mBinding.videoPlayIcon.setVisibility(View.VISIBLE);
                Picasso.get().load(photoUrl).placeholder(R.mipmap.novideo).into(mBinding.blogFullImage);
                Picasso.get().load(photoUrl).placeholder(R.mipmap.novideo).into(mBinding.dotsFullImage);
                mBinding.actionButton.setText("WATCH VIDEO");
            } else {
                mBinding.videoPlayIcon.setVisibility(View.GONE);
                Picasso.get().load(photoUrl).placeholder(R.mipmap.default_feed).into(mBinding.blogFullImage);
                Picasso.get().load(photoUrl).placeholder(R.mipmap.default_feed).into(mBinding.dotsFullImage);
                mBinding.actionButton.setText("CONTINUE READING");
            }
            if (postDetail.getPostDots().size() == 0) {

                mBinding.dotsFullImageParent.setVisibility(View.INVISIBLE);
                mBinding.blogFullImage.setVisibility(View.VISIBLE);
                if (postDetail.isHasPostDots()) {
                    mBinding.viewTagText.setText(mContext.getString(R.string.tap_to_view_tagged));
                    mBinding.viewTags.setVisibility(View.VISIBLE);
                } else {
                    mBinding.viewTags.setVisibility(View.GONE);
                }

            } else {
                mBinding.viewTags.setVisibility(View.VISIBLE);
                if (postDetail.isVisibleDots()) {
                    mBinding.viewTagText.setText(mContext.getString(R.string.hide_tagged_products));
                    mBinding.dotsFullImageParent.setVisibility(View.VISIBLE);
                    mBinding.dotsFullImage.setViewDraw(true);
                    mBinding.dotsFullImage.setPointList(postDetail.getPostDots());
                    mBinding.blogFullImage.setVisibility(View.INVISIBLE);
                } else {
                    mBinding.viewTagText.setText(mContext.getString(R.string.tap_to_view_tagged));
                    mBinding.dotsFullImage.setPointList(new ArrayList<AnnotatedPoint>());
                    mBinding.dotsFullImageParent.setVisibility(View.INVISIBLE);
                    mBinding.blogFullImage.setVisibility(View.VISIBLE);
                }
                mBinding.dotsFullImage.setDotsListener(new DotsSingleTabInterface() {
                    @Override
                    public void singleTab(AnnotatedPoint annotatedPoint) {
                        if (annotatedPoint.getPostDotTypeId() == Constants.DOT_TYPE_LISTING) {
                            mCallback.openListingDetailFragment(annotatedPoint.getListingId());
                        } else {
                            mCallback.openInAppBrowser(annotatedPoint.getLink(), annotatedPoint.getTitle());
                        }
                    }

                    @Override
                    public void singleTab() {
                        postClicked(postDetail);
                    }
                });

            }
        } else {
            if (postDetail.getPostType() != null && postDetail.getPostType().equalsIgnoreCase(Constants.VIDEO_POST)) {
                mBinding.blogFullImage.setImageResource(R.mipmap.novideo);
            } else {
                mBinding.blogFullImage.setImageResource(R.mipmap.default_feed);
            }
            mBinding.dotsFullImageParent.setVisibility(View.GONE);
        }


        mBinding.viewTags.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postDetail.setVisibleDots(!postDetail.isVisibleDots());
                mBinding.progressBar.setVisibility(View.VISIBLE);
                if (postDetail.getPostDots().size() == 0) {
                    mCallback.getPostTags(-2, postDetail.getId(), -2);
                } else {
                    init();
                }
            }
        });

        mBinding.videoPlayIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (postDetail.getLink() != null) {
                    Intent in = new Intent(mContext, InAppActivity.class);
                    in.putExtra(Constants.INTENT_EXTRA_URL, postDetail.getLink());
                    mContext.startActivity(in);
                }
            }
        });

    }

    private void likeClick() {
        postDetail.setUserLiked(!postDetail.isUserLiked());
        updateLikeUI(postDetail.isUserLiked());
        mCallback.onPostsLike(postDetail.getId());
        Intent intent = new Intent(Constants.ACTION_BROADCAST_RECEIVER_REFRESH_FEEDS);
        intent.putExtra(Constants.FEED, postDetail);
        mContext.sendBroadcast(intent);
    }

    private void updateLikeUI(boolean userLiked) {
        if (userLiked) {
            mBinding.heart.setImageResource(R.mipmap.heart);
            mBinding.heartLarge.setImageResource(R.mipmap.heart);
        } else {
            mBinding.heart.setImageResource(R.mipmap.heart_un_selected);
            mBinding.heartLarge.setImageResource(R.mipmap.heart_un_selected);
        }
    }

    private void postClicked(FeedResponse postDetail) {
        if (postDetail.getLink() != null)
            mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(postDetail.getLink())));
    }

    public void setPostDetail(FeedResponse details) {
        this.postDetail = details;
    }

    public void setPostDotsTags(List<AnnotatedPoint> mList) {
        postDetail.setPostDots(mList);
        init();
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public void setPostDetailData(FeedResponse data) {
        this.postDetail = data;
        init();
    }

    public void getPostDetail(final int postId) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getPostDetail("Bearer " + getAccessToken(), "" + postId).enqueue(new Callback<ApiResponse<FeedResponse>>() {
            @Override
            public void onResponse(Call<ApiResponse<FeedResponse>> call, Response<ApiResponse<FeedResponse>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    FeedResponse data = response.body().getData();
                    setPostDetailData(data);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getPostDetail(postId);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<FeedResponse>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }
}