package com.bynmix.app.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.text.Html;
import android.text.Spannable;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bynmix.app.R;

import com.bynmix.app.activities.WebViewActivity;
import com.bynmix.app.adapter.FeedViewPagerAdapter;
import com.bynmix.app.adapter.ProfileIconAdapter;
import com.bynmix.app.databinding.ProfileFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.models.UserResponse;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.sharedpreference.SharedPreferenceUtility;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends BaseFragment {

    private ProfileFragmentBinding mBinding;
    private Context mContext;
    private ProfileListener mCallback;
    private UserResponse user;
    private int userId;
    private List<Integer> profileIcons = new ArrayList<>();
    private SharedPreferenceUtility mSharedPreferenceUtility;
    private FeedViewPagerAdapter feedViewPagerAdapter;
    private MyBynFragment myBynFragment;
    private MyPostFragment myPostFragment;
    private ProfileIconAdapter adapter;

    public static ProfileFragment newInstance() {
        return new ProfileFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (ProfileListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = ProfileFragmentBinding.inflate(inflater, container, false);
        mSharedPreferenceUtility = new SharedPreferenceUtility(mContext);
        setViewPager();
        setUserId();
        setCallBack();
        adapter = new ProfileIconAdapter(mContext, profileIcons, mCallback);
        mBinding.profileRecyclerView.setLayoutManager(new GridLayoutManager(mContext, 3));
        mBinding.profileRecyclerView.setAdapter(adapter);
        if (profileIcons.size() == 0) {
            addProfileIcons();
        }
        mBinding.heartIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openMyLikeFragment(user.getUsername(), mSharedPreferenceUtility.getMyUserId(), Constants.MY_BYN);
            }
        });
        getUserProfile();
        mBinding.filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openFilterFragment(Constants.PROFILE_FILTER);
            }
        });
        return mBinding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshProfile();
    }

    private void refreshProfile() {
        getProfile();
    }

    private void setCallBack() {
        myPostFragment.setCallBack(mCallback);
        myBynFragment.setCallBack(mCallback);
    }

    private void setUserId() {
        myBynFragment.setUserId(userId);
        myBynFragment.isCallApi(true);
        myPostFragment.setUserId(userId);
    }

    private void setViewPager() {
        if (feedViewPagerAdapter == null) {
            feedViewPagerAdapter = new FeedViewPagerAdapter(getChildFragmentManager());
            myBynFragment = new MyBynFragment();
            myPostFragment = new MyPostFragment();
            feedViewPagerAdapter.addFragment(myBynFragment, getResources().getString(R.string.my_byn));
            feedViewPagerAdapter.addFragment(myPostFragment, getResources().getString(R.string.my_post));
        }
        mBinding.viewpager.setAdapter(feedViewPagerAdapter);
        mBinding.viewpager.setOffscreenPageLimit(2);
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);

        mBinding.viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    myBynFragment.callList();
                    mBinding.filter.setVisibility(View.VISIBLE);
                } else {
                    myPostFragment.callList();
                    mBinding.filter.setVisibility(View.GONE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void addProfileIcons() {
        profileIcons.add(R.mipmap.edit_account);
        profileIcons.add(R.mipmap.my_byn);
        profileIcons.add(R.mipmap.posts);
        profileIcons.add(R.mipmap.likes);
        profileIcons.add(R.mipmap.purchases);
        profileIcons.add(R.mipmap.sales);
        profileIcons.add(R.mipmap.my_wallet);
        profileIcons.add(R.mipmap.my_addresses);
        profileIcons.add(R.mipmap.invite);
        profileIcons.add(R.mipmap.notifications);
        profileIcons.add(R.mipmap.redeemed);
        profileIcons.add(R.mipmap.redeem);
        profileIcons.add(R.mipmap.logout);
    }

    private void getUserProfile() {
        if (user == null) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
            getProfile();
        } else {
            setProfileData(user);
        }
    }

    public void getProfile() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        if (userId == mSharedPreferenceUtility.getMyUserId()) {
            apiInstance.getMyProfile("Bearer " + getAccessToken()).enqueue(new retrofit2.Callback<ApiResponse<UserResponse>>() {
                @Override
                public void onResponse(Call<ApiResponse<UserResponse>> call, Response<ApiResponse<UserResponse>> response) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    profileResponse(response);
                }

                @Override
                public void onFailure(Call<ApiResponse<UserResponse>> call, Throwable t) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    showAlertDialog(getResources().getString(R.string.error_server));
                }
            });
        } else {
            apiInstance.getProfile("Bearer " + getAccessToken(), "" + userId).enqueue(new Callback<ApiResponse<UserResponse>>() {
                @Override
                public void onResponse(Call<ApiResponse<UserResponse>> call, Response<ApiResponse<UserResponse>> response) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    profileResponse(response);
                }

                @Override
                public void onFailure(Call<ApiResponse<UserResponse>> call, Throwable t) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    showAlertDialog(getResources().getString(R.string.error_server));
                }
            });
        }

    }


    private void profileResponse(Response<ApiResponse<UserResponse>> response) {
        if (response.isSuccessful() && response.code() == 200 && response.body() != null) {
            UserResponse userResponse = response.body().getData();
            if (userResponse != null) {
                setProfileData(userResponse);
            } else {
                Toast.makeText(mContext, "Something went wrong", Toast.LENGTH_SHORT).show();
            }

        } else if (response.code() == 401) {
            getAccessTokenFromServer(new AccessTokenResponse() {
                @Override
                public void successAccessToken() {
                    getProfile();
                }

                @Override
                public void errorAccessToken() {
                    mBinding.progressBar.setVisibility(View.GONE);
                }
            });
        }
    }


    private void drawStar(int rating) {
        mBinding.starLayout.removeAllViews();
        for (int i = 1; i <= 5; i++) {
            ImageView starImage = new ImageView(mContext);
            if (i <= rating) {
                starImage.setImageResource(R.mipmap.star);
            }
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            int margin = Utils.dpToPx(mContext, 2);
            params.setMargins(margin, 0, margin, 0);
            mBinding.starLayout.addView(starImage);
        }
    }

    public void setProfileData(final UserResponse userResponse) {
        mBinding.progressBar.setVisibility(View.GONE);
        this.user = userResponse;
        setUsername(user.getUsername());
        updateUI();
        setSocialLinks(user);
        setLocation(user);
        adapter.setUser(user);

        if (user.getRating() == 0) {
            mBinding.starLayout.setVisibility(View.GONE);
            mBinding.viewReviews.setVisibility(View.GONE);
        } else {
            mBinding.starLayout.setVisibility(View.VISIBLE);
            mBinding.viewReviews.setVisibility(View.VISIBLE);
            drawStar((int) Math.floor(user.getRating()));
        }

        if (!TextUtils.isEmpty(user.getBlogLink())) {
            mBinding.website.setVisibility(View.VISIBLE);
        } else {
            mBinding.website.setVisibility(View.GONE);
        }

        mBinding.website.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, WebViewActivity.class);
                intent.putExtra(Constants.WEBSITE_LINK, user.getBlogLink());
                startActivity(intent);
            }
        });

        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        mBinding.addPeople.setOnClickListener(v -> mCallback.openSearchUserFragment());

        updateFollowFollowingUI(user.isLoggedInUserFollowing());
        updateFollowFollowing(user);
        mBinding.followFollowing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user.setLoggedInUserFollowing(!user.isLoggedInUserFollowing());
                int followCount;
                if (user.isLoggedInUserFollowing()) {
                    followCount = user.getFollowersCount() + 1;
                } else {
                    if (user.getFollowersCount() > 0) {
                        followCount = user.getFollowersCount() - 1;
                    } else {
                        followCount = 0;
                    }
                }
                user.setFollowersCount(followCount);
                updateFollowFollowingUI(user.isLoggedInUserFollowing());
                updateFollowFollowing(user);
                mCallback.onFollowerUser(user.getUserId());
            }
        });

        if (!TextUtils.isEmpty(user.getUsername())) {
            mBinding.userName.setVisibility(View.VISIBLE);
        } else {
            mBinding.userName.setVisibility(View.GONE);
        }

        mBinding.userName.setText(user.getUsername());

        if (user.getTitle() != null) {
            mBinding.title.setText(user.getTitle());
            mBinding.title.setVisibility(View.VISIBLE);
        } else {
            mBinding.title.setVisibility(View.GONE);
        }

        String shortBio = user.getShortBio();

        if (shortBio != null) {
            shortBio = shortBio.trim();
            if (TextUtils.isEmpty(shortBio)) {
                mBinding.shortBio.setVisibility(View.GONE);
            } else {
                mBinding.shortBio.setVisibility(View.VISIBLE);
                mBinding.shortBio.setMovementMethod(LinkMovementMethod.getInstance());

                if (shortBio.length() < Constants.SHORT_BIO_LENGTH) {
                    mBinding.shortBio.setText(shortBio);
                } else {
                    setShortBio(shortBio, "Read More");
                }
            }
        } else {
            mBinding.shortBio.setVisibility(View.GONE);
        }

        String userPhoto = user.getPhotoUrl();

        if (!TextUtils.isEmpty(userPhoto)) {
            Picasso.get().load(userPhoto).placeholder(R.mipmap.default_profile_image).into(mBinding.profileImage);
        } else {
            mBinding.profileImage.setImageResource(R.mipmap.default_profile_image);
        }

        String backgroungPhoto = user.getBackgroundPhoto();

        if (!TextUtils.isEmpty(backgroungPhoto)) {
            Picasso.get().load(backgroungPhoto).into(mBinding.profileBackgroundImage);
        } else {
            mBinding.profileBackgroundImage.setImageResource(R.mipmap.default_profile_image);
        }

        mBinding.website.setPaintFlags(mBinding.website.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        mBinding.editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openEditProfileFragment(user);
            }
        });

        mBinding.editBackgroundPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openGalleryFor(Constants.FOR_BACKGROUND_PIC, 1);
            }
        });

        mBinding.editProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openGalleryFor(Constants.FOR_PROFILE_PIC, 1);
            }
        });

        mBinding.followerLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user.getFollowersCount() > 0) {
                    mCallback.openFollowFollowingFragment(Constants.FOLLOWER, user.getUserId(), user.getUsername());
                }
            }
        });

        mBinding.followingLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user.getFollowingCount() > 0) {
                    mCallback.openFollowFollowingFragment(Constants.FOLLOWING, user.getUserId(), user.getUsername());
                }
            }
        });

        mBinding.viewReviews.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openReviewFragment(user.getUserId(), user.getUsername());
            }
        });
    }

    private void setUsername(String username) {
        myBynFragment.setUsername(username);
        myPostFragment.setUsername(username);

    }

    private void setLocation(UserResponse user) {
        String city, state;
        city = user.getCity();
        state = user.getStateAbbreviated();
        if (state != null) {
            mBinding.locationLayout.setVisibility(View.VISIBLE);
            if (city != null) {
                mBinding.location.setText(String.format("%s, %s", city, state));
            } else {
                mBinding.location.setText(state);
            }
        } else if (city != null) {
            mBinding.location.setText(city);
            mBinding.locationLayout.setVisibility(View.VISIBLE);
        } else {
            mBinding.locationLayout.setVisibility(View.GONE);
        }
    }

    private void updateFollowFollowing(UserResponse user) {
        if (user.getFollowersCount() >= 0) {
            mBinding.followerCount.setText("" + user.getFollowersCount());
        } else {
            mBinding.followerCount.setText("" + 0);
        }

        if (user.getFollowingCount() >= 0) {
            mBinding.followingCount.setText("" + user.getFollowingCount());
        } else {
            mBinding.followingCount.setText("" + 0);
        }
    }

    private void updateFollowFollowingUI(boolean loggedInUserFollowing) {
        if (loggedInUserFollowing) {
            mBinding.followFollowing.setImageResource(R.mipmap.following);
        } else {
            mBinding.followFollowing.setImageResource(R.mipmap.follow);
        }
    }

    private void setSocialLinks(UserResponse user) {
        final String fbLink, intsaLink, twitterLink, pinterestLink, googlePlusLink;
        fbLink = user.getFacebookLink();
        intsaLink = user.getInstagramLink();
        twitterLink = user.getTwitterLink();
        pinterestLink = user.getPinterestLink();
        googlePlusLink = user.getGooglePlusLink();
        mBinding.facebook.setVisibility(View.GONE);
        mBinding.instagram.setVisibility(View.GONE);
        mBinding.twitter.setVisibility(View.GONE);
        mBinding.googlePlus.setVisibility(View.GONE);
        mBinding.pinterest.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(fbLink)) {
            mBinding.facebook.setVisibility(View.VISIBLE);
            mBinding.facebook.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(fbLink.trim())));
                }
            });
        } else {
            mBinding.facebook.setVisibility(View.GONE);
        }
        if (!TextUtils.isEmpty(intsaLink)) {
            mBinding.instagram.setVisibility(View.VISIBLE);
            mBinding.instagram.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(intsaLink.trim())));
                }
            });
        } else {
            mBinding.instagram.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(twitterLink)) {
            mBinding.twitter.setVisibility(View.VISIBLE);
            mBinding.twitter.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(twitterLink.trim())));
                }
            });
        } else {
            mBinding.twitter.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(googlePlusLink)) {
            mBinding.googlePlus.setVisibility(View.VISIBLE);
            mBinding.googlePlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(googlePlusLink.trim())));
                }
            });
        } else {
            mBinding.googlePlus.setVisibility(View.GONE);
        }

        if (!TextUtils.isEmpty(pinterestLink)) {
            mBinding.pinterest.setVisibility(View.VISIBLE);
            mBinding.pinterest.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(pinterestLink.trim())));
                }
            });
        } else {
            mBinding.pinterest.setVisibility(View.GONE);
        }
    }

    private void setShortBio(final String shortBio, final String s) {
        String bioSubString = shortBio;

        if (s.equalsIgnoreCase("Read More")) {
            if (bioSubString.length() > Constants.SHORT_BIO_LENGTH) {
                bioSubString = bioSubString.substring(0, Constants.SHORT_BIO_LENGTH);
                bioSubString += "...";

            }
        }

        bioSubString.replace(" ", "&nbsp;");
        bioSubString.replace("\n", "<br>");
        bioSubString.replace("\r", "<br>");
        bioSubString.replace("\n\r", "<br>");

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            mBinding.shortBio.setText(Html.fromHtml(bioSubString + " <font color='#32afff'>" + s + "</font>", Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
        } else {
            mBinding.shortBio.setText(Html.fromHtml(bioSubString + " <font color='#32afff'>" + s + "</font>"), TextView.BufferType.SPANNABLE);
        }

        Spannable mySpannableBioSubString = (Spannable) mBinding.shortBio.getText();
        ClickableSpan myClickableBioSubString = new ClickableSpan() {
            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);    // this remove the underline
            }

            @Override
            public void onClick(View widget) {
                if (s.equalsIgnoreCase("Read More")) {
                    setShortBio(shortBio, "Read Less");
                } else {
                    setShortBio(shortBio, "Read More");
                }
            }
        };

        Log.d("MSG", "" + mBinding.shortBio.getText().length());
        try {
            String data = mBinding.shortBio.getText().toString();
            mySpannableBioSubString.setSpan(myClickableBioSubString, data.length() - s.length(), data.length() - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateUI() {
        mBinding.mainLayout.setVisibility(View.VISIBLE);

        if (mSharedPreferenceUtility.getMyUserId() == userId) {
            mBinding.tabs.setVisibility(View.GONE);
            mBinding.viewPagerLayout.setVisibility(View.GONE);
            mBinding.profileRecyclerView.setVisibility(View.VISIBLE);
            mBinding.editProfileImage.setVisibility(View.VISIBLE);
            mBinding.editProfile.setVisibility(View.VISIBLE);
            mBinding.editBackgroundPhoto.setVisibility(View.VISIBLE);
            mBinding.heartIcon.setVisibility(View.VISIBLE);
            mBinding.filter.setVisibility(View.GONE);
            mBinding.followFollowing.setVisibility(View.GONE);
            mBinding.heading.setText("My Profile");
        } else {
            mBinding.heading.setText("Profile");
            mBinding.tabs.setVisibility(View.VISIBLE);
            mBinding.viewPagerLayout.setVisibility(View.VISIBLE);
            mBinding.profileRecyclerView.setVisibility(View.GONE);
            mBinding.editProfileImage.setVisibility(View.GONE);
            mBinding.editProfile.setVisibility(View.GONE);
            mBinding.editBackgroundPhoto.setVisibility(View.GONE);
            mBinding.heartIcon.setVisibility(View.GONE);
            mBinding.filter.setVisibility(View.VISIBLE);
            mBinding.followFollowing.setVisibility(View.VISIBLE);
        }
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void updateMyBynFeeds(List<FeedResponse> myBynFeeds, int currentPageNumber, int totalPage) {
        myBynFragment.setList(myBynFeeds, currentPageNumber, totalPage);
    }

    public void updateMyPostFeeds(List<FeedResponse> myBynFeeds, int currentpageNumber, int totalPages) {
        myPostFragment.setList(myBynFeeds, currentpageNumber, totalPages);
    }

    public void onItemFound(int postType) {
        if (postType == Constants.MY_POST) {
            myPostFragment.setError(404);
        } else {
            myBynFragment.setError(404);
        }
    }

    public void error(int postType) {
        if (postType == Constants.MY_POST) {
            myPostFragment.setError(400);
        } else {
            myBynFragment.setError(400);
        }
    }

    public void updateBackgroundPhoto(String selectedImagePath) {
        try {
            user.setBackgroundPhoto(selectedImagePath);
            Picasso.get().load(selectedImagePath).placeholder(R.mipmap.pattern2).into(mBinding.profileBackgroundImage);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateProfilePhoto(String selectedImagePath) {
        try {
            user.setPhotoUrl(selectedImagePath);
            Picasso.get().load(selectedImagePath).placeholder(R.mipmap.default_profile_image).into(mBinding.profileImage);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void clearBynList() {
        myBynFragment.clearList();
    }

    public void setPostDotsTags(List<AnnotatedPoint> mList, int position) {
        myPostFragment.setPostDotsTags(mList, position);
    }
}
