package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ahmadrosid.svgloader.SvgLoader;
import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.databinding.PurchaseDetailBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.PurchasesDetail;
import com.bynmix.app.models.SalesDetail;
import com.bynmix.app.models.Stock;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.PrintLabelDialog;
import com.bynmix.app.utils.PurchaseFeedbackDialog;
import com.bynmix.app.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PurchaseDetailFragment extends BaseFragment {
    private PurchaseDetailBinding mBinding;
    private ProfileListener mCallback;
    private Context mContext;
    private int id;
    private Stock detail;
    private int type;
    private boolean isEnabled = false;
    public static boolean isReloadRequired = false;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (ProfileListener) context;
        mContext = context;
    }

    public static PurchaseDetailFragment newInstance() {
        return new PurchaseDetailFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = PurchaseDetailBinding.inflate(inflater, container, false);
        if (type == Constants.MY_SALES) {
            mBinding.toolbarHeading.setText("Sales Detail");
            mBinding.detailTypeText.setText("Sale Details");
            mBinding.discountText.setText("Bynmix Commission");
            mBinding.purchaseByTextTitle.setText("Purchased by:");
        } else {
            mBinding.toolbarHeading.setText("Purchase Detail");
            mBinding.detailTypeText.setText("Purchase Details");
            mBinding.discountText.setText("Discount / Coupon");
            mBinding.purchaseByTextTitle.setText("Sold by:");
        }
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
        if(isReloadRequired) {
            isReloadRequired = false;
            detail = null;
        }
        if (detail == null) {
            getDetail();
        } else {
            if (isAdded()){
                setData();
            }

        }
        return mBinding.getRoot();
    }

    private void setData() {
        int statusId = 0;
        isEnabled = false;
        mBinding.progressBar.setVisibility(View.GONE);
        mBinding.mainLayout.setVisibility(View.VISIBLE);
        String photoUrl = detail.getListingImageUrl();
        if (!TextUtils.isEmpty(photoUrl)) {
            Picasso.get().load(photoUrl).placeholder(R.mipmap.default_feed).into(mBinding.listingImage);
        } else {
            mBinding.listingImage.setImageResource(R.mipmap.default_feed);
        }

        if (detail.getPointsStatusId() > 0) {
            mBinding.pointsEarned.setVisibility(View.VISIBLE);
            if (detail.getPointsStatusId() == 1) {
                mBinding.pointsEarned.setTextColor(mContext.getResources().getColor(R.color.blue));
                mBinding.pointsEarned.setText("Points Earned: " + detail.getPoints() + " Points (Pending Feedback left)");
            } else if (detail.getPointsStatusId() == 2) {
                mBinding.pointsEarned.setTextColor(mContext.getResources().getColor(R.color.green));
                mBinding.pointsEarned.setText("Points Earned: " + detail.getPoints() + " Points");
            } else if (detail.getPointsStatusId() == 3) {
                mBinding.pointsEarned.setTextColor(mContext.getResources().getColor(R.color.black));
                mBinding.pointsEarned.setText("Points Retracted: " + detail.getPoints() + " Points");
            } else if (detail.getPointsStatusId() == 4) {
                mBinding.pointsEarned.setTextColor(mContext.getResources().getColor(R.color.black));
                mBinding.pointsEarned.setText("Points Pending: " + detail.getPoints() + " Points (Pending Dispute)");
            } else {
                mBinding.pointsEarned.setVisibility(View.GONE);
            }
        } else {
            mBinding.pointsEarned.setVisibility(View.GONE);
        }
        mBinding.postTitle.setText(detail.getListingTitle());
        if (detail instanceof PurchasesDetail) {
            PurchasesDetail purchasesDetail = (PurchasesDetail) detail;
            statusId = purchasesDetail.getPurchaseStatusId();
            mBinding.username.setText(detail.getBuyerAndSellerInfo().getSellerUsername());
            String sellerUserPhotoUrl = detail.getBuyerAndSellerInfo().getSellerImageUrl();
            if (!TextUtils.isEmpty(sellerUserPhotoUrl)) {
                Picasso.get().load(sellerUserPhotoUrl).placeholder(R.mipmap.default_feed).into(mBinding.userPhoto);
            } else {
                mBinding.userPhoto.setImageResource(R.mipmap.default_feed);
            }
            mBinding.date.setText(purchasesDetail.getPurchaseDateFormatted());
            mBinding.totalPrice.setText("$" + Utils.formatValueToTwoDecimalPlaces(purchasesDetail.getPurchaseTotalPrice()));
            mBinding.shippingPrice.setText("$" + Utils.formatValueToTwoDecimalPlaces(purchasesDetail.getPurchaseShippingPrice()));
            mBinding.originalPrice.setText("$" + Utils.formatValueToTwoDecimalPlaces(purchasesDetail.getPurchaseOriginalBeforeDiscountPrice()));
            mBinding.paidPrice.setText("$" + Utils.formatValueToTwoDecimalPlaces(purchasesDetail.getPurchaseOriginalAfterDiscountPrice()));
            mBinding.paidShippingPrice.setText("$" + Utils.formatValueToTwoDecimalPlaces(purchasesDetail.getPurchaseShippingPrice()));
            mBinding.discount.setText("$" + Utils.formatValueToTwoDecimalPlaces(purchasesDetail.getDiscount()));
            mBinding.totalPaidPrice.setText("$" + Utils.formatValueToTwoDecimalPlaces(purchasesDetail.getPurchaseTotalPrice()));
            if (purchasesDetail.getPaidWith() != null) {
                mBinding.paidWithLayout.setVisibility(View.VISIBLE);
                String cardImage = purchasesDetail.getPaidWith().getBrandImageUrl();
                if (!TextUtils.isEmpty(cardImage)) {
                    SvgLoader.pluck()
                            .with((HomeActivity) mContext)
                            .load(cardImage, mBinding.cardImage);
                }

                mBinding.brand.setText(purchasesDetail.getPaidWith().getBrand());
                mBinding.lastFour.setText("" + purchasesDetail.getPaidWith().getLastFour());
            } else {
                mBinding.paidWithLayout.setVisibility(View.GONE);
            }
            updateFollowFollowingUI(detail.getBuyerAndSellerInfo().isLoggedInUserFollowingSeller());

            if (purchasesDetail.isCanCancelPurchase()) {
                mBinding.cancelLayout.setVisibility(View.VISIBLE);
                mBinding.leaveFeedbackMainLayout.setVisibility(View.GONE);
            } else {
                mBinding.cancelLayout.setVisibility(View.GONE);
                mBinding.leaveFeedbackMainLayout.setVisibility(View.VISIBLE);
            }


        } else {
            SalesDetail salesDetail = (SalesDetail) detail;
            statusId = salesDetail.getSaleStatusId();
            mBinding.username.setText(detail.getBuyerAndSellerInfo().getBuyerUsername());
            String buyerUserPhotoUrl = detail.getBuyerAndSellerInfo().getBuyerImageUrl();
            if (!TextUtils.isEmpty(buyerUserPhotoUrl)) {
                Picasso.get().load(buyerUserPhotoUrl).placeholder(R.mipmap.default_feed).into(mBinding.userPhoto);
            } else {
                mBinding.userPhoto.setImageResource(R.mipmap.default_feed);
            }
            mBinding.date.setText(salesDetail.getSaleDateFormatted());
            mBinding.totalPrice.setText("$" + Utils.formatValueToTwoDecimalPlaces(salesDetail.getSaleTotalPrice()));
            mBinding.shippingPrice.setText("$" + Utils.formatValueToTwoDecimalPlaces(salesDetail.getSaleShippingPrice()));
            mBinding.originalPrice.setText("$" + Utils.formatValueToTwoDecimalPlaces(salesDetail.getSaleOriginalPrice()));
            mBinding.paidWithLayout.setVisibility(View.GONE);
            mBinding.paidPrice.setText("$" + Utils.formatValueToTwoDecimalPlaces(salesDetail.getSaleOriginalPrice()));
            mBinding.paidShippingPrice.setText("$" + Utils.formatValueToTwoDecimalPlaces(salesDetail.getSaleShippingPrice()));
            if (salesDetail.getSaleBynmixCommission() > 0) {
                mBinding.discount.setText("-$" + Utils.formatValueToTwoDecimalPlaces(salesDetail.getSaleBynmixCommission()));
            } else {
                mBinding.discount.setText("-");
            }
            mBinding.totalPaidPrice.setText("$" + Utils.formatValueToTwoDecimalPlaces(salesDetail.getSaleProfit()));
            updateFollowFollowingUI(detail.getBuyerAndSellerInfo().isLoggedInUserFollowingBuyer());

            if (salesDetail.isCanCancelSale()) {
                mBinding.cancelLayout.setVisibility(View.VISIBLE);
                mBinding.leaveFeedbackMainLayout.setVisibility(View.GONE);
            } else {
                mBinding.cancelLayout.setVisibility(View.GONE);
                mBinding.leaveFeedbackMainLayout.setVisibility(View.VISIBLE);
            }
        }
        mBinding.name.setText(detail.getShippedTo().getFullName());
        String addressOne = detail.getShippedTo().getAddressOne();
        String addressTwo = detail.getShippedTo().getAddressTwo();
        if (TextUtils.isEmpty(addressTwo)) {
            mBinding.address.setText(addressOne);
        } else {
            mBinding.address.setText(addressOne + " " + addressTwo);
        }
        if (detail.getShippedTo().isDefaultAddress()) {
            mBinding.defaultAddress.setVisibility(View.VISIBLE);
        } else {
            mBinding.defaultAddress.setVisibility(View.GONE);
        }
        mBinding.state.setText(detail.getShippedTo().getCity() + " " + detail.getShippedTo().getState() + " " + detail.getShippedTo().getZipCode());
        mBinding.leaveFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PurchaseFeedbackDialog purchaseFeedbackDialog = new PurchaseFeedbackDialog();
                purchaseFeedbackDialog.showDialog(mContext, mCallback, detail, 0);
            }
        });
        if (detail.getRating() > 0) {
            mBinding.ratingLayout.setVisibility(View.VISIBLE);
            mBinding.leaveFeedback.setVisibility(View.GONE);
            drawStar(detail);
        } else {
            mBinding.ratingLayout.setVisibility(View.GONE);
            mBinding.leaveFeedback.setVisibility(View.VISIBLE);
        }

        mBinding.cancelLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openCancelSaleFragment(detail);
            }
        });

        String status = detail.getShippedTo().getShippingStatus();
        String statusDetail = null;
        if (!TextUtils.isEmpty(status)) {
            mBinding.statusLayout.setVisibility(View.VISIBLE);
            mBinding.status.setText(status);
            if (status.equalsIgnoreCase("delivered")) {
                isEnabled = true;
                statusDetail = "on " + detail.getShippedTo().getDeliveryDateFormatted();
            } else {
                statusDetail = detail.getShippedTo().getShippingStatusDetails();
                isEnabled = false;
            }
            if (!TextUtils.isEmpty(statusDetail)) {
                mBinding.deliveredDate.setVisibility(View.VISIBLE);
                mBinding.deliveredDate.setText(statusDetail);
            } else {
                mBinding.deliveredDate.setVisibility(View.GONE);
            }
        } else {
            mBinding.statusLayout.setVisibility(View.GONE);
            isEnabled = false;
        }

        if(detail.canPrintShippingLabel()) {
            mBinding.actionButtonText.setText(getString(R.string.print_shipping_label));
            mBinding.statusLayout.setVisibility(View.VISIBLE);
            mBinding.deliveredDate.setVisibility(View.GONE);
            mBinding.status.setText(getString(R.string.not_yet_shipped));
        } else {
            mBinding.actionButtonText.setText(getString(R.string.view_shipping_history));
        }

        mBinding.actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(detail.canPrintShippingLabel()) {
                    PrintLabelDialog.showDialog(mContext, detail, new DialogClickListener() {
                        @Override
                        public void onClick() {
                            if (mCallback.conditionsForApiCall()) {
                                SalesDetail salesDetail = (SalesDetail) detail;
                                ((HomeActivity)mContext).emailShippingLabel(salesDetail.getSaleId(), salesDetail.getListingTitle());
                            }
                        }
                    });
                } else {
                    mCallback.getTrackInformation(detail);
                }
            }
        });

        mBinding.promotion1.setVisibility(View.GONE);
        mBinding.promotion2.setVisibility(View.GONE);
        if(detail.getPromotionsFormatted() != null && detail.getPromotionsFormatted().size() > 0) {
            List<String> promotion = detail.getPromotionsFormatted();
            for (int i = 0; i < promotion.size(); i++) {
                if(detail.getPromotionsFormatted().get(i).toLowerCase().contains(new String("Promotion Price").toLowerCase())) {
                    mBinding.promotion1.setVisibility(View.VISIBLE);
                    mBinding.promotion1.setText(promotion.get(i));
                } else {
                    mBinding.promotion2.setVisibility(View.VISIBLE);
                    mBinding.promotion2.setText(promotion.get(i));
                }
            }
        }

        enableFeedback(isEnabled);

        if (detail.isHasOpenDispute()) {
            mBinding.reportText.setText(mContext.getString(R.string.view_dispute_details));
        } else {
            mBinding.reportText.setText(mContext.getString(R.string.report_an_issue));
            enableDisableDispute(detail.isCanOpenDispute());
        }

        mBinding.reportAnIssueLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detail instanceof PurchasesDetail) {
                    if (detail.isHasOpenDispute()) {
                        mCallback.openDisputeDetailFragment(detail.getDisputeId(), detail);
                    } else {
                        mCallback.checkReportAnIssue(detail);
                    }
                } else {
                    if (detail.isHasOpenDispute()) {
                        mCallback.openDisputeDetailFragment(detail.getDisputeId(), detail);
                    } else {
                        mCallback.checkReportAnIssue(detail);
                    }
                }
            }
        });

        mBinding.heart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detail instanceof PurchasesDetail) {
                    detail.getBuyerAndSellerInfo().setLoggedInUserFollowingSeller(!detail.getBuyerAndSellerInfo().isLoggedInUserFollowingSeller());
                    updateFollowFollowingUI(detail.getBuyerAndSellerInfo().isLoggedInUserFollowingSeller());
                    mCallback.onFollowerUser(detail.getBuyerAndSellerInfo().getSellerId());
                } else {
                    detail.getBuyerAndSellerInfo().setLoggedInUserFollowingBuyer(!detail.getBuyerAndSellerInfo().isLoggedInUserFollowingBuyer());
                    updateFollowFollowingUI(detail.getBuyerAndSellerInfo().isLoggedInUserFollowingBuyer());
                    mCallback.onFollowerUser(detail.getBuyerAndSellerInfo().getBuyerId());
                }

            }
        });

        mBinding.username.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (detail instanceof PurchasesDetail) {
                    mCallback.openProfile(detail.getBuyerAndSellerInfo().getSellerId());
                } else {
                    mCallback.openProfile(detail.getBuyerAndSellerInfo().getBuyerId());
                }
            }
        });
    }

    private void enableDisableDispute(boolean isEnabled) {
        if (isEnabled) {
            mBinding.reportAnIssueLayout.setBackgroundColor(mContext.getResources().getColor(R.color.black));
            mBinding.reportAnIssueLayout.setEnabled(true);
        } else {
            mBinding.reportAnIssueLayout.setEnabled(false);
            mBinding.reportAnIssueLayout.setBackgroundColor(mContext.getResources().getColor(R.color.disable_dialog));
        }
    }

    private void updateFollowFollowingUI(boolean isFollowing) {
        if (isFollowing) {
            mBinding.heart.setImageResource(R.mipmap.heart);
        } else {
            mBinding.heart.setImageResource(R.mipmap.heart_un_selected);
        }

    }

    private void drawStar(Stock detail) {
        mBinding.starLayout.removeAllViews();
        for (int i = 1; i <= 5; i++) {
            ImageView starImage = new ImageView(mContext);
            if (i <= detail.getRating()) {
                starImage.setImageResource(R.mipmap.star);
            } else {
                starImage.setImageResource(R.mipmap.star_no_color);
            }
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            int margin = Utils.dpToPx(mContext, 2);
            params.setMargins(margin, margin, margin, margin);
            mBinding.starLayout.addView(starImage);
        }
    }

    private void enableFeedback(boolean isEnabled) {
        if (isEnabled) {
            mBinding.leaveFeedback.setBackgroundColor(mContext.getResources().getColor(R.color.purple));
            mBinding.leaveFeedback.setEnabled(true);
        } else {
            mBinding.leaveFeedback.setEnabled(false);
            mBinding.leaveFeedback.setBackgroundColor(mContext.getResources().getColor(R.color.disable_dialog));
        }
    }

    private void getDetail() {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        mBinding.mainLayout.setVisibility(View.GONE);
        if (type == Constants.MY_PURCHASES) {
            getPurchaseDetail();
        } else {
            getSaleDetail();
        }
    }


    public void setId(int id, int type) {
        this.id = id;
        this.type = type;
    }

    public void updateDetail(Stock stocks) {
        this.detail = stocks;
        if (isAdded()){
            setData();
        }
    }

    public void changeDetailStatus() {
        detail.setPrintShippingLabel(false);
        setData();
    }


    public void getPurchaseDetail() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getPurchasesDetail("Bearer " + getAccessToken(), "" + id).enqueue(new Callback<ApiResponse<PurchasesDetail>>() {
            @Override
            public void onResponse(Call<ApiResponse<PurchasesDetail>> call, Response<ApiResponse<PurchasesDetail>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    Stock stock = response.body().getData();
                    updateDetail(stock);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getPurchaseDetail();
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ApiResponse<PurchasesDetail>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                showAlertDialog(getString(R.string.error_server));

            }
        });
    }

    public void getSaleDetail() {

        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getSaleDetail("Bearer " + getAccessToken(), "" + id).enqueue(new Callback<ApiResponse<SalesDetail>>() {
            @Override
            public void onResponse(Call<ApiResponse<SalesDetail>> call, Response<ApiResponse<SalesDetail>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    Stock stock = response.body().getData();
                    updateDetail(stock);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getSaleDetail();
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ApiResponse<SalesDetail>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                showAlertDialog(getString(R.string.error_server));

            }
        });
    }
}