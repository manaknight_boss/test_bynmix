package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.adapter.FeedViewPagerAdapter;
import com.bynmix.app.databinding.PurchaseFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.PurchaseListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.MyPurchasesStats;
import com.bynmix.app.models.MySalesStats;
import com.bynmix.app.models.PurchasesDetail;
import com.bynmix.app.models.ResponseList;
import com.bynmix.app.models.SalesDetail;
import com.bynmix.app.models.Stock;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.DialogAlert;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PurchasesFragment extends BaseFragment implements PurchaseListener {

    private PurchaseFragmentBinding mBinding;
    private ProfileListener mCallback;
    private Context mContext;
    private FeedViewPagerAdapter feedViewPagerAdapter;
    private MyPurchasesFragment myPurchasesFragment;
    private MyStatsFragment mystatsFragment;
    private int type;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (ProfileListener) context;
        mContext = context;
    }

    public static PurchasesFragment newInstance() {
        return new PurchasesFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = PurchaseFragmentBinding.inflate(inflater, container, false);
        setViewPager();
        if (type == Constants.MY_PURCHASES) {
            mBinding.heading.setText(getString(R.string.purchases));
            myPurchasesFragment.setType(type);
        } else {
            mBinding.heading.setText(getString(R.string.sales));
            mystatsFragment.setType(type);
        }
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        mBinding.sortGrid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myPurchasesFragment.setSortType(0);
            }
        });

        mBinding.sortVertical.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myPurchasesFragment.setSortType(1);
            }
        });

        return mBinding.getRoot();
    }

    private void setViewPager() {
        if (feedViewPagerAdapter == null) {
            feedViewPagerAdapter = new FeedViewPagerAdapter(getChildFragmentManager());
            myPurchasesFragment = new MyPurchasesFragment();
            mystatsFragment = new MyStatsFragment();
            String fragmentName;
            if (type == Constants.MY_PURCHASES) {
                fragmentName = getResources().getString(R.string.my_purchases);
            } else {
                fragmentName = getResources().getString(R.string.my_sales);
            }
            feedViewPagerAdapter.addFragment(myPurchasesFragment, fragmentName);
            feedViewPagerAdapter.addFragment(mystatsFragment, getResources().getString(R.string.my_stats));
            myPurchasesFragment.setType(type);
            mystatsFragment.setType(type);
            myPurchasesFragment.setListener(this);
            mystatsFragment.setListener(this);
        }
        mBinding.viewpager.setAdapter(feedViewPagerAdapter);
        mBinding.viewpager.setOffscreenPageLimit(2);
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);
        mBinding.viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    mBinding.toolbarIcons.setVisibility(View.VISIBLE);
                    myPurchasesFragment.setType(type);
                    myPurchasesFragment.callList();
                } else {
                    mBinding.toolbarIcons.setVisibility(View.GONE);
                    mystatsFragment.setType(type);
                    mystatsFragment.callList();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void updateFeedBack(Stock detail, int position) {
        myPurchasesFragment.updateFeedBack(detail, position);
    }

    public void updateMyPurchasesStats(MyPurchasesStats myStats) {
        mystatsFragment.updateMyPurchasesStats(myStats);
    }

    public void setType(int type) {
        this.type = type;
    }

    public void updateMySalesStats(MySalesStats myStats) {
        mystatsFragment.updateMySalesStats(myStats);
    }

    public void updateList(List<Stock> mList, int currentPage, int totalPage) {
        myPurchasesFragment.setList(mList, currentPage, totalPage);
    }

    @Override
    public void cancelPurchase(int id) {
        ((HomeActivity)mContext).cancelPurchase(id);
    }

    public void getMyPurchases(final int currentPageNumber) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getMyPurchases("Bearer " + getAccessToken(), "DESC").enqueue(new Callback<ApiResponse<ResponseList<PurchasesDetail>>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseList<PurchasesDetail>>> call, Response<ApiResponse<ResponseList<PurchasesDetail>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    ResponseList<PurchasesDetail> data = response.body().getData();
                    int currentPage = data.getPageNumber();
                    int totalPage = data.getTotalPages();
                    List<PurchasesDetail> purchasesDetail = data.getItems();
                    List<Stock> stocks = new ArrayList<>();
                    stocks.addAll(purchasesDetail);
                    updateList(stocks, currentPage, totalPage);

                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getMyPurchases(currentPageNumber);
                        }

                        @Override
                        public void errorAccessToken() {
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                myPurchasesFragment.hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseList<PurchasesDetail>>> call, Throwable t) {
                showAlertDialog(getResources().getString(R.string.error_server));
                myPurchasesFragment.hideProgress();
            }
        });
    }


    public void getMySales(final int currentPageNumber) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getMySales("Bearer " + getAccessToken(), "DESC").enqueue(new Callback<ApiResponse<ResponseList<SalesDetail>>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseList<SalesDetail>>> call, Response<ApiResponse<ResponseList<SalesDetail>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    ResponseList<SalesDetail> data = response.body().getData();
                    int currentPage = data.getPageNumber();
                    int totalPage = data.getTotalPages();
                    List<SalesDetail> salesDetails = data.getItems();
                    List<Stock> stocks = new ArrayList<>();
                    stocks.addAll(salesDetails);
                    updateList(stocks, currentPage, totalPage);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getMyPurchases(currentPageNumber);
                        }

                        @Override
                        public void errorAccessToken() {
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                myPurchasesFragment.hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseList<SalesDetail>>> call, Throwable t) {
                showAlertDialog(getResources().getString(R.string.error_server));
                myPurchasesFragment.hideProgress();
            }
        });
    }

    public void getMySalesStats() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.mySalesStats("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<MySalesStats>>() {
            @Override
            public void onResponse(Call<ApiResponse<MySalesStats>> call, Response<ApiResponse<MySalesStats>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    MySalesStats myStats = response.body().getData();
                    mCallback.updateSaleStats(myStats);
                    updateMySalesStats(myStats);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getMySalesStats();
                        }

                        @Override
                        public void errorAccessToken() {

                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mystatsFragment.hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<MySalesStats>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getString(R.string.error_server));
                mystatsFragment.hideProgress();
            }
        });
    }

    public void getMyPurchasesStats() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.myStats("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<MyPurchasesStats>>() {
            @Override
            public void onResponse(Call<ApiResponse<MyPurchasesStats>> call, Response<ApiResponse<MyPurchasesStats>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    MyPurchasesStats myStats = response.body().getData();
                    mCallback.updatePurchaseStats(myStats);
                    updateMyPurchasesStats(myStats);

                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getMyPurchasesStats();
                        }

                        @Override
                        public void errorAccessToken() {
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mystatsFragment.hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<MyPurchasesStats>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getString(R.string.error_server));
                mystatsFragment.hideProgress();
            }
        });
    }

    public void relistItem(final int listingId) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.relistItem("Bearer " + getAccessToken(), "" + listingId).enqueue(new Callback<ApiResponse<Integer>>() {
            @Override
            public void onResponse(Call<ApiResponse<Integer>> call, Response<ApiResponse<Integer>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    mCallback.onBackPressed();
                    mCallback.openMyBynFragment();
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            relistItem(listingId);
                        }

                        @Override
                        public void errorAccessToken() {
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Integer>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }
}
