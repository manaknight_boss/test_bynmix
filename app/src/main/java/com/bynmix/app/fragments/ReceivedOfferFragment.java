package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.adapter.NotificationAdapter;
import com.bynmix.app.databinding.NotificationFragmentBinding;
import com.bynmix.app.interfaces.NotificationListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.Notification;
import com.bynmix.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class ReceivedOfferFragment extends Fragment {

    private NotificationFragmentBinding mBinding;
    private Context mContext;
    private ProfileListener mCallback;
    private List<Notification> mList = new ArrayList<>();
    private NotificationAdapter mAdapter;
    private boolean isAlreadyCalled;
    private int currentPageNumber;
    private int totalPages;
    private NotificationListener notificationListener;

    public static ReceivedOfferFragment newInstance() {

        return new ReceivedOfferFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (ProfileListener) context;
        mContext = context;
    }

    @Override
    public void onResume() {
        super.onResume();
        callList();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = NotificationFragmentBinding.inflate(inflater, container, false);
        setAdapter();
        mBinding.swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                {

                    if (mCallback.conditionsForApiCall()) {
                        currentPageNumber = 0;
                        fetchReceivedOffersNotification();
                    } else {
                        hideSwipeRefresh();
                    }
                }
            }
        });

        mBinding.recycleView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();
                boolean endHasBeenReached = lastVisible >= totalItemCount - 4;
                if (totalItemCount > 0 && endHasBeenReached) {
                    if (!isAlreadyCalled && currentPageNumber < totalPages && mList.size() > 0) {
                        isAlreadyCalled = true;
                        fetchReceivedOffersNotification();
                    }
                }

            }
        });


        return mBinding.getRoot();
    }

    private void setLoadMoreProgress() {
        if (mList.size() == 0) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
        } else {
            mBinding.progressBar.setVisibility(View.GONE);
        }
    }

    private void hideSwipeRefresh() {
        mBinding.swipeRefreshLayout.setRefreshing(false);
    }

    private void fetchReceivedOffersNotification() {
        setLoadMoreProgress();
        notificationListener.getReceivedOffersNotifications(currentPageNumber);
    }

    private void setAdapter() {
        mAdapter = new NotificationAdapter(mContext, mList, mCallback, Constants.RECEIVED_OFFERS);
        mBinding.recycleView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recycleView.setAdapter(mAdapter);
    }

    public void callList() {
        currentPageNumber = 0;
        fetchReceivedOffersNotification();
    }

    public void setList(List<Notification> mList, int currentPage, int totalPages) {
        this.currentPageNumber = currentPage;
        this.totalPages = totalPages;
        if (currentPageNumber > 1) {
            this.mList.addAll(mList);
        } else {
            this.mList = mList;
        }
        if (mList.size() == 0) {
            setError();
        } else {
            mBinding.nothingToShowPopUp.setVisibility(View.GONE);
            mAdapter.setList(mList);
        }
        isAlreadyCalled = false;
        mBinding.progressBar.setVisibility(View.GONE);
        hideSwipeRefresh();
    }

    public void setError() {
        mBinding.nothingToShow.setVisibility(View.VISIBLE);
        mBinding.errorText.setText("You don’t have any received offers yet.");
        mBinding.errorText2.setText("Any offers other fashionistas make for your active listings will display here.");
        mBinding.nothingToShowPopUp.setVisibility(View.VISIBLE);
        mList.clear();
        currentPageNumber = 0;
    }

    public void setListener(NotificationListener notificationListener) {
        this.notificationListener = notificationListener;
    }
}
