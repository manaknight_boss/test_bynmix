package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.databinding.FragmentReportAnIssueBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Dispute;
import com.bynmix.app.models.DisputeReasons;
import com.bynmix.app.models.PurchasesDetail;
import com.bynmix.app.models.Stock;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.DialogAlert;
import com.bynmix.app.utils.Utils;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportAnIssueFragment extends BaseFragment {

    private FragmentReportAnIssueBinding mBinding;
    private Context mContext;
    private CreatePostListener mCallback;
    private Stock detail;
    List<String> mImageList = new ArrayList<>();
    private String image1, image2, image3, image4;
    private List<DisputeReasons> mList = new ArrayList<>();
    private List<String> disputeReasonsName = new ArrayList<>();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (CreatePostListener) context;
    }

    public static ReportAnIssueFragment newInstance() {
        return new ReportAnIssueFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentReportAnIssueBinding.inflate(inflater, container, false);
        if(((PurchasesDetail)detail).isCanCancelPurchase() && ((PurchasesDetail)detail).getPurchaseStatusId() == Constants.PURCHASE_STATUS_ID) {
            setData();
        } else {
            if (mList.size() == 0) {
                mBinding.progressBar.setVisibility(View.VISIBLE);
                mBinding.mainLayout.setVisibility(View.GONE);
                getIssues();
            } else {
                setData();
            }
        }
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
        imagesSelectClick();
        imagesRemoveClick();
        mBinding.submitButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendData();
            }
        });
        return mBinding.getRoot();
    }

    private void getIssues() {
        getDisputesIssues();
    }

    private void sendData() {
        String reasonName, comment;
        comment = mBinding.comment.getText().toString();
        Dispute dispute = new Dispute();
        int disputeIssueId = 0;
        reasonName = mBinding.spinner.getSelectedItem().toString();
        if (reasonName.equalsIgnoreCase("Select issue type….")) {
            Toast.makeText(mContext, "Please select an issue to add dispute", Toast.LENGTH_SHORT).show();
            return;
        }
        for (int i = 0; i < mList.size(); i++) {
            if (reasonName.equalsIgnoreCase(mList.get(i).getDisputeTypeName())) {
                disputeIssueId = mList.get(i).getDisputeTypeId();
                break;
            }
        }


        dispute.setComments(comment);
        dispute.setDisputeTypeId(disputeIssueId);
        dispute.setPurchaseId(((PurchasesDetail) detail).getPurchaseId());
        if (!TextUtils.isEmpty(image1)) {
            mImageList.add(image1);
        }
        if (!TextUtils.isEmpty(image2)) {
            mImageList.add(image2);
        }
        if (!TextUtils.isEmpty(image3)) {
            mImageList.add(image3);
        }
        if (!TextUtils.isEmpty(image4)) {
            mImageList.add(image4);
        }
        sendApi(dispute);
    }

    private void sendApi(final Dispute dispute) {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.reportAnIssue("Bearer " + ((HomeActivity) mContext).getAccessToken(), dispute).enqueue(new Callback<ApiResponse<Integer>>() {
            @Override
            public void onResponse(Call<ApiResponse<Integer>> call, Response<ApiResponse<Integer>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    MyPurchasesFragment.isReloadRequired = true;
                    PurchaseDetailFragment.isReloadRequired = true;
                    if (mImageList.size() > 0) {
                        int id = response.body().getData();
                        mCallback.sendReportAnIssueImages(id, mImageList);
                    } else {
                        mCallback.popFragment();
                    }
                } else if (response.code() == 401) {
                    ((HomeActivity) mContext).getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            sendApi(dispute);
                        }

                        @Override
                        public void errorAccessToken() {

                        }
                    });
                } else {
                    ((HomeActivity) mContext).mAlertDialog.showDialog(mContext, getResources().getString(R.string.error_server));
                }
                mBinding.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ApiResponse<Integer>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                ((HomeActivity) mContext).mAlertDialog.showDialog(mContext, getResources().getString(R.string.error_server));
            }
        });
    }

    private void imagesRemoveClick() {
        mBinding.crossIcon1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateEvidencePhoto1(null);
            }
        });
        mBinding.crossIcon2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateEvidencePhoto2(null);
            }
        });
        mBinding.crossIcon3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateEvidencePhoto3(null);
            }
        });
        mBinding.crossIcon4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateEvidencePhoto4(null);
            }
        });
    }

    private void imagesSelectClick() {
        mBinding.addPhoto1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openGalleryFor(Constants.REPORT_AN_ISSUE_IMAGE_1, 1);
            }
        });
        mBinding.addPhoto2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openGalleryFor(Constants.REPORT_AN_ISSUE_IMAGE_2, 1);
            }
        });
        mBinding.addPhoto3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openGalleryFor(Constants.REPORT_AN_ISSUE_IMAGE_3, 1);
            }
        });
        mBinding.addPhoto4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openGalleryFor(Constants.REPORT_AN_ISSUE_IMAGE_4, 1);
            }
        });
    }

    private void setData() {
        mBinding.progressBar.setVisibility(View.GONE);
        mBinding.mainLayout.setVisibility(View.VISIBLE);
        if(((PurchasesDetail)detail).isCanCancelPurchase() && ((PurchasesDetail)detail).getPurchaseStatusId() == Constants.PURCHASE_STATUS_ID) {
            mBinding.cancelOnlyLayout.setVisibility(View.VISIBLE);
            mBinding.reportReasonLayout.setVisibility(View.GONE);
            mBinding.cancelOnlyButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (detail instanceof PurchasesDetail) {
                        DialogAlert.showDialog(mContext, detail.getListingTitle(), mContext.getString(R.string.are_you_sure_cancel_purchase), R.string.yes_cancel_purchase, new DialogClickListener() {
                            @Override
                            public void onClick() {
                                ((HomeActivity)mContext).cancelPurchase(((PurchasesDetail) detail).getPurchaseId());
                            }
                        });
                    } else {
                        mCallback.openCancelSaleFragment(detail);
                    }

                }
            });
            mBinding.submitButtonLayout.setVisibility(View.GONE);
        } else {
            mBinding.cancelOnlyLayout.setVisibility(View.GONE);
            mBinding.reportReasonLayout.setVisibility(View.VISIBLE);
            ArrayAdapter<String> disputeReasonAdapter = new ArrayAdapter<String>(mContext, R.layout.profile_spinner_item, disputeReasonsName);
            mBinding.spinner.setAdapter(disputeReasonAdapter);
            if (((PurchasesDetail) detail).isCanCancelPurchase()) {
                mBinding.cancelLayout.setVisibility(View.VISIBLE);
            } else {
                mBinding.cancelLayout.setVisibility(View.GONE);
            }
            mBinding.cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (detail instanceof PurchasesDetail) {
                        DialogAlert.showDialog(mContext, detail.getListingTitle(), mContext.getString(R.string.are_you_sure_cancel_purchase), R.string.yes_cancel_purchase, new DialogClickListener() {
                            @Override
                            public void onClick() {
                                ((HomeActivity)mContext).cancelPurchase(((PurchasesDetail) detail).getPurchaseId());
                            }
                        });
                    } else {
                        mCallback.openCancelSaleFragment(detail);
                    }

                }
            });

            mBinding.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                    if(view != null) {
                        if (position == 0) {
                            ((TextView) view).setTextColor(getResources().getColor(R.color.lightGrey));
                        } else {
                            ((TextView) view).setTextColor(getResources().getColor(R.color.colorPrimary));
                        }
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }

        mBinding.title.setText(Html.fromHtml("\"" + "<b>" + "<font color='" + mContext.getResources().getColor(R.color.purple) + "'>" + detail.getListingTitle() + "</font>" + "</b>" + "\""), TextView.BufferType.SPANNABLE);
        String image = detail.getListingImageUrl();
        if (!TextUtils.isEmpty(image)) {
            Picasso.get().load(image).into(mBinding.image);
        } else {
            mBinding.image.setImageResource(R.mipmap.default_feed);
        }

        mBinding.status.setText(((PurchasesDetail) detail).getPurchaseStatus());

        setIcons(((PurchasesDetail) detail).getPurchaseStatusId());
    }

    private void setIcons(int purchaseStatusId) {
        switch (purchaseStatusId) {
            case 1:
                mBinding.statusIcon.setImageResource(R.mipmap.hourglass);
                break;
            case 2:
                mBinding.statusIcon.setImageResource(R.mipmap.hourglass);
                break;
            case 3:
                mBinding.statusIcon.setImageResource(R.mipmap.hourglass);
                break;
            case 4:
                mBinding.statusIcon.setImageResource(R.mipmap.label_black);
                break;
            case 5:
                mBinding.statusIcon.setImageResource(R.mipmap.truck);
                break;
            case 6:
                mBinding.statusIcon.setImageResource(R.mipmap.hourglass);
                break;
            case 7:
                mBinding.statusIcon.setImageResource(R.mipmap.delivery);
                break;
            case 8:
                mBinding.statusIcon.setImageResource(R.mipmap.hourglass);
                break;
            case 9:
                mBinding.statusIcon.setImageResource(R.mipmap.hourglass);
                break;
            case 10:
                mBinding.statusIcon.setImageResource(R.mipmap.hourglass);
                break;
            case 11:
                mBinding.statusIcon.setImageResource(R.mipmap.hourglass);
                break;
            case 12:
                mBinding.statusIcon.setImageResource(R.mipmap.hourglass);
                break;
        }

    }

    public void error() {
        mBinding.progressBar.setVisibility(View.GONE);
    }

    public void setStock(Stock detail) {
        this.detail = detail;
    }

    public void setPhoto(String image, int requestCode) {
        if (requestCode == Constants.CROPPER_REPORT_AN_ISSUE_REQUEST_CODE_1) {
            updateEvidencePhoto1(image);
        } else if (requestCode == Constants.CROPPER_REPORT_AN_ISSUE_REQUEST_CODE_2) {
            updateEvidencePhoto2(image);
        }
        if (requestCode == Constants.CROPPER_REPORT_AN_ISSUE_REQUEST_CODE_3) {
            updateEvidencePhoto3(image);
        }
        if (requestCode == Constants.CROPPER_REPORT_AN_ISSUE_REQUEST_CODE_4) {
            updateEvidencePhoto4(image);
        }
    }

    private void updateEvidencePhoto1(String image) {
        this.image1 = image;
        if (!TextUtils.isEmpty(image)) {
            mBinding.addPhoto1.setVisibility(View.GONE);
            mBinding.selectedPhotoLayout1.setVisibility(View.VISIBLE);
            File fLocal = new File(image);
            try {
                Picasso.get().load(fLocal).into(mBinding.listingPhoto1);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            mBinding.addPhoto1.setVisibility(View.VISIBLE);
            mBinding.selectedPhotoLayout1.setVisibility(View.GONE);
        }
    }


    private void updateEvidencePhoto2(String image) {
        this.image2 = image;
        if (!TextUtils.isEmpty(image)) {
            mBinding.addPhoto2.setVisibility(View.GONE);
            mBinding.selectedPhotoLayout2.setVisibility(View.VISIBLE);
            File fLocal = new File(image);
            try {
                Picasso.get().load(fLocal).into(mBinding.listingPhoto2);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            mBinding.addPhoto2.setVisibility(View.VISIBLE);
            mBinding.selectedPhotoLayout2.setVisibility(View.GONE);
        }
    }


    private void updateEvidencePhoto3(String image) {
        this.image3 = image;
        if (!TextUtils.isEmpty(image)) {
            mBinding.addPhoto3.setVisibility(View.GONE);
            mBinding.selectedPhotoLayout3.setVisibility(View.VISIBLE);
            File fLocal = new File(image);
            try {
                Picasso.get().load(fLocal).into(mBinding.listingPhoto3);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            mBinding.addPhoto3.setVisibility(View.VISIBLE);
            mBinding.selectedPhotoLayout3.setVisibility(View.GONE);
        }
    }


    private void updateEvidencePhoto4(String image) {
        this.image4 = image;
        if (!TextUtils.isEmpty(image)) {
            mBinding.addPhoto4.setVisibility(View.GONE);
            mBinding.selectedPhotoLayout4.setVisibility(View.VISIBLE);
            File fLocal = new File(image);
            try {
                Picasso.get().load(fLocal).into(mBinding.listingPhoto4);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } else {
            mBinding.addPhoto4.setVisibility(View.VISIBLE);
            mBinding.selectedPhotoLayout4.setVisibility(View.GONE);
        }
    }

    public void updateDisputeReason(List<DisputeReasons> disputeReasons) {
        this.mList = disputeReasons;
        for (int i = 0; i < mList.size(); i++) {
            disputeReasonsName.add(mList.get(i).getDisputeTypeName());
        }
        disputeReasonsName.add(0, "Select issue type….");
        setData();
    }


    public void getDisputesIssues() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.disputeReasons("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<List<DisputeReasons>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<DisputeReasons>>> call, Response<ApiResponse<List<DisputeReasons>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    List<DisputeReasons> disputeReasons = response.body().getData();
                    updateDisputeReason(disputeReasons);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getDisputesIssues();
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<List<DisputeReasons>>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
                showAlertDialog(getString(R.string.error_server));
            }
        });
    }
}
