package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.databinding.FragmentReportAnIssueListingBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Dispute;
import com.bynmix.app.models.DisputeReasons;
import com.bynmix.app.models.IssueType;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.CongoDialog;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReportAnIssueListingFragment extends BaseFragment {

    private FragmentReportAnIssueListingBinding mBinding;
    private Context mContext;
    private CreatePostListener mCallback;
    private ListingDetail detail;
    private List<IssueType> mList = new ArrayList<>();
    private List<String> disputeReasonsName = new ArrayList<>();
    String reasonName, comment;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (CreatePostListener) context;
    }

    public static ReportAnIssueListingFragment newInstance() {
        return new ReportAnIssueListingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentReportAnIssueListingBinding.inflate(inflater, container, false);
        if (mList.size() == 0) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
            mBinding.mainLayout.setVisibility(View.GONE);
            getIssues();
        } else {
            setData();
        }
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
        mBinding.submitButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendData();
            }
        });

        mBinding.comment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                checkButtonEnableComplete();
            }
        });


        return mBinding.getRoot();
    }

    private void checkButtonEnableComplete() {
        comment = mBinding.comment.getText().toString().trim();
        reasonName = mBinding.spinner.getSelectedItem().toString();
        if (!TextUtils.isEmpty(comment) && !reasonName.equalsIgnoreCase("Select issue type….")) {
            mBinding.submitButtonLayout.setBackground(mContext.getResources().getDrawable(R.drawable.purple_rounded_button));
            mBinding.submitButtonLayout.setEnabled(true);
        } else {
            mBinding.submitButtonLayout.setBackground(mContext.getResources().getDrawable(R.drawable.disable_dialog_button));
            mBinding.submitButtonLayout.setEnabled(false);
        }
    }

    private void getIssues() {
        getDisputesIssues();
    }

    private void sendData() {
        Dispute dispute = new Dispute();
        int disputeIssueId = 0;
        for (int i = 0; i < mList.size(); i++) {
            if (reasonName.equalsIgnoreCase(mList.get(i).getName())) {
                disputeIssueId = mList.get(i).getReportedIssueTypeId();
                break;
            }
        }
        dispute.setComments(comment);
        dispute.setReportedIssueTypeId(disputeIssueId);
        dispute.setIssueTypeId(1);
        dispute.setEntityId(detail.getId());
        sendApi(dispute);
    }

    private void sendApi(final Dispute dispute) {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.reportAnIssueListing("Bearer " + ((HomeActivity) mContext).getAccessToken(), dispute).enqueue(new Callback<ApiResponse<Integer>>() {
            @Override
            public void onResponse(Call<ApiResponse<Integer>> call, Response<ApiResponse<Integer>> response) {
                if (response.isSuccessful() && response.code() == 201) {
                    CongoDialog congoDialog = new CongoDialog();
                    congoDialog.showDialog(mContext, "Thank you!", getString(R.string.report_an_issue_submit_message));
                    mCallback.popFragment();
                } else if (response.code() == 401) {
                    ((HomeActivity) mContext).getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            sendApi(dispute);
                        }

                        @Override
                        public void errorAccessToken() {

                        }
                    });
                } else {
                    ((HomeActivity) mContext).mAlertDialog.showDialog((HomeActivity) mContext, getResources().getString(R.string.error_server));
                }
                mBinding.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ApiResponse<Integer>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                ((HomeActivity) mContext).mAlertDialog.showDialog((HomeActivity) mContext, getResources().getString(R.string.error_server));
            }
        });
    }


    private void setData() {
        mBinding.progressBar.setVisibility(View.GONE);
        mBinding.mainLayout.setVisibility(View.VISIBLE);
        ArrayAdapter<String> disputeReasonAdapter = new ArrayAdapter<String>(mContext, R.layout.profile_spinner_item, disputeReasonsName);
        mBinding.spinner.setAdapter(disputeReasonAdapter);

        mBinding.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                checkButtonEnableComplete();
                if (position == 0) {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.lightGrey));
                } else {
                    ((TextView) view).setTextColor(getResources().getColor(R.color.colorPrimary));
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void error() {
        mBinding.progressBar.setVisibility(View.GONE);
    }

    public void setListing(ListingDetail detail) {
        this.detail = detail;
    }

    public void updateIssueReason(List<IssueType> issueTypes) {
        this.mList = issueTypes;
        for (int i = 0; i < mList.size(); i++) {
            disputeReasonsName.add(mList.get(i).getName());
        }
        disputeReasonsName.add(0, "Select issue type….");
        setData();
    }


    public void getDisputesIssues() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getIssueTypes("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<List<IssueType>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<IssueType>>> call, Response<ApiResponse<List<IssueType>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    List<IssueType> issueTypes = response.body().getData();
                    updateIssueReason(issueTypes);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getDisputesIssues();
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<List<IssueType>>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
                showAlertDialog(getString(R.string.error_server));
            }
        });
    }
}
