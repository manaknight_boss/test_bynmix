package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.FeedViewPagerAdapter;
import com.bynmix.app.databinding.FragmentReviewsBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.ReviewListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Reviews;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ReviewFragment extends BaseFragment implements ReviewListener {

    private FragmentReviewsBinding mBinding;
    private CreatePostListener mCallback;
    private Context mContext;
    private FeedViewPagerAdapter feedViewPagerAdapter;
    private BuyerSellerReviewFragment buyerReviewFragment;
    private BuyerSellerReviewFragment sellerReviewFragment;
    private int userId;
    private String username;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (CreatePostListener) context;
        mContext = context;
    }

    public static ReviewFragment newInstance() {
        return new ReviewFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentReviewsBinding.inflate(inflater, container, false);
        setViewPager();
        setType();
        setUserId();
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        mBinding.heading.setText("@" + username + " " + getString(R.string.reviews));

        return mBinding.getRoot();
    }

    private void setUserId() {
        buyerReviewFragment.setUserId(userId);
        buyerReviewFragment.setUsername(username);
        sellerReviewFragment.setUsername(username);
        sellerReviewFragment.setUserId(userId);
    }

    private void setType() {
        buyerReviewFragment.setType(Constants.BUYER);
        sellerReviewFragment.setType(Constants.SELLER);
    }

    private void setViewPager() {
        if (feedViewPagerAdapter == null) {
            feedViewPagerAdapter = new FeedViewPagerAdapter(getChildFragmentManager());
            buyerReviewFragment = new BuyerSellerReviewFragment();
            sellerReviewFragment = new BuyerSellerReviewFragment();
            feedViewPagerAdapter.addFragment(buyerReviewFragment, getResources().getString(R.string.as_buyer));
            feedViewPagerAdapter.addFragment(sellerReviewFragment, getResources().getString(R.string.as_seller));
            buyerReviewFragment.setCallBack(this);
            sellerReviewFragment.setCallBack(this);
        }
        mBinding.viewpager.setAdapter(feedViewPagerAdapter);
        mBinding.viewpager.setOffscreenPageLimit(2);
        mBinding.tabs.setupWithViewPager(mBinding.viewpager);

        mBinding.viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    buyerReviewFragment.callList();
                } else {
                    sellerReviewFragment.callList();
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    public void setMyUserId(int myUserId) {
        this.userId = myUserId;
    }

    public void updateBuyerReviews(List<Reviews> mList, int currentPageNumber, int totalPage) {
        buyerReviewFragment.setList(mList, currentPageNumber, totalPage);
    }

    public void updateSellerReviews(List<Reviews> mList, int currentPageNumber, int totalPage) {
        sellerReviewFragment.setList(mList, currentPageNumber, totalPage);
    }

    public void error(int postType, int error) {
        if (postType == Constants.SELLER) {
            sellerReviewFragment.setError(error);
        } else {
            buyerReviewFragment.setError(error);
        }
    }

    public void setUsername(String username) {

        this.username = username;
    }


    public void fetchBuyerReviews(final int userId, final int currentPageNumber) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.fetchBuyerReviews("Bearer " + getAccessToken(), "" + userId, "desc", currentPageNumber + 1).enqueue(new Callback<ApiResponse<Reviews>>() {
            @Override
            public void onResponse(Call<ApiResponse<Reviews>> call, Response<ApiResponse<Reviews>> response) {
                setReviewResponse(response, userId, 0, currentPageNumber);
            }

            @Override
            public void onFailure(Call<ApiResponse<Reviews>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }


    void setReviewResponse(Response<ApiResponse<Reviews>> response, final int userId, final int type, final int currentPageNumber) {
        if (response.isSuccessful() && response.code() == 200) {
            Reviews reviews = response.body().getData();
            int currentPage = reviews.getPageNumber();
            int totalPage = reviews.getTotalPages();
            List<Reviews> mList = reviews.getItems();
            if (type == Constants.BUYER) {
                if (currentPage == 1 && mList.size() == 0) {
                    setMyReviewsError(404, Constants.BUYER);
                } else {
                    updateBuyerReviews(mList, currentPage, totalPage);
                }
            } else {
                if (currentPage == 1 && mList.size() == 0) {
                    setMyReviewsError(404, Constants.SELLER);
                } else {
                    updateSellerReviews(mList, currentPage, totalPage);
                }
            }
        } else if (response.code() == 401) {
            getAccessTokenFromServer(new AccessTokenResponse() {
                @Override
                public void successAccessToken() {
                    if (type == Constants.BUYER) {
                        fetchBuyerReviews(userId, currentPageNumber);
                    } else {
                        fetchSellerReviews(userId, currentPageNumber);
                    }
                }

                @Override
                public void errorAccessToken() {
                }
            });
        } else {
            setMyReviewsError(400, type);
        }
    }

    private void setMyReviewsError(int errorCode, int type) {
        error(type, errorCode);
    }

    public void fetchSellerReviews(final int userId, final int currentPageNumber) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.fetchSellerReviews("Bearer " + getAccessToken(), "" + userId, "desc", currentPageNumber + 1).enqueue(new Callback<ApiResponse<Reviews>>() {
            @Override
            public void onResponse(Call<ApiResponse<Reviews>> call, Response<ApiResponse<Reviews>> response) {
                setReviewResponse(response, userId, 1, currentPageNumber);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Reviews>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

}
