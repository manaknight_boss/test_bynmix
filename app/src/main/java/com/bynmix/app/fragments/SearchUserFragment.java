package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.adapter.FollowFollowingAdapter;
import com.bynmix.app.adapter.SearchUserAdapter;
import com.bynmix.app.databinding.FragmentSearchUserBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Bloggers;
import com.bynmix.app.models.UserFilterData;
import com.bynmix.app.models.UserResponse;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.BaseActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchUserFragment extends Fragment {
    private FragmentSearchUserBinding mBinding;
    private ProfileListener mCallBack;
    private Context mContext;
    private List<UserResponse> mList = new ArrayList<>();
    private FollowFollowingAdapter mAdapter;
    private String searchedText;
    private int currentPageNumber;
    private int totalPages;
    private boolean isAlreadyCalled;
    private Bloggers mBlogger = new Bloggers();
    private Timer timer;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallBack = (ProfileListener) mContext;
    }

    public static SearchUserFragment newInstance() {
        return new SearchUserFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentSearchUserBinding.inflate(inflater, container, false);
        isAlreadyCalled = false;
        setAdapter();
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.onBackPressed();
            }
        });
        if (mList.size() == 0) {
            currentPageNumber = 0;
            mBinding.progressBar.setVisibility(View.VISIBLE);
            searchedText = null;
            getSearchedUsers();
        }

        mBinding.filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.openFilterUserFragment();
            }
        });

        mBinding.crossIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchedText = null;
                setSearchBarUI();
            }
        });


        mBinding.autoComplete.requestFocus();
        mBinding.autoComplete.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {
                mBinding.searchIcon.setVisibility(View.GONE);
                mBinding.crossIcon.setVisibility(View.GONE);
                final String text = s.toString().trim();
                currentPageNumber = 0;
                totalPages = 0;
                if (text.length() >= 1) {
                    mBinding.progressBarSearch.setVisibility(View.VISIBLE);
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            if (mCallBack.conditionsForApiCall()) {
                                searchedText = text;
                                getSearchedUsers();
                            }
                        }
                    }, 600); // 600ms delay before the timer executes the „run“ method from TimerTask
                } else if (s.length() == 0) {
                    mBinding.progressBarSearch.setVisibility(View.VISIBLE);
                    searchedText = null;
                    getSearchedUsers();
                }

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                mBinding.crossIcon.setVisibility(View.VISIBLE);
                mBinding.crossIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mBinding.autoComplete.setText("");
                        setSearchBarUI();
                    }
                });
                if (timer != null) {
                    timer.cancel();
                }
            }

        });
        mBinding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();
                boolean endHasBeenReached = lastVisible >= totalItemCount - 4;
                if (totalItemCount > 0 && endHasBeenReached) {
                    if (!isAlreadyCalled && currentPageNumber < totalPages && mList.size() > 0) {
                        isAlreadyCalled = true;
                        getSearchedUsers();
                        setLoadMore();
                    }
                }
            }
        });

        return mBinding.getRoot();
    }

    private void setLoadMore() {
        if (isAlreadyCalled) {
            mAdapter.setProgress(true);
        } else {
            mAdapter.setProgress(false);
        }
    }

    private void getSearchedUsers() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        UserFilterData filterData = mCallBack.getUserFilterData();
        filterData.setSearchText(searchedText);
        String accessToken = ((BaseActivity) mContext).getAccessToken();
        apiInstance.searchUser("Bearer " + accessToken, filterData, currentPageNumber + 1).enqueue(new Callback<ApiResponse<Bloggers>>() {
            @Override
            public void onResponse(Call<ApiResponse<Bloggers>> call, Response<ApiResponse<Bloggers>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    if (response.body().getData() != null) {
                        mBlogger = response.body().getData();
                        List<UserResponse> list = mBlogger.getItems();
                        currentPageNumber = mBlogger.getPageNumber();
                        totalPages = mBlogger.getTotalPages();
                        if (currentPageNumber > 1) {
                            mList.addAll(list);
                        } else {
                            mList = list;
                        }
                        mAdapter.setList(mList);
                        if (mList == null || mList.size() <= 0) {
                            setError(404);
                        }

                    }

                } else {
                    setError(400);
                }
                isAlreadyCalled = false;
                mBinding.progressBar.setVisibility(View.GONE);
                setSearchBarUI();
                setLoadMore();
            }

            @Override
            public void onFailure(Call<ApiResponse<Bloggers>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                setSearchBarUI();
                setError(400);
            }
        });
    }

    private void setError(int errorCode) {
        mBinding.nothingToShowPopUp.setVisibility(View.VISIBLE);
        if (errorCode == 404) {
            mBinding.errorPopup.setVisibility(View.GONE);
            mBinding.nothingToShow.setVisibility(View.VISIBLE);
        } else {
            mBinding.errorPopup.setVisibility(View.VISIBLE);
            mBinding.nothingToShow.setVisibility(View.GONE);
        }
    }

    private void setSearchBarUI() {
        mBinding.progressBarSearch.setVisibility(View.GONE);
        searchedText = mBinding.autoComplete.getText().toString();
        if (TextUtils.isEmpty(searchedText)) {
            mBinding.searchIcon.setVisibility(View.VISIBLE);
            mBinding.crossIcon.setVisibility(View.GONE);
            searchedText = null;
        } else {
            mBinding.searchIcon.setVisibility(View.GONE);
            mBinding.crossIcon.setVisibility(View.VISIBLE);
        }
    }

    private void setAdapter() {
        mAdapter = new FollowFollowingAdapter(mContext, mList, mCallBack);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    public void clearListingList() {
        currentPageNumber = 0;
        mList.clear();
    }
}
