package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.ExistingAddressAdapter;
import com.bynmix.app.databinding.SelectDifferentPaymentFragmentBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.SelectExistingAddressListener;
import com.bynmix.app.models.Address;

import java.util.ArrayList;
import java.util.List;

public class SelectDifferentAddressFragment extends BaseFragment {
    private SelectDifferentPaymentFragmentBinding mBinding;
    private ProfileListener mCallBack;
    private Context mContext;
    private List<Address> mAddressesList = new ArrayList<>();
    private ExistingAddressAdapter mAdapter;
    private SelectExistingAddressListener mListener;
    private Address defaultAddress;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallBack = (ProfileListener) mContext;
    }

    public static SelectDifferentAddressFragment newInstance() {
        return new SelectDifferentAddressFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = SelectDifferentPaymentFragmentBinding.inflate(inflater, container, false);
        mBinding.toolbarHeading.setText(getString(R.string.select_different_address));
        mBinding.progressBar.setVisibility(View.VISIBLE);
        mBinding.recyclerView.setVisibility(View.GONE);
        getMyAddress();
        setAdapter();
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.onBackPressed();
            }
        });
        return mBinding.getRoot();
    }

    private void setAdapter() {
        mAdapter = new ExistingAddressAdapter(mContext, mAddressesList, mCallBack, mListener);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    public void setListener(SelectExistingAddressListener mListener) {
        this.mListener = mListener;
    }

    public void setDefaultAddress(Address selectedAddress) {
        this.defaultAddress = selectedAddress;
    }

    public void updateAddressList(List<Address> addresses) {
        mBinding.progressBar.setVisibility(View.GONE);
        mBinding.recyclerView.setVisibility(View.VISIBLE);
        for (int i = 0; i < addresses.size(); i++) {

            if (defaultAddress.getAddressId() == addresses.get(i).getAddressId()) {
                addresses.remove(i);
            }
        }
        this.mAddressesList = addresses;
        mAdapter.setList(mAddressesList);
    }
}
