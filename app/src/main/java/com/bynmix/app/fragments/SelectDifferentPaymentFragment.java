package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.SelectDifferentPaymentAdapter;
import com.bynmix.app.databinding.SelectDifferentPaymentFragmentBinding;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.SelectNewPaymentListener;
import com.bynmix.app.models.CardDetail;
import com.bynmix.app.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class SelectDifferentPaymentFragment extends Fragment {
    private SelectDifferentPaymentFragmentBinding mBinding;
    private ProfileListener mCallBack;
    private Context mContext;
    private List<CardDetail> mList = new ArrayList<>();
    private SelectDifferentPaymentAdapter mAdapter;
    private SelectNewPaymentListener mListener;
    private CardDetail defaultPayment;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallBack = (CreatePostListener) mContext;
    }

    public static SelectDifferentPaymentFragment newInstance() {
        return new SelectDifferentPaymentFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = SelectDifferentPaymentFragmentBinding.inflate(inflater, container, false);
        mBinding.toolbarHeading.setText(getString(R.string.select_different_payment));
        mBinding.progressBar.setVisibility(View.VISIBLE);
        mBinding.recyclerView.setVisibility(View.GONE);
        mCallBack.getWalletDetail();
        setAdapter();
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.onBackPressed();
            }
        });
        return mBinding.getRoot();
    }

    private void setAdapter() {
        mAdapter = new SelectDifferentPaymentAdapter(mContext, mList, mCallBack, mListener, Constants.CHANGE);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    public void updateList(final List<CardDetail> cardDetails) {
        mBinding.progressBar.setVisibility(View.GONE);
        mBinding.recyclerView.setVisibility(View.VISIBLE);
        for (int i = 0; i <cardDetails.size() ; i++) {
            if(defaultPayment.getCardId() == cardDetails.get(i).getCardId()){
                cardDetails.remove(i);
            }
        }
        this.mList = cardDetails;
        mAdapter.setList(mList);
    }

    public void setListener(SelectNewPaymentListener mListener) {
        this.mListener = mListener;
    }

    public void setDefaultPayment(CardDetail cardDetail) {
     this.defaultPayment = cardDetail;
    }
}
