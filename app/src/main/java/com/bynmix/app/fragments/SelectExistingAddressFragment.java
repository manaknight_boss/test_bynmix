package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.SellerAddressAdapter;
import com.bynmix.app.databinding.MyAddressesFragmentBinding;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.SelectExistingAddressListener;
import com.bynmix.app.models.Address;
import com.bynmix.app.utils.Constants;

import java.util.List;

public class SelectExistingAddressFragment extends BaseFragment implements SelectExistingAddressListener {
    private MyAddressesFragmentBinding mBinding;
    private ProfileListener mCallBack;
    private Context mContext;
    private List<Address> mAddressesList;
    private Address shippingAddress;
    private Address returnAddress;
    private SellerAddressAdapter mAdapter;
    private SelectExistingAddressListener mListener;
    private boolean isShowAddAddressButton;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallBack = (ProfileListener) mContext;
    }

    public static SelectExistingAddressFragment newInstance() {
        return new SelectExistingAddressFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = MyAddressesFragmentBinding.inflate(inflater, container, false);
        setAdapter();
        if (isShowAddAddressButton) {
            mBinding.toolbarHeading.setText("Change Default Address");
        } else {
            mBinding.toolbarHeading.setText(getString(R.string.select_an_existing_address));
        }
        mBinding.progressBar.setVisibility(View.VISIBLE);
        getMyAddress();
        /*if (!isShowAddAddressButton) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
            getMyAddress();
        } else {
            if (mAddressesList.size() == 0) {
                mBinding.progressBar.setVisibility(View.VISIBLE);
                getMyAddress();
            } else {
                setData();
            }
        }*/
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.onBackPressed();
            }
        });

        mBinding.addNewAddressLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Address address = new Address();
                mCallBack.openAddNewAddressFragment(address, Constants.EXISTING_ADD_ADDRESS_FRAGMENT, SelectExistingAddressFragment.this);
            }
        });

        if (isShowAddAddressButton) {
            mBinding.addNewAddressLayout.setVisibility(View.VISIBLE);
        } else {
            mBinding.addNewAddressLayout.setVisibility(View.GONE);
        }

        return mBinding.getRoot();
    }

    private void setData() {
        if (mAddressesList.size() > 0) {
            mBinding.recyclerView.setVisibility(View.VISIBLE);
            for (int i = 0; i < mAddressesList.size(); i++) {
                if (mAddressesList.get(i).isShippingAddress()) {
                    shippingAddress = mAddressesList.get(i);
                }
                if (mAddressesList.get(i).isReturnAddress()) {
                    returnAddress = mAddressesList.get(i);
                }
                if (mAddressesList.get(i).isShippingAddress() && mAddressesList.get(i).isReturnAddress()) {
                    mAddressesList.remove(i);
                }
            }
            mAdapter.setList(mAddressesList, shippingAddress, returnAddress);
        } else {
            mBinding.recyclerView.setVisibility(View.GONE);
        }
        mBinding.progressBar.setVisibility(View.GONE);
    }

    private void setAdapter() {
        mAdapter = new SellerAddressAdapter(mContext, mAddressesList, shippingAddress, returnAddress, mCallBack, mListener);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    public void updateList(final List<Address> addresses) {
        this.mAddressesList = addresses;
    }

    public void setListener(SelectExistingAddressListener mListener) {
        this.mListener = mListener;
    }

    public void showAddAddressButton(boolean showAddAddressButton) {
        this.isShowAddAddressButton = showAddAddressButton;
    }

    public void updateAddressList(List<Address> addresses) {
        this.mAddressesList = addresses;
        setData();
    }

    public void addedAddress(Address address) {
        boolean isShippingAddress, isReturnAddress;
        isShippingAddress = address.isShippingAddress();
        isReturnAddress = address.isReturnAddress();
        if (isReturnAddress) {
            for (int i = 0; i < mAddressesList.size(); i++) {
                mAddressesList.get(i).setReturnAddress(false);
            }
        }
        if (isShippingAddress) {
            for (int i = 0; i < mAddressesList.size(); i++) {
                mAddressesList.get(i).setShippingAddress(false);
            }
        }
        this.mAddressesList.add(address);
        mAdapter.setList(mAddressesList, shippingAddress, returnAddress);
        setData();
    }

    @Override
    public void selectedAddress(Address address) {
        mAddressesList.add(address);
        mAdapter.setList(mAddressesList, shippingAddress, returnAddress);
    }
}
