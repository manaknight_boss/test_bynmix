package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.adapter.SellerPaymentAdapter;
import com.bynmix.app.databinding.FragmentSelectExistingPaymentBinding;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.SelectNewPaymentListener;
import com.bynmix.app.models.CardDetail;

import java.util.ArrayList;
import java.util.List;

public class SelectExistingPaymentFragment extends Fragment implements DialogClickListener {
    private FragmentSelectExistingPaymentBinding mBinding;
    private ProfileListener mCallBack;
    private Context mContext;
    private List<CardDetail> mList = new ArrayList<>();
    private CardDetail selectedPayment;
    private SellerPaymentAdapter mAdapter;
    private SelectNewPaymentListener mListener;
    private boolean isShowButton;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallBack = (ProfileListener) mContext;
    }

    public static SelectExistingPaymentFragment newInstance() {
        return new SelectExistingPaymentFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentSelectExistingPaymentBinding.inflate(inflater, container, false);
        setAdapter();
        if (mList.size() == 0) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
            mBinding.addPaymentLayout.setVisibility(View.GONE);
            mCallBack.getWalletDetail();
        } else {
            setData();
        }
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.onBackPressed();
            }
        });
        mBinding.addPaymentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.openAddPaymentFragment(0, SelectExistingPaymentFragment.this, 1);
            }
        });
        return mBinding.getRoot();
    }

    private void setData() {
        mBinding.progressBar.setVisibility(View.GONE);
        mBinding.mainLayout.setVisibility(View.VISIBLE);
        if (isShowButton) {
            mBinding.addPaymentLayout.setVisibility(View.VISIBLE);
        } else {
            mBinding.addPaymentLayout.setVisibility(View.GONE);
        }
        if (mList.size() > 0) {
            for (int i = 0; i < mList.size(); i++) {
                if (selectedPayment.getCardId() == mList.get(i).getCardId()) {
                    mList.remove(i);
                    break;
                }
            }
        }
        mAdapter.setList(mList, selectedPayment);
    }

    private void setAdapter() {
        mAdapter = new SellerPaymentAdapter(mContext, mList, selectedPayment, mCallBack, mListener);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    public void setListener(SelectNewPaymentListener mListener) {
        this.mListener = mListener;
    }


    @Override
    public void onClick() {
        mCallBack.getWalletDetail();
    }

    public void setList(List<CardDetail> cardDetailList) {
        this.mList = cardDetailList;
        setData();
    }

    public void updateSelectedCard(CardDetail cardDetail) {
        this.selectedPayment = cardDetail;
    }

    public void showPaymentButton(boolean isShowButton) {
        this.isShowButton = isShowButton;
    }
}
