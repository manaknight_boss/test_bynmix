package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.AddDotsListingAdapter;
import com.bynmix.app.databinding.AddDotsListingFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.AddTagsListener;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.models.Feeds;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectListingForDotsFragment extends BaseFragment {

    private AddDotsListingFragmentBinding mBinding;
    private AddTagsListener listener;
    private CreatePostListener mCallBack;
    private Context mContext;
    private List<FeedResponse> mList = new ArrayList<>();
    private AddDotsListingAdapter adapter;
    private String searchPattern;
    private boolean isAlreadyCalled;
    private int currentPageNumber;
    private int totalPages;
    private int position;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallBack = (CreatePostListener) context;
        mContext = context;
    }

    public static SelectListingForDotsFragment newInstance() {
        return new SelectListingForDotsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = AddDotsListingFragmentBinding.inflate(inflater, container, false);
        getListings();
        setAdapter();
        adapter.setProgress(false);
        mBinding.recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mCallBack.hideKeyboard(mContext);
                return false;
            }
        });
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallBack.onBackPressed();
            }
        });
        mBinding.autoComplete.requestFocus();
        mBinding.autoComplete.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable editable) {
                // TODO Auto-generated method stub

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newText = s.toString();
                if (newText.length() >= 1) {
                    mBinding.crossIcon.setVisibility(View.VISIBLE);
                    searchPattern = newText;
                } else {
                    searchPattern = null;
                    mBinding.crossIcon.setVisibility(View.GONE);
                }
                currentPageNumber = 0;
                totalPages = 0;
                getListings();
            }
        });

        mBinding.crossIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.crossIcon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mBinding.autoComplete.setText("");
                        searchPattern = null;
                        currentPageNumber = 0;
                        totalPages = 0;
                        getListings();
                    }
                });
            }
        });
        mBinding.recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                final LinearLayoutManager layoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();
                boolean endHasBeenReached = lastVisible >= totalItemCount - 4;
                if (totalItemCount > 0 && endHasBeenReached) {
                    if (!isAlreadyCalled && currentPageNumber < totalPages && mList.size() > 0) {
                        isAlreadyCalled = true;
                        getListings();
                    }
                }
            }
        });
        return mBinding.getRoot();
    }

    private void setAdapter() {
        adapter = new AddDotsListingAdapter(mContext, mList, listener, mCallBack, position);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(adapter);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
    }

    private void getListings() {
        if (currentPageNumber == 0) {
            mBinding.progressBar.setVisibility(View.VISIBLE);
        }
        getListingTitles(currentPageNumber, totalPages, searchPattern);
    }

    private void setLoadMoreProgress() {
        if (isAlreadyCalled) {
            adapter.setProgress(true);
        } else {
            adapter.setProgress(false);
        }
    }

    public void updateList(List<FeedResponse> list, int currentPageNumber, int totalPages) {
        this.currentPageNumber = currentPageNumber;
        this.totalPages = totalPages;
        if (currentPageNumber > 1) {
            this.mList.addAll(list);
        } else {
            this.mList = list;
        }
        isAlreadyCalled = false;
        setLoadMoreProgress();
        mBinding.progressBar.setVisibility(View.GONE);
        adapter.setList(mList);
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public void setListener(AddTagsListener dotsListener) {
        this.listener = dotsListener;
    }

    public void getListingTitles(final int currentPageNumber, final int totalPages, final String searchPattern) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getListingTitles("Bearer " + getAccessToken(), searchPattern, currentPageNumber + 1).enqueue(new Callback<ApiResponse<Feeds>>() {
            @Override
            public void onResponse(Call<ApiResponse<Feeds>> call, Response<ApiResponse<Feeds>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mBinding.progressBar.setVisibility(View.GONE);
                    Feeds feeds = response.body().getData();
                    int currentPage = feeds.getPageNumber();
                    int totalPage = feeds.getTotalPages();
                    List<FeedResponse> mList = feeds.getItems();
                    updateList(mList, currentPage, totalPage);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getListingTitles(currentPageNumber, totalPages, searchPattern);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Feeds>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }
}