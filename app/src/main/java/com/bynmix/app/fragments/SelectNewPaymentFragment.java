package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.adapter.MyWalletAdapter;
import com.bynmix.app.adapter.SelectNewPaymentAdapter;
import com.bynmix.app.databinding.SelectNewPaymentFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.SelectNewPaymentListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.CardDetail;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectNewPaymentFragment extends BaseFragment implements SelectNewPaymentListener {

    private SelectNewPaymentFragmentBinding mBinding;
    private Context mContext;
    private ProfileListener mCallback;
    private List<CardDetail> mCardDetailList = new ArrayList<>();
    private SelectNewPaymentAdapter mAdapter;
    private boolean isEnable = false;
    private int selectedId;
    private CardDetail cardDetail;
    private int position;

    public static SelectNewPaymentFragment newInstance() {

        return new SelectNewPaymentFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (ProfileListener) context;
        mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = SelectNewPaymentFragmentBinding.inflate(inflater, container, false);
        mBinding.replaceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setNewPayment(mContext, cardDetail.getCardId(), selectedId);
            }
        });

        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        setAdapter();
        updateUI();
        return mBinding.getRoot();
    }

    private void setAdapter() {
        mAdapter = new SelectNewPaymentAdapter(mContext, mCardDetailList, mCallback, this);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void selectedId(int id) {
        this.selectedId = id;
        isEnable = true;
        updateUI();
    }

    @Override
    public void selectedPayment(CardDetail cardDetail) {

    }

    private void updateUI() {
        if (isEnable) {
            mBinding.replaceButton.setEnabled(true);
            mBinding.replaceButton.setBackground(mContext.getResources().getDrawable(R.drawable.purple_rounded_button));
        } else {
            mBinding.replaceButton.setBackground(mContext.getResources().getDrawable(R.drawable.disable_dialog_button));
            mBinding.replaceButton.setEnabled(false);
        }
        if (mCardDetailList.size() == 0) {
            mBinding.nothingToShowPopUp.setVisibility(View.VISIBLE);
            mBinding.replaceButton.setVisibility(View.GONE);
        } else {
            mBinding.nothingToShowPopUp.setVisibility(View.GONE);
            mBinding.replaceButton.setVisibility(View.VISIBLE);
        }
    }

    public void setList(List<CardDetail> mList, CardDetail cardDetail, int position) {
        this.mCardDetailList = mList;
        this.cardDetail = cardDetail;
        this.position = position;
    }

    public void setNewPayment(final Context mContext, final int cardId, final int selectedId) {
        showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.setNewPayment("Bearer " + getAccessToken(), "" + cardId, "" + selectedId).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    mCallback.onBackPressed();

                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            setNewPayment(mContext, cardId, selectedId);
                        }

                        @Override
                        public void errorAccessToken() {
                            hideProgress();
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }
}
