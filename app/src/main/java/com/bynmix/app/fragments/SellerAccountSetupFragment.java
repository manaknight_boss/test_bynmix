package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.ahmadrosid.svgloader.SvgLoader;
import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.databinding.FragmentSellerAcountSetupBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.interfaces.SelectExistingAddressListener;
import com.bynmix.app.interfaces.SelectNewPaymentListener;
import com.bynmix.app.models.Address;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.CardDetail;
import com.bynmix.app.models.SellerPersonalInfo;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.DialogAlert;
import com.bynmix.app.utils.Utils;

import java.lang.reflect.Field;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellerAccountSetupFragment extends BaseFragment implements SelectNewPaymentListener, SelectExistingAddressListener {
    public static boolean isCallPaymentAPI;
    private FragmentSellerAcountSetupBinding mBinding;
    private Context mContext;
    private CreatePostListener mCallback;
    private Address selectedAddress;
    private CardDetail selectedPayment;
    private List<Address> mAddressList = new ArrayList<>();
    private List<CardDetail> mCardDetailList = new ArrayList<>();
    private List<String> monthList = new ArrayList<>();
    private List<String> dayList = new ArrayList<>();
    private List<String> yearList = new ArrayList<>();
    boolean isPersonalInfoSelect = false;
    private int month = 0;
    private int date = 0;
    private int year = 0;
    private String securityPin = null;
    private boolean isCallFirstTime = true;

    public static SellerAccountSetupFragment newInstance() {
        return new SellerAccountSetupFragment();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isCallPaymentAPI) {
            isCallPaymentAPI = false;
            mCallback.getWalletDetail();
        }
    }

    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentSellerAcountSetupBinding.inflate(inflater, container, false);
        setDateData();
        if (mAddressList.size() == 0) {
            getMyAddress();
        }
        if (mCardDetailList.size() == 0) {
            mCallback.getWalletDetail();
        }
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onBackPressed();
            }
        });
        if (isCallFirstTime) {
            if (selectedPayment == null || selectedAddress == null) {
                mBinding.progressBar.setVisibility(View.VISIBLE);
                mBinding.mainLayout.setVisibility(View.GONE);
                mCallback.getDefaultCard();
            } else {
                setDefaultValues();
            }
        } else {
            setDefaultValues();
        }
        setSpinners();

        mBinding.changeAddress.deleteAddress.setVisibility(View.GONE);
        mBinding.changePayment.deletePayment.setVisibility(View.GONE);

        mBinding.changeAddress.editAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedAddress != null) {
                    if (!isAlreadyAddressExistInList(selectedAddress)) {
                        mAddressList.add(selectedAddress);
                    }
                }
                mCallback.openSelectExistingAddressFragment(SellerAccountSetupFragment.this, mAddressList, true);
            }
        });

        mBinding.changePayment.editPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openSelectExistingPaymentFragment(SellerAccountSetupFragment.this, selectedPayment, true);
            }
        });

        mBinding.addAddressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Address address = new Address();
                mCallback.openAddNewAddressFragment(address, Constants.ADD_ADDRESS, null);
            }
        });

        mBinding.addPaymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openAddPaymentFragment(0, null, 0);
            }
        });

        mBinding.startSellingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendData();
            }
        });

        mBinding.lastFour.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = mBinding.lastFour.getText().toString();
                if (TextUtils.isEmpty(text) || text.length() < 4) {
                    securityPin = null;
                } else {
                    securityPin = text;
                }
                checkIfStep1Complete();
            }
        });

        return mBinding.getRoot();
    }

    private boolean isAlreadyAddressExistInList(Address selectedAddress) {
        boolean isAddressInList = false;
        for (int i = 0; i < mAddressList.size(); i++) {
            if (selectedAddress.getAddressId() == mAddressList.get(i).getAddressId()) {
                isAddressInList = true;
                break;
            }
        }
        return isAddressInList;
    }

    private void sendData() {
        SellerPersonalInfo sellerPersonalInfo = new SellerPersonalInfo();
        sellerPersonalInfo.setDobDay(date);
        sellerPersonalInfo.setDobMonth(month);
        sellerPersonalInfo.setDobYear(Integer.parseInt(yearList.get(year)));
        sellerPersonalInfo.setAddressId(selectedAddress.getAddressId());
        sellerPersonalInfo.setLastFourSocial(securityPin);
        callApi(sellerPersonalInfo);
    }

    private void callApi(final SellerPersonalInfo sellerPersonalInfo) {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.sendSellerDetail("Bearer " + ((HomeActivity) mContext).getAccessToken(), sellerPersonalInfo).enqueue(new Callback<ApiResponse<String>>() {
            @Override
            public void onResponse(final Call<ApiResponse<String>> call, Response<ApiResponse<String>> response) {
                mBinding.progressBar.setVisibility(View.GONE);
                if (response.isSuccessful() && (response.code() == 200 || response.code() == 201)) {
                    DialogAlert.showCongoDialog(mContext, "Awesome!", getString(R.string.you_are_ready_for_sale), "NOT RIGHT NOW", R.string.yes_list_now, new DialogClickListener() {
                        @Override
                        public void onClick() {
                            mCallback.openCreateListingFragment();
                        }
                    });
                    mCallback.popFragment();
                } else if (response.code() == 401) {
                    ((HomeActivity) mContext).getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            callApi(sellerPersonalInfo);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    DialogAlert.showCancelSaleErrorDialog(mContext, message, "");
                    mBinding.progressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<String>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                ((HomeActivity) mContext).mAlertDialog.showDialog((HomeActivity) mContext, getResources().getString(R.string.error_server));
            }
        });

    }

    private void setSpinners() {
        try {
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(mBinding.year);
            android.widget.ListPopupWindow popupWindow2 = (android.widget.ListPopupWindow) popup.get(mBinding.month);
            android.widget.ListPopupWindow popupWindow3 = (android.widget.ListPopupWindow) popup.get(mBinding.day);
            // Set popupWindow height to 500px
            int screenHeight = Utils.getScreenHeight(mContext);
            screenHeight = (int) (screenHeight * 0.5);
            popupWindow.setHeight(screenHeight);
            popupWindow2.setHeight(screenHeight);
            popupWindow3.setHeight(screenHeight);
        } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
        }
        ArrayAdapter<String> monthAdapter = new ArrayAdapter<String>(mContext, R.layout.profile_spinner_item, monthList);
        mBinding.month.setAdapter(monthAdapter);
        ArrayAdapter<String> yearAdapter = new ArrayAdapter<String>(mContext, R.layout.profile_spinner_item, yearList);
        mBinding.year.setAdapter(yearAdapter);
        ArrayAdapter<String> dayAdapter = new ArrayAdapter<String>(mContext, R.layout.profile_spinner_item, dayList);
        mBinding.day.setAdapter(dayAdapter);
        mBinding.month.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (view != null) {
                    if (position == 0) {
                        ((TextView) view).setTextColor(mContext.getResources().getColor(R.color.lightGrey));
                    } else {
                        ((TextView) view).setTextColor(mContext.getResources().getColor(R.color.category_dialog_color));
                    }
                }
                month = position;
                checkIfStep1Complete();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mBinding.year.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (view != null) {
                    if (position == 0) {
                        ((TextView) view).setTextColor(mContext.getResources().getColor(R.color.lightGrey));
                    } else {
                        ((TextView) view).setTextColor(mContext.getResources().getColor(R.color.category_dialog_color));
                    }
                }
                year = position;
                checkIfStep1Complete();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        mBinding.day.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (view != null) {
                    if (position == 0) {
                        ((TextView) view).setTextColor(mContext.getResources().getColor(R.color.lightGrey));
                    } else {
                        ((TextView) view).setTextColor(mContext.getResources().getColor(R.color.category_dialog_color));
                    }
                }
                date = position;
                checkIfStep1Complete();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void checkIfStep1Complete() {
        if (date != 0 && month != 0 && year != 0 && !TextUtils.isEmpty(securityPin)) {
            mBinding.step1Icon.setImageResource(R.mipmap.check_circle_01);
            isPersonalInfoSelect = true;
        } else {
            mBinding.step1Icon.setImageResource(R.mipmap.exclamation_mark_svgrepo_com);
            isPersonalInfoSelect = false;
        }
        updateUI();
    }

    private void setDateData() {
        if (yearList.size() == 0) {
            for (int i = 1920; i <= Calendar.getInstance().get(Calendar.YEAR) - 13; i++) {
                yearList.add("" + i);
            }
            Collections.reverse(yearList);
            yearList.add(0, "Year");
        }

        if (monthList.size() == 0) {
            String[] months = new DateFormatSymbols().getMonths();
            for (int i = 0; i < months.length; i++) {
                String month = months[i];
                System.out.println("month = " + month);
                monthList.add(months[i]);
            }
            monthList.add(0, "Month");
        }

        if (dayList.size() == 0) {
            for (int i = 1; i <= 31; i++) {
                dayList.add("" + i);
            }
            dayList.add(0, "Day");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        mCallback = (CreatePostListener) context;
    }

    public void update(CardDetail cardDetail, Address address) {
        this.selectedPayment = cardDetail;
        this.selectedAddress = address;
        setDefaultValues();
    }

    private void setDefaultValues() {
        mBinding.progressBar.setVisibility(View.GONE);
        mBinding.mainLayout.setVisibility(View.VISIBLE);
        if (selectedPayment != null) {
            mBinding.paymentIcon.setImageResource(R.mipmap.check_circle_01);
            mBinding.changePaymentLayout.setVisibility(View.VISIBLE);
            mBinding.addPaymentButton.setVisibility(View.GONE);
            String photoUrl = selectedPayment.getBrandImageUrl();
            if (!TextUtils.isEmpty(photoUrl)) {
                SvgLoader.pluck()
                        .with((HomeActivity) mContext)
                        .load(photoUrl, mBinding.changePayment.cardImage);
            }
            String cardName = "<b>" + selectedPayment.getBrand() + "</b> " + " ending in " + "<b>" + selectedPayment.getLastFour() + "<b>";
            mBinding.changePayment.cardHeading.setText(Html.fromHtml(cardName), TextView.BufferType.SPANNABLE);
            mBinding.changePayment.name.setText(selectedPayment.getNameOnCard());
            mBinding.changePayment.expiryDate.setText("Expires " + selectedPayment.getExpirationMonth() + "/" + selectedPayment.getExpirationYear());
        } else {
            mBinding.changePaymentLayout.setVisibility(View.GONE);
            mBinding.addPaymentButton.setVisibility(View.VISIBLE);
            mBinding.paymentIcon.setImageResource(R.mipmap.exclamation_mark_svgrepo_com);
        }
        if (selectedAddress != null) {
            mBinding.addressIcon.setImageResource(R.mipmap.check_circle_01);
            mBinding.changeAddressLayout.setVisibility(View.VISIBLE);
            mBinding.addAddressButton.setVisibility(View.GONE);
            mBinding.changeAddress.fullName.setText(selectedAddress.getFullName());
            if (!TextUtils.isEmpty(selectedAddress.getAddressOne())) {
                mBinding.changeAddress.addressOne.setVisibility(View.VISIBLE);
                mBinding.changeAddress.addressOne.setText(selectedAddress.getAddressOne());
            } else {
                mBinding.changeAddress.addressOne.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(selectedAddress.getAddressTwo())) {
                mBinding.changeAddress.addressTwo.setVisibility(View.VISIBLE);
                mBinding.changeAddress.addressTwo.setText(selectedAddress.getAddressTwo());
            } else {
                mBinding.changeAddress.addressTwo.setVisibility(View.GONE);
            }
            mBinding.changeAddress.zipcode.setText(selectedAddress.getCity() + " " + selectedAddress.getStateAbbreviation() + ", " + selectedAddress.getZipCode());
        } else {
            mBinding.changeAddressLayout.setVisibility(View.GONE);
            mBinding.addAddressButton.setVisibility(View.VISIBLE);
            mBinding.addressIcon.setImageResource(R.mipmap.exclamation_mark_svgrepo_com);
        }
        updateUI();
    }

    private void updateUI() {
        if (selectedAddress != null && selectedPayment != null && isPersonalInfoSelect) {
            mBinding.startSellingButton.setEnabled(true);
            mBinding.startSellingButton.setBackgroundColor(mContext.getResources().getColor(R.color.purple));
        } else {
            mBinding.startSellingButton.setEnabled(false);
            mBinding.startSellingButton.setBackgroundColor(mContext.getResources().getColor(R.color.disable_button));
        }

    }

    @Override
    public void selectedAddress(Address address) {
        isCallFirstTime = false;
        if (!isAlreadyAddressExistInList(address)) {
            mAddressList.add(address);
        }

        for (int i = 0; i < mAddressList.size(); i++) {
            if (address.getAddressId() == mAddressList.get(i).getAddressId()) {
                mAddressList.get(i).setShippingAddress(true);
                this.selectedAddress = mAddressList.get(i);
            } else {
                mAddressList.get(i).setShippingAddress(false);
            }
        }

        setDefaultValues();
    }

    @Override
    public void selectedId(int id) {
    }

    @Override
    public void selectedPayment(CardDetail cardDetail) {
        cardDetail.setDefault(true);
        if (selectedPayment == cardDetail) {
            mCardDetailList.add(selectedPayment);
        } else {
            selectedPayment.setDefault(false);
            mCardDetailList.add(selectedPayment);
        }
        this.selectedPayment = cardDetail;
        setDefaultValues();
    }

    public void updateAddressList(List<Address> addresses) {
        this.mAddressList = addresses;
    }

    public void updatePaymentList(List<CardDetail> cardDetailList) {
        this.mCardDetailList = cardDetailList;
    }
}
