package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.adapter.SellerPolicyAdapter;
import com.bynmix.app.databinding.FragmentSellerPolicyBinding;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Sections;
import com.bynmix.app.models.TermsOfServicesModel;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SellerPolicyFragment extends BaseFragment {

    private FragmentSellerPolicyBinding mBinding;
    private CreatePostListener mCallback;
    private Context mContext;
    private List<Sections> mList = new ArrayList<>();
    private SellerPolicyAdapter mAdapter;

    public static SellerPolicyFragment newInstance() {
        return new SellerPolicyFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mCallback = (CreatePostListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentSellerPolicyBinding.inflate(inflater, container, false);
        mBinding.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
        if (mList.size() == 0) {
            callApi();
        }
        setAdapter();
        return mBinding.getRoot();
    }

    private void setAdapter() {
        mAdapter = new SellerPolicyAdapter(mContext, mList);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(mAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        ((HomeActivity)mContext).hideBottomBar();
    }

    @Override
    public void onStop() {
        ((HomeActivity)mContext).showBottomBar();
        super.onStop();
    }

    private void callApi() {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getSellerPolicy("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<TermsOfServicesModel>>() {
            @Override
            public void onResponse(Call<ApiResponse<TermsOfServicesModel>> call, Response<ApiResponse<TermsOfServicesModel>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    TermsOfServicesModel termsOfServicesModel = response.body().getData();
                    List<Sections> mList = termsOfServicesModel.getSections();
                    mAdapter.setList(mList);
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    ((HomeActivity) mContext).mAlertDialog.showDialog(mContext, message);
                }
                mBinding.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ApiResponse<TermsOfServicesModel>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
            }
        });

    }
}
