package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.databinding.ShippingFragmentBinding;
import com.bynmix.app.interfaces.ConfirmInterface;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.models.ListingDisplayImage;
import com.bynmix.app.models.ShippingInfo;
import com.bynmix.app.utils.ConfirmDialog;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ShippingFragment extends BaseFragment {

    private ShippingFragmentBinding mBinding;
    private CreatePostListener mCallback;
    private Context mContext;
    private List<ShippingInfo> shippingInfoList = new ArrayList<>();
    private ListingDetail listingDetail;
    private List<ListingDisplayImage> mPhotoList;
    private int listingId;
    private int type;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (CreatePostListener) context;
        mContext = context;
    }

    public static ShippingFragment newInstance() {
        return new ShippingFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = ShippingFragmentBinding.inflate(inflater, container, false);
        getShippingInfo();
        if (type == Constants.EDIT) {
            mBinding.doneText.setText(getString(R.string.update_text));
            mBinding.toolbar.setVisibility(View.GONE);
        } else {
            mBinding.doneText.setText(getString(R.string.done_text));
            mBinding.toolbar.setVisibility(View.VISIBLE);
        }
        String hintMessage = Utils.getColorText("NOTE:", mContext.getResources().getColor(R.color.text_greyishBrown));
        mBinding.hint.setText(Html.fromHtml(hintMessage + mContext.getResources().getString(R.string.shipping_hint_text)), TextView.BufferType.SPANNABLE);

        updateButtonUI(mBinding.itemWeight.getText().toString().trim());
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
        mBinding.itemWeight.requestFocus();
        mBinding.itemWeight.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable editable) {
                // TODO Auto-generated method stub

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // TODO Auto-generated method stub

            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String newText = s.toString();
                if (newText.length() > 0) {
                    String weight = mBinding.itemWeight.getText().toString().trim();
                    try {
                        setWeight(Double.parseDouble(weight));
                    } catch (Exception ex) {
                        setWeight(0);
                    }
                } else {
                    updateUI(null, null);
                    if(listingDetail != null) {
                        listingDetail.setShippingInfo(null);
                    }
                }
                updateButtonUI(newText);
            }

        });

        mBinding.done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (type == Constants.EDIT) {
                    mCallback.putEditListing(listingId, listingDetail);
                } else {
                    mCallback.sendListingDetail(listingDetail, mPhotoList);
                }
            }
        });
        return mBinding.getRoot();
    }

    private void updateButtonUI(String newText) {
        if (TextUtils.isEmpty(newText)) {
            mBinding.done.setEnabled(false);
            mBinding.done.setBackground(mContext.getResources().getDrawable(R.drawable.disable_dialog_button));
        } else {
            mBinding.done.setEnabled(true);
            mBinding.done.setBackground(mContext.getResources().getDrawable(R.drawable.purple_rounded_button));
        }
    }

    private void setWeight(double weight) {

        if (weight <= shippingInfoList.get(0).getShippingWeightMax()) {
            listingDetail.setShippingWeight(weight);
            listingDetail.setShippingRateId(shippingInfoList.get(0).getShippingRateId());
            listingDetail.setShippingInfo(shippingInfoList.get(0));
            updateUI(mBinding.firstOption, mBinding.firstLayout);
        } else if (weight <= shippingInfoList.get(1).getShippingWeightMax()) {
            listingDetail.setShippingWeight(weight);
            listingDetail.setShippingRateId(shippingInfoList.get(1).getShippingRateId());
            listingDetail.setShippingInfo(shippingInfoList.get(1));
            updateUI(mBinding.secondOption, mBinding.secondLayout);
        } else if (weight <= shippingInfoList.get(2).getShippingWeightMax()) {
            listingDetail.setShippingWeight(weight);
            listingDetail.setShippingRateId(shippingInfoList.get(2).getShippingRateId());
            listingDetail.setShippingInfo(shippingInfoList.get(2));
            updateUI(mBinding.thirdOption, mBinding.thirdLayout);
        } else {
            listingDetail.setShippingWeight(weight);
            listingDetail.setShippingRateId(shippingInfoList.get(3).getShippingRateId());
            listingDetail.setShippingInfo(shippingInfoList.get(3));
            updateUI(mBinding.fourthOption, mBinding.fourthLayout);
        }
    }

    private String getColorText(String string) {
        String error = "<b>" + "<font color='" + mContext.getResources().getColor(R.color.purple) + "'>" + string + "</font>" + "</b> ";
        return error;
    }

    private void updateUI(RelativeLayout selectedView, RelativeLayout selectedLayout) {
        mBinding.firstOption.setBackgroundColor(mContext.getResources().getColor(R.color.colorPurchase_item));
        mBinding.secondOption.setBackgroundColor(mContext.getResources().getColor(R.color.colorPurchase_item));
        mBinding.thirdOption.setBackgroundColor(mContext.getResources().getColor(R.color.colorPurchase_item));
        mBinding.fourthOption.setBackgroundColor(mContext.getResources().getColor(R.color.colorPurchase_item));

        mBinding.firstLayout.setBackgroundColor(mContext.getResources().getColor(R.color.background_color));
        mBinding.secondLayout.setBackgroundColor(mContext.getResources().getColor(R.color.background_color));
        mBinding.thirdLayout.setBackgroundColor(mContext.getResources().getColor(R.color.background_color));
        mBinding.fourthLayout.setBackgroundColor(mContext.getResources().getColor(R.color.background_color));
        if (selectedView != null && selectedLayout != null) {
            selectedView.setBackgroundColor(mContext.getResources().getColor(R.color.white));
            selectedLayout.setBackgroundColor(mContext.getResources().getColor(R.color.purple));
        }
    }

    public void updateInfo(List<ShippingInfo> shippingInfoList) {
        this.shippingInfoList = shippingInfoList;
    }

    public void setListingData(ListingDetail listingDetail, List<ListingDisplayImage> photoUrlList) {
        this.listingDetail = listingDetail;
        this.mPhotoList = photoUrlList;
    }

    public void setData(int listingId, ListingDetail listingDetail) {
        this.listingDetail = listingDetail;
        this.listingId = listingId;
        mBinding.itemWeight.setText("" + listingDetail.getShippingWeight());
        setWeight(listingDetail.getShippingWeight());
    }

    public void setType(int type) {
        this.type = type;
    }

    public void askForDataLoss(final int id) {
        ConfirmDialog confirmDialog = new ConfirmDialog();
        confirmDialog.showDialog(mContext, new ConfirmInterface() {
            @Override
            public void confirm() {
                mCallback.clearListDetail();
                if(id != 0) {
                    ((HomeActivity)mContext).moveToTab(id);
                } else {
                    mCallback.popFragment();
                    mCallback.popFragment();
                    mCallback.popFragment();
                }
            }
        });
    }
}
