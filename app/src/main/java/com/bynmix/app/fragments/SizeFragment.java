package com.bynmix.app.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.adapter.SizeAdapter;
import com.bynmix.app.databinding.SizeFragmentBinding;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.SizeListener;
import com.bynmix.app.interfaces.SizeMainListener;
import com.bynmix.app.models.Size;
import com.bynmix.app.models.SizeType;

import java.util.ArrayList;
import java.util.List;

public class SizeFragment extends BaseFragment implements SizeListener {

    private SizeFragmentBinding mBinding;
    private CreatePostListener mCallback;
    private Context mContext;
    private List<SizeType> mList = new ArrayList<>();
    private SizeAdapter adapter;
    private String fragmentName;
    private SizeMainListener sizeMainListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (CreatePostListener) context;
        mContext = context;
    }

    public static SizeFragment newInstance() {
        return new SizeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = SizeFragmentBinding.inflate(inflater, container, false);
        if (!TextUtils.isEmpty(fragmentName)) {
            callList(fragmentName);
        }
        setRecyclerView();

        mBinding.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mList.size() > 0) {
                    resetSizeSelection();
                    SizeType sizeType = mList.get(0);
                    Size size = sizeType.getSizes().get(0);
                    size.setSelected(true);
//                    mCallback.setSizesInMainSizesFragment(sizeType, size);
                    mCallback.setSizeInListingDetail(size, sizeType, fragmentName);
                    mCallback.onBackPressed();
//                    updateUI(size);
                }
            }
        });
        return mBinding.getRoot();
    }

    private void updateUI(Size size) {
        Typeface typeface;
        if (size.isSelected()) {
            mBinding.sizeItemLayout.setBackground(mContext.getResources().getDrawable(R.drawable.purple_out_line_rectangle_background));
            typeface = ResourcesCompat.getFont(mContext, R.font.raleway_bold);
        } else {
            typeface = ResourcesCompat.getFont(mContext, R.font.raleway_regular);
            mBinding.sizeItemLayout.setBackground(mContext.getResources().getDrawable(R.drawable.color_layout_background));
        }
        mBinding.sizeItem.setTypeface(typeface);
    }

    private void setRecyclerView() {
        adapter = new SizeAdapter(mContext, mList, this);
        mBinding.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mBinding.recyclerView.setAdapter(adapter);
    }


    public void updateSizeList(List<SizeType> sizeTypeList, String sizeTitle) {
        this.mList = sizeTypeList;
        if (sizeTitle.equalsIgnoreCase("One Size")) {
            mBinding.recyclerView.setVisibility(View.GONE);
            mBinding.item.setVisibility(View.VISIBLE);
            mBinding.sizeItem.setText(mList.get(0).getSizes().get(0).getSizeName());
            mBinding.sizeText.setText(mList.get(0).getSizeTypeName());
            updateUI(mList.get(0).getSizes().get(0));
        } else {
            mBinding.recyclerView.setVisibility(View.VISIBLE);
            mBinding.item.setVisibility(View.GONE);
            adapter.setList(mList);
        }

    }


    public void callList(String sizeTitle) {
        if (mList.size() == 0) {
            sizeMainListener.getSizes(sizeTitle);
        } else {
            resetSizeSelection();
        }
    }

    public void addFragmentName(String fragmentName) {
        this.fragmentName = fragmentName;
    }

    @Override
    public void selectedSize(SizeType sizeType, Size size) {
//        mCallback.setSizesInMainSizesFragment(sizeType, size);
        mCallback.setSizeInListingDetail(size, sizeType, fragmentName);
        mCallback.onBackPressed();
    }

    @Override
    public void resetSizeSelection() {
        adapter.resetSizeSelection();
        adapter.notifyDataSetChanged();
    }

    public void setListener(SizeMainListener sizeMainListener) {
        this.sizeMainListener = sizeMainListener;
    }
}
