package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.adapter.FeedViewPagerAdapter;
import com.bynmix.app.databinding.SizeMainFragmentBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.ConfirmInterface;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.interfaces.SizeMainListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Size;
import com.bynmix.app.models.SizeType;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.ConfirmDialog;
import com.bynmix.app.utils.ErrorDialog;
import com.bynmix.app.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SizeMainFragment extends BaseFragment implements SizeMainListener {

    private SizeMainFragmentBinding mBinding;
    private CreatePostListener mCallback;
    private Context mContext;
    private List<String> mList = new ArrayList<>();
    private FeedViewPagerAdapter feedViewPagerAdapter;
    private Size size = null;
    private SizeType sizeType = null;
    private ErrorDialog errorDialog = new ErrorDialog();
    private String sizeHeader;
    private int sizeId;
    private int sizeTypeId;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallback = (CreatePostListener) context;
        mContext = context;
    }

    public static SizeMainFragment newInstance() {
        return new SizeMainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = SizeMainFragmentBinding.inflate(inflater, container, false);
        mBinding.toolbarHeading.setText(getString(R.string.size_fragment_heading));
        getSizesTypes();
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });

        mBinding.info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openSellerPolicyFragment();
            }
        });
        return mBinding.getRoot();
    }

    private void getSizesTypes() {
        mBinding.progressBar.setVisibility(View.VISIBLE);
        getSizeTitle();
    }

    public void updateSizeTitleList(List<String> sizeTitleList) {
        mBinding.progressBar.setVisibility(View.GONE);
        this.mList = sizeTitleList;
        setViewPager();
    }

    public void updateSizeList(List<SizeType> sizeTypeList, String sizeTitle) {
        for (int i = 0; i < mList.size(); i++) {
            if (mList.get(i).equalsIgnoreCase(sizeTitle)) {
                SizeFragment sizeFragment = (SizeFragment) feedViewPagerAdapter.getItem(i);
                for (int j = 0; j < sizeTypeList.size(); j++) {
                    if (sizeTypeId == sizeTypeList.get(j).getSizeTypeId()) {
                        sizeType = sizeTypeList.get(j);
                        for (int k = 0; k < sizeTypeList.get(j).getSizes().size(); k++) {
                            if (sizeId == sizeTypeList.get(j).getSizes().get(k).getSizeId()) {
                                sizeTypeList.get(j).getSizes().get(k).setSelected(true);
                                size = sizeTypeList.get(j).getSizes().get(k);
                            }
                        }
                    }
                }
                sizeFragment.updateSizeList(sizeTypeList, sizeTitle);
                break;
            }
        }
    }

    private void setViewPager() {
        if (feedViewPagerAdapter == null) {
            feedViewPagerAdapter = new FeedViewPagerAdapter(getChildFragmentManager());
            for (int i = 0; i < mList.size(); i++) {
                SizeFragment sizeFragment = new SizeFragment();
                String fragmentName = mList.get(i);
                feedViewPagerAdapter.addFragment(sizeFragment, fragmentName);
                sizeFragment.addFragmentName(mList.get(i));
                sizeFragment.setListener(this);
            }
        }
        mBinding.viewPager.setAdapter(feedViewPagerAdapter);
        mBinding.viewPager.setOffscreenPageLimit(mList.size());
        mBinding.tabs.setupWithViewPager(mBinding.viewPager);
        if (sizeHeader != null) {
            moveToSelectedFragment();
        }
        setLeftRightButtons(mList.size(), mBinding.tabs.getSelectedTabPosition());
        mBinding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                SizeFragment sizeFragment = (SizeFragment) feedViewPagerAdapter.getItem(position);
                sizeFragment.callList(mList.get(position));
                setLeftRightButtons(mList.size(), position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void moveToSelectedFragment() {
        for (int i = 0; i < mList.size(); i++) {
            if (sizeHeader.equalsIgnoreCase(mList.get(i))) {
                mBinding.viewPager.setCurrentItem(i);
                SizeFragment sizeFragment = (SizeFragment) feedViewPagerAdapter.getItem(i);
                sizeFragment.callList(mList.get(i));
            }
        }
    }

    public void updateSizes(SizeType sizeType, Size size) {
        if (size != null) {
            this.sizeType = sizeType;
            this.size = size;
        } else {
            this.sizeType = null;
            this.size = null;
        }
    }

    public void setSizeHeader(int sizeTypeId, int sizeId, String sizeHeader) {
        this.sizeHeader = sizeHeader;
        this.sizeId = sizeId;
        this.sizeTypeId = sizeTypeId;
    }

    private void setLeftRightButtons(int size, int position) {
        mBinding.nextButton.setVisibility(View.GONE);
        mBinding.previousButton.setVisibility(View.GONE);
        if (size > 1) {
            if (position == 0) {
                mBinding.nextButton.setVisibility(View.VISIBLE);
            } else if (position == size - 1) {
                mBinding.previousButton.setVisibility(View.VISIBLE);
            } else {
                mBinding.nextButton.setVisibility(View.VISIBLE);
                mBinding.previousButton.setVisibility(View.VISIBLE);
            }
        }
        mBinding.nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.viewPager.setCurrentItem(mBinding.viewPager.getCurrentItem() + 1);
            }
        });
        mBinding.previousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBinding.viewPager.setCurrentItem(mBinding.viewPager.getCurrentItem() - 1);
            }
        });
    }

    public void getSizeTitle() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getSizeType("Bearer " + getAccessToken()).enqueue(new Callback<ApiResponse<List<String>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<String>>> call, Response<ApiResponse<List<String>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    List<String> sizeTitleList = response.body().getData();
                    updateSizeTitleList(sizeTitleList);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getSizeTitle();
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<List<String>>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));

            }
        });
    }

    public void getSizes(final String sizeTitle) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getSize("Bearer " + getAccessToken(), sizeTitle).enqueue(new Callback<ApiResponse<List<SizeType>>>() {
            @Override
            public void onResponse(Call<ApiResponse<List<SizeType>>> call, Response<ApiResponse<List<SizeType>>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    List<SizeType> sizeType = response.body().getData();
                    updateSizeList(sizeType, sizeTitle);
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            getSizes(sizeTitle);
                        }

                        @Override
                        public void errorAccessToken() {

                        }
                    });
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    showAlertDialog(message);
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<List<SizeType>>> call, Throwable t) {
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));

            }
        });
    }

    public void askForDataLoss(final int id) {
        ConfirmDialog confirmDialog = new ConfirmDialog();
        confirmDialog.showDialog(mContext, new ConfirmInterface() {
            @Override
            public void confirm() {
                mCallback.clearListDetail();
                if(id != 0) {
                    ((HomeActivity)mContext).moveToTab(id);
                } else {
                    mCallback.popFragment();
                    mCallback.popFragment();
                    mCallback.popFragment();
                }
            }
        });
    }
}
