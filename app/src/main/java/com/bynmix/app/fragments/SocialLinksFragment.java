package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bynmix.app.databinding.FragmentSocialLinksBinding;
import com.bynmix.app.interfaces.EditProfileListener;
import com.bynmix.app.models.UserResponse;

public class SocialLinksFragment extends BaseFragment {

    private FragmentSocialLinksBinding mBinding;
    private UserResponse user;
    private EditProfileListener mCallback;
    private Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentSocialLinksBinding.inflate(inflater, container, false);
        setData();
        mBinding.save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendUserData();
            }
        });
        return mBinding.getRoot();

    }


    private void sendUserData() {
        String website = mBinding.website.getText().toString().trim();
        String facebook = mBinding.facebook.getText().toString().trim();
        String instagram = mBinding.instagram.getText().toString().trim();
        String pinterest = mBinding.pinterest.getText().toString().trim();
        String twitter = mBinding.twitter.getText().toString().trim();
        String googlePlus = mBinding.googlePlus.getText().toString().trim();
        if (website.isEmpty()) {
            website = "";
        } else if (!website.startsWith("http")) {
            website = "http://" + website;
        }

        if (facebook.isEmpty()) {
            facebook = "";
        } else if (!facebook.startsWith("http")) {
            facebook = "http://" + facebook;
        }

        if (instagram.isEmpty()) {
            instagram = "";
        } else if (!instagram.startsWith("http")) {
            instagram = "http://" + instagram;
        }

        if (pinterest.isEmpty()) {
            pinterest = "";
        } else if (!pinterest.startsWith("http")) {
            pinterest = "http://" + pinterest;
        }

        if (twitter.isEmpty()) {
            twitter = "";
        } else if (!twitter.startsWith("http")) {
            twitter = "http://" + twitter;
        }

        if (googlePlus.isEmpty()) {
            googlePlus = "";
        } else if (!googlePlus.startsWith("http")) {
            googlePlus = "http://" + googlePlus;
        }

        user.setBlogLink(website);
        user.setFacebookLink(facebook);
        user.setInstagramLink(instagram);
        user.setTwitterLink(twitter);
        user.setPinterestLink(pinterest);
        user.setGooglePlusLink(googlePlus);
        mCallback.onClick(user);
    }

    private void setData() {
        mBinding.website.setText(user.getBlogLink());
        mBinding.facebook.setText(user.getFacebookLink());
        mBinding.instagram.setText(user.getInstagramLink());
        mBinding.twitter.setText(user.getTwitterLink());
        mBinding.pinterest.setText(user.getPinterestLink());
        mBinding.googlePlus.setText(user.getGooglePlusLink());
    }

    public void setUser(UserResponse user) {
        this.user = user;
    }

    public void setListener(EditProfileListener listener, UserResponse user) {
        this.mCallback = listener;
        this.user = user;
    }
}
