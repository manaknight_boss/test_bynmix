package com.bynmix.app.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.databinding.FragmentWithdrawBinding;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.BankAccount;
import com.bynmix.app.models.Payout;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.utils.Constants;
import com.bynmix.app.utils.DialogAlert;
import com.bynmix.app.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WithdrawFragment extends BaseFragment {

    private FragmentWithdrawBinding mBinding;
    private ProfileListener mCallback;
    private Context mContext;
    private int currentBalance;
    private List<BankAccount> mList = new ArrayList<>();
    private List<String> bankAccountName = new ArrayList<>();
    int bankId = 0;

    public static WithdrawFragment newInstance() {
        return new WithdrawFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
        this.mCallback = (ProfileListener) context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = FragmentWithdrawBinding.inflate(inflater, container, false);
        mBinding.backIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
            }
        });
        mBinding.balance.setText("" + currentBalance + ".00");
        mBinding.withdrawButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addBankDetail();
            }
        });
        mBinding.addBankButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openAddBankDetailFragment(currentBalance);
            }
        });
        addName();
        setAdapter();
        return mBinding.getRoot();
    }

    private void addName() {
        for (int i = 0; i < mList.size(); i++) {
            bankAccountName.add(mList.get(i).getBankNickName() + " (" + mList.get(i).getLastFourNumbers() + ")");
        }
        bankAccountName.add(0, "Saved bank accounts");
    }

    private void setAdapter() {
        ArrayAdapter<String> bankAccountAdapter = new ArrayAdapter<String>(mContext, R.layout.profile_spinner_item, bankAccountName);
        mBinding.bankAccount.setAdapter(bankAccountAdapter);

        mBinding.bankAccount.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                if (view != null) {
                    if (position == 0) {
                        ((TextView) view).setTextColor(getResources().getColor(R.color.lightGrey));
                    } else {
                        ((TextView) view).setTextColor(getResources().getColor(R.color.colorPrimary));
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void addBankDetail() {
        int position = mBinding.bankAccount.getSelectedItemPosition();
        String withdrawAmount = mBinding.withdrawAmount.getText().toString().trim();
        if (position == 0) {
            Toast.makeText(mContext, "First Select Your Bank", Toast.LENGTH_SHORT).show();
            return;
        }
        bankId = mList.get(position - 1).getId();
        if (TextUtils.isEmpty(withdrawAmount)) {
            mBinding.withdrawAmount.setError("Enter Withdraw amount");
            return;
        }
        final double amount = Integer.parseInt(withdrawAmount);

        if (amount <= 0) {
            mBinding.withdrawAmount.setError("Invalid amount");
            return;
        }

        if (amount > currentBalance) {
            mBinding.withdrawAmount.setError(getString(R.string.error_insufficient_balance));
            return;
        }
        if (amount > Constants.MAX_WITHDRAW_LIMIT) {
            mBinding.withdrawAmount.setError(getString(R.string.error_max_withdraw_limit));
            return;
        }
        String title = "$" + Utils.formatValueToTwoDecimalPlaces(amount);
        DialogAlert.showWithDrawDialog(mContext, title, new DialogClickListener() {
            @Override
            public void onClick() {
                Payout payout = new Payout();
                payout.setWithdrawAmount(amount);
                callApi(payout);
            }
        });
    }

    private void callApi(final Payout payout) {
        showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.withdrawAmountWithId("Bearer " + getAccessToken(), "" + bankId, payout).enqueue(new Callback<ApiResponse<Payout>>() {
            @Override
            public void onResponse(Call<ApiResponse<Payout>> call, Response<ApiResponse<Payout>> response) {
                mBinding.progressBar.setVisibility(View.GONE);
                if (response.isSuccessful() && response.code() == 200 || response.code() == 201) {
                    Payout payouts = response.body().getData();
                    String lastFour = Utils.getColorText(payouts.getLastFour(), mContext.getResources().getColor(R.color.withdraw_pink));
                    String formattedDate = Utils.getColorText(payouts.getFormattedDate(), mContext.getResources().getColor(R.color.withdraw_pink));
                    String withdrawAmount = Utils.getColorText("$" + Utils.formatValueToTwoDecimalPlaces(payouts.getAmount()), mContext.getResources().getColor(R.color.withdraw_pink));
                    String descriptionMiddle = "An amount of " + withdrawAmount + " has been processed to bank account ending in " + lastFour + " and is expected to appear in your account on " + formattedDate;
                    String message = mContext.getString(R.string.happy_pay_day) + descriptionMiddle + mContext.getString(R.string.withdraw_success_msg);
                    DialogAlert.showSaleCancelSuccessfulDialog(mContext, mContext.getResources().getString(R.string.success), message, false);
                    mCallback.onBackPressed();
                    mCallback.onBackPressed();
                    mCallback.openTransactionHistoryScreen();
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            callApi(payout);
                        }

                        @Override
                        public void errorAccessToken() {
                            mBinding.progressBar.setVisibility(View.GONE);
                        }
                    });
                } else {
                    try {
                        JSONObject errorObject = new JSONObject(response.errorBody().string().trim());
                        JSONArray error = errorObject.getJSONArray("errors");
                        JSONObject messageObject = error.getJSONObject(0);
                        String message = messageObject.getString("message");
                        DialogAlert.showCancelSaleErrorDialog(mContext, message, getResources().getString(R.string.withdraw_error));
                    } catch (Exception e) {
                        e.printStackTrace();
                        showAlertDialog(getResources().getString(R.string.error_server));
                    }
                }
                hideProgress();
            }

            @Override
            public void onFailure(Call<ApiResponse<Payout>> call, Throwable t) {
                mBinding.progressBar.setVisibility(View.GONE);
                hideProgress();
                showAlertDialog(getResources().getString(R.string.error_server));
            }
        });
    }

    public void setCurrentBalance(int currentBalance) {
        this.currentBalance = Math.abs(currentBalance);
    }

    public void setList(List<BankAccount> payouts) {
        this.mList = payouts;
    }
}
