package com.bynmix.app.interfaces;


public interface AccessTokenResponse {

    void successAccessToken();

    void errorAccessToken();
}
