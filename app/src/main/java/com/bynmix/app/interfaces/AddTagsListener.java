package com.bynmix.app.interfaces;

import com.bynmix.app.models.FeedResponse;

public interface AddTagsListener {
    void changeSubmitButtonState(boolean enable);
    void onItemClick(FeedResponse feedResponse, int position);
}
