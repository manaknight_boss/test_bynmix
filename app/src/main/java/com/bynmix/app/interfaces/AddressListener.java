package com.bynmix.app.interfaces;

import com.bynmix.app.models.Address;

public interface AddressListener{

    void deleteAddress(Address address, int position);
}
