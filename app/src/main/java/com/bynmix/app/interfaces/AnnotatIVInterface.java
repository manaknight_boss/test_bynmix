package com.bynmix.app.interfaces;

import com.bynmix.app.models.AnnotatedPoint;

public interface AnnotatIVInterface {
    void add(AnnotatedPoint annotatedPoint);

    void update(AnnotatedPoint annotatedPoint);

    void remove(AnnotatedPoint annotatedPoint);

    void changeScrollStatus(boolean enable);
}
