package com.bynmix.app.interfaces;

import com.bynmix.app.models.Category;

import java.util.List;

public interface BaseFilterListener {

    void setBrandView();

    void setSelectedView(int id, String name, int type, int subType, List<Category> categories, List<Integer> idsList);

    void removeCheck(int type, int id, int subTypeId);
}
