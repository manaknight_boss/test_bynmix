package com.bynmix.app.interfaces;

import com.bynmix.app.models.UserResponse;

public interface BloggerListener {
    void onClickBlogger(UserResponse userResponse);

    void unSelectBlogger(UserResponse userResponse);
}
