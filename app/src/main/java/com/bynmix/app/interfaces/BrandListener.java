package com.bynmix.app.interfaces;

import com.bynmix.app.models.Brand;


public interface BrandListener {
    void onClickBrand(Brand brand);
}
