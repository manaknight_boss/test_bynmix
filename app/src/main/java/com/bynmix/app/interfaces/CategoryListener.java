package com.bynmix.app.interfaces;
import com.bynmix.app.models.ListingDetail;

public interface CategoryListener {

    void selectedCategory(ListingDetail listingDetail);
}

