package com.bynmix.app.interfaces;

import com.bynmix.app.models.Colors;

import java.util.List;

public interface ColorListener {

    void addColor(List<Colors> colorsList);
}
