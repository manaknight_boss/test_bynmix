package com.bynmix.app.interfaces;

public interface CommentListener {
    void addComment(int id, String comment);
}
