package com.bynmix.app.interfaces;

import com.bynmix.app.models.Address;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.models.Brand;
import com.bynmix.app.models.CardDetail;
import com.bynmix.app.models.Category;
import com.bynmix.app.models.CategoryType;
import com.bynmix.app.models.Colors;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.models.HeightModel;
import com.bynmix.app.models.ListingCheck;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.models.ListingDisplayImage;
import com.bynmix.app.models.SelectedFilter;
import com.bynmix.app.models.Size;
import com.bynmix.app.models.SizeType;
import com.bynmix.app.utils.SavingPostDialog;

import java.util.List;

public interface CreatePostListener extends ProfileListener {

    void sendPostDetail(FeedResponse post);

    void editPostDetail(FeedResponse post, String photoUrl, String oldPhotoUrl);

    void openCategoryFragment(int categoryId, int categoryTypeId);

    void openColorFragment(List<Integer> colorIdsList);

    void openSizeFragment(int sizeTypeId, int sizeId, String sizeHeader);

    void openBrandFragment(int brandId);

    void getCategories();

    void getColors();

    void getBrands();

    void setCategoryInListingDetail(Category category, CategoryType categoryType);

    void setSizeInListingDetail(Size size, SizeType sizeType, String sizeHeader);

    void setColorInListingDetail(List<Colors> colorsList);

    void setBrandInListingDetail(Brand brand);

    void sendListingDetail(ListingDetail listingDetail, List<ListingDisplayImage> photoUrlList);

    ListingDetail listingDetail();

    void clearListDetail();

    void openShippingFragment(ListingDetail listingDetail, List<ListingDisplayImage> photoUrlList);

    void setSizesInMainSizesFragment(SizeType sizeType, Size size);

    void updateListingImages(int position);

    void setFilterData(List<SelectedFilter> mSelectedFilterList, int fragmentType);

    List<SelectedFilter> getSelectedFilterList();

    List<SelectedFilter> getSelectedUserFilterList();


    void popFragment();

    void setAllCategoryList(List<Category> allCategoryList);

    void putEditListing(final int listingId, final ListingDetail listingDetail);

    void addDeletedImagesId(int listingImageId);

    void openOfferCompletionFragment(ListingDetail listingDetail, int type);

    void getDefaultCard();

    void openSelectDifferentPaymentFragment(CardDetail selectedPayment, SelectNewPaymentListener mCallBack);

    void openSelectDifferentAddressFragment(Address selectedAddress, SelectExistingAddressListener mCallBack);

    void getDefaultReturnAddress(CardDetail cardDetail);

    void openCheckListingDialog(ListingCheck status);

    void openListingOfferFragment(int listingId);

    void clearListingImagesList();

    void deletedListingImagesIdClear();

    String getPostImage();

    boolean isEditPost();

    List<AnnotatedPoint> getAnnotatedPoints();

    void setAnnotatedPoints(List<AnnotatedPoint> points);

    List<AnnotatedPoint> getDeletedDots();

    void setDeletedDots(List<AnnotatedPoint> points);

    void addDeletedDots(AnnotatedPoint point);

    void dotsDetailFragment(FeedResponse post);

    void openSelectListingForDotsFragment(int position, AddTagsListener dotsListener);

    void setUserFilterData(List<SelectedFilter> mSelectedFilterList, HeightModel heightModel);

    HeightModel getHeightModel();

    void setHeightModel(HeightModel mSelectedHeightModel);

    void openCreateListingFragment();

    void openSellerAccountSetupFragment();

    void openSelectExistingPaymentFragment(SelectNewPaymentListener selectNewPaymentListener, CardDetail mCardDetail, boolean b);

    void sendReportAnIssueImages(int id, List<String> mImageList);

    void openNotificationFragment();

    void updateListingImages(int listingId, List<ListingDisplayImage> mPhotoList);

    void openSellerPolicyFragment();

    void openReportAnIssueListingFragment(ListingDetail listingDetail);

    void clearAllFragment();
}
