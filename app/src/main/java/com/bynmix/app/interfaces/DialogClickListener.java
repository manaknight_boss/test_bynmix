package com.bynmix.app.interfaces;

public interface DialogClickListener {
    void onClick();
}
