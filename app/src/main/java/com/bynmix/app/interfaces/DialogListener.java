package com.bynmix.app.interfaces;

public interface DialogListener {

    void openCreateAccountDialog();
    void openCreateAccountWithEmailDialog();
}
