package com.bynmix.app.interfaces;

import com.bynmix.app.models.AnnotatedPoint;

public interface DotsSingleTabInterface {
    void singleTab(AnnotatedPoint annotatedPoint);
    void singleTab();
}
