package com.bynmix.app.interfaces;

import com.bynmix.app.models.ListingDetail;

public interface EditListingListener {

    void onClick(ListingDetail listingDetail);
}
