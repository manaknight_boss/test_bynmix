package com.bynmix.app.interfaces;

public interface EditPostListener {

    void disablePost(final int id, final int position, final int type);
    void deletePost(final int id, final int position);
}
