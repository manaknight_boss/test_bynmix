package com.bynmix.app.interfaces;

import com.bynmix.app.models.UserResponse;

public interface EditProfileListener {
    void onClick(UserResponse user);
}
