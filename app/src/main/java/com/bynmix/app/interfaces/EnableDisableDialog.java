package com.bynmix.app.interfaces;

public interface EnableDisableDialog {
    void onClickListener();
    void onDeleteListener();
}
