package com.bynmix.app.interfaces;

import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.models.UserResponse;

import java.util.List;


public interface FeedItemClickListener extends NetworkListener, DialogListener, CommentListener {

    List<FeedResponse> getList();

    List<UserResponse> getBestUserList();

    List<UserResponse> getFollowingList();

    void onListingLike(int id);

    void onPostsLike(int id);

    void openProfile(int userId);

    void openFollowFollowingFragment(int type, int userId, String userName);

    void openBestUserFragment(int tabPosition);

    void onFollowerUser(int userId);

    void openListingDetailFragment(int id);

    void openInAppBrowser(String url, String title);

    void getListingDetail(int id);

    void getListingDetailImages(int id);

    void openLikeFragment(int id, int type);

    void openFilterFragment(int type);

    int getFilterFragmentType();

    void acceptOffer(int listingId, int offerId, boolean isLoggedInUserOffer);

    void declineOffer(int listingId, int offerId, int type);

    void openMakeAnOfferFragment(int listingId, int offerId, int buyerId);

    void getPostTags(int position, int id, int feedType);

    void getPostTags(int id);

    void openPostDetailFragment(FeedResponse feed);

    void openSearchUserFragment();
}
