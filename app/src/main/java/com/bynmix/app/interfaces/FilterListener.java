package com.bynmix.app.interfaces;

import com.bynmix.app.models.Category;
import com.bynmix.app.models.CategoryType;
import com.bynmix.app.models.FilterSizeType;
import com.bynmix.app.models.FilterSizes;
import com.zhihu.matisse.filter.Filter;

import java.util.List;

public interface FilterListener extends BaseFilterListener{

    void setConditionView();

    void setSizeView();

    void setColorView();

    void setCategoryView();

    void setPriceView();

    void openCategoryView(CategoryType categoryType);

    void openSizeFitView(FilterSizeType filterSizeType);

    void setAllCategoryList(List<Category> allCategoryList);

}
