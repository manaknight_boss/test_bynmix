package com.bynmix.app.interfaces;


public interface FilterUserListener extends BaseFilterListener {
    void setStylesView();

    void setBodyTypeView();

    void setHeightView();
}
