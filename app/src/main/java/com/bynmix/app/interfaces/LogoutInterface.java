package com.bynmix.app.interfaces;

public interface LogoutInterface {
    void logoutId();
    void clearFilterList();
}
