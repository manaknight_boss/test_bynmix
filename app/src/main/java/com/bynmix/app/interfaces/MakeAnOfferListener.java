package com.bynmix.app.interfaces;

public interface MakeAnOfferListener {
    void submitOffer(String offer);
}
