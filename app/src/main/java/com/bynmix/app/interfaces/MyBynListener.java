package com.bynmix.app.interfaces;

public interface MyBynListener {

    void disableListing(final int id, final int position, final int type);

    void deleteListing(final int id, final int position);
}
