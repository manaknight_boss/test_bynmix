package com.bynmix.app.interfaces;

import android.content.Context;

import com.bynmix.app.models.CardDetail;

import java.util.List;

public interface MyWalletListener {

    void checkCard(Context mContext, CardDetail cardDetail, ProfileListener mCallBack, int position, List<CardDetail> mList);

    void setAsDefaultCard(final int cardId);

    void getPayouts(int totalAvailableForWithdrawal);

    void deleteCardDetail(CardDetail cardDetail, int position);
}
