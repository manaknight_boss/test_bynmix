package com.bynmix.app.interfaces;


public interface NetworkListener extends translateString, OnBackPressedListener {
    boolean conditionsForApiCall();
}
