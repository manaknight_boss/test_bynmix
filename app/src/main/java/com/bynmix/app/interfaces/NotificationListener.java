package com.bynmix.app.interfaces;

import com.bynmix.app.models.Settings;

public interface NotificationListener {

    void getNotifications(int currentpage);

    void decreaseCount(Settings settings);

    void getMyOffersNotifications(int currentPageNumber);

    void getReceivedOffersNotifications(int currentPageNumber);
}
