package com.bynmix.app.interfaces;

import com.bynmix.app.models.NotificationSettings;

import java.util.List;

public interface NotificationSettingListener {

    void updateNotificationSettings(List<NotificationSettings> settings);
}
