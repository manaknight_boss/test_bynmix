package com.bynmix.app.interfaces;

import com.bynmix.app.models.SubOfferHistory;

public interface OfferHistoryListener {
    void cancelOffer(SubOfferHistory offerHistory);
}
