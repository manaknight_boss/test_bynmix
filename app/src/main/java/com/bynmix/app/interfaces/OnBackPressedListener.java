package com.bynmix.app.interfaces;

import android.content.Context;

public interface OnBackPressedListener {
    void onBackPressed();
    void hideKeyboard(Context context);
}
