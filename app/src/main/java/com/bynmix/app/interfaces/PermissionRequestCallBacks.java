package com.bynmix.app.interfaces;

public interface PermissionRequestCallBacks {
    void onPermissionsGranted();

    void onPermissionsDenied();
}
