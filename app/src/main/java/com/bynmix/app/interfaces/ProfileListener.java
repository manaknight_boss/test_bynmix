package com.bynmix.app.interfaces;

import com.bynmix.app.models.Address;
import com.bynmix.app.models.BankAccount;
import com.bynmix.app.models.CardDetail;
import com.bynmix.app.models.CategoryType;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.models.ListingDisplayImage;
import com.bynmix.app.models.MyPurchasesStats;
import com.bynmix.app.models.MySalesStats;
import com.bynmix.app.models.Stock;
import com.bynmix.app.models.UserFilterData;
import com.bynmix.app.models.UserResponse;

import java.util.List;

public interface ProfileListener extends FeedItemClickListener {

    void openEditProfileFragment(UserResponse user);

    void openGalleryFor(int constant, int imageCount);

    void logout();

    void openMyPostFragment();

    void openMyLikeFragment(String username, int userId, int type);

    void openMyBynFragment();

    void openAddListingDetailFragment(List<ListingDisplayImage> photoList);

    void setCategory(ListingDetail listingDetail);

    void showCategoryTick(CategoryType categoryType);

    void clearListingDetails();

    void openEditMyPostFragment(FeedResponse post);

    void openAddNewAddressFragment(Address address, int type, SelectExistingAddressListener mListener);

    void openMyAddressFragment();

    void openAddAddressFragment();

    void editAddress(Address address);

    void openWallet();

    void openEditMyListingFragment(int id);

    void openAddPaymentFragment(int cardId, DialogClickListener mListener, int type);

    void getWalletDetail();

    void openSelectExistingAddressFragment(SelectExistingAddressListener mCallBack, List<Address> mAddressList, boolean b);

    void openSelectNewPaymentFragment(List<CardDetail> mList, CardDetail cardDetail, int position);

    void openNotificationSettingsFragment();

    void openPurchasesFragment(int type);

    void sendRating(Stock detail, int position);

    void openPurchaseDetailFragment(int id, int type);

    MySalesStats mySaleState();

    MyPurchasesStats myPurchaseState();

    void openAddPostDetailFragment();

    void openCancelSaleFragment(Stock detail);

    void openEditPostDotsFragment(FeedResponse post);

    void openReviewFragment(int userId, String userName);

    void openFilterUserFragment();

    UserFilterData getUserFilterData();

    void openAddBankDetailFragment(int currentBalance);

    void openReportAnIssueFragment(Stock detail);

    void checkReportAnIssue(Stock stock);

    void openDisputeDetailFragment(int dispute, Stock detail);

    void openTransactionHistoryScreen();

    void updateSaleStats(MySalesStats myStats);

    void updatePurchaseStats(MyPurchasesStats myStats);

    void getNotificationCount();

    void openPostDetailFragmentWithId(int postId);

    void getTrackInformation(Stock detail);

    void openWithDrawFragment(int currentBalance, List<BankAccount> payouts);

    void openSearchUserFragment();
}
