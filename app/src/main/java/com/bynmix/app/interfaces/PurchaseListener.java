package com.bynmix.app.interfaces;


public interface PurchaseListener {

    void cancelPurchase(int id);

    void getMyPurchases(int currentPage);

    void getMySales(int currentPageNumber);

    void getMySalesStats();

    void getMyPurchasesStats();

    void relistItem(final int listingId);
}
