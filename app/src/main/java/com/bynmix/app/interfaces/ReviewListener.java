package com.bynmix.app.interfaces;

public interface ReviewListener {
    void fetchSellerReviews(final int userId, final int currentPageNumber);

    void fetchBuyerReviews(final int userId, final int currentPageNumber);
}
