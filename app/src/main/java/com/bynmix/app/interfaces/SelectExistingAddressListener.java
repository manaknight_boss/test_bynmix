package com.bynmix.app.interfaces;

import com.bynmix.app.models.Address;

public interface SelectExistingAddressListener {
    void selectedAddress(Address address);
}
