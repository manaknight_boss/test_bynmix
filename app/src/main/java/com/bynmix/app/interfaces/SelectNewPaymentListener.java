package com.bynmix.app.interfaces;

import com.bynmix.app.models.CardDetail;

public interface SelectNewPaymentListener {

    void selectedId(int id);
    void selectedPayment(CardDetail cardDetail);
}
