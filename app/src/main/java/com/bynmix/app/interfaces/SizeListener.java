package com.bynmix.app.interfaces;

import com.bynmix.app.models.Size;
import com.bynmix.app.models.SizeType;

public interface SizeListener {

    void selectedSize(SizeType sizeType, Size size);
    void resetSizeSelection();
}
