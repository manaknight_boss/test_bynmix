package com.bynmix.app.interfaces;

public interface SizeMainListener {

    void getSizes(final String sizeTitle);
}
