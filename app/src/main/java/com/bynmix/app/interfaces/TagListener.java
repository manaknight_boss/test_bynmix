package com.bynmix.app.interfaces;

import com.bynmix.app.models.Descriptor;

public interface TagListener {

    void onClickTag(Descriptor tag);
}
