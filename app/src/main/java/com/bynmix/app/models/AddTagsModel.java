package com.bynmix.app.models;

public class AddTagsModel {

    private String heading;
    private String title;
    private int image;
    private String buttonText;
    private boolean hasVideo;


    public AddTagsModel(String heading, String title, String buttonText, int image, boolean hasVideo) {
        this.heading = heading;
        this.title = title;
        this.image = image;
        this.buttonText = buttonText;
        this.hasVideo  = hasVideo;
    }
    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public boolean isHasVideo() {
        return hasVideo;
    }

    public void setHasVideo(boolean hasVideo) {
        this.hasVideo = hasVideo;
    }
}
