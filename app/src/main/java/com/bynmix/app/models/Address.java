package com.bynmix.app.models;

public class Address {
    private int addressId;
    private String fullName;
    private String addressOne;
    private String addressTwo;
    private String state;
    private String stateAbbreviation;
    private String city;
    private String zipCode;
    private String country;
    private boolean isShippingAddress;
    private boolean isReturnAddress;
    private boolean isVerified;
    private boolean isDeleted;
    private int userId;
    private String username;
    private String userImage;
    private boolean isExistingAddress = false;
    private boolean isSelectedCheckBox;
    private boolean isDefaultAddress;
    private String shippingStatusDetails;
    private String shippingStatus;
    private String deliveryDateFormatted;

    public boolean isSelectedCheckBox() {
        return isSelectedCheckBox;
    }

    public void setSelectedCheckBox(boolean selectedCheckBox) {
        isSelectedCheckBox = selectedCheckBox;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddressOne() {
        return addressOne;
    }

    public void setAddressOne(String addressOne) {
        this.addressOne = addressOne;
    }

    public String getAddressTwo() {
        return addressTwo;
    }

    public void setAddressTwo(String addressTwo) {
        this.addressTwo = addressTwo;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStateAbbreviation() {
        return stateAbbreviation;
    }

    public void setStateAbbreviation(String stateAbbreviation) {
        this.stateAbbreviation = stateAbbreviation;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public boolean isShippingAddress() {
        return isShippingAddress;
    }

    public void setShippingAddress(boolean shippingAddress) {
        isShippingAddress = shippingAddress;
    }

    public boolean isReturnAddress() {
        return isReturnAddress;
    }

    public void setReturnAddress(boolean returnAddress) {
        isReturnAddress = returnAddress;
    }

    public boolean isVerified() {
        return isVerified;
    }

    public void setVerified(boolean verified) {
        isVerified = verified;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public boolean isExistingAddress() {
        return isExistingAddress;
    }

    public void setExistingAddress(boolean existingAddress) {
        isExistingAddress = existingAddress;
    }

    public String getShippingStatusDetails() {
        return shippingStatusDetails;
    }

    public void setShippingStatusDetails(String shippingStatusDetails) {
        this.shippingStatusDetails = shippingStatusDetails;
    }

    public String getShippingStatus() {
        return shippingStatus;
    }

    public void setShippingStatus(String shippingStatus) {
        this.shippingStatus = shippingStatus;
    }

    public boolean isDefaultAddress() {
        return isDefaultAddress;
    }

    public void setDefaultAddress(boolean defaultAddress) {
        isDefaultAddress = defaultAddress;
    }

    public String getDeliveryDateFormatted() {
        return deliveryDateFormatted;
    }
}
