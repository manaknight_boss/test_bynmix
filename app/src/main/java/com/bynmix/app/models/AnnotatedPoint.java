package com.bynmix.app.models;

import android.os.Build;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Objects;

public class AnnotatedPoint implements Serializable {
    public int id;
    @SerializedName("xCoordinate")
    public float x;
    @SerializedName("yCoordinate")
    public float y;
    @SerializedName("hexColor")
    public int color;
    private String postDotType;
    private String title;
    private String link;
    private int listingId;
    private int postId;
    private int postDotTypeId;
    private int postDotId;
    private boolean isDeleted;
    private String listingTitle;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AnnotatedPoint that = (AnnotatedPoint) o;
        return id == that.id && color == that.color &&
                Float.compare(that.x, x) == 0 &&
                Float.compare(that.y, y) == 0;
    }

    @Override
    public int hashCode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return Objects.hash(id, x, y, color);
        }
        return 0;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getListingId() {
        return listingId;
    }

    public void setListingId(int listingId) {
        this.listingId = listingId;
    }

    public String getPostDotType() {
        return postDotType;
    }

    public void setPostDotType(String postDotType) {
        this.postDotType = postDotType;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getPostDotTypeId() {
        return postDotTypeId;
    }

    public void setPostDotTypeId(int postDotTypeId) {
        this.postDotTypeId = postDotTypeId;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getPostDotId() {
        return postDotId;
    }

    public void setPostDotId(int postDotId) {
        this.postDotId = postDotId;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public String getListingTitle() {
        return listingTitle;
    }

    public void setListingTitle(String listingTitle) {
        this.listingTitle = listingTitle;
    }
}
