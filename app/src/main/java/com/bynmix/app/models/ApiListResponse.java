package com.bynmix.app.models;

import java.util.List;


public class ApiListResponse<T> {
    private Integer apiVersion;
    private T data;
    private List<Object> errors = null;
    private Boolean isError;
    private Integer statusCode;

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("apiVersion ").append(apiVersion).append("\n data\n ");
        result.append(" errors ").append(errors).append(" isError ").append(isError).append(" statusCode ").append(statusCode);

        return result.toString();
    }
}