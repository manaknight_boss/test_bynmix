package com.bynmix.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;



public class ApiResponse<T> {

    private Integer apiVersion;
    private T data;
    private List<Object> errors = null;
    private Boolean isError;
    private Integer statusCode;

    public Integer getApiVersion() {
        return apiVersion;
    }

    public void setApiVersion(Integer apiVersion) {
        this.apiVersion = apiVersion;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<Object> getErrors() {
        return errors;
    }

    public void setErrors(List<Object> errors) {
        this.errors = errors;
    }

    public Boolean getIsError() {
        return isError;
    }

    public void setIsError(Boolean isError) {
        this.isError = isError;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    @Override
    public String toString() {
        return new StringBuilder("apiVersion ").append(apiVersion).append(" data ").append(data)
                .append(" errors ").append(errors).append(" isError ").append(isError).append(" statusCode ")
                .append(statusCode).toString();
    }
}