package com.bynmix.app.models;

public class Availability {

    private int availabilityId;
    private String availabilityName;

    public int getAvailabilityId() {
        return availabilityId;
    }

    public void setAvailabilityId(int availabilityId) {
        this.availabilityId = availabilityId;
    }

    public String getAvailabilityName() {
        return availabilityName;
    }

    public void setAvailabilityName(String availabilityName) {
        this.availabilityName = availabilityName;
    }
}
