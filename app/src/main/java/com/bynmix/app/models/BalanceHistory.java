package com.bynmix.app.models;

import java.util.List;

public class BalanceHistory extends PageResponse{
    private int amount;
    private String arrivalDateFormatted;
    private String bankAccountNickName;
    private String lastFour;
    private String status;
    private int statusId;
    private String formattedDisplayText;
    private List<BalanceHistory> items;

    public static int TYPE_PENDING = 1;
    public static int IN_TRANSIT = 2;
    public static int TYPE_PAID = 3;
    public static int TYPE_CANCELED = 4;
    public static int TYPE_FAILED = 5;

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getArrivalDateFormatted() {
        return arrivalDateFormatted;
    }

    public void setArrivalDateFormatted(String arrivalDateFormatted) {
        this.arrivalDateFormatted = arrivalDateFormatted;
    }

    public String getBankAccountNickName() {
        return bankAccountNickName;
    }

    public void setBankAccountNickName(String bankAccountNickName) {
        this.bankAccountNickName = bankAccountNickName;
    }

    public String getLastFour() {
        return lastFour;
    }

    public void setLastFour(String lastFour) {
        this.lastFour = lastFour;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getFormattedDisplayText() {
        return formattedDisplayText;
    }

    public void setFormattedDisplayText(String formattedDisplayText) {
        this.formattedDisplayText = formattedDisplayText;
    }

    public List<BalanceHistory> getItems() {
        return items;
    }

    public void setItems(List<BalanceHistory> items) {
        this.items = items;
    }
}
