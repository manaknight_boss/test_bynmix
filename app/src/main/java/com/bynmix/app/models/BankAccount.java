package com.bynmix.app.models;

public class BankAccount {

    private int id;
    private String bankNickName;
    private String lastFourNumbers;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBankNickName() {
        return bankNickName;
    }

    public void setBankNickName(String bankNickName) {
        this.bankNickName = bankNickName;
    }

    public String getLastFourNumbers() {
        return lastFourNumbers;
    }

    public void setLastFourNumbers(String lastFourNumbers) {
        this.lastFourNumbers = lastFourNumbers;
    }
}
