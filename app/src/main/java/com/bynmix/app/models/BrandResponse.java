package com.bynmix.app.models;

import java.io.Serializable;
import java.util.List;

public class BrandResponse extends PageResponse implements Serializable {

    private List<Brand> items;

    public List<Brand> getItems() {
        return items;
    }

    public void setItems(List<Brand> items) {
        this.items = items;
    }
}
