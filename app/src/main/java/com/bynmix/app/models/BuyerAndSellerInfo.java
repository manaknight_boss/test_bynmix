package com.bynmix.app.models;

public class BuyerAndSellerInfo {
    private int buyerId;
    private String buyerUsername;
    private String buyerImageUrl;
    private int sellerId;
    private String sellerUsername;
    private String sellerImageUrl;
    private boolean loggedInUserFollowingSeller;

    public boolean isLoggedInUserFollowingSeller() {
        return loggedInUserFollowingSeller;
    }

    public void setLoggedInUserFollowingSeller(boolean loggedInUserFollowingSeller) {
        this.loggedInUserFollowingSeller = loggedInUserFollowingSeller;
    }

    public boolean isLoggedInUserFollowingBuyer() {
        return loggedInUserFollowingBuyer;
    }

    public void setLoggedInUserFollowingBuyer(boolean loggedInUserFollowingBuyer) {
        this.loggedInUserFollowingBuyer = loggedInUserFollowingBuyer;
    }

    private boolean loggedInUserFollowingBuyer;

    public int getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerUsername() {
        return buyerUsername;
    }

    public void setBuyerUsername(String buyerUsername) {
        this.buyerUsername = buyerUsername;
    }

    public String getBuyerImageUrl() {
        return buyerImageUrl;
    }

    public void setBuyerImageUrl(String buyerImageUrl) {
        this.buyerImageUrl = buyerImageUrl;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerUsername() {
        return sellerUsername;
    }

    public void setSellerUsername(String sellerUsername) {
        this.sellerUsername = sellerUsername;
    }

    public String getSellerImageUrl() {
        return sellerImageUrl;
    }

    public void setSellerImageUrl(String sellerImageUrl) {
        this.sellerImageUrl = sellerImageUrl;
    }

}
