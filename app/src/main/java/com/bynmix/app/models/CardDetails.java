package com.bynmix.app.models;

import com.stripe.android.model.Card;

public class CardDetails extends Card {
    private String fullName;
    private String sourceToken;
    private int settingAddressId;
    private boolean isNewAddress;
    private boolean isDefaultShippingAddress;
    private boolean isDefaultReturnAddress;
    private boolean isDefault;

    public CardDetails(String number, Integer expMonth, Integer expYear, String cvc, String name, String addressLine1, String addressLine2, String addressCity, String addressState, String addressZip, String addressCountry, String brand, String last4, String fingerprint, String funding, String country, String currency, String id) {
        super(number, expMonth, expYear, cvc, name, addressLine1, addressLine2, addressCity, addressState, addressZip, addressCountry, brand, last4, fingerprint, funding, country, currency, id);

    }

    public CardDetails(String number, Integer expMonth, Integer expYear, String cvc, String name, String addressLine1, String addressLine2, String addressCity, String addressState, String addressZip, String addressCountry,String fullName,String sourceToken,int settingAddressId, boolean isNewAddress, boolean isDefaultShippingAddress, boolean isDefaultReturnAddress, boolean isDefault) {
        super(number, expMonth, expYear, cvc, name, addressLine1, addressLine2, addressCity, addressState, addressZip, addressCountry, null);
        this.fullName = fullName;
        this.sourceToken = sourceToken;
        this.settingAddressId = settingAddressId;
        this.isNewAddress = isNewAddress;
        this.isDefaultReturnAddress = isDefaultReturnAddress;
        this.isDefaultShippingAddress = isDefaultShippingAddress;
        this.isDefault = isDefault;
    }

    public CardDetails(String number, Integer expMonth, Integer expYear, String cvc) {
        super(number, expMonth, expYear, cvc);
    }


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getSourceToken() {
        return sourceToken;
    }

    public void setSourceToken(String sourceToken) {
        this.sourceToken = sourceToken;
    }

    public int getSettingAddressId() {
        return settingAddressId;
    }

    public void setSettingAddressId(int settingAddressId) {
        this.settingAddressId = settingAddressId;
    }

    public boolean isNewAddress() {
        return isNewAddress;
    }

    public void setNewAddress(boolean newAddress) {
        isNewAddress = newAddress;
    }

    public boolean isDefaultShippingAddress() {
        return isDefaultShippingAddress;
    }

    public void setDefaultShippingAddress(boolean defaultShippingAddress) {
        isDefaultShippingAddress = defaultShippingAddress;
    }

    public boolean isDefaultReturnAddress() {
        return isDefaultReturnAddress;
    }

    public void setDefaultReturnAddress(boolean defaultReturnAddress) {
        isDefaultReturnAddress = defaultReturnAddress;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }
}
