package com.bynmix.app.models;


public class Colors {

    private int colorId;
    private String colorName;
    private String hexCode;
    private boolean isSelectedColor;

    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public String getHexCode() {
        return hexCode;
    }

    public void setHexCode(String hexCode) {
        this.hexCode = hexCode;
    }

    public boolean isSelectedColor() {
        return isSelectedColor;
    }

    public void setSelectedColor(boolean selectedColor) {
        isSelectedColor = selectedColor;
    }
}
