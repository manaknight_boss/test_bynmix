package com.bynmix.app.models;

public class Comments {

    private int listingCommentId;
    private int listingId;
    private String createdDate;
    private String createdDateFormatted;
    private String comment;
    private int userId;
    private String userImage;
    private String username;
    private String direction;
    private String additionalComment;
    private boolean isBuyerComment;
    private boolean isChargeDisputeEvidenceComment;

    public int getListingCommentId() {
        return listingCommentId;
    }

    public void setListingCommentId(int listingCommentId) {
        this.listingCommentId = listingCommentId;
    }

    public int getListingId() {
        return listingId;
    }

    public void setListingId(int listingId) {
        this.listingId = listingId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedDateFormatted() {
        return createdDateFormatted;
    }

    public void setCreatedDateFormatted(String createdDateFormatted) {
        this.createdDateFormatted = createdDateFormatted;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public String getAdditionalComment() {
        return additionalComment;
    }

    public void setAdditionalComment(String additionalComment) {
        this.additionalComment = additionalComment;
    }

    public boolean isBuyerComment() {
        return isBuyerComment;
    }

    public void setBuyerComment(boolean buyerComment) {
        isBuyerComment = buyerComment;
    }

    public boolean isChargeDisputeEvidenceComment() {
        return isChargeDisputeEvidenceComment;
    }

    public void setChargeDisputeEvidenceComment(boolean chargeDisputeEvidenceComment) {
        isChargeDisputeEvidenceComment = chargeDisputeEvidenceComment;
    }
}
