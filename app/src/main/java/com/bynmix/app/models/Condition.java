package com.bynmix.app.models;

public class Condition {

    private int conditionId;
    private String conditionName;
    private boolean isConditionSelected;

    public Condition(int id, String conditionName, boolean isConditionSelected) {
        this.conditionId = id;
        this.conditionName = conditionName;
        this.isConditionSelected = isConditionSelected;
    }

    public int getConditionId() {
        return conditionId;
    }

    public void setConditionId(int conditionId) {
        this.conditionId = conditionId;
    }

    public String getConditionName() {
        return conditionName;
    }

    public void setConditionName(String conditionName) {
        this.conditionName = conditionName;
    }

    public boolean isConditionSelected() {
        return isConditionSelected;
    }

    public void setConditionSelected(boolean conditionSelected) {
        isConditionSelected = conditionSelected;
    }
}
