package com.bynmix.app.models;

import java.util.List;

public class Dispute {
    private int issueTypeId;
    private int purchaseId;
    private String comments;
    private List<String> images;
    private int reportedIssueTypeId;
    private int entityId;
    private int disputeTypeId;


    public int getIssueTypeId() {
        return issueTypeId;
    }

    public void setIssueTypeId(int issueTypeId) {
        this.issueTypeId = issueTypeId;
    }

    public int getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(int purchaseId) {
        this.purchaseId = purchaseId;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public int getReportedIssueTypeId() {
        return reportedIssueTypeId;
    }

    public void setReportedIssueTypeId(int reportedIssueTypeId) {
        this.reportedIssueTypeId = reportedIssueTypeId;
    }

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }

    public int getDisputeTypeId() {
        return disputeTypeId;
    }

    public void setDisputeTypeId(int disputeTypeId) {
        this.disputeTypeId = disputeTypeId;
    }
}
