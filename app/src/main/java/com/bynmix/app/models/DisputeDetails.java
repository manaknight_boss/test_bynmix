package com.bynmix.app.models;

import java.util.ArrayList;
import java.util.List;

public class DisputeDetails {


    private int id;
    private String comments;
    private String expirationDate;
    private String expirationDateFormatted;
    private String createdDateFormatted;
    private boolean isResolved;
    private int disputeTypeId;
    private String disputeType;
    private int disputeStatusId;
    private String disputeStatus;
    private int resolvedByUserId;
    private boolean hasEvidence;
    private boolean isInProgress;
    private boolean canCancelDispute;
    private boolean canLeaveComment;
    private List<String> images;
    private int purchaseId;
    private int saleId;
    private String listingTitle;
    private int buyerId;
    private String buyerUsername;
    private String buyerImageUrl;
    private int sellerId;
    private String sellerUsername;
    private List<Comments> additionalComments = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public boolean isResolved() {
        return isResolved;
    }

    public void setResolved(boolean resolved) {
        isResolved = resolved;
    }

    public String getDisputeStatus() {
        return disputeStatus;
    }

    public void setDisputeStatus(String disputeStatus) {
        this.disputeStatus = disputeStatus;
    }

    public int getResolvedByUserId() {
        return resolvedByUserId;
    }

    public void setResolvedByUserId(int resolvedByUserId) {
        this.resolvedByUserId = resolvedByUserId;
    }

    public boolean isHasEvidence() {
        return hasEvidence;
    }

    public void setHasEvidence(boolean hasEvidence) {
        this.hasEvidence = hasEvidence;
    }

    public boolean isInProgress() {
        return isInProgress;
    }

    public void setInProgress(boolean inProgress) {
        isInProgress = inProgress;
    }

    public boolean isCanCancelDispute() {
        return canCancelDispute;
    }

    public void setCanCancelDispute(boolean canCancelDispute) {
        this.canCancelDispute = canCancelDispute;
    }

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public int getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(int purchaseId) {
        this.purchaseId = purchaseId;
    }

    public int getSaleId() {
        return saleId;
    }

    public void setSaleId(int saleId) {
        this.saleId = saleId;
    }

    public String getListingTitle() {
        return listingTitle;
    }

    public void setListingTitle(String listingTitle) {
        this.listingTitle = listingTitle;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerUsername() {
        return buyerUsername;
    }

    public void setBuyerUsername(String buyerUsername) {
        this.buyerUsername = buyerUsername;
    }

    public String getBuyerImageUrl() {
        return buyerImageUrl;
    }

    public void setBuyerImageUrl(String buyerImageUrl) {
        this.buyerImageUrl = buyerImageUrl;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerUsername() {
        return sellerUsername;
    }

    public void setSellerUsername(String sellerUsername) {
        this.sellerUsername = sellerUsername;
    }

    public String getCreatedDateFormatted() {
        return createdDateFormatted;
    }

    public void setCreatedDateFormatted(String createdDateFormatted) {
        this.createdDateFormatted = createdDateFormatted;
    }

    public List<Comments> getAdditionalComments() {
        return additionalComments;
    }

    public void setAdditionalComments(List<Comments> additionalComments) {
        this.additionalComments = additionalComments;
    }

    public boolean isCanLeaveComment() {
        return canLeaveComment;
    }

    public void setCanLeaveComment(boolean canLeaveComment) {
        this.canLeaveComment = canLeaveComment;
    }

    public String getExpirationDateFormatted() {
        return expirationDateFormatted;
    }

    public void setExpirationDateFormatted(String expirationDateFormatted) {
        this.expirationDateFormatted = expirationDateFormatted;
    }

    public int getDisputeTypeId() {
        return disputeTypeId;
    }

    public void setDisputeTypeId(int disputeTypeId) {
        this.disputeTypeId = disputeTypeId;
    }

    public String getDisputeType() {
        return disputeType;
    }

    public void setDisputeType(String disputeType) {
        this.disputeType = disputeType;
    }

    public int getDisputeStatusId() {
        return disputeStatusId;
    }

    public void setDisputeStatusId(int disputeStatusId) {
        this.disputeStatusId = disputeStatusId;
    }
}
