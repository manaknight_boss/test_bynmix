package com.bynmix.app.models;

public class DisputeReasons {

    private int id;
    private int disputeTypeId;
    private String disputeTypeName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getDisputeTypeId() {
        return disputeTypeId;
    }

    public void setDisputeTypeId(int disputeTypeId) {
        this.disputeTypeId = disputeTypeId;
    }

    public String getDisputeTypeName() {
        return disputeTypeName;
    }

    public void setDisputeTypeName(String disputeTypeName) {
        this.disputeTypeName = disputeTypeName;
    }
}
