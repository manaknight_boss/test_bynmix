package com.bynmix.app.models;

public class FacebookAndGmail {

    private String email;
    private String username;
    private String firstname;
    private String lastname;
    private String access_token;
    private String provider;
    private String client_id;
    private boolean isMobileRegistration;
    private String appDeviceId;
    private String appDeviceType;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public boolean isMobileRegistration() {
        return isMobileRegistration;
    }

    public void setMobileRegistration(boolean mobileRegistration) {
        isMobileRegistration = mobileRegistration;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAppDeviceId() {
        return appDeviceId;
    }

    public void setAppDeviceId(String appDeviceId) {
        this.appDeviceId = appDeviceId;
    }

    public String getAppDeviceType() {
        return appDeviceType;
    }

    public void setAppDeviceType(String appDeviceType) {
        this.appDeviceType = appDeviceType;
    }
}
