package com.bynmix.app.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FeedResponse extends ListingFeeds implements Serializable, Cloneable {

    private String type;
    private String createdDate;
    private String createdDateFormatted;
    private String likedDateFormatted;
    private int id;
    private String title;
    private String description;
    private String photoUrl;
    private int likesCount;
    private int userId;
    private String username;
    private String userPhotoUrl;
    private boolean userLiked;
    private String lastPriceDropDate;
    private String lastPriceDropPercentage;
    private double originalPrice;
    private double price;
    private String availability;
    private String brand;
    private String category;
    private String categoryType;
    private String condition;
    private String size;
    private String sizeType;
    private String link;
    private String postType;
    private String postedDate;
    private List<String> tags;
    private boolean isEditable = false;
    private int postImageId;
    private String shortDescription;
    private boolean hasPostDots;
    private boolean isVisibleDots;
    private String imageUrl;
    private List<String> postTags;
    private boolean canUserLikeItem;

    private List<AnnotatedPoint> postDots = new ArrayList<>();
    private int postDotsCount;

    public String getCreatedDateFormatted() {
        return createdDateFormatted;
    }

    public void setCreatedDateFormatted(String createdDateFormatted) {
        this.createdDateFormatted = createdDateFormatted;
    }

    public String getPostedDate() {
        return postedDate;
    }

    public void setPostedDate(String postedDate) {
        this.postedDate = postedDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }


    public String getUserPhotoUrl() {
        return userPhotoUrl;
    }

    public void setUserPhotoUrl(String userPhotoUrl) {
        this.userPhotoUrl = userPhotoUrl;
    }

    public boolean isUserLiked() {
        return userLiked;
    }

    public void setUserLiked(boolean userLiked) {
        this.userLiked = userLiked;
    }

    public String getLastPriceDropDate() {
        return lastPriceDropDate;
    }

    public void setLastPriceDropDate(String lastPriceDropDate) {
        this.lastPriceDropDate = lastPriceDropDate;
    }

    public String getLastPriceDropPercentage() {
        return lastPriceDropPercentage;
    }

    public void setLastPriceDropPercentage(String lastPriceDropPercentage) {
        this.lastPriceDropPercentage = lastPriceDropPercentage;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }


    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getSizeType() {
        return sizeType;
    }

    public void setSizeType(String sizeType) {
        this.sizeType = sizeType;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getPostType() {
        return postType;
    }

    public void setPostType(String postType) {
        this.postType = postType;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isEditable() {
        return isEditable;
    }

    public void setEditable(boolean editable) {
        isEditable = editable;
    }

    public int getPostImageId() {
        return postImageId;
    }

    public void setPostImageId(int postImageId) {
        this.postImageId = postImageId;
    }

    public List<String> getPostTags() {
        return postTags;
    }

    public void setPostTags(List<String> postTags) {
        this.postTags = postTags;
    }

    public int getPostDotsCount() {
        return postDotsCount;
    }

    public void setPostDotsCount(int postDotsCount) {
        this.postDotsCount = postDotsCount;
    }

    public String getLikedDateFormatted() {
        return likedDateFormatted;
    }

    public void setLikedDateFormatted(String likedDateFormatted) {
        this.likedDateFormatted = likedDateFormatted;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public List<AnnotatedPoint> getPostDots() {
        return postDots;
    }

    public void setPostDots(List<AnnotatedPoint> postDots) {
        this.postDots = postDots;
    }

    public boolean isHasPostDots() {
        return hasPostDots;
    }

    public void setHasPostDots(boolean hasPostDots) {
        this.hasPostDots = hasPostDots;
    }

    public boolean isVisibleDots() {
        return isVisibleDots;
    }

    public void setVisibleDots(boolean visibleDots) {
        isVisibleDots = visibleDots;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public boolean isCanUserLikeItem() {
        return canUserLikeItem;
    }

    public void setCanUserLikeItem(boolean canUserLikeItem) {
        canUserLikeItem = canUserLikeItem;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }
}
