package com.bynmix.app.models;

import java.io.Serializable;
import java.util.List;


public class Feeds extends PageResponse implements Serializable {

    private List<FeedResponse> items;
    private FeedResponse data;

    public List<FeedResponse> getItems() {
        return items;
    }

    public FeedResponse getData() {
        return data;
    }

    public void setItems(List<FeedResponse> items) {
        this.items = items;
    }


}
