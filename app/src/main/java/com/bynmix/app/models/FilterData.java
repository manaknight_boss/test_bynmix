package com.bynmix.app.models;


import java.util.List;

public class FilterData {

    private String searchText;
    private List<Integer>conditionIds;
    private List<Integer>availabilityIds;
    private List<Integer>sizeIds;
    private List<Integer>categoryIds;
    private List<Integer>colorIds;
    private int customMinPrice;
    private int customMaxPrice = 5000;
    private List<Integer>brandIds;

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public List<Integer> getConditionIds() {
        return conditionIds;
    }

    public void setConditionIds(List<Integer> conditionIds) {
        this.conditionIds = conditionIds;
    }

    public List<Integer> getAvailabilityIds() {
        return availabilityIds;
    }

    public void setAvailabilityIds(List<Integer> availabilityIds) {
        this.availabilityIds = availabilityIds;
    }

    public List<Integer> getSizeIds() {
        return sizeIds;
    }

    public void setSizeIds(List<Integer> sizeIds) {
        this.sizeIds = sizeIds;
    }

    public List<Integer> getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(List<Integer> categoryIds) {
        this.categoryIds = categoryIds;
    }

    public List<Integer> getColorIds() {
        return colorIds;
    }

    public void setColorIds(List<Integer> colorIds) {
        this.colorIds = colorIds;
    }

    public double getCustomMinPrice() {
        return customMinPrice;
    }

    public void setCustomMinPrice(int customMinPrice) {
        this.customMinPrice = customMinPrice;
    }

    public double getCustomMaxPrice() {
        return customMaxPrice;
    }

    public void setCustomMaxPrice(int customMaxPrice) {
        this.customMaxPrice = customMaxPrice;
    }

    public List<Integer> getBrandIds() {
        return brandIds;
    }

    public void setBrandIds(List<Integer> brandIds) {
        this.brandIds = brandIds;
    }

}
