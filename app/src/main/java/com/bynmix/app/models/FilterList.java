package com.bynmix.app.models;

import android.content.Context;

import com.bynmix.app.interfaces.FilterListener;

import java.util.List;

public class FilterList {

   private String filterName;
   private boolean isSelected;
   private boolean isItemSelected;

    public FilterList(String filterName, boolean isSelected) {
        this.filterName = filterName;
        this.isSelected = isSelected;
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public boolean isItemSelected() {
        return isItemSelected;
    }

    public void setItemSelected(boolean itemSelected) {
        isItemSelected = itemSelected;
    }
}
