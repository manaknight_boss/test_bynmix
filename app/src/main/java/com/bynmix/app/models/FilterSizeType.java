package com.bynmix.app.models;

import java.util.List;

public class FilterSizeType {

    private int sizeTypeId;
    private String sizeTypeName;
    private List<FilterSizes> sizes;
    private boolean isSelected;

    public int getSizeTypeId() {
        return sizeTypeId;
    }

    public void setSizeTypeId(int sizeTypeId) {
        sizeTypeId = sizeTypeId;
    }

    public String getSizeTypeName() {
        return sizeTypeName;
    }

    public void setSizeTypeName(String sizeTypeName) {
        sizeTypeName = sizeTypeName;
    }

    public List<FilterSizes> getSizes() {
        return sizes;
    }

    public void setSizes(List<FilterSizes> sizes) {
        this.sizes = sizes;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
