package com.bynmix.app.models;

import java.util.List;

public class FilterSizes {

    private String fit;
    private List<Size> sizes;

    public String getFit() {
        return fit;
    }

    public void setFit(String fit) {
        this.fit = fit;
    }

    public List<Size> getSizes() {
        return sizes;
    }

    public void setSizes(List<Size> sizes) {
        this.sizes = sizes;
    }
}
