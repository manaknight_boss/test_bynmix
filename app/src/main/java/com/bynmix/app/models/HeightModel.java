package com.bynmix.app.models;

public class HeightModel {

    private int customMinHeightFeet = 3;
    private int customMinHeightInches = 0;
    private int customMaxHeightFeet = 8;
    private int customMaxHeightInches = 0;
    private boolean isHeightShowAll;
    private float maxPinValue = 100;
    private float minPinValue = 0;

    public int getCustomMinHeightFeet() {
        return customMinHeightFeet;
    }

    public void setCustomMinHeightFeet(int customMinHeightFeet) {
        this.customMinHeightFeet = customMinHeightFeet;
    }

    public int getCustomMinHeightInches() {
        return customMinHeightInches;
    }

    public void setCustomMinHeightInches(int customMinHeightInches) {
        this.customMinHeightInches = customMinHeightInches;
    }

    public int getCustomMaxHeightFeet() {
        return customMaxHeightFeet;
    }

    public void setCustomMaxHeightFeet(int customMaxHeightFeet) {
        this.customMaxHeightFeet = customMaxHeightFeet;
    }

    public int getCustomMaxHeightInches() {
        return customMaxHeightInches;
    }

    public void setCustomMaxHeightInches(int customMaxHeightInches) {
        this.customMaxHeightInches = customMaxHeightInches;
    }

    public boolean isHeightShowAll() {
        return isHeightShowAll;
    }

    public void setHeightShowAll(boolean heightShowAll) {
        isHeightShowAll = heightShowAll;
    }

    public float getMaxPinValue() {
        return maxPinValue;
    }

    public void setMaxPinValue(float maxPinValue) {
        this.maxPinValue = maxPinValue;
    }

    public float getMinPinValue() {
        return minPinValue;
    }

    public void setMinPinValue(float minPinValue) {
        this.minPinValue = minPinValue;
    }
}
