package com.bynmix.app.models;

public class IssueType {
    private int reportedIssueTypeId;
    private int issueType;
    private String name;

    public int getReportedIssueTypeId() {
        return reportedIssueTypeId;
    }

    public void setReportedIssueTypeId(int reportedIssueTypeId) {
        this.reportedIssueTypeId = reportedIssueTypeId;
    }

    public int getIssueType() {
        return issueType;
    }

    public void setIssueType(int issueType) {
        this.issueType = issueType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
