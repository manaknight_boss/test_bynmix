package com.bynmix.app.models;

public class ListingCheck {

    private boolean isError;
    private String errorMessage;
    private boolean addressMissing;
    private boolean paymentMissing;
    private boolean isNewSeller;

    public boolean isError() {
        return isError;
    }

    public void setError(boolean error) {
        isError = error;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public boolean isAddressMissing() {
        return addressMissing;
    }

    public void setAddressMissing(boolean addressMissing) {
        this.addressMissing = addressMissing;
    }

    public boolean isPaymentMissing() {
        return paymentMissing;
    }

    public void setPaymentMissing(boolean paymentMissing) {
        this.paymentMissing = paymentMissing;
    }

    public boolean isNewSeller() {
        return isNewSeller;
    }

    public void setNewSeller(boolean newSeller) {
        isNewSeller = newSeller;
    }
}
