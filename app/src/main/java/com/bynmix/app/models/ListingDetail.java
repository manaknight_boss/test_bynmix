package com.bynmix.app.models;

import java.util.List;

public class ListingDetail {

    private String createdDate;
    private String createdDateFormatted;
    private String updatedDate;
    private int id;
    private String title;
    private String description;
    private String photoUrl;
    private Status status;
    private int likesCount;
    private int userId;
    private String username;
    private String userPhotoUrl;
    private boolean userLiked;
    private double originalPrice;
    private double price;
    private String link;
    private boolean isActive;
    private boolean isDeleted;
    private boolean isPurchased;
    private boolean userLoggedIn;

    private int categoryId;
    private int sizeTypeId;
    private int conditionId = 1;
    private int sizeId;
    private int categoryTypeId;
    private int brandId;
    private int availabilityId;
    private double shippingWeight;
    private int shippingRateId;
    private List<Integer> colorIds;


    private Availability availability;
    private Brand brand;
    private Category category;
    private CategoryType categoryType;
    private Condition condition;
    private Size size;
    private SizeType sizeType;
    private ShippingInfo shippingInfo;
    private int offerId;

    private int statusId;


    private List<Colors> colors;
    private List<Comments> comments;
    private List<Tags> listingTags;
    private List<ListingDisplayImage> listingDisplayImages;
    private List<Integer> imageIds;

    private List<String> tags;


    private List<String> images;

    private String sizeHeader;

    private boolean doesLoggedInUserHaveActiveBid;


    private double offerPrice;

    private String buyerName;

    private boolean isBuyItNowOnly;

    private int lowestAllowableOffer;

    private boolean canUserReportAnIssue;
    private boolean canUserLikeListing;


    public void setListingId(int listingId) {
        this.listingId = listingId;
    }

    private int listingId;

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getCreatedDateFormatted() {
        return createdDateFormatted;
    }

    public void setCreatedDateFormatted(String createdDateFormatted) {
        this.createdDateFormatted = createdDateFormatted;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserPhotoUrl() {
        return userPhotoUrl;
    }

    public void setUserPhotoUrl(String userPhotoUrl) {
        this.userPhotoUrl = userPhotoUrl;
    }

    public boolean isUserLiked() {
        return userLiked;
    }

    public void setUserLiked(boolean userLiked) {
        this.userLiked = userLiked;
    }

    public double getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(double originalPrice) {
        this.originalPrice = originalPrice;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public boolean isPurchased() {
        return isPurchased;
    }

    public void setPurchased(boolean purchased) {
        isPurchased = purchased;
    }

    public boolean isUserLoggedIn() {
        return userLoggedIn;
    }

    public void setUserLoggedIn(boolean userLoggedIn) {
        this.userLoggedIn = userLoggedIn;
    }

    public Availability getAvailability() {
        return availability;
    }

    public void setAvailability(Availability availability) {
        this.availability = availability;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public CategoryType getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(CategoryType categoryType) {
        this.categoryType = categoryType;
    }

    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public SizeType getSizeType() {
        return sizeType;
    }

    public void setSizeType(SizeType sizeType) {
        this.sizeType = sizeType;
    }

    public ShippingInfo getShippingInfo() {
        return shippingInfo;
    }

    public void setShippingInfo(ShippingInfo shippingInfo) {
        this.shippingInfo = shippingInfo;
    }

    public List<Colors> getColors() {
        return colors;
    }

    public void setColors(List<Colors> colors) {
        this.colors = colors;
    }

    public List<Comments> getComments() {
        return comments;
    }

    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }

    public List<Tags> getListingTags() {
        return listingTags;
    }

    public void setListingTags(List<Tags> listingTags) {
        this.listingTags = listingTags;
    }

    public List<ListingDisplayImage> getListingDisplayImages() {
        return listingDisplayImages;
    }

    public void setListingDisplayImages(List<ListingDisplayImage> listingDisplayImages) {
        this.listingDisplayImages = listingDisplayImages;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }


    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getSizeTypeId() {
        return sizeTypeId;
    }

    public void setSizeTypeId(int sizeTypeId) {
        this.sizeTypeId = sizeTypeId;
    }

    public int getConditionId() {
        return conditionId;
    }

    public void setConditionId(int conditionId) {
        this.conditionId = conditionId;
    }

    public int getSizeId() {
        return sizeId;
    }

    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }

    public int getCategoryTypeId() {
        return categoryTypeId;
    }

    public void setCategoryTypeId(int categoryTypeId) {
        this.categoryTypeId = categoryTypeId;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getAvailabilityId() {
        return availabilityId;
    }

    public void setAvailabilityId(int availabilityId) {
        this.availabilityId = availabilityId;
    }

    public double getShippingWeight() {

        return shippingWeight;
    }

    public void setShippingWeight(double shippingWeight) {
        this.shippingWeight = shippingWeight;
    }

    public int getShippingRateId() {
        return shippingRateId;
    }

    public void setShippingRateId(int shippingRateId) {
        this.shippingRateId = shippingRateId;
    }

    public List<Integer> getColorIds() {
        return colorIds;
    }

    public void setColorIds(List<Integer> colorIds) {
        this.colorIds = colorIds;
    }

    public String getSizeHeader() {
        return sizeHeader;
    }

    public void setSizeHeader(String sizeHeader) {
        this.sizeHeader = sizeHeader;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public List<Integer> getImageIds() {
        return imageIds;
    }

    public void setImageIds(List<Integer> imageIds) {
        this.imageIds = imageIds;
    }

    public boolean isDoesLoggedInUserHaveActiveBid() {
        return doesLoggedInUserHaveActiveBid;
    }

    public void setDoesLoggedInUserHaveActiveBid(boolean doesLoggedInUserHaveActiveBid) {
        this.doesLoggedInUserHaveActiveBid = doesLoggedInUserHaveActiveBid;
    }

    public double getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(double offerPrice) {
        this.offerPrice = offerPrice;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        this.buyerName = buyerName;
    }

    public boolean isBuyItNowOnly() {
        return isBuyItNowOnly;
    }

    public void setBuyItNowOnly(boolean buyItNowOnly) {
        isBuyItNowOnly = buyItNowOnly;
    }

    public int getLowestAllowableOffer() {
        return lowestAllowableOffer;
    }

    public void setLowestAllowableOffer(int lowestAllowableOffer) {
        this.lowestAllowableOffer = lowestAllowableOffer;
    }

    public boolean isCanUserReportAnIssue() {
        return canUserReportAnIssue;
    }

    public void setCanUserReportAnIssue(boolean canUserReportAnIssue) {
        canUserReportAnIssue = canUserReportAnIssue;
    }

    public boolean isCanUserLikeListing() {
        return canUserLikeListing;
    }

    public void setCanUserLikeItem(boolean canUserLikeListing) {
        this.canUserLikeListing = canUserLikeListing;
    }
}
