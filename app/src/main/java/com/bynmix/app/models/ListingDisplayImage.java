package com.bynmix.app.models;

public class ListingDisplayImage {

    private int listingImageId;
    private int displayOrder;
    private String imageUrl;
    private int type;
    public int getListingImageId() {
        return listingImageId;
    }

    public void setListingImageId(int listingImageId) {
        this.listingImageId = listingImageId;
    }

    public int getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(int displayOrder) {
        this.displayOrder = displayOrder;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
