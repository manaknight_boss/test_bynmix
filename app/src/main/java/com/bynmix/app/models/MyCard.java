package com.bynmix.app.models;

public class MyCard {
    private String fullName;
    private String sourceToken;
    private int settingAddressId;
    private boolean isNewAddress;
    private boolean isDefaultShippingAddress;
    private boolean isDefaultReturnAddress;
    private boolean isDefault;
    private String name;
    private String addressCountry;
    private String addressLine1;
    private String addressLine2;
    private String addressCity;
    private String addressState;
    private String addressZipcode;
    private int expirationYear;
    private int expirationMonth;
    private String nameOnCard;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getSourceToken() {
        return sourceToken;
    }

    public void setSourceToken(String sourceToken) {
        this.sourceToken = sourceToken;
    }

    public int getSettingAddressId() {
        return settingAddressId;
    }

    public void setSettingAddressId(int settingAddressId) {
        this.settingAddressId = settingAddressId;
    }

    public boolean isNewAddress() {
        return isNewAddress;
    }

    public void setNewAddress(boolean newAddress) {
        isNewAddress = newAddress;
    }

    public boolean isDefaultShippingAddress() {
        return isDefaultShippingAddress;
    }

    public void setDefaultShippingAddress(boolean defaultShippingAddress) {
        isDefaultShippingAddress = defaultShippingAddress;
    }

    public boolean isDefaultReturnAddress() {
        return isDefaultReturnAddress;
    }

    public void setDefaultReturnAddress(boolean defaultReturnAddress) {
        isDefaultReturnAddress = defaultReturnAddress;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddressCountry() {
        return addressCountry;
    }

    public void setAddressCountry(String addressCountry) {
        this.addressCountry = addressCountry;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public void setAddressCity(String addressCity) {
        this.addressCity = addressCity;
    }

    public String getAddressState() {
        return addressState;
    }

    public void setAddressState(String addressState) {
        this.addressState = addressState;
    }

    public String getAddressZipcode() {
        return addressZipcode;
    }

    public void setAddressZipcode(String addressZipcode) {
        this.addressZipcode = addressZipcode;
    }

    public int getExpirationYear() {
        return expirationYear;
    }

    public void setExpirationYear(int expirationYear) {
        this.expirationYear = expirationYear;
    }

    public int getExpirationMonth() {
        return expirationMonth;
    }

    public void setExpirationMonth(int expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    public String getNameOnCard() {
        return nameOnCard;
    }

    public void setNameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }
}
