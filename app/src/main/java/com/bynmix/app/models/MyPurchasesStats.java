package com.bynmix.app.models;

public class MyPurchasesStats {

    private int purchaseTotalId;
    private int totalItems;
    private int totalPendingPurchasePoints;
    private int totalPurchasePoints;
    private int totalPurchases;
    private int purchasesNeedingFeedback;
    private double totalSpentThisMonth;
    private int buyerId;
    private String buyerUsername;
    private String buyerImageUrl;
    private int totalPurchaseItems;
    private int totalBuyerFeedback;
    private int totalBuyerFeedbackScore;

    public int getPurchaseTotalId() {
        return purchaseTotalId;
    }

    public void setPurchaseTotalId(int purchaseTotalId) {
        this.purchaseTotalId = purchaseTotalId;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public int getTotalPendingPurchasePoints() {
        return totalPendingPurchasePoints;
    }

    public void setTotalPendingPurchasePoints(int totalPendingPurchasePoints) {
        this.totalPendingPurchasePoints = totalPendingPurchasePoints;
    }

    public int getTotalPurchasePoints() {
        return totalPurchasePoints;
    }

    public void setTotalPurchasePoints(int totalPurchasePoints) {
        this.totalPurchasePoints = totalPurchasePoints;
    }

    public int getTotalPurchases() {
        return totalPurchases;
    }

    public void setTotalPurchases(int totalPurchases) {
        this.totalPurchases = totalPurchases;
    }

    public double getTotalSpentThisMonth() {
        return totalSpentThisMonth;
    }

    public void setTotalSpentThisMonth(double totalSpentThisMonth) {
        this.totalSpentThisMonth = totalSpentThisMonth;
    }

    public int getPurchasesNeedingFeedback() {
        return purchasesNeedingFeedback;
    }

    public void setPurchasesNeedingFeedback(int purchasesNeedingFeedback) {
        this.purchasesNeedingFeedback = purchasesNeedingFeedback;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerUsername() {
        return buyerUsername;
    }

    public void setBuyerUsername(String buyerUsername) {
        this.buyerUsername = buyerUsername;
    }

    public String getBuyerImageUrl() {
        return buyerImageUrl;
    }

    public void setBuyerImageUrl(String buyerImageUrl) {
        this.buyerImageUrl = buyerImageUrl;
    }


    public int getTotalPurchaseItems() {
        return totalPurchaseItems;
    }

    public void setTotalPurchaseItems(int totalPurchaseItems) {
        this.totalPurchaseItems = totalPurchaseItems;
    }

    public int getTotalBuyerFeedback() {
        return totalBuyerFeedback;
    }

    public void setTotalBuyerFeedback(int totalBuyerFeedback) {
        this.totalBuyerFeedback = totalBuyerFeedback;
    }

    public int getTotalBuyerFeedbackScore() {
        return totalBuyerFeedbackScore;
    }

    public void setTotalBuyerFeedbackScore(int totalBuyerFeedbackScore) {
        this.totalBuyerFeedbackScore = totalBuyerFeedbackScore;
    }
}
