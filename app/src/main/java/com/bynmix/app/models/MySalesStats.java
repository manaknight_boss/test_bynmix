package com.bynmix.app.models;

public class MySalesStats {

    private int saleTotalId;
    private int totalItems;
    private int totalPendingItems;
    private int totalPendingSalePoints;
    private double totalPendingSales;
    private int totalSalePoints;
    private double totalSales;
    private int sellerId;
    private String sellerUsername;
    private String sellerImageUrl;
    private int totalSellerFeedback;
    private int totalSellerFeedbackScore;
    private int totalPendingSaleItems;
    private int totalSaleItems;
    private double totalSellerScore;


    public int getSaleTotalId() {
        return saleTotalId;
    }

    public void setSaleTotalId(int saleTotalId) {
        this.saleTotalId = saleTotalId;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public int getTotalPendingItems() {
        return totalPendingItems;
    }

    public void setTotalPendingItems(int totalPendingItems) {
        this.totalPendingItems = totalPendingItems;
    }

    public int getTotalPendingSalePoints() {
        return totalPendingSalePoints;
    }

    public void setTotalPendingSalePoints(int totalPendingSalePoints) {
        this.totalPendingSalePoints = totalPendingSalePoints;
    }

    public double getTotalPendingSales() {
        return totalPendingSales;
    }

    public void setTotalPendingSales(double totalPendingSales) {
        this.totalPendingSales = totalPendingSales;
    }

    public int getTotalSalePoints() {
        return totalSalePoints;
    }

    public void setTotalSalePoints(int totalSalePoints) {
        this.totalSalePoints = totalSalePoints;
    }

    public double getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(double totalSales) {
        this.totalSales = totalSales;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerUsername() {
        return sellerUsername;
    }

    public void setSellerUsername(String sellerUsername) {
        this.sellerUsername = sellerUsername;
    }

    public String getSellerImageUrl() {
        return sellerImageUrl;
    }

    public void setSellerImageUrl(String sellerImageUrl) {
        this.sellerImageUrl = sellerImageUrl;
    }

    public int getTotalSellerFeedback() {
        return totalSellerFeedback;
    }

    public void setTotalSellerFeedback(int totalSellerFeedback) {
        this.totalSellerFeedback = totalSellerFeedback;
    }

    public int getTotalSellerFeedbackScore() {
        return totalSellerFeedbackScore;
    }

    public void setTotalSellerFeedbackScore(int totalSellerFeedbackScore) {
        this.totalSellerFeedbackScore = totalSellerFeedbackScore;
    }

    public int getTotalPendingSaleItems() {
        return totalPendingSaleItems;
    }

    public void setTotalPendingSaleItems(int totalPendingSaleItems) {
        this.totalPendingSaleItems = totalPendingSaleItems;
    }

    public int getTotalSaleItems() {
        return totalSaleItems;
    }

    public void setTotalSaleItems(int totalSaleItems) {
        this.totalSaleItems = totalSaleItems;
    }

    public double getTotalSellerScore() {
        return totalSellerScore;
    }

    public void setTotalSellerScore(double totalSellerScore) {
        this.totalSellerScore = totalSellerScore;
    }
}
