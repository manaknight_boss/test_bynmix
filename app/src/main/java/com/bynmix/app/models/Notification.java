package com.bynmix.app.models;


public class Notification {

    private int notificationId;
    private String notificationDate;
    private String notificationDateFormatted;
    private int notificationTypeId;
    private String notificationType;
    private String iconUrl;
    private boolean isUnread;
    private int listingId;
    private String listingImage;
    private double listingPrice;
    private String listingTitle;
    private String offerExpiresDate;
    private int sellerId;
    private String createdDate;
    private int userId;
    private String username;
    private String userImage;
    private boolean isActiveOffer;
    private int listingPercentDrop;
    private int initiatorUserId;
    private int offerId;
    private boolean isLoggedInUserOffer;
    private boolean isSellerOffer;
    private String postLink;
    private String formattedText;
    private int promotionTypeId;
    private int postId;
    private boolean isActive;

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }

    public String getNotificationDateFormatted() {
        return notificationDateFormatted;
    }

    public void setNotificationDateFormatted(String notificationDateFormatted) {
        this.notificationDateFormatted = notificationDateFormatted;
    }

    public int getNotificationTypeId() {
        return notificationTypeId;
    }

    public void setNotificationTypeId(int notificationTypeId) {
        this.notificationTypeId = notificationTypeId;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public boolean isUnread() {
        return isUnread;
    }

    public void setUnread(boolean unread) {
        isUnread = unread;
    }

    public int getListingId() {
        return listingId;
    }

    public void setListingId(int listingId) {
        this.listingId = listingId;
    }

    public String getListingImage() {
        return listingImage;
    }

    public void setListingImage(String listingImage) {
        this.listingImage = listingImage;
    }

    public String getListingTitle() {
        return listingTitle;
    }

    public void setListingTitle(String listingTitle) {
        this.listingTitle = listingTitle;
    }

    public String getOfferExpiresDate() {
        return offerExpiresDate;
    }

    public void setOfferExpiresDate(String offerExpiresDate) {
        this.offerExpiresDate = offerExpiresDate;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    public boolean isActiveOffer() {
        return isActiveOffer;
    }

    public void setActiveOffer(boolean activeOffer) {
        isActiveOffer = activeOffer;
    }

    public int getListingPercentDrop() {
        return listingPercentDrop;
    }

    public void setListingPercentDrop(int listingPercentDrop) {
        this.listingPercentDrop = listingPercentDrop;
    }

    public int getInitiatorUserId() {
        return initiatorUserId;
    }

    public void setInitiatorUserId(int initiatorUserId) {
        this.initiatorUserId = initiatorUserId;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public boolean isLoggedInUserOffer() {
        return isLoggedInUserOffer;
    }

    public void setLoggedInUserOffer(boolean loggedInUserOffer) {
        isLoggedInUserOffer = loggedInUserOffer;
    }

    public double getListingPrice() {
        return listingPrice;
    }

    public void setListingPrice(double listingPrice) {
        this.listingPrice = listingPrice;
    }

    public boolean isSellerOffer() {
        return isSellerOffer;
    }

    public void setSellerOffer(boolean sellerOffer) {
        isSellerOffer = sellerOffer;
    }

    public String getPostLink() {
        return postLink;
    }

    public void setPostLink(String postLink) {
        this.postLink = postLink;
    }

    public String getFormattedText() {
        return formattedText;
    }

    public void setFormattedText(String formattedText) {
        this.formattedText = formattedText;
    }

    public int getPromotionTypeId() {
        return promotionTypeId;
    }

    public void setPromotionTypeId(int promotionTypeId) {
        promotionTypeId = promotionTypeId;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
