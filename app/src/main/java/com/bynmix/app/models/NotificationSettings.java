package com.bynmix.app.models;

public class NotificationSettings {

    private int settingNotificationId;

    private boolean isEnabled;
    private int userId;
    private NotificationSettingTypes settingType;

    public int getSettingNotificationId() {
        return settingNotificationId;
    }

    public void setSettingNotificationId(int settingNotificationId) {
        this.settingNotificationId = settingNotificationId;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public NotificationSettingTypes getSettingType() {
        return settingType;
    }

    public void setSettingType(NotificationSettingTypes settingType) {
        this.settingType = settingType;
    }
}
