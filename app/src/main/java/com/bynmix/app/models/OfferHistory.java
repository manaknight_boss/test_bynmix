package com.bynmix.app.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class OfferHistory implements Serializable {

    private int latestOfferId;
    private boolean isLatestOfferExpired;
    private boolean isLatestOfferBySeller;
    private int latestOfferType;
    private String latestOfferTypeName;
    private String latestOfferDateFormatted;
    private boolean isOfferAllowed = true;
    private boolean isBuyerAndCanOffer;
    private boolean isSellerAndCanCounterOffer;
    private int listingId;
    private String listingTitle;
    private String listingImage;
    private boolean isListingBuyItNowOnly;
    private int buyerId;
    private String buyerUsername;
    private int sellerId;
    private String sellerUsername;
    private String sellerUserImage;
    private boolean isOfferCancelable;
    private List<SubOfferHistory> offerHistories = new ArrayList<>();


    public int getLatestOfferId() {
        return latestOfferId;
    }

    public void setLatestOfferId(int latestOfferId) {
        this.latestOfferId = latestOfferId;
    }

    public boolean isLatestOfferExpired() {
        return isLatestOfferExpired;
    }

    public void setLatestOfferExpired(boolean latestOfferExpired) {
        isLatestOfferExpired = latestOfferExpired;
    }

    public boolean isLatestOfferBySeller() {
        return isLatestOfferBySeller;
    }

    public void setLatestOfferBySeller(boolean latestOfferBySeller) {
        isLatestOfferBySeller = latestOfferBySeller;
    }

    public int getLatestOfferType() {
        return latestOfferType;
    }

    public void setLatestOfferType(int latestOfferType) {
        this.latestOfferType = latestOfferType;
    }

    public String getLatestOfferTypeName() {
        return latestOfferTypeName;
    }

    public void setLatestOfferTypeName(String latestOfferTypeName) {
        this.latestOfferTypeName = latestOfferTypeName;
    }

    public String getLatestOfferDateFormatted() {
        return latestOfferDateFormatted;
    }

    public void setLatestOfferDateFormatted(String latestOfferDateFormatted) {
        this.latestOfferDateFormatted = latestOfferDateFormatted;
    }

    public boolean isOfferAllowed() {
        return isOfferAllowed;
    }

    public void setOfferAllowed(boolean offerAllowed) {
        isOfferAllowed = offerAllowed;
    }

    public boolean isBuyerAndCanOffer() {
        return isBuyerAndCanOffer;
    }

    public void setBuyerAndCanOffer(boolean buyerAndCanOffer) {
        isBuyerAndCanOffer = buyerAndCanOffer;
    }

    public boolean isSellerAndCanCounterOffer() {
        return isSellerAndCanCounterOffer;
    }

    public void setSellerAndCanCounterOffer(boolean sellerAndCanCounterOffer) {
        isSellerAndCanCounterOffer = sellerAndCanCounterOffer;
    }

    public int getListingId() {
        return listingId;
    }

    public void setListingId(int listingId) {
        this.listingId = listingId;
    }

    public String getListingTitle() {
        return listingTitle;
    }

    public void setListingTitle(String listingTitle) {
        this.listingTitle = listingTitle;
    }

    public String getListingImage() {
        return listingImage;
    }

    public void setListingImage(String listingImage) {
        this.listingImage = listingImage;
    }

    public boolean isListingBuyItNowOnly() {
        return isListingBuyItNowOnly;
    }

    public void setListingBuyItNowOnly(boolean listingBuyItNowOnly) {
        isListingBuyItNowOnly = listingBuyItNowOnly;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerUsername() {
        return buyerUsername;
    }

    public void setBuyerUsername(String buyerUsername) {
        this.buyerUsername = buyerUsername;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerUsername() {
        return sellerUsername;
    }

    public void setSellerUsername(String sellerUsername) {
        this.sellerUsername = sellerUsername;
    }

    public String getSellerUserImage() {
        return sellerUserImage;
    }

    public void setSellerUserImage(String sellerUserImage) {
        this.sellerUserImage = sellerUserImage;
    }

    public List<SubOfferHistory> getOfferHistories() {
        return offerHistories;
    }

    public void setOfferHistories(List<SubOfferHistory> offerHistories) {
        this.offerHistories = offerHistories;
    }

    public boolean isOfferCancelable() {
        return isOfferCancelable;
    }

    public void setOfferCancelable(boolean offerCancelable) {
        this.isOfferCancelable = offerCancelable;
    }
}
