package com.bynmix.app.models;

public class OfferHistoryTest {
    private String offerDate;
    private double offerPrice;
    private String offerExpirationDate;
    private int offerOrder;
    private int previousOfferId;
    private boolean isCounterOffer;
    private boolean isSellerCounterOffer;
    private boolean isBuyerOffer;
    private int cardId;
    private int addressId;
    private int sellerReturnAddressId;
    private int offerId;
    private int offerType;
    private int buyerId;
    private int sellerId;
    private int listingId;
    private String createdDate;
    private boolean isActiveOffer;
    private boolean isDeclinedBySeller;
    private boolean isAcceptedByBuyer;
    private String offerTypeName;

    private String buyerUsername;
    private String buyerUserImage;
    private String sellerUsername;
    private String sellerUserImage;
    private String listingTitle;
    private String listingImage;
    private String offerDateFormatted;
    private String AcceptedDateFormatted;
    private String DeclinedDateFormatted;
    private String declinedReason;

    private int latestOfferType;
    private String latestOfferTypeName;

    private boolean IsOfferCancelable;
    private boolean IsSellerAndCanCounterOffer;
    private boolean IsBuyerAndCanOffer;

    public String getOfferDate() {
        return offerDate;
    }

    public void setOfferDate(String offerDate) {
        this.offerDate = offerDate;
    }

    public double getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(double offerPrice) {
        this.offerPrice = offerPrice;
    }

    public String getOfferExpirationDate() {
        return offerExpirationDate;
    }

    public void setOfferExpirationDate(String offerExpirationDate) {
        this.offerExpirationDate = offerExpirationDate;
    }

    public int getOfferOrder() {
        return offerOrder;
    }

    public void setOfferOrder(int offerOrder) {
        this.offerOrder = offerOrder;
    }

    public int getPreviousOfferId() {
        return previousOfferId;
    }

    public void setPreviousOfferId(int previousOfferId) {
        this.previousOfferId = previousOfferId;
    }

    public boolean isCounterOffer() {
        return isCounterOffer;
    }

    public void setCounterOffer(boolean counterOffer) {
        isCounterOffer = counterOffer;
    }

    public boolean isSellerCounterOffer() {
        return isSellerCounterOffer;
    }

    public void setSellerCounterOffer(boolean sellerCounterOffer) {
        isSellerCounterOffer = sellerCounterOffer;
    }

    public boolean isBuyerOffer() {
        return isBuyerOffer;
    }

    public void setBuyerOffer(boolean buyerOffer) {
        isBuyerOffer = buyerOffer;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public int getSellerReturnAddressId() {
        return sellerReturnAddressId;
    }

    public void setSellerReturnAddressId(int sellerReturnAddressId) {
        this.sellerReturnAddressId = sellerReturnAddressId;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public int getOfferType() {
        return offerType;
    }

    public void setOfferType(int offerType) {
        this.offerType = offerType;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public int getListingId() {
        return listingId;
    }

    public void setListingId(int listingId) {
        this.listingId = listingId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isActiveOffer() {
        return isActiveOffer;
    }

    public void setActiveOffer(boolean activeOffer) {
        isActiveOffer = activeOffer;
    }

    public String getBuyerUsername() {
        return buyerUsername;
    }

    public void setBuyerUsername(String buyerUsername) {
        this.buyerUsername = buyerUsername;
    }

    public String getBuyerUserImage() {
        return buyerUserImage;
    }

    public void setBuyerUserImage(String buyerUserImage) {
        this.buyerUserImage = buyerUserImage;
    }

    public String getSellerUsername() {
        return sellerUsername;
    }

    public void setSellerUsername(String sellerUsername) {
        this.sellerUsername = sellerUsername;
    }

    public String getSellerUserImage() {
        return sellerUserImage;
    }

    public void setSellerUserImage(String sellerUserImage) {
        this.sellerUserImage = sellerUserImage;
    }

    public String getListingTitle() {
        return listingTitle;
    }

    public void setListingTitle(String listingTitle) {
        this.listingTitle = listingTitle;
    }

    public String getListingImage() {
        return listingImage;
    }

    public void setListingImage(String listingImage) {
        this.listingImage = listingImage;
    }

    public String getOfferDateFormatted() {
        return offerDateFormatted;
    }

    public void setOfferDateFormatted(String offerDateFormatted) {
        this.offerDateFormatted = offerDateFormatted;
    }

    public String getAcceptedDateFormatted() {
        return AcceptedDateFormatted;
    }

    public void setAcceptedDateFormatted(String acceptedDateFormatted) {
        AcceptedDateFormatted = acceptedDateFormatted;
    }

    public String getDeclinedDateFormatted() {
        return DeclinedDateFormatted;
    }

    public void setDeclinedDateFormatted(String declinedDateFormatted) {
        DeclinedDateFormatted = declinedDateFormatted;
    }

    public String getOfferTypeName() {
        return offerTypeName;
    }

    public void setOfferTypeName(String offerTypeName) {
        this.offerTypeName = offerTypeName;
    }

    public String getDeclinedReason() {
        return declinedReason;
    }

    public void setDeclinedReason(String declinedReason) {
        this.declinedReason = declinedReason;
    }

    public boolean isDeclinedBySeller() {
        return isDeclinedBySeller;
    }

    public void setDeclinedBySeller(boolean declinedBySeller) {
        isDeclinedBySeller = declinedBySeller;
    }

    public boolean isAcceptedByBuyer() {
        return isAcceptedByBuyer;
    }

    public void setAcceptedByBuyer(boolean acceptedByBuyer) {
        isAcceptedByBuyer = acceptedByBuyer;
    }

    public int getLatestOfferType() {
        return latestOfferType;
    }

    public void setLatestOfferType(int latestOfferType) {
        this.latestOfferType = latestOfferType;
    }

    public String getLatestOfferTypeName() {
        return latestOfferTypeName;
    }

    public void setLatestOfferTypeName(String latestOfferTypeName) {
        this.latestOfferTypeName = latestOfferTypeName;
    }

    public boolean isOfferCancelable() {
        return IsOfferCancelable;
    }

    public void setOfferCancelable(boolean offerCancelable) {
        IsOfferCancelable = offerCancelable;
    }

    public boolean isSellerAndCanCounterOffer() {
        return IsSellerAndCanCounterOffer;
    }

    public void setSellerAndCanCounterOffer(boolean sellerAndCanCounterOffer) {
        IsSellerAndCanCounterOffer = sellerAndCanCounterOffer;
    }

    public boolean isBuyerAndCanOffer() {
        return IsBuyerAndCanOffer;
    }

    public void setBuyerAndCanOffer(boolean buyerAndCanOffer) {
        IsBuyerAndCanOffer = buyerAndCanOffer;
    }
}
