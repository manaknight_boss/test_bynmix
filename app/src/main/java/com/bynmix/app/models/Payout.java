package com.bynmix.app.models;

public class Payout {
    private double amount;
    private String arrivalDate;
    private String bankAccountNickName;
    private String lastFour;
    private String status;
    private String createdDate;
    private String arrivalDateFormatted;
    private String bankNickName;
    private String routingNumber;
    private String accountNumber;
    private String nameOnBankAccount;
    private double withdrawAmount;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getBankAccountNickName() {
        return bankAccountNickName;
    }

    public void setBankAccountNickName(String bankAccountNickName) {
        this.bankAccountNickName = bankAccountNickName;
    }

    public String getLastFour() {
        return lastFour;
    }

    public void setLastFour(String lastFour) {
        this.lastFour = lastFour;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getFormattedDate() {
        return arrivalDateFormatted;
    }

    public void setFormattedDate(String formattedDate) {
        this.arrivalDateFormatted = formattedDate;
    }

    public String getBankNickName() {
        return bankNickName;
    }

    public void setBankNickName(String bankNickName) {
        this.bankNickName = bankNickName;
    }

    public String getRoutingNumber() {
        return routingNumber;
    }

    public void setRoutingNumber(String routingNumber) {
        this.routingNumber = routingNumber;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getNameOnBankAccount() {
        return nameOnBankAccount;
    }

    public void setNameOnBankAccount(String nameOnBankAccount) {
        this.nameOnBankAccount = nameOnBankAccount;
    }

    public double getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(double withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }
}
