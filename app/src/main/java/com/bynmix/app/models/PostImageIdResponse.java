package com.bynmix.app.models;

import java.util.List;

public class PostImageIdResponse {
    private List<PostImage> data;

    public List<PostImage> getData() {
        return data;
    }
}
