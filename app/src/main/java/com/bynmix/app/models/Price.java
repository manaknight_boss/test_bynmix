package com.bynmix.app.models;

public class Price {

    private int id;
    private String price;
    private boolean isPriceSelected;

    public Price(int id, String price, boolean isPriceSelected) {
        this.id = id;
        this.price = price;
        this.isPriceSelected = isPriceSelected;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public boolean isPriceSelected() {
        return isPriceSelected;
    }

    public void setPriceSelected(boolean priceSelected) {
        isPriceSelected = priceSelected;
    }
}
