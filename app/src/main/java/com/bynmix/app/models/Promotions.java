package com.bynmix.app.models;

public class Promotions {

    private int promotionTypeId;
    private int amountOff;
    private String promotionStartDate;
    private String promotionEndDate;
    private int promotionId;
    private int promotionTypeEnumId;
    private String promotionType;
    private String bannerText;

    public int getPromotionTypeId() {
        return promotionTypeId;
    }

    public void setPromotionTypeId(int promotionTypeId) {
        this.promotionTypeId = promotionTypeId;
    }

    public int getAmountOff() {
        return amountOff;
    }

    public void setAmountOff(int amountOff) {
        this.amountOff = amountOff;
    }

    public String getPromotionStartDate() {
        return promotionStartDate;
    }

    public void setPromotionStartDate(String promotionStartDate) {
        this.promotionStartDate = promotionStartDate;
    }

    public String getPromotionEndDate() {
        return promotionEndDate;
    }

    public void setPromotionEndDate(String promotionEndDate) {
        this.promotionEndDate = promotionEndDate;
    }

    public int getPromotionId() {
        return promotionId;
    }

    public void setPromotionId(int promotionId) {
        this.promotionId = promotionId;
    }

    public int getPromotionTypeEnumId() {
        return promotionTypeEnumId;
    }

    public void setPromotionTypeEnumId(int promotionTypeEnumId) {
        this.promotionTypeEnumId = promotionTypeEnumId;
    }

    public String getPromotionType() {
        return promotionType;
    }

    public void setPromotionType(String promotionType) {
        this.promotionType = promotionType;
    }

    public String getBannerText() {
        return bannerText;
    }

    public void setBannerText(String bannerText) {
        this.bannerText = bannerText;
    }
}
