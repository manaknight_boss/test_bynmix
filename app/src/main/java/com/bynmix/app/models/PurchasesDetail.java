package com.bynmix.app.models;

import java.util.List;

public class PurchasesDetail extends Stock {
    private int purchaseId;
    private String purchaseDate;
    private String purchaseDateFormatted;
    private double purchaseCharged;
    private double purchaseDiscountAmount;
    private int purchasePercentDiscount;
    private double purchasePrice;
    private int userShippingAddressId;
    private int sellerId;
    private String sellerUsername;
    private String sellerImageUrl;
    private int purchaseStatusId;
    private String purchaseStatus;
    private double purchaseShippingPrice;
    private int purchaseTotalPrice;
    private int purchaseOriginalBeforeDiscountPrice;
    private int purchaseOriginalAfterDiscountPrice;
    private boolean canCancelPurchase;
    private String purchaseSubStatus;


    private CardDetail paidWith;

    public CardDetail getPaidWith() {
        return paidWith;
    }

    public void setPaidWith(CardDetail paidWith) {
        this.paidWith = paidWith;
    }

    private int discount;

    private String purchaseMonth;
    private String purchaseYear;

    public String getPurchaseMonth() {
        return purchaseMonth;
    }

    public void setPurchaseMonth(String purchaseMonth) {
        this.purchaseMonth = purchaseMonth;
    }

    public String getPurchaseYear() {
        return purchaseYear;
    }

    public void setPurchaseYear(String purchaseYear) {
        this.purchaseYear = purchaseYear;
    }

    public int getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(int purchaseId) {
        this.purchaseId = purchaseId;
    }

    public String getPurchaseDate() {
        return purchaseDate;
    }

    public void setPurchaseDate(String purchaseDate) {
        this.purchaseDate = purchaseDate;
    }

    public String getPurchaseDateFormatted() {
        return purchaseDateFormatted;
    }

    public void setPurchaseDateFormatted(String purchaseDateFormatted) {
        this.purchaseDateFormatted = purchaseDateFormatted;
    }

    public double getPurchaseCharged() {
        return purchaseCharged;
    }

    public void setPurchaseCharged(double purchaseCharged) {
        this.purchaseCharged = purchaseCharged;
    }

    public double getPurchaseDiscountAmount() {
        return purchaseDiscountAmount;
    }

    public void setPurchaseDiscountAmount(double purchaseDiscountAmount) {
        this.purchaseDiscountAmount = purchaseDiscountAmount;
    }

    public int getPurchasePercentDiscount() {
        return purchasePercentDiscount;
    }

    public void setPurchasePercentDiscount(int purchasePercentDiscount) {
        this.purchasePercentDiscount = purchasePercentDiscount;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public int getUserShippingAddressId() {
        return userShippingAddressId;
    }

    public void setUserShippingAddressId(int userShippingAddressId) {
        this.userShippingAddressId = userShippingAddressId;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerUsername() {
        return sellerUsername;
    }

    public void setSellerUsername(String sellerUsername) {
        this.sellerUsername = sellerUsername;
    }

    public String getSellerImageUrl() {
        return sellerImageUrl;
    }

    public void setSellerImageUrl(String sellerImageUrl) {
        this.sellerImageUrl = sellerImageUrl;
    }

    public int getPurchaseStatusId() {
        return purchaseStatusId;
    }

    public void setPurchaseStatusId(int purchaseStatusId) {
        this.purchaseStatusId = purchaseStatusId;
    }

    public String getPurchaseStatus() {
        return purchaseStatus;
    }

    public void setPurchaseStatus(String purchaseStatus) {
        this.purchaseStatus = purchaseStatus;
    }

    public int getPurchaseTotalPrice() {
        return purchaseTotalPrice;
    }

    public void setPurchaseTotalPrice(int purchaseTotalPrice) {
        this.purchaseTotalPrice = purchaseTotalPrice;
    }

    public int getPurchaseOriginalBeforeDiscountPrice() {
        return purchaseOriginalBeforeDiscountPrice;
    }

    public void setPurchaseOriginalBeforeDiscountPrice(int purchaseOriginalBeforeDiscountPrice) {
        this.purchaseOriginalBeforeDiscountPrice = purchaseOriginalBeforeDiscountPrice;
    }

    public int getPurchaseOriginalAfterDiscountPrice() {
        return purchaseOriginalAfterDiscountPrice;
    }

    public void setPurchaseOriginalAfterDiscountPrice(int purchaseOriginalAfterDiscountPrice) {
        this.purchaseOriginalAfterDiscountPrice = purchaseOriginalAfterDiscountPrice;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public boolean isCanCancelPurchase() {
        return canCancelPurchase;
    }

    public void setCanCancelPurchase(boolean canCancelPurchase) {
        this.canCancelPurchase = canCancelPurchase;
    }

    public String getPurchaseSubStatus() {
        return purchaseSubStatus;
    }

    public void setPurchaseSubStatus(String purchaseSubStatus) {
        this.purchaseSubStatus = purchaseSubStatus;
    }

    public double getPurchaseShippingPrice() {
        return purchaseShippingPrice;
    }

    public void setPurchaseShippingPrice(double purchaseShippingPrice) {
        this.purchaseShippingPrice = purchaseShippingPrice;
    }
}

