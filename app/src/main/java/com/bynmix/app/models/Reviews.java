package com.bynmix.app.models;

import java.io.Serializable;
import java.util.List;

public class Reviews extends PageResponse implements Serializable {
    private String createdDateFormatted;
    private List<Reviews> items;
   private int purchaseFeedbackId;
    private String createdDate;
    private String feedback;
    private double rating;
    private int listingId;
    private String listingTitle;
    private String listingPhotoUrl;
    private int buyerId;
    private String buyerUsername;
    private String buyerPhotoUrl;
    private int sellerId;
    private String sellerUsername;
    private String sellerPhotoUrl;

    public String getListingTitle() {
        return listingTitle;
    }

    public void setListingTitle(String listingTitle) {
        this.listingTitle = listingTitle;
    }

    public String getCreatedDateFormatted() {
        return createdDateFormatted;
    }

    public void setCreatedDateFormatted(String createdDateFormatted) {
        this.createdDateFormatted = createdDateFormatted;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public List<Reviews> getItems() {
        return items;
    }

    public void setItems(List<Reviews> items) {
        this.items = items;
    }


    public int getPurchaseFeedbackId() {
        return purchaseFeedbackId;
    }

    public void setPurchaseFeedbackId(int purchaseFeedbackId) {
        this.purchaseFeedbackId = purchaseFeedbackId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public int getListingId() {
        return listingId;
    }

    public void setListingId(int listingId) {
        this.listingId = listingId;
    }

    public String getListingPhotoUrl() {
        return listingPhotoUrl;
    }

    public void setListingPhotoUrl(String listingPhotoUrl) {
        this.listingPhotoUrl = listingPhotoUrl;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerUsername() {
        return buyerUsername;
    }

    public void setBuyerUsername(String buyerUsername) {
        this.buyerUsername = buyerUsername;
    }

    public String getBuyerPhotoUrl() {
        return buyerPhotoUrl;
    }

    public void setBuyerPhotoUrl(String buyerPhotoUrl) {
        this.buyerPhotoUrl = buyerPhotoUrl;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerUsername() {
        return sellerUsername;
    }

    public void setSellerUsername(String sellerUsername) {
        this.sellerUsername = sellerUsername;
    }

    public String getSellerPhotoUrl() {
        return sellerPhotoUrl;
    }

    public void setSellerPhotoUrl(String sellerPhotoUrl) {
        this.sellerPhotoUrl = sellerPhotoUrl;
    }
}
