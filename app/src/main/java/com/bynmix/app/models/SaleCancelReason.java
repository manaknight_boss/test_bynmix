package com.bynmix.app.models;

public class SaleCancelReason {

    private int saleCancelReasonId;
    private int saleCancelReasonType;
    private String cancelReason;
    private String comments;

    public int getSaleCancelReasonId() {
        return saleCancelReasonId;
    }

    public void setSaleCancelReasonId(int saleCancelReasonId) {
        this.saleCancelReasonId = saleCancelReasonId;
    }

    public int getSaleCancelReasonType() {
        return saleCancelReasonType;
    }

    public void setSaleCancelReasonType(int saleCancelReasonType) {
        this.saleCancelReasonType = saleCancelReasonType;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
}
