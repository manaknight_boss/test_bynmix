package com.bynmix.app.models;

public class SalesDetail extends Stock {

    private int saleId;
    private String saleDate;
    private String saleDateFormatted;
    private double saleOriginalPrice;
    private int saleEarned;
    private double salePrice;
    private double saleProfit;
    private int saleStatusId;
    private String saleStatus;
    private int userReturnAddressId;
    private int buyerId;
    private String buyerUsername;
    private String buyerImageUrl;

    private int saleTotalPrice;
    private int saleShippingPrice;
    private double saleBynmixCommission;
    private String saleMonth;
    private String saleYear;
    private boolean canCancelSale;
    private String saleSubStatus;
    private double saleTotalDisplayPrice;

    public double getSaleTotalDisplayPrice() {
        return saleTotalDisplayPrice;
    }

    public void setSaleTotalDisplayPrice(double saleTotalDisplayPrice) {
        this.saleTotalDisplayPrice = saleTotalDisplayPrice;
    }

    public int getSaleId() {
        return saleId;
    }

    public void setSaleId(int saleId) {
        this.saleId = saleId;
    }

    public String getSaleDate() {
        return saleDate;
    }

    public void setSaleDate(String saleDate) {
        this.saleDate = saleDate;
    }

    public String getSaleDateFormatted() {
        return saleDateFormatted;
    }

    public void setSaleDateFormatted(String saleDateFormatted) {
        this.saleDateFormatted = saleDateFormatted;
    }

    public int getSaleEarned() {
        return saleEarned;
    }

    public void setSaleEarned(int saleEarned) {
        this.saleEarned = saleEarned;
    }

    public double getSalePrice() {
        return salePrice;
    }

    public void setSalePrice(double salePrice) {
        this.salePrice = salePrice;
    }

    public double getSaleProfit() {
        return saleProfit;
    }

    public void setSaleProfit(double saleProfit) {
        this.saleProfit = saleProfit;
    }

    public int getSaleStatusId() {
        return saleStatusId;
    }

    public void setSaleStatusId(int saleStatusId) {
        this.saleStatusId = saleStatusId;
    }

    public String getSaleStatus() {
        return saleStatus;
    }

    public void setSaleStatus(String saleStatus) {
        this.saleStatus = saleStatus;
    }

    public int getUserReturnAddressId() {
        return userReturnAddressId;
    }

    public void setUserReturnAddressId(int userReturnAddressId) {
        this.userReturnAddressId = userReturnAddressId;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerUsername() {
        return buyerUsername;
    }

    public void setBuyerUsername(String buyerUsername) {
        this.buyerUsername = buyerUsername;
    }

    public String getBuyerImageUrl() {
        return buyerImageUrl;
    }

    public void setBuyerImageUrl(String buyerImageUrl) {
        this.buyerImageUrl = buyerImageUrl;
    }

    public int getSaleTotalPrice() {
        return saleTotalPrice;
    }

    public void setSaleTotalPrice(int saleTotalPrice) {
        this.saleTotalPrice = saleTotalPrice;
    }

    public int getSaleShippingPrice() {
        return saleShippingPrice;
    }

    public void setSaleShippingPrice(int saleShippingPrice) {
        this.saleShippingPrice = saleShippingPrice;
    }

    public double getSaleBynmixCommission() {
        return saleBynmixCommission;
    }

    public void setSaleBynmixCommission(double saleBynmixCommission) {
        this.saleBynmixCommission = saleBynmixCommission;
    }

    public double getSaleOriginalPrice() {
        return saleOriginalPrice;
    }

    public void setSaleOriginalPrice(double saleOriginalPrice) {
        this.saleOriginalPrice = saleOriginalPrice;
    }

    public String getSaleMonth() {
        return saleMonth;
    }

    public void setSaleMonth(String saleMonth) {
        this.saleMonth = saleMonth;
    }

    public String getSaleYear() {
        return saleYear;
    }

    public void setSaleYear(String saleYear) {
        this.saleYear = saleYear;
    }

    public boolean isCanCancelSale() {
        return canCancelSale;
    }

    public void setCanCancelSale(boolean canCancelSale) {
        this.canCancelSale = canCancelSale;
    }

    public String getSaleSubStatus() {
        return saleSubStatus;
    }

    public void setSaleSubStatus(String saleSubStatus) {
        this.saleSubStatus = saleSubStatus;
    }
}
