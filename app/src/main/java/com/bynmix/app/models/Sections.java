package com.bynmix.app.models;

import java.util.List;

public class Sections {
    private int id;
    private int order;
    private String title;
    private String description;
    private List<Sections> subSections;
    private List<String> points;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Sections> getSubSection() {
        return subSections;
    }

    public void setSubSection(List<Sections> subSection) {
        this.subSections = subSection;
    }

    public List<String> getPoints() {
        return points;
    }

    public void setPoints(List<String> points) {
        this.points = points;
    }
}
