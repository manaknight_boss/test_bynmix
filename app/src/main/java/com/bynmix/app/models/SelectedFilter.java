package com.bynmix.app.models;

import java.util.List;

public class SelectedFilter {
    private int id;
    private String name;
    private int type;
    private int subType;
    private List<Integer> idsList;

    public SelectedFilter(int id, String name, int type, int subType, List<Integer> idsList) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.subType = subType;
        this.idsList =  idsList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getSubType() {
        return subType;
    }

    public void setSubType(int subType) {
        this.subType = subType;
    }

    public List<Integer> getIdsList() {
        return idsList;
    }

    public void setIdsList(List<Integer> idsList) {
        this.idsList = idsList;
    }
}
