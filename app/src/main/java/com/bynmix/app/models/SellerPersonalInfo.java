package com.bynmix.app.models;

public class SellerPersonalInfo {

    private int dobMonth;
    private int dobDay;
    private int dobYear;
    private String lastFourSocial;
    private int addressId;

    public int getDobMonth() {
        return dobMonth;
    }

    public void setDobMonth(int dobMonth) {
        this.dobMonth = dobMonth;
    }

    public int getDobDay() {
        return dobDay;
    }

    public void setDobDay(int dobDay) {
        this.dobDay = dobDay;
    }

    public int getDobYear() {
        return dobYear;
    }

    public void setDobYear(int dobYear) {
        this.dobYear = dobYear;
    }

    public String getLastFourSocial() {
        return lastFourSocial;
    }

    public void setLastFourSocial(String lastFourSocial) {
        this.lastFourSocial = lastFourSocial;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }
}
