package com.bynmix.app.models;

import java.io.Serializable;
import java.util.List;

public class Settings extends PageResponse implements Serializable {

    private List<NotificationSettings> settings;
    private List<Notification> items;
    private List<Integer> notificationIds;
    private int type;

    public List<NotificationSettings> getSettings() {
        return settings;
    }

    public void setSettings(List<NotificationSettings> settings) {
        this.settings = settings;
    }

    public List<Notification> getItems() {
        return items;
    }

    public void setItems(List<Notification> items) {
        this.items = items;
    }

    public List<Integer> getNotificationIds() {
        return notificationIds;
    }

    public void setNotificationIds(List<Integer> notificationIds) {
        this.notificationIds = notificationIds;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
