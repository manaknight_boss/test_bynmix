package com.bynmix.app.models;

public class ShippingHistory {
    private int trackingLogId;
    private String carrier;
    private String eta;
    private String trackingNumber;
    private String locationCity;
    private String locationCountry;
    private String locationState;
    private String locationZip;
    private String originalETA;
    private String status;
    private String statusCreated;
    private String statusDate;
    private String statusDetails;
    private String displayStatus;

    public int getTrackingLogId() {
        return trackingLogId;
    }

    public void setTrackingLogId(int trackingLogId) {
        this.trackingLogId = trackingLogId;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getEta() {
        return eta;
    }

    public void setEta(String eta) {
        this.eta = eta;
    }

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public String getLocationCity() {
        return locationCity;
    }

    public void setLocationCity(String locationCity) {
        this.locationCity = locationCity;
    }

    public String getLocationCountry() {
        return locationCountry;
    }

    public void setLocationCountry(String locationCountry) {
        this.locationCountry = locationCountry;
    }

    public String getLocationState() {
        return locationState;
    }

    public void setLocationState(String locationState) {
        this.locationState = locationState;
    }

    public String getLocationZip() {
        return locationZip;
    }

    public void setLocationZip(String locationZip) {
        this.locationZip = locationZip;
    }

    public String getOriginalETA() {
        return originalETA;
    }

    public void setOriginalETA(String originalETA) {
        this.originalETA = originalETA;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusCreated() {
        return statusCreated;
    }

    public void setStatusCreated(String statusCreated) {
        this.statusCreated = statusCreated;
    }

    public String getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(String statusDate) {
        this.statusDate = statusDate;
    }

    public String getStatusDetails() {
        return statusDetails;
    }

    public void setStatusDetails(String statusDetails) {
        this.statusDetails = statusDetails;
    }

    public String getDisplayStatus() {
        return displayStatus;
    }

    public void setDisplayStatus(String displayStatus) {
        this.displayStatus = displayStatus;
    }
}
