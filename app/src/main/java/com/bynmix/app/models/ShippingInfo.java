package com.bynmix.app.models;

public class ShippingInfo {

    private int shippingRateId;
    private ShippingType shippingType;
    private int maxHeightDimension;
    private int maxLengthDimension;
    private int maxWidthDimension;
    private double shippingWeightMax;
    private double rate;

    public int getShippingRateId() {
        return shippingRateId;
    }

    public void setShippingRateId(int shippingRateId) {
        this.shippingRateId = shippingRateId;
    }

    public ShippingType getShippingType() {
        return shippingType;
    }

    public void setShippingType(ShippingType shippingType) {
        this.shippingType = shippingType;
    }

    public int getMaxHeightDimension() {
        return maxHeightDimension;
    }

    public void setMaxHeightDimension(int maxHeightDimension) {
        this.maxHeightDimension = maxHeightDimension;
    }

    public int getMaxLengthDimension() {
        return maxLengthDimension;
    }

    public void setMaxLengthDimension(int maxLengthDimension) {
        this.maxLengthDimension = maxLengthDimension;
    }

    public int getMaxWidthDimension() {
        return maxWidthDimension;
    }

    public void setMaxWidthDimension(int maxWidthDimension) {
        this.maxWidthDimension = maxWidthDimension;
    }

    public double getShippingWeightMax() {
        return shippingWeightMax;
    }

    public void setShippingWeightMax(double shippingWeightMax) {
        this.shippingWeightMax = shippingWeightMax;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
