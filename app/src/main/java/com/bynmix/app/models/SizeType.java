package com.bynmix.app.models;

import java.util.List;

public class SizeType {

    private int sizeTypeId;
    private String sizeTypeName;
    private List<Size> sizes;

    public List<Size> getSizes() {
        return sizes;
    }

    public void setSizes(List<Size> sizes) {
        this.sizes = sizes;
    }

    public int getSizeTypeId() {
        return sizeTypeId;
    }

    public void setSizeTypeId(int sizeTypeId) {
        this.sizeTypeId = sizeTypeId;
    }

    public String getSizeTypeName() {
        return sizeTypeName;
    }

    public void setSizeTypeName(String sizeTypeName) {
        this.sizeTypeName = sizeTypeName;
    }
}
