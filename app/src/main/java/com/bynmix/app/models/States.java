package com.bynmix.app.models;


import android.content.Context;

import com.bynmix.app.interfaces.AddTagsListener;

import java.util.List;

public class States {
    private int stateId;
    private String abbreviation;
    private String name;

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
