package com.bynmix.app.models;

public class Status {
   private int listingStatusId;
   private int  status;
   private String statusName;
   private boolean isVisibleToUser;


    public Status(int status, String statusName) {
        this.status = status;
        this.statusName = statusName;
    }


    public int getListingStatusId() {
        return listingStatusId;
    }

    public void setListingStatusId(int listingStatusId) {
        this.listingStatusId = listingStatusId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public boolean isVisibleToUser() {
        return isVisibleToUser;
    }

    public void setVisibleToUser(boolean visibleToUser) {
        isVisibleToUser = visibleToUser;
    }
}
