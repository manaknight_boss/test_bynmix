package com.bynmix.app.models;

import java.util.List;

public class Stock {
    private int rating;
    private String feedback;
    private String ribbonColor;
    private boolean isComplete;
    private int points;
    private int pointsStatusId;
    private String pointsStatus;
    private double shippingPrice;
    private int listingId;
    private String listingTitle;
    private String listingImageUrl;
    private int userId;
    private String username;
    private String userImageUrl;
    private BuyerAndSellerInfo buyerAndSellerInfo;
    private Address shippedTo;
    private List<Integer> promotions;
    private List<String> promotionsFormatted;

    private boolean canPrintShippingLabel;
    private boolean canLeaveFeedback;

    private boolean canOpenDispute;
    private boolean hasOpenDispute;
    private boolean hasSubStatus;

    private int disputeId;

    public boolean canPrintShippingLabel() {
        return canPrintShippingLabel;
    }

    public void setPrintShippingLabel(boolean canPrintShippingLabel) {
        this.canPrintShippingLabel = canPrintShippingLabel;
    }

    public Address getShippedTo() {
        return shippedTo;
    }

    public void setShippedTo(Address shippedTo) {
        this.shippedTo = shippedTo;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getRibbonColor() {
        return ribbonColor;
    }

    public void setRibbonColor(String ribbonColor) {
        this.ribbonColor = ribbonColor;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getPointsStatusId() {
        return pointsStatusId;
    }

    public void setPointsStatusId(int pointsStatusId) {
        this.pointsStatusId = pointsStatusId;
    }

    public String getPointsStatus() {
        return pointsStatus;
    }

    public void setPointsStatus(String pointsStatus) {
        this.pointsStatus = pointsStatus;
    }

    public double getShippingPrice() {
        return shippingPrice;
    }

    public void setShippingPrice(double shippingPrice) {
        this.shippingPrice = shippingPrice;
    }

    public int getListingId() {
        return listingId;
    }

    public void setListingId(int listingId) {
        this.listingId = listingId;
    }

    public String getListingTitle() {
        return listingTitle;
    }

    public void setListingTitle(String listingTitle) {
        this.listingTitle = listingTitle;
    }

    public String getListingImageUrl() {
        return listingImageUrl;
    }

    public void setListingImageUrl(String listingImageUrl) {
        this.listingImageUrl = listingImageUrl;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public void setUserImageUrl(String userImageUrl) {
        this.userImageUrl = userImageUrl;
    }

    public BuyerAndSellerInfo getBuyerAndSellerInfo() {
        return buyerAndSellerInfo;
    }

    public void setBuyerAndSellerInfo(BuyerAndSellerInfo buyerAndSellerInfo) {
        this.buyerAndSellerInfo = buyerAndSellerInfo;
    }

    public List<Integer> getPromotions() {
        return promotions;
    }

    public void setPromotions(List<Integer> promotions) {
        this.promotions = promotions;
    }

    public List<String> getPromotionsFormatted() {
        return promotionsFormatted;
    }

    public void setPromotionsFormatted(List<String> promotionsFormatted) {
        this.promotionsFormatted = promotionsFormatted;
    }

    public boolean isCanLeaveFeedback() {
        return canLeaveFeedback;
    }

    public void setCanLeaveFeedback(boolean canLeaveFeedback) {
        this.canLeaveFeedback = canLeaveFeedback;
    }

    public boolean isCanOpenDispute() {
        return canOpenDispute;
    }

    public void setCanOpenDispute(boolean canOpenDispute) {
        this.canOpenDispute = canOpenDispute;
    }

    public boolean isHasOpenDispute() {
        return hasOpenDispute;
    }

    public void setHasOpenDispute(boolean hasOpenDispute) {
        this.hasOpenDispute = hasOpenDispute;
    }

    public boolean isHasSubStatus() {
        return hasSubStatus;
    }

    public void setHasSubStatus(boolean hasSubStatus) {
        this.hasSubStatus = hasSubStatus;
    }

    public int getDisputeId() {
        return disputeId;
    }

    public void setDisputeId(int disputeId) {
        this.disputeId = disputeId;
    }
}
