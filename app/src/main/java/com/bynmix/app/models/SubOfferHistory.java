package com.bynmix.app.models;

import java.io.Serializable;

public class SubOfferHistory implements Serializable {

    public static int TYPE_OFFER = 1;
    public static int TYPE_ACCEPTED = 3;
    public static int TYPE_DECLINED =2;

    private String offerDate;
    private String offerDateFormatted;
    private double offerPrice;
    private String offerExpirationDate;
    private int offerOrder;
    private int previousOfferId;
    private boolean isCounterOffer;
    private boolean isSellerCounterOffer;
    private boolean isBuyerOffer;
    private int cardId;
    private int addressId;
    private int sellerReturnAddressId;
    private int offerId;
    private int offerType;
    private String offerTypeName;
    private int buyerId;
    private String buyerUsername;
    private String buyerUserImage;
    private int sellerId;
    private String sellerUsername;
    private String sellerUserImage;
    private int listingId;
    private String listingTitle;
    private String listingImage;
    private boolean isListingBuyItNowOnly;
    private String createdDate;
    private boolean isActiveOffer;
    private String declinedReason;
    private boolean isDeclinedBySeller;
    private double declinedPrice;
    private String declinedDateFormatted;

    private boolean isAcceptedByBuyer;
    private double acceptedPrice;
    private String acceptedDateFormatted;

    public String getDeclinedReason() {
        return declinedReason;
    }

    public void setDeclinedReason(String declinedReason) {
        this.declinedReason = declinedReason;
    }

    public String getOfferDate() {
        return offerDate;
    }

    public void setOfferDate(String offerDate) {
        this.offerDate = offerDate;
    }

    public String getOfferDateFormatted() {
        return offerDateFormatted;
    }

    public void setOfferDateFormatted(String offerDateFormatted) {
        this.offerDateFormatted = offerDateFormatted;
    }

    public double getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(double offerPrice) {
        this.offerPrice = offerPrice;
    }

    public String getOfferExpirationDate() {
        return offerExpirationDate;
    }

    public void setOfferExpirationDate(String offerExpirationDate) {
        this.offerExpirationDate = offerExpirationDate;
    }

    public int getOfferOrder() {
        return offerOrder;
    }

    public void setOfferOrder(int offerOrder) {
        this.offerOrder = offerOrder;
    }

    public int getPreviousOfferId() {
        return previousOfferId;
    }

    public void setPreviousOfferId(int previousOfferId) {
        this.previousOfferId = previousOfferId;
    }

    public boolean isCounterOffer() {
        return isCounterOffer;
    }

    public void setCounterOffer(boolean counterOffer) {
        isCounterOffer = counterOffer;
    }

    public boolean isSellerCounterOffer() {
        return isSellerCounterOffer;
    }

    public void setSellerCounterOffer(boolean sellerCounterOffer) {
        isSellerCounterOffer = sellerCounterOffer;
    }

    public boolean isBuyerOffer() {
        return isBuyerOffer;
    }

    public void setBuyerOffer(boolean buyerOffer) {
        isBuyerOffer = buyerOffer;
    }

    public int getCardId() {
        return cardId;
    }

    public void setCardId(int cardId) {
        this.cardId = cardId;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public int getSellerReturnAddressId() {
        return sellerReturnAddressId;
    }

    public void setSellerReturnAddressId(int sellerReturnAddressId) {
        this.sellerReturnAddressId = sellerReturnAddressId;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public int getOfferType() {
        return offerType;
    }

    public void setOfferType(int offerType) {
        this.offerType = offerType;
    }

    public String getOfferTypeName() {
        return offerTypeName;
    }

    public void setOfferTypeName(String offerTypeName) {
        this.offerTypeName = offerTypeName;
    }

    public int getBuyerId() {
        return buyerId;
    }

    public void setBuyerId(int buyerId) {
        this.buyerId = buyerId;
    }

    public String getBuyerUsername() {
        return buyerUsername;
    }

    public void setBuyerUsername(String buyerUsername) {
        this.buyerUsername = buyerUsername;
    }

    public String getBuyerUserImage() {
        return buyerUserImage;
    }

    public void setBuyerUserImage(String buyerUserImage) {
        this.buyerUserImage = buyerUserImage;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerUsername() {
        return sellerUsername;
    }

    public void setSellerUsername(String sellerUsername) {
        this.sellerUsername = sellerUsername;
    }

    public String getSellerUserImage() {
        return sellerUserImage;
    }

    public void setSellerUserImage(String sellerUserImage) {
        this.sellerUserImage = sellerUserImage;
    }

    public int getListingId() {
        return listingId;
    }

    public void setListingId(int listingId) {
        this.listingId = listingId;
    }

    public String getListingTitle() {
        return listingTitle;
    }

    public void setListingTitle(String listingTitle) {
        this.listingTitle = listingTitle;
    }

    public String getListingImage() {
        return listingImage;
    }

    public void setListingImage(String listingImage) {
        this.listingImage = listingImage;
    }

    public boolean isListingBuyItNowOnly() {
        return isListingBuyItNowOnly;
    }

    public void setListingBuyItNowOnly(boolean listingBuyItNowOnly) {
        isListingBuyItNowOnly = listingBuyItNowOnly;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isActiveOffer() {
        return isActiveOffer;
    }

    public void setActiveOffer(boolean activeOffer) {
        isActiveOffer = activeOffer;
    }

    public boolean isDeclinedBySeller() {
        return isDeclinedBySeller;
    }

    public void setDeclinedBySeller(boolean declinedBySeller) {
        isDeclinedBySeller = declinedBySeller;
    }

    public double getDeclinedPrice() {
        return declinedPrice;
    }

    public void setDeclinedPrice(double declinedPrice) {
        this.declinedPrice = declinedPrice;
    }

    public String getDeclinedDateFormatted() {
        return declinedDateFormatted;
    }

    public void setDeclinedDateFormatted(String declinedDateFormatted) {
        this.declinedDateFormatted = declinedDateFormatted;
    }

    public boolean isAcceptedByBuyer() {
        return isAcceptedByBuyer;
    }

    public void setAcceptedByBuyer(boolean acceptedByBuyer) {
        isAcceptedByBuyer = acceptedByBuyer;
    }

    public double getAcceptedPrice() {
        return acceptedPrice;
    }

    public void setAcceptedPrice(double acceptedPrice) {
        this.acceptedPrice = acceptedPrice;
    }

    public String getAcceptedDateFormatted() {
        return acceptedDateFormatted;
    }

    public void setAcceptedDateFormatted(String acceptedDateFormatted) {
        this.acceptedDateFormatted = acceptedDateFormatted;
    }
}
