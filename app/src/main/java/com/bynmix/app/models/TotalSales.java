package com.bynmix.app.models;

public class TotalSales {

    private int saleTotalId;
    private int totalItems;
    private int totalPendingItems;
    private int totalPendingSalePoints;
    private double totalPendingSales;
    private int totalSalePoints;
    private int totalSales;
    private int sellerId;
    private String sellerUsername;
    private String sellerImageUrl;
    private int totalAvailableForWithdrawal;

    public int getSaleTotalId() {
        return saleTotalId;
    }

    public void setSaleTotalId(int saleTotalId) {
        this.saleTotalId = saleTotalId;
    }

    public int getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(int totalItems) {
        this.totalItems = totalItems;
    }

    public int getTotalPendingItems() {
        return totalPendingItems;
    }

    public void setTotalPendingItems(int totalPendingItems) {
        this.totalPendingItems = totalPendingItems;
    }

    public int getTotalPendingSalePoints() {
        return totalPendingSalePoints;
    }

    public void setTotalPendingSalePoints(int totalPendingSalePoints) {
        this.totalPendingSalePoints = totalPendingSalePoints;
    }

    public double getTotalPendingSales() {
        return totalPendingSales;
    }

    public void setTotalPendingSales(double totalPendingSales) {
        this.totalPendingSales = totalPendingSales;
    }

    public int getTotalSalePoints() {
        return totalSalePoints;
    }

    public void setTotalSalePoints(int totalSalePoints) {
        this.totalSalePoints = totalSalePoints;
    }

    public int getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(int totalSales) {
        this.totalSales = totalSales;
    }

    public int getSellerId() {
        return sellerId;
    }

    public void setSellerId(int sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerUsername() {
        return sellerUsername;
    }

    public void setSellerUsername(String sellerUsername) {
        this.sellerUsername = sellerUsername;
    }

    public String getSellerImageUrl() {
        return sellerImageUrl;
    }

    public void setSellerImageUrl(String sellerImageUrl) {
        this.sellerImageUrl = sellerImageUrl;
    }

    public int getTotalAvailableForWithdrawal() {
        return totalAvailableForWithdrawal;
    }

    public void setTotalAvailableForWithdrawal(int totalAvailableForWithdrawal) {
        this.totalAvailableForWithdrawal = totalAvailableForWithdrawal;
    }
}
