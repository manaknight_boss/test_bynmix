package com.bynmix.app.models;

import java.util.List;

public class Tracking {
    private String trackingNumber;
    private List<ShippingHistory> shippingHistory;
    private String listingTitle;
    private  int listingId;

    public String getTrackingNumber() {
        return trackingNumber;
    }

    public void setTrackingNumber(String trackingNumber) {
        this.trackingNumber = trackingNumber;
    }

    public List<ShippingHistory> getShippingHistory() {
        return shippingHistory;
    }

    public void setShippingHistory(List<ShippingHistory> shippingHistory) {
        this.shippingHistory = shippingHistory;
    }

    public String getListingTitle() {
        return listingTitle;
    }

    public void setListingTitle(String listingTitle) {
        this.listingTitle = listingTitle;
    }

    public int getListingId() {
        return listingId;
    }

    public void setListingId(int listingId) {
        this.listingId = listingId;
    }
}
