package com.bynmix.app.models;

import java.util.List;

public class UserFilterData {
    private String searchText;
    private String userId;
    private List<Integer> styleIds;
    private List<Integer> brandIds;
    private List<Integer> bodyTypeIds;
    private String customMinHeightFeet;
    private String customMinHeightInches;
    private String customMaxHeightFeet;
    private String customMaxHeightInches;

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<Integer> getStyleIds() {
        return styleIds;
    }

    public void setStyleIds(List<Integer> styleIds) {
        this.styleIds = styleIds;
    }

    public List<Integer> getBrandIds() {
        return brandIds;
    }

    public void setBrandIds(List<Integer> brandIds) {
        this.brandIds = brandIds;
    }

    public List<Integer> getBodyTypeIds() {
        return bodyTypeIds;
    }

    public void setBodyTypeIds(List<Integer> bodyTypeIds) {
        this.bodyTypeIds = bodyTypeIds;
    }

    public String getCustomMinHeightFeet() {
        return customMinHeightFeet;
    }

    public void setCustomMinHeightFeet(String customMinHeightFeet) {
        this.customMinHeightFeet = customMinHeightFeet;
    }

    public String getCustomMinHeightInches() {
        return customMinHeightInches;
    }

    public void setCustomMinHeightInches(String customMinHeightInches) {
        this.customMinHeightInches = customMinHeightInches;
    }

    public String getCustomMaxHeightFeet() {
        return customMaxHeightFeet;
    }

    public void setCustomMaxHeightFeet(String customMaxHeightFeet) {
        this.customMaxHeightFeet = customMaxHeightFeet;
    }

    public String getCustomMaxHeightInches() {
        return customMaxHeightInches;
    }

    public void setCustomMaxHeightInches(String customMaxHeightInches) {
        this.customMaxHeightInches = customMaxHeightInches;
    }
}
