package com.bynmix.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import retrofit2.http.Field;

/**
 * Created by manaknight on 2018-05-14.
 */

public class UserResponse extends UserSocialLinks {

    private int userId;
    private String username;
    private String firstname;
    private String lastname;
    private String shortTitle;
    private String email;
    private String password;
    private String blogLink;
    private String city;
    private String dateJoined;
    private int followersCount;
    private int followingCount;
    private int listingsCount;
    private String photoUrl;
    private int postsCount;
    private String title;
    private String shortBio;
    private String state;
    private String stateAbbreviated;
    private boolean onboardingComplete;
    private int lastOnboardingScreenCompleted;
    private boolean selected;
    private boolean isLoggedInUserFollowing;
    private boolean isUserFollowingLoggedInUser;
    private String backgroundPhoto;
    private boolean isMobileRegistration;
    private boolean isUserSameAsLoggedInUser;
    private int code;
    private String userBrandsFormatted;
    private String userDescriptorsFormatted;

    private int gender;
    private int bodyTypeId;
    private int heightFeet;
    private int heightInches;

    private double rating;
    private List<String> reviews;

    private boolean finishedRegister;


    public boolean isLoggedInUserFollowing() {
        return isLoggedInUserFollowing;
    }

    public void setLoggedInUserFollowing(boolean loggedInUserFollowing) {
        isLoggedInUserFollowing = loggedInUserFollowing;
    }

    public boolean isUserFollowingLoggedInUser() {
        return isUserFollowingLoggedInUser;
    }

    public void setUserFollowingLoggedInUser(boolean userFollowingLoggedInUser) {
        isUserFollowingLoggedInUser = userFollowingLoggedInUser;
    }

    public String getBackgroundPhoto() {
        return backgroundPhoto;
    }

    public void setBackgroundPhoto(String backgroundPhoto) {
        this.backgroundPhoto = backgroundPhoto;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBlogLink() {
        return blogLink;
    }

    public void setBlogLink(String blogLink) {
        this.blogLink = blogLink;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(String dateJoined) {
        this.dateJoined = dateJoined;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public int getFollowersCount() {
        return followersCount;
    }

    public void setFollowersCount(int followersCount) {
        this.followersCount = followersCount;
    }

    public int getFollowingCount() {
        return followingCount;
    }

    public void setFollowingCount(int followingCount) {
        this.followingCount = followingCount;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getListingsCount() {
        return listingsCount;
    }

    public void setListingsCount(int listingsCount) {
        this.listingsCount = listingsCount;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public int getPostsCount() {
        return postsCount;
    }

    public void setPostsCount(int postsCount) {
        this.postsCount = postsCount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortBio() {
        return shortBio;
    }

    public void setShortBio(String shortBio) {
        this.shortBio = shortBio;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public boolean isOnboardingComplete() {
        return onboardingComplete;
    }

    public void setOnboardingComplete(boolean onboardingComplete) {
        this.onboardingComplete = onboardingComplete;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isMobileRegistration() {
        return isMobileRegistration;
    }

    public void setMobileRegistration(boolean mobileRegistration) {
        isMobileRegistration = mobileRegistration;
    }

    public String getStateAbbreviated() {
        return stateAbbreviated;
    }

    public void setStateAbbreviated(String stateAbbreviated) {
        this.stateAbbreviated = stateAbbreviated;
    }

    public boolean isUserSameAsLoggedInUser() {
        return isUserSameAsLoggedInUser;
    }

    public void setUserSameAsLoggedInUser(boolean userSameAsLoggedInUser) {
        isUserSameAsLoggedInUser = userSameAsLoggedInUser;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getUserBrandsFormatted() {
        return userBrandsFormatted;
    }

    public void setUserBrandsFormatted(String userBrandsFormatted) {
        this.userBrandsFormatted = userBrandsFormatted;
    }

    public int getLastOnboardingScreenCompleted() {
        return lastOnboardingScreenCompleted;
    }

    public void setLastOnboardingScreenCompleted(int lastOnboardingScreenCompleted) {
        this.lastOnboardingScreenCompleted = lastOnboardingScreenCompleted;
    }

    public String getUserDescriptorsFormatted() {
        return userDescriptorsFormatted;
    }

    public void setUserDescriptorsFormatted(String userDescriptorsFormatted) {
        this.userDescriptorsFormatted = userDescriptorsFormatted;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getBodyTypeId() {
        return bodyTypeId;
    }

    public void setBodyTypeId(int bodyTypeId) {
        this.bodyTypeId = bodyTypeId;
    }

    public int getHeightFeet() {
        return heightFeet;
    }

    public void setHeightFeet(int heightFeet) {
        this.heightFeet = heightFeet;
    }

    public int getHeightInches() {
        return heightInches;
    }

    public void setHeightInches(int heightInches) {
        this.heightInches = heightInches;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public List<String> getReviews() {
        return reviews;
    }

    public void setReviews(List<String> reviews) {
        this.reviews = reviews;
    }

    public String getShortTitle() {
        return shortTitle;
    }

    public void setShortTitle(String shortTitle) {
        this.shortTitle = shortTitle;
    }

    public boolean isFinishedRegister() {
        return finishedRegister;
    }

    public void setFinishedRegister(boolean finishedRegister) {
        this.finishedRegister = finishedRegister;
    }

    @Override
    public String toString() {
        return new StringBuilder("userId ").append(userId).append(" username ").append(username)
                .append(" blogLink ").append(blogLink).append(" city ").append(city)
                .append(" dateJoined ").append(dateJoined).append(" firstname ").append(firstname)
                .append(" followersCount ").append(followersCount).append(" followingCount ")
                .append(followingCount).append(" lastname ").append(lastname).append(" listingsCount ")
                .append(listingsCount).append(" photoUrl ").append(photoUrl)
                .append(" postsCount ").append(postsCount).append(" title ").append(title)
                .append(" shortBio ").append(shortBio).append(" state ").append(state).toString();
    }
}