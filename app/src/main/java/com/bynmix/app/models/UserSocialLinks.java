package com.bynmix.app.models;

public class UserSocialLinks {

    private int userSocialLinksId;
    private String facebookLink;
    private String pinterestLink;
    private String twitterLink;
    private String googlePlusLink;
    private String instagramLink;

    public String getFacebookLink() {
        return facebookLink;
    }

    public void setFacebookLink(String facebookLink) {
        this.facebookLink = facebookLink;
    }

    public String getPinterestLink() {
        return pinterestLink;
    }

    public void setPinterestLink(String pinterestLink) {
        this.pinterestLink = pinterestLink;
    }

    public String getTwitterLink() {
        return twitterLink;
    }

    public void setTwitterLink(String twitterLink) {
        this.twitterLink = twitterLink;
    }

    public String getGooglePlusLink() {
        return googlePlusLink;
    }

    public void setGooglePlusLink(String googlePlusLink) {
        this.googlePlusLink = googlePlusLink;
    }

    public int getUserSocialLinksId() {
        return userSocialLinksId;
    }

    public void setUserSocialLinksId(int userSocialLinksId) {
        this.userSocialLinksId = userSocialLinksId;
    }

    public String getInstagramLink() {
        return instagramLink;
    }

    public void setInstagramLink(String instagramLink) {
        this.instagramLink = instagramLink;
    }
}
