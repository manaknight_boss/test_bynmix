package com.bynmix.app.networks;

/**
 * Created by manaknight on 2018-05-04.
 */

import com.bynmix.app.models.Address;
import com.bynmix.app.models.AnnotatedPoint;
import com.bynmix.app.models.ApiListResponse;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.BalanceHistory;
import com.bynmix.app.models.BankAccount;
import com.bynmix.app.models.Bloggers;
import com.bynmix.app.models.BodyType;
import com.bynmix.app.models.Brand;
import com.bynmix.app.models.BrandResponse;
import com.bynmix.app.models.CardDetail;
import com.bynmix.app.models.CategoryType;
import com.bynmix.app.models.Colors;
import com.bynmix.app.models.Comments;
import com.bynmix.app.models.Condition;
import com.bynmix.app.models.Descriptor;
import com.bynmix.app.models.Device;
import com.bynmix.app.models.Dispute;
import com.bynmix.app.models.DisputeDetails;
import com.bynmix.app.models.DisputeReasons;
import com.bynmix.app.models.FacebookAndGmail;
import com.bynmix.app.models.FeedResponse;
import com.bynmix.app.models.Feeds;
import com.bynmix.app.models.FilterData;
import com.bynmix.app.models.FilterSizeType;
import com.bynmix.app.models.IssueType;
import com.bynmix.app.models.ListingCheck;
import com.bynmix.app.models.ListingDetail;
import com.bynmix.app.models.ListingDisplayImage;
import com.bynmix.app.models.MyCard;
import com.bynmix.app.models.MyPurchasesStats;
import com.bynmix.app.models.MySalesStats;
import com.bynmix.app.models.NotificationSettings;
import com.bynmix.app.models.OfferDetail;
import com.bynmix.app.models.OfferHistory;
import com.bynmix.app.models.Payout;
import com.bynmix.app.models.PostBloggerIds;
import com.bynmix.app.models.PostBrandIds;
import com.bynmix.app.models.PostDetail;
import com.bynmix.app.models.PostImage;
import com.bynmix.app.models.PostTagIds;
import com.bynmix.app.models.Promotions;
import com.bynmix.app.models.PurchasesDetail;
import com.bynmix.app.models.ResponseList;
import com.bynmix.app.models.Reviews;
import com.bynmix.app.models.SaleCancelReason;
import com.bynmix.app.models.SalesDetail;
import com.bynmix.app.models.Sections;
import com.bynmix.app.models.SellerPersonalInfo;
import com.bynmix.app.models.Settings;
import com.bynmix.app.models.ShippingInfo;
import com.bynmix.app.models.SizeType;
import com.bynmix.app.models.States;
import com.bynmix.app.models.Status;
import com.bynmix.app.models.Stock;
import com.bynmix.app.models.SubOfferHistory;
import com.bynmix.app.models.Tags;
import com.bynmix.app.models.TermsOfServicesModel;
import com.bynmix.app.models.TokenResponse;
import com.bynmix.app.models.TotalSales;
import com.bynmix.app.models.Tracking;
import com.bynmix.app.models.UserFilterData;
import com.bynmix.app.models.UserResponse;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiEndpointInterface {

    @POST("token")
    @FormUrlEncoded
    Call<TokenResponse> login(@Field("username") String username,
                              @Field("password") String password,
                              @Field("grant_type") String grant_type,
                              @Field("client_id") String client_id,
                              @Field("client_secret") String client_secret);

    @GET("api/users")
    Call<ApiResponse<UserResponse>> getUser(@Header("Authorization") String authorization);

    @POST("api/accounts/register")
    Call<ResponseBody> register(@Body UserResponse registerUser);

    @GET("api/onboarding/brands")
    Call<ApiResponse<List<Brand>>> getBrands(@Header("Authorization") String authorization, @Query("search") String search);

    @POST("api/onboarding/brands")
    Call<ApiResponse<ResponseBody>> postBrands(@Header("Authorization") String authorization, @Body PostBrandIds brandIds);

    @GET("api/onboarding/bloggers")
    Call<ApiResponse<Bloggers>> getBloggers(@Header("Authorization") String authorization, @Query("search") String search, @Query("page") int page);

    @POST("api/onboarding/bloggers")
    Call<ApiResponse<ResponseBody>> postBloggers(@Header("Authorization") String authorization, @Body PostBloggerIds bloggerIds);

    @GET("api/onboarding/descriptors")
    Call<ApiResponse<List<Descriptor>>> getTags(@Header("Authorization") String authorization, @Query("search") String search);

    @GET("api/users/styles")
    Call<ApiResponse<List<Descriptor>>> getDescriptors(@Header("Authorization") String authorization, @Query("search") String search);

    @POST("api/onboarding/descriptors")
    Call<ApiResponse<ResponseBody>> postTags(@Header("Authorization") String authorization, @Body PostTagIds tagIds);

    @PUT("api/users")
    @FormUrlEncoded
    Call<ApiResponse<UserResponse>> putOnBoardingTrue(@Header("Authorization") String authorization, @Field("onboardingComplete") boolean isOnBoarding);

    @POST("token")
    @FormUrlEncoded
    Call<TokenResponse> accessToken(@Field("refresh_token") String refresh_token,
                                    @Field("grant_type") String grant_type,
                                    @Field("client_id") String client_id);

    @POST("api/accounts/oauth/login")
    Call<TokenResponse> fbAndGmailLogin(@Body FacebookAndGmail facebookAndGmail);


    @GET("api/users/best-match?pageSize=25")
    Call<ApiResponse<Bloggers>> getBestUser(@Header("Authorization") String authorization);


    @PUT("api/feed/all")
    Call<ApiResponse<Feeds>> getAllFeeds(@Header("Authorization") String authorization, @Body FilterData filterData, @Query("page") int page);

    @PUT("api/feed/listings")
    Call<ApiResponse<Feeds>> putListingFeeds(@Header("Authorization") String authorization, @Body FilterData filterData, @Query("page") int page);

    @PUT("api/feed/posts")
    Call<ApiResponse<Feeds>> putPostFeeds(@Header("Authorization") String authorization, @Body FilterData filterData, @Query("page") int page);

    @GET("api/users")
    Call<ApiResponse<UserResponse>> getMyProfile(@Header("Authorization") String authorization);

    @PUT("api/listings/{resource}/like")
    Call<ApiResponse<Feeds>> putListingLikes(@Header("Authorization") String authorization, @Path("resource") String resource);

    @PUT("api/posts/{resource}/like")
    Call<ApiResponse<Feeds>> putPostLikes(@Header("Authorization") String authorization, @Path("resource") String resource);

    @GET("api/users/{resource}")
    Call<ApiResponse<UserResponse>> getProfile(@Header("Authorization") String authorization, @Path("resource") String resource);

    @PUT("api/users/{resource}/listings")
    Call<ApiResponse<Feeds>> putMyBynFeeds(@Header("Authorization") String authorization, @Path("resource") String resource, @Body FilterData filterData, @Query("sortDirection") String sort, @Query("page") int page);

    @PUT("api/users/{resource}/posts")
    Call<ApiResponse<Feeds>> putMyPostFeeds(@Header("Authorization") String authorization, @Path("resource") String resource, @Body FilterData filterData, @Query("sortDirection") String sort, @Query("page") int page);

    @PUT("api/users/follow/{resource}")
    Call<ApiResponse<UserResponse>> putFollower(@Header("Authorization") String authorization, @Path("resource") String resource);

    @GET("api/users/follow/followings")
    Call<ApiResponse<Bloggers>> getBestUserForListingAndPost(@Header("Authorization") String authorization);

    @GET("api/users/follow/{resource}/followers")
    Call<ApiResponse<Bloggers>> getFollower(@Header("Authorization") String authorization, @Path("resource") String resource, @Query("page") int page);

    @GET("api/users/follow/{resource}/followings")
    Call<ApiResponse<Bloggers>> getFollowing(@Header("Authorization") String authorization, @Path("resource") String resource, @Query("page") int page);

    @PUT("api/browse/all")
    Call<ApiResponse<Feeds>> getBrowseAllFeeds(@Header("Authorization") String authorization, @Body FilterData filterData, @Query("page") int page);

    @PUT("api/browse/listings")
    Call<ApiResponse<Feeds>> putBrowseListingFeeds(@Header("Authorization") String authorization, @Body FilterData filterData, @Query("page") int page);

    @PUT("api/browse/posts")
    Call<ApiResponse<Feeds>> putBrowsePostFeeds(@Header("Authorization") String authorization, @Body FilterData filterData, @Query("page") int page);

    @PUT("api/users/{resource}/images")
    @Multipart
    Call<ApiResponse<String>> putProfilePhoto(@Header("Authorization") String authorization, @Path("resource") String resource, @Part MultipartBody.Part image);

    @PUT("api/users/{resource}/images/background")
    @Multipart
    Call<ApiResponse<String>> putBackgroundPhoto(@Header("Authorization") String authorization, @Path("resource") String resource, @Part MultipartBody.Part image);

    @GET("api/settings/states")
    Call<ApiListResponse<List<States>>> getState(@Header("Authorization") String authorization);

    @PUT("api/users/posts")
    Call<ApiResponse<Feeds>> getMyPostFeeds(@Header("Authorization") String authorization, @Query("sortDirection") String sort, @Query("page") int page);

    @GET("api/posts/{resource}")
    Call<ApiResponse<PostDetail>> getMyEditPost(@Header("Authorization") String authorization, @Path("resource") String resource);

    @PUT("api/users/listings")
    Call<ApiResponse<Feeds>> getMyBynFeeds(@Header("Authorization") String authorization, @Body FilterData filterData, @Query("sortDirection") String sort, @Query("page") int page);

    @POST("api/posts")
    Call<ApiResponse<Integer>> sendPostData(@Header("Authorization") String authorization, @Body FeedResponse post);

    @POST("api/posts/{resource}/images")
    @Multipart
    Call<ApiResponse<List<UserResponse>>> sendPostImage(@Header("Authorization") String authorization, @Path("resource") String resource, @Part MultipartBody.Part image);

    @GET("api/listings/{resource}")
    Call<ApiResponse<ListingDetail>> listingDetailById(@Header("Authorization") String authorization, @Path("resource") String resource);

    @PUT("api/listings/{resource}")
    Call<ApiResponse<ResponseBody>> updateListingDetailById(@Header("Authorization") String authorization, @Path("resource") String resource, @Body ListingDetail listingDetail);

    @GET("api/listings/{resource}/images")
    Call<ApiResponse<List<ListingDisplayImage>>> listingDetailImagesById(@Header("Authorization") String authorization, @Path("resource") String resource);

    @GET("api/listings/{resource}/like/all")
    Call<ApiResponse<Bloggers>> getListingLikes(@Header("Authorization") String authorization, @Path("resource") String resource);

    @GET("api/posts/{resource}/like/all")
    Call<ApiResponse<Bloggers>> getPostLikes(@Header("Authorization") String authorization, @Path("resource") String resource);

    @GET("api/listings/filters/categories")
    Call<ApiResponse<List<CategoryType>>> getCategory(@Header("Authorization") String authorization);

    @GET("api/listings/filters/sizes")
    Call<ApiResponse<List<SizeType>>> getSize(@Header("Authorization") String authorization, @Query("fit") String sizeTitle);

    @GET("api/listings/filters/colors")
    Call<ApiResponse<List<Colors>>> getColors(@Header("Authorization") String authorization);

    @GET("api/listings/filters/brands")
    Call<ApiResponse<BrandResponse>> getBrandsListing(@Header("Authorization") String authorization, @Query("pageSize") int page);

    @POST("api/listings/{resource}/comment")
    @FormUrlEncoded
    Call<ApiResponse<ResponseBody>> addComment(@Header("Authorization") String authorization, @Path("resource") String resource, @Field("comment") String comment);

    @GET("api/listings/filters/size-fits")
    Call<ApiResponse<List<String>>> getSizeType(@Header("Authorization") String authorization);

    @POST("api/listings")
    Call<ApiResponse<Integer>> sendListingData(@Header("Authorization") String authorization, @Body ListingDetail listingDetail);

    @POST("api/listings/{resource}/images")
    @Multipart
    Call<ApiResponse<List<String>>> sendListingImage(@Header("Authorization") String authorization, @Path("resource") String resource, @Part List<MultipartBody.Part> images);

    @PUT("api/users/listing-likes")
    Call<ApiResponse<Feeds>> getMyBynLikes(@Header("Authorization") String authorization, @Body FilterData filterData, @Query("sort") String liked, @Query("sortDirection") String sort, @Query("page") int page);

    @GET("api/users/post-likes")
    Call<ApiResponse<Feeds>> getMyPostLikes(@Header("Authorization") String authorization, @Query("sortDirection") String sort, @Query("page") int page);

    @GET("api/shipping/parcels/shipping-rates")
    Call<ApiResponse<List<ShippingInfo>>> getShippingInfo(@Header("Authorization") String authorization);

    @PUT("api/posts/{resource}/active")
    Call<ApiResponse<Feeds>> enableDisablePost(@Header("Authorization") String authorization, @Path("resource") String resource);

    @DELETE("api/posts/{resource}")
    Call<ApiResponse<Feeds>> deletePost(@Header("Authorization") String authorization, @Path("resource") String resource);

    @PUT("api/posts/{resource}")
    Call<ApiResponse<ResponseBody>> editPostData(@Header("Authorization") String authorization, @Path("resource") String resource, @Body FeedResponse post);

    @POST("api/settings/address")
    Call<ApiResponse<Integer>> addAddress(@Header("Authorization") String authorization, @Body Address address);

    @GET("api/settings/address")
    Call<ApiResponse<List<Address>>> getMyAddress(@Header("Authorization") String authorization);

    @DELETE("api/settings/address/{resource}")
    Call<ApiResponse<Feeds>> deleteAddress(@Header("Authorization") String authorization, @Path("resource") String resource);

    @PUT("api/settings/address/{resource}")
    Call<ApiResponse<ResponseBody>> editAddress(@Header("Authorization") String authorization, @Path("resource") String resource, @Body Address address);

    @GET("api/listings/filters/sizes")
    Call<ApiResponse<List<FilterSizeType>>> getFilterSize(@Header("Authorization") String authorization, @Query("filterView") boolean filterView);

    @GET("api/posts/{resource}/images")
    Call<ApiResponse<List<PostImage>>> getImageId(@Header("Authorization") String authorization, @Path("resource") String resource);

    @HTTP(method = "DELETE", path = "api/posts/{resource}/images", hasBody = true)
    @FormUrlEncoded
    Call<ApiResponse<ResponseBody>> deletePostImage(@Header("Authorization") String authorization, @Path("resource") String resource, @Field("imageIds") List<Integer> imageIds);

    @PUT("api/listings/{resource}/active")
    Call<ApiResponse<Feeds>> enableDisableListing(@Header("Authorization") String authorization, @Path("resource") String resource);

    @DELETE("api/listings/{resource}")
    Call<ApiResponse<Feeds>> deleteListing(@Header("Authorization") String authorization, @Path("resource") String resource);

    @HTTP(method = "DELETE", path = "api/listings/{resource}/images", hasBody = true)
    @FormUrlEncoded
    Call<ApiResponse<ResponseBody>> deleteListingImages(@Header("Authorization") String authorization, @Path("resource") String resource, @Field("imageIds") List<Integer> imageIds);

    @POST("api/transactions/card")
    Call<ApiResponse<ResponseBody>> addPayment(@Header("Authorization") String authorization, @Body MyCard myCard);

    @GET("api/transactions/card")
    Call<ApiResponse<List<CardDetail>>> getWalletDetail(@Header("Authorization") String authorization);

    @GET("api/transactions/payout/history")
    Call<ApiResponse<BalanceHistory>> getBalanceHistory(@Header("Authorization") String authorization, @Query("sortDirection") String sort, @Query("page") int page);

    @GET("api/users/totals/sales")
    Call<ApiResponse<TotalSales>> getTotalBalance(@Header("Authorization") String authorization);

    @DELETE("api/transactions/card/{resource}")
    Call<ApiResponse<CardDetail>> deleteCardDetail(@Header("Authorization") String authorization, @Path("resource") String resource);

    @PUT("api/transactions/card/{resource}/default")
    Call<ApiResponse<TotalSales>> SetAsDefaultCard(@Header("Authorization") String authorization, @Path("resource") String resource);

    @GET("api/transactions/card/{resource}/check")
    Call<ApiResponse<Boolean>> checkCard(@Header("Authorization") String authorization, @Path("resource") String resource);

    @PUT("api/transactions/card/{resource}")
    Call<ApiResponse<ResponseBody>> editPayment(@Header("Authorization") String authorization, @Path("resource") String resource, @Body MyCard myCard);

    @DELETE("api/transactions/card/{resource}/replace/{resource2}")
    Call<ApiResponse<ResponseBody>> setNewPayment(@Header("Authorization") String authorization, @Path("resource") String resource, @Path("resource2") String resource2);

    @GET("api/listings/check")
    Call<ApiResponse<ListingCheck>> checkListing(@Header("Authorization") String authorization);

    @GET("api/settings/user-notifications")
    Call<ApiResponse<List<NotificationSettings>>> getNotificationSettings(@Header("Authorization") String authorization);

    @PUT("api/settings")
    Call<ApiResponse<ResponseBody>> updateNotificationSettings(@Header("Authorization") String authorization, @Body Settings settings);

    @GET("api/notifications/count")
    Call<ApiResponse<Integer>> getNotificationCount(@Header("Authorization") String authorization);

    @GET("api/notifications")
    Call<ApiResponse<Settings>> getNotifications(@Header("Authorization") String authorization, @Query("page") int page, @Query("sortDirection") String sort);

    @PUT("api/notifications")
    Call<ApiResponse<ResponseBody>> decreaseCount(@Header("Authorization") String authorization, @Body Settings settings);

    @GET("api/transactions/card/{resource}")
    Call<ApiResponse<CardDetail>> getMyWalletCard(@Header("Authorization") String authorization, @Path("resource") String resource);

    @POST("api/accounts/password/send-code")
    Call<ResponseBody> sendCode(@Body UserResponse registerUser);

    @PUT("api/accounts/password/verify-code")
    Call<ResponseBody> verifyCode(@Body UserResponse registerUser);

    @PUT("api/accounts/password/reset")
    Call<ResponseBody> resetPassword(@Body UserResponse registerUser);

    @GET("api/listings/{resource}/actions/offer-history")
    Call<ApiResponse<OfferHistory>> offerHistory(@Header("Authorization") String authorization, @Path("resource") String resource, @Query("sortDirection") String sort);

    @GET("api/transactions/card/default")
    Call<ApiResponse<CardDetail>> getDefaultCard(@Header("Authorization") String authorization);

    @GET("api/settings/address/default-shipping")
    Call<ApiResponse<Address>> getDefaultAddress(@Header("Authorization") String authorization);

    @POST("api/listings/{resource}/actions/buyer-offer")
    Call<ApiResponse<ResponseBody>> offerSubmit(@Header("Authorization") String authorization, @Path("resource") String resource, @Body SubOfferHistory offerHistory);

    @PUT("api/listings/{resource}/actions/offer/{resource2}/cancel")
    Call<ApiResponse<ResponseBody>> cancelOffer(@Header("Authorization") String authorization, @Path("resource") String resource, @Path("resource2") String resource2);

    @GET("api/settings/address/default-return")
    Call<ApiResponse<Address>> getDefaultReturnAddress(@Header("Authorization") String authorization);

    @GET("api/notifications/offers/my-offers")
    Call<ApiResponse<Settings>> getMyOffersNotifications(@Header("Authorization") String authorization, @Query("page") int page, @Query("sortDirection") String sort);

    @GET("api/notifications/offers/received-offers")
    Call<ApiResponse<Settings>> getReceivedOffersNotifications(@Header("Authorization") String authorization, @Query("page") int page, @Query("sortDirection") String sort);

    @POST("api/listings/{resource}/actions/quick-buy")
    Call<ApiResponse<ResponseBody>> quickBuy(@Header("Authorization") String authorization, @Path("resource") String resource, @Body SubOfferHistory offerHistory);

    @GET("api/listings/{resource}/actions/buyer-bids")
    Call<ApiResponse<OfferDetail>> getBuyerBids(@Header("Authorization") String authorization, @Path("resource") String resource, @Query("page") int page);

    @GET("api/listings/filters/statuses")
    Call<ApiResponse<List<Status>>> getListingStatus(@Header("Authorization") String authorization);

    @GET("api/purchases")
    Call<ApiResponse<ResponseList<PurchasesDetail>>> getMyPurchases(@Header("Authorization") String authorization, @Query("sortDirection") String sort);

    @POST("api/listings/{resource}/actions/offer/{resource2}/accept")
    Call<ApiResponse<ResponseBody>> acceptOffer(@Header("Authorization") String authorization, @Path("resource") String resource, @Path("resource2") String resource2);

    @POST("api/listings/{resource}/actions/offer/{resource2}/decline")
    Call<ApiResponse<ResponseBody>> declineOffer(@Header("Authorization") String authorization, @Path("resource") String resource, @Path("resource2") String resource2);

    @POST("api/listings/{resource}/actions/seller-offer/{resource2}")
    Call<ApiResponse<ResponseBody>> counterOffer(@Header("Authorization") String authorization, @Path("resource") String resource, @Path("resource2") String resource2, @Body SubOfferHistory offerHistory);

    @GET("api/listings/{resource}/actions/offer-history/{resource2}")
    Call<ApiResponse<OfferHistory>> offerHistorySeller(@Header("Authorization") String authorization, @Path("resource") String resource, @Path("resource2") String resource2, @Query("sortDirection") String sort);

    @POST("api/purchases/{resource}/buyer-feedback")
    Call<ApiResponse<ResponseBody>> sendPurchaseRating(@Header("Authorization") String authorization, @Path("resource") String resource, @Body Stock detail);

    @POST("api/sales/{resource}/seller-feedback")
    Call<ApiResponse<ResponseBody>> sendSaleRating(@Header("Authorization") String authorization, @Path("resource") String resource, @Body Stock detail);

    @GET("api/users/totals/purchases")
    Call<ApiResponse<MyPurchasesStats>> myStats(@Header("Authorization") String authorization);

    @PUT("api/users/app-device")
    Call<ApiResponse<ResponseBody>> pushNotification(@Header("Authorization") String authorization, @Body Device device);

    @GET("api/purchases/{resource}/tracking")
    Call<ApiResponse<Tracking>> trackInformation(@Header("Authorization") String authorization, @Path("resource") String resource);

    @GET("api/sales")
    Call<ApiResponse<ResponseList<SalesDetail>>> getMySales(@Header("Authorization") String authorization, @Query("sortDirection") String sort);

    @GET("api/users/totals/sales")
    Call<ApiResponse<MySalesStats>> mySalesStats(@Header("Authorization") String authorization);

    @GET("api/purchases/{resource}")
    Call<ApiResponse<PurchasesDetail>> getPurchasesDetail(@Header("Authorization") String authorization, @Path("resource") String resource);

    @GET("api/sales/{resource}")
    Call<ApiResponse<SalesDetail>> getSaleDetail(@Header("Authorization") String authorization, @Path("resource") String resource);

    @GET("api/posts/{resource}/dots")
    Call<ApiResponse<List<AnnotatedPoint>>> getPostTags(@Header("Authorization") String authorization, @Path("resource") String resource);

    @GET("api/listings/title")
    Call<ApiResponse<Feeds>> getListingTitles(@Header("Authorization") String authorization, @Query("search") String search, @Query("page") int page);

    @DELETE("api/posts/{resource}/dots")
    Call<ApiResponse<ResponseBody>> deletePostDots(@Header("Authorization") String authorization, @Path("resource") String resource);

    @GET("api/users/bodyTypes")
    Call<ApiResponse<List<BodyType>>> getBodyType(@Header("Authorization") String authorization);

    @POST("api/listings/{resource}/relist")
    Call<ApiResponse<Integer>> relistItem(@Header("Authorization") String authorization, @Path("resource") String resource);

    @GET("api/sales/cancel-reasons")
    Call<ApiResponse<List<SaleCancelReason>>> cancelSaleReason(@Header("Authorization") String authorization);

    @POST("api/sales/{resource}/cancel")
    Call<ApiResponse<Boolean>> cancelOrder(@Header("Authorization") String authorization, @Path("resource") String resource, @Body SaleCancelReason saleCancelReason);

    @GET("api/listings/filters/conditions")
    Call<ApiResponse<List<Condition>>> getConditionTypes(@Header("Authorization") String authorization);

    @GET("api/posts/{resource}")
    Call<ApiResponse<FeedResponse>> getPostDetail(@Header("Authorization") String authorization, @Path("resource") String resource);

    @GET("api/info/TOS")
    Call<ApiResponse<TermsOfServicesModel>> getTermsOfServices();

    @GET("api/info/about")
    Call<ApiResponse<TermsOfServicesModel>> getAbout();

    @GET("api/info/privacy-policy")
    Call<ApiResponse<TermsOfServicesModel>> getPrivacyPolicy();

    @PUT("api/users/search")
    Call<ApiResponse<Bloggers>> searchUser(@Header("Authorization") String authorization, @Body UserFilterData filterData, @Query("page") int page);

    @POST("api/sales/{resource}/shipping-label")
    Call<ApiResponse<String>> emailShippingLabel(@Header("Authorization") String authorization, @Path("resource") String resource);

    @GET("api/users/feedback/{resource}/as-buyer")
    Call<ApiResponse<Reviews>> fetchBuyerReviews(@Header("Authorization") String authorization, @Path("resource") String resource, @Query("sortDirection") String sort, @Query("page") int page);

    @GET("api/users/feedback/{resource}/as-seller")
    Call<ApiResponse<Reviews>> fetchSellerReviews(@Header("Authorization") String authorization, @Path("resource") String resource, @Query("sortDirection") String sort, @Query("page") int page);

    @GET("api/disputes/{resource}/check")
    Call<ApiResponse<ListingCheck>> checkReportAnIssue(@Header("Authorization") String authorization, @Path("resource") String resource);

    @POST("api/disputes")
    Call<ApiResponse<Integer>> reportAnIssue(@Header("Authorization") String authorization, @Body Dispute dispute);

    @GET("api/reported-issues/issue-types")
    Call<ApiResponse<List<IssueType>>> getIssueTypes(@Header("Authorization") String authorization);

    @GET("api/disputes/dispute-types")
    Call<ApiResponse<List<DisputeReasons>>> disputeReasons(@Header("Authorization") String authorization);

    @POST("api/disputes/{resource}/images")
    @Multipart
    Call<ApiResponse<List<String>>> reportAnIssueImages(@Header("Authorization") String authorization, @Path("resource") String resource, @Part List<MultipartBody.Part> images);

    @GET("api/disputes/{resource}/details")
    Call<ApiResponse<DisputeDetails>> getDisputeDetails(@Header("Authorization") String authorization, @Path("resource") String resource);

    @POST("api/transactions/account")
    Call<ApiResponse<String>> sendSellerDetail(@Header("Authorization") String authorization, @Body SellerPersonalInfo sellerPersonalInfo);

    @PUT("api/disputes/{resource}/cancel")
    Call<ApiResponse<DisputeDetails>> cancelDispute(@Header("Authorization") String authorization, @Path("resource") String resource);

    @POST("api/disputes/{resource}/comment")
    Call<ApiResponse<Integer>> addDisputeComment(@Header("Authorization") String authorization, @Path("resource") String resource, @Body Comments comments);

    @GET("api/transactions/payout/bank-accounts")
    Call<ApiResponse<List<BankAccount>>> getPayout(@Header("Authorization") String authorization);

    @POST("api/transactions/payout")
    Call<ApiResponse<Payout>> withdrawAmount(@Header("Authorization") String authorization, @Body Payout payout);

    @POST("api/transactions/payout/{resource}")
    Call<ApiResponse<Payout>> withdrawAmountWithId(@Header("Authorization") String authorization, @Path("resource") String resource, @Body Payout payout);

    @POST("api/purchases/{resource}/cancel")
    Call<ApiResponse<Boolean>> canelPurchase(@Header("Authorization") String authorization, @Path("resource") String resource);

    @PUT("api/users")
    Call<ApiResponse<UserResponse>> putOnEditUserProfile(@Header("Authorization") String authorization, @Body UserResponse user);

    @GET("api/promotions")
    Call<ApiResponse<List<Promotions>>> getPromotions(@Header("Authorization") String authorization);

    @GET("api/listings/seller-policy")
    Call<ApiResponse<TermsOfServicesModel>> getSellerPolicy(@Header("Authorization") String authorization);

    @POST("api/reported-issues/report")
    Call<ApiResponse<Integer>> reportAnIssueListing(@Header("Authorization") String authorization,@Body Dispute dispute);
}
