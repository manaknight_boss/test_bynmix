package com.bynmix.app.networks;

import android.util.Log;

import com.bynmix.app.BuildConfig;
import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Ryan Wong on 2018-05-03.
 */

public class ApiService {
    private static Retrofit client = null;

    public ApiService() {

    }

    public static ApiEndpointInterface instance() {
        if (client == null) {
            final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(20, TimeUnit.MINUTES)
                    .writeTimeout(20, TimeUnit.MINUTES)
                    .readTimeout(30, TimeUnit.MINUTES)
                    .build();
            client = new Retrofit.Builder()
                    .client(okHttpClient)
                    .baseUrl(BuildConfig.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(new Gson()))
                    .build();
            Log.d("Base_URL", BuildConfig.BASE_URL);
        }

        return client.create(ApiEndpointInterface.class);
    }
}
