package com.bynmix.app.sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;

import com.bynmix.app.R;


public class SharedPreferenceUtility {

    private final SharedPreferences mSharedPreferences;
    private static final String IS_APP_OPEN_FIRST_TIME = "IS_APP_OPEN_FIRST_TIME";
    private static final String MY_USER_ID = "MY_USER_ID";
    private static final String DISPLAY_NAME = "DISPLAY_NAME";
    private static final String IS_OFFER_NOTIFICATION = "IS_OFFER_NOTIFICATION";
    private static final String CLOUD_MESSAGING_TOKEN = "CLOUD_MESSAGING_TOKEN";
    private static final String PROMOTION_ID = "PROMOTION_ID";

    public SharedPreferenceUtility(Context context) {
        mSharedPreferences = context.getSharedPreferences(context.getString(R.string.shared_preference_file),
                Context.MODE_PRIVATE);
    }

    public void setIsAppOpenFirstTime(boolean isDone) {
        mSharedPreferences.edit()
                .putBoolean(IS_APP_OPEN_FIRST_TIME, isDone)
                .apply();
    }

    public boolean getAppOpenFirstTime() {
        return mSharedPreferences.getBoolean(IS_APP_OPEN_FIRST_TIME, false);
    }

    public void setIsOfferNotification(boolean isOfferNotification) {
        mSharedPreferences.edit()
                .putBoolean(IS_OFFER_NOTIFICATION, isOfferNotification)
                .apply();
    }

    public boolean getOfferNotification() {
        return mSharedPreferences.getBoolean(IS_OFFER_NOTIFICATION, false);
    }

    public void setMyUserId(int userId) {
        mSharedPreferences.edit()
                .putInt(MY_USER_ID, userId)
                .apply();
    }

    public int getMyUserId() {
        return mSharedPreferences.getInt(MY_USER_ID, 0);
    }

    public void setPromotionId(int promotionId) {
        mSharedPreferences.edit()
                .putInt(PROMOTION_ID, promotionId)
                .apply();
    }

    public int getPromotionId() {
        return mSharedPreferences.getInt(PROMOTION_ID, 0);
    }


    public void setDisplayName(String displayName) {
        mSharedPreferences.edit()
                .putString(DISPLAY_NAME, displayName)
                .apply();
    }

    public String getDisplayName() {
        return mSharedPreferences.getString(DISPLAY_NAME, null);
    }

    public void setCloudMessagingToken(String token) {
        mSharedPreferences.edit()
                .putString(CLOUD_MESSAGING_TOKEN, token)
                .apply();
    }

    public String getCloudMessagingToken() {
        return mSharedPreferences.getString(CLOUD_MESSAGING_TOKEN, null);
    }

    public void clear() {
        mSharedPreferences.edit().clear().apply();
    }
}
