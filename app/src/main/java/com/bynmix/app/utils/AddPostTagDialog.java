package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.bynmix.app.R;
import com.bynmix.app.databinding.AddTagsDialogBinding;
import com.bynmix.app.models.AddTagsModel;

import java.util.ArrayList;
import java.util.List;

public class AddPostTagDialog {
    private AddTagsDialogBinding mBinding;
    private int count = 0;
    private List<AddTagsModel> mList = new ArrayList<>();

    public void showDialog(final Context mContext) {
        final Dialog dialog = new Dialog(mContext);
        addData(mContext);
        mBinding = AddTagsDialogBinding.inflate(LayoutInflater.from(mContext));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = mBinding.getRoot();
        dialog.setContentView(view);
        updateView(mContext);
        mBinding.crossIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mBinding.videoView.isPlaying()) {
                    mBinding.videoView.stopPlayback();
                }
                dialog.dismiss();
            }
        });
        mBinding.nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                count++;
                if (count >= mList.size()) {
                    mBinding.videoView.stopPlayback();
                    dialog.dismiss();
                } else {
                    updateView(mContext);
                }

            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }

    private void initializeBackgroundVideo(Context mContext) {
        Uri uri = Uri.parse("android.resource://" + mContext.getPackageName() + "/" + R.raw.how_to_delete_a_post_dot);
        mBinding.videoView.setVideoURI(uri);
        mBinding.videoView.start();
        mBinding.videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                mBinding.videoView.start();
            }
        });
    }

    private void updateView(Context mContext) {
        mBinding.imagesLayout.setVisibility(View.GONE);
        mBinding.buttonText.setText(mList.get(count).getButtonText());
        mBinding.heading.setText(mList.get(count).getHeading());
        if (!TextUtils.isEmpty(mList.get(count).getTitle())) {
            mBinding.title.setVisibility(View.VISIBLE);
            mBinding.title.setText(mList.get(count).getTitle());
        } else {
            mBinding.title.setVisibility(View.GONE);
        }
        if (mList.get(count).isHasVideo()) {
            initializeBackgroundVideo(mContext);
            mBinding.image.setVisibility(View.INVISIBLE);
            mBinding.videoView.setVisibility(View.VISIBLE);
        } else {
            if (mBinding.videoView.isPlaying()) {
                mBinding.videoView.stopPlayback();
            }
            if (count == mList.size() - 1) {
                mBinding.imagesLayout.setVisibility(View.VISIBLE);
                mBinding.mediaLayout.setVisibility(View.GONE);
            } else {
                mBinding.image.setVisibility(View.VISIBLE);
                mBinding.image.setImageResource(mList.get(count).getImage());
            }
            mBinding.videoView.setVisibility(View.INVISIBLE);
        }
    }

    private void addData(Context mContext) {
        mList.add(new AddTagsModel(mContext.getString(R.string.dots_flow_clicked_1_heading), mContext.getString(R.string.dots_flow_clicked_1_title), mContext.getString(R.string.dots_flow_clicked_1_button_text), R.mipmap.post_tags, false));
        mList.add(new AddTagsModel(mContext.getString(R.string.dots_flow_clicked_2_heading), mContext.getString(R.string.dots_flow_clicked_2_title), mContext.getString(R.string.dots_flow_clicked_2_button_text), R.mipmap.add_post_tags, false));
        mList.add(new AddTagsModel(mContext.getString(R.string.dots_flow_clicked_3_heading), mContext.getString(R.string.dots_flow_clicked_3_title), mContext.getString(R.string.dots_flow_clicked_3_button_text), R.mipmap.add_post_tags, true));
        mList.add(new AddTagsModel(mContext.getString(R.string.dots_flow_clicked_4_heading), "", mContext.getString(R.string.dots_flow_clicked_4_button_text), R.mipmap.step_one, false));
    }
}
