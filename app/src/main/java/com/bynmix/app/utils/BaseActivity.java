package com.bynmix.app.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.support.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;
import android.widget.VideoView;

import com.bynmix.app.BuildConfig;
import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.activities.MainActivity;
import com.bynmix.app.activities.onboarding.SelectBloggerActivity;
import com.bynmix.app.activities.onboarding.SelectTagsActivity;
import com.bynmix.app.activities.onboarding.WelcomeActivity;
import com.bynmix.app.activities.onboarding.WelcomeProfileInfoActivity;
import com.bynmix.app.interfaces.AccessTokenResponse;
import com.bynmix.app.interfaces.NetworkListener;
import com.bynmix.app.interfaces.PermissionRequestCallBacks;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.Device;
import com.bynmix.app.models.FacebookAndGmail;
import com.bynmix.app.models.ImageData;
import com.bynmix.app.models.TokenResponse;
import com.bynmix.app.models.UserResponse;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.bynmix.app.sharedpreference.SharedPreferenceUtility;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BaseActivity extends AppCompatActivity implements NetworkListener {

    public VideoView videoView;
    public String DEBUGTAG = "DEBUG";
    public String ERRORTAG = "ERROR";
    public Constants config = new Constants();
    public SharedPreferences preferences;
    public DialogUtility mDialogUtility;
    public ProgressBarDialog mProgressBarDialog;
    private SharedPreferenceUtility mSharedPreferenceUtility;
    public CustomTextDialog mAlertDialog;
    protected android.app.ProgressDialog mProgressDialog;
    private PermissionRequestCallBacks mPermissionRequestListener;
    private static final String IS_FIRST_TIME_PERMISSION_REQUEST = "is_first_time_permission_request";
    private boolean isRefreshAPICalled = false;
    private List<AccessTokenResponse> refreshAPIQueue = new ArrayList<>();
    protected void init() {
        preferences = BaseActivity.this.getSharedPreferences(BaseActivity.this.getString(R.string.shared_preference_file), Context.MODE_PRIVATE);
    }

    public void generateAppToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        try {
                            String token = task.getResult().getToken();
                            mSharedPreferenceUtility.setCloudMessagingToken(token);
                            Log.d("token", token);
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Fabric.with(this, new Crashlytics());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        mDialogUtility = new DialogUtility(this);
        mProgressBarDialog = new ProgressBarDialog(this);
        mProgressDialog = new android.app.ProgressDialog(this);
        init();
        mSharedPreferenceUtility = new SharedPreferenceUtility(this);
        mAlertDialog = new CustomTextDialog();
        generateAppToken();
    }

    protected void initializeBackgroundVideo(int filename) {
        videoView = (VideoView) findViewById(R.id.videoView);
        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + filename);
        videoView.setVideoURI(uri);
        videoView.start();

        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                videoView.start();
            }
        });
    }

    protected boolean isInternetAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public boolean conditionsForApiCall() {
        boolean valid = true;

        if (!isInternetAvailable()) {
            Toast.makeText(this, translateString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
            valid = false;
        }

        return valid;
    }

    protected void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    protected boolean validate() {
        return true;
    }

    public String translateString(int id) {
        return getString(id);
    }

    public String getAccessToken() {
        return preferences.getString(config.ACCESS_TOKEN, "");
    }

    protected String getTempToken() {
        return preferences.getString(config.TEMP_TOKEN, "");
    }

    protected String getRefreshToken() {
        return preferences.getString(config.REFRESH_TOKEN, "");
    }

    protected void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) BaseActivity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
        inputManager.hideSoftInputFromWindow(BaseActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

    }

    public void getAccessTokenFromServer(final AccessTokenResponse accessTokenResponse) {
        if(!isRefreshAPICalled) {
            isRefreshAPICalled = true;
            Log.d("refresh_token", "Refresh token called");
            ApiEndpointInterface apiInstance = ApiService.instance();
            final String refreshToken = getRefreshToken();
            Log.d("refresh_token", "Refresh Token : " + refreshToken);
            if (!TextUtils.isEmpty(refreshToken)) {
                apiInstance.accessToken(refreshToken,
                        config.REFRESH_TOKEN,
                        BuildConfig.CLIENT_ID).enqueue(new Callback<TokenResponse>() {
                    @Override
                    public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                        Log.d("refresh_token", "Response code: " + response.code());
                        if (response.isSuccessful() && response.code() == 200) {
                            String accessToken = response.body().getAccessToken();
                            String refreshToken = response.body().getRefreshToken();
                            if (!TextUtils.isEmpty(accessToken)) {
                                preferences.edit().putString(config.REFRESH_TOKEN, refreshToken).apply();
                                preferences.edit().putString(config.ACCESS_TOKEN, accessToken).apply();
                                isRefreshAPICalled = false;
                                accessTokenResponse.successAccessToken();
                                callAllPendingApi();
                            }
                        } else if (response.code() == 500) {
                            mDialogUtility.displayMessageDialog(getResources().getString(R.string.error_server));
                            logoutId();
                            accessTokenResponse.errorAccessToken();
                        } else if (response.code() == 400) {
                            // todo check for error "error": "invalid_grant"
//                        accessTokenResponse.successAccessToken();
                            logoutId();
                        } else {
                            logoutId();
                        }

                    }

                    @Override
                    public void onFailure(Call<TokenResponse> call, Throwable t) {
                        logoutId();
                    }
                });
            }
        } else {
            refreshAPIQueue.add(accessTokenResponse);
        }
    }

    private void callAllPendingApi() {
        for(AccessTokenResponse accessTokenResponse : refreshAPIQueue) {
            accessTokenResponse.successAccessToken();
        }
        refreshAPIQueue.clear();
    }

    public void logoutId() {
        mSharedPreferenceUtility.clear();
        preferences.edit().putString(config.ACCESS_TOKEN, null).apply();
        preferences.edit().putString(config.TEMP_TOKEN, null).apply();
        finish();
        openStartingScreen();
    }

    private void openStartingScreen() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


    public void signIn(String email, String firstName, String lastName, String token, String provider) {
        mProgressBarDialog.showProgress();
        ApiEndpointInterface apiInstance = ApiService.instance();
        FacebookAndGmail facebookAndGmail = new FacebookAndGmail();
        facebookAndGmail.setEmail(email);
        facebookAndGmail.setFirstname(firstName);
        facebookAndGmail.setLastname(lastName);
        facebookAndGmail.setAccess_token(token);
        facebookAndGmail.setProvider(provider);
        facebookAndGmail.setMobileRegistration(true);
        facebookAndGmail.setClient_id(BuildConfig.CLIENT_ID);
        facebookAndGmail.setAppDeviceId(mSharedPreferenceUtility.getCloudMessagingToken());
        facebookAndGmail.setAppDeviceType("android");
        apiInstance.fbAndGmailLogin(facebookAndGmail).enqueue(new Callback<TokenResponse>() {

            @Override
            public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {

                if (response.isSuccessful() && response.code() == 200) {
                    preferences.edit().putString(config.TEMP_TOKEN, response.body().getAccessToken()).apply();
                    preferences.edit().putString(config.REFRESH_TOKEN, response.body().getRefreshToken()).apply();
                    String accessToken = response.body().getAccessToken();

                    if (!TextUtils.isEmpty(accessToken)) {
                        sendGetUserGet();
                    } else {
                        mAlertDialog.showDialog(BaseActivity.this, getResources().getString(R.string.error_server));

                        mProgressBarDialog.hideProgress();
                    }
                } else if (response.code() == 500) {
                    mAlertDialog.showDialog(BaseActivity.this, getResources().getString(R.string.error_server));
                    mProgressBarDialog.hideProgress();
                } else {
                    mAlertDialog.showDialog(BaseActivity.this, getResources().getString(R.string.error_server));
                    mProgressBarDialog.hideProgress();
                }
            }

            @Override
            public void onFailure(Call<TokenResponse> call, Throwable t) {
                Log.e(DEBUGTAG, "Unable to submit post to API.");
                mProgressBarDialog.hideProgress();
            }
        });
    }

    protected void sendGetUserGet() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        apiInstance.getUser("Bearer " + getTempToken()).enqueue(new Callback<ApiResponse<UserResponse>>() {
            @Override
            public void onResponse(Call<ApiResponse<UserResponse>> call, Response<ApiResponse<UserResponse>> response) {

                if (response.isSuccessful() && response.code() == 200) {

                    mProgressBarDialog.hideProgress();
                    UserResponse data = response.body().getData();
                    boolean onBoardingComplete = data.isOnboardingComplete();
                    mSharedPreferenceUtility.setMyUserId(data.getUserId());
                    mSharedPreferenceUtility.setDisplayName(data.getUsername());

                    if (onBoardingComplete) {
                        pushNotifications();
                        preferences.edit().putString(config.ACCESS_TOKEN, getTempToken()).apply();
                        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                        videoView.stopPlayback();
                        startActivity(intent);
                        finish();
                    } else {
                        int lastCompletedScreen = data.getLastOnboardingScreenCompleted();
                        switch (lastCompletedScreen) {
                            case 0:
                                openWelcomeBrandsActivity();
                                break;

                            case 1:
                                OpenTagsActivity();
                                break;

                            case 2:
                                OpenBloggerActivity();
                                break;

                            case 3:
                                openWelcomeProfileActivity();
                                break;
                        }


                    }

                } else if (response.code() == 500) {
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(BaseActivity.this, getResources().getString(R.string.error_server));
                } else {
                    mAlertDialog.showDialog(BaseActivity.this, getResources().getString(R.string.error_server));
                    mProgressBarDialog.hideProgress();
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<UserResponse>> call, Throwable t) {
                Log.e(DEBUGTAG, "Unable to login to API.");
                mProgressBarDialog.hideProgress();
            }
        });
    }

    public void openWelcomeBrandsActivity() {
        Intent intent = new Intent(getApplicationContext(), WelcomeActivity.class);
        videoView.stopPlayback();
        startActivity(intent);
        finish();
    }

    protected void pushNotifications() {
        ApiEndpointInterface apiInstance = ApiService.instance();
        Device device = new Device();
        device.setDeviceId(mSharedPreferenceUtility.getCloudMessagingToken());
        device.setDeviceType("android");
        apiInstance.pushNotification("Bearer " + getAccessToken(), device).enqueue(new Callback<ApiResponse<ResponseBody>>() {
            @Override
            public void onResponse(Call<ApiResponse<ResponseBody>> call, Response<ApiResponse<ResponseBody>> response) {
                if (response.isSuccessful() && response.code() == 200) {
                } else if (response.code() == 401) {
                    getAccessTokenFromServer(new AccessTokenResponse() {
                        @Override
                        public void successAccessToken() {
                            pushNotifications();
                        }

                        @Override
                        public void errorAccessToken() {
                            mProgressBarDialog.hideProgress();
                        }
                    });
                } else {
                    mProgressBarDialog.hideProgress();
                    mAlertDialog.showDialog(BaseActivity.this, getResources().getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ApiResponse<ResponseBody>> call, Throwable t) {
                mProgressBarDialog.hideProgress();
                mAlertDialog.showDialog(BaseActivity.this, getResources().getString(R.string.error_server));

            }
        });
    }

    protected ImageData compressImage(String imageUri, int compressQuantity) {

        String filePath = imageUri;
        Bitmap scaledBitmap = getCompressedBitmap(filePath);
        if (scaledBitmap == null) {
            return null;
        }
        String filename = writeBitmapToLocalPath(scaledBitmap, Constants.FULL_IMAGE, compressQuantity);
        ImageData data = new ImageData();
        data.setBitmap(scaledBitmap);
        data.setUrl(filename);
        return data;
    }

    private Bitmap getCompressedBitmap(String filePath) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        Bitmap bmp = null;

        options.inJustDecodeBounds = true;
        bmp = BitmapFactory.decodeFile(filePath, options);
        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;


        Bitmap scaledBitmap = createCompressedBitmap(filePath, options, bmp, actualHeight, actualWidth, Constants.MAX_WIDTH, Constants.MAX_HEIGHT);
        return scaledBitmap;
    }

    private Bitmap createCompressedBitmap(String filePath, BitmapFactory.Options options, Bitmap bmp, int actualHeight, int actualWidth, float maxHeight, float maxWidth) {
        Bitmap scaledBitmap = null;

        float imgRatio = actualWidth / (float) actualHeight;
        float maxRatio = maxWidth / (float) maxHeight;
        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }
        options.inSampleSize = calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inPurgeable = true;
        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            } else if (orientation == 3) {
                matrix.postRotate(180);
            } else if (orientation == 8) {
                matrix.postRotate(270);
            }

            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return scaledBitmap;
    }

    public int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        final float totalPixels = width * height;
        final float totalReqPixelsCap = reqWidth * reqHeight * 2;
        while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
            inSampleSize++;
        }

        return inSampleSize;
    }


    public String writeBitmapToLocalPath(Bitmap scaledBitmap, String imageType, int percentage) {
        FileOutputStream out = null;
        String filename = getFilename(imageType);

        try {
            out = new FileOutputStream(filename);
            boolean didWrite = scaledBitmap.compress(Bitmap.CompressFormat.JPEG, percentage, out);

            out.close();

            System.out.println(didWrite);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }
        return filename;
    }

    public String getFilename(String type) {

        ImageCache imageCache = new ImageCache(this);
        File file = imageCache.getCacheDirectory();
        String imagePrefix = null;
        if (type.equalsIgnoreCase(Constants.FULL_IMAGE)) {
            imagePrefix = "F_";
        } else {
            imagePrefix = "T_";
        }
        String uriSting = (file.getAbsolutePath() + "/" + imagePrefix + System.currentTimeMillis() + ".jpg");
        return uriSting;

    }

    protected ImageData thumbUrl(Bitmap bitmap) {
        String filename = writeBitmapToLocalPath(bitmap, Constants.THUMB_URL, 80);
        ImageData data = new ImageData();
        data.setThumbUrl(filename);
        return data;
    }

    public List<String> getNotGrantedPermissions(String... permissions) {

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return null;
        }

        List<String> deniedPermissions = new ArrayList<>(permissions.length);

        for (String permission : permissions) {
            int status = ContextCompat.checkSelfPermission(this, permission);
            if (status != PackageManager.PERMISSION_GRANTED) {
                deniedPermissions.add(permission);
            }
        }
        return deniedPermissions;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        final List<String> notGrantedPermissions = getNotGrantedPermissions(permissions);
        if (notGrantedPermissions.size() == 0) {
            mPermissionRequestListener.onPermissionsGranted();
        } else {
            mPermissionRequestListener.onPermissionsDenied();
        }
    }

    public void checkPermissionForMarshMellow(PermissionRequestCallBacks permissionRequestCallBacks, String message, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            this.mPermissionRequestListener = permissionRequestCallBacks;
            boolean isRequestPermissionRationale = true;

            final List<String> notGrantedPermissions = getNotGrantedPermissions(permissions);
            if (notGrantedPermissions.size() == 0) {
                permissionRequestCallBacks.onPermissionsGranted();
                return;
            }
            for (String permission : notGrantedPermissions) {
                if (isRequestPermissionRationale) {
                    isRequestPermissionRationale = ActivityCompat.shouldShowRequestPermissionRationale(BaseActivity.this, permission);
                }
            }

            final SharedPreferences preferences = getPreferences(MODE_PRIVATE);
            final boolean isFirstTime = preferences.getBoolean(IS_FIRST_TIME_PERMISSION_REQUEST, true);
            if (isRequestPermissionRationale || isFirstTime) {
                if (isFirstTime) {
                    preferences.edit().putBoolean(IS_FIRST_TIME_PERMISSION_REQUEST, false).apply();
                }
                String[] onlyDeniedPermissions = getArrayFromList(notGrantedPermissions);
                ActivityCompat.requestPermissions(BaseActivity.this, onlyDeniedPermissions, onlyDeniedPermissions.length);

            } else {
                if (!TextUtils.isEmpty(message)) {
                    mDialogUtility.displayPermissionMessageDialog(message);
                } else {
                    mPermissionRequestListener.onPermissionsDenied();
                }
            }
        } else {
            permissionRequestCallBacks.onPermissionsGranted();
        }
    }

    public static String[] getArrayFromList(List<String> list) {

        if (list == null || list.size() <= 0) {
            return null;
        }

        String[] array = new String[list.size()];
        for (int i = 0; i < list.size(); i++) {
            array[i] = list.get(i);
        }
        return array;
    }

    public void sendSignInPost(String username, String password) {
        ApiEndpointInterface apiInstance = ApiService.instance();
        mProgressBarDialog.showProgress();
        apiInstance.login(username, password,
                config.GRANT_TYPE,
                BuildConfig.CLIENT_ID,
                BuildConfig.CLIENT_SECRET).enqueue(new Callback<TokenResponse>() {

            @Override
            public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {

                if (response.isSuccessful() && response.code() == 200) {
                    preferences.edit().putString(config.TEMP_TOKEN, response.body().getAccessToken()).apply();
                    preferences.edit().putString(config.REFRESH_TOKEN, response.body().getRefreshToken()).apply();
                    String accessToken = response.body().getAccessToken();

                    if (!TextUtils.isEmpty(accessToken)) {
                        sendGetUserGet();
                    } else {
                        mAlertDialog.showDialog(BaseActivity.this, getResources().getString(R.string.login_error) + "               ");
                        mProgressBarDialog.hideProgress();
                    }

                } else if (response.code() == 500) {
                    mAlertDialog.showDialog(BaseActivity.this, getResources().getString(R.string.error_server));
                    mProgressBarDialog.hideProgress();
                } else if (response.code() == 400) {
                    try {
                        JSONObject errorObject = new JSONObject(response.errorBody().string().trim());
                        JSONArray error = errorObject.getJSONArray("errors");
                        JSONObject messageObject = error.getJSONObject(0);
                        String message = messageObject.getString("message");
                        mAlertDialog.showDialog(BaseActivity.this, message);
                    } catch (Exception e) {
                        e.printStackTrace();
                        mAlertDialog.showDialog(BaseActivity.this, getResources().getString(R.string.error_server));
                    }
                    mProgressBarDialog.hideProgress();
                } else {
                    mAlertDialog.showDialog(BaseActivity.this, getResources().getString(R.string.error_server));
                    mProgressBarDialog.hideProgress();
                }
            }

            @Override
            public void onFailure(Call<TokenResponse> call, Throwable t) {

                mAlertDialog.showDialog(BaseActivity.this, getResources().getString(R.string.error_server));
                Log.e(DEBUGTAG, "Unable to submit post to API.");
                mProgressBarDialog.hideProgress();
            }
        });
    }

    @Override
    public void hideKeyboard(Context context) {
        try {
            InputMethodManager imm = (InputMethodManager) context
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(((Activity) context).getWindow()
                    .getCurrentFocus().getWindowToken(), 0);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void OpenTagsActivity() {
        Intent intent = new Intent(this, SelectTagsActivity.class);
        startActivity(intent);
        finish();
    }

    public void OpenBloggerActivity() {
        Intent intent = new Intent(this, SelectBloggerActivity.class);
        startActivity(intent);
        finish();
    }

    public void openWelcomeProfileActivity() {
        Intent intent = new Intent(getApplicationContext(), WelcomeProfileInfoActivity.class);
        startActivity(intent);
        finish();
    }

}
