package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.ApiResponse;
import com.bynmix.app.models.ListingCheck;

import retrofit2.Callback;

public class CheckListingDialog {
    public void showDialog(Context mContext, final ProfileListener mCallback, ListingCheck status) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.edit_post_dialog);
        TextView title = dialog.findViewById(R.id.post_title);
        TextView description = dialog.findViewById(R.id.description);
        RelativeLayout addPayment = dialog.findViewById(R.id.delete);
        RelativeLayout addAddress = dialog.findViewById(R.id.disable);
        RelativeLayout cancel = dialog.findViewById(R.id.cancel);
        ImageView close = dialog.findViewById(R.id.close);
        TextView paymentButtonText = dialog.findViewById(R.id.delete_button_text);
        TextView addressButtonText = dialog.findViewById(R.id.disable_button_text);
        TextView cancelButtonText = dialog.findViewById(R.id.cancel_button_text);
        title.setVisibility(View.GONE);
        addPayment.setBackground(mContext.getResources().getDrawable(R.drawable.purple_rounded_button));
        addAddress.setBackground(mContext.getResources().getDrawable(R.drawable.light_pink_rounded_background));
        paymentButtonText.setText("ADD A PAYMENT");
        addressButtonText.setText("ADD AN ADDRESS");
        cancelButtonText.setText("CANCEL");
        if (status.isAddressMissing() && status.isPaymentMissing()) {
            addAddress.setVisibility(View.VISIBLE);
            addPayment.setVisibility(View.VISIBLE);
        } else if (status.isAddressMissing()) {
            addAddress.setVisibility(View.VISIBLE);
            addPayment.setVisibility(View.GONE);
        } else {
            addAddress.setVisibility(View.GONE);
            addPayment.setVisibility(View.VISIBLE);
        }
        description.setText(status.getErrorMessage());
        addPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openAddPaymentFragment(0, null, 0);
                dialog.dismiss();
            }
        });

        addAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.openMyAddressFragment();
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onBackPressed();
                dialog.dismiss();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onBackPressed();
                dialog.dismiss();
            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }
}
