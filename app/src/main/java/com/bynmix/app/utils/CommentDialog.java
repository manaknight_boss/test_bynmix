package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.interfaces.FeedItemClickListener;

public class CommentDialog {
    public void showDialog(final Context mContext, final FeedItemClickListener mCallBack, final int id, final String username, String type) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.add_comment_dialog);
        ImageView cross = dialog.findViewById(R.id.cross_icon);
        final AppCompatEditText commentEditText = dialog.findViewById(R.id.comment);
        Utils.setCapitalizeTextWatcher(commentEditText);
        RelativeLayout submitButton = dialog.findViewById(R.id.submit);
        RelativeLayout cancelButton = dialog.findViewById(R.id.cancel);
        TextView usernameText = dialog.findViewById(R.id.username);

        if (type.equalsIgnoreCase(Constants.TYPE_COMMENT)) {
            usernameText.setVisibility(View.GONE);
        } else {
            usernameText.setVisibility(View.VISIBLE);
            usernameText.setText("@" + username);
        }
        usernameText.setText("@" + username);
        commentEditText.setSelection(commentEditText.getText().length());
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String comment = commentEditText.getText().toString().trim();

                if (!TextUtils.isEmpty(comment)) {
                    mCallBack.addComment(id, comment);
                } else {
                    Toast.makeText(mContext, "Add comment first", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });

        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }

}
