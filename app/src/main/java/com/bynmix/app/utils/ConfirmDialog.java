package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.interfaces.ConfirmInterface;

public class ConfirmDialog {
    public void showDialog(Context mContext, final ConfirmInterface mCallback) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.logout_dialog);
        TextView textViewConfirm = dialog.findViewById(R.id.logout_text_field);
        textViewConfirm.setText("All your changes will lost. Still want to go back?");
        RelativeLayout confirm = dialog.findViewById(R.id.confirm);
        RelativeLayout cancel = dialog.findViewById(R.id.cancel);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.confirm();
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

}
