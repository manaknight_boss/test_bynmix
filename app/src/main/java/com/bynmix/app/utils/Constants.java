package com.bynmix.app.utils;

import com.bynmix.app.R;


public class Constants {
    public static final int MAX_WITHDRAW_LIMIT = 1000;
    public static final int TYPE_COMMENTS = 0;
    public static final int TYPE_DISPUTES = 1;
    public static final int EDIT = 1;
    public static final String TYPE = "type";
    public static final String ID = "id";
    public static final int FEED_LISTING = 1;
    public static final int FEED_POST = 0;
    public static final String INTENT_EXTRA_URL = "url";
    public static final float MAX_HEIGHT = 1000.0f;
    public static final float MAX_WIDTH = 1000.0f;
    public static final int TYPE_ABOUT = 0;
    public static final int TYPE_TOS = 1;
    public static String ACTION_BROADCAST_RECEIVER_REFRESH_FEEDS = "ACTION_BROADCAST_RECEIVER_REFRESH_FEEDS";
    public static int MAX_LISTING_IMAGES = 4;
    public static int MY_PURCHASES = 0;
    public static int MY_SALES = 1;
    public static int TERMS_OF_SERVICES = 1;
    public static int PRIVACY_POLICY = 2;
    public static int ABOUT_US = 3;
    public final int TRY_CLOTHING_VIDEO_URI = R.raw.bynmix_fashionista_trying_on_outfit;
    public final int PICK_CLOTHING_VIDEO_URI = R.raw.bynmix_fashion_designer_taking_photos;
    public final String ACCESS_TOKEN = "access_token";
    public final String REFRESH_TOKEN = "refresh_token";
    public final String GRANT_TYPE = "password";
    public final String TEMP_TOKEN = "temp_token";
    public static int AlL_FEEDS = 0;
    public static int AlL_LISTING = 1;
    public static int AlL_POSTS = 2;
    public static int SHORT_BIO_LENGTH = 150;
    public static int SHORT_DESCRIPTION_LENGTH = 150;
    public static int MY_POST = 1;
    public static int MY_BYN = 0;
    public static int FOLLOWER = 0;
    public static int FOLLOWING = 1;
    public static int FOR_PROFILE_PIC = 1;
    public static int FOR_BACKGROUND_PIC = 2;
    public static int CREATE_POST = 3;
    public static int EDIT_POST = 5;
    public static int CREATE_LISTING = 4;
    public static int EDIT_POST_CROPPER_REQUEST_CODE = 15;
    public static int CREATE_POST_CROPPER_REQUEST_CODE = 13;
    public static int CREATE_LISTING_CROPPER_REQUEST_CODE = 14;
    public static int CROPPER_REQUEST_CODE = 20;
    public static int CROPPER_COVER_REQUEST_CODE = 21;
    public static int CROPPER_REPORT_AN_ISSUE_REQUEST_CODE_1 = 22;
    public static int CROPPER_REPORT_AN_ISSUE_REQUEST_CODE_2 = 23;
    public static int CROPPER_REPORT_AN_ISSUE_REQUEST_CODE_3 = 24;
    public static int CROPPER_REPORT_AN_ISSUE_REQUEST_CODE_4 = 25;
    public static int REPORT_AN_ISSUE_IMAGE_1 = 16;
    public static int REPORT_AN_ISSUE_IMAGE_2 = 17;
    public static int REPORT_AN_ISSUE_IMAGE_3 = 18;
    public static int REPORT_AN_ISSUE_IMAGE_4 = 19;
//    public static int COMPRESS_80 = 80;
//    public static int COMPRESS_50 = 50;
//    public static int COMPRESS_70 = 70;
    public static int COMPRESS_100 = 100;
    //public static int NO_COMPRESS = 0;
    public static int CREATE_ACCOUNT = 0;
    public static int LOGIN = 1;
    public static int WEB_LINK = 1;
    public static int YOU_TUBE_LINK = 2;
    public static int LIKE_FRAGMENT = 1;
    public static int PROFILE_FRAGMENT = 0;
    public static int FEED_FILTER = 0;
    public static int PROFILE_FILTER = 1;

    public final static int CONDITION_FILTER = 0;
    public final static int SIZE_FILTER = 1;
    public final static int COLOR_FILTER = 2;
    public final static int CATEGORY_FILTER = 3;
    public final static int PRICE_FILTER = 4;
    public final static int BRAND_FILTER = 5;

    public static int DISABLE = 1;
    public static int DELETE = 2;
    public static int ENABLE = 0;

    public static int SHIPPING_ADDRESS = 0;
    public static int RETURN_ADDRESS = 1;
    public static int EDIT_ADDRESS = 1;
    public static int ADD_ADDRESS = 0;
    public static int EXISTING_ADD_ADDRESS_FRAGMENT = 2;

    public static int LOCAL_PHOTO = 1;
    public static int GLOBAL_PHOTO = 0;

    public static int STATUS_AVAILABLE = 1;
    public static int STATUS_SOLD = 2;
    public static int STATUS_ON_HOLD = 3;
    public static int STATUS_NOT_FOR_SALE = 4;
    public static int STATUS_DELETED = 5;

    public static int ALL_NOTIFICATION = 0;
    public static int MY_OFFERS = 1;
    public static int RECEIVED_OFFERS = 2;

    public static int OFFER = 0;
    public static int BUY = 1;

    public final static int PRICE_DROP = 1;
    public final static int OFFER_RECEIVED = 2;
    public final static int COUNTER_OFFERED = 3;
    public final static int ACCEPTED_OFFER = 4;
    public final static int DECLINED_OFFER = 5;
    public final static int ITEM_SOLD = 6;
    public final static int SHIPPING_LABEL_PRINTED = 7;
    public final static int PURCHASE_SHIPPED = 8;
    public final static int PURCHASE_DELIVERED = 9;
    public final static int FEEDBACK_LEFT = 10;
    public final static int INVALID_PAYMENT = 11;
    public final static int USER_FOLLOWING = 12;
    public final static int LISTING_LIKED = 13;
    public final static int POST_LIKED = 14;
    public final static int COMMENT_RECEIVED = 15;
    public final static int PROMOTION = 16;

    public final static int PURCHASE_STATUS_ID = 1;


    public final static int DOUBLE_POINTS = 1;
    public final static int TRIPLE_POINTS = 2;
    public final static int QUARDRUPLE_POINTS = 3;
    public final static int FREE_SHIPPING = 4;
    public final static int THREE_DOLLAR_SHIPPING = 5;
    public final static int FIVE_PERCENT_OFF = 6;
    public final static int TEN_PERCENT_OFF = 7;


    public static final int EDIT_LISTING = 1;


    public static final int FEEDS_SCROLL_TO_TOP_SMOOTHLY_AFTER_POINT = 4;

    public static long IMAGE_UPLOAD_DELAY = 1000;

    public static String COVER_FILE_EXTRA = "cover_file_extra";
    public static String THUMB_URL = "THUMB_URL";
    public static String FULL_IMAGE = "FULL_IMAGE";
    public static String IMAGE_MEDIA_TYPE = "image";
    public static String POSTS = "post";
    public static String VIDEO_POSTS = "Video Post";
    public static String VIDEO_POST = "VideoPost";
    public static String WEBSITE_LINK = "blog Link";
    public static String FEED = "FEED";
    public static String FEED_FRAGMENT = "FeedFragment";
    public static String BROWSE_FRAGMENT = "BrowseFragment";
    public static String PACKAGE = "package";
    public static String TYPE_POST = "Post";
    public static String TYPE_LISTING = "Listing";
    public static String LEFT = "Left";
    public static String TYPE_COMMENT = "comment";
    public static String TYPE_REPLY = "reply";
    public static String STORAGE_PERMISSION_REQUIRED = "Need Storage Permission" +
            " Tap Settings > Permissions, and turn on all permission.";
    public static final String ON_SOLD = "sold";
    public static final String ON_HOLD = "on hold";
    public static final String NOT_FOR_SALE = "not for sale";

    public static String VISA = "Visa";
    public static String MASTERCARD = "MasterCard";
    public static String AMERICAN_EXPRESS = "American Express";
    public static String DINERS_CLUB = "Diners Club";
    public static String JCB = "JCB";
    public static String DISCOVER = "Discover";


    public static String NOTIFICATION_FRAGMENT_SELECTION = "notification_fragment_selection";
    public static String NOTIFICATION_FRAGMENT = "NOTIFICATION_FRAGMENT";

    public static final int DOT_TYPE_DEFAULT = 0;
    public static final int DOT_TYPE_ONLINE_RETAILER = 1;
    public static final int DOT_TYPE_LISTING = 2;

    public static final int USER_STYLE_FILTER = 0;
    public static final int USER_BODY_TYPE_FILTER = 1;
    public static final int USER_BRAND_FILTER = 2;
    public static final int HEIGHT_FILTER = 3;

    public static final int BUYER = 0;
    public static final int SELLER = 1;

    public static final int SELECT = 0;
    public static final int CHANGE = 1;

}
