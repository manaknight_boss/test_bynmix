package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.bynmix.app.activities.OAuthActivity;
import com.bynmix.app.activities.createAccounts.CreateAccountFormActivity;
import com.bynmix.app.databinding.CreateAccountDialogBinding;
import com.bynmix.app.interfaces.FeedItemClickListener;
import com.google.android.gms.auth.api.Auth;

public class CreateAccountDialog extends OAuthActivity {
    private CreateAccountDialogBinding mBinding;

    public void showDialog(Context mContext, final FeedItemClickListener mCallBack) {
        final Dialog dialog = new Dialog(mContext);
        mBinding = CreateAccountDialogBinding.inflate(LayoutInflater.from(mContext));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = mBinding.getRoot();
        dialog.setContentView(view);
        mBinding.loginHere.setPaintFlags(mBinding.loginHere.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        initializeGoogleLogin();

        mBinding.facebookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (conditionsForApiCall()) {
                    login_type = "Facebook";
                    initializeFacebookLogin(Constants.CREATE_ACCOUNT);
                }
            }
        });

        mBinding.googlePlusButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (conditionsForApiCall()) {
                    login_type = "Google";
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, RC_SIGN_IN);
                }
            }
        });

        mBinding.emailButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CreateAccountFormActivity.class);
                startActivity(intent);
            }
        });
        mBinding.mainMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mBinding.crossIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }
}
