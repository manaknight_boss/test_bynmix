package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.util.Patterns;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.activities.createAccounts.TermsOfServicesActivity;
import com.bynmix.app.databinding.CreateAccountFormDialogBinding;
import com.bynmix.app.interfaces.FeedItemClickListener;
import com.bynmix.app.models.UserResponse;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateAccountFormDialog extends BaseActivity {
    private CreateAccountFormDialogBinding mBinding;
    protected ApiEndpointInterface apiInstance;
    private String username, email, password, firstname, lastname;
    private boolean isChecked;
    private Dialog dialog;
    private Context mContext;

    public void showDialog(Context context, FeedItemClickListener mCallBack) {
        mContext = context;
        dialog = new Dialog(mContext);
        mBinding = CreateAccountFormDialogBinding.inflate(LayoutInflater.from(mContext));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = mBinding.getRoot();
        dialog.setContentView(view);


        Utils.setCapitalizeTextWatcher(mBinding.accountFormTextUsername);
        Utils.setCapitalizeTextWatcher(mBinding.accountFormTextFirstName);
        Utils.setCapitalizeTextWatcher(mBinding.accountFormTextLastName);
        apiInstance = ApiService.instance();
        updateView();
        mBinding.checkImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isChecked = !isChecked;
                updateView();
            }
        });

        mBinding.termsOfServices.setPaintFlags(mBinding.termsOfServices.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        mBinding.termsOfServices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TermsOfServicesActivity.class);
                videoView.stopPlayback();
                intent.putExtra("type", Constants.TERMS_OF_SERVICES);
                startActivity(intent);
            }
        });

        mBinding.submitButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSubmitFormClick();
            }
        });


        mBinding.crossIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }

    private void onSubmitFormClick() {
        email = mBinding.accountFormTextEmail.getText().toString().trim();
        username = mBinding.accountFormTextUsername.getText().toString().trim();
        firstname = mBinding.accountFormTextFirstName.getText().toString().trim();
        lastname = mBinding.accountFormTextLastName.getText().toString().trim();
        password = mBinding.accountFormTextPassword.getText().toString().trim();

        if (conditionsForApiCall()) {
            if (!validate()) {
                showToast(translateString(R.string.fail_signup));
            } else {
                if (isChecked) {
                    registerUser();
                } else {
                    Toast.makeText(this, "Please Select terms and conditions", Toast.LENGTH_SHORT).show();
                    return;
                }

            }
        }

    }

    protected void registerUser() {
        mProgressBarDialog.showProgress();
        final UserResponse registerUser = new UserResponse();
        registerUser.setUsername(username);
        registerUser.setFirstname(firstname);
        registerUser.setLastname(lastname);
        registerUser.setEmail(email);
        registerUser.setPassword(password);
        registerUser.setMobileRegistration(true);
        apiInstance.register(registerUser).enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful() && response.code() == 200) {
                    sendSignInPost(username, password);
                    dialog.dismiss();
                } else if (response.code() == 500) {
                    mAlertDialog.showDialog(mContext, getResources().getString(R.string.error_server));
                } else {
                    String message = Utils.ErrorMessage(mContext, response.errorBody());
                    mAlertDialog.showDialog(mContext, message);
                }
                mProgressBarDialog.hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                mAlertDialog.showDialog(mContext, getResources().getString(R.string.error_server));
                mProgressBarDialog.hideProgress();

            }
        });
    }

    protected boolean validate() {
        boolean valid = true;

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mBinding.accountFormTextEmail.setError(translateString(R.string.error_email));
            valid = false;
        }

        if (username.isEmpty()) {
            mBinding.accountFormTextUsername.setError(translateString(R.string.error_username));
            valid = false;
        }

        if (firstname.isEmpty()) {
            mBinding.accountFormTextFirstName.setError(translateString(R.string.error_first_name));
            valid = false;
        }

        if (lastname.isEmpty()) {
            mBinding.accountFormTextLastName.setError(translateString(R.string.error_last_name));
            valid = false;
        }

        if (password.isEmpty() || password.length() < 6) {
            mBinding.accountFormTextPassword.setError(translateString(R.string.error_password));
            valid = false;
        }

        return valid;
    }

    private void updateView() {
        if (isChecked) {
            mBinding.checkImage.setImageResource(R.mipmap.checkbox_checked);
        } else {
            mBinding.checkImage.setImageResource(R.mipmap.checkbox);
        }
    }
}
