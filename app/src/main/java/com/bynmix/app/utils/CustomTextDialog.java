package com.bynmix.app.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bynmix.app.R;

public class CustomTextDialog {

    /**
     * Builds a Custom Text Dialog that can be closed by clicking on the X
     *
     * @param context
     * @param message
     */
    public void showDialog(Context context, String message) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_text_dialog);
        dialog.getWindow().setGravity(Gravity.TOP);
        TextView text = (TextView) dialog.findViewById(R.id.dialog_body_text);
        text.setText(Html.fromHtml(message), TextView.BufferType.SPANNABLE);
        ImageView closeButton = (ImageView) dialog.findViewById(R.id.dialog_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * context.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }

}
