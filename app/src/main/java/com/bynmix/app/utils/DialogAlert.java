package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.support.v7.widget.AppCompatEditText;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.interfaces.CommentListener;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.interfaces.EnableDisableDialog;
import com.bynmix.app.interfaces.MakeAnOfferListener;
import com.bynmix.app.models.Address;
import com.bynmix.app.models.CardDetail;
import com.bynmix.app.models.ListingDetail;

public class DialogAlert {
    public static void showDialog(Context mContext, String title, String descriptionText, int buttonText, final DialogClickListener okListener) {
        final Dialog dialog = new Dialog(mContext);
        String description;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.cancel_offer);
        TextView message = dialog.findViewById(R.id.heading_text);
        TextView button = dialog.findViewById(R.id.button_text);
        RelativeLayout cancelOffer = dialog.findViewById(R.id.delete);
        RelativeLayout cancel = dialog.findViewById(R.id.cancel);
        TextView cancelText = dialog.findViewById(R.id.cancel_button_text);
        ImageView close = dialog.findViewById(R.id.close);
        button.setText(buttonText);
        if (buttonText == R.string.yes_cancel_purchase) {
            cancelText.setText("NOT YET");
        }
        description = descriptionText + " \"" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + title + "</font>" + "\" ? ";
        message.setText(Html.fromHtml(description), TextView.BufferType.SPANNABLE);

        cancelOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (okListener != null) {
                    okListener.onClick();
                    dialog.dismiss();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();

    }

    public static void showDialog(Context mContext, String title, String descriptionText, String descriptionLast, int buttonText, final DialogClickListener okListener) {
        final Dialog dialog = new Dialog(mContext);
        String description;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.cancel_offer);
        TextView message = dialog.findViewById(R.id.heading_text);
        TextView button = dialog.findViewById(R.id.button_text);
        RelativeLayout cancelOffer = dialog.findViewById(R.id.delete);
        RelativeLayout cancel = dialog.findViewById(R.id.cancel);
        ImageView close = dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        button.setText(buttonText);
        description = descriptionText + " " + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + title + "</font>" + " " + descriptionLast;
        message.setText(Html.fromHtml(description));
        cancelOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (okListener != null) {
                    okListener.onClick();
                    dialog.dismiss();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }


    public static void showCongoDialog(Context mContext, String titleText, String descriptionText, String cancelButton, int buttonText, final DialogClickListener okListener) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.delete_image_and_dots);
        TextView title = dialog.findViewById(R.id.post_title);
        TextView message = dialog.findViewById(R.id.description);
        TextView button = dialog.findViewById(R.id.delete_button);
        TextView cancelButtonText = dialog.findViewById(R.id.cancel_button_text);
        RelativeLayout cancelOffer = dialog.findViewById(R.id.delete);
        RelativeLayout cancel = dialog.findViewById(R.id.cancel);
        ImageView cross = dialog.findViewById(R.id.cross_icon);
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        button.setText(buttonText);
        title.setText(titleText);
        cancelButtonText.setText(cancelButton);
        message.setText(descriptionText);
        cancelOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (okListener != null) {
                    okListener.onClick();
                    dialog.dismiss();
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();

    }


    public static void showCancelSaleErrorDialog(Context mContext, String message, String description) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_cancel_sale_error);
        TextView text = (TextView) dialog.findViewById(R.id.reason);
        TextView descriptionText = (TextView) dialog.findViewById(R.id.description);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        descriptionText.setText(description);
        text.setText(message);
        dialog.show();
    }

    public static void showCardNotAcceptedDialog(Context mContext, String message) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_card_not_accepted);
        TextView text = dialog.findViewById(R.id.reason);
        ImageView close = dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        text.setText(message);
        dialog.show();
    }


    public static void showSaleCancelSuccessfulDialog(Context context, String title, String message, boolean isTrue) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_sale_cancel_successfully);
        TextView titleText = (TextView) dialog.findViewById(R.id.title);
        TextView text = (TextView) dialog.findViewById(R.id.message);
        TextView show = (TextView) dialog.findViewById(R.id.show);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        text.setText(Html.fromHtml(message));
        titleText.setText(Html.fromHtml(title));
        if (isTrue) {
            show.setVisibility(View.VISIBLE);
        } else {
            show.setVisibility(View.GONE);
        }
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * context.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }


    public static void showSuccessfulDialog(Context mContext, String message) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_successfully_done);
        TextView text = (TextView) dialog.findViewById(R.id.description);
        RelativeLayout done = (RelativeLayout) dialog.findViewById(R.id.got_it);
        TextView button = dialog.findViewById(R.id.action_button);
        ImageView close = dialog.findViewById(R.id.close);
        close.setOnClickListener(v -> dialog.dismiss());
        button.setText("GOT IT!");
        text.setText(Html.fromHtml(message));
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }

    public static void showCommentDialog(final Context mContext, int type, final CommentListener commentListener) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.add_comment_dialog);
        ImageView cross = (ImageView) dialog.findViewById(R.id.cross_icon);
        final AppCompatEditText commentEditText = (AppCompatEditText) dialog.findViewById(R.id.comment);
        RelativeLayout submitButton = (RelativeLayout) dialog.findViewById(R.id.submit);
        RelativeLayout cancelButton = (RelativeLayout) dialog.findViewById(R.id.cancel);
        TextView usernameText = (TextView) dialog.findViewById(R.id.username);
        usernameText.setVisibility(View.GONE);
        if (type == Constants.TYPE_DISPUTES) {
            commentEditText.setHint(mContext.getResources().getString(R.string.dispute_comment_hint));
        } else {
            commentEditText.setHint(mContext.getResources().getString(R.string.enter_comment_here));
        }
        Utils.setCapitalizeTextWatcher(commentEditText);
        commentEditText.setSelection(commentEditText.getText().length());
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (commentListener != null) {
                    String comment = commentEditText.getText().toString().trim();
                    if (!TextUtils.isEmpty(comment)) {
                        commentListener.addComment(0, comment);
                    } else {
                        Toast.makeText(mContext, "Add comment first", Toast.LENGTH_SHORT).show();
                    }
                }
                dialog.dismiss();
            }
        });

        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }

    public static void showWithDrawDialog(Context mContext, String title, final DialogClickListener okListener) {
        final Dialog dialog = new Dialog(mContext);
        String description;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.cancel_offer);
        TextView message = (TextView) dialog.findViewById(R.id.heading_text);
        TextView button = (TextView) dialog.findViewById(R.id.button_text);
        RelativeLayout cancelOffer = (RelativeLayout) dialog.findViewById(R.id.delete);
        RelativeLayout cancel = (RelativeLayout) dialog.findViewById(R.id.cancel);
        TextView cancelText = dialog.findViewById(R.id.cancel_button_text);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        button.setText(mContext.getString(R.string.yes_withdraw));
        cancelText.setText("CANCEL");
        description = "Are you sure you want to transfer " + " " + "<b><font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + title + "</font></b>" + " " + mContext.getString(R.string.withdraw_description_dialog) + "<b><font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + " $2.00 transfer fee" + "</font></b>" + "  per transfer made.";
        message.setText(Html.fromHtml(description));
        cancelOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (okListener != null) {
                    okListener.onClick();
                    dialog.dismiss();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }

    public static void showDialog(Context mContext, String title, final int type, final String postType, boolean isActive, final EnableDisableDialog okListener) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.edit_post_dialog);
        TextView postTitle = (TextView) dialog.findViewById(R.id.post_title);
        TextView description = (TextView) dialog.findViewById(R.id.description);
        RelativeLayout deletePost = (RelativeLayout) dialog.findViewById(R.id.delete);
        RelativeLayout disablePost = (RelativeLayout) dialog.findViewById(R.id.disable);
        RelativeLayout cancel = (RelativeLayout) dialog.findViewById(R.id.cancel);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        postTitle.setText(Html.fromHtml(title), TextView.BufferType.SPANNABLE);
        if (postType.equalsIgnoreCase(Constants.TYPE_POST)) {
            if (type == Constants.DISABLE) {
                deletePost.setVisibility(View.GONE);
                disablePost.setBackground(mContext.getResources().getDrawable(R.drawable.purple_rounded_button));
                ((TextView) (dialog.findViewById(R.id.disable_button_text))).setText(mContext.getString(R.string.disable_for_now));
                description.setText(mContext.getString(R.string.disable_description));
            } else if (type == Constants.ENABLE) {
                deletePost.setVisibility(View.GONE);
                disablePost.setBackground(mContext.getResources().getDrawable(R.drawable.purple_rounded_button));
                ((TextView) (dialog.findViewById(R.id.disable_button_text))).setText(mContext.getString(R.string.re_enable_post));
                description.setText(mContext.getString(R.string.enable_post_description));
            } else {
                deletePost.setVisibility(View.VISIBLE);
                disablePost.setBackground(mContext.getResources().getDrawable(R.drawable.light_purple_button));
                ((TextView) (dialog.findViewById(R.id.disable_button_text))).setText(mContext.getString(R.string.disable_this_post));
                description.setText(mContext.getString(R.string.delete_description));
            }
        } else {
            if (type == Constants.DISABLE) {
                deletePost.setVisibility(View.GONE);
                disablePost.setBackground(mContext.getResources().getDrawable(R.drawable.purple_rounded_button));
                ((TextView) (dialog.findViewById(R.id.disable_button_text))).setText(mContext.getString(R.string.disable_this_listing));
                description.setText(mContext.getString(R.string.disable_description_listing));
            } else if (type == Constants.ENABLE) {
                deletePost.setVisibility(View.GONE);
                disablePost.setBackground(mContext.getResources().getDrawable(R.drawable.purple_rounded_button));
                ((TextView) (dialog.findViewById(R.id.disable_button_text))).setText(mContext.getString(R.string.re_enable_listing));
                description.setText(mContext.getString(R.string.enable_listing_description));
            } else {
                deletePost.setVisibility(View.VISIBLE);
                disablePost.setBackground(mContext.getResources().getDrawable(R.drawable.light_purple_button));
                ((TextView) (dialog.findViewById(R.id.disable_button_text))).setText(mContext.getString(R.string.disable_this_listing));
                description.setText(mContext.getString(R.string.delete_description_listing));
            }
        }

        if (isActive) {
            disablePost.setVisibility(View.VISIBLE);
        } else {
            disablePost.setVisibility(View.GONE);
            description.setVisibility(View.GONE);
        }

        disablePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (okListener != null) {
                    okListener.onClickListener();
                    dialog.dismiss();
                }
            }
        });
        deletePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (okListener != null) {
                    okListener.onDeleteListener();
                    dialog.dismiss();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }


    public static void showDialog(final Context mContext, final Address address, final int type, final DialogClickListener okListener) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.delete_address_dialog);
        TextView fullName = (TextView) dialog.findViewById(R.id.full_name);
        TextView addressOne = (TextView) dialog.findViewById(R.id.address_one);
        TextView addressTwo = (TextView) dialog.findViewById(R.id.address_two);
        TextView zipcode = (TextView) dialog.findViewById(R.id.zipcode);
        RelativeLayout deletePost = (RelativeLayout) dialog.findViewById(R.id.delete);
        RelativeLayout cancel = (RelativeLayout) dialog.findViewById(R.id.cancel);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);

        fullName.setText(address.getFullName());
        if (address.getAddressOne() != null) {
            addressOne.setVisibility(View.VISIBLE);
            addressOne.setText(address.getAddressOne());
        } else {
            addressOne.setVisibility(View.GONE);
        }
        if (address.getAddressTwo() != null) {
            addressTwo.setVisibility(View.VISIBLE);
            addressTwo.setText(address.getAddressTwo());
        } else {
            addressTwo.setVisibility(View.GONE);
        }
        zipcode.setText(address.getCity() + " " + address.getStateAbbreviation() + ", " + address.getZipCode());


        deletePost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NotDeletedAddressDialog notDeletedAddressDialog = new NotDeletedAddressDialog();
                if (address.isShippingAddress() || address.isReturnAddress()) {
                    String message;
                    if (type == Constants.RETURN_ADDRESS) {
                        message = Utils.getColorText(mContext.getString(R.string.default_return_address), mContext.getResources().getColor(R.color.lightPurple4));
                        message = mContext.getString(R.string.selected_address_cannot_be_deleted) + " " + message + ". " + mContext.getString(R.string.please_select_another);
                    } else {
                        message = Utils.getColorText(mContext.getString(R.string.default_shipping_address), mContext.getResources().getColor(R.color.lightPurple4));
                        message = mContext.getString(R.string.selected_address_cannot_be_deleted) + " " + message + ". " + mContext.getString(R.string.please_select_another);
                    }
                    notDeletedAddressDialog.showDialog(mContext, message);
                    dialog.dismiss();
                } else {
                    if (okListener != null) {
                        okListener.onClick();
                        dialog.dismiss();
                    }
                }
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }

    public static void showDialog(final Context mContext, final CardDetail cardDetail, final boolean status, int type, final DialogClickListener okListener) {
        final Dialog dialog = new Dialog(mContext);
        String description;
        String button;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.edit_post_dialog);
        TextView message = dialog.findViewById(R.id.description);
        RelativeLayout deletePost = dialog.findViewById(R.id.disable);
        TextView buttonText = dialog.findViewById(R.id.disable_button_text);
        RelativeLayout cancel = dialog.findViewById(R.id.cancel);
        TextView postTitle = dialog.findViewById(R.id.post_title);
        RelativeLayout disablePost = dialog.findViewById(R.id.delete);
        ImageView close = dialog.findViewById(R.id.close);
        postTitle.setVisibility(View.GONE);
        disablePost.setVisibility(View.GONE);


        if (!status) {
            description = mContext.getString(R.string.are_you_sure_to_delete) + " your " + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + cardDetail.getBrand() + "</font>" + " payment in ending in " + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + cardDetail.getLastFour() + "</font>" + "\" ? ";
            button = mContext.getString(R.string.permanently_delete);
        } else {
            if (type == 1) {
                description = Utils.getColorText("Default Payment", mContext.getResources().getColor(R.color.lightPurple4));
                description = mContext.getString(R.string.selected_payment_cannot_be_deleted) + " " + description + ". " + mContext.getString(R.string.please_select_another_payment);
                button = mContext.getString(R.string.selecct_different_card);
            } else if (type == 2) {
                description = Utils.getColorText("Default Payment", mContext.getResources().getColor(R.color.lightPurple4));
                description = mContext.getString(R.string.selected_payment_cannot_be_deleted) + " " + description + ". " + mContext.getString(R.string.please_select_another_payment);
                button = mContext.getString(R.string.got_it);
                cancel.setVisibility(View.GONE);
            } else {
                description = mContext.getString(R.string.linked_payment_account);
                button = mContext.getString(R.string.selecct_different_card);
            }

        }
        message.setText(Html.fromHtml(description), TextView.BufferType.SPANNABLE);
        buttonText.setText(button);

        if(type == 2) {
            deletePost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
        } else {
            deletePost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (okListener != null) {
                        okListener.onClick();
                        dialog.dismiss();
                    }
                }
            });
        }
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }


    public static void makeAnOfferDialog(final Context mContext, final ListingDetail listingDetail, boolean isBuyer, final MakeAnOfferListener listener) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_make_an_offer);
        ImageView cross = dialog.findViewById(R.id.cross_icon);
        final AppCompatEditText offerEditText = dialog.findViewById(R.id.offer);
        RelativeLayout submitButton = dialog.findViewById(R.id.submit);
        RelativeLayout cancelButton = dialog.findViewById(R.id.cancel);
        TextView headingText = dialog.findViewById(R.id.heading);
        TextView titleText = dialog.findViewById(R.id.title);
        TextView price = dialog.findViewById(R.id.price);
        String title = Utils.getColorText(listingDetail.getTitle(), mContext.getResources().getColor(R.color.lightPurple));
        titleText.setText(Html.fromHtml("\"" + title + "\""), TextView.BufferType.SPANNABLE);
        price.setText(Utils.formatValueToTwoDecimalPlaces(listingDetail.getPrice()));
        if (isBuyer) {
            headingText.setText("Make an offer for:");
        } else {
            headingText.setText("Counter offer for:");
        }

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    String offer = offerEditText.getText().toString().trim();
                    if (!TextUtils.isEmpty(offer)) {
                        double offerPrice = Double.parseDouble(offer);
                        if (offerPrice > 0) {
                            if (offerPrice < listingDetail.getPrice()) {
                                if (offerPrice > listingDetail.getLowestAllowableOffer()) {
                                    listener.submitOffer(offer);
                                    dialog.dismiss();
                                } else {
                                    offerEditText.setError("Sorry! It looks like your offer for this listing is too low. Try submitting a higher amount so that the seller can consider the offer.");
                                }

                            } else {
                                offerEditText.setError("Please make an offer lower than the current price.");
                            }
                        }

                    } else {
                        offerEditText.setError("Add Offer first");
                    }
                }
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }
}

