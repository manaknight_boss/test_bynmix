package com.bynmix.app.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.view.KeyEvent;

import com.bynmix.app.R;


public class DialogUtility {

    private AlertDialog mAlertDialog;
    private Context mContext;

    public DialogUtility(Context mContext) {
        this.mContext = mContext;
    }

    @NonNull
    private DialogInterface.OnKeyListener onBackPressListener() {
        return new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK &&
                        event.getAction() == KeyEvent.ACTION_UP &&
                        !event.isCanceled()) {
                    hideDialog();
                    return true;
                }
                return false;
            }
        };
    }

    public void hideDialog() {
        if (mAlertDialog != null) {
            mAlertDialog.dismiss();
        }
    }

    public void displayMessageDialog(String message) {
        mAlertDialog = new AlertDialog.Builder(mContext)
                .setTitle(mContext.getString(R.string.error))
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        hideDialog();
                    }
                })
                .setCancelable(false)
                .setOnKeyListener(onBackPressListener())
                .create();
        mAlertDialog.show();
    }

    public void displayPermissionMessageDialog(String message) {
        mAlertDialog = new AlertDialog.Builder(mContext)
                .setTitle("Permissions")
                .setMessage(message)
                .setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts(Constants.PACKAGE, mContext.getPackageName(), null);
                        intent.setData(uri);
                        mContext.startActivity(intent);
                    }
                })

                .create();
        mAlertDialog.show();
    }

}
