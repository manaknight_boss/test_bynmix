package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.text.Html;
import android.view.Window;
import android.widget.TextView;

import com.bynmix.app.R;

public class ErrorDialog {

    public void showDialog(Context mContext, String message) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.add_post_error_dialog);
        TextView errorText = (TextView) dialog.findViewById(R.id.error_text);
        errorText.setText(Html.fromHtml(message), TextView.BufferType.SPANNABLE);
        dialog.show();
    }
}
