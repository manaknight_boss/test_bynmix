package com.bynmix.app.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bynmix.app.R;

public class ForgotPasswordDialog {

    public void showDialog(Activity activity, String message, String message1) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.custom_text_dialog);

        TextView text = (TextView) dialog.findViewById(R.id.dialog_body_text);
        TextView textView = (TextView) dialog.findViewById(R.id.dialog_body_text2);
        text.setText(message);
        if(message1!=null){
            textView.setText(message1);
            textView.setVisibility(View.VISIBLE);
        }else {
            textView.setVisibility(View.GONE);
        }

        ImageView closeButton = (ImageView) dialog.findViewById(R.id.dialog_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
