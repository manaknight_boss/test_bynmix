package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.bynmix.app.R;
import com.bynmix.app.activities.HomeActivity;
import com.bynmix.app.activities.OAuthActivity;
import com.bynmix.app.activities.createAccounts.CreateAccountActivity;
import com.bynmix.app.activities.createAccounts.ForgotPasswordActivity;
import com.bynmix.app.databinding.LoginDialogBinding;
import com.bynmix.app.interfaces.FeedItemClickListener;
import com.bynmix.app.networks.ApiEndpointInterface;
import com.bynmix.app.networks.ApiService;
import com.google.android.gms.auth.api.Auth;

public class LoginDialog extends OAuthActivity {
    private LoginDialogBinding mBinding;
    protected ApiEndpointInterface apiInstance;
    private String username, password;
    private Dialog dialog;
    private Context mContext;

    public void showDialog(Context context, FeedItemClickListener mCallBack) {
        mContext = context;
        dialog = new Dialog(mContext);
        mBinding = LoginDialogBinding.inflate(LayoutInflater.from(mContext));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        View view = mBinding.getRoot();
        dialog.setContentView(view);
        mBinding.createAccount.setPaintFlags(mBinding.createAccount.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Utils.setCapitalizeTextWatcher(mBinding.mainInputTextUsername);
        init();
        initializeBackgroundVideo(config.PICK_CLOTHING_VIDEO_URI);
        initializeGoogleLogin();
        String accessToken = getAccessToken();
        if (!TextUtils.isEmpty(accessToken)) {
            dialog.dismiss();
            Intent intent = new Intent(mContext, HomeActivity.class);
            startActivity(intent);
            finish();
        }
        mBinding.mainFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (conditionsForApiCall()) {
                    login_type = "Facebook";
                    initializeFacebookLogin(Constants.LOGIN);
                }
            }
        });

        mBinding.mainGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (conditionsForApiCall()) {
                    login_type = "Google";
                    Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                    startActivityForResult(signInIntent, RC_SIGN_IN);
                }
            }
        });

        mBinding.mainForgot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ForgotPasswordActivity.class);
                startActivity(intent);
            }
        });

        mBinding.mainSigninButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                username = mBinding.mainInputTextUsername.getText().toString().trim();
                password = mBinding.mainInputTextPassword.getText().toString().trim();

                if (conditionsForApiCall()) {
                    if (!validate()) {
                        showToast(translateString(R.string.fail_signin));
                    } else {
                        sendSignInPost(username, password);
                    }
                }
            }
        });

        mBinding.mainMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Intent intent = new Intent(getApplicationContext(), CreateAccountActivity.class);
                startActivity(intent);
            }
        });
        apiInstance = ApiService.instance();
        mBinding.crossIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }

    protected boolean validate() {
        boolean valid = true;

        if (username.isEmpty()) {
            mBinding.mainInputTextUsername.setError(translateString(R.string.error_username));
            valid = false;
        }

        if (password.isEmpty() || password.length() < 6) {
            mBinding.mainInputTextPassword.setError(translateString(R.string.error_password));
            valid = false;
        }
        return valid;
    }
}
