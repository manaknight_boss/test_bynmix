package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.text.Html;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.interfaces.LogoutInterface;

public class LogoutDialog {

    public void showDialog(Context mContext, final LogoutInterface mCallback) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.logout_dialog);
        RelativeLayout confirm = (RelativeLayout) dialog.findViewById(R.id.confirm);
        RelativeLayout cancel = (RelativeLayout) dialog.findViewById(R.id.cancel);
        TextView textView = dialog.findViewById(R.id.logout_text_field);
        String logoutText = Utils.getColorText("logout", mContext.getResources().getColor(R.color.lightPurple));
        textView.setText(Html.fromHtml(mContext.getString(R.string.logout_text) + " " + logoutText + "?"), TextView.BufferType.SPANNABLE);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.logoutId();
                mCallback.clearFilterList();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
}
