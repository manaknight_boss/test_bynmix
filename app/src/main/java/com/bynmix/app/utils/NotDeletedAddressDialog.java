package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bynmix.app.R;

public class NotDeletedAddressDialog {
    public void showDialog(Context mContext, String message) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.not_deleted_address_dialog);
        TextView errorText = (TextView) dialog.findViewById(R.id.heading_text);
        RelativeLayout okButton =(RelativeLayout) dialog.findViewById(R.id.got_it);
        errorText.setText(Html.fromHtml(message), TextView.BufferType.SPANNABLE);
        ImageView close = (ImageView) dialog.findViewById(R.id.close);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               dialog.hide();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }
}
