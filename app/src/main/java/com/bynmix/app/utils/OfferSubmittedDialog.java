package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bynmix.app.R;

public class OfferSubmittedDialog {
    public void showDialog(Context mContext, int type, String message) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.offer_submitted_dialog);
        TextView heading = dialog.findViewById(R.id.heading_text);
        TextView description = dialog.findViewById(R.id.description);
        ImageView image = dialog.findViewById(R.id.logo);
        ImageView close = dialog.findViewById(R.id.close);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        if (type == 1) {
            image.setImageResource(R.mipmap.logo_white);
            heading.setText("Congratulations!");
            if (TextUtils.isEmpty(message)) {
                description.setText(mContext.getString(R.string.offer_submitted_text));
            } else {
                description.setText(Html.fromHtml(message), TextView.BufferType.SPANNABLE);
            }

        } else {
            image.setImageResource(R.mipmap.sad_cloud);
            heading.setText("Whoops! Something went wrong!");
            description.setText(message);
        }
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }
}
