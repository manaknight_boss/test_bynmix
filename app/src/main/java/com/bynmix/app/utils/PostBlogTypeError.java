package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.interfaces.CreatePostListener;
import com.bynmix.app.models.FeedResponse;

public class PostBlogTypeError {

    public void showDialog(Context mContext, final CreatePostListener mCallback, final String postType, String description, String confirmButton, String cancelButton, final FeedResponse post, final String photoUrl, final String oldPhotoUrl) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.post_blog_type_error);
        TextView errorText = (TextView) dialog.findViewById(R.id.error_text);
        TextView confirmText = (TextView) dialog.findViewById(R.id.confirm_text);
        TextView cancelText = (TextView) dialog.findViewById(R.id.cancel_text);
        RelativeLayout confirm = (RelativeLayout) dialog.findViewById(R.id.confirm);
        RelativeLayout cancel = (RelativeLayout) dialog.findViewById(R.id.cancel);
        ImageView close = (ImageView) dialog.findViewById(R.id.dialog_close);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            errorText.setText(Html.fromHtml("<b>" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + postType + "</font>" + "</b> " + description, Html.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
        } else {
            errorText.setText(Html.fromHtml("<b>" + "<font color='" + mContext.getResources().getColor(R.color.very_light_purple) + "'>" + postType + "</font>" + "</b> " + description), TextView.BufferType.SPANNABLE);
        }

        confirmText.setText(confirmButton);
        cancelText.setText(cancelButton);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (postType.equalsIgnoreCase(Constants.VIDEO_POSTS)) {
                    post.setPostType("" + Constants.WEB_LINK);
                } else {
                    post.setPostType("" + Constants.YOU_TUBE_LINK);
                }

                if (post.isEditable()) {
                    mCallback.editPostDetail(post, photoUrl, oldPhotoUrl);
                } else {
                    mCallback.sendPostDetail(post);
                }
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }


}

