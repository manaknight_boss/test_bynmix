package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.databinding.DialogPrintLabelBinding;
import com.bynmix.app.interfaces.DialogClickListener;
import com.bynmix.app.models.Stock;

public class PrintLabelDialog {
    public static void showDialog(final Context mContext, final Stock detail, final DialogClickListener okListener) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        DialogPrintLabelBinding dialogBinding = DialogPrintLabelBinding.inflate(LayoutInflater.from(mContext));
        dialog.setContentView(dialogBinding.getRoot());
        dialogBinding.title.setText(Html.fromHtml("\"" + "<b>" + "<font color='" + mContext.getResources().getColor(R.color.lightPurple) + "'>" + detail.getListingTitle().trim() + "</font>" + "</b>" + "\""), TextView.BufferType.SPANNABLE);
        dialogBinding.trackingNumber.setText(Html.fromHtml("By pressing \"" + Utils.getColorText("EMAIL SHIPPING LABEL", mContext.getResources().getColor(R.color.white)) + "\" " + "below, we will deduct " + Utils.getColorText("$4", mContext.getResources().getColor(R.color.lightPurple)) + " from your earnings to pay for shipping costs."));
        dialogBinding.crossIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialogBinding.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (okListener != null) {
                    okListener.onClick();
                    dialog.dismiss();
                }
            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }
}
