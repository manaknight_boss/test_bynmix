package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.Window;
import android.widget.ProgressBar;

import com.bynmix.app.R;


public class ProgressBarDialog {
    private Dialog popDialog;
    private Context mContext;
    private boolean isProgressBarVisible = true;

    public ProgressBarDialog(Context context) {
        this.mContext = context;
    }

    /*
     * This method display a message or alert for any functionality
     */
    public void showProgress() {
        if (isProgressBarVisible) {
            isProgressBarVisible = false;
            popDialog = new Dialog(mContext);
            popDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            popDialog.setContentView(R.layout.progressbar);
            popDialog.getWindow().setBackgroundDrawable(
                    new ColorDrawable(android.graphics.Color.TRANSPARENT));
            ProgressBar progressBar = popDialog.findViewById(R.id.progress_bar);

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                Drawable wrapDrawable = DrawableCompat.wrap(progressBar.getIndeterminateDrawable());
                DrawableCompat.setTint(wrapDrawable, ContextCompat.getColor(mContext, R.color.purple));
                progressBar.setIndeterminateDrawable(DrawableCompat.unwrap(wrapDrawable));
            } else {
                progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(mContext, R.color.purple), PorterDuff.Mode.SRC_IN);
            }

            popDialog.setCancelable(false);

            popDialog.show();
        }
    }

    public void hideProgress() {
        if (popDialog != null) {
            isProgressBarVisible = true;
            popDialog.dismiss();
            popDialog = null;
        }
    }
}
