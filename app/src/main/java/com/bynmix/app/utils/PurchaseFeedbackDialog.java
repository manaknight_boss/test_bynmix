package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.PurchasesDetail;
import com.bynmix.app.models.SalesDetail;
import com.bynmix.app.models.Stock;

public class PurchaseFeedbackDialog {
    Stock detail = new PurchasesDetail();

    public void showDialog(final Context mContext, final ProfileListener mCallBack, final Stock purchasesDetail, final int position) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.purchase_feedback_dialog);
        this.detail = purchasesDetail;
        ImageView cross = (ImageView) dialog.findViewById(R.id.cross_icon);
        final EditText feedbackEditText = (EditText) dialog.findViewById(R.id.feedback);
        RelativeLayout submitButton = (RelativeLayout) dialog.findViewById(R.id.submit);
        RelativeLayout cancelButton = (RelativeLayout) dialog.findViewById(R.id.cancel);
        TextView heading = (TextView) dialog.findViewById(R.id.heading);
        TextView title = (TextView) dialog.findViewById(R.id.title);
        LinearLayout starLayout = (LinearLayout) dialog.findViewById(R.id.star_layout);

        if (purchasesDetail instanceof PurchasesDetail) {
            heading.setText(mContext.getString(R.string.leave_feedback_for));
        } else {
            heading.setText(mContext.getString(R.string.leave_feedback_for_buyer));
        }

        title.setText(Html.fromHtml("\"" + "<b>" + "<font color='" + mContext.getResources().getColor(R.color.lightPurple) + "'>" + detail.getListingTitle().trim() + "</font>" + "</b>" + "\""), TextView.BufferType.SPANNABLE);

        feedbackEditText.setSelection(feedbackEditText.getText().length());
        drawStar(mContext, starLayout, detail);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String feedback = feedbackEditText.getText().toString().trim();
                detail.setFeedback(feedback);
                mCallBack.sendRating(detail, position);
                dialog.dismiss();
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }

    class StarClickListener implements View.OnClickListener {

        private final Context mContext;
        private final Stock mDetail;
        private final int starPosition;
        private LinearLayout starLayout;

        public StarClickListener(Context mContext, int starPosition, LinearLayout starLayout, Stock detail) {
            this.mContext = mContext;
            this.starLayout = starLayout;
            this.starPosition = starPosition;
            this.mDetail = detail;
        }

        @Override
        public void onClick(View view) {
            mDetail.setRating(starPosition);
            detail = mDetail;
            drawStar(mContext, starLayout, detail);
        }
    }

    private void drawStar(Context mContext, LinearLayout starLayout, Stock detail) {
        starLayout.removeAllViews();
        for (int i = 1; i <= 5; i++) {
            ImageView starImage = new ImageView(mContext);
            if (i <= detail.getRating()) {
                starImage.setImageResource(R.mipmap.star_large);
            } else {
                starImage.setImageResource(R.mipmap.white_outline_star);
            }
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            int margin = Utils.dpToPx(mContext, 5);
            params.setMargins(margin, 0, margin, 0);
            starImage.setLayoutParams(params);
            starLayout.addView(starImage);
            starImage.setOnClickListener(new StarClickListener(mContext, i, starLayout, detail));
        }
    }
}
