package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.TextView;

import com.bynmix.app.R;

public class SavingPostDialog {

    private Dialog dialog;
    private Context mContext;
    private String savingPost;

    public SavingPostDialog(Context mContext, String savingPost) {
        this.mContext = mContext;
        this.savingPost = savingPost;
    }


    public void showDialog() {
        dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.saving_post_dialog);
        TextView savingPostText = (TextView) dialog.findViewById(R.id.saving_post);
        savingPostText.setText(savingPost);
        dialog.show();
    }

    public void dismiss() {
        dialog.dismiss();
    }
}
