package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.adapter.SelectCategoryAdapter;
import com.bynmix.app.interfaces.CategoryListener;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.models.Category;
import com.bynmix.app.models.CategoryType;
import com.bynmix.app.models.ListingDetail;

public class SelectCategoryDialog implements CategoryListener {
    ListingDetail listingDetail = new ListingDetail();
    ImageView select;
    RecyclerView recyclerView;
    TextView categoryTypeText;

    public void showDialog(Context mContext, final CategoryType categoryType, final ProfileListener mCallback) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.select_category_dialog);
        select = (ImageView) dialog.findViewById(R.id.select);
        recyclerView = (RecyclerView) dialog.findViewById(R.id.recycle_view);
        categoryTypeText = (TextView) dialog.findViewById(R.id.category_type);
        categoryTypeText.setText(categoryType.getCategoryTypeName());
//        SelectCategoryAdapter adapter = new SelectCategoryAdapter(mContext, categoryType.getCategories(), this);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
//        recyclerView.setAdapter(adapter);
        updateUI();
        select.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listingDetail.setCategoryType(categoryType);
                mCallback.setCategory(listingDetail);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    @Override
    public void selectedCategory(ListingDetail category) {
//        this.listingDetail.setCategory(category);
        updateUI();
    }

    private void updateUI() {
        if (listingDetail.getCategory() != null) {
            select.setVisibility(View.VISIBLE);
        } else {
            select.setVisibility(View.INVISIBLE);
        }
    }
}
