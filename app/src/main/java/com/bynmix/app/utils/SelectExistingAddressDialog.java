package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.adapter.SelectExistingAddressAdapter;
import com.bynmix.app.interfaces.ProfileListener;
import com.bynmix.app.interfaces.SelectExistingAddressListener;
import com.bynmix.app.models.Address;

import java.util.List;

public class SelectExistingAddressDialog implements SelectExistingAddressListener {
    RelativeLayout updateLayout;
    ImageView cross;
    RecyclerView recyclerView;
    TextView headingText;
    Address address;
    ProfileListener mCallBack;

    public void showDialog(Context mContext, int type, final List<Address> addresses, final ProfileListener mCallBack) {
        final Dialog dialog = new Dialog(mContext);
        this.mCallBack = mCallBack;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.select_existing_address_dialog);
        cross = (ImageView) dialog.findViewById(R.id.cross_icon);
        recyclerView = (RecyclerView) dialog.findViewById(R.id.recycler_view);
        headingText = (TextView) dialog.findViewById(R.id.heading);
        updateLayout = (RelativeLayout) dialog.findViewById(R.id.update_layout);
        if (type == Constants.SHIPPING_ADDRESS) {
            headingText.setText(mContext.getString(R.string.select_exixting_address_text));
        } else {
            headingText.setText(mContext.getString(R.string.select_exixting_return_address_text));
        }
        SelectExistingAddressAdapter adapter = new SelectExistingAddressAdapter(mContext, addresses, type, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerView.setAdapter(adapter);
        dialog.show();

        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.hide();
            }
        });
        updateLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                address.setExistingAddress(true);
                mCallBack.editAddress(address);
                dialog.hide();
            }
        });
        Window window = dialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.CENTER;
        window.setAttributes(wlp);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        dialog.show();
        dialog.getWindow().setAttributes(wlp);
    }

    @Override
    public void selectedAddress(Address address) {
        this.address = address;
    }

}
