package com.bynmix.app.utils;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.InsetDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bynmix.app.R;
import com.bynmix.app.adapter.TrackingAdapter;
import com.bynmix.app.models.Stock;
import com.bynmix.app.models.Tracking;

public class TrackingDialog {
    public void showDialog(final Context mContext, final Stock detail, Tracking tracking) {
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.tracking_dialog);
        ImageView cross = (ImageView) dialog.findViewById(R.id.cross_icon);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recycler_view);
        TextView title = (TextView) dialog.findViewById(R.id.title);
        TextView trackNumber = (TextView) dialog.findViewById(R.id.tracking_number);
        LinearLayout noTrackingInformation = (LinearLayout) dialog.findViewById(R.id.no_tracking_information);
        title.setText(Html.fromHtml("\"" + "<b>" + "<font color='" + mContext.getResources().getColor(R.color.lightPurple) + "'>" + detail.getListingTitle().trim() + "</font>" + "</b>" + "\""), TextView.BufferType.SPANNABLE);
        if (tracking == null || tracking.getShippingHistory() == null || tracking.getShippingHistory().size() == 0) {
            recyclerView.setVisibility(View.GONE);
            noTrackingInformation.setVisibility(View.VISIBLE);
            trackNumber.setText(mContext.getString(R.string.seller_has_not_shipped));
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            noTrackingInformation.setVisibility(View.GONE);
            trackNumber.setText(tracking.getTrackingNumber());
            TrackingAdapter trackingAdapter = new TrackingAdapter(mContext, tracking.getShippingHistory());
            recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
            recyclerView.setAdapter(trackingAdapter);
        }
        cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.getWindow().setGravity(Gravity.TOP);
        ColorDrawable back = new ColorDrawable(Color.TRANSPARENT);
        int px = (int) (100 * mContext.getResources().getDisplayMetrics().density + 0.5f);
        InsetDrawable inset = new InsetDrawable(back, 0, px, 0, 0);
        dialog.getWindow().setBackgroundDrawable(inset);
        dialog.show();
    }
}
