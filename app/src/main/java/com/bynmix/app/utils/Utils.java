package com.bynmix.app.utils;

import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Display;
import android.view.WindowManager;

import com.bynmix.app.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;

public class Utils {
    private static int screenWidth = 0;
    private static int screenHeight = 0;

    public static int dpToPx(Context context, int dp) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale);
    }

    public static String getColorText(Context mContext, String string) {
        return "<b>" + "<font color='" + mContext.getResources().getColor(R.color.purple) + "'>" + string + "</font>" + "</b>";
    }

    public static String getColorText(String string, int color) {
        return "<b>" + "<font color='" + color + "'>" + string + "</font>" + "</b>";
    }

    public static int getScreenWidth(Context c) {
        if (screenWidth == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenWidth = size.x;
        }

        return screenWidth;
    }

    public static int getScreenHeight(Context c) {
        if (screenHeight == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenHeight = size.y;
        }
        return screenHeight;
    }

    public static boolean isAndroid5() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static String formatValueToTwoDecimalPlaces(double values) {
        return String.format("%.2f", values);
    }

    public static boolean containsURL(String input) {

        String tempInput = input.toLowerCase();
        String parts[] = tempInput.split(" ");
        boolean containUrl = false;
        for (int i = 0; i < parts.length; i++) {
            String part = parts[i];
            if (part.contains(".com") && !isEmailId(part) && !startOrEndsWithSpecialSymbols(part)) {
                containUrl = true;
                break;
            } else if (part.contains("www.")) {
                containUrl = true;
                break;
            } else if (part.contains("http://")) {
                containUrl = true;
                break;
            } else if (part.contains("https://")) {
                containUrl = true;
                break;
            }
        }
        return containUrl;
    }

    private static boolean startOrEndsWithSpecialSymbols(String part) {
        if (Character.isLetter(part.charAt(0)) || Character.isLetter(part.charAt(part.length() - 1))) {
            return false;
        }
        return true;
    }

    private static boolean isEmailId(String part) {
        Pattern pattern = Pattern.compile("[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}");
        Matcher mat = pattern.matcher(part);
        if (mat.matches()) {
            return true;
        }
        return false;
    }

    public static String ErrorMessage(Context mContext, ResponseBody response) {
        String message;
        try {
            JSONObject errorObject = new JSONObject(response.string().trim());
            JSONArray error = errorObject.getJSONArray("errors");
            JSONObject messageObject = error.getJSONObject(0);
            message = messageObject.getString("message");
        } catch (Exception e) {
            message = mContext.getString(R.string.error_server);
        }
        return message;
    }

    public static void setCapitalizeTextWatcher(final AppCompatEditText editText) {
        final TextWatcher textWatcher = new TextWatcher() {

            int mStart = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mStart = start + count;
            }

            @Override
            public void afterTextChanged(Editable s) {
                String input = s.toString();
                String capitalizedText;
                if (input.length() < 1)
                    capitalizedText = input;
                else
                    capitalizedText = input.substring(0, 1).toUpperCase() + input.substring(1);
                if (!capitalizedText.equals(editText.getText().toString())) {
                    editText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            editText.setSelection(mStart);
                            editText.removeTextChangedListener(this);
                        }
                    });
                    editText.clearComposingText();
                    editText.setText(capitalizedText);
                }
            }
        };

        editText.addTextChangedListener(textWatcher);
    }
}

